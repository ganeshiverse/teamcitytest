package com.babylon.recognition;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.babylon.App;
import com.babylon.R;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.ActivityRecognitionClient;

/**
 * Class for connecting to Location Services and removing activity recognition updates.
 * <b>
 * Note: Clients must ensure that Google Play services is available before removing activity
 * recognition updates.
 * </b> Use GooglePlayServicesUtil.isGooglePlayServicesAvailable() to check.
 *
 *
 * To use a DetectionRemover, instantiate it, then call removeUpdates().
 *
 */
public class DetectionRemover implements ConnectionCallbacks, OnConnectionFailedListener {

    private static final String TAG = DetectionRemover.class.getSimpleName();

    private Context mContext;

    private ActivityRecognitionClient mActivityRecognitionClient;

    private PendingIntent mCurrentIntent;

    /**
     * Construct a DetectionRemover for the current Context
     *
     * @param context A valid Context
     */
    public DetectionRemover(Context context) {
        mContext = context;
        mActivityRecognitionClient = null;
    }

    /**
     * Remove the activity recognition updates associated with a PendIntent. The PendingIntent is
     * the one used in the request to add activity recognition updates.
     *
     * @param requestIntent The PendingIntent used to request activity recognition updates
     */
    public void removeUpdates(PendingIntent requestIntent) {
        mCurrentIntent = requestIntent;
        requestConnection();
    }

    /**
     * Request a connection to Location Services. This call returns immediately,
     * but the request is not complete until onConnected() or onConnectionFailure() is called.
     */
    private void requestConnection() {
        getActivityRecognitionClient().connect();
    }

    /**
     * Get the current activity recognition client, or create a new one if necessary.
     *
     * @return An ActivityRecognitionClient object
     */
    public ActivityRecognitionClient getActivityRecognitionClient() {
        if (mActivityRecognitionClient == null) {
            setActivityRecognitionClient(new ActivityRecognitionClient(mContext, this, this));
        }
        return mActivityRecognitionClient;
    }

    /**
     * Get a activity recognition client and disconnect from Location Services
     */
    private void requestDisconnection() {
        getActivityRecognitionClient().disconnect();
        setActivityRecognitionClient(null);
    }

    /**
     * Set the global activity recognition client
     * @param client An ActivityRecognitionClient object
     */
    public void setActivityRecognitionClient(ActivityRecognitionClient client) {
        mActivityRecognitionClient = client;

    }

    /*
     * Called by Location Services once the activity recognition client is connected.
     *
     * Continue by removing activity recognition updates.
     */
    @Override
    public void onConnected(Bundle connectionData) {
        Log.d(TAG, mContext.getString(R.string.location_service_disconnected));
        continueRemoveUpdates();
    }

    /**
     * Once the connection is available, send a request to remove activity recognition updates.
     */
    private void continueRemoveUpdates() {
        mActivityRecognitionClient.removeActivityUpdates(mCurrentIntent);
        mCurrentIntent.cancel();
        requestDisconnection();
    }

    /*
     * Called by Location Services once the activity recognition client is disconnected.
     */
    @Override
    public void onDisconnected() {
        L.i(TAG, mContext.getString(R.string.location_service_disconnected));
        mActivityRecognitionClient = null;
    }

    /*
     * Implementation of OnConnectionFailedListener.onConnectionFailed
     * If a connection or disconnection request fails, report the error
     * connectionResult is passed in from Location Services
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
//            try {
                sendConnectionFailureBroadcast();
//                connectionResult.startResolutionForResult((Activity) mContext,
//                        Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);
//            } catch (SendIntentException e) {
//                L.e(TAG, e.toString());
//            }
        } else {
//            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
//                    connectionResult.getErrorCode(),
//                    (Activity) mContext,
//                    Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);
//            if (dialog != null) {
//                dialog.show();
//            }
        }
    }

    private void sendConnectionFailureBroadcast() {
        Intent intent = new Intent(Constants.CONNECTION_FAILURE_FILTER);
        intent.setAction(Constants.DETECTIONREMOVER_CONNECTION_FAILURE);
        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(intent);
    }
}

