package com.babylon.api.request.base;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.babylon.utils.L;

public class ResponseLoader<O> extends AsyncTaskLoader<Response<O>> {

    private static final String TAG = ResponseLoader.class.getSimpleName();
    private final AbsRequest request;

    public ResponseLoader(Context context, AbsRequest request) {
        super(context);
        this.request = request;
    }

    @Override
    public Response<O> loadInBackground() {
        L.d(TAG, "loadInBackground() " + request.toString());
        Response<O> response = request.execute();
        return response;
    }

    @Override
    protected void onStartLoading() {
        L.d(TAG, "onStartLoading() " + request.toString());
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        request.cancel();
        L.d(TAG, "onStopLoading() " + request.toString());
    }
}
