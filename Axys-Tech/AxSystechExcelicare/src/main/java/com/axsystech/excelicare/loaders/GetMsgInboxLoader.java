package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by bhargav on 7/29/2015.
 */
public class GetMsgInboxLoader extends DataAsyncTaskLibLoader<MessagesInboxAndSentResponse> {

    private String token;
    private String panelId;
    private String patientId;
    private int start;
    private int max;
    private int isDictionaryJSONResponse;

    public GetMsgInboxLoader(Context context, String token, String panelId, String patientId, int start, int max, int isDictionaryJSONResponse) {
        super(context);
        this.token = token;
        this.panelId = panelId;
        this.patientId = patientId;
        this.start = start;
        this.max = max;
        this.isDictionaryJSONResponse = isDictionaryJSONResponse;
    }

    @Override
    protected MessagesInboxAndSentResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMessageInboxListContent(token, panelId, patientId, start, max, isDictionaryJSONResponse);
    }
}
