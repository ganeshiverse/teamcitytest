package com.iversecomics.client.util;

public class Time {
    public static final long SECOND = 1000;       // milliseconds
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR   = MINUTE * 60;
    public static final long DAY    = HOUR * 24;
    public static final long WEEK   = DAY * 7;
    public static final long YEAR   = DAY * 365;
}
