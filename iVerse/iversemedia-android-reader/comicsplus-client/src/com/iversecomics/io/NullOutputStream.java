package com.iversecomics.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Output stream that drops all write requests.
 * 
 * @version $Id$
 */
public class NullOutputStream extends OutputStream {

    @Override
    public void close() throws IOException {
        /* do nothing */
    }

    @Override
    public void flush() throws IOException {
        /* do nothing */
    }

    @Override
    public void write(byte[] b) throws IOException {
        /* do nothing */
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        /* do nothing */
    }

    @Override
    public void write(int b) throws IOException {
        /* do nothing */
    }
}
