package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Nutrient;
import com.babylon.utils.DateUtils;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ChartGet extends AbsRequestGet<ResponseList<Nutrient>> {

    @Override
    public String getUrl() {
        String url = Urls.get(Urls.CHART_GET);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param date date start of day
     */
    public static ChartGet newChartGet(long date, String periodId, String featureId) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

        long currentTimeInSeconds = DateUtils.getDateNowInSeconds();
        params.add(new BasicNameValuePair("dateNow", String.valueOf(currentTimeInSeconds)));

        params.add(new BasicNameValuePair("date", String.valueOf(date)));
        params.add(new BasicNameValuePair("period", periodId));
        params.add(new BasicNameValuePair("feature", featureId));

        return (ChartGet) new ChartGet().withParams(params);
    }
}
