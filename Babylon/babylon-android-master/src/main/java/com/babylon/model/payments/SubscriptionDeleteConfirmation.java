package com.babylon.model.payments;

import com.google.gson.annotations.SerializedName;

public class SubscriptionDeleteConfirmation {

    @SerializedName("confirm_title")
    private String Title;
    @SerializedName("confirm_text")
    private String Text;
    @SerializedName("confirm_button_text")
    private String ButtonText;

    private Subscription subscription;
    private Promotion promotion;

    public SubscriptionDeleteConfirmation(String title, String message, String buttonText) {
        this.Title = title;
        this.Text = message;
        this.ButtonText = buttonText;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getButtonText() {
        return ButtonText;
    }

    public void setButtonText(String buttonText) {
        ButtonText = buttonText;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Promotion getPromotion() {
        return promotion;
    }
}
