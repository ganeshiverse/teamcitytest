package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/16/2015.
 */
public class GetCircleInvitationReceivedFromDbLoader extends DataAsyncTaskLibLoader<List<CirclesInvitationReceivedDbModel>> {

    public GetCircleInvitationReceivedFromDbLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<CirclesInvitationReceivedDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<CirclesInvitationReceivedDbModel, Long> circleInvitationReceivedListDao = helper.getDao(CirclesInvitationReceivedDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<CirclesInvitationReceivedDbModel>>() {
                @Override
                public List<CirclesInvitationReceivedDbModel> call() throws Exception {
                    return circleInvitationReceivedListDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
