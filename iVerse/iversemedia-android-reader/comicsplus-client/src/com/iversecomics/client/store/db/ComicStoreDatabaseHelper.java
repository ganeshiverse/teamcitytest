package com.iversecomics.client.store.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicStoreDatabaseHelper extends SQLiteOpenHelper {
    private static final Logger             LOG       = LoggerFactory.getLogger(ComicStoreDatabaseHelper.class);
    private static ComicStoreDatabaseHelper singleton = null;
    private Context context;

    public static synchronized ComicStoreDatabaseHelper getInstance(Context context) {
        if (singleton == null) {
            singleton = new ComicStoreDatabaseHelper(context);
        }
        return singleton;
    }

    public ComicStoreDatabaseHelper(Context context) {
        super(context, ComicStoreDB.DB_NAME, null, ComicStoreDB.DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql;

        // Genres Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(GenresTable.TABLE);
        sql.append(" ( ").append(GenresTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(GenresTable.GENREID).append(" TEXT NOT NULL");
        sql.append(" , ").append(GenresTable.NAME).append(" TEXT");
        sql.append(" , ").append(GenresTable.BANNER_FILENAME).append(" TEXT");
        sql.append(" , UNIQUE ( ").append(GenresTable.GENREID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX genreIdsIndex ON ").append(GenresTable.TABLE);
        sql.append(" ( ").append(GenresTable.GENREID);
        sql.append(" );");
        db.execSQL(sql.toString());

        // Publishers Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(PublishersTable.TABLE);
        sql.append(" ( ").append(PublishersTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(PublishersTable.PUBLISHERID).append(" TEXT NOT NULL");
        sql.append(" , ").append(PublishersTable.NAME).append(" TEXT");
        sql.append(" , ").append(PublishersTable.FEATURED).append(" TEXT");
        sql.append(" , ").append(PublishersTable.BANNER_IMAGE_FILE_NAME).append(" TEXT");
        sql.append(" , ").append(PublishersTable.LOGO_IMAGE_FILE_NAME).append(" TEXT");
        sql.append(" , ").append(PublishersTable.IMAGE_URL).append(" TEXT");
        sql.append(" , UNIQUE ( ").append(PublishersTable.PUBLISHERID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX publisherIdsIndex ON ").append(PublishersTable.TABLE);
        sql.append(" ( ").append(PublishersTable.PUBLISHERID);
        sql.append(" );");
        db.execSQL(sql.toString());

        // Groups Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(GroupsTable.TABLE);
        sql.append(" ( ").append(GroupsTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(GroupsTable.GROUPID).append(" TEXT NOT NULL");
        sql.append(" , ").append(GroupsTable.NAME).append(" TEXT");
        sql.append(" , ").append(GroupsTable.PARENTID).append(" TEXT");
        sql.append(" , ").append(GroupsTable.PUBLISHERID).append(" TEXT");
        sql.append(" , ").append(GroupsTable.FEATURED).append(" TEXT");
        sql.append(" , ").append(GroupsTable.IMAGE_URL).append(" TEXT");
        sql.append(" , ").append(GroupsTable.BANNER_IMAGE_FILE_NAME).append(" TEXT");
        sql.append(" , ").append(GroupsTable.LOGO_IMAGE_FILE_NAME).append(" TEXT");
        sql.append(" , UNIQUE ( ").append(GroupsTable.GROUPID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX groupIdsIndex ON ").append(GroupsTable.TABLE);
        sql.append(" ( ").append(GroupsTable.GROUPID);
        sql.append(" );");
        db.execSQL(sql.toString());

        // Comics Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(ComicsTable.TABLE);
        sql.append(" ( ").append(ComicsTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(ComicsTable.COMICID).append(" TEXT NOT NULL");
        sql.append(" , ").append(ComicsTable.NAME).append(" TEXT");
        sql.append(" , ").append(ComicsTable.COMIC_BUNDLE_NAME).append(" TEXT");
        sql.append(" , ").append(ComicsTable.AVG_RATING).append(" REAL");
        sql.append(" , ").append(ComicsTable.DESCRIPTION).append(" TEXT");
        sql.append(" , ").append(ComicsTable.IMAGE_FILE_NAME).append(" TEXT");
        sql.append(" , ").append(ComicsTable.LEGAL).append(" TEXT");
        sql.append(" , ").append(ComicsTable.NUMBER).append(" INTEGER");
        sql.append(" , ").append(ComicsTable.AMOUNT_USD).append(" INTEGER");
        sql.append(" , ").append(ComicsTable.PRICING_TIER).append(" INTEGER");
        sql.append(" , ").append(ComicsTable.PRODUCT_TYPE).append(" TEXT");
        sql.append(" , ").append(ComicsTable.RAW_NAME).append(" TEXT");
        sql.append(" , ").append(ComicsTable.SERIES_BUNDLE_NAME).append(" TEXT");
        sql.append(" , ").append(ComicsTable.SKU).append(" TEXT");
        sql.append(" , ").append(ComicsTable.PUBLISHERID).append(" TEXT");
        sql.append(" , ").append(ComicsTable.PUBLISHER_LEGACYID).append(" TEXT");
        sql.append(" , ").append(ComicsTable.PUBLISHER_NAME).append(" TEXT");
        sql.append(" , ").append(ComicsTable.SUBSCRIPTION).append(" INTEGER");
        sql.append(" , ").append(ComicsTable.TAPJOY).append(" INTEGER");
        sql.append(" , ").append(ComicsTable.ASSET_TYPES).append(" TEXT");
        sql.append(" , UNIQUE ( ").append(ComicsTable.COMICID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX comicIdsIndex ON ").append(ComicsTable.TABLE);
        sql.append(" ( ").append(ComicsTable.COMICID);
        sql.append(" );");
        db.execSQL(sql.toString());

        // Ordering Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(OrderingTable.TABLE);
        sql.append(" ( ").append(OrderingTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(OrderingTable.COMICID).append(" TEXT NOT NULL");
        sql.append(" , ").append(OrderingTable.INDEX).append(" INTEGER");
        sql.append(" , ").append(OrderingTable.LISTID).append(" TEXT");
        sql.append(" , ").append(OrderingTable.TIMESTAMP).append(" INTEGER");
        sql.append(" , UNIQUE ( ").append(OrderingTable.COMICID);
        sql.append(" , ").append(OrderingTable.LISTID);
        sql.append(" , ").append(OrderingTable.INDEX);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX comicIdOrderingIndex ON ").append(OrderingTable.TABLE);
        sql.append(" ( ").append(OrderingTable.COMICID);
        sql.append(" , ").append(OrderingTable.LISTID);
        sql.append(" );");
        db.execSQL(sql.toString());

        // Freshness Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(FreshnessTable.TABLE);
        sql.append(" ( ").append(FreshnessTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(FreshnessTable.KEY).append(" TEXT NOT NULL");
        sql.append(" , ").append(FreshnessTable.TIMESTAMP_CREATED).append(" INTEGER");
        sql.append(" , ").append(FreshnessTable.TIMESTAMP_EXPIRED).append(" INTEGER");
        sql.append(" , UNIQUE ( ").append(FreshnessTable.KEY);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX freshnessKeyIndex ON ").append(FreshnessTable.TABLE);
        sql.append(" ( ").append(FreshnessTable.KEY);
        sql.append(" );");
        db.execSQL(sql.toString());

        // Server Config Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(ServerConfigTable.TABLE);
        sql.append(" ( ").append(ServerConfigTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(ServerConfigTable.KEY).append(" TEXT NOT NULL");
        sql.append(" , ").append(ServerConfigTable.VALUE).append(" TEXT");
        sql.append(" , UNIQUE ( ").append(ServerConfigTable.KEY);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX configKeyIndex ON ").append(ServerConfigTable.TABLE);
        sql.append(" ( ").append(ServerConfigTable.KEY);
        sql.append(" );");
        db.execSQL(sql.toString());
        
        
        // Featured Slots Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(FeaturedSlotTable.TABLE);
        sql.append(" ( ").append(FeaturedSlotTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(FeaturedSlotTable.SLOTID).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.SLOT).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.SLOTTYPE).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.TITLE).append(" TEXT");
        sql.append(" , ").append(FeaturedSlotTable.IMAGEURL).append(" TEXT");
        sql.append(" , ").append(FeaturedSlotTable.TARGETURL).append(" TEXT");
        sql.append(" , ").append(FeaturedSlotTable.SUBSCRIPTION).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.LANGUAGECODE).append(" TEXT");
        sql.append(" , ").append(FeaturedSlotTable.PRODUCTID).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.PROMOTIONID).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.SUPPLIERID).append(" INTEGER");
        sql.append(" , ").append(FeaturedSlotTable.GROUPID).append(" INTEGER");
        sql.append(" , UNIQUE ( ").append(FeaturedSlotTable.SLOTID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());
        
        // Promotion Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(PromotionTable.TABLE);
        sql.append(" ( ").append(PromotionTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(PromotionTable.PROMOTIONID).append(" INTEGER");
        sql.append(" , ").append(PromotionTable.NAME).append(" TEXT");
        sql.append(" , ").append(PromotionTable.IMAGEURL).append(" TEXT");
        sql.append(" , ").append(PromotionTable.IMAGEURLPHONE).append(" TEXT");
        sql.append(" , ").append(PromotionTable.TARGETURL).append(" TEXT");
        sql.append(" , ").append(PromotionTable.ACTIVE).append(" INTEGER");
        sql.append(" , UNIQUE ( ").append(PromotionTable.PROMOTIONID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            LOG.warn("Upgrading from version %d to %d, which will destroy all old data.", oldVersion, newVersion);
            rebuild(db);
        }
    }

    public void rebuild(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + GenresTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS genreIdsIndex");
        db.execSQL("DROP TABLE IF EXISTS " + PublishersTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS publisherIdsIndex");
        db.execSQL("DROP TABLE IF EXISTS " + GroupsTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS groupIdsIndex");
        db.execSQL("DROP TABLE IF EXISTS " + ComicsTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS comicIdsIndex");
        db.execSQL("DROP TABLE IF EXISTS " + OrderingTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS comicIdOrderingIndex");
        db.execSQL("DROP TABLE IF EXISTS " + FreshnessTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS freshnessKeyIndex");
        db.execSQL("DROP TABLE IF EXISTS " + ServerConfigTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS configKeyIndex");
        
        db.execSQL("DROP TABLE IF EXISTS " + FeaturedSlotTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PromotionTable.TABLE);
        
        onCreate(db);
    }

    @Override
    public synchronized SQLiteDatabase getWritableDatabase() {
        try {
            return super.getWritableDatabase();
        } catch (SQLiteException e) {
            // that's our notification
            LOG.warn("Found an issue opening the database: " + e);
        }

        // try to delete the file
        context.deleteDatabase(getDatabaseName());

        // now return a freshly created database
        return super.getWritableDatabase();
    }
}
