package com.babylon.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Address;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.babylon.App;
import com.babylon.BuildConfig;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.adapter.RegionArrayAdapter;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.CreatePatientPost;
import com.babylon.api.request.RegionsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.model.payments.PromoCode;
import com.babylon.utils.*;
import com.babylon.view.TickButtonWithProgress;
import com.google.gson.JsonSyntaxException;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.seismic.ShakeDetector;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateAccountActivity extends BaseActivity implements View.OnClickListener , ShakeDetector.Listener{

    private static final String TAG = CreateAccountActivity.class.getSimpleName();

    private EditText firstNameEdt;
    private EditText lastNameEdt;
    private EditText emailEdt;
    private EditText passwordEdt;
    private EditText promoCodeEditText;
    private Spinner regionSpinner;
    private TickButtonWithProgress createAccountBtn;
    private CheckBox isAgreedTermsAndConditionsCheckBox;
    private Date birthday;
    private EditText birthDateditText;
    // private BirthdayViewController birthdayViewController;
    private Region region;
    private Patient facebookPatient;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    //private ProgressDialog mProgressDialog;
    private  static  int MIN_AGE=16;
    private RequestQueue mQueue;
    private JsonObjectRequest mJsonReq;
    private static final int SELECT_PRACTICE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        // Mix panel event for registration started
        JSONObject props = new JSONObject();
        try
        {
            props.put("Date",PreferencesManager.getInstance().getDateString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Bundle fb_parameters = new Bundle();
        fb_parameters.putString("Date", PreferencesManager.getInstance().getDateString());

        MixPanelHelper helper = new MixPanelHelper(getApplicationContext(),props,fb_parameters);
        helper.sendEvent(MixPanelEventConstants.REGISTRATION_STARTED);

        AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.REGISTRATION_STARTED_EVENT);
        myCalendar = Calendar.getInstance();
        initViews();
        loadLocations();
        Patient patient = (Patient) getIntent().getSerializableExtra(Keys.PATIENT);
        if (patient != null) {

                fillPatientInfo(patient);

        }

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(CreateAccountActivity.this);
        sd.start(sensorManager);
    }

    public void initViews() {

        getActionBar().show();

        setupActionBar(ActionBarType.CREATE_ACCOUNT);

        firstNameEdt = (EditText) findViewById(R.id.nameEdt);
        lastNameEdt = (EditText) findViewById(R.id.lastNameEdt);
        promoCodeEditText = (EditText) findViewById(R.id.promo_code_edit_text);

        emailEdt = (EditText) findViewById(R.id.emailEdt);
        //emailConfirmEdt = (EditText) findViewById(R.id.confEmailEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);
        //TextView birthDateTextView = (TextView) findViewById(R.id.dateofBirthEdt);
        createAccountBtn = (TickButtonWithProgress) findViewById(R.id.createAccountBtn);
        //TextView signInTxt = (TextView) findViewById(R.id.signInTxt);
        regionSpinner = (Spinner) findViewById(R.id.location_spinner);
        isAgreedTermsAndConditionsCheckBox = (CheckBox) findViewById(R.id
                .create_account_terms_and_conditions_check_box);

        createAccountBtn.setOnClickListener(this);
        //signInTxt.setOnClickListener(this);

        // disable auto correction on promotion code
        promoCodeEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        //birthDateErrorEditText = (EditText) findViewById(R.id.edit_text_birthday_error);
        //birthDateErrorEditText.setInputType(InputType.TYPE_NULL);

        birthDateditText = (EditText) findViewById(R.id.dateofBirthEdt);

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                birthday = myCalendar.getTime();
                updateLabel();
            }

        };



        birthDateditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(CreateAccountActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        birthDateditText.setTextColor(Color.WHITE);
        birthDateditText.setText(sdf.format(birthday));
    }

    private void fillPatientInfo(Patient patient) {
        facebookPatient = patient;

        final String firstName = patient.getFirstName();
        final String lastName = patient.getLastName();
        final String email = patient.getEmail();
        Date patientBirthday = patient.getBirthday();

        if (!TextUtils.isEmpty(email)) {
            emailEdt.setText(email);
            //emailConfirmEdt.setText(email);
        }
        if (!TextUtils.isEmpty(firstName)) {
            firstNameEdt.setText(firstName);
        }
        if (!TextUtils.isEmpty(lastName)) {
            lastNameEdt.setText(lastName);
        }
        if (patientBirthday != null) {
            //   birthday = patientBirthday;
            //birthdayViewController.setDate(birthday);
        }
    }


    private void loadLocations() {
       // mProgressDialog = UIUtils.getLoadingDialog(this);
       // mProgressDialog.show();
        RegionsGet regionsGet = new RegionsGet();
        regionsGet.execute(this, new RequestListener<Region[]>() {

            @Override
            public void onRequestComplete(Response<Region[]> response)
            {
              //  if(mProgressDialog.isShowing()) mProgressDialog.hide();
                if (response.getStatus() == HttpStatus.SC_OK)
                {
                    if (response.getData() != null)
                    {
                        Region[] regions = response.getData();
                        PreferencesManager.getInstance().setRegions(regions);

                        if (regions.length != 0)
                        {
                            createAccountBtn.setEnabled(true);
                            int selectedRegionIndex = 0;
                            Address currentAddress = App.getInstance().getAddress();
                            if (currentAddress != null)
                            {
                                String countryCode = new Locale("", currentAddress.getCountryCode()).getISO3Country();
                                for (int regionIndex = 0; regionIndex < regions.length; ++regionIndex)
                                {
                                    if (regions[regionIndex].getCountryCode().equals(countryCode))
                                    {
                                        selectedRegionIndex = regionIndex;
                                        break;
                                    }
                                }
                            }
                            Region selectedRegion = regions[selectedRegionIndex];
                            setRegion(selectedRegion);
                            regionSpinner.setEnabled(true);

                            //LinearLayout locationLayout = (LinearLayout) findViewById(R.id.location_layout);
                            //View locationDivider = findViewById(R.id.location_divider);

                            /*locationDivider.setVisibility(View.VISIBLE);
                            locationLayout.setVisibility(View.VISIBLE);*/

                            final RegionArrayAdapter adapter = new RegionArrayAdapter(CreateAccountActivity.this,
                                    R.layout.spinner_item_region, regions);
                            regionSpinner.setAdapter(adapter);
                            regionSpinner.setSelection(selectedRegionIndex);
                            regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    setRegion(adapter.getItem(position));
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // empty
                                }
                            });
                        }
                    }
                }
                else
                {
                   // if(mProgressDialog.isShowing()) mProgressDialog.hide();
                    new AlertDialog.Builder(CreateAccountActivity.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                            .setTitle("Error")
                            .setMessage("Unable to complete request. Please try again later")
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            CreateAccountActivity.this.onBackPressed();
                                        }
                                    })
                            .setCancelable(false)
                            .show();
                }
            }


        });
    }

    private void setRegion(Region region) {
        this.region = region;
        Integer minAge = region.getMinimumAge();
        if (minAge != null && minAge != Constants.REGION_MIN_AGE_DEFAULT) {
            //birthdayViewController.setMinAge(minAge);
            //birthdayViewController.validateBirthday(birthday, birthDateErrorEditText);
        }
    }

    private void configOkButton(boolean isValid) {
        final LinearLayout okButton = (LinearLayout) findViewById(R.id.createAccountBtn);

        if (okButton.isEnabled() != isValid) {
            okButton.setEnabled(isValid);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.createAccountBtn:
                //String emailConfirmation = emailConfirmEdt.getText().toString();
                //if (!TextUtils.isEmpty(emailConfirmation)) {
                if (App.getInstance().isConnection())
                {
                        createAccount();
                }
                else
                {
                    App.getInstance().showWaitingMessage(R.string.offline_error);
                }
                //showPracticeSelection();
                //} else {
                //    AlertDialogHelper.getInstance().showMessage(this, getString(R.string.empty_email_confirmation));
                //}
                break;
            /*case R.id.signInTxt:
                Intent intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();
                break;*/
            default:
                break;
        }
    }

   /* private void createAccount() {
        Patient newPatient = getPatient();

        boolean result=validateDOB();

        if(result) {

            boolean isAllFieldsFilled = isAllFieldsFilled();
            if (isAllFieldsFilled && isAgreedTermsAndConditionsCheckBox.isChecked()) {
                final CreatePatientPost patientsPost = new CreatePatientPost();
                patientsPost.withEntry(newPatient);
                createAccountBtn.setProgressState(true);
                patientsPost.execute(this, new RequestListener<Patient>() {
                    @Override
                    public void onRequestComplete(Response<Patient> response) {
                        processResponse(response);
                        getSupportLoaderManager().destroyLoader(patientsPost.hashCode());
                    }
                });
            } else if (!isAllFieldsFilled) {
                Toast.makeText(this, R.string.sign_up_filled_fields, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.terms_and_conditions_must_be,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }*/


    private void createAccount()
    {
        Patient newPatient = getPatient();

        if (!isAllFieldsFilled()) {
            AlertDialogHelper.getInstance().showMessage(this, R.string.sign_up_fill_in_all_fields);
            return;
        }

        if (newPatient.getRegionId() == null) {

            AlertDialogHelper.getInstance().showMessage(this, R.string.sign_up_select_region);
            return;
        }

        if (!newPatient.getAgreedTermsAndConditions()) {
            AlertDialogHelper.getInstance().showMessage(this, R.string.terms_and_conditions_must_be);
            return;
        }
        if (!CommonHelper.isEmailValid(newPatient.getEmail())) {

            AlertDialogHelper.getInstance().showMessage(this, R.string.email_not_valid);
            return;

        }
        if (newPatient.getFirstName().trim().length() == 0) {
            AlertDialogHelper.getInstance().showMessage(this, R.string.fname_not_valid);
            return;
        }
        if (newPatient.getLastName().trim().length() == 0) {
            AlertDialogHelper.getInstance().showMessage(this, R.string.fname_not_valid);
        }
        if(birthday == null)
        {
            AlertDialogHelper.getInstance().showMessage(this, R.string.dob_empty_error);
            return;
        }
        if (birthday == null || (new Date().getYear() - birthday.getYear()) < 16)
        {
            AlertDialogHelper.getInstance().showMessage(this, R.string.age_not_valid);
            return;
        }
            boolean isAllFieldsFilled = isAllFieldsFilled();
            if (isAllFieldsFilled && isAgreedTermsAndConditionsCheckBox.isChecked())
            {
                final CreatePatientPost patientsPost = new CreatePatientPost();
                patientsPost.withEntry(newPatient);
                createAccountBtn.setProgressState(true);
                patientsPost.execute(this, new RequestListener<Patient>() {
                    @Override
                    public void onRequestComplete(Response<Patient> response) {
                        processResponse(response);
                        getSupportLoaderManager().destroyLoader(patientsPost.hashCode());
                    }
                });
            } else if (!isAllFieldsFilled)
            {
                Toast.makeText(this, R.string.sign_up_filled_fields, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.terms_and_conditions_must_be,
                        Toast.LENGTH_SHORT).show();

        }
    }



    private boolean validateName(String firstName) {
        Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
        Matcher ms = ps.matcher(firstName);
        boolean bs = ms.matches();

        return  ((!bs)) ? false :true ;


    }

    public static float getAge(final Date birthdate) {
        return getAge(Calendar.getInstance().getTime(), birthdate);
    }


    public static float getAge(final Date current, final Date birthdate) {

        if (birthdate == null) {
            return 0;
        }
        if (current == null) {
            return getAge(birthdate);
        } else {
            final Calendar calend = new GregorianCalendar();
            calend.set(Calendar.HOUR_OF_DAY, 0);
            calend.set(Calendar.MINUTE, 0);
            calend.set(Calendar.SECOND, 0);
            calend.set(Calendar.MILLISECOND, 0);

            calend.setTimeInMillis(current.getTime() - birthdate.getTime());

            float result = 0;
            result = calend.get(Calendar.YEAR) - 1970;
            result += (float) calend.get(Calendar.MONTH) / (float) 12;
            return result;
        }

    }



    private boolean isAllFieldsFilled() {
        boolean isEmailFilled = !TextUtils.isEmpty(emailEdt.getText().toString());
        boolean isPasswordFilled = !TextUtils.isEmpty(passwordEdt.getText().toString());
        boolean isNameFilled = !TextUtils.isEmpty(firstNameEdt.getText().toString());
        boolean isSurnameFilled = !TextUtils.isEmpty(lastNameEdt.getText().toString());

        return  isEmailFilled && isPasswordFilled
                && isNameFilled && isSurnameFilled;
    }

    public Patient getPatient() {

        String firstName = (firstNameEdt.getText().toString().trim());
            String lastName =lastNameEdt.getText().toString().trim();

        final String email = emailEdt.getText().toString().trim();
             //   emailEdt.getText().toString().trim();
        final String promoCode = promoCodeEditText.getText().toString().trim();

        //Mix Panel Event for Promocode
       // if(promoCode != null || !promoCode.isEmpty()) {
        if(!TextUtils.isEmpty(promoCode)) {
            try {
                JSONObject props = new JSONObject();
                props.put("Promo Code Title", promoCode);

                Bundle fb_parameters = new Bundle();
                fb_parameters.putString("Promo Code Title", promoCode);


                MixPanelHelper helper = new MixPanelHelper(CreateAccountActivity.this, props,fb_parameters);
                helper.sendEvent(MixPanelEventConstants.PROMO_CODE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //final String emailConfirm = emailConfirmEdt.getText().toString();
        final String password = passwordEdt.getText().toString();
        boolean isAgreedTermsAndConditions = isAgreedTermsAndConditionsCheckBox.isChecked();

        final Patient newPatient = facebookPatient != null ?
                facebookPatient : new Patient();
        newPatient.setFirstName(firstName);
        newPatient.setLastName(lastName);
        newPatient.setEmail(email);
        //newPatient.setEmailConfirmation(emailConfirm);
        newPatient.setPassword(password);
        if (region != null) {
            newPatient.setRegionId(region.getId());
        }
        newPatient.setAgreedTermsAndConditions(isAgreedTermsAndConditions);

        newPatient.setBirthday(birthday);


        if (!TextUtils.isEmpty(promoCode))
        {
            List<PromoCode> redemptionAttributes = new ArrayList<>();
            PromoCode promotionCode = new PromoCode();
            promotionCode.setCode(promoCode);
            redemptionAttributes.add(promotionCode);
            newPatient.setRedemptionAttributes(redemptionAttributes);
        }

        return newPatient;
    }

    private void processResponse(Response<Patient> response) {
        createAccountBtn.setProgressState(false);
        if (response.isSuccess()) {
            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.REGISTRATION_COMPLETED_EVENT);

            Patient patient = response.getData();
            Intent intent = new Intent();
            intent.putExtra(Keys.PATIENT, patient);

            setResult(RESULT_OK, intent);
            finish();
        }
        else
        {
           final String userResponseError = response.getUserResponseError();

            if (!TextUtils.isEmpty(userResponseError)) {
                if (!userResponseError.equals(getString(R.string.offline_error))) {
                    try {

                        String[] split = userResponseError.split("base");
                        String toastMessage;
                        if(split.length==1)
                        {
                            toastMessage=split[0];
                        }
                        else
                        {
                            toastMessage=split[1];
                        }

                        toastMessage= toastMessage.trim();
                        AlertDialogHelper.getInstance().showMessage(this, toastMessage);
                    } catch (IllegalStateException e) {
                        L.e(TAG, e.getMessage(), e);
                    } catch (JsonSyntaxException e) {
                        L.e(TAG, e.getMessage(), e);
                    }
                } else {
                    String toastMessage=getString(R.string.register_error_occured_message);
                    AlertDialogHelper.getInstance().showMessage(this, toastMessage);
                }
            }
            else if(response.getStatus()==400){
                String toastMessage=getString(R.string.register_error_occured_message);
                AlertDialogHelper.getInstance().showMessage(this, toastMessage);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AnalyticsWrapper.flush();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void hearShake()
    {
        if(BuildConfig.DEBUG)
        {
            //Fill form with values
            firstNameEdt.setText("LARRY");
            lastNameEdt.setText("PAGE");
            passwordEdt.setText("password");


            String myFormat = "MM/dd/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            Calendar tempCal = Calendar.getInstance();
            tempCal.set(1980, Calendar.JANUARY, 01);
            birthday = new Date();
            birthday.setYear(tempCal.getTime().getYear());
            birthday.setMonth(tempCal.getTime().getMonth());
            birthday.setDate(tempCal.getTime().getDate());
            updateLabel();

            CheckBox isAgreedTermsAndConditionsCheckBox = (CheckBox) findViewById(R.id
                    .create_account_terms_and_conditions_check_box);
            isAgreedTermsAndConditionsCheckBox.setChecked(true);
        }
    }


}
