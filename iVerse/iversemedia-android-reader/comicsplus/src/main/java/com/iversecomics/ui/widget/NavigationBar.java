package com.iversecomics.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.iversecomics.archie.android.R;
import com.iversecomics.util.ui.UiUtil;

public class NavigationBar extends LinearLayout implements View.OnClickListener {

    public interface NavigationListener {
        public void onBackClick();

        public void onMenuToggle();

        public void onMyComicsClick();

        public void onAppIconCkick();
    }

    private static final int DURATION_ACTION_BAR_ANIMATION = 500;

    private NavigationListener mNavListener;

    public NavigationBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.view_navigation_bar, this, true);

            findViewById(R.id.action_back).setOnClickListener(this);
            findViewById(R.id.action_menu).setOnClickListener(this);
            findViewById(R.id.action_go_mycomics).setOnClickListener(this);
            findViewById(R.id.icon).setOnClickListener(this);
        }
    }

    public void setNavigationListener(NavigationListener listener) {
        mNavListener = listener;
    }

    public void setBackButtonVisibility(int visibility) {
        findViewById(R.id.action_back).setVisibility(visibility);
    }

    public void setMenuButtonVisibility(int visibility) {
        findViewById(R.id.action_menu).setVisibility(visibility);
    }

    public void setMyComicsVisibility(int visibility) {
        findViewById(R.id.action_go_mycomics).setVisibility(visibility);
    }

//todo decide where
//    public void setTitle(String title){
//        ((ComicBackButton)findViewById(R.id.action_back)).setText(title);
//    }
//
//    public void setTitle(int  titleId){
//        ((ComicBackButton)findViewById(R.id.action_back)).setText(titleId);
//    }

    @Override
    public void onClick(View view) {
        UiUtil.applyButtonEffect(view);
        if (mNavListener == null)
            return;

        switch (view.getId()) {
            case R.id.action_back:
                mNavListener.onBackClick();
                break;
            case R.id.action_menu:
                mNavListener.onMenuToggle();
                break;
            case R.id.action_go_mycomics:
                mNavListener.onMyComicsClick();
                break;
            case R.id.icon:
                mNavListener.onAppIconCkick();
                break;
        }
    }

    /* joliver: menu icon animation, I don't think this is necessary.
       Rather it's a result of Christian trying to mimic native iOS viewcontroller transitions

    class ActionTranslation {
        View view;
        float left;
        float right;
    }
    Vector<ActionTranslation> actionTranslations = new Vector<ActionTranslation>();
    protected void animateActionBar(boolean flagShow, boolean fromRight) {

        // action bar fading
        {
            AlphaAnimation anim;
            if (flagShow) {
                anim = new AlphaAnimation(0.0f, 1.0f);
            } else {
                anim = new AlphaAnimation(1.0f, 0.0f);
                anim.setAnimationListener(new AlphaAnimation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }
            anim.setDuration(DURATION_ACTION_BAR_ANIMATION);
            setVisibility(View.VISIBLE);
            startAnimation(anim);
        }
        // buttons animation
        for (ActionTranslation t : actionTranslations) {
            TranslateAnimation anim;
            if (flagShow) {
                if (fromRight) {
                    anim = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_SELF, t.right,
                            TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, 0);
                } else {
                    anim = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_SELF, t.left,
                            TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, 0);
                }
            } else {
                if (fromRight) {
                    anim = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, t.left, TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, 0);
                } else {
                    anim = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, t.right, TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, 0);
                }
            }
            anim.setDuration(DURATION_ACTION_BAR_ANIMATION);
            t.view.startAnimation(anim);
        }
    }

    protected void addActionTranslating(View action, float left, float right) {
        ActionTranslation t = new ActionTranslation();
        t.view = action;
        t.left = left;
        t.right = right;
        actionTranslations.add(t);
    }
    */
}
