package com.iversecomics.client.store.db;

import android.net.Uri;

import com.iversecomics.app.AppConstants;

public final class ComicStoreDB {
    public static String AUTHORITY = AppConstants.SKU_PACKAGE_NAME + ".store";
    /**
     * The content:// style URL for this database
     */
    protected static final Uri CONTENT_URI = Uri.parse("content://" + ComicStoreDB.AUTHORITY);

    public static final String DB_NAME = "store_cache.db";
    public static final int DB_VERSION = 2;
}
