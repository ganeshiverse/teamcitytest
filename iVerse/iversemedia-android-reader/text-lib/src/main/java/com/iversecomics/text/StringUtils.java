package com.iversecomics.util.text;

import java.util.Collection;
import java.util.Iterator;

public final class StringUtils {
    /**
     * Simple, yet efficient test for a String that is blank. This implementation does not use {@link String#trim()} as
     * that creates a new string object.
     *
     * @param str
     *            the string object to test
     * @return true if null or of no length or of all whitespace, false if a non whitespace character detected.
     */
    public static boolean isBlank(CharSequence str) {
        if (str == null) {
            return true;
        }
        int len = str.length();
        if (len == 0) {
            return true;
        }
        for (int i = 0; i < len; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNoBlank(CharSequence str) {
        return !StringUtils.isBlank(str);
    }

    public static String join(Collection<?> items, String delimiter) {
        if ((items == null) || items.isEmpty()) {
            return "";
        }

        final Iterator<?> iter = items.iterator();
        final StringBuilder buffer = new StringBuilder(iter.next().toString());

        while (iter.hasNext()) {
            buffer.append(delimiter).append(iter.next());
        }

        return buffer.toString();
    }
    
    public static String join(String items[], String delimiter) {
        if ((items == null) || (items.length <= 0)) {
            return "";
        }

        StringBuilder buffer = new StringBuilder();

        boolean needDelim = false;
        for(String str: items) {
            if(needDelim) {
                buffer.append(delimiter);
            }
            buffer.append(str);
            needDelim = true;
        }

        return buffer.toString();
    }

    private StringUtils() {
        /* prevent instantiation */
    }
}
