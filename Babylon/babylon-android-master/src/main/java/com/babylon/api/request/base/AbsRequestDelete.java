package com.babylon.api.request.base;

import com.babylon.api.request.BaseResponseProcessor;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public abstract class AbsRequestDelete<O> extends AbsRequest<HttpDelete, O> {

    protected List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

    public AbsRequestDelete withParams(List<BasicNameValuePair> params) {
        this.params = params;
        return this;
    }

    @Override
    protected HttpDelete acquireHttpRequest() {
        return new HttpDelete();
    }

    @Override
    protected AbsResponseProcessor<O> getResponseProcessor() {
        return new BaseResponseProcessor<O>(getOutputType());
    }
}
