package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.GetFoldersListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by hkyerra on 7/9/2015.
 */
public class GetFolderListLoader extends DataAsyncTaskLibLoader<GetFoldersListResponse> {
    private String token;
    private String mUserId;

    public GetFolderListLoader(Context context, String token, String userId) {
        super(context);
        this.token = token;
        this.mUserId = userId;
    }

    @Override
    protected GetFoldersListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMediaFolderList(token, mUserId);
    }
}
