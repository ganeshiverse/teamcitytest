package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 12:25
 *
 * @author SomeswarReddy
 */
public class LoginLoader extends DataAsyncTaskLibLoader<LoginResponse> {

    private final String mPassword;
    private final String mDeviceId;
    private final String mUserName;

    public LoginLoader(final Context context, final String deviceId, final String userName, final String password) {
        super(context);
        this.mPassword = password;
        this.mDeviceId = deviceId;
        this.mUserName = userName;
    }

    @Override
    protected LoginResponse performLoad() throws Exception {
        final LoginResponse authResponse = ServerMethods.getInstance().authenticateUser(mDeviceId, mUserName, mPassword);

        return authResponse;
    }
}
