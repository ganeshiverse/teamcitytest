package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppointmentListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/20/2015.
 */
public class GetAppointmentsListPaginationLoader extends DataAsyncTaskLibLoader<AppointmentListResponse> {

    private String token;
    private String panelId;
    private String patientId;
    private String start;
    private String max;
    private String isDictonaryResponse;
    private String filterCriteria;
    private int pageIndex;

    public GetAppointmentsListPaginationLoader(Context context, String token, String panelId, String patientId, int pageIndex,
                                 String isDictonaryResponse) {
        super(context);
        this.token = token;
        this.panelId = panelId;
        this.patientId = patientId;
        this.start = start;
        this.max = max;
        this.pageIndex = pageIndex;
        this.isDictonaryResponse = isDictonaryResponse;
        //  this.filterCriteria = filterCriteria;
    }

    @Override
    protected AppointmentListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getAppointmentsListPagination(token, panelId, patientId, pageIndex, isDictonaryResponse);
    }
}
