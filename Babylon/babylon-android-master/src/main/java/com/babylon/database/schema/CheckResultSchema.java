package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public interface CheckResultSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.SYMPTOMS.getName() + ") ON CONFLICT REPLACE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.CHECK_RESULT.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        SYMPTOMS("answersList", DBType.TEXT),
        OUTCOME("outcome", DBType.TEXT),
        BODY_PART_ID("bodyPartId", DBType.TEXT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public interface Query {
        String[] PROJECTION = {
                Table.CHECK_RESULT.getName() + "." +
                        Columns._ID.getName(),
                Columns.SYNC_WITH_SERVER.getName(),
                Columns.SYMPTOMS.getName(),
                Columns.OUTCOME.getName(),
                Columns.BODY_PART_ID.getName()
        };

        int _ID = 0;
        int SYNC_WITH_SERVER = 1;
        int SYMPTOMS = 2;
        int OUTCOME = 3;
        int BODY_PART_ID = 4;

    }
}
