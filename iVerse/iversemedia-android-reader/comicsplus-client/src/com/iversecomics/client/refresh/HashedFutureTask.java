package com.iversecomics.client.refresh;

import android.os.Handler;

import java.util.concurrent.FutureTask;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class HashedFutureTask extends FutureTask<Void> {
    private static final Logger LOG = LoggerFactory.getLogger(HashedFutureTask.class);
    public static final Handler HANDLER = new Handler();

    private final Task                task;
    private final TaskCallback        callback;


    public HashedFutureTask(Task task, TaskCallback callback) {
        super(task, null);
        this.task = task;
        this.callback = callback;
    }

    @Override
    protected void done() {
        LOG.debug("[DONE] %s", task);
        super.done();
        Throwable throwable = null;

        // Show any Throwables
        try {
            get();
        } catch (Throwable error) {
            LOG.error(error, "%s#execTask() failure", task);
            throwable = error;
        }

        final Throwable error = throwable;

        if (callback != null) {
            HANDLER.post(new Runnable() {
                @Override
                public void run() {
                    callback.onTaskCompleted(error);
                }
            });
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HashedFutureTask) {
            return this.task.equals(((HashedFutureTask) obj).task);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return task.hashCode();
    }

    @Override
    public void run() {
        LOG.debug("[RUN] %s (cancelled=%b)", task, isCancelled());
        super.run();
    }

    @Override
    public String toString() {
        return String.format("HashedFutureTask[task=%s@%X]@%X", task.getClass().getSimpleName(), task.hashCode(),
                this.hashCode());
    }
}