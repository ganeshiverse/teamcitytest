package com.babylon.model;

import com.babylon.enums.MonitorMeParam;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.DateUtils;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MonitorMeFeatures extends BaseModel {
    @SerializedName("date_time")
    private Date date;
    private List<Feature> features;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public List<MonitorMeParam> getMonitorMeparams() {
        List<MonitorMeParam> list = new LinkedList<MonitorMeParam>();

        for (int i = 0; i < features.size(); i++) {
            String id = features.get(i).getId();
            MonitorMeParam monitorMeParam = MonitorMeParam.getMonitorMeParameter(id);
            list.add(monitorMeParam);
        }

        return list;
    }

    public Feature getFeature(MonitorMeParam parameter) {
        Feature feature = new Feature(parameter.getId(), DailyFeatureMap.EMPTY_VALUE);
        if (features != null) {
            for (int i = 0; i < features.size(); i++) {
                Feature curFeature = features.get(i);
                String curFeatureId = curFeature.getId();
                if (curFeatureId.equals(parameter.getId())) {
                    feature = curFeature;
                    break;
                }
            }
        }
        return feature;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public static class Feature extends ServerModel {
        private String id;
        private String name;
        private String value;
        private String units;
        private Integer state;
        @SerializedName("normal_value")
        private String normalValue;
        private String description;
        private Map<String, Double> ranges;
        /* database field only */
        private Date date;
        private Boolean refreshed;

        public Feature() {
            /* empty constructor */
        }

        public Feature(String featureId, String value) {
            this(featureId, value, 0);
        }

        public Feature(String featureId, String value, long dateInMillis) {
            setId(featureId);
            setValue(value);
            setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
            setDate(dateInMillis == 0 ? DateUtils.findOutSavingDate(new Date())
                    : DateUtils.findOutSavingDate(dateInMillis));
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Float getFloatValue() {
            return BaseModel.getFloat(value);
        }

        public String getValue() {
            return value;
        }

        public String getUnits() {
            return units;
        }

        public void setUnits(String units) {
            this.units = units;
        }

        public Integer getState() {
            return state;
        }

        public void setState(Integer state) {
            this.state = state;
        }

        public String getNormalValue() {
            return normalValue;
        }

        public void setNormalValue(String normalValue) {
            this.normalValue = normalValue;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Map<String, Double> getRanges() {
            return ranges;
        }

        public void setRanges(Map<String, Double> ranges) {
            this.ranges = ranges;
        }

        public Boolean isRefreshed() {
            return refreshed;
        }

        public void setRefreshed(Boolean refreshed) {
            this.refreshed = refreshed;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }
}
