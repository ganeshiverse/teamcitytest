package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Redemption;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.JsonObject;

import org.apache.http.message.BasicHeader;

public class RedemptionPost extends AbsRequestPost<String, Redemption> {

    public AbsRequestPost<String, Redemption> withEntry(String promoCode, long patientId) {
        JsonObject jsonObject = new JsonObject();
        JsonObject jsonCodeObject = new JsonObject();

        jsonCodeObject.addProperty("code", promoCode);
        jsonCodeObject.addProperty("patient_id", patientId);
        jsonObject.add("redemption", jsonCodeObject);
        jsonBody = jsonObject.toString();
        return this;
    }
    
    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.REDEMPTION_POST);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<Redemption> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }
}
