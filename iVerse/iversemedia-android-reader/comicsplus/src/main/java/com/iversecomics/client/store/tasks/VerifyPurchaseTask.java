package com.iversecomics.client.store.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Preferences;
import com.iversecomics.client.Storage;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.iab.base64.Base64;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class VerifyPurchaseTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(VerifyPurchaseTask.class);
    private String deviceId;
    private String artifactType;
    private String comicName;
    private String comicBundleName;
    private String receiptData;
    private String coverImageFileName;
    private String signature;
    public static boolean isOnDemand = false;
    private Context context;

    /**
     * Use tapjoy points to authorize comic download from server.
     */
    private boolean purchaseWithTapjoy;

    public VerifyPurchaseTask(ComicStore comicStore) {
        super(comicStore);
    }

    public VerifyPurchaseTask(Context context, ComicStore comicStore, Comic comic, boolean isOnDemand) {
        super(comicStore);
        setComicBundleName(comic.getComicBundleName());
        setComicName(comic.getName());
        setDeviceId(AndroidInfo.getUniqueDeviceID(context));
        setArtifactType(AndroidInfo.getArtifactForDevice(context));
        setCoverImageFileName(comic.getImageFileName());
        setOnDemand(isOnDemand);
        this.context = context;
    }

    @Override
    public void execTask() {
        try {
            final JSONObject payload = new JSONObject();
            payload.put("deviceId", deviceId);
            payload.put("artifactType", artifactType);
            payload.put("productId", comicBundleName);

            //if the user already subscribed and pressed "save - offline" button we will add the below parameters to the PAYLOAD.
            if(isOnDemand()) {
                payload.put("installId", ComicPreferences.getInstance().getInstallId());
                payload.put("onDemand", true);
                saveOfflineQueTitles(comicBundleName);
            }

            // set receipt data for purchase with tapjoy points
            if (isPurchaseWithTapjoy()) {
                payload.put("receiptData", Base64.encode("TAPJOY".getBytes()));
            }
            // check if this is a market place purchase
            else if (!TextUtils.isEmpty(receiptData)) {

                payload.put("receiptSignature",  signature.trim());
                payload.put("receiptData", Base64.encode(receiptData.toString().trim().getBytes()));
            }

            final Storage storage = new Storage();
            final File outputFile = storage.createComicBundleDownloadReference(comicBundleName);
            final Uri destUri = Uri.fromFile(outputFile);

            // final Uri uri = Uri.parse(productUrl);
            final DownloaderManager downloader = DownloaderManager.getInstance();
            final DownloaderManager.Request request = new DownloaderManager.Request(
                    Uri.parse("http://www.iversemedia.com/" + comicBundleName));
            request.setDestinationUri(destUri);
            // request.setOverwriteDestination(true);
            request.setTitle(comicName);
            request.setVerifyPurchasePayload(payload.toString());
            downloader.enqueue(request, coverImageFileName);
            LOG.debug("Download request enqueued for comic bundle %s named %s url %s payload %s", comicBundleName, comicName, destUri, payload.toString(), coverImageFileName);
        } catch (IOException e) {
            LOG.warn(e, "SD card not available.");
        } catch (Exception e) {
            LOG.warn(e, "Unable to verify purchase for: " + comicBundleName);
        }
    }


    private void saveOfflineQueTitles(String comicBundleName){
        final SharedPreferences preferences = IverseApplication.getApplication().getSharedPreferences(Preferences.NAME, 0);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(comicBundleName, true);
        editor.commit();
    }

    public String getArtifactType() {
        return artifactType;
    }

    public String getComicBundleName() {
        return comicBundleName;
    }

    public String getComicName() {
        return comicName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getReceiptData() {
        return receiptData;
    }

    public String getCoverImageFileName() {
        return coverImageFileName;
    }

    public void setArtifactType(String artifactType) {
        this.artifactType = artifactType;
    }

    public void setComicBundleName(String comicBundleName) {
        this.comicBundleName = comicBundleName;
    }

    public void setComicName(String comicName) {
        this.comicName = comicName;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setReceiptData(String receiptData) {
        this.receiptData = receiptData;
    }

    public void setCoverImageFileName(String fileName) {
        this.coverImageFileName = fileName;
    }

    public boolean isPurchaseWithTapjoy() {
        return purchaseWithTapjoy;
    }

    public void setPurchaseWithTapjoy(boolean purchaseWithTapjoy) {
        this.purchaseWithTapjoy = purchaseWithTapjoy;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public boolean isOnDemand() {
        return isOnDemand;
    }

    public void setOnDemand(boolean isOnDemand) {
        this.isOnDemand = isOnDemand;
    }
}
