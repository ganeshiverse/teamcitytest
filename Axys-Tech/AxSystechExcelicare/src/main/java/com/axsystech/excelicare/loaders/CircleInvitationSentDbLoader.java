package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleInvitationSentDbModel;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/16/2015.
 */
public class CircleInvitationSentDbLoader extends DataAsyncTaskLibLoader<ArrayList<CircleInvitationSentDbModel>> {

    private ArrayList<CircleInvitationSentDbModel> circleInvitationSentDbModels;
    private String TAG = CircleInvitationSentDbLoader.class.getSimpleName();

    public CircleInvitationSentDbLoader(final Context context, final ArrayList<CircleInvitationSentDbModel> mediaItems) {
        super(context, true);
        this.circleInvitationSentDbModels = mediaItems;
    }

    @Override
    protected ArrayList<CircleInvitationSentDbModel> performLoad() throws Exception {
        if (circleInvitationSentDbModels == null)
            throw new SQLException("Unable to cache circle Invitation sent list in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (circleInvitationSentDbModels != null && circleInvitationSentDbModels.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<CircleInvitationSentDbModel>>() {
                    @Override
                    public ArrayList<CircleInvitationSentDbModel> call() throws Exception {
                        for (final CircleInvitationSentDbModel dbModel : circleInvitationSentDbModels) {
                            final Dao<CircleInvitationSentDbModel, Long> invitationSentListDbModelLongDao = helper.getDao(CircleInvitationSentDbModel.class);

                            invitationSentListDbModelLongDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved Invitation sent in DB...");
                        }
                        return circleInvitationSentDbModels;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return circleInvitationSentDbModels;
    }

}