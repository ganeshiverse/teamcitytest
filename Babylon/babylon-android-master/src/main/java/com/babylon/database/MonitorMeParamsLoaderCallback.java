package com.babylon.database;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.babylon.database.schema.BaseQuery;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Load monitor me params
 */
public abstract class MonitorMeParamsLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {

    public abstract Context getContext();

    /**
     * @return list of monitor me params, which need to load
     */
    public abstract List<MonitorMeParam> getMonitorMeParamList();
    public abstract void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> params);

    public static Bundle createBundle(long date) {
        Bundle b = new Bundle();

        b.putLong(Constants.EXTRA_MIN_DATE, date);

        return b;
    }

    private long getDate(Bundle b) {
        long result;
        if(b == null) {
            result = DateUtils.getStartDate(new Date()).getTime();
        } else {
             result = b.getLong(Constants.EXTRA_MIN_DATE);
        }

        return result;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        List<MonitorMeParam> params = getMonitorMeParamList();
        if (params != null && !params.isEmpty()) {

            StringBuilder query = new StringBuilder("(");
            query.append(MonitorMeParamSchema.Query.DAY_SELECTION)
            .append(") AND (")
            .append(BaseQuery.getMonitorMeParamsSelection(params))
            .append(")");

            long minDate = getDate(bundle);
            return new CursorLoader(getContext(),
                    MonitorMeParamSchema.CONTENT_URI, null,
                    query.toString(),
                    MonitorMeParamSchema.Query.getDateSelectionArguments(minDate),
                    null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Map<String, MonitorMeFeatures.Feature> featureMap =
                new HashMap<String, MonitorMeFeatures.Feature>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature();
                feature.createFromCursor(cursor);
                featureMap.put(feature.getId(), feature);
            } while (cursor.moveToNext());
        }
        onParamsLoaded(featureMap);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        /* loader reset */
    }
}
