package com.iversecomics.client;

import android.content.Context;
import android.os.Environment;

import com.iversecomics.client.my.SDStateException;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public final class Storage {
    public static class ComicFileFilter implements FileFilter {
        @Override
        public boolean accept(File path) {
            if (!path.isFile()) {
                return false;
            }

            String name = path.getName().toLowerCase();
            return name.endsWith(".cb") || name.endsWith(".cbsd");
        }
    }

    private static final String FIX_MANUALLY = "  Please correct and try again.";

    private static final String WAIT_AND_TRY_AGAIN = "  Wait a few minutes and try again.";
    private static final Logger LOG = LoggerFactory.getLogger(Storage.class);

    public static void assertAvailable() throws SDStateException {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Everything ok.
            return;
        }

        if (Environment.MEDIA_BAD_REMOVAL.equals(state)) {
            throw new SDStateException("SD-Card unavailable due to bad removal." + FIX_MANUALLY);
        }

        if (Environment.MEDIA_CHECKING.equals(state)) {
            throw new SDStateException("SD-Card unavailable due to active filesystem check." + WAIT_AND_TRY_AGAIN);
        }

        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            throw new SDStateException("SD-Card is read-only." + FIX_MANUALLY);
        }

        if (Environment.MEDIA_NOFS.equals(state)) {
            throw new SDStateException("SD-Card is present, but without a filesystem?" + FIX_MANUALLY);
        }

        if (Environment.MEDIA_REMOVED.equals(state)) {
            throw new SDStateException("SD-Card is not present." + FIX_MANUALLY);
        }

        if (Environment.MEDIA_SHARED.equals(state)) {
            throw new SDStateException(
                    "SD-Card is unavailable when being shared via USB.  Disable USB sharing and try again.");
        }

        if (Environment.MEDIA_UNMOUNTABLE.equals(state)) {
            throw new SDStateException("SD-Card is present but can not mounted."
                    + "  This is typically due to a bad SD-Card." + FIX_MANUALLY);
        }

        if (Environment.MEDIA_UNMOUNTED.equals(state)) {
            throw new SDStateException("SD-Card is present but not mounted." + FIX_MANUALLY);
        }
    }

    /**
     * Ensure that the name is safe to use as a path directory (or file) name.
     *
     * @param rawname the raw path name.
     * @return the safe to use name
     */
    private static String safePathName(String rawname) {
        char path[] = rawname.toCharArray();
        int len = path.length;
        for (int i = 0; i < len; i++) {
            switch (path[i]) {
                case '/':
                case '\\':
                case '?':
                case '%':
                case '*':
                case ':':
                case '|':
                case '"':
                case '<':
                case '>':
                    path[i] = '_';
                    break;
            }
        }
        return String.valueOf(path);
    }

    private static File extFilesDir;
    private File baseDir;
    private boolean initialized;

    public static void setExternalFilesDir(final File dir) {
        extFilesDir = dir;
    }

    public static File getExternalComicsStorageDir() {
        return new File(extFilesDir, "comics");
    }

    public Storage() throws IOException {
        baseDir = getExternalComicsStorageDir();
        initialized = initDir();
    }

    public File createComicBundleDownloadReference(String comicBundleName) {
        // Note: the extra ".dl" at the end is removed once the download is complete
        // See MyDownloadReceiver for removal location.
        String filename = comicBundleName.replace("com.iversecomics.", "") + ".cb.dl";
        return getFile(safePathName(filename));
    }

    public File getCoverFile(Context context, String comicId) {
        File coversDir = context.getDir("covers", Context.MODE_WORLD_READABLE);
        return new File(coversDir, comicId + ".png");
    }

    public File getFile(String path) {
        return new File(baseDir, path);
    }

    public boolean initDir() throws IOException {
        if (!baseDir.exists()) {
            if (!baseDir.mkdirs()) {
                initialized = false;
                // TODO: Toss Intent for bad SD Card Config
                throw new IOException("Unable to create comics directory on SDCard");
            }
        }
        File nomedia = new File(baseDir, ".nomedia");
        if (!nomedia.exists()) {
            if (!nomedia.createNewFile()) {
                // TODO: Toss Intent for bad SD Card Config
                LOG.warn("Unable to create " + nomedia);
                initialized = false;
                return false;
            }
        }
        initialized = true;
        return true;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public File[] listAssets() throws SDStateException, IOException {
        File ret[] = baseDir.listFiles(new ComicFileFilter());
        if (ret == null) {
            return new File[0]; // Empty array
        }
        return ret;
    }
}
