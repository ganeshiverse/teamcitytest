package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 29/09/15.
 */
public class SystemPreferencesObject extends BaseResponse implements Serializable {
    @SerializedName("data")
    private ArrayList<SystemPreferencesData> systemPreferencesDataAL;

    public ArrayList<SystemPreferencesData> getSystemPreferencesDataAL() {
        return systemPreferencesDataAL;
    }
}
