package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SaveUserDBLoader extends DataAsyncTaskLibLoader<User> {

    private final User mUser;

    public SaveUserDBLoader(final Context context, final User user) {
        super(context, true);
        mUser = user;
    }

    @Override
    protected User performLoad() throws Exception {
        if (mUser == null) throw new SQLException("Unable to cache user details in DB");
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final Dao<User, Long> userDao = helper.getDao(User.class);
            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<User>() {
                @Override
                public User call() throws Exception {
                    userDao.createOrUpdate(mUser);

                    return mUser;
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
