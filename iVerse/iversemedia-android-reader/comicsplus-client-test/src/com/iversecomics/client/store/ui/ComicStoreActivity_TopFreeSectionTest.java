package com.iversecomics.client.store.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.LargeTest;

import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ui.adapters.ComicListAdapter;

public class ComicStoreActivity_TopFreeSectionTest extends AbstractComicStoreActivityTestCase {
    public ComicStoreActivity_TopFreeSectionTest() {
        super();

        Intent intent = new Intent();
        intent.putExtra("testing", true);
        intent.putExtra(ComicStoreActivity.BUNDLE_ACTIVE_PAGE, TopFreeComicsFragment.TAG);
        setActivityIntent(intent);
    }

    @LargeTest
    public void testOnResumeLoadListFromServer() throws Throwable {
        final ComicStoreActivity activity = getActivity();
        assertNotNull("Activity should have launched", activity);

        waitForRefreshTasks(activity);

        Fragment fragment = activity.getActiveFragment();
        MoreAsserts.assertAssignableFrom(TopFreeComicsFragment.class, fragment);

        ComicListFragment topFreeFrag = (ComicListFragment) fragment;
        ComicListAdapter listadapter = topFreeFrag.getAdapter();
        assertNotNull("ComicListAdapter should not be null", listadapter);

        refresh(listadapter);

        OurAsserts.assertAtLeast("ComicListAdapter.count", 20, listadapter.getCount());
    }
}
