package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class CacheDeviceUsersLoader extends DataAsyncTaskLibLoader<Boolean> {

    private String TAG = CacheDeviceUsersLoader.class.getSimpleName();
    private final RegisterResponse mRegisterResponse;

    public CacheDeviceUsersLoader(final Context context, final RegisterResponse registerResponse) {
        super(context, true);
        mRegisterResponse = registerResponse;
    }

    @Override
    protected Boolean performLoad() throws Exception {
        if (mRegisterResponse == null) throw new SQLException("Unable to cache system config details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if(mRegisterResponse.getDataObject() != null) {

                if(mRegisterResponse.getDataObject().getDeviceUser() != null) {
                    // Cache Device Users
                    final ArrayList<RegisterResponse.DeviceUserDataObject> deviceUsersArrayList = mRegisterResponse.getDataObject().getDeviceUser().getDeviceUserDataObjectsAL();

                    if (deviceUsersArrayList != null && deviceUsersArrayList.size() > 0) {
                        TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Boolean>() {
                            @Override
                            public Boolean call() throws Exception {
                                for (final RegisterResponse.DeviceUserDataObject responseModel : deviceUsersArrayList) {
                                    DeviceUsersDbModel dbModel = ConverterResponseToDbModel.getDeviceUsersDbMobel(responseModel);

                                    if (dbModel != null) {
                                        final Dao<DeviceUsersDbModel, Long> deviceUserDao = helper.getDao(DeviceUsersDbModel.class);

                                        deviceUserDao.createOrUpdate(dbModel);
                                        Trace.d(TAG, "Saved Device User Details in DB...");
                                    }
                                }

                                return true;
                            }
                        });
                    }
                }
             }
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return true;
    }
}
