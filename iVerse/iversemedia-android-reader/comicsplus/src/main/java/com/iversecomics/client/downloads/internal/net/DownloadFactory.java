package com.iversecomics.client.downloads.internal.net;

public class DownloadFactory {

    public static DownloadClient getDownloadClient() {
        return new DownloadClient();
    }

    private DownloadFactory() {
    }
}
