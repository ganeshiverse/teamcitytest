package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 15/09/15.
 */
public class CountryListResponse extends BaseResponse implements Serializable {

    @SerializedName("SysLookupNames")
    private ArrayList<SystemLookupNames> systemLookupNamesAL;

    @SerializedName("UserLookups")
    private ArrayList<UserLookups> userLookupsAL;

    @SerializedName("SystemLookups")
    private ArrayList<SystemLookups> systemLookupsAL;

    public ArrayList<SystemLookupNames> getSystemLookupNamesAL() {
        return systemLookupNamesAL;
    }

    public ArrayList<UserLookups> getUserLookupsAL() {
        return userLookupsAL;
    }

    public ArrayList<SystemLookups> getSystemLookupsAL() {
        return systemLookupsAL;
    }

    public class SystemLookups implements Serializable {
        @SerializedName("Id")
        private String id;

        @SerializedName("list")
        private ArrayList<CountryDetails> countryDetailsAL;

        public String getId() {
            return id;
        }

        public ArrayList<CountryDetails> getCountryeDetailsAL() {
            return countryDetailsAL;
        }
    }

    public class CountryDetails implements Serializable {
        @SerializedName("Id")
        private String id;

        @SerializedName("Value")
        private String value;

        public String getId() {
            return id;
        }

        public String getValue() {
            return value;
        }
    }

    public class SystemLookupNames implements Serializable {}

    public class UserLookups implements Serializable {}
}
