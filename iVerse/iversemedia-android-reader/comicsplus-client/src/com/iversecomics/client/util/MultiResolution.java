package com.iversecomics.client.util;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

public class MultiResolution {
	
	Context context;

	public int widthScreen = 0;
	public int heightScreen = 0;
	public int widthApp = 0;
	public int heightApp = 0;
	public float ratioX = 0f;
	public float ratioY = 0f;
	public float ratioOrth = 0f;
	public float ratioOrthBig = 0f;
	public boolean lock;
	
	
	public Rect rectClient;
	public Rect rectOrth;
	public Rect rectOrthBig;

	static MultiResolution instance = new MultiResolution();;

	public static MultiResolution getInstance() {
		return instance;
	}
	

	public void initialize(Context context, int widthScreen, int heightScreen
			, int widthApp, int heightApp, boolean lockaspect) {
		
		this.context = context;
		
		this.widthScreen = widthScreen;
		this.heightScreen = heightScreen;
		this.widthApp = widthApp;
		this.heightApp = heightApp;
		this.lock = lockaspect;

		float fRatioX = (float) widthScreen / widthApp;
		float fRatioY = (float) heightScreen / heightApp;

		ratioX = fRatioX;
		ratioY = fRatioY;
		if (fRatioX > fRatioY) {
			if(lockaspect) {
				if (fRatioX / fRatioY >= 1.12) {
					ratioX = fRatioY;
					ratioY = fRatioY;
				}
			}
			ratioOrth = fRatioY;
			ratioOrthBig = fRatioX;
		} else {
			if(lockaspect)
			{
				if (fRatioY / fRatioX >= 1.12) {
					ratioX = fRatioX;
					ratioY = fRatioX;
				}
			}
			ratioOrth = fRatioX;
			ratioOrthBig = fRatioY;
		}
		
		// Init Client Rect
		{
			int width = (int)(widthApp * ratioX);
			int height = (int)(heightApp * ratioY);
			int left = (widthScreen - width) / 2;
			int top = (heightScreen - height) / 2;
			rectClient = new Rect(left, top, left+width, top+height);			
		}
		// Init Orth Rect
		{
			int width = (int)(widthApp * ratioOrth);
			int height = (int)(heightApp * ratioOrth);
			int left = (widthScreen - width) / 2;
			int top = (heightScreen - height) / 2;
			rectOrth = new Rect(left, top, left+width, top+height);			
		}
		// Init OrthBig Rect
		{
			int width = (int)(widthApp * ratioOrthBig);
			int height = (int)(heightApp * ratioOrthBig);
			int left = (widthScreen - width) / 2;
			int top = (heightScreen - height) / 2;
			rectOrthBig = new Rect(left, top, left+width, top+height);			
		}
	}

	public int getScreenWidth() {
		return widthScreen;
	}

	public int getScreenHeight() {
		return heightScreen;
	}
	
	public int getAppWidth() {
		return widthApp;
	}
	
	public int getAppHeight() {
		return heightApp;
	}

	public float getRatioX() {
		return ratioX;
	}
	
	public float getRatioY() {
		return ratioX;
	}
	
	public float getRatioOrth() {
		return ratioOrth;
	}
	
	public float getRatioOrthBig() {
		return ratioOrthBig;
	}

	public Rect getClientRect() {
		return rectClient;
	}
	
	public Rect toClientCoord(int x, int y, int width, int height) {
		int left = rectClient.left + (int)(x * ratioX);
		int top = rectClient.top + (int)(y * ratioY);
		int right = left + (int)(width * ratioX);
		int bottom = top + (int)(height * ratioY);
		return new Rect(left, top, right, bottom);
	}
	
	public Rect toOrthCoord(int x, int y, int width, int height) {
		int left = rectOrth.left + (int)(x * ratioOrth);
		int top = rectOrth.top + (int)(y * ratioOrth);
		int right = left + (int)(width * ratioOrth);
		int bottom = top + (int)(height * ratioOrth);
		return new Rect(left, top, right, bottom);
	}
	
	public Rect toOrthBigCoord(int x, int y, int width, int height) {
		int left = rectOrthBig.left + (int)(x * ratioOrthBig);
		int top = rectOrthBig.top + (int)(y * ratioOrthBig);
		int right = left + (int)(width * ratioOrthBig);
		int bottom = top + (int)(height * ratioOrthBig);
		return new Rect(left, top, right, bottom);
	}

	public static final int COORD_CLIENT = 0;
	public static final int COORD_ORTH = 1;
	public static final int COORD_ORTH_BIG = 2;
	public Rect toCoord(int type, int x, int y, int width, int height) {
		if (type == COORD_CLIENT) {
			return toClientCoord(x, y, width, height);
		}
		if (type == COORD_ORTH) {
			return toOrthCoord(x, y, width, height);
		}
		if (type == COORD_ORTH_BIG) {
			return toOrthBigCoord(x, y, width, height);
		}
		return null;
	}

	
	public FrameLayout.LayoutParams createLayoutParams(int x, int y, int width, int height) {
		return createLayoutParams(x, y, width, height, Gravity.LEFT | Gravity.TOP);
	}
	public FrameLayout.LayoutParams createLayoutParams(int x, int y, int width, int height, int gravity) {
		return createLayoutParams(x, y, width, height, gravity, COORD_CLIENT);
	}
	public FrameLayout.LayoutParams createLayoutParams(int x, int y, int width, int height, int gravity, int type) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = gravity;
		Rect rect = toCoord(type, x, y, width, height);
		ret.leftMargin = rect.left;
		ret.topMargin = rect.top;
		ret.width = rect.width();
		ret.height = rect.height();
		return ret;
	}


	public void setLayoutParams(View view, int x, int y, int width, int height) {
		setLayoutParams(view, x, y, width, height, Gravity.LEFT | Gravity.TOP);
	}
	public void setLayoutParams(View view, int x, int y, int width, int height, int gravity) {
		setLayoutParams(view, x, y, width, height, gravity, COORD_CLIENT);
	}
	public void setLayoutParams(View view, int x, int y, int width, int height, int gravity, int type) {
		FrameLayout.LayoutParams layout = (FrameLayout.LayoutParams)view.getLayoutParams();
		
		layout.gravity = gravity;
		Rect rect = toCoord(type, x, y, width, height);
		layout.leftMargin = rect.left;
		layout.topMargin = rect.top;
		layout.width = rect.width();
		layout.height = rect.height();
		view.setLayoutParams(layout);
	}

	
	
	public FrameLayout.LayoutParams createLayoutParamsForTopCenter(int y) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
		ret.topMargin = rectClient.top + (int)(y * ratioY);
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForTopCenter(int xMargin, int yMargin) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
		int left = rectClient.left + (int)(xMargin * ratioX);
		ret.leftMargin = left;
		ret.rightMargin = left;
		ret.topMargin = rectClient.top + (int)(yMargin * ratioY);
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForTopCenter(int yMargin, int width, int height) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
		ret.topMargin = rectClient.top + (int)(yMargin * ratioY);
		ret.width = (int)(width * ratioX);
		ret.height = (int)(height * ratioX);
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForLeftTopMargin(int xMargin, int yMargin) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.LEFT;
		ret.leftMargin = rectClient.left + (int)(xMargin * ratioX);
		ret.topMargin = rectClient.top + (int)(yMargin * ratioY);
		return ret;
	}

	public FrameLayout.LayoutParams createLayoutParamsForLeftRightTopMargin(int xMargin, int yMargin) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.LEFT;
		ret.leftMargin = rectClient.left + (int)(xMargin * ratioX);
		ret.rightMargin = ret.leftMargin;
		ret.topMargin = rectClient.top + (int)(yMargin * ratioY);
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForCenteredIcon(int cx, int cy, int width, int height) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.LEFT;
		int x = rectClient.left + (int)(cx * ratioX);
		int y = rectClient.top + (int)(cy * ratioY);
		int w = (int)(width * ratioOrth);
		int h = (int)(height * ratioOrth);
		ret.leftMargin = x - w/2;
		ret.topMargin = y - h/2;
		ret.width = w;
		ret.height = h;
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForTopOrtho(int x, int y, int width, int height) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.LEFT;
		int w = (int)(width * ratioX);
		int h = (int)(w * height / width);
		ret.leftMargin = rectClient.left + (int)(x * ratioX);
		ret.topMargin = rectClient.top + (int)(y*ratioY);
		ret.width = w;
		ret.height = h;
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForTopCenterOrtho(int y, int width, int height) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.LEFT;
		int w = (int)(width * ratioX);
		int h = (int)(w * height / width);
		ret.leftMargin = rectClient.left + (rectClient.width() - w) / 2;
		ret.topMargin = rectClient.top + (int)(y*ratioY);
		ret.width = w;
		ret.height = h;
		return ret;
	}
	
	public FrameLayout.LayoutParams createLayoutParamsForBottomOrtho(int x, int y, int width, int height) {
		FrameLayout.LayoutParams ret = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ret.gravity = Gravity.TOP | Gravity.LEFT;
		int w = (int)(width * ratioX);
		int h = (int)(w * height / width);
		ret.leftMargin = rectClient.left + (int)(x * ratioX);
		ret.topMargin = rectClient.top + (int)((y+height)*ratioY) - h;
		ret.width = w;
		ret.height = h;
		return ret;
	}
	
	public FrameLayout.LayoutParams createBackgroundLayoutParams() {
		return createLayoutParams(0, 0, widthApp, heightApp);
	}

	public FrameLayout.LayoutParams createOrthBackgroundLayoutParams() {
		return createLayoutParams(0, 0, widthApp, heightApp, Gravity.LEFT | Gravity.TOP, MultiResolution.COORD_ORTH);
	}

	public FrameLayout.LayoutParams createOrthBigBackgroundLayoutParams() {
		return createLayoutParams(0, 0, widthApp, heightApp, Gravity.LEFT | Gravity.TOP, MultiResolution.COORD_ORTH_BIG);
	}
	
	public FrameLayout.LayoutParams expandLayoutParam(FrameLayout.LayoutParams param, int width) 
	{
		param.leftMargin -= width;
		param.topMargin -= width;
		param.width += width * 2;
		param.height += width * 2;
		param.rightMargin -= width;
		param.bottomMargin -= width;
		return param;
	}
	
	
	public int tmpWidthScreen = 0;
	public int tmpHeightScreen = 0;
	public int tmpWidthApp = 0;
	public int tmpHeightApp = 0;
	public boolean tmplock = false;
	public void saveState()
	{
		tmpWidthScreen = widthScreen;
		tmpHeightScreen = heightScreen;
		tmpWidthApp = widthApp;
		tmpHeightApp = heightApp;
		tmplock = lock;
	}
	
	public void loadState()
	{
//		widthScreen = tmpWidthScreen;
//		heightScreen = tmpHeightScreen;
//		widthApp = tmpWidthApp;
//		heightApp = tmpHeightApp;
		initialize(context, widthScreen, heightScreen, widthApp, heightApp, tmplock);
	}

}
