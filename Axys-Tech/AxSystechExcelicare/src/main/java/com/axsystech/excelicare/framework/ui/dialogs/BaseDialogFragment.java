package com.axsystech.excelicare.framework.ui.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.mig35.injectorlib.utils.inject.Injector;

/**
 * Date: 20.06.14
 * Time: 12:11
 *
 * @author SomeswarReddy
 */
public abstract class BaseDialogFragment extends DialogFragment {

    private Injector mInjector;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        mInjector = Injector.init(this);

        super.onCreate(savedInstanceState);

        mInjector.applyOnFragmentCreate(this, savedInstanceState);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mInjector.applyOnFragmentViewCreated(this);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        mInjector.applyOnFragmentSaveInstanceState(this, outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mInjector.applyOnFragmentDestroyView(this);
    }
}