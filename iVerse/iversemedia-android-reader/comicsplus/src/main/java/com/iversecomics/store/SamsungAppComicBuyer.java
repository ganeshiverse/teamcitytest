package com.iversecomics.store;

public class SamsungAppComicBuyer {//implements SamsungIapHelper.OnInitIapListener {
//
//    private IverseApplication mApplication;
//    private Activity mActivity;
//    private Context mContext;
//
//    private Handler handler;
//
//    public SamsungAppComicBuyer(Activity activity) {
//        mActivity = activity;
//        mApplication = (IverseApplication)mActivity.getApplication();
//        mContext = mApplication.getApplicationContext();
//    }
//
//
//
//
//    /***********  Samsung  *************/
//    public void buyComicFromSamsungApps(String sku) {
//        // TODO: joliver: just testing here....
//    }
//
//    public boolean isSamsungIapPackageInstalled(Context context) {
//        PackageManager pm = context.getPackageManager();
//        try {
//            pm.getApplicationInfo( "com.sec.android.iap", PackageManager.GET_META_DATA );
//            return true;
//        }
//        catch(PackageManager.NameNotFoundException e ) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    private static final int IAP_SIGNATURE_HASHCODE = 0x7a7eaf4b;
//    public boolean isValidIAPPackage( Context context) {
//        boolean result = true;
//        try
//        {
//            android.content.pm.Signature[] sigs = context.getPackageManager().getPackageInfo(
//                    "com.sec.android.iap",
//                    PackageManager.GET_SIGNATURES ).signatures;
//            if( sigs[0].hashCode() != IAP_SIGNATURE_HASHCODE ) {
//                result = false; }
//        }
//        catch( Exception e ) {
//            e.printStackTrace(); result = false;
//        }
//        return result; }
//
//
//    public void samsungInAppPurchase(Context context) {
//        // If the installed IAP package is not valid:
//        if( false == isValidIAPPackage( context )) {
//            new AlertDialog.Builder( context )
//                    .setTitle(R.string.in_app_purchase).setMessage(R.string.invalid_iap_package).setPositiveButton( android.R.string.ok,
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick( DialogInterface dialog, int which ) {
//                            dialog.dismiss();
//                        }
//                    } ).show();
//        }
//    }
//
//
//
//
//    /**********   Samsung   ************/
//    /**
//     * IAP Service
//     * initIAP()
//     *
//     * bind IAPService. If IAPService properly bound,
//     * initIAP() method is called to initialize IAPService.
//     */
//    public void bindIapService() {
//
//        // 1. bind to IAPService
//        mSamsungIapHelper.bindIapService(
//                new SamsungIapHelper.OnIapBindListener()
//                {
//                    @Override
//                    public void onBindIapFinished( int result )
//                    {
//                        // 1) If successfully bound IAPService
//                        // ============================================================
//                        if ( result == SamsungIapHelper.IAP_RESPONSE_RESULT_OK )
//                        {
//                            // IAPService
//                            // initialize IAPService.
//                            // PurchaseMethodListActivity is called after IAPService
//                            // is initialized
//                            // --------------------------------------------------------
//                            mSamsungIapHelper.safeInitIap( mActivity );
//                            // --------------------------------------------------------
//                        }
//                        // 2) If IAPService is not bound correctly
//                        else
//                        {
//                            // dismiss ProgressDialog
//                            mSamsungIapHelper.dismissProgressDialog();
//
//                            // show alert dialog for bind failure
//                            mSamsungIapHelper.showIapDialog(
//                                    mActivity,
//                                    mActivity.getString(R.string.in_app_purchase),
//                                    mActivity.getString(R.string.msg_iap_service_bind_failed),
//                                    true,
//                                    null );
//                        }
//
//                    }
//                });
//
//    }
//
//
//    SamsungIapHelper mSamsungIapHelper;
//    private static final String  TAG = SamsungAppComicBuyer.class.getSimpleName();
//
//    /* TODO: joliver: where should we put this... MainActivity?
//    // Samsung Account
//    // treat result of SamsungAccount Authentication and IAPService     @Override
//    protected void onActivityResult
//    (
//            int     _requestCode,
//            int     _resultCode,
//            Intent  _intent
//    )
//    {
//        switch( _requestCode )
//        {
//            // 1. IAP treat result of IAPService
//            // ================================================================
//            case SamsungIapHelper.REQUEST_CODE_IS_IAP_PAYMENT:
//            {
//                if( null == _intent )
//                {
//                    break;
//                }
//
//                Bundle extras         = _intent.getExtras();
//
//                String itemId         = "";
//                String thirdPartyName = "";
//
//                // payment success   : 0
//                // payment cancelled : 1
//                // ============================================================
//                int statusCode        = 1;
//                // ============================================================
//
//                String errorString    = "";
//                PurchaseVO purchaseVO = null;
//
//                // 1) IAP 에서 전달된 Bundle 정보가 존재할 경우
//                //    If there is bundle passed from IAP
//                // ------------------------------------------------------------
//                if( null != extras )
//                {
//                    thirdPartyName = extras.getString(
//                            SamsungIapHelper.KEY_NAME_THIRD_PARTY_NAME );
//
//                    statusCode = extras.getInt(
//                            SamsungIapHelper.KEY_NAME_STATUS_CODE );
//
//                    errorString = extras.getString(
//                            SamsungIapHelper.KEY_NAME_ERROR_STRING );
//
//                    itemId = extras.getString(
//                            SamsungIapHelper.KEY_NAME_ITEM_ID );
//
//                    // 로그 출력 : 릴리즈 전에 삭제하세요.
//                    // print log : Please remove before release
//                    // --------------------------------------------------------
//                    Log.i(TAG, "3rdParty Name : " + thirdPartyName + "\n" +
//                            "ItemId        : " + itemId + "\n" +
//                            "StatusCode    : " + statusCode + "\n" +
//                            "errorString   : " + errorString);
//                    // --------------------------------------------------------
//                }
//                // ------------------------------------------------------------
//                // 2) IAP 에서 전달된 Bundle 정보가 존재하지 않는 경우
//                //    If there is no bundle passed from IAP
//                // ------------------------------------------------------------
//                else
//                {
//                    mSamsungIapHelper.showIapDialog(
//                            mActivity,
//                            mActivity.getString( R.string.dlg_title_payment_error ),
//                            mActivity.getString( R.string.msg_payment_was_not_processed_successfully ),
//                            true,
//                            null );
//                }
//                // ------------------------------------------------------------
//
//                // 3) 결제가 성공했을 경우
//                //    If payment was not cancelled
//                // ------------------------------------------------------------
//                if( Activity.RESULT_OK == _resultCode )
//                {
//                    // a. IAP 에서 넘어온 결제 결과가 성공인 경우 verifyurl 과
//                    //    purchaseId 값으로 서버에 해당 결제가 유효한 지 확인한다.
//                    //    if Payment succeed
//                    // --------------------------------------------------------
//                    if( statusCode == SamsungIapHelper.IAP_ERROR_NONE )
//                    {
//                        // 정상적으로 결제가 되었으므로 PurchaseVO를 생성한다.
//                        // make PurcahseVO
//                        // ----------------------------------------------------
//                        purchaseVO = new PurchaseVO( extras.getString(
//                                SamsungIapHelper.KEY_NAME_RESULT_OBJECT ) );
//                        // ----------------------------------------------------
//
//                        // 결제 유효성을 확인한다.
//                        // verify payment result
//                        // ----------------------------------------------------
//                        mSamsungIapHelper.verifyPurchaseResult(
//                                mActivity,
//                                purchaseVO );
//                        // ----------------------------------------------------
//                    }
//                    // --------------------------------------------------------
//                    // b. IAP 에서 넘어온 결제 결과가 실패인 경우 에러메시지를 출력
//                    //    Payment failed
//                    // --------------------------------------------------------
//                    else
//                    {
//                        mSamsungIapHelper.showIapDialog(
//                                mActivity,
//                                mActivity.getString( R.string.dlg_title_payment_error ),
//                                errorString,
//                                true,
//                                null );
//                    }
//                    // --------------------------------------------------------
//                }
//                // ------------------------------------------------------------
//                // 4) If payment was cancelled
//                // ------------------------------------------------------------
//                else if( Activity.RESULT_CANCELED == _resultCode )
//                {
//                    mSamsungIapHelper.showIapDialog(
//                            mActivity,
//                            mActivity.getString( R.string.dlg_title_payment_cancelled ),
//                            mActivity.getString( R.string.dlg_msg_payment_cancelled ),
//                            true,
//                            null );
//                }
//                // ------------------------------------------------------------
//
//                break;
//            }
//            // ================================================================
//
//            // 2. treat result of SamsungAccount authentication
//            // ================================================================
//            case SamsungIapHelper.REQUEST_CODE_IS_ACCOUNT_CERTIFICATION :
//            {
//                // 2) 삼성 계정 인증결과 성공인 경우
//                //    If SamsungAccount authentication is succeed
//                // ------------------------------------------------------------
//                if( Activity.RESULT_OK == _resultCode )
//                {
//                    // IAP Service 바인드 및 초기화 진행
//                    // start binding and initialization for IAPService
//                    // --------------------------------------------------------
//                    bindIapService();
//                    // --------------------------------------------------------
//                }
//                // ------------------------------------------------------------
//                // 3) 삼성 계정 인증을 취소했을 경우
//                //    If SamsungAccount authentication is cancelled
//                // ------------------------------------------------------------
//                else if( Activity.RESULT_CANCELED == _resultCode )
//                {
//                    // 프로그래스를 dismiss 처리합니다.
//                    // dismiss ProgressDialog for SamsungAccount Authentication
//                    // --------------------------------------------------------
//                    mSamsungIapHelper.dismissProgressDialog();
//                    // --------------------------------------------------------
//
//                    mSamsungIapHelper.showIapDialog(
//                            mActivity,
//                            mActivity.getString( R.string.dlg_title_samsungaccount_authentication ),
//                            mActivity.getString( R.string.msg_authentication_has_been_cancelled ),
//                            true,
//                            null );
//                }
//                // ------------------------------------------------------------
//
//                break;
//            }
//            // ================================================================
//        }
//    }
//
//    */
//
//    @Override
//    public void onSucceedInitIap() {
//
//        // TODO: joliver, test an in-app purchase
//        String mItemGroupId = "";
//        String mItemId = "";
//
//        // 프로그래스 다이얼로그를 dismiss 한다
//        // dismiss ProgressDialog
//        // ====================================================================
//        mSamsungIapHelper.dismissProgressDialog();
//        // ====================================================================
//
//        // 초기화가 정상적으로 완료되었기 때문에 IAP 결제방법 목록을 호출한다.
//        // call PurchaseMethodListActivity of IAP
//        // ====================================================================
//        mSamsungIapHelper.startPurchase(
//                mActivity,
//                SamsungIapHelper.REQUEST_CODE_IS_IAP_PAYMENT,
//                mItemGroupId,
//                mItemId );
//        // ====================================================================
//    }

}
