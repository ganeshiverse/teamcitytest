package com.babylon.fragment.payments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.babylon.R;
import com.babylon.adapter.SelectSubscriptionArrayAdapter;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Patient;
import com.babylon.model.payments.Plan;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.UIUtils;
import com.babylon.view.PromoCodeView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class SelectSubscriptionFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public static final String TAG = SelectSubscriptionFragment.class.getSimpleName();
    public static final String PATIENT_BUNDLE = "PATIENT";
    public static final String PLAN_BUNDLE = "PLAN";

    private ListView subscriptionsListView;
    private EditText promoCodeEditText;
    private SelectSubscriptionArrayAdapter selectSubscriptionAdapter;
    private OnSubscriptionSelectedListener selectionCallback;
    private DataProtocol dataProtocol;
    private Patient patient;


    public static SelectSubscriptionFragment newInstance(Patient patient) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(PATIENT_BUNDLE, patient);
        SelectSubscriptionFragment fragment = new SelectSubscriptionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_subscriptions_select, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        patient = (Patient)bundle.getSerializable(PATIENT_BUNDLE);

        setTitle(R.string.subscription_title);

        subscriptionsListView = (ListView) view.findViewById(R.id.subscriptions_list_view);

        subscriptionsListView.setOnItemClickListener(this);

        final View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.list_header_select_subscription,
                null);
        subscriptionsListView.addHeaderView(headerView, null, false);

        showFooter();


    }

    private void showFooter() {
        final View footerView = new PromoCodeView(getActivity());
        promoCodeEditText = (EditText) footerView.findViewById(R.id
                .select_subscription_promo_code_edit_text);

        promoCodeEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        footerView.findViewById(R.id.select_subscription_promo_code_btn).setOnClickListener(onPromoCodeClickListener);
        subscriptionsListView.addFooterView(footerView, null, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            selectionCallback = (OnSubscriptionSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SubscriptionSelectedInterface");
        }
        try {
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Plan[] plans = dataProtocol.getPlans();

        if(plans != null) {
            selectSubscriptionAdapter = new SelectSubscriptionArrayAdapter(getActivity(), plans, patient);
            subscriptionsListView.setAdapter(selectSubscriptionAdapter);
        }

        if (dataProtocol.shouldShowCancelButton())
        {
            setLeftButton(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataProtocol.cancelAppointment();
                }
            }, R.string.select_subscription_cancel_appointment_button);
        }



    }

    @Override
    public void onDetach() {
        super.onDetach();
        selectionCallback = null;
        dataProtocol = null;
    }

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    @Override
    protected void onTopBarButtonPressed() {
        UIUtils.hideSoftKeyboard(getActivity(), promoCodeEditText);
        super.onTopBarButtonPressed();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Plan selectedSubscription = selectSubscriptionAdapter.getSubscriptionAtPosition(position -
                subscriptionsListView.getHeaderViewsCount());
        if (selectionCallback != null) {
            UIUtils.hideSoftKeyboard(getActivity(), promoCodeEditText);
            selectionCallback.onSubscriptionSelected(selectedSubscription);
        }
    }

    private final View.OnClickListener onPromoCodeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final String promoCode = promoCodeEditText.getText().toString();
            JSONObject props = new JSONObject();
            try {
                props.put("Promo Code",promoCode);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Bundle fb_parameters = new Bundle();
            fb_parameters.putString("Promo Code", promoCode);

            MixPanelHelper helper = new MixPanelHelper(getActivity(),props,fb_parameters);
            helper.sendEvent(MixPanelEventConstants.PROMOTION);

            if(!TextUtils.isEmpty(promoCode))
            {
                UIUtils.hideSoftKeyboard(getActivity(), promoCodeEditText);
                selectionCallback.onPromoCodeEntered(promoCode);
            }
            else
            {
                Toast.makeText(getActivity().getApplicationContext(),getResources().getString(R.string
                            .promo_empty),Toast.LENGTH_SHORT).show();
            }
        }
    };


    public interface DataProtocol {
        public Plan[] getPlans();
        public Boolean shouldShowCancelButton();
        public void cancelAppointment();
    }

    public interface OnSubscriptionSelectedListener {
        void onSubscriptionSelected(Plan selectedSubscription);

        void onPromoCodeEntered(String promoCode);
    }

}
