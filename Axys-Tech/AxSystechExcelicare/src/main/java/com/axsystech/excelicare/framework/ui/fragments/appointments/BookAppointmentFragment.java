package com.axsystech.excelicare.framework.ui.fragments.appointments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.data.responses.TimeSlotsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.DatePickerFragment;
import com.axsystech.excelicare.loaders.GetTimeSlotsLoader;
import com.axsystech.excelicare.loaders.RemoveCircleLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.ExpandableHeightGridView;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class BookAppointmentFragment extends BaseFragment implements View.OnClickListener {

    private static final String GET_TIMESLOTS_LOADER = "com.axsystech.excelicare.framework.ui.fragments.appointments.BookAppointmentFragment.GET_TIMESLOTS_LOADER";

    private String TAG = ClinicianDetailsFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private BookAppointmentFragment mFragment;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mClinicName;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectView(R.id.isFavoriteCheckBox)
    private CheckBox isFavoriteCheckBox;

    @InjectView(R.id.appointmentUserImageView)
    private ImageView appointmentUserImageView;

    @InjectView(R.id.clinicianNameTextView)
    private TextView clinicianNameTextView;

    @InjectView(R.id.qualificationTextView)
    private TextView qualificationTextView;

    @InjectView(R.id.experienceTextView)
    private TextView experienceTextView;

    @InjectView(R.id.specializationTextView)
    private TextView specializationTextView;

    @InjectView(R.id.linearLayoutRootLayout)
    private LinearLayout linearLayoutRootLayout;

    @InjectView(R.id.registerNumberTextView)
    private TextView registerNumberTextView;

    @InjectView(R.id.doctorRatingBar)
    private RatingBar doctorRatingBar;

    @InjectView(R.id.likeTextView)
    private TextView likeTextView;
    //*****************************
    @InjectView(R.id.clinicDetailsButton)
    private Button clinicDetailsButton;

    @InjectView(R.id.timeSlotsButton)
    private Button timeSlotsButton;

    @InjectView(R.id.attachmentsButton)
    private Button attachmentsButton;
    //******************************
    @InjectView(R.id.clinicDetailsListView)
    private ListView clinicDetailsListView;
    //******************************
    @InjectView(R.id.timeSlotsLayout)
    private ScrollView timeSlotsLayout;

    @InjectView(R.id.consultationModeInPersonLayout)
    private LinearLayout consultationModeInPersonLayout;

    @InjectView(R.id.consultationModeInPersonCheckBox)
    private CheckBox consultationModeInPersonCheckBox;

    @InjectView(R.id.consultationModeCallLayout)
    private LinearLayout consultationModeCallLayout;

    @InjectView(R.id.consultationModeCallCheckBox)
    private CheckBox consultationModeCallCheckBox;

    @InjectView(R.id.consultationModeVideoLayout)
    private LinearLayout consultationModeVideoLayout;

    @InjectView(R.id.consultationModeVideoCheckBox)
    private CheckBox consultationModeVideoCheckBox;

    @InjectView(R.id.noteTitleTextView)
    private TextView noteTitleTextView;

    @InjectView(R.id.noteDespTextView)
    private TextView noteDespTextView;

    @InjectView(R.id.timeSlotDateTextView)
    private TextView timeSlotDateTextView;

    @InjectView(R.id.textClinicName)
    private TextView textClinicName;

    @InjectView(R.id.buttonNextClinicdetails)
    private Button mButtonNextClinicdetails;

    @InjectView(R.id.buttonNextTimeSlots)
    private Button mButtonNextTimeSlots;

    @InjectView(R.id.buttonNextAttachments)
    private Button mButtonNextAttachments;

    @InjectView(R.id.bv_timeslotleftarrow)
    private Button mBv_timeslotleftarrow;

    @InjectView(R.id.bv_timeslotrightarrow)
    private Button mBv_timeslotrightarrow;

    boolean bookAppoinments = true;

    @InjectView(R.id.timeSlotsExpandableHeightGridView)
    private ExpandableHeightGridView timeSlotsExpandableHeightGridView;

    private Calendar mSelectedTimeSlotCalendar = Calendar.getInstance();

    //******************************
    @InjectView(R.id.attachmentsLayout)
    private LinearLayout attachmentsLayout;

    @InjectView(R.id.clinicNameTimeSlotsTextView)
    private TextView clinicNameTimeSlotsTextView;

    @InjectView(R.id.consultationModeTitleTextView)
    private TextView consultationModeTitleTextView;

    @InjectView(R.id.selectDateTitleTextView)
    private TextView selectDateTitleTextView;

    @InjectView(R.id.selectTimeTitleTextView)
    private TextView selectTimeTitleTextView;

    private ClinicianDetailsResponse.ClinicianDetailsData mSelectedClinicianDetailsData;
    private int mSelectedClinicPosition;
    private SearchDoctorsResponse.DoctorDetails mSelectedClinicianObject;

    private ClinicianDetailsResponse.ClinicDetails mSelectedClinicDetails;
    private ClinicDetailsAdapter mClinicDetailsAdapter;

    private TimeSlotsGridViewAdapter mTimeSlotsGridViewAdapter;

    private TimeSlotsResponse.TimeSlotDetails mSelectedTimeSlotObject;
    private String mSelectedConsultationMode = "In Person";

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), "Choose Appointment"));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.book_appointment_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        readIntentArgs();
        preLoadData();
        // get timeslots for current date
        mSelectedTimeSlotCalendar = Calendar.getInstance();
        getTimeSlots(mSelectedTimeSlotCalendar);
        addEventListeners();

        loadHeaderData();
        clinicDetailsButton.setSelected(true);
        timeSlotsButton.setSelected(false);
        attachmentsButton.setSelected(false);
        loadTabsData();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_CLINIC_TITLE)) {
                mClinicName = bundle.getString(AxSysConstants.EXTRA_CLINIC_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_CLINICIAN_DETAILS)) {
                mSelectedClinicianDetailsData = (ClinicianDetailsResponse.ClinicianDetailsData) bundle.getSerializable(AxSysConstants.EXTRA_CLINICIAN_DETAILS);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION)) {
                mSelectedClinicPosition = bundle.getInt(AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION, 0);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT)) {
                mSelectedClinicianObject = (SearchDoctorsResponse.DoctorDetails) bundle.getSerializable(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT);
            }
        }
    }

    private void preLoadData() {

        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Book Appointments")) {
                    if (m.getValue() == 1) {
                        bookAppoinments = true;
                    } else if (m.getValue() == 0) {
                        bookAppoinments = false;
                    }
                }
            }
        }


        if (mSelectedClinicianDetailsData != null && mSelectedClinicianDetailsData.getClinicDetailsArrayList() != null &&
                mSelectedClinicianDetailsData.getClinicDetailsArrayList().size() > mSelectedClinicPosition) {
            mSelectedClinicDetails = mSelectedClinicianDetailsData.getClinicDetailsArrayList().get(mSelectedClinicPosition);
        }

        mTimeSlotsGridViewAdapter = new TimeSlotsGridViewAdapter(null);
        timeSlotsExpandableHeightGridView.setAdapter(mTimeSlotsGridViewAdapter);
        timeSlotsExpandableHeightGridView.setExpanded(true);
    }

    private void getTimeSlots(Calendar selectedCalendar) {
        mSelectedTimeSlotCalendar = selectedCalendar;

        // 2015-08-22
        SimpleDateFormat sdf = new SimpleDateFormat(AxSysConstants.DF_TIMESLOT_REQUEST_DATE_FORMAT);

        // Update date textview with the selected slot date
        SimpleDateFormat sdf1 = new SimpleDateFormat(AxSysConstants.DF_TIMESLOT_DISPLAY_DATE_FORMAT);
        timeSlotDateTextView.setText(sdf1.format(selectedCalendar.getTime()));

        if (mSelectedClinicDetails != null && mSelectedClinicianDetailsData != null) {
            // get time slots for current date
            GetTimeSlotsLoader getTimeSlotsLoader = new GetTimeSlotsLoader(mActivity, mSelectedClinicDetails.getID(), mSelectedClinicianDetailsData.getClinicianId(),
                    sdf.format(selectedCalendar.getTime()), mActiveUser.getToken());
            if (getView() != null && isAdded()) {
                displayProgressLoader(false);
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_TIMESLOTS_LOADER), getTimeSlotsLoader);
            }
        }
    }

    private void addEventListeners() {

        isFavoriteCheckBox.setOnClickListener(mFragment);
        clinicDetailsButton.setOnClickListener(mFragment);
        timeSlotsButton.setOnClickListener(mFragment);
        timeSlotDateTextView.setOnClickListener(mFragment);
        attachmentsButton.setOnClickListener(mFragment);
        mButtonNextClinicdetails.setOnClickListener(mFragment);
        mButtonNextTimeSlots.setOnClickListener(mFragment);
        mBv_timeslotleftarrow.setOnClickListener(mFragment);
        mBv_timeslotrightarrow.setOnClickListener(mFragment);

        consultationModeInPersonLayout.setOnClickListener(mFragment);
        consultationModeCallLayout.setOnClickListener(mFragment);
        consultationModeVideoLayout.setOnClickListener(mFragment);
        mButtonNextAttachments.setOnClickListener(mFragment);

        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

        noteTitleTextView.setTypeface(bold);
        noteDespTextView.setTypeface(bold);
    }

    private void loadHeaderData() {
        if (mSelectedClinicianDetailsData != null) {
            // Update Clinician header details

            if (!TextUtils.isEmpty(mSelectedClinicianDetailsData.getIsFavorite())) {
                if (mSelectedClinicianDetailsData.getIsFavorite().trim().equalsIgnoreCase("True")) {
                    isFavoriteCheckBox.setChecked(true);
                } else {
                    isFavoriteCheckBox.setChecked(false);
                }
            } else if (mSelectedClinicianObject != null && !TextUtils.isEmpty(mSelectedClinicianObject.getIsFavorite())) {
                if (mSelectedClinicianObject.getIsFavorite().trim().equalsIgnoreCase("True")) {
                    isFavoriteCheckBox.setChecked(true);
                } else {
                    isFavoriteCheckBox.setChecked(false);
                }
            }

            /*isFavoriteCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectedClinicianObject != null) {
                        SaveFavoriteProviderTask saveFavoriteProviderTask = new SaveFavoriteProviderTask(mSelectedClinicianObject.getClinicianId(),
                                isFavoriteCheckBox.isChecked(), mActiveUser.getToken(), new SaveFavoriteProviderTask.SaveFavoriteProviderListener() {
                            @Override
                            public void onFavoriteProviderSaved() {
                                mSelectedClinicianObject.setIsFavorite(isFavoriteCheckBox.isChecked() ? "True" : "False");
                            }

                            @Override
                            public void onFavoriteProviderFailed() {
                                showToast(R.string.error_save_favorite);
                            }
                        });

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            saveFavoriteProviderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            saveFavoriteProviderTask.execute();
                        }
                    }
                }
            });*/

            if (!TextUtils.isEmpty(mSelectedClinicianDetailsData.getPhoto())) {
                String imagePath = mSelectedClinicianDetailsData.getPhoto();
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                /*Picasso.with(mActivity)
                        .load(imagePath)
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .into(appointmentUserImageView);*/

                Picasso.with(getActivity()).load(imagePath).transform(new CircleTransform()).into(appointmentUserImageView);

            }

            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface boldItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            clinicianNameTextView.setText(!TextUtils.isEmpty(mSelectedClinicianDetailsData.getClinicianName()) ? /*"Dr " +*/ mSelectedClinicianDetailsData.getClinicianName() : "");
            clinicianNameTextView.setTypeface(bold);
            qualificationTextView.setText(!TextUtils.isEmpty(mSelectedClinicianDetailsData.getQualification()) ? mSelectedClinicianDetailsData.getQualification() : "");
            qualificationTextView.setTypeface(regular);
            experienceTextView.setText(!TextUtils.isEmpty(mSelectedClinicianDetailsData.getExperience()) ? mSelectedClinicianDetailsData.getExperience() : "");
            experienceTextView.setTypeface(bold);
            specializationTextView.setText(!TextUtils.isEmpty(mSelectedClinicianDetailsData.getSpeciality()) ? mSelectedClinicianDetailsData.getSpeciality() : "");
            specializationTextView.setTypeface(regular);
            textClinicName.setText(mClinicName);

            clinicNameTimeSlotsTextView.setText(mClinicName);
            clinicNameTimeSlotsTextView.setTypeface(bold);
            consultationModeTitleTextView.setTypeface(bold);
            selectDateTitleTextView.setTypeface(bold);
            selectTimeTitleTextView.setTypeface(bold);

            registerNumberTextView.setText("");
            if (!TextUtils.isEmpty(mSelectedClinicianDetailsData.getRating())) {
                float rating = Float.parseFloat(mSelectedClinicianDetailsData.getRating());
                doctorRatingBar.setRating(rating);
            } else {
                doctorRatingBar.setRating(0.0f);
            }

            if (!TextUtils.isEmpty(mSelectedClinicianDetailsData.getLikes())) {
                likeTextView.setText(mSelectedClinicianDetailsData.getLikes());
                likeTextView.setTypeface(bold);
            } else if (mSelectedClinicianObject != null && !TextUtils.isEmpty(mSelectedClinicianObject.getLikes())) {
                likeTextView.setText(mSelectedClinicianObject.getLikes());
                likeTextView.setTypeface(bold);
            }
        }
    }

    private void loadTabsData() {
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        if (clinicDetailsButton.isSelected()) {
            clinicDetailsButton.setTypeface(bold);
            timeSlotsButton.setTypeface(regular);
            timeSlotsButton.setTextColor(getResources().getColor(R.color.circles_text_color));
            attachmentsButton.setTextColor(getResources().getColor(R.color.circles_text_color));
            clinicDetailsButton.setTextColor(getResources().getColor(R.color.sidemenu_selectaccount_color));
            attachmentsButton.setTypeface(regular);
            clinicDetailsListView.setVisibility(View.VISIBLE);
            timeSlotsLayout.setVisibility(View.GONE);
            attachmentsLayout.setVisibility(View.GONE);

            mButtonNextClinicdetails.setVisibility(View.VISIBLE);
            mButtonNextTimeSlots.setVisibility(View.GONE);
            mButtonNextAttachments.setVisibility(View.GONE);

            // Display only selected clinic details from list
            ArrayList<ClinicianDetailsResponse.ClinicDetails> selectedClinic = new ArrayList<ClinicianDetailsResponse.ClinicDetails>();
            selectedClinic.add(mSelectedClinicDetails);

            if (mClinicDetailsAdapter == null) {
                mClinicDetailsAdapter = new ClinicDetailsAdapter(selectedClinic);
            } else {
                mClinicDetailsAdapter.updateData(selectedClinic);
            }
            clinicDetailsListView.setAdapter(mClinicDetailsAdapter);


        } else if (timeSlotsButton.isSelected()) {
            clinicDetailsButton.setTypeface(regular);
            clinicDetailsButton.setTextColor(getResources().getColor(R.color.circles_text_color));
            attachmentsButton.setTextColor(getResources().getColor(R.color.circles_text_color));
            timeSlotsButton.setTextColor(getResources().getColor(R.color.sidemenu_selectaccount_color));
            timeSlotsButton.setTypeface(bold);
            attachmentsButton.setTypeface(regular);
            clinicDetailsListView.setVisibility(View.GONE);
            timeSlotsLayout.setVisibility(View.VISIBLE);
            attachmentsLayout.setVisibility(View.GONE);

            mButtonNextClinicdetails.setVisibility(View.GONE);
            mButtonNextTimeSlots.setVisibility(View.GONE);
            mButtonNextAttachments.setVisibility(View.VISIBLE);
        } else if (attachmentsButton.isSelected()) {
            clinicDetailsButton.setTypeface(regular);
            timeSlotsButton.setTypeface(regular);
            attachmentsButton.setTypeface(bold);
            clinicDetailsListView.setVisibility(View.GONE);
            timeSlotsLayout.setVisibility(View.GONE);
            attachmentsLayout.setVisibility(View.VISIBLE);

            mButtonNextClinicdetails.setVisibility(View.GONE);
            mButtonNextTimeSlots.setVisibility(View.GONE);
            mButtonNextAttachments.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getString(R.string.text_alert));
                alert.setMessage(getString(R.string.confirmation_to_cancel_selected_info));
                alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mActivity.onBackPressed();
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();
                break;


            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_TIMESLOTS_LOADER)) {
            showToast(R.string.error_timeslot_details);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_TIMESLOTS_LOADER)) {
            if (result != null && result instanceof TimeSlotsResponse) {
                TimeSlotsResponse timeSlotsResponse = (TimeSlotsResponse) result;

                if (timeSlotsResponse.getTimeSlotsData() != null &&
                        timeSlotsResponse.getTimeSlotsData().getTimeSlotDetailsArrayList() != null &&
                        timeSlotsResponse.getTimeSlotsData().getTimeSlotDetailsArrayList().size() > 0) {

                    if (mTimeSlotsGridViewAdapter == null) {
                        mTimeSlotsGridViewAdapter = new TimeSlotsGridViewAdapter(timeSlotsResponse.getTimeSlotsData().getTimeSlotDetailsArrayList());
                    } else {
                        mTimeSlotsGridViewAdapter.updateData(timeSlotsResponse.getTimeSlotsData().getTimeSlotDetailsArrayList());
                    }
                    timeSlotsExpandableHeightGridView.setAdapter(mTimeSlotsGridViewAdapter);
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.isFavoriteCheckBox:
                break;

            case R.id.clinicDetailsButton:
                clinicDetailsButton.setSelected(true);
                timeSlotsButton.setSelected(false);
                attachmentsButton.setSelected(false);

                loadTabsData();
                break;

            case R.id.timeSlotsButton:
                clinicDetailsButton.setSelected(false);
                timeSlotsButton.setSelected(true);
                attachmentsButton.setSelected(false);

                loadTabsData();
                break;

            case R.id.attachmentsButton:
                clinicDetailsButton.setSelected(false);
                timeSlotsButton.setSelected(false);
                attachmentsButton.setSelected(true);

                loadTabsData();
                break;

            case R.id.timeSlotDateTextView:
                showDatePicker();
                break;

            case R.id.consultationModeInPersonLayout:
                consultationModeInPersonCheckBox.setChecked(true);
                consultationModeCallCheckBox.setChecked(false);
                consultationModeVideoCheckBox.setChecked(false);

                mSelectedConsultationMode = "In Person";
                break;

            case R.id.consultationModeCallLayout:
                consultationModeInPersonCheckBox.setChecked(false);
                consultationModeCallCheckBox.setChecked(true);
                consultationModeVideoCheckBox.setChecked(false);

                mSelectedConsultationMode = "Call";
                break;

            case R.id.consultationModeVideoLayout:
                consultationModeInPersonCheckBox.setChecked(false);
                consultationModeCallCheckBox.setChecked(false);
                consultationModeVideoCheckBox.setChecked(true);

                mSelectedConsultationMode = "Video";
                break;

            case R.id.buttonNextClinicdetails:
               /* clinicDetailsButton.setSelected(false);
                timeSlotsButton.setSelected(true);
                attachmentsButton.setSelected(false);

                loadTabsData();*/

                showToast(R.string.select_timeslot);
                break;

            case R.id.buttonNextTimeSlots:
                clinicDetailsButton.setSelected(false);
                timeSlotsButton.setSelected(false);
                attachmentsButton.setSelected(true);

                loadTabsData();

                break;

            case R.id.buttonNextAttachments:
                if (mSelectedTimeSlotObject != null) {
                    // navigate to Review Appointment Details Fragment
                    FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                    FragmentTransaction ft1 = fragmentManager.beginTransaction();

                    ReviewAppointmentDetailsFragment reviewAppointmentDetailsFragment = new ReviewAppointmentDetailsFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                    bundle1.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_review_appointment_details));
                    bundle1.putString(AxSysConstants.EXTRA_APPOINTMENT_REVIEW_TYPE, AxSysConstants.BOOK_APPOINTMENT_REVIEW_TYPE_BOOK);
                    bundle1.putSerializable(AxSysConstants.EXTRA_CLINICIAN_DETAILS, mSelectedClinicianDetailsData);
                    bundle1.putInt(AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION, mSelectedClinicPosition);
                    bundle1.putSerializable(AxSysConstants.EXTRA_BOOK_APPOINTMENT_DATETIME, mSelectedTimeSlotCalendar);
                    bundle1.putSerializable(AxSysConstants.EXTRA_BOOK_APPOINTMENT_TIMESLOT, mSelectedTimeSlotObject);
                    bundle1.putString(AxSysConstants.EXTRA_BOOK_APPOINTMENT_CONS_MODE, mSelectedConsultationMode);
                    bundle1.putSerializable(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT, mSelectedClinicianObject);
                    reviewAppointmentDetailsFragment.setArguments(bundle1);
                    ft1.add(R.id.container_layout, reviewAppointmentDetailsFragment);
                    ft1.addToBackStack(ReviewAppointmentDetailsFragment.class.getSimpleName());
                    ft1.commit();
                } else {
                    showToast(R.string.select_timeslot);
                }
                break;

            case R.id.bv_timeslotleftarrow:
                //if required put condition to check previous dates.
                mSelectedTimeSlotCalendar.add(Calendar.DATE, -1);
                getTimeSlots(mSelectedTimeSlotCalendar);

                break;
            //Sravan added the case for rigth arrow
            case R.id.bv_timeslotrightarrow:
                mSelectedTimeSlotCalendar.add(Calendar.DATE, 1);
                getTimeSlots(mSelectedTimeSlotCalendar);
                break;

            default:
                break;
        }
    }

    private void showDatePicker() {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt("year", mSelectedTimeSlotCalendar.get(Calendar.YEAR));
        args.putInt("month", mSelectedTimeSlotCalendar.get(Calendar.MONTH));
        args.putInt("day", mSelectedTimeSlotCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerFragment.setArguments(args);

        datePickerFragment.setCallBack(onDateSet);
        datePickerFragment.show(getActivity().getFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    private DatePickerDialog.OnDateSetListener onDateSet = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mSelectedTimeSlotCalendar.set(year, monthOfYear, dayOfMonth);

            // get time slots for the selected date
            getTimeSlots(mSelectedTimeSlotCalendar);
        }
    };

    private class ClinicDetailsAdapter extends BaseAdapter {

        private ArrayList<ClinicianDetailsResponse.ClinicDetails> clinicDetailsArrayList;

        public ClinicDetailsAdapter(ArrayList<ClinicianDetailsResponse.ClinicDetails> clinicDetailsArrayList) {
            this.clinicDetailsArrayList = clinicDetailsArrayList;
        }

        public void updateData(ArrayList<ClinicianDetailsResponse.ClinicDetails> clinicDetailsArrayList) {
            this.clinicDetailsArrayList = clinicDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (clinicDetailsArrayList != null && clinicDetailsArrayList.size() > 0) ? clinicDetailsArrayList.size() : 0;
        }

        @Override
        public ClinicianDetailsResponse.ClinicDetails getItem(int position) {
            return clinicDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ClinicianDetailsResponse.ClinicDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.clinic_details_list_row, parent, false);
            }

            TextView clinicNameTextView = (TextView) convertView.findViewById(R.id.clinicNameTextView);
            TextView feeTitleTextView = (TextView) convertView.findViewById(R.id.feeTextView);
            TextView feeValueTextView = (TextView) convertView.findViewById(R.id.feeValueTextView);
            TextView timeSlotsTextView = (TextView) convertView.findViewById(R.id.timeSlotsTextView);
            TextView locationDetailsTextView = (TextView) convertView.findViewById(R.id.locationDetailsTextView);
            TextView contactDetailsTextView = (TextView) convertView.findViewById(R.id.contactDetailsTextView);
            TextView emailDetailsTextView = (TextView) convertView.findViewById(R.id.emailDetailsTextView);
            TextView websiteDetailsTextView = (TextView) convertView.findViewById(R.id.websiteDetailsTextView);
            TextView detailsTitleTextView = (TextView) convertView.findViewById(R.id.detailsTextView);

            Button bookAppointmentButton = (Button) convertView.findViewById(R.id.bookAppointmentButton);
            LinearLayout noteLayout = (LinearLayout) convertView.findViewById(R.id.noteLayout);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

            clinicNameTextView.setText(mClinicName);
            clinicNameTextView.setTypeface(bold);
            detailsTitleTextView.setTypeface(bold);
            feeTitleTextView.setTypeface(bold);

            feeValueTextView.setText(rowObject.getFee());
            feeValueTextView.setTypeface(bold);

            if (bookAppoinments) {
                bookAppointmentButton.setVisibility(View.VISIBLE);
                noteLayout.setVisibility(View.VISIBLE);
            } else {
                bookAppointmentButton.setVisibility(View.GONE);

            }

            if (rowObject.getTimingsArrayList() != null && rowObject.getTimingsArrayList().size() > 0) {
                StringBuffer strBuffer = new StringBuffer();
                for (int idx = 0; idx < rowObject.getTimingsArrayList().size(); idx++) {
                    ClinicianDetailsResponse.Timings timeSlot = rowObject.getTimingsArrayList().get(idx);

                    if (!TextUtils.isEmpty(timeSlot.getTimingSlots())) {
                        strBuffer.append(timeSlot.getTimingSlots());
                    }

                    if (idx < rowObject.getTimingsArrayList().size()) {
                        strBuffer.append(", ");
                    }
                }
                timeSlotsTextView.setText(strBuffer.toString());
                timeSlotsTextView.setTypeface(regular);
            }

            locationDetailsTextView.setText(rowObject.getAddress1() +
                    (!TextUtils.isEmpty(rowObject.getAddress2()) ? ", " + rowObject.getAddress2() : "") +
                    (!TextUtils.isEmpty(rowObject.getAddress3()) ? ", " + rowObject.getAddress3() : "") +
                    (!TextUtils.isEmpty(rowObject.getState()) ? ", " + rowObject.getState() : "") +
                    (!TextUtils.isEmpty(rowObject.getPostcode()) ? ", " + rowObject.getPostcode() : ""));
            locationDetailsTextView.setTypeface(regular);

            contactDetailsTextView.setText(getString(R.string.text_contact) + (!TextUtils.isEmpty(rowObject.getPhone()) ? rowObject.getPhone() : ""));
            contactDetailsTextView.setTypeface(regular);

            emailDetailsTextView.setText(getString(R.string.text_email) + (!TextUtils.isEmpty(rowObject.getEmail()) ? rowObject.getEmail() : ""));
            emailDetailsTextView.setTypeface(regular);

            websiteDetailsTextView.setText(!TextUtils.isEmpty(rowObject.getURL()) ? rowObject.getURL() : "");
            websiteDetailsTextView.setTypeface(regular);

            bookAppointmentButton.setVisibility(View.GONE);

            return convertView;
        }
    }

    private class TimeSlotsGridViewAdapter extends BaseAdapter {

        private SimpleDateFormat timeSlotFormat = new SimpleDateFormat(AxSysConstants.DF_TIMESLOT_DISPLAY_FORMAT);
        private ArrayList<TimeSlotsResponse.TimeSlotDetails> timeSlotDetailsArrayList;

        private View previouslySelectedSlot;

        public TimeSlotsGridViewAdapter(ArrayList<TimeSlotsResponse.TimeSlotDetails> timeSlotDetailsArrayList) {
            this.timeSlotDetailsArrayList = timeSlotDetailsArrayList;
        }

        public void updateData(ArrayList<TimeSlotsResponse.TimeSlotDetails> timeSlotDetailsArrayList) {
            this.timeSlotDetailsArrayList = timeSlotDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return timeSlotDetailsArrayList != null && timeSlotDetailsArrayList.size() > 0 ? timeSlotDetailsArrayList.size() : 0;
        }

        @Override
        public TimeSlotsResponse.TimeSlotDetails getItem(int position) {
            return timeSlotDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final TimeSlotsResponse.TimeSlotDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.timeslots_row, parent, false);
            }

            final TextView timeSlotTimeTextView = (TextView) convertView.findViewById(R.id.timeSlotTimeTextView);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

            timeSlotTimeTextView.setTypeface(regular);
            timeSlotTimeTextView.setTextSize(17);

            if (!TextUtils.isEmpty(rowObject.getTimeSlot())) {
                String timeSlot = "";
                try {
                    Date timeSlotDateTime = timeSlotFormat.parse(rowObject.getTimeSlot());
                    timeSlot = timeSlotFormat.format(timeSlotDateTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                timeSlotTimeTextView.setText(timeSlot);
            } else {
                timeSlotTimeTextView.setText("00:00");
            }

            if (rowObject.isSlotAvailable()) {
                // Slot available
                timeSlotTimeTextView.setTextColor(getResources().getColor(R.color.actionbar_color));
            } else {
                // Already booked
                timeSlotTimeTextView.setTextColor(getResources().getColor(android.R.color.darker_gray));
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // click should work for only available slots

                    if (rowObject.isSlotAvailable()) {
                        // slot available
                        // reset the previously selected slot
                        if (previouslySelectedSlot != null) {
                            previouslySelectedSlot.setBackgroundResource(R.drawable.white_form_no_corners);
                            ((TextView) previouslySelectedSlot.findViewById(R.id.timeSlotTimeTextView)).setTextColor(getResources().getColor(R.color.actionbar_color));
                        }

                        view.setBackgroundResource(R.drawable.blue_form_no_corners);
                        timeSlotTimeTextView.setTextColor(getResources().getColor(R.color.white));

                        mSelectedTimeSlotObject = rowObject;

                        previouslySelectedSlot = view;
                    }
                }
            });

            return convertView;
        }
    }

}
