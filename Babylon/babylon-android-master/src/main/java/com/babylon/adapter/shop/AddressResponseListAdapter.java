package com.babylon.adapter.shop;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.model.Kit.Address;

import java.util.List;

public class AddressResponseListAdapter extends ArrayAdapter<Address> {




    private static final String TAG = AddressResponseListAdapter.class.getSimpleName();

    public AddressResponseListAdapter(Context context, List<Address> categories) {
        super(context, 0, categories);

    }

    private class ViewHolder
    {

        TextView catNameView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = View.inflate(getContext(), R.layout.list_item_address_details, null);
            viewHolder.catNameView = (TextView) convertView.findViewById(R.id.address_details_name);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Address responseItem = getItem(position);

        viewHolder.catNameView.setText(responseItem.getFirstLine()+", "+responseItem.getSecondLine()
                +"\n"+responseItem.getCity()+","+responseItem.getPostCode());


        return convertView;
    }




}
