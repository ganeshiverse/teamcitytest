package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.data.responses.ProxyListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.31.15
 * Time: 13:04
 *
 * @author Artem G Ajaykumar
 */
public class DisplayProxyListLoader extends DataAsyncTaskLibLoader<ProxyListResponse> {

    private String token;
    private String lmd;
    private String includehistory;

    public DisplayProxyListLoader(Context context, String token, String lmd, String includehistory) {
        super(context);
        this.token = token;
        this.lmd = lmd;
        this.includehistory = includehistory;
    }

    @Override
    protected ProxyListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getProxyList(token, lmd, includehistory);
    }
}
