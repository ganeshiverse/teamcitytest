package com.babylon.utils.samsung.health;

import android.content.ContentProviderOperation;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import com.babylon.App;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.enums.ActivityType;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.activity.ActivitySaver;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataResolver;

import java.util.ArrayList;
import java.util.List;

public class StepsProcessor extends AsyncTask<HealthDataResolver.ReadResult, Void, Void> {
    private static String TAG = StepsProcessor.class.getSimpleName();
    private long dateOfNewestDataInMillis = 0;
    private boolean wasActivityRewrited;
    final List<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

    @Override
    protected Void doInBackground(HealthDataResolver.ReadResult... params) {
        Cursor cursor = null;

        try {
            cursor = params[0].getResultCursor();
            if (cursor != null && cursor.moveToFirst()) {

                L.d(TAG, "count = " + String.valueOf(cursor.getCount()));

                PhysicalActivity physicalActivity = null;

                do {
                    int steps = cursor.getInt(cursor.getColumnIndex(HealthConstants.StepCount
                            .COUNT));
                    int calories = cursor.getInt(cursor.getColumnIndex(HealthConstants.StepCount
                            .CALORIE));
                    long startTime = cursor.getLong(cursor.getColumnIndex(HealthConstants.StepCount
                            .START_TIME));
                    long endTime = cursor.getLong(cursor.getColumnIndex(HealthConstants.StepCount
                            .END_TIME));
                    float distance = cursor.getFloat(cursor.getColumnIndex(HealthConstants.StepCount
                            .DISTANCE));

                    L.d(TAG, "steps = " + steps);
                    L.d(TAG, "calories = " + calories);
                    L.d(TAG, "startTime = " + DateUtils.longToString(startTime));
                    L.d(TAG, "endTime = " + DateUtils.longToString(endTime));

                    if (cursor.isFirst()) {
                        dateOfNewestDataInMillis = startTime;
                    }

                    physicalActivity = new PhysicalActivity(startTime, endTime, calories, steps,
                            ActivityType.WALKING, PhysicalActivity.Source.SAMSUNG);
                    physicalActivity.setDistanceInMeter(distance);

                    boolean curWasActivityRewrited = savePhysicalActivity(physicalActivity);

                    if(curWasActivityRewrited && !wasActivityRewrited) {
                        wasActivityRewrited = true;
                    }


                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        if (!operations.isEmpty()) {
            DatabaseOperationUtils.getInstance().applyBatch(App.getInstance().getContentResolver(), operations);
        }

        return null;
    }


    private boolean savePhysicalActivity(PhysicalActivity physicalActivity) {
        boolean wasActivityUpdated = false;
        if (android.text.format.DateUtils.isToday(physicalActivity.getStartDate().getTime())) {
            wasActivityUpdated = ActivitySaver.rewrite(App.getInstance().getContentResolver(),
                    physicalActivity);
        } else {
            ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(PhysicalActivitySchema
                    .CONTENT_URI);
            builder.withValues(physicalActivity.toContentValues());
            operations.add(builder.build());
            L.d(TAG, "steps added to operations = " + physicalActivity.getSteps());
        }
        return wasActivityUpdated;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        final Bundle b = new Bundle();

        if (android.text.format.DateUtils.isToday(dateOfNewestDataInMillis) && wasActivityRewrited) {
            b.putLong(Constants.EXTRA_MIN_DATE, DateUtils.getDayStartMills(dateOfNewestDataInMillis));
            b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(dateOfNewestDataInMillis, 1));
            b.putBoolean(SyncAdapter.KEY_IS_SYNCED_MONITOR_ME_REPORT, true);
        }

        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY, b, SyncAdapter.SYNC_PUSH);
    }
}
