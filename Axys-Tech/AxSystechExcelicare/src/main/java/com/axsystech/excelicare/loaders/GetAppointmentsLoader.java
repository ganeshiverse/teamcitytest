package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppointmentListResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class GetAppointmentsLoader extends DataAsyncTaskLibLoader<AppointmentListResponse> {

    private String token;
    private String panelId;
    private String patientId;
    private String start;
    private String max;
    private String isDictonaryResponse;
    private String filterCriteria;

    public GetAppointmentsLoader(Context context, String token, String panelId, String patientId, String start, String max,
                                 String isDictonaryResponse, String filterCriteria) {
        super(context);
        this.token = token;
        this.panelId = panelId;
        this.patientId = patientId;
        this.start = start;
        this.max = max;
        this.isDictonaryResponse = isDictonaryResponse;
        this.filterCriteria = filterCriteria;
    }

    @Override
    protected AppointmentListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getAppointmentsList(token, panelId, patientId, start, max, isDictonaryResponse, filterCriteria);
    }
}
