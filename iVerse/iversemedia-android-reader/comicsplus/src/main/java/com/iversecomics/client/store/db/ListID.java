package com.iversecomics.client.store.db;

public final class ListID {
    public static final String NEW_RELEASES = "new";
    public static final String TOP_PAID = "top-paid";
    public static final String TOP_FREE = "top-free";
    public static final String FEATURED_COMICS = "featured";
    public static final String SEARCH = "search";
    public static final String RELEASE_BY_DATE = "release-by-date";
    public static final String PUBLISHERS = "publishers";
    public static final String GROUPS = "groups";
    public static final String FEATUREDSLOTS = "featuredslots";
    public static final String ON_DEMAND = "ondemand";

    public static final String getComicsByGenre(String genreId) {
        return "by-genre:" + genreId;
    }

    public static final String getFeaturedComicsByCategoryId(String categoryId) {
        return "by-categoryid-featured-comics:" + categoryId;
    }

    public static String getComicsByPublisher(String publisherId) {
        return "by-publisher:" + publisherId;
    }

    public static String getComicsBySeries(String seriesId) {
        return "by-series:" + seriesId;
    }

    public static String getComicsByGroup(String groupId) {
        return "by-group:" + groupId;
    }

    public static String getComicsByReleaseDate(String releaseDate) {
        return "by-release:" + releaseDate;
    }

    public static String getComicsByPromotion(String promotionID) {
        return "by-promotion:" + promotionID;
    }
}
