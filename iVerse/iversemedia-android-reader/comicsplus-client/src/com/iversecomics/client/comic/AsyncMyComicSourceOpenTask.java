package com.iversecomics.client.comic;

import java.io.IOException;

import android.os.AsyncTask;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class AsyncMyComicSourceOpenTask extends AsyncTask<Void, Void, Boolean> {
    private static final Logger LOG = LoggerFactory.getLogger(AsyncMyComicSourceOpenTask.class);
    private IMyComicSourceAdapter comicSource;
    OnAsyncMyComicSourceOpenTaskListener listener;
    public AsyncMyComicSourceOpenTask(OnAsyncMyComicSourceOpenTaskListener listener, IMyComicSourceAdapter comicSource) {
        this.comicSource = comicSource;
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            comicSource.open();
            
        } catch (IOException e) {
        	
        	//TODO from the docs
        	//getSigners() - Returns null. (On Android, a ClassLoader can load classes 
        	//from multiple dex files. All classes from any given dex file will have the 
        	//same signers, but different dex files may have different signers. This does 
        	//not fit well with the original ClassLoader-based model of getSigners.)
            LOG.warn(e, "Unable to open comicSource: %s", this.comicSource.getClass().getSigners());
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    
    protected void onPostExecute(Boolean result) {
        if(listener != null)
        {
        	listener.OnAsyncMyComicSourceOpenTaskFinished(result.booleanValue());
        }
    }
    
    
    public interface OnAsyncMyComicSourceOpenTaskListener
    {
    	public abstract void OnAsyncMyComicSourceOpenTaskFinished(boolean bSuccess);
    }
}