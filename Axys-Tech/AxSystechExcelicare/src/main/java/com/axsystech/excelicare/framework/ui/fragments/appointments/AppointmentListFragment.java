package com.axsystech.excelicare.framework.ui.fragments.appointments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AppointmentListResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.CancelledAppointmentsLoader;
import com.axsystech.excelicare.loaders.GetAppointmentsListPaginationLoader;
import com.axsystech.excelicare.loaders.GetAppointmentsLoader;
import com.axsystech.excelicare.loaders.PastAppointmentsLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.DateFormatUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by someswarreddy on 22/08/15.
 */
public class AppointmentListFragment extends BaseFragment implements View.OnClickListener, TextWatcher, CancelAppointmentFragment.CancelCallBackListener {

    private static final String GET_APPOINTMENTS_LIST = "com.axsystech.excelicare.framework.ui.fragments.appointments.AppointmentListFragment.GET_APPOINTMENTS_LIST";
    private static final String SEARCH_APPOINTMENTS_LIST = "com.axsystech.excelicare.framework.ui.fragments.appointments.AppointmentListFragment.SEARCH_APPOINTMENTS_LIST";
    private static final String GET_PAST_APPOINTMENTS_LIST = "com.axsystech.excelicare.framework.ui.fragments.appointments.AppointmentListFragment.GET_PAST_APPOINTMENTS_LIST";
    private static final String GET_CANCELLED_APPOINTMENTS_LIST = "com.axsystech.excelicare.framework.ui.fragments.appointments.AppointmentListFragment.GET_CANCELLED_APPOINTMENTS_LIST";

    private String TAG = AppointmentListFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private AppointmentListFragment mFragment;

    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.listView)
    private ListView listView;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    int PageCount = 0;
    int pageCountPast = 0;
    int pageCountCancelled = 0;

    private ArrayList<AppointmentListResponse.AppointmentDetails> appointmentDetailsAL;

    private ArrayList<AppointmentListResponse.AppointmentDetails> appointmentDetailsALPagination = new ArrayList<AppointmentListResponse.AppointmentDetails>();

    private AppointmentsListAdapter appointmentsListAdapter;

    @Override
    protected void initActionBar() {

        mActivity.getSupportActionBar().setTitle(com.axsystech.excelicare.util.views.TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

       /* Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");
*/
        mActivity.getSupportActionBar().setTitle(mActionBarTitle);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message_inbox, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mLayoutInflater = LayoutInflater.from(mActivity);
        mFragment = this;

        readIntentArgs();
        initViews();
        addEventListeners();
        preLoadData();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void initViews() {
        searchEditText.setHint(getString(R.string.hint_search_appontments));
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        appointmentsListAdapter = new AppointmentsListAdapter(null);
        listView.setAdapter(appointmentsListAdapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount)) {

                    if (appointmentDetailsAL != null && appointmentDetailsAL.size() != 0) {
                        preLoadData();

                    }
                }
            }
        });

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // http://115.119.121.38:8989/ECService_PHRV64VL_DEV/AxRSTableView.svc/getListContents?
                    // Token=c9507279-93e1-4a65-a6cf-77771e1fbaf4&PanelId=1112&PatientId=40&Start=1&Max=100&IsDictionaryJSONResponse=1&FilterCriteria=ajmera

                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        GetAppointmentsLoader searchAppointmentsLoader = new GetAppointmentsLoader(mActivity, mActiveUser.getToken(), AxSysConstants.PANEL_ID_APPOINTMENTS,
                                mActiveUser.getPatientID(), "1", "100", "1", searchEditText.getText().toString().trim().replace(" ", ""));
                        if (getView() != null && isAdded()) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SEARCH_APPOINTMENTS_LIST), searchAppointmentsLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                return false;
            }
        });
    }

    private void preLoadData() {

        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            // get list of appointments for current user
            PageCount = PageCount + 1;
            GetAppointmentsListPaginationLoader getAppointmentsLoader = new GetAppointmentsListPaginationLoader(mActivity, mActiveUser.getToken(), AxSysConstants.PANEL_ID_APPOINTMENTS,
                    mActiveUser.getPatientID(), PageCount, "1");
            if (getView() != null && isAdded()) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_APPOINTMENTS_LIST), getAppointmentsLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
        appointmentsListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_APPOINTMENTS_LIST)) {
            showToast(R.string.no_appointments);
        }
    }

//    @Override
//    public void onPause() {
//        super.onPause();
//        System.out.println("AppointmentFragment . . . "+PageCount);
//        PageCount = 0;
//    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_APPOINTMENTS_LIST)) {
            if (result != null && result instanceof AppointmentListResponse) {
                AppointmentListResponse appointmentListResponse = (AppointmentListResponse) result;
                if (appointmentListResponse.getPanel() != null) {
                    appointmentDetailsAL = appointmentListResponse.getPanel().getAppointmentDetailsAL();

                    if (appointmentDetailsALPagination == null) {
                        appointmentDetailsALPagination.addAll(appointmentDetailsAL);
                        if (appointmentDetailsALPagination != null && appointmentDetailsALPagination.size() > 0) {
                            // update to list adapter
                            appointmentsListAdapter = new AppointmentsListAdapter(appointmentDetailsALPagination);
                            listView.setAdapter(appointmentsListAdapter);
                        } else {
                            showToast("Your appointments list is empty");
                        }
                    } else {
                        appointmentDetailsALPagination.addAll(appointmentDetailsAL);
                        if (appointmentDetailsALPagination != null && appointmentDetailsALPagination.size() > 0) {
                            // update to list adapter
                            appointmentsListAdapter = new AppointmentsListAdapter(appointmentDetailsALPagination);
                            listView.setAdapter(appointmentsListAdapter);
                        } else {
                            showToast("Your appointments list is empty");
                        }
                    }

                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SEARCH_APPOINTMENTS_LIST)) {
            if (result != null && result instanceof AppointmentListResponse) {
                AppointmentListResponse appointmentListResponse = (AppointmentListResponse) result;
                if (appointmentListResponse.getPanel() != null) {
                    ArrayList<AppointmentListResponse.AppointmentDetails> searchResultsAL = appointmentListResponse.getPanel().getAppointmentDetailsAL();

                    if (appointmentsListAdapter != null) {
                        appointmentsListAdapter.updateData(searchResultsAL);
                    } else {
                        appointmentsListAdapter = new AppointmentsListAdapter(searchResultsAL);
                        listView.setAdapter(appointmentsListAdapter);
                    }

                    if (searchResultsAL != null && searchResultsAL.size() > 0) {

                    } else {
                        showToast(R.string.no_search_results_found);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_PAST_APPOINTMENTS_LIST)) {
            if (result != null && result instanceof AppointmentListResponse) {
                AppointmentListResponse appointmentListResponse = (AppointmentListResponse) result;
                if (appointmentListResponse.getPanel() != null) {
                    ArrayList<AppointmentListResponse.AppointmentDetails> pastAppoinmentsList = appointmentListResponse.getPanel().getAppointmentDetailsAL();

                    if (pastAppoinmentsList != null && pastAppoinmentsList.size() < 0) {

                        appointmentsListAdapter = new AppointmentsListAdapter(pastAppoinmentsList);
                        listView.setAdapter(appointmentsListAdapter);
                    }
                } else {
                    showToast(R.string.no_past_appointments_found);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_CANCELLED_APPOINTMENTS_LIST)) {
            if (result != null && result instanceof AppointmentListResponse) {
                AppointmentListResponse appointmentListResponse = (AppointmentListResponse) result;
                if (appointmentListResponse.getPanel() != null) {
                    ArrayList<AppointmentListResponse.AppointmentDetails> cancelledAppoinmentsList = appointmentListResponse.getPanel().getAppointmentDetailsAL();

                    if (cancelledAppoinmentsList != null && cancelledAppoinmentsList.size() < 0) {

                        appointmentsListAdapter = new AppointmentsListAdapter(cancelledAppoinmentsList);
                        listView.setAdapter(appointmentsListAdapter);
                    }
                } else {
                    showToast(R.string.no_past_appointments_found);
                }
            }
        }
    }

    @Override
    public void onCancelSuccess() {
        PageCount = 0;
        appointmentDetailsAL.clear();
        appointmentDetailsALPagination.clear();
        appointmentsListAdapter.notifyDataSetChanged();
        //getLoaderHelper().removeLoaderFromRunningLoaders(get);
//        if (getView() != null && isAdded()) {
//            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_APPOINTMENTS_LIST), getAppointmentsLoader);
//        }
        preLoadData();
    }

    private class AppointmentsListAdapter extends BaseAdapter {

        private ArrayList<AppointmentListResponse.AppointmentDetails> membersInvitationsCircle;

        public AppointmentsListAdapter(ArrayList<AppointmentListResponse.AppointmentDetails> membersInvitationsCircle) {
            this.membersInvitationsCircle = membersInvitationsCircle;
        }

        public void updateData(ArrayList<AppointmentListResponse.AppointmentDetails> membersInvitationsCircle) {
            this.membersInvitationsCircle = membersInvitationsCircle;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return membersInvitationsCircle != null && membersInvitationsCircle.size() > 0 ? membersInvitationsCircle.size() : 0;
        }

        @Override
        public AppointmentListResponse.AppointmentDetails getItem(int position) {
            return membersInvitationsCircle.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final AppointmentListResponse.AppointmentDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.appointments_list_row, viewGroup, false);
            }

            ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImageView);
//            Picasso.with(getActivity()).load(R.drawable.user_icon).placeholder(R.drawable.user_icon).error(R.drawable.ic_launcher)
//                    .into(memberImageView);
            Picasso.with(getActivity()).load(R.drawable.user_icon).transform(new CircleTransform()).into(memberImageView);
            TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberNameTextView);
            TextView professionTextView = (TextView) convertView.findViewById(R.id.professionTextView);
            TextView dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
            TextView statusTextView = (TextView) convertView.findViewById(R.id.statusTextView);
            TextView clinicTextView = (TextView) convertView.findViewById(R.id.clinicTextView);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            String drName = !TextUtils.isEmpty(rowObject.getDoctor()) ? rowObject.getDoctor() : "";

            memberNameTextView.setText("Dr. " + drName);
            memberNameTextView.setTypeface(bold);

            professionTextView.setText(!TextUtils.isEmpty(rowObject.getSpeciality()) ? (rowObject.getSpeciality()) : "");
            professionTextView.setTypeface(regular);

//            String displayDate = "";
//            if(!TextUtils.isEmpty(rowObject.getAppointmentDateTime()))
//            DateFormatUtils.formatDefaultDate(rowObject.getAppointmentDateTime(),"dd-MMM-yyyy HH:mm");
            dateTextView.setText(!TextUtils.isEmpty(rowObject.getAppointmentDateTime()) ? rowObject.getAppointmentDateTime() : "");
            dateTextView.setTypeface(bold);

            if (!TextUtils.isEmpty(rowObject.getAppointmentType())) {
                if (rowObject.getAppointmentType().equals("New")) {
                    statusTextView.setText("Pending");
                }
                if (statusTextView.getText().toString().equals("Pending")) {
                    statusTextView.setTextColor(getResources().getColor(R.color.circles_search_text_color));
                }
//                if(rowObject.getAppointmentType().trim().equalsIgnoreCase("Confirmed")) {
//                    statusTextView.setTextColor(getResources().getColor(R.color.app_theme_color));
//                } else {
//                    statusTextView.setTextColor(getResources().getColor(R.color.gray_color));
//                }
            } else {
                statusTextView.setText("");
            }

            statusTextView.setTypeface(regular);

            clinicTextView.setText(!TextUtils.isEmpty(rowObject.getClinicName()) ? rowObject.getClinicName() : "");
            clinicTextView.setTypeface(bold);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                    final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                    final CancelAppointmentFragment cancelAppointmentFragment = new CancelAppointmentFragment();
                    cancelAppointmentFragment.setCancelCallBackListener(mFragment);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                    bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_appointment_details));
                    bundle.putString(AxSysConstants.EXTRA_APPOINTMENT_ID, rowObject.getID());
                    bundle.putString(AxSysConstants.EXTRA_APPOINTMENT_TYPE, rowObject.getAppointmentType());
                    cancelAppointmentFragment.setArguments(bundle);
                    ft.add(R.id.container_layout, cancelAppointmentFragment, CancelAppointmentFragment.class.getSimpleName());
                    ft.addToBackStack(CancelAppointmentFragment.class.getSimpleName());
                    ft.commit();
                }
            });

            return convertView;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_appointments_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            case R.id.action_past_appointments:
                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);
                    // get list of appointments for current user
                    pageCountPast = pageCountPast + 1;
                    PastAppointmentsLoader pastAppointmentsLoader = new PastAppointmentsLoader(mActivity, mActiveUser.getToken(), AxSysConstants.PANEL_ID_PAST_APPOINTMENTS,
                            mActiveUser.getPatientID(), pageCountPast, "2", "1");
                    if (getView() != null && isAdded()) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_PAST_APPOINTMENTS_LIST), pastAppointmentsLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            case R.id.action_cancelled_appointments:
                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);
                    // get list of appointments for current user
                    pageCountCancelled = pageCountCancelled + 1;
                    CancelledAppointmentsLoader cancelledAppointmentsLoader = new CancelledAppointmentsLoader(mActivity, mActiveUser.getToken(), AxSysConstants.PANEL_ID_CANCELLED_APPOINTMENTS,
                            mActiveUser.getPatientID(), pageCountCancelled, "2", "1");
                    if (getView() != null && isAdded()) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CANCELLED_APPOINTMENTS_LIST), cancelledAppointmentsLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                CommonUtils.hideSoftKeyboard(mActivity, searchEditText);

                // update to list adapter with original items
                if (appointmentDetailsAL != null && appointmentDetailsAL.size() > 0) {
                    // update to list adapter
                    if (appointmentsListAdapter != null) {
                        appointmentsListAdapter.updateData(appointmentDetailsAL);
                    } else {
                        appointmentsListAdapter = new AppointmentsListAdapter(appointmentDetailsAL);
                        listView.setAdapter(appointmentsListAdapter);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

}
