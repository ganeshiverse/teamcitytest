package com.babylon.api.request.base;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;

import com.babylon.App;
import com.babylon.BuildConfig;
import com.babylon.R;
import com.babylon.api.request.SessionsPost;
import com.babylon.broadcast.SignOutBroadcastReceiver;
import com.babylon.model.Patient;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;

public abstract class AbsRequest<M extends HttpRequestBase, O> {
    public static final String UTF_8 = "UTF-8";
    public static final String TAG = AbsRequest.class.getSimpleName();

    private static final String USER_AGENT = "android";

    protected M httpRequest;
    private AndroidHttpClient httpClient;

    protected void setHeaders(M httpRequest)
    {
        httpRequest.addHeader(new BasicHeader("content-type", "application/json;charset=utf-8"));
        httpRequest.addHeader(new BasicHeader("timezone", DateUtils.getDefaultTimezoneId()));
        httpRequest.addHeader(new BasicHeader("JsonStub-User-Key", "160775b2-a68d-4a3e-90e1-f7cbee0919ed"));
        httpRequest.addHeader(new BasicHeader("JsonStub-Project-Key", "151446b5-26f6-4e47-987d-5143f0ce8c14"));

        for (Map.Entry<String, String> entry : getVersionHeader().entrySet())
            httpRequest.addHeader(new BasicHeader(entry.getKey(), entry.getValue()));

    }

    protected BasicHeader getAuthHeader()
    {
        Wsse wsse = SyncUtils.getWsse();
        String token = wsse != null ? wsse.toString() : "";
        return new BasicHeader(Constants.Header_RequestKeys.HEADER_WSSE_KEY, token);
    }

    public static HashMap<String, String> getVersionHeader()
    {
        HashMap versionHeaders = new HashMap<String, String>();
        versionHeaders.put("X-Platform", "Android");
        versionHeaders.put("X-Platform-Version", String.valueOf(Build.VERSION.RELEASE));

        String versionName = String.valueOf(BuildConfig.VERSION_NAME);
        try
        {
            versionName = App.getInstance().getPackageManager().getPackageInfo(App.getInstance().getPackageName(), 0).versionName;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }

        versionHeaders.put("X-App-Version", versionName);

        return versionHeaders;
    }

    private void updateAuthHeader()
    {
        BasicHeader authHeader = getAuthHeader();
        if (authHeader != null)
        {
            httpRequest.removeHeaders(authHeader.getName());
            httpRequest.addHeader(authHeader);
        }
    }

    protected abstract String getUrl();

    protected abstract M acquireHttpRequest();

    protected abstract AbsResponseProcessor<O> getResponseProcessor();

    /**
     * For default nor redirect screen after sign in
     * If after auto sign out app should redirect user to special screen after sign in
     * Should override  this method in request
     * @return Intent which know to which screen should redirect after sign in
     */
    protected Intent getSignOutBroadcastIntent() {
        return null;
    }

    protected Response<O> processResponse(HttpResponse httpResponse)
    {
        AbsResponseProcessor<O> responseProcessor = getResponseProcessor();
        responseProcessor.setGson(getGson());
        return responseProcessor.getResponse(httpResponse);
    }

    AbsRequest()
    {
        httpRequest = acquireHttpRequest();
        failIfTrue(httpRequest == null, "Method acquireHttpRequest() shouldn't return null",
                getClass().getSimpleName());
    }

    public M getHttpRequest() {
        return httpRequest;
    }

    public Response<O> execute()
    {
        Response<O> response;
        try
        {
            L.d(TAG, toString());

            M request = prepareRequest();
            HttpResponse httpResponse = getHttpResponse(request);
            if (shouldRelogin(httpResponse) && SyncUtils.getWsse() != null && !PreferencesManager.getInstance().isManualHandlingAutoSignOut())
            {
                SyncUtils.setWsse(null);
                Log.d("AbsRequest", "signout called");
                SignOutBroadcastReceiver.setEnabled(true);
                SignOutBroadcastReceiver.sendBroadcast();
            }
            response = processResponse(httpResponse);
        }
        catch (IOException e)
        {
            L.e(TAG, e.getMessage(), e);
            response = new Response<O>();
            response.setStatus(Response.RESULT_ERROR);

            if (!App.getInstance().isConnection())
            {
                final String message = App.getInstance().getString(R.string.offline_error);
                final Error error = new Error(message, message);

                response.addError(error);
            }
        }
        finally
        {
            cancel();
        }
        return response;
    }

    protected HttpResponse getHttpResponse(M request) throws IOException
    {
        return getAndroidHttpClient().execute(request);
    }

    protected void relogin() {
        Patient patientWithCreds = SyncUtils.getPatientWithCreds();
        SessionsPost sessionsPost = new SessionsPost();
        sessionsPost.withEntry(patientWithCreds);
        Response<Patient> response = sessionsPost.execute();
        if (response.isSuccess()) {
            Patient data = response.getData();
            SyncUtils.setWsse(new Wsse(data.getId(), data.getToken()));
        }
    }

    private boolean shouldRelogin(HttpResponse httpResponse) {
        StatusLine statusLine = httpResponse.getStatusLine();
        boolean result;
        if (statusLine != null && statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public void cancel()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                if (httpClient != null)
                {
                    httpClient.close();
                }
            }
        }).start();
    }

    protected M prepareRequest()
    {
        updateAuthHeader();
        setHeaders(httpRequest);
        httpRequest.setURI(getURI());
        return httpRequest;
    }

    protected String getUrlParams(List<BasicNameValuePair> params)
    {
        StringBuilder urlParams = new StringBuilder();
        final int size = params.size();
        if (size > 0)
        {
            urlParams.append("?");
        }
        for (int i = 0; i < size; i++)
        {
            NameValuePair nameValuePair = params.get(i);
            urlParams.append(nameValuePair.getName());
            urlParams.append("=");
            try
            {
                String value = URLEncoder.encode(nameValuePair.getValue(), AbsRequest.UTF_8);
                urlParams.append(value);
            }
            catch (UnsupportedEncodingException e)
            {
                L.e(TAG, e.getMessage(), e);
            }
            if (i != size - 1)
            {
                urlParams.append("&");
            }
        }
        return urlParams.toString();
    }

    protected String getParam(List<BasicNameValuePair> params, String name) {
        for (int i = 0; i < params.size(); i++) {
            NameValuePair nameValuePair = params.get(i);
            if (nameValuePair.getName().equals(name)) {
                return nameValuePair.getValue();
            }
        }
        return null;
    }

    public URI getURI() {
        URI uri = null;
        try {
            String url = getUrl();
            failIfTrue(TextUtils.isEmpty(url), "Url can't be null. Check getUrl() method",
                    getClass().getSimpleName());
            uri = new URI(url);
        } catch (URISyntaxException e) {
            L.e(TAG, e.toString(), e);
        }
        return uri;
    }

    protected Gson getGson() {
        return GsonWrapper.getGson();
    }

    protected String toJson(Object entry) {
        final String json = getGson().toJson(entry);
        return json;
    }

    protected String toJson(List<Object> entries) {
        String json = getGson().toJson(entries);
        return json;
    }

    protected AndroidHttpClient getAndroidHttpClient() {
        if (httpClient != null) {
            httpClient.close();
        }

        httpClient = AndroidHttpClient.newInstance(USER_AGENT, App.getInstance());
        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);

        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", socketFactory, 443));

        return httpClient;
    }

    protected ParameterizedType getResponseType(int typePosition) {
        final Type cls = getParamClass(typePosition);
        return new ParameterizedType() {

            @Override
            public Type getRawType() {
                return Response.class;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

            @Override
            public Type[] getActualTypeArguments() {
                return new Type[]{cls};
            }
        };
    }

    /**
     * return last generic param which should describe output entity type
     *
     * @return
     */
    protected Type getOutputType() {
        Type[] actualTypeArguments = ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments();
        int outputTypePosition = actualTypeArguments.length - 1;
        return getParamClass(outputTypePosition);
    }

    protected Type getParamClass(int typePosition) {
        return ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[typePosition];
    }

    public void execute(final FragmentActivity activity,
                        @NonNull final RequestListener<O> requestListener) {
        /* NonNull annotation not help in this case, because it may happen in runtime */
        if (activity != null) {
            final LoaderManager loaderManager = activity.getSupportLoaderManager();
            final int loaderId = hashCode();
            loaderManager.restartLoader(loaderId, null, new LoaderManager.LoaderCallbacks<Response<O>>() {

                @Override
                public Loader<Response<O>> onCreateLoader(int i, Bundle bundle) {
                    return new ResponseLoader(activity, AbsRequest.this);
                }

                @Override
                public void onLoadFinished(Loader<Response<O>> responseLoader, Response<O> response) {
                    requestListener.onRequestComplete(response);
                    loaderManager.destroyLoader(loaderId);
                }

                @Override
                public void onLoaderReset(Loader<Response<O>> responseLoader) {

                }
            });
        }
    }

    public static  void failIfTrue(boolean isEmpty, String message, String className) {
        if (isEmpty) {
            String text = TextUtils.concat(className, ": ", message).toString();
            throw new IllegalArgumentException(text);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbsRequest that = (AbsRequest) o;

        if (!getUrl().equals(that.getUrl())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getUrl().hashCode();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(httpRequest.getMethod());
        builder.append(":");
        builder.append(getUrl());
        builder.append(":");
        builder.append(getUrl().hashCode());
        return builder.toString();
    }
}

