package com.babylon.fragment.check;

import android.text.TextUtils;
import android.widget.Toast;

import com.babylon.R;
import com.babylon.enums.CheckGender;
import com.babylon.model.check.SymptomAnswer;
import com.babylon.model.check.ViewState;

import java.util.ArrayList;

public class BodyPartQuestionFragment extends QuestionsFragment {

    public static final String BODY_PART_FRAGMENT_TAG = "BODY_PART_FRAGMENT_TAG";

    public static BodyPartQuestionFragment newInstance(CheckGender gender, int bodyPartId, String title) {
        BodyPartQuestionFragment fragment = new BodyPartQuestionFragment();
        fragment.putArgs(gender, bodyPartId, title);
        return fragment;
    }

    protected ArrayList<String> getHashesList(ArrayList<SymptomAnswer> symptomAnswers) {
        ArrayList<String> symptomsHash = new ArrayList<String>();
        String hash = TextUtils.join("-", symptomAnswers);
        symptomsHash.add(hash);
        return symptomsHash;
    }

    protected boolean hasUnselectedAnswers(ArrayList<SymptomAnswer> symptomAnswers) {
        for (SymptomAnswer symptomAnswer : symptomAnswers) {
            if (ViewState.UNSELECTED.equals(symptomAnswer.getState())) {
                return true;
            }
        }
        return false;
    }

    protected void notifyAboutUnselectedAnswers() {
        Toast.makeText(getActivity(), getString(R.string.select_all_answers), Toast.LENGTH_SHORT).show();
    }
}
