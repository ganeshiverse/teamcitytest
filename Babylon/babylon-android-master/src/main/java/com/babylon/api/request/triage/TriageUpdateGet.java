package com.babylon.api.request.triage;

import android.content.ContentResolver;
import android.text.TextUtils;

import com.babylon.api.request.base.Urls;

import org.apache.http.message.BasicNameValuePair;

public class TriageUpdateGet extends TriageExportGet {

    private static final String LAST_UPDATE_DATE = "lastUpdatedDate";
    private final long lastSyncTime;

    public TriageUpdateGet(ContentResolver contentResolver, long lastSyncTime) {
        super(contentResolver);
        this.lastSyncTime = lastSyncTime;
    }

    @Override
    protected String getUrl() {
        params.add(new BasicNameValuePair(LAST_UPDATE_DATE, String.valueOf(lastSyncTime)));
        return TextUtils.concat(Urls.get(Urls.TRIAGE_UPDATE), getUrlParams(params)).toString();
    }
}
