package com.babylon.activity.family;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.model.Account;
import com.babylonpartners.babylon.CircleTransform;

import java.util.List;

public class FamilyAccountsListAdapter extends ArrayAdapter<Account> {

    private static final String TAG = FamilyAccountsListAdapter.class.getSimpleName();

    public FamilyAccountsListAdapter(Context context, List<Account> accounts) {
        super(context, 0, accounts);
    }

    private class ViewHolder
    {
        ImageView avatarView;
        TextView accountNameView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = View.inflate(getContext(), R.layout.list_item_family_account, null);
            viewHolder.avatarView = (ImageView) convertView.findViewById(R.id.avatar_image_view);
            viewHolder.accountNameView = (TextView) convertView.findViewById(R.id.account_name_text_view);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Account account = getItem(position);
        if (account.getAvatar() != null) {
            byte[] decodedString = Base64.decode(account.getAvatar(), Base64.DEFAULT);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            CircleTransform xform = new CircleTransform();
            Bitmap circleBitmap = xform.transform(decodedBitmap);
            decodedBitmap.recycle();
            viewHolder.avatarView.setImageBitmap(circleBitmap);
        } else {
            viewHolder.avatarView.setImageResource(R.drawable.profile_avatar);
        }
        viewHolder.accountNameView.setText(account.getFirstName() + " " + account.getLastName());

        return convertView;
    }
}
