package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class ChangePasswordVerifyOtpLoader extends DataAsyncTaskLibLoader<ChangePasswordResponse> {

    private final String deviceId;
    private final String token;
    private final String siteID;
    private final String password;

    public ChangePasswordVerifyOtpLoader(final Context context, String deviceId, String token, String siteID, String password) {
        super(context);
        this.deviceId = deviceId;
        this.token = token;
        this.siteID = siteID;
        this.password = password;
    }

    @Override
    protected ChangePasswordResponse performLoad() throws Exception {
        return ServerMethods.getInstance().verifyOtpAndRegister(deviceId, token, siteID, password);
    }
}
