package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.ContentChangeTracker;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class SearchStoreTaskTest extends AbstractStubActivityTestCase {
    private static final Logger LOG = LoggerFactory.getLogger(SearchStoreTaskTest.class);

    @MediumTest
    public void testSearchStore() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(ComicsTable.CONTENT_URI, this);

        String query = "robo";
        ComicStore comicStore = getComicStore();
        SearchStoreTask task = new SearchStoreTask(comicStore, query);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, ComicsTable.CONTENT_URI);
        LOG.debug("Found %d results for search query \"%s\"", actualCount, query);
        OurAsserts.assertGreaterThan("Comics table rows", 10, actualCount);
    }
}
