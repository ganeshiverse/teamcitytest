package com.axsystech.excelicare.util;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.framework.ui.activities.LoginActivity;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

/**
 * Created by hkyerra on 9/13/2015.
 */
public class GcmIntentService extends IntentService {
    //    private static final int NOTIFICATION_ID = 1;
    private static final String TAG = "GcmIntentService";
    private NotificationManager mNotificationManager;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                for (int i = 0; i < 5; i++) {
                    Log.i(TAG, "Working... " + (i + 1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                String msg = extras.getString("message");
                //if (msg != null)
                    //sendNotification(extras.toString());

                Log.i(TAG, msg + "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        Log.i("GCMIntent Srevice1", "" + msg);
        String username = null, pswrd = null;
        Intent loginIntent = null;
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Log.i("GCMIntent Srevice2", "" + msg);
        String mPushtext = null, pushType = null;
        String loopId = null;
        try {
            JSONArray jsonArray = new JSONArray(msg);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            //JSONObject jsonObject = new JSONObject(msg);
            // LoopMeConstants.PUSH_TEXT = jsonObject.getString("text");
            mPushtext = jsonObject.getString("alert");
            //pushType = jsonObject.getString("type");
            //loopId = jsonObject.getString("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //check the username is empty
//        TODO get password and username from db
//        username = SharedPreferenceUtil.getStringFromSP(AxSysTechApplication.getAppContext(),
//                AxSysConstants.U);
//        pswrd = SharedPreferenceUtil.getStringFromSP(LoopMeApplication.getAppContext(),
//                LoopMeConstants.SP_PASSWORD);


        Trace.i("----username username-----", "--" + username.length());

        if (username != null && username.length() > 0 && pswrd != null && pswrd.length() > 0) {
//        Trace.i(TAG,"Login status :"+SharedPreferenceUtil.getStringFromSPreferences(LoopMeApplication.getAppContext(),LoopMeConstants.CHECK_LOGIN));
            loginIntent = new Intent(this, MainContentActivity.class);
            loginIntent.putExtra("push", "push");
            Trace.i(TAG, "msg :" + msg);
            loginIntent.putExtra("message", msg);


        } else {
            loginIntent = new Intent(this, LoginActivity.class);
            loginIntent.putExtra("push", "push");
            loginIntent.putExtra("message", msg);
        }

        Random NOTIFICATION_ID = new Random();
       /* PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, LoginActivity.class), 0);*/
        //if (!pushType.equalsIgnoreCase("closedloop")) {
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    loginIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Log.i("GCMIntent Srevice3", "" + msg);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle("Notifications testing")
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(mPushtext))
                            .setContentText(mPushtext)
                            .setSound(alarmSound)
                            .setAutoCancel(true);

            Log.i("GCMIntent Srevice4", "" + msg);
            mBuilder.setContentIntent(contentIntent);
            Log.i("GCMIntent Srevice5", "" + msg);

            mNotificationManager.notify(NOTIFICATION_ID.nextInt(), mBuilder.build());
            mBuilder.setAutoCancel(true);
            //mBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            Log.i("GCMIntent Srevice6", "" + msg);
        //}
//        else {
//
//            int CANCELNOTIFICATIONID = NOTIFICATION_ID.nextInt();
//            // define sound URI, the sound to be played when there's a notification
//            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Log.i("******* Srevice6", "" + msg);
//            // intent triggered, you can add other intent for other actions
//            Intent intent = new Intent(GcmIntentService.this, LoginActivity.class);
//            PendingIntent pIntent = PendingIntent.getActivity(GcmIntentService.this, 0, intent, 0);
//
//
//            Intent deleteIntent = new Intent(GcmIntentService.this, DeleteArchiveLoopActivity.class);
//            deleteIntent.putExtra(LoopMeConstants.EXTRA_DELETE_ARCHIVE_LOOPS, "Delete loops");
//            Trace.i(TAG, "Looptype Delete loop");
//            deleteIntent.putExtra("DELETE_ARCHIVE_LOOP_ID", loopId);
//            deleteIntent.putExtra("NOTIFICATONID", CANCELNOTIFICATIONID);
//            deleteIntent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
//            PendingIntent pDeleteIntent = PendingIntent.getActivity(this, 145623, deleteIntent, PendingIntent.FLAG_ONE_SHOT);
//
//            Intent archiveIntent = new Intent(GcmIntentService.this, DeleteArchiveLoopActivity.class);
//            Trace.i(TAG, "Looptype Archive loop");
//            archiveIntent.putExtra(LoopMeConstants.EXTRA_DELETE_ARCHIVE_LOOPS, "Archive loops");
//            archiveIntent.putExtra("DELETE_ARCHIVE_LOOP_ID", loopId);
//            archiveIntent.putExtra("NOTIFICATONID", CANCELNOTIFICATIONID);
//            archiveIntent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
//            PendingIntent pArchiveIntent = PendingIntent.getActivity(this, 145623, archiveIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
//            notificationBuilder.setSmallIcon(R.drawable.ic_launcher);
//            notificationBuilder.setContentTitle("ULoop Me");
//            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(mPushtext));
//            notificationBuilder.setContentText(mPushtext);
//            notificationBuilder.setSound(soundUri);
//            notificationBuilder.addAction(R.drawable.delete, "Delete", pDeleteIntent);
//            notificationBuilder.addAction(R.drawable.archive, "Archive", pArchiveIntent);
//            notificationBuilder.setAutoCancel(false);
//
//            notificationBuilder.setContentIntent(pArchiveIntent);
//            Log.i("GCMIntent Srevice5", "" + msg);
//            notificationBuilder.setOngoing(true);
//            mNotificationManager.notify(CANCELNOTIFICATIONID, notificationBuilder.build());
//        }
    }
}
