package com.iversecomics.client.refresh;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

public class HashedBlockingQueueTest extends AndroidTestCase {
    private void assertOfferFailure(HashedBlockingQueue queue, String msg) {
        MessageTask task = new MessageTask(msg);
        assertFalse("Should have failed in adding msg [" + msg + "]", queue.offer(task));
    }

    private void assertOfferSuccess(HashedBlockingQueue queue, String msg) {
        MessageTask task = new MessageTask(msg);
        assertTrue("Should have succeeded in adding msg [" + msg + "]", queue.offer(task));
    }

    @SmallTest
    public void testQueueContains() {
        HashedBlockingQueue queue = new HashedBlockingQueue();

        assertOfferSuccess(queue, "by order of the prophet");
        assertOfferSuccess(queue, "we ban that boogie sound");
        assertOfferSuccess(queue, "degenerate the faithful");
        assertOfferSuccess(queue, "with that crazy casbah sound");
        assertOfferSuccess(queue, "rock the casbah");

        // Test offer with ones that currently exist in the queue.
        assertOfferFailure(queue, "rock the casbah");
        assertOfferFailure(queue, "we ban that boogie sound");

        // Remove an entry.
        queue.remove(new MessageTask("degenerate the faithful"));

        // Try to add it back before it expires.
        assertOfferFailure(queue, "degenerate the faithful");
    }
}
