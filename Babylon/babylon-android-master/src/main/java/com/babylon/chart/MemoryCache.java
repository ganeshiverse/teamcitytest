package com.babylon.chart;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class MemoryCache {
    private Map<String, SoftReference<String>> cache = new HashMap<String, SoftReference<String>>();

    public String get(String id) {
        if (!cache.containsKey(id))
            return null;
        SoftReference<String> ref = cache.get(id);
        return ref.get();
    }

    public void put(String id, String data) {
        cache.put(id, new SoftReference<String>(data));
    }

    public void clear() {
        cache.clear();
    }
}