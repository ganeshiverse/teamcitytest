package com.iversecomics.store;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Storage;
import com.iversecomics.client.account.Utils;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.tasks.VerifyPurchaseTask;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.ui.LoginActivity;
import com.iversecomics.ui.SignUpActivity;

import java.util.Locale;


public class ComicBuyer {
    private final static Logger LOG = LoggerFactory.getLogger(ComicBuyer.class);
    AbstractAppStoreAdapter appStoreAdapter;
    private IverseApplication mApplication;
    private Activity mActivity;
    private Context mContext;
    private Comic mComic;
    //    private BillingService billingService;
//    private ComicPurchaseObserver purchaseObserver;
    private Handler handler;

    public ComicBuyer(Activity activity, AbstractAppStoreAdapter appStoreAdapter) {
        this.appStoreAdapter = appStoreAdapter;
        if (!(activity instanceof AppStoreAdapterLocator)) {
            throw new IllegalArgumentException("Activity must implement AppStoreAdapterLocator.");
        }
        mActivity = activity;
        mApplication = (IverseApplication) mActivity.getApplication();
        mContext = activity.getApplicationContext();

        handler = new Handler();
//        purchaseObserver = new ComicPurchaseObserver(handler);

    }

    public void purchaseComic(final View btn, final Comic comic) {

        mComic = comic;

        //require user to be logged in to download
        if (!Utils.isLoggedIn(mContext)) {
            showAccountRequiredDialog(mActivity);
            return;
        }

        //skip the purchase and warn the user if the sd is not available
        if (storageAvailable()) {
            final MyComicsModel model = new MyComicsModel(mContext);
            final MyComicsModel.PossessionState p = model.isOwned(comic);
            //if the comic is free or it is owned but not downloaded
            final boolean downloadAgain = (p == MyComicsModel.PossessionState.PURCHASED);
            if (comic.isFree() || downloadAgain) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

                builder.setMessage(downloadAgain ? R.string.download_restore : R.string.download_message_free);

                builder.setCancelable(true);
                //new PurchaseFreeComic((AbstractStoreActivity) CurrentIssueFragment.this.getActivity(), comic)
                builder.setPositiveButton(R.string.download_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        btn.setClickable(false);

                        final VerifyPurchaseTask task = new VerifyPurchaseTask(mContext, mApplication.getComicStore(), comic, false);
                        mApplication.getTaskPool().submit(task);

                        Toast.makeText(mContext, mContext.getResources().getString(R.string.download_requested) + comic.getName(), Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton(R.string.download_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {

                if (comic.isFree()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setMessage(R.string.download_message_free);
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.download_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            purchaseFreeComic(false);
                        }
                    });
                    builder.setNegativeButton(R.string.download_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    mComic = comic;
                    appStoreAdapter.purchaseComic(comic);
                }

            }
        }
    }

    public void downloadSubscriptioBook(Comic comic){
        mComic = comic;
        purchaseFreeComic(true);
    }

    public void purchaseSubscription() {
        // joliver: 12/30/2013, There should only be one subscription product SKU at the moment,
        // we'll have to add another to support the Samsung App Store.
        if (!Utils.isLoggedIn(mContext)) {
            showAccountRequiredDialog(mActivity);
            return;
        }
        if (ServerConfig.getDefault().getSubscriptionProducts().size() > 0) {
            String subscriptionProductId = ServerConfig.getDefault().getSubscriptionProducts().get(0);
//            appStoreAdapter.purchaseSubscription(/*subscriptionProductId*/"com.iversecomics.archie.android");
            appStoreAdapter.purchaseSubscription(subscriptionProductId);

        } else {
            LOG.warn("No subscription SKUs configured.");
        }
    }


    public boolean storageAvailable() {
        boolean storageAvailable = true;

        try {
            Storage.assertAvailable();
        } catch (Exception e) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(R.string.storage_unavailable_dialog_title)
                    .setIcon(R.drawable.microsd_warn)
                    .setMessage(mApplication.getResources().getString(R.string.storage_unavailable_dialog_message))
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNegativeButton(R.string.settings,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final Intent intent = new Intent(
                                            android.provider.Settings.ACTION_INTERNAL_STORAGE_SETTINGS);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mApplication.startActivity(intent);
                                }
                            });
            builder.create().show();
            storageAvailable = false;
        }
        return storageAvailable;
    }

    private String replaceLanguageAndRegion(String str) {
        // Substitute language and or region if present in string
        if (str.contains("%lang%") || str.contains("%region%")) {
            Locale locale = Locale.getDefault();
            str = str.replace("%lang%", locale.getLanguage().toLowerCase());
            str = str.replace("%region%", locale.getCountry().toLowerCase());
        }
        return str;
    }

    private void purchaseFreeComic(boolean isOndemand) {

        final VerifyPurchaseTask task = new VerifyPurchaseTask(mContext, mApplication.getComicStore(), mComic, isOndemand);
        mApplication.getTaskPool().submit(task);

        Toast.makeText(mContext, mContext.getResources().getString(R.string.download_requested) + mComic.getName(), Toast.LENGTH_LONG).show();
    }

    private void showAccountRequiredDialog(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.drawable.icon)
                .setTitle(R.string.account_required_title)
                .setMessage(com.iversecomics.archie.android.R.string.account_required_message)
                .setNegativeButton(com.iversecomics.archie.android.R.string.account_required_negative_button, null)
                .setPositiveButton(com.iversecomics.archie.android.R.string.account_required_positive_button, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Create the "Sign up or Log In" dialog
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(R.string.sign_or_log_in_title)
                                .setMessage(R.string.sign_or_log_in_message)
                                .setIcon(R.drawable.icon)
                                .setCancelable(true)
                                .setPositiveButton(R.string.log_in_button, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Offline support
                                        if(ConnectionHelper.isConnectedToInternet(context)) {
                                            context.startActivity(new Intent(context, LoginActivity.class));
                                        }else{
                                            ConnectionHelper.showNoInternetMessage();
                                        }

                                    }
                                })

                                .setNegativeButton(R.string.sign_up_button, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Offline support
                                        if(ConnectionHelper.isConnectedToInternet(context)) {
                                            context.startActivity(new Intent(context, SignUpActivity.class));
                                        }else{
                                            ConnectionHelper.showNoInternetMessage();
                                        }

                                    }
                                });

                        builder.create().show();
                    }
                }).create().show();
    }

//    /**
//     * A {@link com.iversecomics.store.iab.PurchaseObserver} is used to get callbacks when Android Market sends messages to this application so
//     * that we can update the UI.
//     */
//    private class ComicPurchaseObserver extends PurchaseObserver {
//        private final Logger LOG = LoggerFactory.getLogger(ComicPurchaseObserver.class);
//
//        public ComicPurchaseObserver(Handler handler) {
//            super(mActivity, handler);
//        }
//
//        @Override
//        public void onBillingSupported(boolean supported) {
//            LOG.debug("supported: %b", supported);
//            if (!supported) {
//                //Show "IAB not available" dialog
//                String helpUrl = replaceLanguageAndRegion(mApplication.getResources().getString(R.string.billing_help_url));
//                LOG.debug(helpUrl);
//                final Uri helpUri = Uri.parse(helpUrl);
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
//                builder.setTitle(R.string.billing_not_supported_title).setIcon(android.R.drawable.stat_sys_warning).setMessage(R.string.billing_not_supported_message)
//                        .setCancelable(false).setPositiveButton(android.R.string.ok, null)
//                        .setNegativeButton(R.string.learn_more, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Intent intent = new Intent(Intent.ACTION_VIEW, helpUri);
//                                mApplication.startActivity(intent);
//                            }
//                        });
//                builder.create().show();
//            }
//        }
//
//        @Override
//        public void onPurchaseStateChange(PurchaseState purchaseState, String itemId, int quantity, long purchaseTime,
//                                          String receiptData, String developerPayload) {
//            LOG.debug("onPurchaseStateChange() itemId: %s %s", itemId, purchaseState);
//
//            if (purchaseState == PurchaseState.PURCHASED) {
//                //Issue Purchase Verification to Our Server
//                final VerifyPurchaseTask task = new VerifyPurchaseTask(mContext, mApplication.getComicStore(), mComic);
//                task.setReceiptData(receiptData);
//                mApplication.getTaskPool().submit(task);
//            }
//        }
//
//        @Override
//        public void onRequestPurchaseResponse(RequestPurchase request, ResponseCode responseCode) {
//            LOG.debug("%s: %s", request.mProductId, responseCode);
//            if (responseCode == ResponseCode.RESULT_OK) {
//                LOG.debug("purchase was successfully sent to server: %s", request.mProductId);
//            } else if (responseCode == ResponseCode.RESULT_USER_CANCELED) {
//                LOG.debug("user canceled purchase (dialog canceled)");
//            } else {
//                LOG.debug("purchase failed, returned code %s", responseCode);
//            }
//        }
//
//        @Override
//        public void onRestoreTransactionsResponse(RestoreTransactions request, ResponseCode responseCode) {
//            if (responseCode == ResponseCode.RESULT_OK) {
//                LOG.debug("completed RestoreTransactions request");
//                // TODO: Flag restore in app, to prevent automatic restore every time the Activity is started.
//            } else {
//                LOG.warn("RestoreTransactions error: %s", responseCode);
//            }
//        }
//    }

}
