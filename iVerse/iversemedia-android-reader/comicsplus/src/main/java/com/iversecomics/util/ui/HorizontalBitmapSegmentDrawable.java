package com.iversecomics.util.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class HorizontalBitmapSegmentDrawable extends Drawable {

    Bitmap source;
    ColorFilter colorFilter;

    int width;
    int height;
    int paddingLeft;
    int paddingRight;

    public HorizontalBitmapSegmentDrawable(Bitmap source, int width, int paddingLeft, int paddingRight) {
        this.source = source;
        this.width = source.getWidth();
        this.height = source.getHeight();
        this.paddingLeft = this.width * paddingLeft / width;
        this.paddingRight = this.width * paddingRight / width;
    }

    Rect rectDst = new Rect();
    Rect rectSrc = new Rect();

    @Override
    public void draw(Canvas canvas) {

        Rect dst = this.getBounds();

        canvas.save();
        Paint paint = new Paint();
        if (colorFilter != null) {
            paint.setColorFilter(colorFilter);
        }
        int h = dst.height();
        int l = paddingLeft * h / height;
        int r = paddingRight * h / height;
        // draw middle
        {
            rectSrc.left = paddingLeft;
            rectSrc.top = 0;
            rectSrc.right = width - paddingRight;
            rectSrc.bottom = height;
            rectDst.left = dst.left + l;
            rectDst.top = dst.top;
            rectDst.right = dst.right - r;
            rectDst.bottom = dst.bottom;
            canvas.drawBitmap(source, rectSrc, rectDst, paint);
        }
        // draw left side
        {
            rectSrc.left = 0;
            rectSrc.top = 0;
            rectSrc.right = paddingLeft;
            rectSrc.bottom = height;
            rectDst.left = dst.left;
            rectDst.top = dst.top;
            rectDst.right = dst.left + l;
            rectDst.bottom = dst.bottom;
            canvas.drawBitmap(source, rectSrc, rectDst, paint);
        }
        // draw right side
        {
            rectSrc.left = width - paddingRight;
            rectSrc.top = 0;
            rectSrc.right = width;
            rectSrc.bottom = height;
            rectDst.left = dst.right - r;
            rectDst.top = dst.top;
            rectDst.right = dst.right;
            rectDst.bottom = dst.bottom;
            canvas.drawBitmap(source, rectSrc, rectDst, paint);
        }
        canvas.restore();
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        colorFilter = cf;
        this.invalidateSelf();
    }

    @Override
    public int getOpacity() {
        return 1;
    }

}