#!/bin/bash

function emuCmd()
{
    EMUCMD="$@"
    echo "[adb shell]\$ $EMUCMD"
    adb shell $EMUCMD
    echo ""
}

emuCmd ls -l /data/data/com.iversecomics.client/databases/

