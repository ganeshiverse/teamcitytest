package com.iversecomics.client.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.bitmap.ILateBitmapListener;
import com.iversecomics.client.util.ResourceUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class LateBitmapView extends ImageView implements ILateBitmapListener {
    private static final Logger LOG = LoggerFactory.getLogger(LateBitmapView.class);
    private Drawable placeholder;

    public LateBitmapView(Context context) {
        super(context);
        init(context);
    }

    public LateBitmapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LateBitmapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        placeholder = context.getResources().getDrawable(R.drawable.gray);
        setImageDrawable(placeholder);
        setAdjustViewBounds(true);
        setScaleType(ScaleType.FIT_CENTER);
    }

    private boolean isTagMatch(String incomingTag) {
        if ((incomingTag == null) || (getTag() == null)) {
            return true; // assume true
        }
        String tag = getTag().toString();
        return incomingTag.equals(tag);
    }

    @Override
    public void setLateBitmap(Bitmap bitmap, String tag) {
        LOG.debug("Got late bitmap (%dx%d): %s", bitmap.getWidth(), bitmap.getHeight(), tag);
        if (isTagMatch(tag)) {
            ResourceUtil.free(getDrawable()); // free existing drawable (if needed)
            setScaleType(ScaleType.FIT_CENTER);
            setImageBitmap(bitmap);
        } else {
            LOG.debug("Got tag mismatch expecting [%s] but got [%s]", tag, getTag());
            ResourceUtil.free(bitmap); // overlapping bitmap, dump it
        }
    }

    public void unset() {
        setScaleType(ScaleType.FIT_CENTER);
        setImageDrawable(placeholder);
    }
}
