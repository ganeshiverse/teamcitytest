package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.data.responses.CityListResponse;
import com.axsystech.excelicare.data.responses.StateListResponse;
import com.axsystech.excelicare.loaders.CityListLoader;
import com.axsystech.excelicare.loaders.StateListLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * Created by someswar on 6/30/2015.
 */
public class StateListActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private static final String GET_STATE_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.SignUpActivity.GET_STATE_LIST_LOADER";

    private StateListActivity mActivity;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.listView)
    private ListView listView;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    private ArrayList<StateListResponse.StateDetails> stateDetailsAL;
    private StateListAdapter stateListAdapter;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity,getString(R.string.signup_text)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_message_inbox);
        mActivity = this;

        preloadData();
        loadCityList();
        readIntentData();
        addEventListeners();

    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_state));
    }

    private void loadCityList() {
        // get City list from Server
        if(CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            StateListLoader stateListLoader = new StateListLoader(mActivity, "", AxSysConstants.STATES_LOOKUP_ID, "");
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_STATE_LIST_LOADER), stateListLoader);
        } else {
            showToast(R.string.error_loading_states);
        }
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if(id == getLoaderHelper().getLoaderId(GET_STATE_LIST_LOADER)) {
            if(result != null && result instanceof StateListResponse) {
                StateListResponse stateListResponse = (StateListResponse) result;

                if(stateListResponse.getSystemLookupsAL() != null && stateListResponse.getSystemLookupsAL().size() > 0) {
                    stateDetailsAL = stateListResponse.getSystemLookupsAL().get(0).getStateDetailsAL();
                    // Bind data to listview
                    if(stateListAdapter == null) {
                        stateListAdapter = new StateListAdapter(stateDetailsAL);
                        listView.setAdapter(stateListAdapter);
                    } else {
                        stateListAdapter.updateData(stateDetailsAL);
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if(id == getLoaderHelper().getLoaderId(GET_STATE_LIST_LOADER)) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                // update to list adapter with original items
                if(stateListAdapter != null) {
                    stateListAdapter.updateData(stateDetailsAL);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if(text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    private class StateListAdapter extends BaseAdapter {

        private ArrayList<StateListResponse.StateDetails> stateDetailsArrayList;

        public StateListAdapter(ArrayList<StateListResponse.StateDetails> stateDetailsArrayList) {
            this.stateDetailsArrayList = stateDetailsArrayList;
        }

        public void updateData(ArrayList<StateListResponse.StateDetails> stateDetailsArrayList) {
            this.stateDetailsArrayList = stateDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (stateDetailsArrayList != null && stateDetailsArrayList.size() > 0) ? stateDetailsArrayList.size() : 0 ;
        }

        @Override
        public StateListResponse.StateDetails getItem(int position) {
            return stateDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final StateListResponse.StateDetails rowObj = getItem(position);

            if(convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.circles_list_row, parent, false);
            }

            TextView cityNameTextView = (TextView) convertView.findViewById(R.id.circleNameTextView);
            cityNameTextView.setText(!TextUtils.isEmpty(rowObj.getValue()) ? rowObj.getValue() : "");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(AxSysConstants.EXTRA_SELECTED_STATE, rowObj);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });

            return convertView;
        }
    }

}
