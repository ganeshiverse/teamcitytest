package com.axsystech.excelicare.loaders;

import android.content.Context;
import android.text.TextUtils;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.exceptions.NoNetworkException;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by alanka on 9/23/2015.
 */
public class RemoveDeviceFromDbLoader extends DataAsyncTaskLibLoader<List<DeviceUsersDbModel>> {

    private final String ecUserId;

    public RemoveDeviceFromDbLoader(final Context context, final String ecUserId) {
        super(context, true);
        this.ecUserId = ecUserId;
    }

    @Override
    protected List<DeviceUsersDbModel> performLoad() throws Exception {
        if (TextUtils.isEmpty(ecUserId))
            throw new SQLException("EcUserId should not be empty...");
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final Dao<DeviceUsersDbModel, Long> deviceUserDbModelDao = helper.getDao(DeviceUsersDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<DeviceUsersDbModel>>() {
                @Override
                public List<DeviceUsersDbModel> call() throws Exception {
                    return deviceUserDbModelDao.deleteBuilder().where().eq(DeviceUsersDbModel.EC_USER_ID, ecUserId).query();
                }
            });
        } catch (final Exception e) {
            throw new NoNetworkException();
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
