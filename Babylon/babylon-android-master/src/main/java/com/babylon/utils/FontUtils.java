package com.babylon.utils;

import android.graphics.Typeface;

import com.babylon.App;

import java.util.HashMap;
import java.util.Map;

public final class FontUtils {
    private static final String TAG = "FontUtils";
    private static final Map<String, Typeface> TYPEFACE_HASH_MAP = new HashMap<String, Typeface>();

    protected FontUtils() {
        // not called
    }

    public enum Font {
        RobotoLight("fonts/Roboto-Light.ttf"),
        RobotoRegular("fonts/Roboto-Regular.ttf"),
        RobotoItalic("fonts/Roboto-Italic.ttf"),
        RobotoBold("fonts/Roboto-Bold.ttf"),
        RobotoMedium("fonts/Roboto-Medium.ttf"),
        RobotoThin("fonts/Roboto-Thin.ttf");

        private final String filePath;

        private Font(String name) {
            filePath = name;
        }

        public String getFilePath() {
            return filePath;
        }
    }

    public static Typeface getTypeface(Font font) {
        String assetPath = font.getFilePath();
        Typeface typeface = TYPEFACE_HASH_MAP.get(assetPath);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(App.getInstance().getAssets(), assetPath);
            } catch (RuntimeException e) {
                L.e(TAG, "Failed to load typeface from font asset '" + assetPath + "'", e);
                return null;
            }
            TYPEFACE_HASH_MAP.put(assetPath, typeface);
        }
        return typeface;
    }
}
