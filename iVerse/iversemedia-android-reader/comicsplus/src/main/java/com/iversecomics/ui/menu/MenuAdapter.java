package com.iversecomics.ui.menu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.store.ui.sidemenu.SideMenuItemInfo;
import com.iversecomics.client.store.ui.sidemenu.SideMenuSection;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.Collections;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class MenuAdapter extends ArrayAdapter<SideMenuItemInfo> implements StickyListHeadersAdapter {
    private List<SideMenuSection> mSections;
    /**
     * Array keeping the first index of the section. Indexes correspond to mSections.
     */
    private int[] mSectionHeads;

    private LayoutInflater mInflater;

    private DisplayImageOptions mDisplayOptions;

    public MenuAdapter(Context context) {
        super(context, 0);
        setData(null);
        mInflater = LayoutInflater.from(context);
        mDisplayOptions = new Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                        //.displayer(new FadeInBitmapDisplayer(200))
                .build();
    }

    public void setData(List<SideMenuSection> data) {
        clear();
        if (data == null) {
            mSections = Collections.emptyList();
        } else {
            mSections = data;
        }
        int numSections = mSections.size();
        mSectionHeads = new int[numSections];

        int sectionHead = 0;

        for (int i = 0; i < numSections; ++i) {
            mSectionHeads[i] = sectionHead;

            SideMenuSection section = mSections.get(i);

            int sectionSize = section.size();
            List<SideMenuItemInfo> menuItems = section.menuItems;

            for (int j = 0; j < sectionSize; ++j) {
                add(menuItems.get(j));
            }

            sectionHead += sectionSize;
        }
        notifyDataSetChanged();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ItemHolder holder;
        SideMenuItemInfo info = getItem(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.menu_list_item, parent, false);

            TextView title = (TextView) view.findViewById(R.id.title);
            ImageView thumb = (ImageView) view.findViewById(R.id.thumb);

            holder = new ItemHolder(title, thumb);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ItemHolder) view.getTag();
        }


        holder.info = info;
        holder.title.setText(info.getTitle());

        ImageView thumb = holder.thumb;
        String imageUrl = info.getImageURL();
        thumb.setImageDrawable(null);
        if (imageUrl == null || imageUrl.length() == 0) {
            thumb.setVisibility(View.GONE);
            ImageLoader.getInstance().cancelDisplayTask(thumb);
        } else {
            thumb.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(info.getImageURL(), holder.thumb, mDisplayOptions, holder);
        }

        // show chevron icon to the right to indicate this item has a sub-menu
        Drawable chevron = null;
        if (info.hasSubMenu()) {
            chevron = getContext().getResources().getDrawable(R.drawable.cell_icon_chevron);
        }
        holder.title.setCompoundDrawablesWithIntrinsicBounds(null, null, chevron, null);

        return view;
    }


//    @Override
//    public SideMenuItemInfo getItem(int position) {
//
//        int c = 0;
//        for (int i = 0; i < mSections.size(); i++) {
//            if (position >= c && position < c + mSections.get(i).second.size()) {
//                return mSections.get(i).second.get(position - c);
//            }
//            c += mSections.get(i).second.size();
//        }
//
//        return null;
//
//    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        int sectionIndex = getSectionIndex(position);
        SideMenuSection section = mSections.get(sectionIndex);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.menu_item_header, parent, false);
        }

        TextView title = (TextView) convertView;
        title.setText(section.title);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return getSectionIndex(position);
    }

    private int getSectionIndex(int position) {
        int numSections = mSectionHeads.length;
        int sectionIndex = 0;

        for (; sectionIndex < numSections; ++sectionIndex) {
            //This should be the only check. We've reached the section head that is above the position.
            //It accounts for empty sections (e.g. mSectionHeads = [0, 2, 2, 8] and for
            //position 2, we want to return index 2, not 1).
            if (position < mSectionHeads[sectionIndex]) {
                break;
            }
        }

        return sectionIndex - 1;
    }

    private class ItemHolder extends SimpleImageLoadingListener {
        final TextView title;
        final ImageView thumb;
        SideMenuItemInfo info;

        ItemHolder(TextView title, ImageView thumb) {
            this.title = title;
            this.thumb = thumb;
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (!info.imageLoaded) {
                FadeInBitmapDisplayer.animate((ImageView) view, 300);
                info.imageLoaded = true;
            }
        }
    }
}