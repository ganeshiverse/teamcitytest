package com.iversecomics.io;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;

public final class IOUtil {
    public static final int     BUFFER_SIZE = (1024 * 8);

    public static void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }

        try {
            closeable.close();
        } catch (IOException ignore) {
            /* ignore */
        }
    }

    public static long copy(InputStream input, File outputFile) throws IOException {
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(outputFile);
            return copy(input, output);
        } finally {
            close(output);
        }
    }

    public static long copy(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        long count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
    
    public static long copy(InputStream input, OutputStream output, long length) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        long count = 0;
        int n = 0;
        int chunk = BUFFER_SIZE;
        while (count < length) {
        	if (count + chunk > length) {
        		chunk = (int) (length - count);
        	}
        	if (-1 == (n = input.read(buffer, 0, chunk))) {
        		break;
        	}
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    public static String readAsString(InputStream in) throws IOException {
        StringWriter ret = new StringWriter();
        InputStreamReader reader = new InputStreamReader(in);
        char[] buffer = new char[BUFFER_SIZE];
        int n = 0;
        while (-1 != (n = reader.read(buffer))) {
            ret.write(buffer, 0, n);
        }
        return ret.toString();
    }

    private IOUtil() {
        /* prevent instantiation */
    }
}