package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.client.store.model.Group;

public class GroupsTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri      CONTENT_URI            = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "groups");

    public static final Uri      CONTENT_URI_BY_GROUPID = Uri.withAppendedPath(CONTENT_URI, "byid");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of publisher.
     */
    public static final String   CONTENT_TYPE           = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                                + "/vnd.iversecomics.store.groups";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single user.
     */
    public static final String   CONTENT_ITEM_TYPE      = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                                + "/vnd.iversecomics.store.groups";

    public static final String   DEFAULT_SORT_ORDER     = "name ASC";
    public static final String   TABLE                  = "groups";

    // server id for group
    public static final String   GROUPID                = "groupId";
    public static final String   NAME                   = "name";
    public static final String   FEATURED               = "featured";
    public static final String   PARENTID               = "parentId";
    public static final String   PUBLISHERID            = "publisherId";
    public static final String   IMAGE_URL              = "imageUrl";
    public static final String   LOGO_IMAGE_FILE_NAME   = "bannerImageFileName";
    public static final String   BANNER_IMAGE_FILE_NAME = "logoImageFileName";

    public static final String[] FULL_PROJECTION        = new String[] {
            _ID, GROUPID, NAME, FEATURED, PUBLISHERID, PARENTID, IMAGE_URL , BANNER_IMAGE_FILE_NAME, LOGO_IMAGE_FILE_NAME
                                                        };

    public static ContentValues asValues(Group group) {
        ContentValues values = new ContentValues();

        if (group.getDbId() >= 0) {
            values.put(_ID, group.getDbId());
        }

        values.put(GROUPID, group.getGroupId());
        values.put(NAME, group.getName());
        setBoolean(values, FEATURED, group.isFeatured());
        values.put(PARENTID, group.getParentId());
        values.put(PUBLISHERID, group.getPublisherId());
        values.put(IMAGE_URL, group.getImageUrl());
        values.put(LOGO_IMAGE_FILE_NAME, group.getLogoImageFileName());
        values.put(BANNER_IMAGE_FILE_NAME, group.getBannerImageFileName());

        return values;
    }

    public static Group fromCursor(Cursor c) {
        Group group = new Group();

        group.setDbId(getLong(c, _ID));
        group.setGroupId(getString(c, GROUPID));
        group.setName(getString(c, NAME));
        group.setFeatured(getBoolean(c, FEATURED));
        group.setParentId(getString(c, PARENTID));
        group.setPublisherId(getString(c, PUBLISHERID));
        group.setImageUrl(getString(c, IMAGE_URL));
        group.setBannerImageFileName(getString(c, BANNER_IMAGE_FILE_NAME));
        group.setLogoImageFileName(getString(c, LOGO_IMAGE_FILE_NAME));

        return group;
    }
}
