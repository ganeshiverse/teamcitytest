package com.babylon.fragment.nutrient;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.database.schema.FoodSchema;

public class CaloriesConsumedCursorAdapter extends CursorAdapter {

    public CaloriesConsumedCursorAdapter(Activity activity, Cursor cursor) {
        super(activity, cursor, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder viewHolder = (ViewHolder) view.getTag();

        final String title = cursor.getString(FoodSchema.Query.TITLE);
        final String amount = String.valueOf(cursor.getInt(FoodSchema.Query.CALORIES)
                * cursor.getInt(FoodSchema.Query.COUNT));

        viewHolder.titleTextView.setText(title);
        viewHolder.amountTextView.setText(amount);

        if(cursor.isLast()) {
            viewHolder.dividerImageView.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.dividerImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = View.inflate(context, R.layout.list_item_calories_burnt, null);
        final ViewHolder holder = new ViewHolder();

        holder.titleTextView = (TextView) view.findViewById(R.id.list_item_calories_title_text_view);
        holder.amountTextView = (TextView) view.findViewById(R.id.list_item_calories_amount_text_view);
        holder.dividerImageView = view.findViewById(R.id.bottomDivider);

        view.setTag(holder);

        return view;
    }

    private static class ViewHolder {
        TextView titleTextView;
        TextView amountTextView;
        View dividerImageView;
    }

}
