package com.babylon.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.babylon.Keys;
import com.babylon.R;
import com.squareup.picasso.Picasso;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SelectImageManager {
    private static final String TAG = "SelectImageManager";
    public static final int GALLERY = 101;
    public static final int CAMERA = 102;
    public static final int GOOGLE = 103;
    public static final int FACEBOOK = 104;
    public static final int VKONTAKTE = 105;

    private static final int NONE = 0;
    private static final int HORIZONTAL = 1;
    private static final int VERTICAL = 2;
    private static final int[][] OPERATIONS = new int[][]{new int[]{0, NONE}, new int[]{0, HORIZONTAL},
            new int[]{180, NONE}, new int[]{180, VERTICAL}, new int[]{90, HORIZONTAL}, new int[]{90, NONE},
            new int[]{90, HORIZONTAL}, new int[]{-90, NONE},};
    public static final int ONE_MEGABYTE_IN_BYTES = 1024;

    private static SelectImageManager instance = null;
    private OnImageCompleteListener imageCompliteListener;
    private Activity activity;

    // Configuration
    private int maxSizeInPx = 1200;
    private long maxSizeInByte = 512 * ONE_MEGABYTE_IN_BYTES; // 512 kb
    private boolean checkScaleByByteSize = false;

    private Dialog selectImageDialog;

    public interface OnBitmapLoadListener {
        void onBitmapLoaded(Bitmap bitmap);
    }

    public static SelectImageManager getInstance() {
        if (instance == null) {
            instance = new SelectImageManager();
        }
        return instance;
    }

    public SelectImageManager() {
    }

    public void seMaxSize(int maxSixeInPx) {
        maxSizeInPx = maxSixeInPx;
    }

    public void seMaxSizeInByte(long maxSizeInByte) {
        this.maxSizeInByte = maxSizeInByte;
    }

    public void startGallery(Activity activity) {
        this.activity = activity;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("image/*");
        activity.startActivityForResult(i, GALLERY);
    }

    public void startCamera(Activity activity) {
        this.activity = activity;
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile()));
        activity.startActivityForResult(i, CAMERA);
    }

    public void showSelectImageDialog(final Activity activity, final boolean showRemoveButton,
                                      boolean showSocialButton, final OnImageCompleteListener imageCompliteListener) {
        this.activity = activity;
        this.imageCompliteListener = imageCompliteListener;

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.select_image_dialog, null);
        v.findViewById(R.id.gallery).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageDialog.dismiss();
                startGallery(activity);
            }
        });

        v.findViewById(R.id.camera).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageDialog.dismiss();
                startCamera(activity);
            }
        });

        v.findViewById(R.id.remove).setVisibility(showRemoveButton ? View.VISIBLE : View.GONE);
        v.findViewById(R.id.remove).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageDialog.dismiss();
                imageCompliteListener.onImageComplete(null);
            }
        });

        v.findViewById(R.id.cancel_btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageDialog.dismiss();
            }
        });

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(v);
        selectImageDialog = dialog;
        dialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent,
                                 OnImageCompleteListener onImageCompleteListener) {
        this.imageCompliteListener = onImageCompleteListener;
        onActivityResult(requestCode, resultCode, intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SelectImageManager.GALLERY) {
                loadBitmapFromGallery(intent, new OnBitmapLoadListener() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap) {
                        onImageLoadComplete(bitmap);
                    }
                });
            } else if (requestCode == SelectImageManager.CAMERA) {
                Bitmap bitmap = getBitmapFromCamera(intent);
                onImageLoadComplete(bitmap);
            }
        }
    }

    private void onImageLoadComplete(Bitmap bitmap) {
        if (imageCompliteListener != null) {
            imageCompliteListener.onImageComplete(bitmap);
        }
    }

    public void loadBitmapFromGallery(Intent intent, final OnBitmapLoadListener onBitmapLoadListener) {
        Bitmap image;
        Uri uri = intent.getData();

        try {
            if (Build.VERSION.SDK_INT < 19) {
                loadBitmapWithoutFileDescriptor(uri, onBitmapLoadListener);
            } else {
                ParcelFileDescriptor parcelFileDescriptor;
                if(activity.getContentResolver()!=null) {
                    parcelFileDescriptor = activity.getContentResolver().openFileDescriptor(uri, "r");
                    image = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                    parcelFileDescriptor.close();
                    image = prepareBitmap(image);
                    onGalleryBitmapLoaded(onBitmapLoadListener, image);
                }
            }
        } catch (FileNotFoundException e) {
            L.e(TAG, e.toString(), e);
            UIUtils.showSimpleToast(activity, R.string.error);
        } catch (IOException e) {
            L.e(TAG, e.toString(), e);
            UIUtils.showSimpleToast(activity, R.string.error);
        } catch (OutOfMemoryError e) {
            L.e(TAG, e.toString(), e);
            UIUtils.showSimpleToast(activity, R.string.error);
        }
    }

    private void loadBitmapWithoutFileDescriptor(Uri uri,
                                                 final OnBitmapLoadListener onBitmapLoadListener) {
        Bitmap image;
        String imagePath;

        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            imagePath = uri.getPath();
            image = prepareBitmap(new File(imagePath));
            onGalleryBitmapLoaded(onBitmapLoadListener, image);
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            if (idx != -1) {
                imagePath = cursor.getString(idx);
            } else {
                imagePath = uri.getLastPathSegment();
            }
            if (imagePath.startsWith("http")) {
                loadGalleryImageFromWeb(imagePath, onBitmapLoadListener);
            } else {
                image = prepareBitmap(new File(imagePath));
                onGalleryBitmapLoaded(onBitmapLoadListener, image);
            }
        }
    }

    private void loadGalleryImageFromWeb(String imagePath, final OnBitmapLoadListener onBitmapLoadListener) {
        Picasso.with(activity).load(imagePath).into(new com.squareup.picasso.Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap bitmapImage = prepareBitmap(bitmap);
                onGalleryBitmapLoaded(onBitmapLoadListener, bitmapImage);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    private void onGalleryBitmapLoaded(OnBitmapLoadListener onBitmapLoadListener,
                                       Bitmap bitmapImage) {
        if (onBitmapLoadListener != null) {
            onBitmapLoadListener.onBitmapLoaded(bitmapImage);
        }
    }

    public Bitmap getBitmapFromCamera(Intent intent) {
        File file = getTempFile();

        if (!file.exists()) {
            try {
                Uri u = intent.getData();
                InputStream is = activity.getContentResolver().openInputStream(u);
                FileOutputStream fos = new FileOutputStream(file, false);
                OutputStream os = new BufferedOutputStream(fos);
                byte[] buffer = new byte[ONE_MEGABYTE_IN_BYTES];
                int byteRead;
                while ((byteRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, byteRead);
                }
                try {
                    fos.close();
                } catch (IOException e) {
                    L.e(TAG, e.toString(), e);
                }
                try {
                    os.close();
                } catch (IOException e) {
                    L.e(TAG, e.toString(), e);
                }
                try {
                    is.close();
                } catch (IOException e) {
                    L.e(TAG, e.toString(), e);
                }
            } catch (Exception e) {
                L.e(TAG, e.toString(), e);
            }
        }

        return prepareBitmap(file);
    }

    public Bitmap getBitmapFromSocial(Intent intent) {
        return getCompressedBitmapByByteSize((Bitmap) intent.getExtras().get(Keys.BITMAP));
    }

    public Bitmap prepareBitmap(Bitmap bitmap) {
        Bitmap resultBitmap = getScaledBitmapByByteSize(bitmap);
        if (checkScaleByByteSize) {
            resultBitmap = getCompressedBitmapByByteSize(resultBitmap);
        }
        return resultBitmap;
    }

    public Bitmap prepareBitmap(File f) {
        Bitmap resultBitmap;

        resultBitmap = getScaledBitmapByPxSize(f);
        if (checkScaleByByteSize) {
            resultBitmap = getCompressedBitmapByByteSize(resultBitmap);
        }
        resultBitmap = rotateByExif(f.getAbsolutePath(), resultBitmap);

        return resultBitmap;
    }

    private Bitmap getScaledBitmapByPxSize(File f) {
        int scale = 1;
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(f.getAbsolutePath(), o);

            if (o.outHeight > maxSizeInPx || o.outWidth > maxSizeInPx) {
                double maxSourceSideSize = (double) Math.max(o.outHeight, o.outWidth);
                scale = (int) Math.pow(2,
                        (int) Math.round(Math.log(maxSizeInPx / maxSourceSideSize) / Math.log(0.5)));
            }

            L.d(TAG, "getScaledBitmapByPxSize Scale: " + scale);

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(f.getAbsolutePath(), o2);
        } catch (OutOfMemoryError e) {
            L.e(TAG, e.toString(), e);
            UIUtils.showSimpleToast(activity, R.string.error);
        }
        return null;
    }

    private Bitmap getScaledBitmapByByteSize(Bitmap sourceBitmap) {
        try {
            long sourceBitmapByte = sourceBitmap.getByteCount() / ONE_MEGABYTE_IN_BYTES;

            if (sourceBitmapByte > maxSizeInByte) {
                int scale = (int) Math.pow(2,
                        (int) Math.round(Math.log(maxSizeInByte / (double) sourceBitmapByte) / Math.log(0.5)));

                L.d(TAG, "getScaledBitmapByByteSize Scale: " + scale);

                int resultWidth = (int) (sourceBitmap.getWidth() / scale);
                int resultHeight = (int) (sourceBitmap.getHeight() / scale);

                return Bitmap.createScaledBitmap(sourceBitmap, resultWidth, resultHeight, false);
            }
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }
        return sourceBitmap;
    }

    private Bitmap getCompressedBitmapByByteSize(Bitmap sourceBitmap) {
        try {
            long sourceBitmapByte = BytesBitmap.getBytes(sourceBitmap).length;

            if (sourceBitmapByte > maxSizeInByte) {
                long x = 100 - 100 * maxSizeInByte / sourceBitmapByte;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                sourceBitmap.compress(Bitmap.CompressFormat.JPEG, (int) x, stream);
                return BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
            }
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        } catch (OutOfMemoryError e) {
            L.e(TAG, e.toString(), e);
            UIUtils.showSimpleToast(activity, R.string.error);
        }
        return sourceBitmap;
    }

    public byte[] getCompressedByByteSize(Bitmap sourceBitmap) {
        try {
            long sourceBitmapByte = BytesBitmap.getBytes(sourceBitmap).length;

            if (sourceBitmapByte > maxSizeInByte) {
                long x = 100 - 100 * maxSizeInByte / sourceBitmapByte;

                L.d("getCompressedByByteSize", String.valueOf(x));

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                sourceBitmap.compress(Bitmap.CompressFormat.JPEG, (int) x, stream);
                return stream.toByteArray();
            }
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }
        return BytesBitmap.getBytes(sourceBitmap);
    }

    public Bitmap rotateByExif(String imagePath, Bitmap bitmap) {
        try {
            ExifInterface oldExif = new ExifInterface(imagePath);
            int index = Integer.valueOf(oldExif.getAttribute(ExifInterface.TAG_ORIENTATION));
            int degrees = OPERATIONS[index][0];

            Matrix matrix = new Matrix();
            matrix.postRotate(degrees);

            if (degrees == 0) {
                return bitmap;
            } else {
                return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }

        return bitmap;

    }

    public File getTempFile() {
        File ret = null;
        try {
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                ret = new File(Environment.getExternalStorageDirectory(), "PostTmp.jpg");
            } else {
                File mydir = activity.getDir("mydir", Context.MODE_PRIVATE);
                ret = new File(mydir.getAbsolutePath() + "/", "PostTmp.jpg");
            }
        } catch (Exception e) {
            L.e(TAG, "Error get Temp File : " + e.toString(), e);
        }
        return ret;
    }

    private void deleteTempFile() {
        try {
            File file = getTempFile();
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }
    }

    public static interface OnImageCompleteListener {
        public void onImageComplete(Bitmap bitmap);
    }
}