package com.babylon.fragment.check;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.adapter.check.QuestionsAdapter;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.check.OutcomeSchema;
import com.babylon.database.schema.check.SymptomHashSchema;
import com.babylon.database.schema.check.SymptomSchema;
import com.babylon.enums.CheckGender;
import com.babylon.model.check.CheckResult;
import com.babylon.model.check.SymptomAnswer;
import com.babylon.model.check.ViewState;
import com.babylon.model.check.export.Outcome;
import com.babylon.model.check.export.Symptom;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.L;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.babylon.database.schema.check.SymptomSchema.SymptomQuery;

public abstract class QuestionsFragment extends BaseCheckFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener, View.OnClickListener {

    private static final String KEY_GENDER = "KEY_GENDER";
    private static final String KEY_BODY_PART_ID = "KEY_BODY_PART_ID";
    private static final String KEY_PARENT_ID = "KEY_PARENT_ID";
    private static final String KEY_TITLE = "KEY_TITLE";

    protected static final String KEY_SYMPTOMS_HASHES = "KEY_SYMPTOMS_HASHES";

    private static final int SYMPTOMS_LOADER = 1011;
    protected static final int OUTCOME_LOADER = 1017;

    private CheckGender gender;
    private int bodyPartId;
    private String title;

    private ListView list;

    private QuestionsAdapter questionsAdapter;
    protected List<SymptomViewItem> symptomsStack;
    protected ArrayList<SymptomAnswer> symptomAnswers;
    private Button goToOutcomeBtn;

    protected void putArgs(CheckGender gender, int bodyPartId, String title) {
        Bundle args = new Bundle();

        args.putSerializable(KEY_GENDER, gender);
        args.putInt(KEY_BODY_PART_ID, bodyPartId);
        args.putString(KEY_TITLE, title);

        setArguments(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            gender = (CheckGender) getArguments().getSerializable(KEY_GENDER);
            bodyPartId = getArguments().getInt(KEY_BODY_PART_ID);
            title = getArguments().getString(KEY_TITLE);
        }
        symptomsStack = new ArrayList<SymptomViewItem>();
        questionsAdapter = new QuestionsAdapter(getActivity(), symptomsStack);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    protected void init() {
        if (symptomsStack != null && symptomsStack.isEmpty()) {
            // Stub root symptom
            Symptom rootSymptom = new Symptom();
            rootSymptom.setBodyPartId(bodyPartId);
            rootSymptom.setGender(gender.checkValue);
            rootSymptom.setParentId(0);

            startGettingChildSymptoms(rootSymptom);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_questions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View footer = LayoutInflater.from(getActivity()).inflate(R.layout.triage_symptoms_list_footer, list, false);
        goToOutcomeBtn = (Button) footer.findViewById(R.id.go_to_outcome);
        goToOutcomeBtn.setOnClickListener(this);
        goToOutcomeBtn.setVisibility(View.INVISIBLE);
        list = (ListView) view.findViewById(R.id.list_view);
        list.addFooterView(footer);
        list.setAdapter(questionsAdapter);
        list.setOnItemClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!TextUtils.isEmpty(title)) {
            ((BaseActivity) getActivity()).setTitle(title);
        }
    }

    @Override
    public boolean isBackAllowed() {
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Loaders work
    ///////////////////////////////////////////////////////////////////////////

    @Nullable
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        CursorLoader loader = null;
        switch (i) {
            case SYMPTOMS_LOADER:
                loader = getSymptomsLoader(bundle);
                break;
            case OUTCOME_LOADER:
                loader = getOutcomeLoader(bundle);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (cursor != null) {
            switch (cursorLoader.getId()) {
                case SYMPTOMS_LOADER:
                    processSymptomsCursor(cursor);
                    break;
                case OUTCOME_LOADER:
                    processOutcomeCursor(cursor);
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
    }

    private CursorLoader getSymptomsLoader(Bundle bundle) {
        String[] selectionArgs = new String[]{
                bundle.getString(KEY_GENDER),
                bundle.getString(KEY_PARENT_ID),
                bundle.getString(KEY_BODY_PART_ID)
        };
        return new CursorLoader(getActivity(),
                SymptomSchema.CONTENT_URI_WITH_CHILDREN_COUNT,
                SymptomQuery.PROJECTION,
                SymptomQuery.SELECTION_WITH_BODY_PART,
                selectionArgs,
                SymptomQuery.SORT_ASC);
    }

    public String joinHashes(CharSequence delimiter, List<String> tokens) {
        StringBuilder sb = new StringBuilder();
        boolean firstTime = true;
        for (String token : tokens) {
            if (firstTime) {
                firstTime = false;
            } else {
                sb.append(delimiter);
            }
            sb.append("'" + token + "'");
        }
        return sb.toString();
    }

    protected CursorLoader getOutcomeLoader(Bundle bundle) {
        ArrayList<String> symptomHashes = bundle.getStringArrayList(KEY_SYMPTOMS_HASHES);
        String selection = SymptomHashSchema.Columns.HASH +
                " IN (" + joinHashes(",", symptomHashes) + ")";
        return new CursorLoader(getActivity(), OutcomeSchema.HASH_BASED_OUTCOME_URI,
                OutcomeSchema.Query.PROJECTION,
                selection,
                null,
                OutcomeSchema.Query.SORT_ID_ASC);
    }

    private void processSymptomsCursor(Cursor cursor) {
        List<SymptomViewItem> symptoms = new ArrayList<SymptomViewItem>();
        while (cursor.moveToNext()) {
            SymptomViewItem symptom = new SymptomViewItem();
            symptom.createFromCursor(cursor);
            symptoms.add(symptom);
        }
        updateSymptomsItemsStack(symptoms);
    }

    protected void processOutcomeCursor(Cursor cursor) {
        if (cursor.moveToFirst()) {
            final Outcome outcome = new Outcome();
            outcome.createFromCursor(cursor);
            if (outcome.getBodyPartId() > 0) {//its means that we should redirect user to additional questions
                Symptom rootSymptom = new Symptom();
                rootSymptom.setBodyPartId(outcome.getBodyPartId());
                rootSymptom.setGender(outcome.getGender());
                rootSymptom.setParentId(0);

                symptomsStack.clear();
                startGettingChildSymptoms(rootSymptom);
            } else {
                getLoaderManager().destroyLoader(OUTCOME_LOADER);
                storeCheckResult(outcome.getId());
                showOutcomeFragment(outcome);
            }
        }
    }

    private void storeCheckResult(int outcomeId) {
        if (SyncUtils.isLoggedIn()) {
            CheckResult checkResult = new CheckResult(bodyPartId, outcomeId, symptomAnswers);
            checkResult.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
            DatabaseOperationUtils.getInstance()
                    .insertSymptomAnswers(
                            checkResult,
                            getActivity().getContentResolver());
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Base logic
    ///////////////////////////////////////////////////////////////////////////

    private void startGettingChildSymptoms(Symptom symptom) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_GENDER, String.valueOf(symptom.getGender()));
        bundle.putString(KEY_PARENT_ID, String.valueOf(symptom.getId()));
        bundle.putString(KEY_BODY_PART_ID, String.valueOf(symptom.getBodyPartId()));

        getLoaderManager().restartLoader(SYMPTOMS_LOADER, bundle, this);
    }

    private void updateSymptomsItemsStack(@NonNull List<SymptomViewItem> symptoms) {
        if (symptoms.size() > 0) {
            int symptomsParentId = symptoms.get(0).getParentId();
            SymptomViewItem symptomViewItem = findItemBySelectedSymptomId(symptomsParentId);
            int parentViewModelPosition = symptomsStack.indexOf(symptomViewItem);
            addItemsList(parentViewModelPosition, symptoms);
        }
        questionsAdapter.notifyDataSetChanged();
        goToOutcomeBtn.setVisibility(View.VISIBLE);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Dialog
    ///////////////////////////////////////////////////////////////////////////

    private void showSymptomChildrenDialog(SymptomViewItem symptom) {
        ChildSymptomDialogFragment childSymptomDialogFragment =
                ChildSymptomDialogFragment.newInstance(gender, symptom.getName(), symptom.getId());
        childSymptomDialogFragment.setTargetFragment(this, -1);
        childSymptomDialogFragment.show(getChildFragmentManager(), null);
    }

    public void onChildSymptomSelected(int symptomViewModelId, Symptom symptom) {
        SymptomViewItem parentSymptomViewItem = findItemById(symptomViewModelId);
        if (parentSymptomViewItem != null) {
            updateItem(parentSymptomViewItem, symptom);
        }
        startGettingChildSymptoms(symptom);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Stack operations
    ///////////////////////////////////////////////////////////////////////////

    private void addItemsList(int position, @NonNull List<SymptomViewItem> symptoms) {
        if (position == -1) {
            symptomsStack.addAll(symptoms);
        } else {
            symptomsStack.addAll(++position, symptoms);
        }
    }

    private void removeItemsList(@NonNull List<SymptomViewItem> items) {
        symptomsStack.removeAll(items);
    }

    private void updateItem(@NonNull SymptomViewItem symptomViewItem, @NonNull Symptom symptom) {
        List<SymptomViewItem> dependsViewModels = findAllDependsItems(symptomViewItem);
        if (dependsViewModels != null) {
            removeItemsList(dependsViewModels);
        }

        symptomViewItem.setSelectedSymptom(symptom);
        symptomViewItem.setAnswerState(ViewState.YES);
    }

    private SymptomViewItem findItemById(int id) {
        SymptomViewItem parentSymptomViewItem = null;
        for (SymptomViewItem symptomViewItem : symptomsStack) {
            if (symptomViewItem.getId() == id) {
                parentSymptomViewItem = symptomViewItem;
                break;
            }
        }
        return parentSymptomViewItem;
    }

    /**
     * Find SymptomItemView where selectedSymptom has id
     *
     * @param id selectedSymptom id
     * @return symptomViewItem or null in case item was not found
     */
    private SymptomViewItem findItemBySelectedSymptomId(int id) {
        SymptomViewItem foundSymptomViewItem = null;
        for (SymptomViewItem symptomViewItem : symptomsStack) {
            Symptom selectedSymptom = symptomViewItem.getSelectedSymptom();
            if (selectedSymptom != null && selectedSymptom.getId() == id) {
                foundSymptomViewItem = symptomViewItem;
                break;
            }
        }
        return foundSymptomViewItem;
    }

    @Nullable
    private List<SymptomViewItem> findAllDependsItems(@NonNull SymptomViewItem parentSymptomViewItem) {
        Symptom selectedSymptom = parentSymptomViewItem.getSelectedSymptom();
        if (selectedSymptom != null) {
            return findAllChildItemsInTree(selectedSymptom.getId());
        }
        return null;
    }

    private List<SymptomViewItem> findAllChildItemsInTree(int parentId) {
        List<SymptomViewItem> symptoms = new ArrayList<SymptomViewItem>();
        for (SymptomViewItem symptomViewItem : symptomsStack) {
            if (parentId == symptomViewItem.getParentId()) {
                symptoms.add(symptomViewItem); //found child symptom
                Symptom selectedSymptom = symptomViewItem.getSelectedSymptom();
                if (selectedSymptom != null) {
                    //try to find all children of previously founded child
                    symptoms.addAll(findAllChildItemsInTree(selectedSymptom.getId()));
                }
            }
        }
        return symptoms;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final SymptomViewItem selectedSymptom = (SymptomViewItem) parent.getAdapter().getItem(position);
        showSymptomChildrenDialog(selectedSymptom);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.go_to_outcome) {
            startGettingOutcome();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Outcome
    ///////////////////////////////////////////////////////////////////////////

    private void updateSymptomAnswers(List<SymptomViewItem> answers) {
        symptomAnswers = new ArrayList<SymptomAnswer>();
        for (SymptomViewItem symptomViewItem : answers) {
            if (!symptomViewItem.isParentSymptom()) {
                int id = symptomViewItem.getId();
                int parentId = symptomViewItem.getParentId();
                ViewState answerState = symptomViewItem.getAnswerState();
                Symptom answerSymptom = symptomViewItem.getSelectedSymptom();
                if (answerSymptom != null) {
                    id = answerSymptom.getId();
                }
                symptomAnswers.add(new SymptomAnswer(id, answerState, parentId));
            }
        }
        Collections.sort(symptomAnswers);
    }

    private void startGettingOutcome() {
        updateSymptomAnswers(symptomsStack);
        if (!hasUnselectedAnswers(symptomAnswers)) {
            ArrayList<String> symptomsHash = getHashesList(symptomAnswers);
            L.d(QuestionsFragment.class.getSimpleName(), "Hashes for outcome: "
                    + symptomsHash.toString());

            Bundle bundle = new Bundle();
            bundle.putStringArrayList(KEY_SYMPTOMS_HASHES, symptomsHash);
            getLoaderManager().restartLoader(OUTCOME_LOADER, bundle, this);
        } else {
            notifyAboutUnselectedAnswers();
        }
    }

    protected abstract ArrayList<String> getHashesList(ArrayList<SymptomAnswer> symptomAnswers);

    protected abstract boolean hasUnselectedAnswers(ArrayList<SymptomAnswer> symptomAnswers);

    protected abstract void notifyAboutUnselectedAnswers();

    private void showOutcomeFragment(final Outcome outcome) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity) getActivity()).
                        startFragment(OutcomeFragment.newInstance(outcome), true,
                                OutcomeFragment.OUTCOME_FRAGMENT_TAG,
                                R.id.main_activity_fragment_container_layout);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // Models
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Model which describe symptom view.
     * <p/>
     * Contains:
     * <br>
     * <li>answerState - set by question view;<br>
     * <li>selectedSymptom - set by symptoms dialog<br>
     * <li>isParentSymptom - true when symptom use for redirect to child symptoms
     * and shouldn't be included in hash for getting outcome
     * <br>
     * Other fields are the same as in Symptom model
     * <p/>
     */
    public static class SymptomViewItem extends Symptom {
        @SkipFieldInContentValues
        private ViewState answerState = ViewState.UNSELECTED;
        @SkipFieldInContentValues
        private Symptom selectedSymptom;

        public ViewState getAnswerState() {
            return answerState;
        }

        public void setAnswerState(ViewState answerState) {
            this.answerState = answerState;
        }

        public void setSelectedSymptom(Symptom selectedSymptom) {
            this.selectedSymptom = selectedSymptom;
        }

        public Symptom getSelectedSymptom() {
            return selectedSymptom;
        }

        public boolean isParentSymptom() {
            if (selectedSymptom != null) {
                return selectedSymptom.getChildrenCount() > 0;
            }
            return false;
        }
    }
}
