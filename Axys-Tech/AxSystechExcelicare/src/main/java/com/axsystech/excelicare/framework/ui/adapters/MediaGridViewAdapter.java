package com.axsystech.excelicare.framework.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.BaseActivity;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.activities.MediaDetailsActivity;
import com.axsystech.excelicare.framework.ui.fragments.media.FilterType;
import com.axsystech.excelicare.framework.ui.fragments.media.MediaFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.models.MediaDetailsViewType;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.loaders.GetMediaItemLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by SomeswarReddy on 7/8/2015.
 */
public class MediaGridViewAdapter extends BaseAdapter {

    private MainContentActivity mActivity;
    private FilterType mSelectedFilterType;
    private ArrayList<MediaItemDbModel> mediaItems;
    private LayoutInflater inflater;

    private User mActiveUser;

    private boolean shouldDisplayLocalMedia = true;

    public MediaGridViewAdapter(MainContentActivity context, FilterType selectedFilterType, ArrayList<MediaItemDbModel> mediaItems, User loginUser,
                                boolean shouldDisplayLocalMedia) {
        this.mActivity = context;
        this.mSelectedFilterType = selectedFilterType;
        this.mediaItems = mediaItems;
        this.mActiveUser = loginUser;
        this.shouldDisplayLocalMedia = shouldDisplayLocalMedia;

        inflater = (LayoutInflater) this.mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void updateGridData(FilterType selectedFilterType, ArrayList<MediaItemDbModel> mediaItems, User loginUser) {
        this.mSelectedFilterType = selectedFilterType;
        this.mediaItems = mediaItems;
        this.mActiveUser = loginUser;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (mediaItems != null && mediaItems.size() > 0 ? mediaItems.size() : 0);
    }

    @Override
    public MediaItemDbModel getItem(int position) {
        return mediaItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final MediaItemDbModel mediaItemDbModel = getItem(position);

        if (null == convertView) {
            convertView = inflater.inflate(R.layout.media_child_view, viewGroup, false);
        }

        Typeface regular = Typeface.createFromAsset(mActivity.getAssets(), "fonts/if_std_reg.ttf");

        ImageView mediaImageView = (ImageView) convertView.findViewById(R.id.mediaImageView);
        ImageView playIconImageView = (ImageView) convertView.findViewById(R.id.playIconImageView);
        TextView mediaDurationTextView = (TextView) convertView.findViewById(R.id.mediaDurationtextView);
        mediaDurationTextView.setTypeface(regular);
        ImageView mediaSyncStatusImageView = (ImageView) convertView.findViewById(R.id.syncStatusImageView);
        TextView mediaNameTextView = (TextView) convertView.findViewById(R.id.mediaNametextView);
        mediaNameTextView.setTypeface(regular);

        FrameLayout progressbarContainer = (FrameLayout) convertView.findViewById(R.id.progressbarContainer);
        final FrameLayout selectionContainer = (FrameLayout) convertView.findViewById(R.id.selectionContainer);

        if (mediaItemDbModel != null) {
            selectionContainer.setVisibility(mediaItemDbModel.isSelected() ? View.VISIBLE : View.GONE);

            if(mediaItemDbModel.isUploading()) {
                progressbarContainer.setVisibility(View.VISIBLE);
            } else {
                progressbarContainer.setVisibility(View.GONE);
            }

            if (mediaItemDbModel.getMediaType() == FilterType.FILTER_PHOTOS) {
                playIconImageView.setVisibility(View.GONE);
                mediaDurationTextView.setVisibility(View.GONE);
            }

            if (mediaItemDbModel.getMediaType() == FilterType.FILTER_AUDIOS) {
                playIconImageView.setImageResource(R.drawable.play_hdpi);
            }

            mediaSyncStatusImageView.setImageResource(mediaItemDbModel.isUploaded() ? R.drawable.photouploaded : R.drawable.photonotselected);

            mediaNameTextView.setText(mediaItemDbModel.getMediaName());

            if (mediaItemDbModel.getMediaType() == FilterType.FILTER_PHOTOS) {
                if (mediaItemDbModel.getPreviewMedia() != null && !TextUtils.isEmpty(mediaItemDbModel.getPreviewMedia())) {
                    String imagePath = mediaItemDbModel.getPreviewMedia();
                    if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                        imagePath = "file://" + imagePath;

                        // get rid of invalid image url like below
                        // http://115.119.121.38:8989/ECService_PHRV64VL_DEV/AxServerTemp//5295//test.jpg_150x150
                    }

                    Picasso.with(mActivity)
                            .load(imagePath)
                            .placeholder(R.drawable.default_media)
                            .error(R.drawable.default_media)
                            .into(mediaImageView);
                }
            } else if (mediaItemDbModel.getMediaType() == FilterType.FILTER_VIDEOS) {
                if (mediaItemDbModel.getPreviewMedia() != null && mediaItemDbModel.getPreviewMedia() != null) {
                    String videoPath = mediaItemDbModel.getPreviewMedia();
                    if (!videoPath.startsWith("file://") && !videoPath.startsWith("http")) {
                        videoPath = "file://" + videoPath;
                    }
//                    String imagePath = mediaItemDbModel.getPreviewMedia();
//                    if(!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
//                        Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(mediaItemDbModel.getPreviewMedia(), MediaStore.Video.Thumbnails.MICRO_KIND);
//                        mediaImageView.setImageBitmap(bmThumbnail);
//                    } else {
                        Picasso.with(mActivity)
                            .load(videoPath)
                            .placeholder(R.drawable.default_media)
                            .error(R.drawable.default_media)
                            .into(mediaImageView);
//                    }
                }
            } else if (mediaItemDbModel.getMediaType() == FilterType.FILTER_AUDIOS) {
                mediaImageView.setImageResource(R.drawable.musicbg);
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shouldDisplayLocalMedia) {
                    // if user navigate here from App menu list, navigate to media details screen
                    if (!GlobalDataModel.getInstance().isMultipleSelectionEnabled()) {
                        // if multiple selection not enabled, navigate to details page
                        if (mediaItemDbModel.getMediaId() > 0 && mediaItemDbModel.isUploaded()) {
                    /*
                     means selected media already uploaded to server, so get the media details from server if online.
                     Otherwise
                      */
                            if (CommonUtils.hasInternet()) {
                                mActivity.displayProgressLoader(false);
                                GetMediaItemLoader getMediaItemLoader = new GetMediaItemLoader(mActivity, mediaItemDbModel.getMediaId(), mActiveUser.getToken());
                                if (!mActivity.isFinishing()) {
                                    mActivity.getLoaderHelper().initAsyncLoader(mActivity.getLoaderHelper().getLoaderId(AxSysConstants.GET_MEDIA_ITEM_LOADER), getMediaItemLoader);
                                }
                            } else {
                                // Display media content cached in DB
                                Intent viewIntent = new Intent(mActivity, MediaDetailsActivity.class);
                                viewIntent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_OBJ, mediaItemDbModel);
                                viewIntent.putExtra(AxSysConstants.EXTRA_MEDIA_DETAILS_VIEWTYPE, MediaDetailsViewType.VIEW);
                                mActivity.startActivityForResult(viewIntent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);
                            }
                        } else {
                            // Display media content cached in DB
                            Intent viewIntent = new Intent(mActivity, MediaDetailsActivity.class);
                            viewIntent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_OBJ, mediaItemDbModel);
                            viewIntent.putExtra(AxSysConstants.EXTRA_MEDIA_DETAILS_VIEWTYPE, MediaDetailsViewType.VIEW);
                            mActivity.startActivityForResult(viewIntent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);
                        }
                    } else {
                        // if multiple selection enabled, add selected media to ArrayList for uploading / deleting
                        mediaItemDbModel.setSelected(!mediaItemDbModel.isSelected());

                        selectionContainer.setVisibility(mediaItemDbModel.isSelected() ? View.VISIBLE : View.GONE);
                    }
                } else {
                    if (GlobalDataModel.getInstance().isMultipleSelectionEnabled()) {
                        // if user navigated here from message compose screen, just toggle the selection view
                        mediaItemDbModel.setSelected(!mediaItemDbModel.isSelected());

                        selectionContainer.setVisibility(mediaItemDbModel.isSelected() ? View.VISIBLE : View.GONE);
                    }
                }
            }
        });

        return convertView;
    }

}
