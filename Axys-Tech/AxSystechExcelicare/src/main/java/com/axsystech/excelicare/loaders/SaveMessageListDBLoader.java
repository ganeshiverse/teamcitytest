package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/9/2015.
 */
public class SaveMessageListDBLoader extends DataAsyncTaskLibLoader<Boolean> {

    private String TAG = SaveMessageListDBLoader.class.getSimpleName();
    private ArrayList<MessagesInboxAndSentResponse.DataObject> msgDataObjectsAL;
    private String panelId;
    private String siteId;
    private long ecUserId;

    public SaveMessageListDBLoader(final Context context, final ArrayList<MessagesInboxAndSentResponse.DataObject> msgDataObjectsAL,
                                   String panelId, String siteId, long ecUserId) {
        super(context, true);
        this.msgDataObjectsAL = msgDataObjectsAL;
        this.panelId = panelId;
        this.siteId = siteId;
        this.ecUserId = ecUserId;
    }

    @Override
    protected Boolean performLoad() throws Exception {
        if (msgDataObjectsAL == null)
            throw new SQLException("Unable to cache message list details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (msgDataObjectsAL != null && msgDataObjectsAL.size() > 0) {
                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {

                        for (final MessagesInboxAndSentResponse.DataObject responseModel : msgDataObjectsAL) {
                            final Dao<MessageListItemDBModel, Long> messageListItemDBModelDao = helper.getDao(MessageListItemDBModel.class);

                            MessageListItemDBModel dbModel = ConverterResponseToDbModel.getMessageListItemDbModel(responseModel, panelId, siteId, ecUserId);

                            messageListItemDBModelDao.createOrUpdate(dbModel);
                        }

                        return true;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return true;
    }

}
