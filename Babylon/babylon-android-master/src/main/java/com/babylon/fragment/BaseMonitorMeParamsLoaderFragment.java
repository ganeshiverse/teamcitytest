package com.babylon.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.babylon.App;
import com.babylon.R;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.BaseModel;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;

public abstract class BaseMonitorMeParamsLoaderFragment extends BaseMonitorMeParamsFragment {
    private boolean isProgressNeeded = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (App.getInstance().isConnection()) {
            final long dateFrom = ChartFragment.getMonthFrom(dateInMillis);
            onStartLoadingMonitorMeParamsFromServer(dateFrom);
            isProgressNeeded = true;
        } else {
            isProgressNeeded = false;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setProgressBarVisible(isProgressNeeded);

        if (isProgressNeeded) {
            isProgressNeeded = false;
            setInputViewsEnable((ViewGroup) getView(), false);
        }

        startLoaderManager();
    }

    public void showNotZeroValue(EditText editText, MonitorMeParam monitorMeParam, DailyFeatureMap dailyFeatureMap) {
        final MonitorMeFeatures.Feature feature = dailyFeatureMap
                .getFeature(monitorMeParam);
        final String value = feature.getValue();

        final String valueEditText = editText.getText().toString();

        String result = TextUtils.isEmpty(valueEditText) ? "" : valueEditText;

        if (!TextUtils.isEmpty(value) && Float.valueOf(value) != 0) {
            result = value;
        }

        if (!valueEditText.equals(result)) {
            editText.setText(result);
        }
    }

    public static void putMonitorMeParam(MonitorMeParam monitorMeParam, EditText editText,
                                         DailyFeatureMap dailyFeatureMap) {
        final String value = String.valueOf(BaseModel.getFloat(editText.getText().toString()));
        dailyFeatureMap.setValue(monitorMeParam, value);
    }

    public static void putMonitorMeParamNotZero(MonitorMeParam monitorMeParam, EditText editText,
                                                DailyFeatureMap dailyFeatureMap) {
        final Float value = BaseModel.getFloat(editText.getText().toString());
        if (value.compareTo(0f) != 0) {
            putMonitorMeParam(monitorMeParam, editText, dailyFeatureMap);
        }
    }

    protected void onLoadedMonitorMeParamsFromServer() {
        if (getView() != null) {
            setInputViewsEnable((ViewGroup) getView(), true);
        }
    }

    private void setInputViewsEnable(ViewGroup parent, boolean isEnable) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup && child.getId() != R.id.topBar) {
                setInputViewsEnable((ViewGroup) child, isEnable);
            } else {
                if (child != null && (child instanceof EditText || child instanceof ImageButton || child instanceof
                        Button)) {
                    child.setEnabled(isEnable);
                }
            }
        }
    }

    protected abstract void onStartLoadingMonitorMeParamsFromServer(long dateFrom);

    protected abstract void startLoaderManager();

}