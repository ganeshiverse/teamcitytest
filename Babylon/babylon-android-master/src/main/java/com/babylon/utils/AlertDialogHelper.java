package com.babylon.utils;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.adapter.ConsultantAdapter;
import com.babylon.adapter.SpecialistAdapter;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.model.Consultant;
import com.babylon.model.Specialist;

import com.babylonpartners.babylon.HomePageBackboneActivity;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * Shows alert dialogs from any application source location
 */
public class AlertDialogHelper {

    public static final String TAG = AlertDialogHelper.class.getSimpleName();

    private final static int MINUTES_MAX = 59;
    private final static int HOURS_MAX = 23;

    private final static int KILOMETERS_MAX = 99;
    private final static int METERS_MAX = 999;
    private final static int METER_STEP = 50;

    private static AlertDialogHelper instance;
    private AlertDialog alertDialog;

    private AlertDialogHelper() {
        //singleton
    }

    public static AlertDialogHelper getInstance() {
        if (instance == null) {
            instance = new AlertDialogHelper();
        }
        return instance;
    }

    public void showMessage(Context context, int messageId, DialogInterface.OnDismissListener onDismissListener) {
        showMessage(context, null, context.getString(messageId), onDismissListener);
    }

    public void showMessage(Context context, String message, DialogInterface.OnDismissListener onDismissListener) {
        showMessage(context, null, message, onDismissListener);
    }

    public void showMessage(Context context, int messageId) {
        showMessage(context, null, context.getString(messageId), null);
    }

    public void showMessage(Context context, String message) {
        showMessage(context, null, message, null);
    }

    public void showMessage(Context context, int titleId, int messageId) {
        showMessage(context, context.getString(titleId), context.getString(messageId), null);
    }

    public void showMessage(Context context, String title, String message, DialogInterface.OnDismissListener
            onDismissListener) {
        try {
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            L.e(TAG, e.getMessage(), e);
        }
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(context);

        if (!TextUtils.isEmpty(title)) {
            alertDialogBuilder.setTitle(title);
        }

        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(context.getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setOnDismissListener(onDismissListener);
        alertDialog.show();
    }

    public void showConsultantAlert(Context context, JSONArray consultantJsonArray,
                                    final HomePageBackboneActivity.OnConsultantChoiceListener onConsultantChoiceListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.choose_consultant));
        View view = View.inflate(context, R.layout.view_list_view, null);
        ListView listView = (ListView) view.findViewById(R.id.list_view);
        builder.setView(listView);
        try {
            final List<Consultant> consultantList = getConsultantList(consultantJsonArray);
            builder.setNegativeButton(context.getString(android.R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            ConsultantAdapter consultantAdapter = new ConsultantAdapter(context, consultantList);
            listView.setAdapter(consultantAdapter);

            final AlertDialog consultAlertDialog = builder.create();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Consultant consultant = consultantList.get(position);
                    if (onConsultantChoiceListener != null) {
                        onConsultantChoiceListener.onConsultantChoiceClick(consultant.getId());
                        consultAlertDialog.dismiss();
                    }
                }
            });
            consultAlertDialog.show();
        } catch (JSONException e) {
            L.e(TAG, e.toString(), e);
        }
    }

    public void showSpecialistAlert(Context context, JSONArray specialistJsonArray,
                                    final HomePageBackboneActivity.OnSpecialistChoiceListener onSpecialistChoiceListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.choose_consultant));
        View view = View.inflate(context, R.layout.view_list_view, null);
        ListView listView = (ListView) view.findViewById(R.id.list_view);
        builder.setView(listView);
        try {
            final List<Specialist> specialistList = getSpecialList(specialistJsonArray);
            builder.setNegativeButton(context.getString(android.R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            SpecialistAdapter specialistAdapter = new SpecialistAdapter(context, specialistList);
            listView.setAdapter(specialistAdapter);

            final AlertDialog specialistAlertDialog = builder.create();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Specialist specialist = specialistList.get(position);
                    if (!specialist.isCategory()
                            && onSpecialistChoiceListener != null) {
                        onSpecialistChoiceListener.onSpecialistChoiceClick(specialist.getId());
                        specialistAlertDialog.dismiss();
                    }
                }
            });
            specialistAlertDialog.show();
        } catch (JSONException e) {
            L.e(TAG, e.toString(), e);
        }
    }

    private List<Consultant> getConsultantList(JSONArray consultantJsonArray) throws JSONException {
        final List<Consultant> consultantList = new ArrayList<Consultant>();
        for (int index = 0; index < consultantJsonArray.length(); ++index) {
            Consultant consultant = GsonWrapper.getGson().fromJson(
                    consultantJsonArray.getJSONObject(index).toString(), Consultant.class);
            if (consultant != null) {
                consultantList.add(consultant);
            }
        }
        return consultantList;
    }

    private List<Specialist> getSpecialList(JSONArray specialistJsonArray) throws JSONException {
        final List<Specialist> specialistList = new ArrayList<Specialist>();
        List<String> alreadyExistCategories = new LinkedList<String>();

        for (int index = 0; index < specialistJsonArray.length(); index++) {
            Specialist specialist = GsonWrapper.getGson().fromJson(
                    specialistJsonArray.getJSONObject(index).toString(), Specialist.class);
            if (specialist != null) {
                boolean isCategoryAlreadyExist = false;
                for (String category : alreadyExistCategories) {
                    String categoryName = specialist.getSpecialism();
                    if (!TextUtils.isEmpty(categoryName)
                            && category.equals(specialist.getSpecialism())) {
                        isCategoryAlreadyExist = true;
                    }
                }
                if (!isCategoryAlreadyExist) {
                    alreadyExistCategories.add(specialist.getSpecialism());
                    specialist.setCategory(true);
                    index--;
                } else {
                    specialist.setCategory(false);
                }
                specialistList.add(specialist);
            }
        }

        return specialistList;
    }

    public interface ConfirmationDialogListener {
        public void confirm();
        public void cancel();
    }


    public void showSimpleConfirmationDialog(Context context, String title, String message, final ConfirmationDialogListener listener, String negativeButtonText, String positiveButtonText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        if (title != null)
            alertDialogBuilder.setTitle(title);

        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (listener != null)
                    listener.cancel();
            }
        });
        alertDialogBuilder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (listener != null)
                    listener.confirm();
            }
        });
        AlertDialog confirmationAlertDialog = alertDialogBuilder.create();
        confirmationAlertDialog.show();
    }

    public void showDefaultSimpleConfirmationDialog(Context context, String message, final ConfirmationDialogListener listener) {
        showSimpleConfirmationDialog(context, null, message, listener, context.getString(R.string.cancel), context.getString(R.string.confirm));
    }

    public void showGpConsentDialog(Context context, final ConfirmationDialogListener listener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setMessage(context.getString(R.string.gp_consent_dialog));

        alertDialogBuilder.setNegativeButton(R.string.gp_consent_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.cancel();
            }
        });
        alertDialogBuilder.setPositiveButton(R.string.gp_consent_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.confirm();
            }
        });
        AlertDialog confirmationAlertDialog = alertDialogBuilder.create();
        confirmationAlertDialog.show();
    }

    public void showConfirmationDialog(Context context, String message, String yesText,
                                       String  noText, final ConfirmationDialogListener listener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setNegativeButton(noText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.cancel();
            }
        });
        alertDialogBuilder.setPositiveButton(yesText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.confirm();
            }
        });
        AlertDialog confirmationAlertDialog = alertDialogBuilder.create();
        confirmationAlertDialog.show();
    }

    public void showAgreeDialog(Context context, String message, int okTextId, int cancelTextId,
                                DialogInterface.OnClickListener agreeOnClickListener) {

        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(context);

        if (!TextUtils.isEmpty(message)) {
            alertDialogBuilder.setMessage(message);
        }
        alertDialogBuilder.setNegativeButton(context.getString(cancelTextId), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setPositiveButton(context.getString(okTextId), agreeOnClickListener);
        AlertDialog agreeAlertDialog = alertDialogBuilder.create();
        agreeAlertDialog.show();
    }

    public void showAddContactDialog(final Context context, boolean isShowForgetCheckBox) {
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(context);
        View checkBoxView = View.inflate(context, R.layout.dialog_add_contact, null);
        final CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.dialog_add_contact_checkbox);
        final TextView textView = (TextView) checkBoxView.findViewById(R.id.dialog_add_contact_text_view);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setChecked(!checkBox.isChecked());
            }
        });

        alertDialogBuilder.setTitle(R.string.app_name);
        alertDialogBuilder.setMessage(R.string.contact_add_notification_question);
        alertDialogBuilder.setNegativeButton(R.string.contact_add_notification_not_add,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        PreferencesManager.getInstance().setShowContactAlert(!checkBox.isChecked());
                    }
                });
        alertDialogBuilder.setPositiveButton(R.string.contact_add_notification_add,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseOperationUtils.getInstance().addBabylonContact(context.getContentResolver());
                        PreferencesManager.getInstance().setShowContactAlert(false);
                        dialog.dismiss();
                    }
                });
        if (isShowForgetCheckBox) {
            alertDialogBuilder.setView(checkBoxView);
        }
        AlertDialog addContactAlertDialog = alertDialogBuilder.create();
        addContactAlertDialog.show();
    }

    public void showDatePicker(Context context, DatePickerDialog.OnDateSetListener onDateSetListener, Date date) {
        Calendar calendar = Calendar.getInstance();

        if (date == null) {
            date = new Date();
        }

        calendar.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void showDatePicker(Context context, DatePickerDialog.OnDateSetListener onDateSetListener) {
        showDatePicker(context, onDateSetListener, null);
    }

    public void showListSingleChoiceAlert(Context context, String[] values, DialogInterface.OnClickListener
            onSetClickListener) {
        showListSingleChoiceAlert(context, 0, values, onSetClickListener);
    }

    public void showListSingleChoiceAlert(Context context, int activePosition, String[] values,
                                          DialogInterface.OnClickListener onSetClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setSingleChoiceItems(values, activePosition, null);
        alertDialogBuilder.setPositiveButton(R.string.weight_parameters_set, onSetClickListener);
        alertDialogBuilder.show();
    }

    public void showHoursMinsChoiceAlertDialog(Context context,
                                               final OnDurationChoiceListener onDurationChoiceListener,
                                               final long dateInMillis) {
        if (context != null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            View wheelAlertDialogView = View.inflate(context, R.layout.alert_dialog_hours_mins, null);

            int currentHours = DateUtils.getHours(dateInMillis);
            int currentMinutes = DateUtils.getMinutes(dateInMillis);

            final NumberPicker hoursWheel = (NumberPicker) wheelAlertDialogView.findViewById(R.id.hoursWheel);
            hoursWheel.setMinValue(0);
            hoursWheel.setMaxValue(HOURS_MAX);
            hoursWheel.setValue(0);

            final NumberPicker minutesWheel = (NumberPicker) wheelAlertDialogView.findViewById(R.id.minutesWheel);
            minutesWheel.setMinValue(0);
            minutesWheel.setMaxValue(MINUTES_MAX);
            minutesWheel.setValue(0);

            if (currentHours != 0 || currentMinutes != 0) {
                hoursWheel.setValue(currentHours);
                minutesWheel.setValue(currentMinutes);
            }

            alertDialogBuilder.setView(wheelAlertDialogView);
            alertDialogBuilder.setPositiveButton(context.getString(R.string.done),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (onDurationChoiceListener != null) {
                                int hours = hoursWheel.getValue();
                                int minutes = minutesWheel.getValue();

                                long durationInMillis = DateUtils.getLongFromHoursAndMinutes(hours, minutes);

                                onDurationChoiceListener.onDurationChoice(durationInMillis);
                            }
                        }
                    });
            alertDialogBuilder.show();
        }
    }

    public void showDistanceChoiceAlertDialog(Context context,
                                              final OnDistanceChoiceListener onDistanceChoiceListener,
                                              final float distanceInMeter) {
        if (context != null) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            View wheelAlertDialogView = View.inflate(context, R.layout.alert_dialog_distance, null);

            float currentKilometers = UIUtils.getKilometers(distanceInMeter);
            float currentMinutes = UIUtils.getMeters(distanceInMeter) / METER_STEP;

            final NumberPicker kilometerWheel = getKilometersPicker(wheelAlertDialogView);

            final NumberPicker metersWheel = (NumberPicker) wheelAlertDialogView.findViewById(R.id.metersWheel);
            final String[] metersValues = getPickerValuesInMeters(metersWheel);

            if (currentKilometers != 0 || currentMinutes != 0) {
                kilometerWheel.setValue((int) currentKilometers);
                metersWheel.setValue((int) currentMinutes);
            }

            alertDialogBuilder.setView(wheelAlertDialogView);
            alertDialogBuilder.setPositiveButton(context.getString(R.string.done),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (onDistanceChoiceListener != null) {
                                int kilometers = kilometerWheel.getValue();
                                int meterIndex = metersWheel.getValue();
                                int meterValue = Integer.valueOf(metersValues[meterIndex]);

                                float distanceInMeter = UIUtils.getLongFromKilometersAndMeters(kilometers, meterValue);

                                onDistanceChoiceListener.onDistanceChoice(distanceInMeter);
                            }
                        }
                    });
            alertDialogBuilder.show();
        }
    }

    private String[] getPickerValuesInMeters(NumberPicker metersWheel) {
        final String[] metersValues = getMetersValues(0, METERS_MAX, METER_STEP);
        metersWheel.setMinValue(0);
        metersWheel.setMaxValue(metersValues.length - 1);
        metersWheel.setValue(0);
        metersWheel.setDisplayedValues(metersValues);
        return metersValues;
    }

    private NumberPicker getKilometersPicker(View wheelAlertDialogView) {
        final NumberPicker kilometerWheel = (NumberPicker) wheelAlertDialogView.findViewById(R.id.kilometerWheel);

        kilometerWheel.setMinValue(0);
        kilometerWheel.setMaxValue(KILOMETERS_MAX);
        kilometerWheel.setValue(0);
        return kilometerWheel;
    }

    public String[] getMetersValues(int minValue, int maxValue, int step) {
        final List<String> valuesList = new ArrayList<String>();

        for (int i = minValue; i < maxValue; i += step) {
            int value = i;
            valuesList.add(String.valueOf(value));
        }

        final String[] values = new String[valuesList.size()];
        return valuesList.toArray(values);
    }

    public interface OnDurationChoiceListener {
        void onDurationChoice(long durationInMillis);
    }

    public interface OnDistanceChoiceListener {
        void onDistanceChoice(float distanceInMeter);
    }
}
