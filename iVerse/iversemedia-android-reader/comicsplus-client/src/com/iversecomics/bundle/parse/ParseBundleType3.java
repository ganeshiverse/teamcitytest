package com.iversecomics.bundle.parse;

import android.text.TextUtils;

import java.io.EOFException;
import java.io.IOException;
import java.security.GeneralSecurityException;

import com.iversecomics.bundle.BundleReadException;
import com.iversecomics.bundle.BundleSecurityException;
import com.iversecomics.bundle.ComicContent;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.bundle.Ownership;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.util.text.StringUtils;

public class ParseBundleType3 extends AbstractParseBundle {
    private final static Logger LOG = LoggerFactory.getLogger(ParseBundleType3.class);
    /** Section Types */
    private static final short ID                       = 1;
    private static final short NAME                     = 3;
    private static final short WRITER                   = 5;
    private static final short ARTIST                   = 6;
    private static final short PUBLISHER_NAME           = 7;
    private static final short PUBLISHER_URL            = 8;
    private static final short PUBLISH_DATE             = 9;
    private static final short COPYRIGHT                = 11;
    private static final short PANEL_COUNT              = 12;
    private static final short VERTICAL_COUNT           = 13;
    private static final short CONTENT_COUNT            = 14;
    private static final short SYNOPSIS                 = 15;
    private static final short DESCRIPTION              = 16;
    private static final short CATEGORY                 = 17;
    private static final short PUBLISHER_ID             = 18;
    private static final short PRODUCT_TYPE             = 19;
    private static final short SERIES_COMIC_ID          = 20;
    private static final short ICON                     = 100;
    private static final short THUMB                    = 101;
    private static final short IMAGE_PANEL              = 110;
    @SuppressWarnings("unused")
    private static final short IMAGE_PANEL_ENCRYPTED    = 111;
    private static final short IMAGE_VERTICAL           = 112;
    @SuppressWarnings("unused")
    private static final short IMAGE_VERTICAL_ENCRYPTED = 113;
    @SuppressWarnings("unused")
    private static final short COMIC_IMAGE_SMALL        = 114;
    @SuppressWarnings("unused")
    private static final short COMIC_IMAGE_MEDIUM       = 115;
    @SuppressWarnings("unused")
    private static final short COMIC_IMAGE_LARGE        = 116;
    @SuppressWarnings("unused")
    private static final short COVER                    = 117;
    private static final short COMIC_AD_HORIZONTAL      = 120;
    private static final short COMIC_AD_VERTICAL        = 121;
    private static final short GENERATED_DATE           = 201;
    private static final short GENERATED_BY             = 202;
    private static final short OWNER_EMAIL_ENCRYPTED    = 203;
    private static final short BUNDLE_CHECKSUM          = 299;
    private static final short MAX_TYPE                 = BUNDLE_CHECKSUM;

    @Override
    public void parse(BundleAccess ba) throws BundleReadException {
        LOG.debug("Parsing Bundle Version 3");
        if (ownership == null) {
            // Throw an exception, but be vague.
            throw new BundleSecurityException("Ownership not initialized (1).");
        }

        try {
            short type = ba.readShort();
            while (type != 0) {
                LOG.debug("type: %d" , type);
                if ((type < 0) || (type > MAX_TYPE)) {
                    throw new IOException("Invalid type <" + type + "> at offset <" + ba.getFilePointer() + ">");
                }
                switch (type) {
                    // Metadata
                    case ID:
                        metadata.setId(ba.readString());
                        break;
                    case SERIES_COMIC_ID:
                        metadata.setSeriesId(ba.readString());
                        break;
                    case NAME:
                        metadata.setName(ba.readString());
                        break;
                    case SYNOPSIS:
                        metadata.setSynopsis(ba.readString());
                        break;
                    case DESCRIPTION:
                        metadata.setDescription(ba.readString());
                        break;
                    case CATEGORY:
                        metadata.addCategory(ba.readString());
                        break;
                    case PUBLISHER_ID:
                        metadata.setPublisherId(ba.readString());
                        break;
                    case PRODUCT_TYPE:
                        metadata.setProductType(ba.readString());
                        break;
                    case WRITER:
                        metadata.addWriter(ba.readString());
                        break;
                    case ARTIST:
                        metadata.addArtist(ba.readString());
                        break;
                    case PUBLISHER_NAME:
                        metadata.setPublisherName(ba.readString());
                        break;
                    case PUBLISHER_URL:
                        metadata.setPublisherURL(ba.readString());
                        break;
                    case PUBLISH_DATE:
                        metadata.setPublishDate(ba.readString());
                        break;
                    case COPYRIGHT:
                        metadata.setCopyright(ba.readString());
                        break;
                    case OWNER_EMAIL:
                        metadata.setRegisteredTo(ba.readString());
                        break;
                    case COMIC_AD_HORIZONTAL:
                        LOG.debug("COMIC_AD_HORIZONTAL: 0x%02x", ba.getFilePointer());
                        LOG.debug("AD [%s]", ba.readString() ); // TODO: handle ad
                        // debug("Idx [%d]", ba.readShort());
                        break;
                    case COMIC_AD_VERTICAL:
                        LOG.debug("COMIC_AD_HORIZONTAL: 0x%02x", ba.getFilePointer());
                        LOG.debug("AD [%s]", ba.readString() ); // TODO: handle ad
                        // debug("Idx [%d]", ba.readShort());
                        break;
                    // Icon
                    case ICON:
                        // DEPRECATED getContent().setOffsetIcon(ba.readFileOffsets());
                        break;
                    // Thumb
                    case THUMB:
                        // DEPRECATED getContent().setOffsetThumbslider(ba.readFileOffsets());
                        break;
                    // Comic Registration
                    case OWNER_EMAIL_ENCRYPTED:
                        //encryptedOwner = ba.readEncryptedString();
                        ba.readEncryptedString();
                        break;
                    // Generation Info
                    case GENERATED_DATE:
                        metadata.setGeneratedDate(ba.readDate());
                        break;
                    case GENERATED_BY:
                        metadata.setGeneratedBy(ba.readString());
                        break;
                    // Panels
                    case CONTENT_COUNT:
                        int count = ba.readShort() + 1;
                        LOG.debug("CONTENT COUNT: %d" , count);
                        // panelOffsets = new PanelOffsets(count);
                        break;
                    case PANEL_COUNT:
                        int countH = ba.readShort();
                        LOG.debug("PANEL COUNT: %d" , countH);
                        // panelOffsets = new PanelOffsets(count);
                        break;
                    case VERTICAL_COUNT:
                        int countV = ba.readShort();
                        LOG.debug("VERTICAL COUNT: %d" , countV);
                        // panelOffsets = new PanelOffsets(count);
                        break;
                    case IMAGE_PANEL:
                        getContent().addPanel(ba.readImageOffset(), ComicContent.Type.PANEL);
                        break;
                    case IMAGE_VERTICAL:
                        getContent().addPanel(ba.readImageOffset(), ComicContent.Type.PAGE);
                        break;
                    case BUNDLE_CHECKSUM:
                        /* byte checksum[] = */
                        ba.readChecksum();
                        break;
                    default:
                        throw new BundleReadException(String.format("Unreadable type: %d.%d%n", type, ba
                                .getFilePointer()));
                }
                type = ba.readShort();
            }


        } catch (EOFException e) {
            // at end of bundle.
        } catch (GeneralSecurityException e) {
            throw new BundleSecurityException("Internal security error", e);
        } catch (IOException e) {
            throw new BundleReadException("Unable to open downloaded comic: " + e.getMessage(), e);
        }
        
        metadata.setStatus(ComicMetadata.STATUS_OK);
        LOG.info("Parsing complete");
        
        /*
        if (encryptedOwner == null) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Comic Not Registered To You");
        }
        

        if (encryptedOwner.equals(metadata.getRegisteredTo())) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Illegal Comic Access");
        }
        */
        
        if (TextUtils.isEmpty(ownership.getEmailAddress())) {
            metadata.setStatus(ComicMetadata.STATUS_LIB_FAILURE);
            // Throw an exception, but be vague.
            throw new BundleSecurityException("Ownership not initialized (2).");
        }

        if (TextUtils.isEmpty(ownership.getUniqueId())) {
            metadata.setStatus(ComicMetadata.STATUS_LIB_FAILURE);
            // Throw an exception, but be vague.
            throw new BundleSecurityException("Ownership not initialized (3).");
        }
        
        if (metadata.getRegisteredTo() == null || metadata.getRegisteredTo().equals("")) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Illegal Comic Access");
        }
        
        if (!metadata.getRegisteredTo().equals(ownership.getEmailAddress())) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Illegal Comic Access");
        }
    }

    @Override
    public void setOwnership(Ownership ownership) {
        this.ownership = ownership;
    }
}
