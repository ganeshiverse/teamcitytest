package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 9/23/2015.
 */
public class UserPreferencesLoader extends DataAsyncTaskLibLoader<String> {

    private String token;


    public UserPreferencesLoader(Context context,  String token) {
        super(context);
        this.token = token;
    }

    @Override
    protected String performLoad() throws Exception {
        return ServerMethods.getInstance().getUserPreferences(token);
    }
}
