package com.iversecomics.client.store.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.RatingBar;

import com.iversecomics.client.R;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.store.Price;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.ui.widget.CoverView;

public class ComicListAdapter extends AbstractComicCursorAdapter {
	
	MyComicsModel model;
	
    public ComicListAdapter(Context context, Cursor c, int layoutId) {
        super(context, c, layoutId);
        model = new MyComicsModel(context);
    }
    
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Comic comic = ComicsTable.fromCursor(cursor);

        setText(view, R.id.name, comic.getName());
        setText(view, R.id.publisher, comic.getPublisherName());

        //TODO temp code, join this with the comic detail page price using localized pricing from the IAP api
        if(model.isOwned(comic) == MyComicsModel.PossessionState.AVAILABLE)
        {
        	setText(view, R.id.price, context.getResources().getString(R.string.possession_state_available));
        }
        else if(model.isOwned(comic) == MyComicsModel.PossessionState.PURCHASED)
        {
        	setText(view, R.id.price, context.getResources().getString(R.string.comic_purchased));
        }
        else  if (comic.getPricingTier() == 0)
        {
        	setText(view, R.id.price, context.getResources().getString(R.string.free));
        }
        else
        {
	        final String amount = Price.getComicPrice(comic);
	        setText(view, R.id.price, "$" + amount);
        }

        CoverView cover = (CoverView) view.findViewById(R.id.cover);
        if (cover != null) {
            cover.setCover(comic, getBitmapManager());
        }

        RatingBar ratings = (RatingBar) view.findViewById(R.id.rating);
        if (ratings != null) {
            ratings.setRating((float) comic.getAvgRating());
        }
    }
}
