package com.iversecomics.client.store.tasks;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.iab.base64.Base64;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.concurrent.Future;


/**
 * Submits user's subscription purchase info to iVerse server.
 * Use immediately after a subscription has been purchased from the Google Play store
 * so that server can track subscription status.
 */
public class VerifySubscriptionTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(VerifySubscriptionTask.class);
    private String subscriptionProductSku;
    private String deviceId;
    private String installId;
    private String signature;
    private String receiptData;
    Future<?> serverConfigFuture;

    private static final int mInAppPurchaseNoError =3004;
    Context context;
    Activity activity;

    public VerifySubscriptionTask(Context context, ComicStore comicStore, String subscriptionProductSku, String installId, String receiptData, String signature, Activity activity) {
        super(comicStore);
        this.subscriptionProductSku = subscriptionProductSku;
        this.deviceId = AndroidInfo.getUniqueDeviceID(context);
        this.installId = installId;
        this.receiptData = receiptData;
        this.signature = signature;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public void execTask() {
        try {
            final JSONObject payload = new JSONObject();
            payload.put("productId", subscriptionProductSku);
            payload.put("deviceId", deviceId);
            payload.put("installId", installId);
            // Client asked me to send this encoded data
            payload.put("receiptSignature", signature.trim());
            payload.put("receiptData", Base64.encode(receiptData.toString().trim().getBytes()));

            ComicStore.ServerApi api = this.comicStore.getServerApi();
            JSONObject jsonResp = api.verifySubscription(payload);
            handleServerResponse(jsonResp);
            final String status = jsonResp.optString("status");
            if (status.equals("ok"))
                LOG.info("Subscription info submitted to iVerse server");
            else
                LOG.error("Error submitting subscription status to server: %s", status);

        } catch (Exception e) {
            LOG.warn(e, "Error submitting subscription status to server: " + e.getLocalizedMessage());
        }
    }

    private void handleServerResponse(JSONObject jsonResp) throws JSONException {
        ServerConfig serverConfig = ServerConfig.getDefault();
        //Read the existing subscription state
        ServerConfig.SubscriptionStatus lastStatus = serverConfig.getSubscriptionStatus();
        //Get the new status
        ServerConfig.SubscriptionStatus newStatus = lastStatus;
        //Check for handle-able errors
        String status = jsonResp.optString("status");
        String message = "";
        //Ok, if the new status mismatches the old status, tell the user
        if (status.equals("ok")) {
            //No error, so confirm the new status
            JSONObject subscriptionDetails = jsonResp.getJSONObject("subscription");
            int subscriptionStatus = Integer.parseInt(subscriptionDetails.optString("status"));
            //Valid receipt, subscription is still active (status 1)
            if (subscriptionStatus == 1) {
                //subscribed successfully and your expired date is :: need to show the date...
                newStatus = ServerConfig.SubscriptionStatus.SubscriptionStatusCurrent;
                message = context.getString(R.string.subscription_activated);
                showSubscriptionDetailsDialog(message, context.getString(R.string.subscription_activation_titile));
            }
            //Valid receipt, but subscription is inactive (status 2)
            else {
                // show only expired details..
                newStatus = ServerConfig.SubscriptionStatus.SubscriptionStatusExpired;
                message = context.getString(R.string.subscription_expired);
                showSubscriptionDetailsDialog(message, context.getString(R.string.subscription_expired_title));
            }

        } else if (status.equals("error")) {

            JSONObject error = jsonResp.getJSONObject("error");
            String errorMessage = error.optString("errorMessage");
            int errorCode = Integer.parseInt(error.optString("errorCode"));
            switch (errorCode) {
                case mInAppPurchaseNoError: {
                    LOG.debug("Android Purchase Verification Failed.");
                    showSubscriptionDetailsDialog(errorMessage, context.getResources().getString(R.string.error));
                    break;
                }
                default: {
                    LOG.debug("Server reported un unhandled error");
                    showSubscriptionDetailsDialog(context.getString(R.string.subscription_unhandled_error), context.getResources().getString(R.string.error));
                    return;
                }

            }

        }
        //Ok, if the new status mismatches the old status, tell the user
        if (lastStatus != newStatus) {
            //Subscribed?
            if (newStatus == ServerConfig.SubscriptionStatus.SubscriptionStatusCurrent) {
                LOG.debug("Subscription purchase successful!");

            }
            //Expired?
            if (newStatus == ServerConfig.SubscriptionStatus.SubscriptionStatusExpired) {
                LOG.debug("Subscription has expired!");

            }
        }
        final IverseApplication app = IverseApplication.getApplication();
        final Task task = new ServerConfigTask(app.getApplicationContext(), app.getComicStore());
        serverConfigFuture = app.getTaskPool().submit(task);
    }

    /**
     * Displays the subscription related messages to the user
     *
     * @param message - which need to display
     * @param title   -  Alert dialog title.
     */
    private void showSubscriptionDetailsDialog(final String message, final String title) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(title)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.create().show();
            }
        });
    }
}

    /* TODO: joliver 12/30/2013
    This is how the iOS version parses the server response:

    #pragma mark - Subscription Related
- (void)handleServerResponse:(NSDictionary*)resultsDictionary
{
    //Read the existing subscription state
    SubscriptionStatus lastStatus = [[NSUserDefaults standardUserDefaults] integerForKey:kLastSubscriptionStateKey];

    //Get the new status
    SubscriptionStatus newStatus = lastStatus;

    //Check for handle-able errors
    NSDictionary* errorDictionary = resultsDictionary[@"error"];
    if (errorDictionary != nil)
    {
        int errorCode = [errorDictionary[@"errorCode"] intValue];
        switch (errorCode)
        {
            case kInAppPurchaseNoError:
            {
                break;
            }
            case kInAppPurchaseErrorExpired:
            {
                //Subscription has expired
                DebugLog(@"Server reports expired subscription");
                newStatus = SubscriptionStatusExpired;
                break;
            }
            case kInAppPurchaseErrorServerDown:
            {
                //Display a message to the user
                DebugLog(@"iTunes Server Unavailable");
                [[ErrorDialog sharedInstance] displayErrorMessage:NSLocalizedString(@"Your subscription could not be verified at this time.  Please try again later.",@"Message for failure to validate subscription (server down)") title:NSLocalizedString(@"Unable to Verify Subscription",@"Title for failure to validate subscription")];
                return;
            }
            default:
            {
                DebugLog(@"Server reported unhandle-able error");
                NSString* errorMessage = NSLocalizedString(@"A server error prevented the validation of your subscription.  Click Restore iTunes Purchases in the Settings screen to retry.",@"Message for failure to validate subscription (other)");
#ifndef BUILD_MODE_APPSTORE
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"\n:%@", errorDictionary[@"errorMessage"]]];
#endif

                [[ErrorDialog sharedInstance] displayErrorMessage:errorMessage title:NSLocalizedString(@"Unexpected Server Error",@"Title for unexpected server error")];
                return;
            }
        }
    }
    else
    {
        //No error, so confirm the new status
        if ([resultsDictionary[@"status"] isEqualToString:@"ok"])
        {
            newStatus = SubscriptionStatusCurrent;
        }
    }

    //Ok, if the new status mismatches the old status, tell the user
    if (lastStatus != newStatus)
    {
        //Subscribed?
        if (newStatus == SubscriptionStatusCurrent)
        {
            DebugLog(@"Subscription purchase successful!");
            [[ErrorDialog sharedInstance] displayErrorMessage:NSLocalizedString(@"Your subscription is now active.  Enjoy!",@"Message for subscription active") title:NSLocalizedString(@"Subscription Activated",@"Title for subscription active")];
        }

        //Expired?
        if (newStatus == SubscriptionStatusExpired)
        {
            DebugLog(@"Subscription has expired!");
            [[ErrorDialog sharedInstance] displayErrorMessage:NSLocalizedString(@"Your subscription has expired.", @"Message got subscription expired") title:NSLocalizedString(@"Subscription Expired",@"Title for subscription expired")];
        }
    }

    //Write the new status
    [[NSUserDefaults standardUserDefaults] setInteger:newStatus forKey:kLastSubscriptionStateKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //Force update the server
    [self fetchConfigForced:YES];
}


     */

//}
