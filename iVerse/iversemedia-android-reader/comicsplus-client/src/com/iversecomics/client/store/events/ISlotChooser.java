package com.iversecomics.client.store.events;

public interface ISlotChooser {
    OnSlotClickListener getOnSlotClickListener();

    void setOnSlotClickListener(OnSlotClickListener listener);
}
