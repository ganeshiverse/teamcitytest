package com.iversecomics.client.refresh;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.iversecomics.client.store.db.FreshnessTable;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class Freshness {
    private static final Logger LOG = LoggerFactory.getLogger(Freshness.class);
    private ContentResolver resolver;

    public Freshness(Context context) {
        this.resolver = context.getContentResolver();
    }

    /**
     * Remove entries from freshness cache if they are considered expired.
     *
     * @param cutoffTimestamp the timestamp to use as the cutoff.
     */
    public void clearExpired(long cutoffTimestamp) {
        String where = FreshnessTable.TIMESTAMP_EXPIRED + " < ?";
        String whereArgs[] = {
                Long.toString(cutoffTimestamp)
        };
        try {
            resolver.delete(FreshnessTable.CONTENT_URI, where, whereArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests that the key is still fresh (not expired)
     *
     * @param freshnessKey the key to test
     * @return true if the key is still fresh.
     */
    public boolean isFresh(String freshnessKey) {
        String where = FreshnessTable.KEY + " = ?";
        String whereArgs[] = {
                freshnessKey
        };
        String projection[] = FreshnessTable.FULL_PROJECTION;
        String sortOrder = FreshnessTable.DEFAULT_SORT_ORDER;
        Cursor c = null;
        try {
            c = resolver.query(FreshnessTable.CONTENT_URI, projection, where, whereArgs, sortOrder);
            if (c.moveToFirst()) {
                // Has key in DB
                int colExpiredTimestamp = c.getColumnIndex(FreshnessTable.TIMESTAMP_EXPIRED);
                if (colExpiredTimestamp < 0) {
                    // No such column
                    LOG.error("No such column [%s]", FreshnessTable.TIMESTAMP_EXPIRED);
                    return false;
                }
                long timestampExpired = c.getLong(colExpiredTimestamp);
                long now = System.currentTimeMillis();
                return (timestampExpired > now);
            } else {
                // Does not have key in DB
                // Never reported to us, can't determine freshness
                return false;
            }
        }catch (Exception e){
        }
        finally {
            DBUtil.close(c);
        }
        return false;
    }

    /**
     * Similar to {@link #setExpiresOn(String, long)} but accepts a delta of milliseconds from now when the key expires.
     *
     * @param freshnessKey       the key to set
     * @param expiresAfterMillis the number of milliseconds till the key expires.
     */
    public void setExpiresAfter(String freshnessKey, long expiresAfterMillis) {
        long now = System.currentTimeMillis();
        setExpiresOn(freshnessKey, now + expiresAfterMillis);
    }

    /**
     * Sets the key as being fresh as of the timestamp specified.
     *
     * @param freshnessKey the key to set
     * @param expiresOn    the timestamp to set the key when the key is expired
     */
    public void setExpiresOn(String freshnessKey, long expiresOn) {
        LOG.debug("Setting freshness [%s] to [%d]", freshnessKey, expiresOn);
        ContentValues values = new ContentValues();
        values.put(FreshnessTable.KEY, freshnessKey);
        values.put(FreshnessTable.TIMESTAMP_EXPIRED, expiresOn);
        resolver.insert(FreshnessTable.CONTENT_URI, values);
    }
}
