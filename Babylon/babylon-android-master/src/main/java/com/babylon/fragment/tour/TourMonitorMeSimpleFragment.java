package com.babylon.fragment.tour;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TourMonitorMeSimpleFragment extends Fragment {

    private static final String RESOURCE_ID = "RESOURCE_ID";

    public static TourMonitorMeSimpleFragment getInstance(int resId) {
        final Bundle bundle = new Bundle();
        bundle.putInt(RESOURCE_ID, resId);

        final TourMonitorMeSimpleFragment fragment = new TourMonitorMeSimpleFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {

        final int resId = getArguments().getInt(RESOURCE_ID);
        return inflater.inflate(resId, container, false);
    }
}
