package com.babylon.activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.babylon.R;
import com.babylon.utils.L;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class AudioPlaybackDialogActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnBufferingUpdateListener, SeekBar.OnSeekBarChangeListener {

    public static final String TAG = AudioPlaybackDialogActivity.class.getSimpleName();
    private SeekBar mSeekBar;
    private ImageButton mPlayButton;
    private ImageButton mPauseButton;
    private MediaPlayer mMediaPlayer;
    private MediaObserver mMediaObserver;

    private class MediaObserver implements Runnable {
        private AtomicBoolean stop = new AtomicBoolean(false);
        private AtomicBoolean pause = new AtomicBoolean(false);

        public void stop() {
            stop.set(true);
        }

        public void pause() {
            pause.set(true);
        }

        public void resume() {
            pause.set(false);
        }

        @Override
        public void run() {
            while (!stop.get()) {
                if(!pause.get()) {
                    mSeekBar.setProgress(mMediaPlayer.getCurrentPosition() / 1000);
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    //Ignore exception
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_playback_dialog_activity);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(192, 0, 0, 0)));

        mPlayButton = (ImageButton)findViewById(R.id.play_button);
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPlayButtonPressed(view);
            }
        });
        mPauseButton = (ImageButton)findViewById(R.id.pause_button);
        mPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPauseButtonPressed(view);
            }
        });
        mSeekBar = (SeekBar)findViewById(R.id.playback_seek_bar);
        mSeekBar.setOnSeekBarChangeListener(this);
        mMediaPlayer = new MediaPlayer();
        Bundle extras = getIntent().getExtras();
        String url = extras.getString("audio_url_extra");
        playMp3(url);
    }

    public void playMp3(String link){

        mMediaPlayer.reset();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mMediaPlayer.setDataSource(link);
            mMediaPlayer.setOnBufferingUpdateListener(this);
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.prepareAsync();
        } catch (IllegalArgumentException e) {
            L.e(TAG, e.toString(), e);
        } catch (SecurityException e) {
            L.e(TAG, e.toString(), e);
        } catch (IllegalStateException e) {
            L.e(TAG, e.toString(), e);
        } catch (IOException e) {
            L.e(TAG, e.toString(), e);
        }
    }

    public void onPrepared(MediaPlayer mediaplayer) {
        if(!mMediaPlayer.isPlaying()){
            mMediaPlayer.start();
            mSeekBar.setMax(mMediaPlayer.getDuration());
            mMediaObserver = new MediaObserver();
            new Thread(mMediaObserver).start();
            mPlayButton.setEnabled(false);
            mPauseButton.setEnabled(true);
            mSeekBar.setProgress(0);
            mSeekBar.setMax(100);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mMediaPlayer.reset();
        mSeekBar.setProgress(0);
        mSeekBar.setSecondaryProgress(0);
        mPlayButton.setEnabled(true);
        mPauseButton.setEnabled(false);
        mMediaObserver.stop();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        mSeekBar.setSecondaryProgress(percent);
    }

    public void onPauseButtonPressed(View view) {
        mMediaObserver.stop();
        mMediaPlayer.pause();
        mPauseButton.setEnabled(false);
        mPlayButton.setEnabled(true);
    }

    public void onPlayButtonPressed(View view) {
        mMediaObserver = new MediaObserver();
        new Thread(mMediaObserver).start();
        mMediaPlayer.start();
        mPauseButton.setEnabled(true);
        mPlayButton.setEnabled(false);
    }

    public void onDialogCloseButtonClick(View view) {
        mMediaObserver.stop();
        mMediaPlayer.stop();
        mMediaPlayer.release();
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        mMediaPlayer.seekTo(progress * 1000);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mMediaObserver.pause();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mMediaObserver.resume();
    }
}
