package com.babylon.database;

import android.content.AsyncQueryHandler;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.R;
import com.babylon.database.schema.BaseQuery;
import com.babylon.database.schema.CheckResultSchema;
import com.babylon.database.schema.CommentSchema;
import com.babylon.database.schema.FileEntrySchema;
import com.babylon.database.schema.FoodSchema;
import com.babylon.database.schema.MessageSchema;
import com.babylon.database.schema.MonitorCategorySchema;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.database.schema.StressQuizSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.BaseModel;
import com.babylon.model.Comment;
import com.babylon.model.FileEntry;
import com.babylon.model.Food;
import com.babylon.model.Message;
import com.babylon.model.MonitorMeCategory;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.PhysicalActivity;
import com.babylon.model.StressQuestion;
import com.babylon.model.check.CheckResult;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * Saves, deletes, updates database content
 */
public class DatabaseOperationUtils {

    public static final String REJECT_COMMENT = "REJECT";

    private static final String TAG = DatabaseOperationUtils.class.getSimpleName();

    public interface AsyncQueryListener {
        void onQueryComplete(int token, Object cookie, Cursor cursor);

        void onChangeComplete(int token, Object cookie, int result);

        void onInsertComplete(int token, Object cookie, Uri uri);
    }

    private static DatabaseOperationUtils instance;

    private DatabaseOperationUtils() {
        //singleton
    }

    public static DatabaseOperationUtils getInstance() {
        if (instance == null) {
            instance = new DatabaseOperationUtils();
        }
        return instance;
    }

    public void saveMessagesSynchronously(List<Message> messages, ContentResolver contentResolver) {
        List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        for (int i = 0; i < messages.size(); i++) {
            final Message message = messages.get(i);
            batch.add(ContentProviderOperation.newInsert(MessageSchema.CONTENT_URI).withValues(message
                    .toContentValues()).build());
            batch.addAll(getMessageFilesBatch(message));
            batch.addAll(getCommentsBatch(message));
        }
        PreferencesManager.getInstance().setFirstSync(false);
        applyBatch(contentResolver, batch);
    }

    /**
     * Handle if message reject then add fake comment for save is this answer already viewed or not
     * not delete comment fields, because id, question id, date, text need
     * for select comment on question activity for read or unread logic
     */
    private void handleRejectMessage(Message message) {
        if (message.getStateId() == Message.REJECT_STATE_ID && message.getComments().isEmpty()) {
            List<Comment> comments = new ArrayList<Comment>();
            Comment comment = new Comment();
            comment.setId(comment.getRandomUUIDStr());
            comment.setQuestionId(message.getId());
            comment.setText(REJECT_COMMENT);
            comments.add(comment);
            comment.setDate(message.getDate());
            message.setComments(comments);
        }
    }

    private List<ContentProviderOperation> getCommentsBatch(Message message) {
        List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        handleRejectMessage(message);
        List<Comment> comments = message.getComments();

        for (int i = 0; i < comments.size(); i++) {
            Comment comment = comments.get(i);
            if (PreferencesManager.getInstance().isFirstSync()) {
                comment.setRead(true);
            }
            batch.add(ContentProviderOperation.newInsert(CommentSchema.CONTENT_URI).withValues(comment
                    .toContentValues()).build());
        }
        return batch;
    }

    private List<ContentValues> getFilesContentValues(Message message) {
        List<ContentValues> filesValues = new ArrayList<ContentValues>();
        final List<FileEntry> files = message.getFiles();
        for (int j = 0; j < files.size(); j++) {
            FileEntry fileEntry = files.get(j);
            fileEntry.setQuestionId(message.getId());
            filesValues.add(fileEntry.toContentValues());
        }
        return filesValues;
    }

    private List<ContentProviderOperation> getMessageFilesBatch(Message message) {
        List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        final List<FileEntry> files = message.getFiles();
        for (int j = 0; j < files.size(); j++) {
            FileEntry fileEntry = files.get(j);
            fileEntry.setQuestionId(message.getId());
            fileEntry.setNameById();
            batch.add(ContentProviderOperation.newInsert(FileEntrySchema.CONTENT_URI).withValues(fileEntry
                    .toContentValues()).build());
        }
        return batch;
    }

    private void saveFilesToDb(ContentResolver contentResolver, List<ContentValues> filesValues) {
        contentResolver.bulkInsert(FileEntrySchema.CONTENT_URI, filesValues.toArray(new ContentValues[filesValues
                .size()]));
    }

    public void saveMonitorMeCategories(ContentResolver contentResolver, List<MonitorMeCategory> categories) {
        contentResolver.bulkInsert(MonitorCategorySchema.CONTENT_URI, getContentValuesArray(categories));
    }

    public ContentValues[] getContentValuesArray(List<? extends BaseModel> list) {
        ContentValues[] contentValues = new ContentValues[list.size()];
        for (int index = 0; index < list.size(); index++) {
            contentValues[index] = list.get(index).toContentValues();
        }
        return contentValues;
    }

    public void saveMessage(Message message, ContentResolver contentResolver) {
        saveMessage(message, contentResolver, null);
    }

    public void saveMessage(Message message, ContentResolver contentResolver, AsyncQueryListener asyncQueryListener) {
        BabylonAsyncQueryHandler babylonAsyncQueryHandler = new BabylonAsyncQueryHandler(contentResolver,
                asyncQueryListener);
        babylonAsyncQueryHandler.startInsert(Constants.DATABASE_UNIDENTIFIED_TOKEN, null, MessageSchema.CONTENT_URI,
                message.toContentValues());
        saveFilesToDb(contentResolver, getFilesContentValues(message));
        saveFilesToStorage(message.getFiles());
    }

    public void updateCommentsAsRead(ContentResolver contentResolver) {
        BabylonAsyncQueryHandler babylonAsyncQueryHandler = new BabylonAsyncQueryHandler(contentResolver,
                null);
        ContentValues contentValues = new ContentValues();
        contentValues.put(CommentSchema.Columns.IS_READ.getName(), Boolean.TRUE.toString());
        babylonAsyncQueryHandler.startUpdate(Constants.DATABASE_UNIDENTIFIED_TOKEN, null, CommentSchema.CONTENT_URI,
                contentValues, null, null);
    }

    private void saveFilesToStorage(List<FileEntry> files) {
        for (int i = 0; i < files.size(); i++) {
            FileEntry fileEntry = files.get(i);
            try {
                FileOutputStream outputStream = new FileOutputStream(fileEntry.getFilePath());
                outputStream.write(fileEntry.getData());
                outputStream.close();
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        }
    }

    public static ContentProviderResult[] applyBatch(ContentResolver contentResolver,
                                                     List<ContentProviderOperation> operations) {
        return applyBatch(contentResolver, new ArrayList<>(operations));
    }

    public static ContentProviderResult[] applyBatch(ContentResolver contentResolver,
                                                     ArrayList<ContentProviderOperation> operations) {
        ContentProviderResult[] result = null;
        try {
            result = contentResolver.applyBatch(DatabaseHelper.CONTENT_AUTHORITY, operations);
        } catch (RemoteException | OperationApplicationException e) {
            L.e(TAG, e.toString(), e);
        }
        return result;
    }

    public void addBabylonContact(ContentResolver contentResolver) {
        String babylonSupportNumber = App.getInstance().getCurrentRegion().getSupportNumber();
        if (!TextUtils.isEmpty(babylonSupportNumber)) {
            /* Adding insert operation to operations list
               to insert a new raw contact in the table ContactsContract.RawContacts */
            ArrayList<ContentProviderOperation> providerOperations =
                    new ArrayList<ContentProviderOperation>();

            int rawContactID = 0;
            providerOperations.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .build());

            /* Adding insert operation to operations list
               to insert display name in the table ContactsContract.Data */
            providerOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName
                            .CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            App.getInstance().getString(R.string.app_name))
                    .build());

            /* Adding insert operation to operations list
               to insert Mobile Number in the table ContactsContract.Data */

            providerOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, babylonSupportNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone
                            .TYPE_MOBILE)
                    .build());

            try {
                contentResolver.applyBatch(ContactsContract.AUTHORITY, providerOperations);
            } catch (RemoteException | OperationApplicationException e) {
                L.e(TAG, e.toString(), e);
            }
        }
    }

    public Object[] getColumnValues(String[] columnNames, ContentValues contentValues) {
        Object[] columnValues = new Object[columnNames.length];
        for (int i = 0; i < columnNames.length; i++) {
            columnValues[i] = contentValues
                    .get(columnNames[i]);
        }
        return columnValues;
    }


    public static class BabylonAsyncQueryHandler extends AsyncQueryHandler {

        private AsyncQueryListener completeListener;

        public BabylonAsyncQueryHandler(ContentResolver contentResolver, AsyncQueryListener completeListener) {
            super(contentResolver);
            this.completeListener = completeListener;
        }


        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            super.onQueryComplete(token, cookie, cursor);
            if (completeListener != null) {
                completeListener.onQueryComplete(token, cookie, cursor);
            } else if (cursor != null) {
                cursor.close();
            }
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            super.onInsertComplete(token, cookie, uri);
            if (completeListener != null) {
                completeListener.onInsertComplete(token, cookie, uri);
            }
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
            super.onDeleteComplete(token, cookie, result);
            if (completeListener != null) {
                completeListener.onChangeComplete(token, cookie, result);
            }
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            super.onUpdateComplete(token, cookie, result);
            if (completeListener != null) {
                completeListener.onChangeComplete(token, cookie, result);
            }
        }
    }

    public ContentProviderOperation getQuestionInsertOperation(StressQuestion question) {
        final ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(StressQuizSchema
                .CONTENT_URI);
        final ContentValues values = question.toContentValues();
        builder.withValues(values);

        return builder.build();
    }

    public void addMonitorMeFeatures(Map<String, MonitorMeFeatures.Feature> monitorMeFeatures,
                                     ContentResolver contentResolver) {
        final Iterator<Map.Entry<String, MonitorMeFeatures.Feature>> iterator = monitorMeFeatures.entrySet().iterator();
        final List<ContentProviderOperation> operations = new ArrayList<>();

        while (iterator.hasNext()) {
            addNewMonitorMeFeatureOperation(operations, iterator.next().getValue());
        }

        if (!operations.isEmpty()) {
            applyBatch(contentResolver, operations);
        }
    }

    public void addNewMonitorMeFeatureOperation(List<ContentProviderOperation> operations,
                                                MonitorMeFeatures.Feature feature) {
        ContentProviderOperation.Builder builder = getDeleteBuilder(feature);
        operations.add(builder.build());

        final ContentProviderOperation.Builder insertBuilder = ContentProviderOperation.newInsert(MonitorMeParamSchema
                .CONTENT_URI);
        insertBuilder.withValues(feature.toContentValues());
        operations.add(insertBuilder.build());
    }

    public void saveMonitorMeFeatures(@NonNull List<MonitorMeFeatures.Feature> features) {
        if (!features.isEmpty()) {
            final List<ContentProviderOperation> operations = new LinkedList<>();

            for (MonitorMeFeatures.Feature feature : features) {
                final ContentProviderOperation.Builder insertBuilder =
                        ContentProviderOperation.newInsert(MonitorMeParamSchema.CONTENT_URI);
                insertBuilder.withValues(feature.toContentValues());
                operations.add(insertBuilder.build());
            }

            applyBatch(App.getInstance().getContentResolver(), operations);
        }


    }

    public void updateMonitorMeFeatureOperation(MonitorMeFeatures.Feature feature, ContentResolver contentResolver) {
        final List<ContentProviderOperation> operations = new ArrayList<>();

        final ContentProviderOperation.Builder builder = getUpdateBuilder(feature);
        operations.add(builder.build());
        ContentProviderResult[] results = applyBatch(contentResolver, operations);

        int firstIndex = 0;
        int empty = 0;

        if (results == null || results[firstIndex].count == empty) {
            final ContentProviderOperation.Builder insertBuilder = ContentProviderOperation.newInsert
                    (MonitorMeParamSchema.CONTENT_URI);
            insertBuilder.withValues(feature.toContentValues());
            operations.add(insertBuilder.build());

            applyBatch(contentResolver, operations);
        }
    }

    private ContentProviderOperation.Builder getUpdateBuilder(MonitorMeFeatures.Feature feature) {
        final ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(MonitorMeParamSchema
                .CONTENT_URI);
        builder.withSelection(MonitorMeParamSchema.Query.PARAM_DAY_SELECTION,
                MonitorMeParamSchema.Query.getDateSelectionArguments(feature.getDate().getTime(), feature.getId()));
        builder.withValues(feature.toContentValues());
        return builder;
    }

    private ContentProviderOperation.Builder getDeleteBuilder(MonitorMeFeatures.Feature feature) {
        ContentProviderOperation.Builder builder = ContentProviderOperation.newDelete(MonitorMeParamSchema
                .CONTENT_URI);
        builder.withSelection(MonitorMeParamSchema.Query.PARAM_DAY_SELECTION,
                MonitorMeParamSchema.Query.getDateSelectionArguments(feature.getDate().getTime(), feature.getId()));
        return builder;
    }

    private void addDeletingFeatureOperation(List<ContentProviderOperation> operations, String recordUniqueId) {
        final String selection = MonitorMeParamSchema.Columns._ID.getName() + " = ?";
        final String[] selectionArgs = new String[]{recordUniqueId};

        final ContentProviderOperation.Builder deleteBuilder = ContentProviderOperation.newDelete(MonitorMeParamSchema
                .CONTENT_URI);
        deleteBuilder.withSelection(selection, selectionArgs);

        operations.add(deleteBuilder.build());
    }

    public void insertPhysicalActivity(PhysicalActivity physicalActivity, ContentResolver contentResolver) {
        insertPhysicalActivity(physicalActivity, contentResolver, null);
    }

    public void insertPhysicalActivity(PhysicalActivity physicalActivity, ContentResolver contentResolver,
                                       AsyncQueryListener asyncQueryListener) {
        BabylonAsyncQueryHandler babylonAsyncQueryHandler = new BabylonAsyncQueryHandler(contentResolver,
                asyncQueryListener);
        babylonAsyncQueryHandler.startInsert(Constants.DATABASE_UNIDENTIFIED_TOKEN, null,
                PhysicalActivitySchema.CONTENT_URI, physicalActivity.toContentValues());
    }

    public void updatePhysicalActivity(PhysicalActivity physicalActivity, ContentResolver contentResolver) {
        contentResolver.update(PhysicalActivitySchema.CONTENT_URI, physicalActivity.toContentValues(),
                PhysicalActivitySchema.Columns.ID + "=?", new String[]{
                        physicalActivity.getId()
                }
        );
    }

    public void deletePhysicalActivity(PhysicalActivity physicalActivity, ContentResolver contentResolver) {
        deletePhysicalActivity(physicalActivity, contentResolver, null);
    }

    public void deletePhysicalActivity(PhysicalActivity physicalActivity, ContentResolver contentResolver,
                                       AsyncQueryListener asyncQueryListener) {
        BabylonAsyncQueryHandler babylonAsyncQueryHandler = new BabylonAsyncQueryHandler(contentResolver,
                asyncQueryListener);
        babylonAsyncQueryHandler.startDelete(Constants.DATABASE_UNIDENTIFIED_TOKEN, null,
                PhysicalActivitySchema.CONTENT_URI, PhysicalActivitySchema.Columns.ID + " = ?",
                new String[]{physicalActivity.getId()});
    }

    public void deleteFood(Food food, ContentResolver contentResolver) {
        deleteFood(food, contentResolver, null);
    }

    public void deleteFood(Food food, ContentResolver contentResolver,
                           AsyncQueryListener asyncQueryListener) {
        BabylonAsyncQueryHandler babylonAsyncQueryHandler = new BabylonAsyncQueryHandler(contentResolver,
                asyncQueryListener);
        babylonAsyncQueryHandler.startDelete(Constants.DATABASE_UNIDENTIFIED_TOKEN, null,
                FoodSchema.CONTENT_URI, FoodSchema.Columns.ID + " = ?",
                new String[]{String.valueOf(food.getId())});
    }

    public void operateFood(Food food, ContentResolver contentResolver) {
        operateFood(food, contentResolver, null);
    }

    public void operateFood(Food food, ContentResolver contentResolver,
                            AsyncQueryListener asyncQueryListener) {
        BabylonAsyncQueryHandler babylonAsyncQueryHandler = new BabylonAsyncQueryHandler(contentResolver,
                asyncQueryListener);
        babylonAsyncQueryHandler.startInsert(Constants.DATABASE_UNIDENTIFIED_TOKEN, null,
                FoodSchema.CONTENT_URI, food.toContentValues());
    }

    public
    @Nullable
    PhysicalActivity getPhysicalActivityFromDay(Date date, String source, ContentResolver resolver) {
        final long startDate = DateUtils.getDayStartMills(date);
        final long endDate = DateUtils.getEndOfTheDay(date);

        final String selection = PhysicalActivitySchema.Columns.START_DATE + ">= ? AND "
                + PhysicalActivitySchema.Columns.END_DATE + " < ? AND "
                + PhysicalActivitySchema.Columns.SOURCE + " = ? ";

        final String[] args = {
                String.valueOf(startDate),
                String.valueOf(endDate),
                source
        };

        Cursor c = resolver.query(PhysicalActivitySchema.CONTENT_URI, null, selection, args, null);

        PhysicalActivity physicalActivity = null;

        if (c.moveToFirst()) {
            physicalActivity = new PhysicalActivity();
            physicalActivity.createFromCursor(c);
        }

        return physicalActivity;
    }

    public static
    @Nullable
    PhysicalActivity getThisDatePhysicalActivity(Date date, String source, ContentResolver resolver) {
        final String selection = PhysicalActivitySchema.Columns.START_DATE + "= ?"
                + " AND " + PhysicalActivitySchema.Columns.SOURCE + " = ?";

        final String[] args = {
                String.valueOf(date.getTime() /
                        android.text.format.DateUtils.SECOND_IN_MILLIS
                        * android.text.format.DateUtils.SECOND_IN_MILLIS),
                source
        };

        Cursor c = resolver.query(PhysicalActivitySchema.CONTENT_URI, null, selection, args, null);

        PhysicalActivity physicalActivity = null;

        if (c.moveToFirst()) {
            physicalActivity = new PhysicalActivity();
            physicalActivity.createFromCursor(c);
        }

        return physicalActivity;
    }

    public void deleteFeature(ContentResolver r, List<MonitorMeFeatures> list, long beginOfTheDayInMillis,
                              long dayTo, List<ContentProviderOperation> operations) {
        for (int i = 0; i < list.size(); i++) {
            List<MonitorMeParam> params = list.get(i).getMonitorMeparams();

            StringBuilder query = new StringBuilder("(");
            query.append(MonitorMeParamSchema.Query.PERIOD_DELETE_SELECTION)
                    .append(") AND ( ")
                    .append(BaseQuery.getMonitorMeParamsSelection(params))
                    .append(") ");

            ContentProviderOperation.Builder deleteBuilder = ContentProviderOperation.newDelete(MonitorMeParamSchema
                    .CONTENT_URI);
            deleteBuilder.withSelection(query.toString(), BaseQuery.getDateSelectionArguments(beginOfTheDayInMillis,
                    dayTo));

            operations.add(deleteBuilder.build());
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Check
    ///////////////////////////////////////////////////////////////////////////

    public void insertSymptomAnswers(CheckResult checkResult, ContentResolver contentResolver) {
        new BabylonAsyncQueryHandler(contentResolver, null)
                .startInsert(0, null, CheckResultSchema.CONTENT_URI, checkResult.toContentValues());
    }
}
