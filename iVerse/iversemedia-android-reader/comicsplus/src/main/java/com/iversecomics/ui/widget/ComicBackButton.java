package com.iversecomics.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import com.iversecomics.archie.android.R;

public class ComicBackButton extends TextView {

    public ComicBackButton(Context context, AttributeSet attrs) {
        super(context, attrs);
//        BitmapDrawable back = (BitmapDrawable) (context.getResources().getDrawable(R.drawable.button_back));
        setBackgroundResource(R.drawable.button_back);
        setTextColor(getResources().getColorStateList(R.drawable.button_text));
        setSingleLine();
        setGravity(Gravity.CENTER);
    }
}
