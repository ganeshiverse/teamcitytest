package com.babylon.controllers;

import java.util.Date;

public interface OnDateSetListener {
    public void onDateSet(Date date);
}