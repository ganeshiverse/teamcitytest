package com.babylon.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.utils.FontUtils;

public class TypefaceTextView extends TextView {
    public static final int TEXT_SIZE_LIMIT_FOR_NORMAL_FONT = 6;

    private boolean isFontScalable;

    public TypefaceTextView(Context context) {
        super(context);
    }

    public TypefaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initialize(context, attrs);
        }
    }

    public TypefaceTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void initialize(Context context, AttributeSet attrs) {
        TypedArray attrsArray = context.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView);
        if (attrsArray != null) {
            setupAttrs(attrsArray);
            attrsArray.recycle();
        }
    }

    private void setupAttrs(TypedArray attrsArray) {
        final int count = attrsArray.getIndexCount();

        if (count > 0) {
            applyCustomAttrs(attrsArray, count);
        } else {
            setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoRegular));
        }
    }

    private void applyCustomAttrs(TypedArray attrsArray, int count) {
        for (int i = 0; i < count; ++i) {
            int attr = attrsArray.getIndex(i);
            switch (attr) {
                case R.styleable.TypefaceTextView_font:
                    setTypeface(attrsArray);
                    break;
                case R.styleable.TypefaceTextView_fontScale:
                    setFontScale(attrsArray);
                    break;
            }
        }
    }

    private void setFontScale(TypedArray attrsArray) {
        isFontScalable = attrsArray.getBoolean(R.styleable.TypefaceTextView_fontScale, false);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (isFontScalable) {
            setFontSize(text.length());
        }

        super.setText(text, type);
    }

    private void setFontSize(int propertyTextSize) {
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(
                propertyTextSize <= TEXT_SIZE_LIMIT_FOR_NORMAL_FONT
                        ? R.dimen.monitor_me_property_normal_size : R.dimen.monitor_me_property_small_size
        ));
    }

    private void setTypeface(TypedArray attrsArray) {
        switch (attrsArray.getInteger(R.styleable.TypefaceTextView_font, 1)) {
            case 1:
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoRegular));
                break;
            case 2:
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoLight));
                break;
            case 3:
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoItalic));
                break;
            case 4:
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoBold));
                break;
            case 5:
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoMedium));
                break;
            case 6:
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoThin));
                break;
            default:
                break;
        }
    }

}
