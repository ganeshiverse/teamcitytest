package com.iversecomics.client.store.tasks;

import android.os.AsyncTask;

import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.json.IComicStoreParsingEvent;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.json.JSONArray;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class FetchSubGroupsTask extends AsyncTask<Void, Void, List<Group>> {
    private static final Logger LOG = LoggerFactory.getLogger(FetchSubGroupsTask.class);
    private String groupId;
    private List<Group> groupList = new LinkedList<Group>();
    private ComicStore comicStore;


    public FetchSubGroupsTask(ComicStore comicStore, String groupId) {
        this.groupId = groupId;
        this.comicStore = comicStore;
    }

    @Override
    protected List<Group> doInBackground(Void... parms) {
        return execTask();
    }

    @Override
    protected void onPostExecute(List<Group> result) {
        // textView.setText(result);
    }

    public List<Group> execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getGroupsByGroupUri(groupId);
            //ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            ResponseParser parser = new ResponseParser();

            JSONArray jarray = null;
            try {
                jarray = json.getJSONArray("results");
            } catch (JSONException e) {
                LOG.error("Couldn't parse json array.");
            }


            parser.parseGroups(jarray, updater);
            //server.updateFreshness(listId, Time.DAY);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update subgroups list");
        }

        return groupList;
    }

    public IComicStoreParsingEvent updater = new IComicStoreParsingEvent() {

        @Override
        public void onParsedPublisher(Publisher publisher) {
        }

        @Override
        public void onParsedGroup(Group group) {
            groupList.add(group);
        }

        @Override
        public void onParsedGenre(Genre genre) {
        }

        @Override
        public void onParsedComic(Comic comic) {
        }

        @Override
        public void onParsedFeaturedSlot(FeaturedSlot slot) {

        }

        @Override
        public void onParseStart() {
        }

        @Override
        public void onParseEnd() {
        }

        @Override
        public void onParsedPromotion(Promotion promotion) {

        }


    };


}