package com.iversecomics.otto.event;


public class SubscriptionSaveOffLineErrorEvent {
    private String errorCode;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    private String errorMessage;




    public SubscriptionSaveOffLineErrorEvent() {
    }

    public SubscriptionSaveOffLineErrorEvent(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }


}
