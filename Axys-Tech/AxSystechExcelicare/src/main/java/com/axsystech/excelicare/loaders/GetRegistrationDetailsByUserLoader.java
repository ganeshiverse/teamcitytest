package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.data.responses.RegistrationDetailsByUserResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetRegistrationDetailsByUserLoader extends DataAsyncTaskLibLoader<RegistrationDetailsByUserResponse> {

    private String userName;
    private String deviceId;
    private String siteId;
    private String reqSiteSelector;

    public GetRegistrationDetailsByUserLoader(final Context context, final String userName, final String deviceId, final String siteId, final String reqSiteSelector) {
        super(context);
        this.userName = userName;
        this.deviceId = deviceId;
        this.siteId = siteId;
        this.reqSiteSelector = reqSiteSelector;
    }

    @Override
    protected RegistrationDetailsByUserResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getRegistrationsDetailsByUser(userName, deviceId, siteId, reqSiteSelector);
    }
}