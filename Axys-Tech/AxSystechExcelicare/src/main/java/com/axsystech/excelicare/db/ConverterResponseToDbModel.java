package com.axsystech.excelicare.db;

import android.text.TextUtils;

import com.axsystech.excelicare.data.responses.GetFoldersListResponse;
import com.axsystech.excelicare.data.responses.GetMediaItemResponse;
import com.axsystech.excelicare.data.responses.ListMediaItemThumbNailsResponse;
import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.data.responses.SiteDetailsModel;
import com.axsystech.excelicare.data.responses.SystemPreferencesData;
import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.fragments.media.FilterType;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;

import java.util.ArrayList;

/**
 * This class holds methods to convert response model objects to database model objects.
 * Date: 16.05.14
 * Time: 16:37
 *
 * @author SomeswarReddy
 */
public final class ConverterResponseToDbModel {

    private ConverterResponseToDbModel() {
    }

    public static User getUser(final LoginResponse authResponse, String inputEmailID, String password) {
        final User user = new User();

        user.setEmail(inputEmailID);
        user.setPassword(password);

        if (authResponse != null && authResponse.getLoginDataObject() != null &&
                authResponse.getLoginDataObject().getLoginUser() != null) {
            final LoginResponse.LoginUserResponse userResponse = authResponse.getLoginDataObject().getLoginUser();

            if (userResponse.getUserDataObject() != null) {
                user.setEcUserID(userResponse.getUserDataObject().getEcUserID());
                user.setForename(userResponse.getUserDataObject().getForename());
                user.setPhoto(userResponse.getUserDataObject().getPhoto());
                user.setSiteID(userResponse.getUserDataObject().getSiteID());
                user.setSitename(userResponse.getUserDataObject().getSitename());
                user.setSurname(userResponse.getUserDataObject().getSurname());
                user.setUserDeviceID(userResponse.getUserDataObject().getUserDeviceID());
                user.setPatientID(userResponse.getUserDataObject().getPatientID());
                user.setToken(userResponse.getUserDataObject().getToken());

                if (TextUtils.isEmpty(inputEmailID)) {
                    user.setEmail(userResponse.getUserDataObject().getLoginName());
                }
            }
        }
        return user;
    }

    public static ArrayList<SiteDetailsDBModel> getSiteDetailsDBModel(ArrayList<SiteDetailsModel> siteDetailsResponseModels) {
        ArrayList<SiteDetailsDBModel> siteDetailsDBModelAL = null;

        if (siteDetailsResponseModels != null && siteDetailsResponseModels.size() > 0) {
            siteDetailsDBModelAL = new ArrayList<SiteDetailsDBModel>();

            for (SiteDetailsModel resModel : siteDetailsResponseModels) {
                SiteDetailsDBModel dbModel = new SiteDetailsDBModel();

                dbModel.setSiteId(resModel.getSiteId());
                dbModel.setSiteName(resModel.getSiteName());

                siteDetailsDBModelAL.add(dbModel);
            }
        }

        return siteDetailsDBModelAL;
    }

    public static ArrayList<MediaFoldersModel> getMediaFoldersDBModel(GetFoldersListResponse foldersResponse) {
        ArrayList<MediaFoldersModel> foldersModelArrayList = null;

        if (foldersResponse != null && foldersResponse.getFoldersDataObject() != null &&
                foldersResponse.getFoldersDataObject().getFoldersList() != null &&
                foldersResponse.getFoldersDataObject().getFoldersList().size() > 0) {

            foldersModelArrayList = new ArrayList<MediaFoldersModel>();

            for (GetFoldersListResponse.Folder folderObj : foldersResponse.getFoldersDataObject().getFoldersList()) {
                MediaFoldersModel modelObj = new MediaFoldersModel();

                modelObj.setMediaFolderId(folderObj.getId());
                modelObj.setMediaFolderName(folderObj.getName());
                modelObj.setMediaSubFolderId(folderObj.getSubFolderList().size() > 0 ? folderObj.getSubFolderList().get(0).getId() : 0);
                modelObj.setMediaSubFolderName(folderObj.getSubFolderList().size() > 0 ? folderObj.getSubFolderList().get(0).getName() : "");

                foldersModelArrayList.add(modelObj);
            }
        }

        return foldersModelArrayList;
    }

    public static ArrayList<MediaItemDbModel> getMediaItemsAsDbModel(User loginUser, ArrayList<ListMediaItemThumbNailsResponse.MediaItemServerModel> mediaItemServerModels) {
        ArrayList<MediaItemDbModel> dbModelAL = null;

        if (mediaItemServerModels != null && mediaItemServerModels.size() > 0) {
            dbModelAL = new ArrayList<MediaItemDbModel>();

            for (ListMediaItemThumbNailsResponse.MediaItemServerModel serverModel : mediaItemServerModels) {
                MediaItemDbModel dbModel = new MediaItemDbModel();

                // copy server data to db model object
                dbModel.setFileName(serverModel.getFileName());
                dbModel.setFileDuration(serverModel.getFileDuration());
                dbModel.setFolderId(serverModel.getFolderId());
                dbModel.setSubFolderId(serverModel.getSubfolderId());
                dbModel.setMediaId(serverModel.getId());
                dbModel.setIsUploaded(true);
                dbModel.setIsEdited(false);
                dbModel.setIsUploading(false);
                dbModel.setMediaName(serverModel.getName());
                dbModel.setMediaType(FilterType.getFilterTypeForFolderId(serverModel.getFolderId()));
                dbModel.setCreatedDate(CommonUtils.getMediaDateInMills(AxSysConstants.DF_MEDIA_ITEM_SERVER_DATE_FORMAT, serverModel.getCreatedDate()));
                dbModel.setLmd(CommonUtils.getMediaDateInMills(AxSysConstants.DF_MEDIA_ITEM_SERVER_DATE_FORMAT, serverModel.getLmd()));
                dbModel.setComments(serverModel.getComments());
                dbModel.setAnnotationMedia("");
                dbModel.setOriginalMedia("");
                dbModel.setPreviewMedia(serverModel.getPreviewMedia());
                dbModel.setSiteId(loginUser.getSiteID());
                dbModel.setEcUserID(loginUser.getEcUserID());

                dbModelAL.add(dbModel);
            }

        }

        return dbModelAL;
    }

    public static MediaItemDbModel getMediaItemDbModelForMediaDetailsServerObj(User loginUser, GetMediaItemResponse mediaItemResponse) {
        MediaItemDbModel dbModel = null;

        if (mediaItemResponse != null && mediaItemResponse.getMediaItemData() != null) {
            dbModel = new MediaItemDbModel();

            GetMediaItemResponse.MediaItemData serverModel = mediaItemResponse.getMediaItemData();

            // copy server data to db model object
            dbModel.setFileName(serverModel.getFileName());
            dbModel.setFileDuration(serverModel.getFileDuration());
            dbModel.setFolderId(serverModel.getFolderId());
            dbModel.setSubFolderId(serverModel.getSubfolderId());
            dbModel.setMediaId(serverModel.getMediaId());
            dbModel.setIsUploaded(true);
            dbModel.setIsEdited(false);
            dbModel.setIsUploading(false);
            dbModel.setMediaName(serverModel.getName());
            dbModel.setMediaType(FilterType.getFilterTypeForFolderId(serverModel.getFolderId()));
            dbModel.setCreatedDate(CommonUtils.getMediaDateInMills(AxSysConstants.DF_MEDIA_ITEM_SERVER_DATE_FORMAT, serverModel.getCreatedDate()));
            dbModel.setLmd(CommonUtils.getMediaDateInMills(AxSysConstants.DF_MEDIA_ITEM_SERVER_DATE_FORMAT, serverModel.getLmd()));
            dbModel.setComments(serverModel.getComments());
            dbModel.setAnnotationMedia("");
            dbModel.setOriginalMedia(serverModel.getOriginalURL());
            dbModel.setPreviewMedia(serverModel.getPreviewURL());
            dbModel.setSiteId(loginUser.getSiteID());
            dbModel.setEcUserID(loginUser.getEcUserID());
        }

        return dbModel;
    }

    public static DeviceUsersDbModel getDeviceUsersDbMobel(RegisterResponse.DeviceUserDataObject deviceUserResponseModel) {
        if(deviceUserResponseModel != null) {
            DeviceUsersDbModel deviceUsersDbModel = new DeviceUsersDbModel();

            deviceUsersDbModel.setAddress(deviceUserResponseModel.getAddress());
            deviceUsersDbModel.setDob(deviceUserResponseModel.getDob());
            deviceUsersDbModel.setDeviceId(deviceUserResponseModel.getDeviceID());
            deviceUsersDbModel.setEcUserId(deviceUserResponseModel.getEcUserID());
            deviceUsersDbModel.setForename(deviceUserResponseModel.getForename());
            deviceUsersDbModel.setMobileNo(deviceUserResponseModel.getMobileNo());
            deviceUsersDbModel.setPrimaryIdentifier(deviceUserResponseModel.getPrimary_Identifier());
            deviceUsersDbModel.setGender(deviceUserResponseModel.getSex());
            deviceUsersDbModel.setSiteID(deviceUserResponseModel.getSiteID());
            deviceUsersDbModel.setSurname(deviceUserResponseModel.getSurname());
            deviceUsersDbModel.setUserDeviceID(deviceUserResponseModel.getUserDeviceID());
            deviceUsersDbModel.setLoginName(deviceUserResponseModel.getLoginName());
            deviceUsersDbModel.setToken(deviceUserResponseModel.getToken());

            return deviceUsersDbModel;
        }

        return null;
    }

    public static SystemPreferenceDBModel getSystemPreferenceDBModel(SystemPreferencesData responseModel) {
        if(responseModel != null) {
            SystemPreferenceDBModel systemPreferenceDBModel = new SystemPreferenceDBModel();

            systemPreferenceDBModel.setId(responseModel.getId());
            systemPreferenceDBModel.setDescription(responseModel.getDescription());
            systemPreferenceDBModel.setValue(responseModel.getValue());

            return systemPreferenceDBModel;
        }

        return null;
    }

    public static ClientUrlAndUserDetailsDbModel getUserDetailsDbModel(String loginName, UsersDetailsObject responseModel) {
        ClientUrlAndUserDetailsDbModel clientUrlDbModel = null;
        if(responseModel != null) {
            clientUrlDbModel = new ClientUrlAndUserDetailsDbModel();

            if (!TextUtils.isEmpty(responseModel.getLoginName())) {
                clientUrlDbModel.setUserNameAndSiteId(responseModel.getLoginName().trim() + "|" + responseModel.getSiteID());
            } else {
                clientUrlDbModel.setUserNameAndSiteId(loginName.trim() + "|" + responseModel.getSiteID());
            }
            clientUrlDbModel.setEcUserId(!TextUtils.isEmpty(responseModel.getEcUserID()) ? responseModel.getEcUserID() : "");
            clientUrlDbModel.setLoginName(!TextUtils.isEmpty(responseModel.getLoginName()) ? responseModel.getLoginName() : loginName);
            clientUrlDbModel.setSiteId(!TextUtils.isEmpty(responseModel.getSiteID()) ? responseModel.getSiteID() : "");
            clientUrlDbModel.setSiteName(!TextUtils.isEmpty(responseModel.getSitename()) ? responseModel.getSitename() : "");
            clientUrlDbModel.setSitePhotoUrl(!TextUtils.isEmpty(responseModel.getSitePhotoURL()) ? responseModel.getSitePhotoURL() : "");
            clientUrlDbModel.setClientUrl(!TextUtils.isEmpty(responseModel.getClientURL()) ? responseModel.getClientURL() : "");
            clientUrlDbModel.setSiteDescription(!TextUtils.isEmpty(responseModel.getSiteDescription()) ? responseModel.getSiteDescription() : "");
            clientUrlDbModel.setSiteInfoLink(!TextUtils.isEmpty(responseModel.getSiteInfoLink()) ? responseModel.getSiteInfoLink() : "");
            clientUrlDbModel.setForename(!TextUtils.isEmpty(responseModel.getForename()) ? responseModel.getForename() : "");
            clientUrlDbModel.setDob(!TextUtils.isEmpty(responseModel.getDOB()) ? responseModel.getDOB() : "");
            clientUrlDbModel.setSurname(!TextUtils.isEmpty(responseModel.getSurname()) ? responseModel.getSurname() : "");
            clientUrlDbModel.setAge(!TextUtils.isEmpty(responseModel.getAge()) ? responseModel.getAge() : "");
            clientUrlDbModel.setGender(!TextUtils.isEmpty(responseModel.getGender()) ? responseModel.getGender() : "");
            clientUrlDbModel.setGenderValue(!TextUtils.isEmpty(responseModel.getGenderValue()) ? responseModel.getGenderValue() : "");
            clientUrlDbModel.setPhoto(!TextUtils.isEmpty(responseModel.getPhoto()) ? responseModel.getPhoto() : "");
        }

        return clientUrlDbModel;
    }

    public static MessageListItemDBModel getMessageListItemDbModel(MessagesInboxAndSentResponse.DataObject responseModel, String panelId, String siteId, long ecUserId) {
        MessageListItemDBModel dbModel = null;

        if(responseModel != null) {
            dbModel = new MessageListItemDBModel();

            dbModel.setMessageId(responseModel.getId());
            dbModel.setPanelId(panelId);
            dbModel.setSiteId(!TextUtils.isEmpty(siteId) ? siteId : "");
            dbModel.setEcUserId(ecUserId + "");
            dbModel.setReceivedDate(!TextUtils.isEmpty(responseModel.getReceivedOn()) ? responseModel.getReceivedOn() : "");
            dbModel.setSentDate(!TextUtils.isEmpty(responseModel.getSentDate()) ? responseModel.getSentDate() : "");
            dbModel.setMessageBody(!TextUtils.isEmpty(responseModel.getMessage()) ? responseModel.getMessage() : "");
            dbModel.setUserType(!TextUtils.isEmpty(responseModel.getUsertype()) ? responseModel.getUsertype() : "");
            dbModel.setPhotoUrl(!TextUtils.isEmpty(responseModel.getPhotourl()) ? responseModel.getPhotourl() : "");
            dbModel.setPriority(!TextUtils.isEmpty(responseModel.getPriority()) ? responseModel.getPriority() : "");
            dbModel.setReadStatus(!TextUtils.isEmpty(responseModel.getReadStatus()) ? responseModel.getReadStatus() : "");
            dbModel.setSender(!TextUtils.isEmpty(responseModel.getSender()) ? responseModel.getSender() : "");
            dbModel.setSentTo(!TextUtils.isEmpty(responseModel.getSentTo()) ? responseModel.getSentTo() : "");
            dbModel.setSubject(!TextUtils.isEmpty(responseModel.getSubject()) ? responseModel.getSubject() : "");
            dbModel.setAttachments(!TextUtils.isEmpty(responseModel.getAttachments()) ? responseModel.getAttachments() : "");
            dbModel.setHeader(!TextUtils.isEmpty(responseModel.getHeader()) ? responseModel.getHeader() : "");
            dbModel.setSpeciality(!TextUtils.isEmpty(responseModel.getSpeciality()) ? responseModel.getSpeciality() : "");
        }

        return dbModel;
    }

    public static ArrayList<MessageListItemDBModel> getMessageListDbModelsAL(ArrayList<MessagesInboxAndSentResponse.DataObject> responseModelsAL,
                                                                             String panelId, String siteId, long ecUserId) {
        ArrayList<MessageListItemDBModel> dbModelArrayList = null;

        if(responseModelsAL != null && responseModelsAL.size() > 0) {
            dbModelArrayList = new ArrayList<MessageListItemDBModel>();

            for(int idx = 0 ; idx < responseModelsAL.size() ; idx ++) {
                MessageListItemDBModel dbModel = getMessageListItemDbModel(responseModelsAL.get(idx), panelId, siteId, ecUserId);

                if(dbModel != null) {
                    dbModelArrayList.add(dbModel);
                }
            }

        }

        return dbModelArrayList;
    }

}