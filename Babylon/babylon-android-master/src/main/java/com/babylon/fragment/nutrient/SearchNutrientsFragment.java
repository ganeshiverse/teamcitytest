package com.babylon.fragment.nutrient;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.babylon.R;
import com.babylon.adapter.ChooseMealAdapter;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Nutrient;
import com.babylon.utils.Constants;

import java.util.List;

public class SearchNutrientsFragment extends BaseFragment
        implements AdapterView.OnItemClickListener {

    private List<Nutrient> foodList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_nutrient, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(R.string.calories_add_item);
        setBackButtonVisible(true);

        final ListView foodCaloriesListView = (ListView) view.findViewById(R.id.fragment_choose_meal_list_view);
        initList(foodCaloriesListView);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getFragmentManager().popBackStack();

        final Nutrient nutrient = foodList.get(position);
        onNutrientSelectedBroadcast(nutrient);
    }

    private void onNutrientSelectedBroadcast(Nutrient nutrient){
        final Intent intent = new Intent();

        intent.setAction(Constants.ACTION_FOOD_RECEIVE);
        intent.putExtra(Constants.EXTRA_FOOD, nutrient);
        android.support.v4.content.LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void initList(ListView foodCaloriesListView) {
        Bundle arguments = getArguments();

        if (arguments != null && arguments.get(Constants.EXTRA_FOODS) != null) {
            foodList = (List<Nutrient>) arguments.get(Constants.EXTRA_FOODS);
            ChooseMealAdapter chooseMealAdapter = new ChooseMealAdapter(getActivity(), foodList);
            foodCaloriesListView.setAdapter(chooseMealAdapter);
            foodCaloriesListView.setOnItemClickListener(this);
        }
    }
}
