package com.babylon.model;

import android.text.TextUtils;
import com.babylon.enums.ActivityType;
import com.babylon.sync.SyncUtils;

import java.util.Date;
import java.util.List;

public class PhysicalActivity extends ServerModel {

    private String id;
    private String intensity;
    private String title;
    private String source;

    private Integer calories;
    private Integer steps;
    private Integer type;
    private Integer duration;
    private Float distance;

    private Integer refreshed;

    private Date startDate;
    private Date endDate;

    public PhysicalActivity(ActivityType type) {
        initActivity(type);
    }

    public PhysicalActivity(long startTime, long endTime, ActivityType type, String source) {
        this.startDate = new Date(startTime);
        this.endDate = new Date(endTime);
        this.source = source;
        initActivity(type);
    }

    public PhysicalActivity(long startTime, long endTime, int calories, int steps, ActivityType type, String source) {
        this.startDate = new Date(startTime);
        this.endDate = new Date(endTime);
        this.calories = calories;
        this.steps = steps;
        this.source = source;
        initActivity(type);
    }

    public PhysicalActivity(long dateInMillis, ActivityType type) {
        startDate = new Date(dateInMillis);
        endDate = new Date(dateInMillis);
        initActivity(type);
    }

    public PhysicalActivity getServerValues() {
        PhysicalActivity newActivity = new PhysicalActivity();

        newActivity.setType(type);
        newActivity.setRefreshed(refreshed);
        newActivity.setCalories(calories);
        newActivity.setEndDate(endDate);
        newActivity.setStartDate(startDate);
        newActivity.setIntensity(intensity);
        newActivity.setSteps(steps);
        newActivity.setTitle(title);
        newActivity.setSource(source);
        newActivity.setDistanceInMeter(distance);

        //because server rejects this fields
        newActivity.id = null;
        newActivity.duration = null;

        return newActivity;
    }

    public PhysicalActivity() {
        initActivity();
    }

    public Integer getDuration() {
        return duration;
    }

    private void initActivity(ActivityType type) {
        title = type.getTitle();
        this.type = type.getValue();

        initActivity();
    }

    private void initActivity() {
        syncWithServer = SyncUtils.SYNC_TYPE_ADD;
        id = getRandomUUIDStr();
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Integer getSteps() {
        return getInt(steps);
    }

    public void setSteps(Integer steps) {
        this.steps = steps;
    }

    public Boolean isRefreshed() {
        return refreshed == 1;
    }

    public void setRefreshed(Integer refreshed) {
        this.refreshed = refreshed;
    }

    public Date getStartDate() {
        return startDate == null ? startDate : new Date(startDate.getTime());
    }

    public void setStartDate(Date startDate) {
        this.startDate = new Date(startDate.getTime());
    }

    public Date getEndDate() {
        return endDate == null ? endDate : new Date(endDate.getTime());
    }

    public void setEndDate(Date endDate) {
        this.endDate = new Date(endDate.getTime());
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistanceInMeter(Float distance) {
        this.distance = distance;
    }

    public boolean isEditable() {
        boolean result = false;

        if (!TextUtils.isEmpty(source)) {
            if (source.equalsIgnoreCase(Source.MANUAL)) {
                result = true;
            }
        }

        return result;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public static class PhysicalActivityList {
        private List<PhysicalActivity> activities;

        public List<PhysicalActivity> getActivities() {
            return activities;
        }

        public void setActivities(List<PhysicalActivity> activities) {
            this.activities = activities;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class PatchPhysicalActivity {
        private PhysicalActivity activity;

        public PhysicalActivity getActivity() {
            return activity;
        }

        public PatchPhysicalActivity(PhysicalActivity activity) {
            this.activity = activity.getServerValues();
        }
    }

    public static class Source {
        public static final String MANUAL = "manual";
        public static final String AUTO = "auto";
        public static final String SAMSUNG = "samsung";
        //also exist another source types from validic

        public static boolean isManual(String source) {
            boolean result = false;

            if (!TextUtils.isEmpty(source)) {
                if (source.toLowerCase().equals(MANUAL)) {
                    result = true;
                }
            }

            return result;
        }

        public static boolean isAuto(String source) {
            boolean result = false;

            if (!TextUtils.isEmpty(source)) {
                if (source.equalsIgnoreCase(AUTO)) {
                    result = true;
                }
            }

            return result;
        }

        public static boolean isValidic(String source) {
            boolean result = false;

            if (!TextUtils.isEmpty(source)) {
                if (!source.equalsIgnoreCase(AUTO) && !source.equalsIgnoreCase(MANUAL)) {
                    result = true;
                }
            }

            return result;
        }
    }
}
