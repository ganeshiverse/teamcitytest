package com.babylon.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntegerInputFilter implements InputFilter {

    private Pattern pattern;
    private float min;
    private float max;

    public IntegerInputFilter(float minValue, float maxValue) {
        pattern = Pattern.compile("^[0-9]{0,1}+[0-9]*$");
        min = minValue;
        max = maxValue;
    }


    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            Matcher matcher = pattern.matcher(dest);
            float input = Float.parseFloat(dest.toString() + source.toString());
            if (matcher.matches() && isInRange(min, max, input)) {
                return null;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "";
    }

    private boolean isInRange(float a, float b, float c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
