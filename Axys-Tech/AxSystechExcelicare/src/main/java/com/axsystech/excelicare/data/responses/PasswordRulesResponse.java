package com.axsystech.excelicare.data.responses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by someswar on 7/1/2015.
 */
public class PasswordRulesResponse extends BaseResponse {

    @SerializedName("data")
    private PasswordRulesDataObject passwordRulesDataObject;

    public PasswordRulesDataObject getPasswordRulesDataObject() {
        return passwordRulesDataObject;
    }

    public String getStrongPasswordPattern() {
        if (passwordRulesDataObject != null && passwordRulesDataObject.getStrongPasswordRule() != null &&
                !TextUtils.isEmpty(passwordRulesDataObject.getStrongPasswordRule().getRegexp())) {
            return passwordRulesDataObject.getStrongPasswordRule().getRegexp();
        }

        return "";
    }

    public String getMediumPasswordPattern() {
        if (passwordRulesDataObject != null && passwordRulesDataObject.getMediumPasswordRule() != null &&
                !TextUtils.isEmpty(passwordRulesDataObject.getMediumPasswordRule().getRegexp())) {
            return passwordRulesDataObject.getMediumPasswordRule().getRegexp();
        }

        return "";
    }

    public String getWeakPasswordPattern() {
        if (passwordRulesDataObject != null && passwordRulesDataObject.getWeakPasswordRule() != null &&
                !TextUtils.isEmpty(passwordRulesDataObject.getWeakPasswordRule().getRegexp())) {
            return passwordRulesDataObject.getWeakPasswordRule().getRegexp();
        }

        return "";
    }

    public String getStrongPasswordRuleMsg() {
        if (passwordRulesDataObject != null && passwordRulesDataObject.getStrongPasswordRule() != null &&
                !TextUtils.isEmpty(passwordRulesDataObject.getStrongPasswordRule().getRules())) {
            return passwordRulesDataObject.getStrongPasswordRule().getRules();
        }

        return "";
    }

    public String getMediumPasswordRuleMsg() {
        if (passwordRulesDataObject != null && passwordRulesDataObject.getMediumPasswordRule() != null &&
                !TextUtils.isEmpty(passwordRulesDataObject.getMediumPasswordRule().getRules())) {
            return passwordRulesDataObject.getMediumPasswordRule().getRules();
        }

        return "";
    }

    public String getWeakPasswordRuleMsg() {
        if (passwordRulesDataObject != null && passwordRulesDataObject.getWeakPasswordRule() != null &&
                !TextUtils.isEmpty(passwordRulesDataObject.getWeakPasswordRule().getRules())) {
            return passwordRulesDataObject.getWeakPasswordRule().getRules();
        }

        return "";
    }

    public boolean isStrongPassword(String inputStr) {
//        Pattern pattern = Pattern.compile(getStrongPasswordPattern(), Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(inputStr);
//        if (matcher.matches()) {
//            return true;
//        }

        return inputStr.matches(getStrongPasswordPattern());
    }

    public boolean isMediumPassword(String inputStr) {
//        Pattern pattern = Pattern.compile(getMediumPasswordPattern(), Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(inputStr);
//        if (matcher.matches()) {
//            return true;
//        }

        return inputStr.matches(getMediumPasswordPattern());
    }

    public boolean isWeakPassword(String inputStr) {
//        Pattern pattern = Pattern.compile(getWeakPasswordPattern(), Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(inputStr);
//        if (matcher.matches()) {
//            return true;
//        }

        return inputStr.matches(getWeakPasswordPattern());
    }

    public class PasswordRulesDataObject {
        @SerializedName("Strong")
        private PasswordRule strongPasswordRule;

        @SerializedName("Medium")
        private PasswordRule mediumPasswordRule;

        @SerializedName("Weak")
        private PasswordRule weakPasswordRule;

        public PasswordRule getStrongPasswordRule() {
            return strongPasswordRule;
        }

        public PasswordRule getMediumPasswordRule() {
            return mediumPasswordRule;
        }

        public PasswordRule getWeakPasswordRule() {
            return weakPasswordRule;
        }
    }

    public class PasswordRule {
        @SerializedName("Regexp")
        private String regexp;

        @SerializedName("Rules")
        private String rules;

        public String getRegexp() {
            return regexp;
        }

        public String getRules() {
            return rules;
        }
    }
}
