package com.babylon.sync;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.QuestionListGet;
import com.babylon.api.request.QuestionPost;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.Table;
import com.babylon.database.schema.FileEntrySchema;
import com.babylon.database.schema.MessageSchema;
import com.babylon.model.FileEntry;
import com.babylon.model.Message;
import com.babylon.utils.L;

import org.apache.http.HttpStatus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MessagesSync extends AbsSyncStrategy {

    private static final String TAG = MessagesSync.class.getSimpleName();
    public static final int SERVER_RESPONSE_LIMIT_REACHED = 429;

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        Cursor notSyncMessagesCursor = getUpdateCursor(contentResolver, Table.MESSAGE,
                MessageSchema.CONTENT_URI_QUESTIONS_WITH_FILES_URI, MessageSchema.QueryWithFiles.PROJECTION);
        while (notSyncMessagesCursor.moveToNext()) {
            Message messageForPush = Message.fromCursorWithFiles(notSyncMessagesCursor);

            syncWithServer(contentResolver, messageForPush);
        }
        notSyncMessagesCursor.close();
    }

    public Response<Message.Question> syncWithServer(ContentResolver contentResolver, Message messageForPush) {
        Response<Message.Question> response = postMessage(contentResolver, messageForPush);

        if (response != null && response.isSuccess()) {
            Message messageResult = response.getData().getQuestion();
            if (messageResult != null) {
                onSuccessfulSent(contentResolver, messageForPush, response);
            }
        } else {
            notifyAboutError(response);

            if (response.getStatus() == SERVER_RESPONSE_LIMIT_REACHED) {
                processLimitReachedError(response);
                deleteMessage(contentResolver, messageForPush);
            } else if(response.getStatus() == HttpStatus.SC_BAD_REQUEST) {
                deleteMessage(contentResolver, messageForPush);
            }
        }
        return response;
    }
    
    private void processLimitReachedError(Response<Message.Question> response) {
        if (!App.getInstance().isAppOnForeground()) {
            showLimitReachedNotification(response.getUserResponseError());
        }
    }

    private void onSuccessfulSent(ContentResolver contentResolver, Message messageForPush, Response<Message.Question>
            response) {
        Message messageResult;
        final List<Message> messages = new ArrayList<>();
        
        messageResult = response.getData().getQuestion();
        messages.add(messageResult);
        storeMessagesWithFiles(contentResolver, messages);
        deleteMessage(contentResolver, messageForPush);
        contentResolver.notifyChange(MessageSchema.CONTENT_URI_QUESTIONS_WITH_FILES_URI, null, false);
    }


    private Response<Message.Question> postMessage(ContentResolver contentResolver, Message message) {

        Message requestMessage = new Message();
                requestMessage.setDateSent(message.getSendDate());
        requestMessage.setText(message.getText());
        requestMessage.setFiles(new ArrayList<FileEntry>(
                getQuestionFiles(contentResolver, message.getId())));
        Response<Message.Question> response = new QuestionPost().withEntry(requestMessage).execute();

        return response;
    }


    private void showLimitReachedNotification(String errorText) {
        Notification notification = new Notification.Builder(App.getInstance())
                .setContentTitle(App.getInstance().getString(R.string.message_was_discarded))
                .setContentText(errorText)
                .setSmallIcon(R.drawable.ic_launcher)
                .build();

        NotificationManager manager = (NotificationManager) App.getInstance().getSystemService(Context
                .NOTIFICATION_SERVICE);

        manager.notify(0, notification);
    }

    private void notifyAboutError(Response<Message.Question> response) {
        final Context context = App.getInstance();
        Intent intent = new Intent();
        intent.setAction(SyncAdapter.ACTION_NETWORK_ERROR);
        intent.putExtra(SyncAdapter.KEY_RESPONSE_ENTITY, response);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public void deleteMessage(ContentResolver contentResolver, Message message) {
        StringBuilder selection = new StringBuilder();
        selection.append(MessageSchema.Columns.ID + "= ?");
        String[] selectionArgs = new String[]{message.getId()};

        contentResolver.delete(MessageSchema.CONTENT_URI, selection.toString(), selectionArgs);

        selection.setLength(0);

        selection.append(FileEntrySchema.Columns.QUESTION_ID + "= ?");
        selectionArgs = new String[]{message.getId()};
        contentResolver.delete(FileEntrySchema.CONTENT_URI, selection.toString(), selectionArgs);
    }

    protected List<Message> pullMessages(Bundle bundle) {
        List<Message> messages = null;
        Response<ResponseList<Message>> response = QuestionListGet.newQuestionListGet(0, 1000000).execute();
        if (response != null && response.isSuccess()) {
            messages = response.getData().getList();
        }
        return messages;
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        List<Message> messages = pullMessages(bundle);
        if (messages != null) {
            storeMessagesWithFiles(contentResolver, messages);
        }
    }

    private void storeMessagesWithFiles(ContentResolver contentResolver, List<Message> messages) {
        DatabaseOperationUtils.getInstance().saveMessagesSynchronously(messages, contentResolver);
        for (int i = 0; i < messages.size(); i++) {
            Message message = messages.get(i);
            final List<FileEntry> files = message.getFiles();

            if (files != null) {
                pullFilesEntry(files);
            }
        }
    }

    private void pullFilesEntry(List<FileEntry> files) {
        for (int i = 0; i < files.size(); i++) {
            FileEntry fileEntry = files.get(i);
            if (isFilePullNeeded(fileEntry)) {
                FileDownloader.getInstance().download(fileEntry);
            }
        }
    }

    public boolean isFilePullNeeded(FileEntry fileEntry) {
        boolean needToPullFile = true;
        File file = new File(fileEntry.getFilePath());

        if (file.exists() && file.length() == Long.valueOf(fileEntry.getSize())) {
            needToPullFile = false;
        }
        return needToPullFile;
    }

    private List<FileEntry> getQuestionFiles(ContentResolver contentResolver, String questionId) {
        List<FileEntry> fileEntries = null;

        String selection = TextUtils.concat(
                FileEntrySchema.Columns.SYNC_WITH_SERVER.getName(),
                "== ? AND ",
                FileEntrySchema.Columns.QUESTION_ID + "=?").toString();

        String[] selectionArgs = new String[]{"0", questionId};
        Cursor cursor = contentResolver.query(FileEntrySchema.CONTENT_URI,
                FileEntrySchema.Query.PROJECTION, selection, selectionArgs, null);
        if (cursor != null) {
            fileEntries = new ArrayList<FileEntry>();
            while (cursor.moveToNext()) {
                final FileEntry fileEntry = new FileEntry();
                fileEntry.createFromCursor(cursor);
                byte[] bytes = readFile(fileEntry.getFilePath());
                fileEntry.setData(bytes);
                fileEntries.add(fileEntry);
            }
            cursor.close();
        }
        return fileEntries;
    }

    private byte[] readFile(String filePath) {
        ByteArrayOutputStream buffer = null;
        try {
            FileInputStream inputStream = new FileInputStream(filePath);
            buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            try {
                inputStream.close();
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        } catch (IOException e) {
            L.e(TAG, e.toString(), e);
        }
        if (buffer != null) {
            return buffer.toByteArray();
        } else {
            return null;
        }
    }
}
