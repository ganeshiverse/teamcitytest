package com.iversecomics.client.downloads.internal;

import com.iversecomics.client.downloads.internal.data.DownloadStatus;

import org.apache.http.StatusLine;

/**
 * Unrecoverable Download Failure.
 */
public class DownloadFailedException extends DownloadException {
    private static final long serialVersionUID = -3774199165166649804L;

    public DownloadFailedException(DownloadStatus status, String detailMessage) {
        super(status, detailMessage);
    }

    public DownloadFailedException(DownloadStatus status, String detailMessage, Throwable throwable) {
        super(status, detailMessage, throwable);
    }

    public DownloadFailedException(StatusLine statusLine) {
        super(statusLine);
    }

    public DownloadFailedException(StatusLine statusLine, Throwable throwable) {
        super(statusLine, throwable);
    }
}
