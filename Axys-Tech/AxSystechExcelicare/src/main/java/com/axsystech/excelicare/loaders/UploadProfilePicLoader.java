package com.axsystech.excelicare.loaders;

import android.content.Context;
import android.net.Uri;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 28/08/15.
 */
public class UploadProfilePicLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String patientId;
    private Uri imageUri;

    public UploadProfilePicLoader(Context context, String token, String patientId, Uri imageUri) {
        super(context);
        this.token = token;
        this.patientId = patientId;
        this.imageUri = imageUri;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().uploadProfilePic(token, patientId, imageUri);
    }
}
