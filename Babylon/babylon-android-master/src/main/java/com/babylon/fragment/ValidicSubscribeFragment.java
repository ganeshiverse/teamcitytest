package com.babylon.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.babylon.R;
import com.babylon.api.request.ValidicSubscribePost;
import com.babylon.api.request.ValidicSubscribeUrlGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.utils.L;

public class ValidicSubscribeFragment extends BaseFragment {
    public static final String TAG = ValidicSubscribeFragment.class.getSimpleName();
    private WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_validic_subscribe, container, false);
    }

    @Override
    protected void onTopBarButtonPressed() {
        if (webView.canGoBack()) {
            WebBackForwardList mWebBackForwardList = webView.copyBackForwardList();

            String historyUrl = mWebBackForwardList.getItemAtIndex(mWebBackForwardList.getCurrentIndex() - 1).getUrl();
            if (historyUrl.contains("api.fitbit.com/oauth/authorize?oauth_token")) {
                super.onTopBarButtonPressed();
            } else {
                webView.goBack();
            }
        } else {
            super.onTopBarButtonPressed();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webView = (WebView) view.findViewById(R.id.webView);

        setTitle(R.string.validic_subscribe_title);
        setProgressBarVisible(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
                                     @Override
                                     public void onPageFinished(WebView view, String url) {
                                         super.onPageFinished(view, url);
                                         if (webView.getVisibility() != View.VISIBLE) {
                                             webView.setVisibility(View.VISIBLE);
                                         }
                                         setProgressBarVisible(false);
                                     }

                                     @Override
                                     public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                         setProgressBarVisible(true);
                                         L.d(TAG, url);

                                     }

                                     @Override
                                     public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                         L.d(TAG, url);
                                         return false;
                                     }
                                 }
        );

        new ValidicSubscribePost().execute(getActivity(), new RequestListener<Void>() {
                    @Override
                    public void onRequestComplete(Response<Void> response) {
                        final int alreadySubscribed = 409;
                        if (response.isSuccess() || response.getStatus() == alreadySubscribed) {
                            new ValidicSubscribeUrlGet().execute(getActivity(), new RequestListener<ValidicSubscribeUrlGet.SubscribeUrl>() {
                                @Override
                                public void onRequestComplete(Response<ValidicSubscribeUrlGet.SubscribeUrl> response) {
                                    processSubscriptionResponse(response);
                                }
                            });
                        } else {
                            showErrorMessage(response);
                        }
                    }
                }
        );

    }

    private void processSubscriptionResponse(Response<ValidicSubscribeUrlGet.SubscribeUrl> response) {
        if (response.isSuccess()) {
            if (response.getData() != null && !TextUtils.isEmpty(response.getData().getUrl())) {
                webView.loadUrl(response.getData().getUrl());
            }
        } else {
            showErrorMessage(response);
        }
    }

    private void showErrorMessage(Response response) {
        String text = response.getResponseError();

        if (TextUtils.isEmpty(text)) {
            text = getString(R.string.some_error);
        }

        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
        setProgressBarVisible(false);

        final TextView textView = (TextView) getView().findViewById(R.id.text_view);
        textView.setText(R.string.some_error);

    }
}
