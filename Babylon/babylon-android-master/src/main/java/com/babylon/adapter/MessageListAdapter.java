package com.babylon.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.babylon.PushBroadcastReceiver;
import com.babylon.R;
import com.babylon.fragment.AskFragment;
import com.babylon.images.ImageLoader;
import com.babylon.model.Comment;
import com.babylon.model.FileEntry;
import com.babylon.model.Message;
import com.babylon.model.push.NotifAskConsultant;
import com.babylon.utils.GsonWrapper;
import com.babylon.view.AudioButton;
import com.babylonpartners.babylon.HomePageActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MessageListAdapter extends BaseAdapter {

    private static final String TAG = MessageListAdapter.class.getSimpleName();

    private final List<Message> messages;
    private final Context context;

    public MessageListAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Message getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.list_item_message, null);
            viewHolder.dateTextView = (TextView) convertView.findViewById(R.id.message_date);
            viewHolder.messageTextView = (TextView) convertView.findViewById(R.id.text_view);
            viewHolder.subMessageLayout = (LinearLayout) convertView.findViewById(R.id.subMessageLayout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.subMessageLayout.removeAllViews();

        Message message = getItem(position);
        boolean isTextEmpty = TextUtils.isEmpty(message.getText());
        if (!isTextEmpty) {
            setMessageDate(viewHolder.dateTextView, message.getDate());
            setMessageText(viewHolder, message.getText());
        }
        viewHolder.dateTextView.setVisibility(isTextEmpty ? View.GONE : View.VISIBLE);
        viewHolder.messageTextView.setVisibility(isTextEmpty ? View.GONE : View.VISIBLE);

        processFiles(viewHolder, message);
        processComments(viewHolder, message);

        return convertView;
    }

    private void processFiles(ViewHolder viewHolder, Message message) {
        final List<FileEntry> files = message.getFiles();
        if (files != null) {
            for (int i = 0; i < files.size(); i++) {
                FileEntry fileEntry = files.get(i);
                addFile(viewHolder, fileEntry);
            }
        }
    }

    private void processComments(ViewHolder viewHolder, Message message) {
        final List<Comment> comments = message.getComments();
        if (comments != null) {
            for (int i = 0; i < comments.size(); i++) {
                Comment comment = comments.get(i);
                addComment(viewHolder, comment, message);
            }
        }
    }

    private void addComment(ViewHolder viewHolder, Comment comment, Message message) {
        String text = comment.getText();
        if (text != null) {
            final View view = View.inflate(context, R.layout.list_item_sub_message, null);
            view.findViewById(R.id.image_layout).setVisibility(View.GONE);
            view.findViewById(R.id.play_button).setVisibility(View.GONE);

            final TextView textView = (TextView) view.findViewById(R.id.text_view);
            viewHolder.subMessageLayout.addView(view);
            setSubMessageDate(view, comment.getDate());
            if (message.getStateId() == Message.SUGGEST_APPOINTMENT_STATE_ID) {
                text = text.replaceAll("<a>", "<u>");
                text = text.replaceAll("</a>", "</u>");


                textView.setText(Html.fromHtml(text));
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, HomePageActivity.class);
                        NotifAskConsultant parseData = new NotifAskConsultant();
                        parseData.setAction(PushBroadcastReceiver.PushAction.SUGGEST_CONSULTATION.toString());
                        intent.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, GsonWrapper.getGson().toJson(parseData));
                        context.startActivity(intent);
                    }
                });
            } else {
                textView.setText(text);
            }
        }
    }

    private void setSubMessageDate(View view, Long date) {
        final TextView dateTextView = (TextView) view.findViewById(R.id.message_date);
        setMessageDate(dateTextView, date);
    }

    private void setMessageText(ViewHolder viewHolder, String text) {
        if (!TextUtils.isEmpty(text)) {
            viewHolder.messageTextView.setText(text);
        } else {
            viewHolder.messageTextView.setVisibility(View.GONE);
        }
    }

    private void addFile(ViewHolder viewHolder, FileEntry fileEntry) {
        final String mimeType = fileEntry.getMimetype();
        if (mimeType != null) {
            View view = null;
            if (mimeType.contains("image")) {
                view = getImageView(fileEntry);
            } else if (mimeType.contains("video") || mimeType.contains("audio")) {
                view = getAudioView(fileEntry);
            }
            if (view != null) {
                setSubMessageDate(view, fileEntry.getDate());
                viewHolder.subMessageLayout.addView(view);
            }
        }
    }

    private View getImageView(FileEntry fileEntry) {
        View view = View.inflate(context, R.layout.list_item_sub_message, null);
        view.findViewById(R.id.text_view).setVisibility(View.GONE);
        view.findViewById(R.id.play_button).setVisibility(View.GONE);

        final ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        ImageLoader.getInstance().displayImage(imageView, progressBar, ImageLoader.BIG_IMAGE, false, true, fileEntry);

        return view;
    }

    private View getAudioView(final FileEntry fileEntry) {
        View view = null;
        if (fileEntry.getName() != null && new File(fileEntry.getFilePath()).exists()) {
            view = View.inflate(context, R.layout.list_item_sub_message, null);
            view.findViewById(R.id.image_layout).setVisibility(View.GONE);
            view.findViewById(R.id.text_view).setVisibility(View.GONE);

            final AudioButton audioButton = (AudioButton) view.findViewById(R.id.play_button);
            audioButton.setFilePath(fileEntry.getFilePath());
        }
        return view;
    }

    private void setMessageDate(TextView dateTextView, long date) {
        if (date != 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(AskFragment.ASK_DATE_PATTERN,
                    Locale.getDefault());
            dateTextView.setText(dateFormat.format(new Date(date)));
        } else {
            dateTextView.setVisibility(View.GONE);
        }
    }

    private class ViewHolder {
        LinearLayout subMessageLayout;
        TextView dateTextView;
        TextView messageTextView;
    }
}
