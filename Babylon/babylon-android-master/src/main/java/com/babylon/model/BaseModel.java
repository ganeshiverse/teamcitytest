package com.babylon.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.babylon.utils.FieldType;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public abstract class BaseModel implements Serializable {

    @SkipFieldInContentValues
    public static final String TAG = BaseModel.class.getSimpleName();

    public static Float getFloat(String value) {
        Float result = 0f;

        if (!TextUtils.isEmpty(value) && value.compareTo(".") != 0 && value.compareTo("-") != 0) {
            result = Float.valueOf(value);
        }

        return result;
    }

    public BaseModel() {
    }

    public static boolean isNotEmpty(List list) {
        return list != null && !list.isEmpty();
    }

    public static boolean getBoolean(String value) {
        boolean result = false;

        if (!TextUtils.isEmpty(value)) {
            result = Boolean.valueOf(value);
        }

        return result;
    }

    protected boolean getBoolean(Boolean value) {
        return value != null ? value : false;
    }

    protected Integer getInt(Integer value) {
        return value == null ? 0 : value;
    }

    protected Double getDouble(Double value) {
        return value == null ? 0d : value;
    }

    protected Integer getInt(String value) {
        Integer result = 0;

        if (!TextUtils.isEmpty(value)) {
            result = Integer.valueOf(value);
        }

        return result;
    }

    protected String getNotNullString(String value) {
        String result;

        if (value == null) {
            result = "";
        } else {
            result = value;
        }

        return result;
    }

    protected Long getLong(Long value) {
        return value == null ? 0 : value;
    }

    protected Long getLongMills(Long value) {
        return value == null ? 0 : value * 1000;
    }

    protected Long getLongSec(Long value) {
        return value == null ? 0 : value / 1000;
    }

    private List<Field> setAllFields(Class cls, List<Field> fields) {
        fields.addAll(Arrays.asList(cls.getDeclaredFields()));
        Class superCls = cls.getSuperclass();
        if (superCls != null) {
            setAllFields(superCls, fields);
        }
        return fields;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        List<Field> fields = new ArrayList<Field>();
        setAllFields(this.getClass(), fields);

        for (Field field : fields) {
            String fieldName = field.getName();
            try {
                if (shouldFieldSkipped(field)) {
                    continue;
                }
                String simpleName = field.getType().getSimpleName();
                FieldType fieldType = FieldType.fromClassName(simpleName, field.getType());
                field.setAccessible(true);
                Object value = field.get(this);
                if (fieldType != null && value != null) {
                    switch (fieldType) {
                        case INTEGER:
                            contentValues.put(fieldName, (Integer) value);
                            break;
                        case STRING:
                            contentValues.put(fieldName, (String) value);
                            break;
                        case BYTE:
                            contentValues.put(fieldName, (Byte) value);
                            break;
                        case SHORT:
                            contentValues.put(fieldName, (Short) value);
                            break;
                        case LONG:
                            contentValues.put(fieldName, (Long) value);
                            break;
                        case FLOAT:
                            contentValues.put(fieldName, (Float) value);
                            break;
                        case DOUBLE:
                            contentValues.put(fieldName, (Double) value);
                            break;
                        case BOOLEAN:
                            contentValues.put(fieldName, String.valueOf(value));
                            break;
                        case BLOB:
                            contentValues.put(fieldName, (byte[]) value);
                            break;
                        case DATE:
                            contentValues.put(fieldName, ((Date) value).getTime());
                            break;
                        case SERILAIZABLE:
                            contentValues.put(fieldName, GsonWrapper.getGson().toJson(value));
                            break;
                        default:
                            break;
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                L.e(TAG, e.toString(), e);
            }
        }

        return contentValues;
    }

    public void createFromCursor(Cursor cursor) {
        ContentValues contentValues = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
        List<Field> fields = new ArrayList<Field>();
        setAllFields(this.getClass(), fields);

        for (Field field : fields) {
            String fieldName = field.getName();
            try {
                if (shouldFieldSkipped(field)) {
                    continue;
                }
                String simpleName = field.getType().getSimpleName();
                FieldType fieldType = FieldType.fromClassName(simpleName, field.getType());
                field.setAccessible(true);
                if (fieldType != null) {
                    switch (fieldType) {
                        case INTEGER:
                            field.set(this, contentValues.getAsInteger(fieldName));
                            break;
                        case STRING:
                            field.set(this, contentValues.getAsString(fieldName));
                            break;
                        case BYTE:
                            field.set(this, contentValues.getAsByte(fieldName));
                            break;
                        case SHORT:
                            field.set(this, contentValues.getAsShort(fieldName));
                            break;
                        case LONG:
                            field.set(this, contentValues.getAsLong(fieldName));
                            break;
                        case FLOAT:
                            field.set(this, contentValues.getAsFloat(fieldName));
                            break;
                        case DOUBLE:
                            field.set(this, contentValues.getAsDouble(fieldName));
                            break;
                        case BOOLEAN:
                            field.set(this, contentValues.getAsBoolean(fieldName));
                            break;
                        case BLOB:
                            field.set(this, contentValues.getAsByteArray(fieldName));
                            break;
                        case DATE:
                            Long timeMs = contentValues.getAsLong(fieldName);
                            if (timeMs != null) {
                                field.set(this, new Date(timeMs));
                            }
                            break;
                        case SERILAIZABLE:
                            String json = contentValues.getAsString(fieldName);
                            field.set(this, GsonWrapper.getGson().fromJson(json, field.getGenericType()));
                            break;
                        default:
                            break;
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                L.e(TAG, e.toString(), e);
            }
        }
    }

    private boolean shouldFieldSkipped(Field field) {
        final String fieldName = field.getName();
        return field.isAnnotationPresent
                (SkipFieldInContentValues.class) || fieldName.contains("shadow$");
    }

    public long insertQuery(String tableName, SQLiteDatabase db) {
        long insert = db.insert(tableName, null, toContentValues());
        return insert;
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface SkipFieldInContentValues {
    }

    public BaseModel cloneObject() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream output = new ObjectOutputStream(byteArrayOutputStream);
            output.writeObject(this);
            output.close();

            byte[] bytes = byteArrayOutputStream.toByteArray();
            ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(bytes));
            return (BaseModel) input.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
