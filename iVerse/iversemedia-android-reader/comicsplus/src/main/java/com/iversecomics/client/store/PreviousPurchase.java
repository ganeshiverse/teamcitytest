package com.iversecomics.client.store;

import com.iversecomics.client.store.model.Comic;

public class PreviousPurchase {
    private String image_filename;
    private int tier;
    private int download_count;
    private String purchase_date;
    private String amount_usd;
    private String name;
    private String comic_bundle_name;

    public Comic toComic() {
        final Comic comic = new Comic();
        comic.setImageFileName(image_filename);
        comic.setPricingTier(tier);
        comic.setAmountUSD(amount_usd);
        comic.setName(name);
        comic.setComicBundleName(comic_bundle_name);
        return comic;
    }

    public String getImage_filename() {
        return image_filename;
    }

    public int getTier() {
        return tier;
    }

    public int getDownload_count() {
        return download_count;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public String getAmount_usd() {
        return amount_usd;
    }

    public String getName() {
        return name;
    }

    public String getComic_bundle_name() {
        return comic_bundle_name;
    }
}