package com.axsystech.excelicare.framework.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.data.responses.PrivacyPolicyResponse;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.loaders.PrivacyPolicyLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.NavigationUtils;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectView;

/**
 * Created by someswar on 6/30/2015.
 */
public class PrivacyPolicyActivity extends BaseActivity implements View.OnClickListener, ConfirmationDialogFragment.OnConfirmListener {

    private static final String GET_PRIVACY_POLICY_LOADER = "com.axsystech.excelicare.framework.ui.activities.PrivacyPolicyActivity.GET_PRIVACY_POLICY_LOADER";

    private static final int APP_QUIT_DIALOG_ID = 100;

    @InjectView(R.id.privacyPolicyWebView)
    private WebView privacyPolicyWebView;

  /*  @InjectView(R.id.footor)
    private LinearLayout footor;*/

    @InjectView(R.id.declineBtn)
    private TextView declineBtn;

    @InjectView(R.id.acceptBtn)
    private TextView acceptBtn;

    private NavigationUtils mNavigatationUtils = NavigationUtils.LOGIN;
    private String mInputEmailID = "";
    private boolean justDisplayPrivacyPolicy = false;
    private String selectedSiteId = AxSysConstants.DEFAULT_SITE_ID;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getApplicationContext(), getString(R.string.privacy_policy_text)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_privacy_policy);

        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        declineBtn.setTypeface(bold);
        acceptBtn.setTypeface(bold);

        readIntentData();
        addEventListeners();

        displayProgressLoader(false);
        final PrivacyPolicyLoader privacyPolicyLoader = new PrivacyPolicyLoader(this);
        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_PRIVACY_POLICY_LOADER), privacyPolicyLoader);
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_NAVIGATE_TO)) {
                mNavigatationUtils = mNavigatationUtils.getNavigationType(bundle.getString(AxSysConstants.EXTRA_NAVIGATE_TO, mNavigatationUtils.LOGIN.toString()));
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_JUST_DISPLAY_PRIVACY_POLICY)) {
                justDisplayPrivacyPolicy = bundle.getBoolean(AxSysConstants.EXTRA_JUST_DISPLAY_PRIVACY_POLICY, false);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_SITE_ID)) {
                selectedSiteId = bundle.getString(AxSysConstants.EXTRA_SELECTED_SITE_ID);
            }
        }
    }

    private void addEventListeners() {
        // Do not enable buttons when we are displaying privacy policy to user only for verification
        if (justDisplayPrivacyPolicy) {
            //footor.setVisibility(View.GONE);
            acceptBtn.setVisibility(View.GONE);
            declineBtn.setVisibility(View.GONE);
        } else {
            acceptBtn.setOnClickListener(this);
            declineBtn.setOnClickListener(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(GET_PRIVACY_POLICY_LOADER)) {

            if (result != null && result instanceof PrivacyPolicyResponse) {
                PrivacyPolicyResponse privacyResponse = (PrivacyPolicyResponse) result;

                if (privacyResponse.getPrivacyPolicyHtmlData() != null &&
                        !TextUtils.isEmpty(privacyResponse.getPrivacyPolicyHtmlData().getPrivacyHTML())) {
                    privacyPolicyWebView.loadData(privacyResponse.getPrivacyPolicyHtmlData().getPrivacyHTML(), "text/html; charset=UTF-8", null);
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptBtn:
                if (mNavigatationUtils == NavigationUtils.SIGNUP) {
                    // Navigate user to SIGNUP screen
                    Intent signUpIntent = new Intent(this, SignUpActivity.class);
                    signUpIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                    signUpIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, selectedSiteId);
                    startActivity(signUpIntent);
                    finish();
                }
                break;
            case R.id.declineBtn:
                displayQuitApplicationDialog();
                break;
        }
    }

    private void displayQuitApplicationDialog() {
        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.setup_cancelled_quit));
        bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, APP_QUIT_DIALOG_ID);
        bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
        confirmationDialogFragment.setArguments(bundle);
        showDialog(confirmationDialogFragment);
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == APP_QUIT_DIALOG_ID) {
            CommonUtils.quitApplication(this);
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == APP_QUIT_DIALOG_ID) {

        }
    }
}
