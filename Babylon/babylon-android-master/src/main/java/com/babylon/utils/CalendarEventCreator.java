package com.babylon.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.text.TextUtils;
import com.babylon.App;

import java.util.TimeZone;

public class CalendarEventCreator {

    private long startMillis;
    private long endMillis;
    private String title;

    public CalendarEventCreator(long startMillis, long endMillis, String title) {
        this.endMillis = endMillis;
        this.startMillis = startMillis;
        this.title = title;
    }

    /**
     * before colling this method update manifest with
     * <uses-permission android:name="android.permission.WRITE_CALENDAR" /> permission
     * @return true if event successfully created
     */
    public boolean create() {
        String calendarId = getCalendarsCursor();
        boolean result = false;

        if (!TextUtils.isEmpty(calendarId)) {
            Uri uri = createEvent(calendarId);
            if (uri != null) {
                result = true;
            }
        }


        return result;
    }

    public void startIntent(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        activity.startActivity(intent);
    }

    private Uri createEvent(String calendarId) {
        ContentResolver cr = App.getInstance().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, title);
        values.put(CalendarContract.Events.CALENDAR_ID, calendarId);
        values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        return uri;
    }

    private String getCalendarsCursor() {
        final String[] CALENDARS_PROJECTION = new String[]{
                CalendarContract.Calendars._ID,
        };
        final int PROJECTION_ID_INDEX = 0;

        ContentResolver cr = App.getInstance().getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        Cursor calendarCursors = cr.query(uri, CALENDARS_PROJECTION, null, null, null);

        String calendarId = null;
        if (calendarCursors.moveToFirst()) {
            calendarId = calendarCursors.getString(PROJECTION_ID_INDEX);
        }

        return calendarId;
    }
}
