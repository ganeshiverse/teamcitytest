package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.axsystech.excelicare.util.AESHelper;
import com.axsystech.excelicare.util.CryptoEngine;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by vvydesai on 9/15/2015.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CIRCLE_DETAILS)
public class CircleDetailsDbModel extends DBUtils implements Serializable {

    private static final long serialVersionUID = 2L;

    @DatabaseField(id = true, columnName = CIRCLE_USER_ID)
    private String userId;

    @DatabaseField(columnName = CIRCLE_NAME)
    private String circleName;

    @DatabaseField(columnName = CIRCLE_COMMENTS)
    private String comments;

    @DatabaseField(columnName = CIRCLE_ISPROTECTED)
    private String isProtected;

    @DatabaseField(columnName = CIRCLE_LMD)
    private String lmd;

    @DatabaseField(columnName = CIRCLE_START_DATE)
    private String startDate;

    @DatabaseField(columnName = CIRCLE_SITE_ID)
    private String siteId;

    @DatabaseField(columnName = CIRCLE_STOP_DATE)
    private String stopDate;

    @DatabaseField(columnName = CIRCLE_STOP_REASON_DESC)
    private String stopReasonDesc;

    @DatabaseField(columnName = CIRCLE_STOP_REASON_SLU)
    private String stopReasonSlu;

    @DatabaseField(columnName = CIRCLE_SYSTEM_CIRCLE_ID)
    private String systemCircleId;

    @DatabaseField(columnName = CIRCLE_USER_CIRCLE_ID)
    private String userCircleId;

    @DatabaseField(columnName = CIRCLE_DETAILS_USER_TYPE)
    private String userType;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() throws Exception {
        return CryptoEngine.decrypt(userId);
    }

    public void setUserId(String userId) throws Exception {
        this.userId = CryptoEngine.encrypt(userId);
    }

    public String getCircleName() throws Exception {
        return CryptoEngine.decrypt(circleName);
    }

    public void setCircleName(String circleName) throws Exception {
        this.circleName = CryptoEngine.encrypt(circleName);
    }

    public String getComments() throws Exception {
        return CryptoEngine.decrypt(comments);
    }

    public void setComments(String comments) throws Exception {
        this.comments = CryptoEngine.encrypt(comments);
    }

    public String getIsProtected() throws Exception {
        return CryptoEngine.decrypt(isProtected);
    }

    public void setIsProtected(String isProtected) throws Exception {
        this.isProtected = CryptoEngine.encrypt(isProtected);
    }

    public String getLmd() throws Exception {
        return CryptoEngine.decrypt(lmd);
    }

    public void setLmd(String lmd) throws Exception {
        this.lmd = CryptoEngine.encrypt(lmd);
    }

    public String getStartDate() throws Exception {
        return CryptoEngine.decrypt(startDate);
    }

    public void setStartDate(String startDate) throws Exception {
        this.startDate = CryptoEngine.encrypt(startDate);
    }

    public String getSiteId() throws Exception {
        return CryptoEngine.decrypt(siteId);
    }

    public void setSiteId(String siteId) throws Exception {
        this.siteId = CryptoEngine.encrypt(siteId);
    }

    public String getStopDate() throws Exception {
        return CryptoEngine.decrypt(stopDate);
    }

    public void setStopDate(String stopDate) throws Exception {
        this.stopDate = CryptoEngine.encrypt(stopDate);
    }

    public String getStopReasonDesc() throws Exception {
        return CryptoEngine.decrypt(stopReasonDesc);
    }

    public void setStopReasonDesc(String stopReasonDesc) throws Exception {
        this.stopReasonDesc = CryptoEngine.encrypt(stopReasonDesc);
    }

    public String getStopReasonSlu() throws Exception {
        return CryptoEngine.decrypt(stopReasonSlu);
    }

    public void setStopReasonSlu(String stopReasonSlu) throws Exception {
        this.stopReasonSlu = CryptoEngine.encrypt(stopReasonSlu);
    }

    public String getSystemCircleId() throws Exception {
        return CryptoEngine.decrypt(systemCircleId);
    }

    public void setSystemCircleId(String systemCircleId) throws Exception {
        this.systemCircleId = CryptoEngine.encrypt(systemCircleId);
    }

    public String getUserCircleId() {
        return userCircleId;
    }

    public void setUserCircleId(String userCircleId) {
        this.userCircleId = userCircleId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
