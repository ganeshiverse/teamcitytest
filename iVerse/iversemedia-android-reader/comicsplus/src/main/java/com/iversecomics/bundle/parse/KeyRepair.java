package com.iversecomics.bundle.parse;

import com.iversecomics.io.Digest;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class KeyRepair {
    private Map<String, byte[]> keymap;

    public KeyRepair() {
        keymap = new HashMap<String, byte[]>();
        /* iverse-comic-flashgordon-one-1.0.1-signed.apk */
        keymap.put("99ec9fa1f9fc7983952b92a4b1d702674cdc6849", new byte[]{0x56, 0x00, 0x5C, 0x6B, (byte) 0xB1,
                (byte) 0xF1, 0x18, 0x6F, (byte) 0xE6, (byte) 0xF5, 0x04, (byte) 0xB0, 0x7F, (byte) 0xCA, (byte) 0xD0,
                0x37});
        /* iverse-comic-flashgordon-one-1.0.2-signed.apk */
        keymap.put("0afa4641c67f13b9bdb06130885267b94fd3ac7b", new byte[]{(byte) 0xE2, (byte) 0xD9, 0x27, 0x12,
                (byte) 0x9E, (byte) 0x93, (byte) 0x85, (byte) 0xB9, (byte) 0x9A, 0x6E, (byte) 0xC0, 0x31, 0x45,
                (byte) 0xCD, (byte) 0xAD, 0x46});
        /* iverse-comic-flashgordon-one-1.0.3-signed.apk */
        keymap.put("2634f92220d5261964be3916e08e2a904b729c11", new byte[]{0x5B, 0x7F, (byte) 0xEE, (byte) 0x97,
                (byte) 0x9D, 0x3D, 0x1F, 0x57, 0x5A, (byte) 0xD3, (byte) 0xD6, (byte) 0xDF, (byte) 0xF1, (byte) 0xC6,
                (byte) 0xA6, 0x44});
        /* iverse-comic-flashgordon-two-1.0.1-signed.apk */
        keymap.put("f5b9fbaa50c77346db08f989fdf126d0d6fd83d0", new byte[]{0x1D, 0x60, 0x7A, 0x57, 0x38, (byte) 0xAE,
                (byte) 0x9E, 0x7A, 0x2F, (byte) 0xA5, (byte) 0x8F, (byte) 0xF9, (byte) 0xC9, 0x58, (byte) 0xDD, 0x7F});
        /* iverse-comic-flashgordon-two-1.0.2-signed.apk */
        keymap.put("f36ec4efb06fa703a9d2592a1822541d5cae06ed", new byte[]{(byte) 0xAF, (byte) 0xFE, (byte) 0xE2,
                (byte) 0x86, (byte) 0x93, (byte) 0xCA, (byte) 0xDD, 0x35, 0x12, 0x1C, (byte) 0xD9, (byte) 0xED,
                (byte) 0xFA, 0x34, 0x0C, 0x3B});
        /* iverse-comic-flashgordon-two-1.0.3-signed.apk */
        keymap.put("d0cf3144c608df2de56b1ae5d6bbe7de185000e9", new byte[]{(byte) 0xA6, (byte) 0x82, (byte) 0xD2,
                (byte) 0xA6, (byte) 0x83, (byte) 0xD4, (byte) 0xF7, 0x4D, 0x7D, (byte) 0xFA, 0x64, 0x62, (byte) 0x87,
                0x26, (byte) 0xC1, (byte) 0xBB});
        /* iverse-comic-hexed-one-1.0.1-signed.apk */
        keymap.put("80d3d401cbfb65f85a954664dc9be1f3485af092", new byte[]{(byte) 0xF0, 0x13, (byte) 0xCF, 0x6D,
                (byte) 0x99, 0x0F, 0x02, 0x04, (byte) 0xE3, (byte) 0xFD, (byte) 0x8C, (byte) 0xFB, 0x59, (byte) 0x9B,
                (byte) 0xED, 0x0A});
        /* iverse-comic-hexed-one-1.0.2-signed.apk */
        keymap.put("13b2701c5aab4d9c9b3450a368573ed22787e5fc", new byte[]{(byte) 0xED, (byte) 0xD2, (byte) 0xE2,
                0x07, (byte) 0xA2, (byte) 0xAD, (byte) 0xB2, 0x31, 0x41, (byte) 0xBF, 0x7E, (byte) 0xEA, 0x22, 0x59,
                0x49, (byte) 0xBE});
        /* iverse-comic-hexed-one-1.0.3-signed.apk */
        keymap.put("ac2899036935b948559109b1f54ff6135fe49745", new byte[]{0x54, 0x66, 0x5E, (byte) 0x93, (byte) 0xF3,
                (byte) 0xEF, 0x60, 0x45, 0x34, 0x5E, 0x3A, (byte) 0xA8, (byte) 0xC6, (byte) 0xD5, (byte) 0xAE, 0x53});
        /* iverse-comic-proof-one-1.0.1-signed.apk */
        keymap.put("d9891bba20a4ca14cc508f01974668b7da243ba0", new byte[]{(byte) 0x8E, (byte) 0xF4, 0x09, 0x70, 0x74,
                (byte) 0xC8, (byte) 0xFA, (byte) 0x84, 0x42, 0x6C, 0x0E, (byte) 0xA8, (byte) 0x8D, (byte) 0x9D,
                (byte) 0xB5, (byte) 0xD1});
        /* iverse-comic-proof-one-1.0.2-signed.apk */
        keymap.put("82d7a15a66b7d120978560f69348dec020555cb9", new byte[]{0x4D, 0x2A, (byte) 0x81, 0x3C, (byte) 0xF8,
                (byte) 0x8F, 0x58, 0x26, 0x58, 0x75, (byte) 0xC9, (byte) 0xD9, 0x2C, 0x1B, 0x5F, (byte) 0xA0});
        /* iverse-comic-proof-one-1.0.3-signed.apk */
        keymap.put("778e19fcecbbd102c5fd56a3ddfe6901fc582beb", new byte[]{0x74, (byte) 0xD2, 0x43, (byte) 0xD6,
                (byte) 0xB5, 0x5A, (byte) 0xEA, 0x7D, 0x79, (byte) 0xCE, 0x28, 0x6E, 0x61, 0x48, 0x41, (byte) 0x81});
        /* iverse-comic-proof-two-1.0.1-signed.apk */
        keymap.put("a566c5676820041c3b81118c5c96d14d7e7391a0", new byte[]{0x3F, (byte) 0xA9, (byte) 0xB6, 0x4E, 0x6D,
                (byte) 0x84, 0x66, 0x6A, 0x59, 0x34, 0x39, (byte) 0xE7, 0x30, (byte) 0xD4, (byte) 0xDB, 0x2E});
        /* iverse-comic-proof-two-1.0.2-signed.apk */
        keymap.put("b61da726ede14bd489e82d5945df1c9ad9630ef5", new byte[]{(byte) 0x99, 0x71, (byte) 0xEA,
                (byte) 0xAE, (byte) 0xA5, (byte) 0xEE, 0x41, 0x51, 0x34, (byte) 0xCA, (byte) 0x9F, (byte) 0x9F,
                (byte) 0x95, 0x37, 0x06, (byte) 0xF4});
        /* iverse-comic-proof-two-1.0.3-signed.apk */
        keymap.put("8785b05ea22ab7611ab8f0bf4abd59b4f63bc650", new byte[]{(byte) 0xB1, (byte) 0xCF, 0x0B,
                (byte) 0xD7, (byte) 0xEB, (byte) 0xB7, 0x46, (byte) 0x8E, 0x71, (byte) 0xDE, (byte) 0xC9, 0x06,
                (byte) 0xBA, 0x63, (byte) 0xF1, 0x63});
        /* iverse-comic-startrekcountdown-one-1.0.1-signed.apk */
        keymap.put("26ab33c1945dc99ab7f7ac4002a8fd4801ec2c15", new byte[]{0x2A, 0x60, 0x70, 0x7D, 0x57, (byte) 0xE1,
                (byte) 0xA3, (byte) 0xAD, (byte) 0xC4, (byte) 0xC3, 0x7E, 0x64, 0x2F, 0x48, (byte) 0x82, 0x73});
        /* iverse-comic-startrekcountdown-one-1.0.2-signed.apk */
        keymap.put("145b06613fae0b3d471c827a490414ef9518e2cf", new byte[]{(byte) 0xA6, 0x32, 0x64, 0x79, 0x7E,
                (byte) 0xD9, (byte) 0x81, 0x6B, 0x40, 0x5F, (byte) 0xBA, (byte) 0x85, 0x31, 0x7B, (byte) 0x8A, 0x63});
        /* iverse-comic-startrekcountdown-one-1.0.3-signed.apk */
        keymap.put("15dbbad8a3dd50b3cfef9628913419b3f6724611", new byte[]{0x2B, (byte) 0xE0, 0x55, 0x2C, 0x49,
                (byte) 0xCF, (byte) 0xA5, (byte) 0xEB, (byte) 0xA2, (byte) 0x89, (byte) 0xBB, 0x22, (byte) 0xB3, 0x2D,
                (byte) 0xFD, (byte) 0xFA});
        /* iverse-comic-startrekcountdown-two-1.0.1-signed.apk */
        keymap.put("edd17fa5f2e3ca12863e7054d327376963e1ff5f", new byte[]{(byte) 0x84, (byte) 0x89, 0x29, 0x38,
                (byte) 0x9F, (byte) 0xCF, (byte) 0xFA, (byte) 0x8E, 0x5D, (byte) 0x98, (byte) 0xD8, 0x46, (byte) 0xA8,
                (byte) 0xD3, 0x05, (byte) 0xE1});
        /* iverse-comic-startrekcountdown-two-1.0.2-signed.apk */
        keymap.put("c233af7c62825e5e4c7e11f313b54e0b4775c1ee", new byte[]{0x12, 0x7E, 0x03, (byte) 0x9F, (byte) 0xCE,
                (byte) 0xDC, (byte) 0x9E, (byte) 0xC7, 0x75, 0x05, (byte) 0x88, 0x23, 0x7E, (byte) 0xD5, (byte) 0x88,
                0x3C});
        /* iverse-comic-startrekcountdown-two-1.0.3-signed.apk */
        keymap.put("5439bc0a863d96afb9a6552fbaa4352e80794982", new byte[]{(byte) 0xAD, 0x34, 0x6D, (byte) 0xD4,
                (byte) 0x86, 0x63, 0x02, (byte) 0x99, (byte) 0xA1, 0x44, 0x6E, 0x1E, (byte) 0xB9, 0x36, (byte) 0xB9,
                0x3B});

        /* iverse-comic-flashgordon-one-1.0.4-signed.apk */
        keymap.put("e7153587a0f44ec996613e0486bacdae9774561e", new byte[]{0x5F, (byte) 0xEA, 0x55, 0x0D, (byte) 0x81,
                (byte) 0xA2, (byte) 0xD9, (byte) 0xFC, (byte) 0xC8, 0x72, (byte) 0xBE, (byte) 0xDB, (byte) 0xD0,
                (byte) 0xC8, 0x37, 0x23});
        /* iverse-comic-flashgordon-two-1.0.4-signed.apk */
        keymap.put("c7113deb0d058069a326cfa32f28b473a2031762", new byte[]{0x3C, (byte) 0xC2, 0x58, (byte) 0x8F,
                (byte) 0xCF, 0x0F, (byte) 0xBD, 0x18, (byte) 0xEE, 0x42, 0x3C, 0x1E, 0x03, 0x6B, 0x13, 0x7F});
        /* iverse-comic-hexed-one-1.0.4-signed.apk */
        keymap.put("a9e4978d4cef8254a52cf27605609c299ce9ae4d", new byte[]{0x50, 0x46, 0x27, 0x45, (byte) 0x90, 0x57,
                (byte) 0xD7, 0x78, (byte) 0xEF, (byte) 0xD7, (byte) 0x8A, 0x7A, 0x39, (byte) 0x92, 0x1D, (byte) 0xA6});
        /* iverse-comic-proof-one-1.0.4-signed.apk */
        keymap.put("e862ca47840e428412acaeca9d17b1f35cf44fbc", new byte[]{0x40, 0x3F, (byte) 0x91, 0x1C, 0x7E,
                (byte) 0xEA, 0x65, (byte) 0x8E, 0x00, 0x05, (byte) 0xBC, (byte) 0xC2, 0x70, (byte) 0x9C, (byte) 0xAF,
                (byte) 0xD2});
        /* iverse-comic-proof-two-1.0.4-signed.apk */
        keymap.put("c27333534418770fe9dbb874915310f524addc7d", new byte[]{0x1B, 0x4C, (byte) 0xB5, (byte) 0xF5,
                (byte) 0xB9, 0x0C, (byte) 0xF4, 0x6A, (byte) 0xF7, (byte) 0x8B, 0x60, 0x0B, 0x6F, 0x42, 0x6A, 0x37});
        /* iverse-comic-startrekcountdown-one-1.0.4-signed.apk */
        keymap.put("4c9dd8de7b4c39e62fbada492afc66f6f0a4e9ae", new byte[]{0x0B, (byte) 0x86, 0x1E, 0x7D, 0x0F, 0x61,
                (byte) 0xE0, 0x45, (byte) 0x99, 0x2F, 0x28, (byte) 0xE4, 0x28, (byte) 0xD0, (byte) 0xEF, (byte) 0xF1});
        /* iverse-comic-startrekcountdown-two-1.0.4-signed.apk */
        keymap.put("5d4a21907d870fefe2e00b306b491d87857f17a2", new byte[]{(byte) 0xA4, (byte) 0x9D, (byte) 0xC4,
                (byte) 0xC5, (byte) 0xB6, 0x69, (byte) 0xFB, 0x07, 0x03, (byte) 0xCE, (byte) 0xDB, 0x7F, (byte) 0xC5,
                0x23, (byte) 0xD2, (byte) 0xBB});

    }

    /**
     * The Encrypted Title Stream.
     *
     * @param encryptedStream
     * @return
     * @throws java.io.IOException
     */
    public byte[] getCorrectKey(InputStream encryptedStream) throws IOException {
        String hash = Digest.sha1(encryptedStream);
        return keymap.get(hash);
    }
}
