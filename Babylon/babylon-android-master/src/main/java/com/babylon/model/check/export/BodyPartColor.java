package com.babylon.model.check.export;

public class BodyPartColor extends BaseExportModel {

	int bodyPartId;
	String rgb;

	public int getBodyPartId() {
		return bodyPartId;
	}

	public void setBodyPartId(int bodyPartId) {
		this.bodyPartId = bodyPartId;
	}

	public String getRgb() {
		return rgb;
	}

	public void setRgb(String rgb) {
		this.rgb = rgb;
	}
}
