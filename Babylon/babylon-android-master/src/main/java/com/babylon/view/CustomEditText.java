package com.babylon.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.*;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.utils.DecimalDigitsInputFilter;
import com.babylon.utils.IntegerInputFilter;
import com.babylon.utils.UIUtils;
import com.babylon.validator.FormatterInterface;
import com.babylon.validator.ValidationController;

public class CustomEditText extends LinearLayout {
    //private static final String TAG = CustomEditText.class.getSimpleName();
    private EditText editText;
    private boolean errorStatus = false;
    private static final int LINE = 0;
    private static final int BOX = 1;
    private static final int BOX_FRAME = 2;
    private int type = BOX;
    private int inputType = InputType.TYPE_CLASS_TEXT;
    private int hintResId = 0;
    private int starVisibility = View.GONE;
    private int errorText = 0;
    private int titleText = 0;
    private int imeOptions = 0;
    private TextView errorTextView;
    private int editTextPadding;
    private int boxEditTextLeftPadding;
    private ImageView starImage;
    private int enteredTextAppearance;
    private int editTextId;
    private boolean mandatory = false;
    private int maxLength = 0;
    private boolean noSpecialSymbols = false;
    private boolean lettersOnly = false;
    private boolean singleLine = false;
    private String emptyValidationErrorText = null;
    private String digits = null;

    private FormatterInterface formatter;

    public CustomEditText(Context context) {
        this(context, null);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText);

        editTextPadding = UIUtils.getPxFromDp(getContext(), 0);
        boxEditTextLeftPadding = UIUtils.getPxFromDp(getContext(), 15);

        enteredTextAppearance = a.getResourceId(R.styleable.CustomEditText_enteredTextAppearance, 0);
        errorText = a.getResourceId(R.styleable.CustomEditText_errorText, 0);
        titleText = a.getResourceId(R.styleable.CustomEditText_titleText, 0);
        starVisibility = a.getInt(R.styleable.CustomEditText_star, GONE);
        type = a.getInt(R.styleable.CustomEditText_editTextType, BOX);
        inputType = a.getInt(R.styleable.CustomEditText_inputType, InputType.TYPE_CLASS_TEXT);
        editTextId = a.getInt(R.styleable.CustomEditText_editTextId, InputType.TYPE_CLASS_TEXT);
        imeOptions = a.getInt(R.styleable.CustomEditText_imeOptions, EditorInfo.IME_ACTION_DONE);
        hintResId = a.getResourceId(R.styleable.CustomEditText_hint, 0);
        mandatory = a.getBoolean(R.styleable.CustomEditText_mandatory, false);
        maxLength = a.getInt(R.styleable.CustomEditText_maxLength, 0);
        noSpecialSymbols = a.getBoolean(R.styleable.CustomEditText_noSpecialSymbols, false);
        lettersOnly = a.getBoolean(R.styleable.CustomEditText_lettersOnly, false);
        singleLine = a.getBoolean(R.styleable.CustomEditText_singleLine, false);
        digits = a.getString(R.styleable.CustomEditText_digits);

        a.recycle();

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_edit_text, this);

        starImage = (ImageView) view.findViewById(R.id.image);
        starImage.setVisibility(starVisibility == 8 ? View.GONE : View.VISIBLE);

        if (starVisibility == VISIBLE) {
            setMandatory(true);
        }

        errorTextView = (TextView) view.findViewById(R.id.error_text);
        if (errorText != 0) {
            errorTextView.setText(errorText);
        }

        editText = (EditText) view.findViewById(R.id.edit_text);
        //we have to set unique ID in the layout
        editText.setId(editTextId);
        editText.setInputType(inputType);
        editText.setSingleLine(singleLine);

        if (imeOptions != 0) {
            editText.setImeOptions(imeOptions);
        }

        if (enteredTextAppearance != 0) {
            editText.setTextAppearance(getContext(), enteredTextAppearance);
        }

        if (hintResId != 0) {
            editText.setHint(hintResId);
        }

        if (!TextUtils.isEmpty(digits)) {
            editText.setKeyListener(DigitsKeyListener.getInstance(digits));
        }

        InputFilter maxLengthFilter = null;
        if (maxLength != 0) {
            maxLengthFilter = new InputFilter.LengthFilter(maxLength);
        }

        InputFilter symbolsFilter = null;
        if (noSpecialSymbols) {
            symbolsFilter = new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
                                           int dend) {
                    for (int i = start; i < end; i++) {
                        if (!Character.isLetterOrDigit(source.charAt(i))) {
                            return "";
                        }
                    }
                    return null;
                }
            };
        } else if (lettersOnly) {
            symbolsFilter = new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
                                           int dend) {
                    for (int i = start; i < end; i++) {
                        if (!Character.isLetter(source.charAt(i)) && !"-".equals(String.valueOf(source.charAt(i)))) {
                            return "";
                        }
                    }
                    return null;
                }
            };
        }

        if (maxLengthFilter != null && symbolsFilter != null) {
            editText.setFilters(new InputFilter[]{maxLengthFilter, symbolsFilter});
        } else if (maxLengthFilter != null) {
            editText.setFilters(new InputFilter[]{maxLengthFilter});
        } else if (symbolsFilter != null) {
            editText.setFilters(new InputFilter[]{symbolsFilter});
        }

        setDefaultStatus();
    }

    public void setTextColor(int textColorId, int hintTextColor) {
        editText.setTextColor(getResources().getColor(textColorId));
        editText.setHintTextColor(getResources().getColor(hintTextColor));
    }

    public void setHintTextColor(int hintTextColor) {
        editText.setHintTextColor(getResources().getColor(hintTextColor));
    }

    public void setDefaultStatus() {
        this.editText.setError(null);
        errorStatus = false;

        switch (type) {
            case LINE:
                errorTextView.setVisibility(GONE);
                editText.setBackgroundColor(android.R.color.transparent);
                //editText.setBackgroundResource(R.drawable.edit_text_line);
                //editText.setPadding(editTextPadding, editTextPadding, editTextPadding, editTextPadding);
                break;
            case BOX:
                //editText.setBackgroundResource(R.drawable.edit_text_box);
                //editText.setPadding(boxEditTextLeftPadding, editTextPadding, editTextPadding, editTextPadding);
                break;
            case BOX_FRAME:
                editText.setBackgroundResource(R.drawable.checkbox);
                //editText.setPadding(boxEditTextLeftPadding, editTextPadding, editTextPadding, editTextPadding);
                break;
        }

        if (inputType == InputType.TYPE_TEXT_FLAG_MULTI_LINE) {
            editText.setSingleLine(false);
        }

        starImage.setImageResource(R.drawable.ic_reqfield_default);
    }

    public void setErrorStatus() {
        setErrorStatus("");
    }

    public void setErrorStatus(String text) {
        this.errorTextView.setText(text);
        //this.editText.setError(text);
        Drawable dr = getContext().getResources().
                getDrawable(R.drawable.ic_reqfield_error);
        dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
        this.editText.setError(text, dr);
        errorStatus = true;

        switch (type) {
            case LINE:
                /*if (!TextUtils.isEmpty(text)) {
                    errorTextView.setVisibility(VISIBLE);
                }*/
                //editText.setBackgroundResource(R.drawable.edittext_error);
                //editText.setPadding(editTextPadding, editTextPadding, editTextPadding, editTextPadding);
                break;
            case BOX:
                //editText.setBackgroundResource(R.drawable.edittext_box_error);
                //editText.setPadding(boxEditTextLeftPadding, editTextPadding, editTextPadding, editTextPadding);
                break;
            case BOX_FRAME:
                //editText.setBackgroundResource(R.drawable.edittext_box_error);
                //editText.setPadding(boxEditTextLeftPadding, editTextPadding, editTextPadding, editTextPadding);
                break;
            default:
                break;
        }

        if (inputType == InputType.TYPE_TEXT_FLAG_MULTI_LINE) {
            editText.setSingleLine(false);
        }
        starImage.setImageResource(R.drawable.ic_reqfield_error);
    }

    public void validationAfterTextChanged(final ValidationController validationController) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                validationController.performValidationEditText(CustomEditText.this);
            }
        });
    }

    public String format() {
        String source = getEditText().getText().toString();

        String output = source;
        if (formatter != null) {
            output = formatter.format(source);
            if (!source.equals(output)) {
                getEditText().setText(output);
            }
        }

        return output;
    }

    public String getEmptyValidationErrorText() {
        return emptyValidationErrorText;
    }

    public void setEmptyValidationErrorText(String emptyValidationErrorText) {
        this.emptyValidationErrorText = emptyValidationErrorText;
    }

    public void addDecimalMinMaxValueFilter(int digitsBeforeZero, int digitsAfterZero, float minValue, float maxValue) {
        addDecimalMinMaxValueFilter(digitsBeforeZero, digitsAfterZero, minValue, maxValue, false);
    }

    public void addDecimalMinMaxValueFilter(int digitsBeforeZero, int digitsAfterZero, float minValue, float maxValue,
                                            boolean isSingle) {
        addFilter(new DecimalDigitsInputFilter(digitsBeforeZero, digitsAfterZero, minValue, maxValue), isSingle);
    }

    public void addIntegerMinMaxValueFilter(float minValue, float maxValue) {
        addIntegerMinMaxValueFilter(minValue, maxValue, false);
    }

    public void addIntegerMinMaxValueFilter(float minValue, float maxValue, boolean isSingle) {
        addFilter(new IntegerInputFilter(minValue, maxValue), isSingle);
    }

    public void addFilter(InputFilter newFilter, boolean isSingle) {
        InputFilter[] filters = editText.getFilters();
        if (filters != null && !isSingle) {
            filters = appendIntentFilter(filters, newFilter);
        } else {
            filters = new InputFilter[]{newFilter};
        }
        editText.setFilters(filters);
    }

    private InputFilter[] appendIntentFilter(InputFilter[] filters, InputFilter lengthFilter) {
        int length = filters.length;
        InputFilter[] result = new InputFilter[length + 1];
        for (int i = 0; i < length; i++) {
            result[i] = filters[i];
        }
        result[result.length - 1] = lengthFilter;

        return result;
    }

    public void clearFilters() {
        editText.setFilters(null);
    }


    public boolean isErrorStatus() {
        return errorStatus;
    }

    public String getText() {
        return editText.getText().toString().trim();
    }

    public void setText(String text) {
        editText.setText(text);
    }

    public EditText getEditText() {
        return editText;
    }

    public void setHintText(String text) {
        editText.setHint(text);
    }

    public TextView getErrorTextView() {
        return errorTextView;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public FormatterInterface getFormatter() {
        return formatter;
    }

    public void setFormatter(FormatterInterface formatter) {
        this.formatter = formatter;
    }

    public String getTitleText() {
        return titleText != 0 ? getResources().getString(titleText) : null;
    }

}
