package com.iversecomics.ui;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.events.IComicChooser;
import com.iversecomics.client.store.events.OnComicClickListener;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.tasks.SearchStoreTask;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.SearchCompletedEvent;
import com.iversecomics.ui.slidelist.SlideableComicListAdapter;
import com.iversecomics.ui.slidelist.SlideableListFragment;
import com.iversecomics.ui.widget.ComicReaderSeekBar;
import com.iversecomics.ui.widget.NavigationBar;
import com.iversecomics.util.ui.UiUtil;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.otto.Subscribe;

/**
 */
public class SearchResultsFragment extends SlideableListFragment implements IComicChooser {

    public static final String ARG_SEARCH_TERM = "com.iverse.search_term";
    private static final String QUERY = "query";

    private ComicListFragment.ComicListListener mListener;

    private OnComicClickListener mComicClickListener;
    private EditText mSearchEditText;
    private String startQuery;
    private View processingOverlay;


    public static SearchResultsFragment newInstance(String searchTerm) {
        SearchResultsFragment frag = new SearchResultsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SEARCH_TERM, searchTerm);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_results, container, false);

        initNavBar(rootView);

        processingOverlay = rootView.findViewById(R.id.processing_overlay);

        ComicStore.DatabaseApi db = IverseApplication.getApplication().getComicStore().getDatabaseApi();
        db.clearOldSearchResults();
        Cursor cursor = db.getCursorComicsSearch();

        adapter = new ComicListAdapter(getActivity(), cursor);

        mGrid = (TwoWayGridView) rootView.findViewById(com.iversecomics.archie.android.R.id.grid);
        // TODO
//        mGrid.setEmptyView(emptyView);
        mGrid.setLongClickable(true);
        mGrid.setNumRows(mNumRows);
        mGrid.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                Comic comic = (Comic) view.getTag();
                mListener.onComicClicked(comic);
            }
        });
        mGrid.setEmptyView(rootView.findViewById(R.id.empty_view));
        mGrid.setAdapter(adapter);


        initSeekBar((ComicReaderSeekBar) rootView.findViewById(R.id.seekbar));

        initSearch(rootView);

        if (savedInstanceState != null && savedInstanceState.containsKey(QUERY)) {
            startQuery = savedInstanceState.getString(QUERY);
        }
        if (startQuery == null && getArguments() != null) {
            startQuery = getArguments().getString(ARG_SEARCH_TERM);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        OttoBusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        OttoBusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onSearchOver(SearchCompletedEvent event) {
        if (event.getQuery().equals(startQuery)) {
            hideLoadingDialog();
        }
    }

    private void initNavBar(View rootView) {
        NavigationBar navBar = (NavigationBar) rootView.findViewById(R.id.navbar);
        navBar.setNavigationListener((NavigationBar.NavigationListener) getActivity());
        navBar.setMenuButtonVisibility(View.VISIBLE);
        navBar.setMyComicsVisibility(View.VISIBLE);
    }

    private void initSearch(View mRootView) {
        // text search
        mSearchEditText = (EditText) (mRootView.findViewById(R.id.txtSearch));
        // handle soft keyboard's search button
        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchForProducts(mSearchEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });

        // search button
        View btnSearch = mRootView.findViewById(R.id.btnSearch);
        UiUtil.applyButtonEffect(btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchForProducts(mSearchEditText.getText().toString());
            }
        });
    }

    private void searchForProducts(String query) {
        //Save Offline change
        if(ConnectionHelper.isConnectedToInternet(getActivity()))
            showLoadingDialog();
        hideKeyboard();
        startQuery = query.trim();
        mSearchEditText.setText(query);

        ComicStore comicStore = IverseApplication.getApplication().getComicStore();
        // Clear out old results!
        comicStore.getDatabaseApi().clearOldSearchResults();

        Task task = new SearchStoreTask(comicStore, query);
        IverseApplication.getApplication().getTaskPool().submit(task);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (ComicListFragment.ComicListListener) activity;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // execute the search task, there is no callback for this task
        // instead the database is updated and the cursor attached to the adapter will be notified.
        if (startQuery != null) {
            searchForProducts(startQuery);
        }
    }

    @Override
    public OnComicClickListener getOnComicClickListener() {
        return mComicClickListener;
    }

    @Override
    public void setOnComicClickListener(OnComicClickListener listener) {
        this.mComicClickListener = listener;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(QUERY, startQuery);
    }

    public void showLoadingDialog() {
        processingOverlay.setVisibility(View.VISIBLE);
    }

    private void hideLoadingDialog() {
        processingOverlay.setVisibility(View.INVISIBLE);
    }

    private class ComicListAdapter extends SlideableComicListAdapter {

        public ComicListAdapter(Context context, Cursor c) {
            super(context, c, R.layout.page_comiclist_item);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Comic comic = ComicsTable.fromCursor(cursor);
            view.setTag(comic);

            // joliver 12/23/2013 Anything bigger than LARGE covers take too long to load,
            // and besides the iOS version uses smaller images.
            Uri coverUri = CoverSize.LARGE.getServerUri(comic.getImageFileName());
            ImageView cover = (ImageView) view.findViewById(R.id.cover);
            cover.setImageDrawable(null);
            ImageLoader.getInstance().displayImage(coverUri.toString(), cover);

            if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                int onDemandVisibility = comic.getSubscription() ? View.VISIBLE : View.INVISIBLE;
                view.findViewById(R.id.badgeUnlimited).setVisibility(onDemandVisibility);
            }

            // TODO: IV-81 (Backlogged), joliver: if this is an item in the recently added list, show badgeDateAdded view

            // Check comic possessionState so that we can
            // show a badge in lower right corner if comic has already been purchased or downloaded
            ImageView badgeDownloadedOrPurchased = (ImageView) view.findViewById(R.id.badgeDownloadedOrPurchased);
            MyComicsModel.PossessionState possessionState = mMyComicsModel.isOwned(comic);

            // Show purchased-state badge in lower right if user hasn't disabled them via the Settings menu.
            if (ComicPreferences.getInstance().isShowPurchaseOverlayShown()) {
                // comic is on device and ready to read
                if (possessionState == MyComicsModel.PossessionState.AVAILABLE) {
                    badgeDownloadedOrPurchased.setImageResource(R.drawable.downloaded);
                    badgeDownloadedOrPurchased.setVisibility(View.VISIBLE);
                }
                // comic has been purchased but the comicBundle has not been downloaded
                else if (possessionState == MyComicsModel.PossessionState.PURCHASED) {
                    badgeDownloadedOrPurchased.setImageResource(R.drawable.purchased);
                    badgeDownloadedOrPurchased.setVisibility(View.VISIBLE);
                } else
                    badgeDownloadedOrPurchased.setVisibility(View.INVISIBLE);
            } else
                badgeDownloadedOrPurchased.setVisibility(View.INVISIBLE);

        }
    }

}
