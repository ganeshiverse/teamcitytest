package com.iversecomics.bundle.parse;

import com.iversecomics.bundle.BundleReadException;
import com.iversecomics.bundle.ComicContent;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.bundle.Ownership;

public abstract class AbstractParseBundle {
    protected ComicContent  content;
    protected ComicMetadata metadata;
    protected Ownership     ownership;
    
    public static final short OWNER_EMAIL              = 204;


    public ComicContent getContent() {
        return content;
    }

    public ComicMetadata getMetadata() {
        return metadata;
    }

    public Ownership getOwnership() {
        return ownership;
    }

    public abstract void parse(BundleAccess bundle) throws BundleReadException;

    public void setContent(ComicContent comicContent) {
        this.content = comicContent;
    }

    public void setMetadata(ComicMetadata metadata) {
        this.metadata = metadata;
    }

    public void setOwnership(Ownership ownership) {
        this.ownership = ownership;
    }
}
