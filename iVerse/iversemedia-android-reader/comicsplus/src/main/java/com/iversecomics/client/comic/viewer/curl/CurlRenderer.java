/*
   Copyright 2012 Harri Smatt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.iversecomics.client.comic.viewer.curl;

import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Actual renderer class.
 *
 * @author harism
 */
public class CurlRenderer implements GLSurfaceView.Renderer {

    public static final int MIN_ZOOM = 1;
    public static final int MAX_ZOOM = 3;

    // Constant for requesting left page rect.
    public static final int PAGE_LEFT = 1;
    // Constant for requesting right page rect.
    public static final int PAGE_RIGHT = 2;
    // Constants for changing view mode.
    public static final int SHOW_ONE_PAGE = 1;
    public static final int SHOW_TWO_PAGES = 2;
    // Set to true for checking quickly how perspective projection looks.
    private static final boolean USE_PERSPECTIVE_PROJECTION = true;
    // Background fill color.
    private int mBackgroundColor;
    // Curl meshes used for static and dynamic rendering.
    private Vector<CurlMesh> mCurlMeshes;
    private RectF mMargins = new RectF();
    private Observer mObserver;
    // Page rectangles.
    private RectF mPageRectLeft;
    private RectF mPageRectRight;
    // View mode.
    private int mViewMode = SHOW_ONE_PAGE;
    // Screen size.
    public int mViewportWidth, mViewportHeight;
    // Rect for render area.
    private RectF mViewRect = new RectF();

    public static float WIDTH_OFFSET = 1.17f;
    public static float HEIGHT_OFFSET = 2.00f;

    private float mZoom = 1f;
    private float mOffsetLeft = 0; //1.25f;
    private float mOffsetTop = 0f;


    CurlView parent;
    private int mContentWidth = 0;
    private int mContentHeight = 0;
    boolean mVerticalScroll = false;

    /**
     * Basic constructor.
     */
    public CurlRenderer(Observer observer) {
        mObserver = observer;
        mCurlMeshes = new Vector<CurlMesh>();
        mPageRectLeft = new RectF();
        mPageRectRight = new RectF();
    }

    /**
     * Adds CurlMesh to this renderer.
     */
    public synchronized void addCurlMesh(CurlMesh mesh) {
        removeCurlMesh(mesh);
        mCurlMeshes.add(mesh);
    }


    public void setContentSize(int width, int height) {
        this.mContentWidth = width;
        this.mContentHeight = height;
        updatePageRects();
    }

    /**
     * Returns rect reserved for left or right page. Value page should be
     * PAGE_LEFT or PAGE_RIGHT.
     */
    public RectF getPageRect(int page) {
        if (page == PAGE_LEFT) {
            return mPageRectLeft;
        } else if (page == PAGE_RIGHT) {
            return mPageRectRight;
        }
        return null;
    }

    @Override
    public synchronized void onDrawFrame(GL10 gl) {

        mObserver.onDrawFrame();

        gl.glClearColor(Color.red(mBackgroundColor) / 255f,
                Color.green(mBackgroundColor) / 255f,
                Color.blue(mBackgroundColor) / 255f,
                Color.alpha(mBackgroundColor) / 255f);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glLoadIdentity();

        if (USE_PERSPECTIVE_PROJECTION) {
            GLU.gluPerspective(gl, 20f / (mZoom * 1.07f), (float) mViewportWidth / mViewportHeight, .1f, 100f);
            gl.glTranslatef(mOffsetLeft, mOffsetTop, -6f);
        } else {
            float ratio = (float) mViewportWidth / mViewportHeight;
            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glFrustumf(-ratio / mZoom, ratio / mZoom, -1 / mZoom, 1 / mZoom, 3, 7);
            gl.glTranslatef(mOffsetLeft, mOffsetTop, -6f);
        }

        for (int i = 0; i < mCurlMeshes.size(); ++i) {
            mCurlMeshes.get(i).onDrawFrame(gl);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);
        mViewportWidth = width;
        mViewportHeight = height;

        float ratio = (float) width / height;
        mViewRect.top = 1.0f;
        mViewRect.bottom = -1.0f;
        mViewRect.left = -ratio;
        mViewRect.right = ratio;

        updatePageRects();

        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        if (USE_PERSPECTIVE_PROJECTION) {
            //GLU.gluPerspective(gl, 20f / mZoom, (float) width / height, .1f, 100f);
        } else {
            GLU.gluOrtho2D(gl, mViewRect.left, mViewRect.right,
                    mViewRect.bottom, mViewRect.top);
        }

        WIDTH_OFFSET = HEIGHT_OFFSET * ((float) width / height);

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();


        parent.updatePageLayout();
        parent.requestRender();

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(0f, 0f, 0f, 1f);
        gl.glShadeModel(GL10.GL_SMOOTH);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        gl.glHint(GL10.GL_LINE_SMOOTH_HINT, GL10.GL_NICEST);
        gl.glHint(GL10.GL_POLYGON_SMOOTH_HINT, GL10.GL_NICEST);
        gl.glEnable(GL10.GL_LINE_SMOOTH);
        gl.glDisable(GL10.GL_DEPTH_TEST);
        gl.glDisable(GL10.GL_CULL_FACE);

        mObserver.onSurfaceCreated();
    }

    /**
     * Translates screen coordinates into view coordinates.
     */
    public void translate(PointF pt) {
        pt.x = mViewRect.left + (mViewRect.width() * pt.x / mViewportWidth) / mZoom;
        pt.y = mViewRect.top - (-mViewRect.height() * pt.y / mViewportHeight) / mZoom;
    }


    /**
     * Removes CurlMesh from this renderer.
     */
    public synchronized void removeCurlMesh(CurlMesh mesh) {
        while (mCurlMeshes.remove(mesh)) ;
    }

    /**
     * Change background/clear color.
     */
    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
    }

    /**
     * Set margins or padding. Note: margins are proportional. Meaning a value
     * of .1f will produce a 10% margin.
     */
    public synchronized void setMargins(float left, float top, float right,
                                        float bottom) {
        mMargins.left = left;
        mMargins.top = top;
        mMargins.right = right;
        mMargins.bottom = bottom;
        updatePageRects();
    }

    /**
     * Sets visible page count to one or two. Should be either SHOW_ONE_PAGE or
     * SHOW_TWO_PAGES.
     */
    public synchronized void setViewMode(int viewmode) {
        if (viewmode == SHOW_ONE_PAGE) {
            mViewMode = viewmode;
            //updatePageRects();
        } else if (viewmode == SHOW_TWO_PAGES) {
            mViewMode = viewmode;
            //updatePageRects();
        }
    }

    /**
     * Recalculates page rectangles.
     */
    private void updatePageRects() {
        if (mContentWidth <= 0 || mContentHeight <= 0) {
            return;
        }
        if (mViewRect.width() == 0 || mViewRect.height() == 0) {
            return;
        } else if (mViewMode == SHOW_ONE_PAGE) {
            /*
            mPageRectRight.set(mViewRect);
			mPageRectRight.left += mViewRect.width() * mMargins.left;
			mPageRectRight.right -= mViewRect.width() * mMargins.right;
			mPageRectRight.top += mViewRect.height() * mMargins.top;
			mPageRectRight.bottom -= mViewRect.height() * mMargins.bottom;

			mPageRectLeft.set(mPageRectRight);
			mPageRectLeft.offset(-mPageRectRight.width(), 0);

			int bitmapW = (int) ((mPageRectRight.width() * mViewportWidth) / mViewRect
					.width());
			int bitmapH = (int) ((mPageRectRight.height() * mViewportHeight) / mViewRect
					.height());
			//mObserver.onPageSizeChanged(bitmapW, bitmapH);
			*/

            float widthPage = mViewRect.width();
            float heightPage = widthPage * mContentHeight / mContentWidth;
            float topPage = mViewRect.top;
            if (heightPage < -mViewRect.height()) {
                topPage = mViewRect.top + (mViewRect.height() + heightPage) / 2;
            }
            if (heightPage > -mViewRect.height()) {
                mVerticalScroll = true;
            } else {
                mVerticalScroll = false;
            }
            mPageRectRight.left = mViewRect.left;
            mPageRectRight.top = topPage;
            mPageRectRight.right = mViewRect.right;
            mPageRectRight.bottom = topPage - heightPage;
            mPageRectLeft.set(mPageRectRight);
            mPageRectLeft.offset(-mPageRectRight.width(), 0);

            setOffsetLeft(this.mOffsetLeft);
            setOffsetTop(this.mOffsetTop);

        } else if (mViewMode == SHOW_TWO_PAGES) {
            mPageRectRight.set(mViewRect);
            mPageRectRight.left += mViewRect.width() * mMargins.left;
            mPageRectRight.right -= mViewRect.width() * mMargins.right;
            mPageRectRight.top += mViewRect.height() * mMargins.top;
            mPageRectRight.bottom -= mViewRect.height() * mMargins.bottom;

            mPageRectLeft.set(mPageRectRight);
            mPageRectLeft.right = (mPageRectLeft.right + mPageRectLeft.left) / 2;
            mPageRectRight.left = mPageRectLeft.right;

            int bitmapW = (int) ((mPageRectRight.width() * mViewportWidth) / mViewRect
                    .width());
            int bitmapH = (int) ((mPageRectRight.height() * mViewportHeight) / mViewRect
                    .height());
            //mObserver.onPageSizeChanged(bitmapW, bitmapH);
        }
    }

    /**
     * Observer for waiting render engine/state updates.
     */
    public interface Observer {
        /**
         * Called from onDrawFrame called before rendering is started. This is
         * intended to be used for animation purposes.
         */
        public void onDrawFrame();

        /**
         * Called from onSurfaceCreated to enable texture re-initialization etc
         * what needs to be done when this happens.
         */
        public void onSurfaceCreated();
    }


    /**
     * ********  Added by Christian Soe  **********
     */

    public float getZoom() {
        return mZoom;
    }

    public void setZoom(float zoom) {
        if (zoom < MIN_ZOOM)
            mZoom = MIN_ZOOM;
        else if (zoom > MAX_ZOOM)
            mZoom = MAX_ZOOM;
        else
            mZoom = zoom;

        setOffsetLeft(this.mOffsetLeft);
        setOffsetTop(this.mOffsetTop);
    }

    public float getOffsetLeft() {
        return mOffsetLeft;
    }

    public void setOffsetLeft(float offsetLeft, float zoom) {
        this.mZoom = zoom;
        setOffsetLeft(offsetLeft);
    }

    public float setOffsetLeft(float offsetLeft) {
        float diff = mViewRect.width() - (mViewRect.width() / mZoom);
        if (offsetLeft < -diff / 2)
            offsetLeft = -diff / 2;
        if (offsetLeft > diff / 2)
            offsetLeft = diff / 2;
        this.mOffsetLeft = offsetLeft;
        return this.mOffsetLeft;
    }

    public float getOffsetTop() {
        return mOffsetTop;
    }

    public void setOffsetTop(float offsetTop, float zoom) {
        this.mOffsetTop = offsetTop;
        setOffsetTop(offsetTop);
    }

    public float setOffsetTop(float offsetTop) {
        float heightPage = Math.abs(mPageRectRight.height());
        float heightScreen = Math.abs(mViewRect.height());
        if (heightPage * mZoom < heightScreen) {
            this.mOffsetTop = 0;
        } else {
            float top, bottom;
            if (heightPage > heightScreen) {
                top = (heightPage - heightScreen / 2 - heightScreen / 2 / mZoom);
                bottom = -(heightScreen / 2 - heightScreen / 2 / mZoom);
            } else {
                top = (heightPage / 2 - heightScreen / 2 / mZoom);
                bottom = -top;
            }
            if (offsetTop > top) {
                offsetTop = top;
            }
            if (offsetTop < bottom) {
                offsetTop = bottom;
            }
            this.mOffsetTop = offsetTop;
        }
        return this.mOffsetTop;
    }
}
