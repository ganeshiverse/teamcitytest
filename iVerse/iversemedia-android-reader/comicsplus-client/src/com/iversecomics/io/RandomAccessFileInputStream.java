package com.iversecomics.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class RandomAccessFileInputStream extends InputStream {

    private static final int END_OF_SECTION = (-1);
    private RandomAccessFile file;
    private long             endPosition;

    public RandomAccessFileInputStream(RandomAccessFile file, long length) throws IOException {
        super();
        this.file = file;
        this.endPosition = file.getFilePointer() + length;
    }

    @Override
    public void mark(int readlimit) {
        /* ignore */
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public int read() throws IOException {
        if (file.getFilePointer() >= endPosition) {
            // At end of stream / section
            return END_OF_SECTION;
        }
        return this.file.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        long len = Math.min(b.length, endPosition - file.getFilePointer());
        if (len <= 0) {
            // At end of stream / section
            return END_OF_SECTION;
        }
        return this.file.read(b, 0, (int) len);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        long alen = Math.min(len, endPosition - file.getFilePointer());
        if (len <= 0) {
            // At end of stream / section
            return END_OF_SECTION;
        }
        return this.file.read(b, off, (int) alen);
    }
}
