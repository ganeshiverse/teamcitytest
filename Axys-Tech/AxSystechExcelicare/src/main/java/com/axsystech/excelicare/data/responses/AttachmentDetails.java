package com.axsystech.excelicare.data.responses;

import java.io.Serializable;

/**
 * Created by someswarreddy on 07/10/15.
 */
public class AttachmentDetails implements Serializable {

    private String attachmentType;

    private String attachment;

    private String attachmentName;

    private String attachmentSize;

    private String attachmentType_LU;

    private String dashboardId;

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getAttachmentSize() {
        return attachmentSize;
    }

    public void setAttachmentSize(String attachmentSize) {
        this.attachmentSize = attachmentSize;
    }

    public String getAttachmentType_LU() {
        return attachmentType_LU;
    }

    public void setAttachmentType_LU(String attachmentType_LU) {
        this.attachmentType_LU = attachmentType_LU;
    }

    public String getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(String dashboardId) {
        this.dashboardId = dashboardId;
    }
}
