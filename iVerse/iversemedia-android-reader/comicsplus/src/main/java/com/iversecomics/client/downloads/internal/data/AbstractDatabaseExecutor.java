package com.iversecomics.client.downloads.internal.data;

import android.content.ContentProviderClient;
import android.net.Uri;

import org.acra.ErrorReporter;

public abstract class AbstractDatabaseExecutor implements Runnable {
    protected ContentProviderClient client;
    protected Uri contentUri;

    public AbstractDatabaseExecutor(Uri contentUri, ContentProviderClient client) {
        this.contentUri = contentUri;
        this.client = client;
    }

    public abstract void execute();

    @Override
    public void run() {
        try {
            execute();
        } catch (Throwable t) {
            ErrorReporter.getInstance().handleSilentException(t);
        }
    }
}
