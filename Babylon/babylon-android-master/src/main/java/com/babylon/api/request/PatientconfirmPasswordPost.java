package com.babylon.api.request;

import com.babylon.App;
import com.babylon.api.request.base.*;
import com.babylon.model.Password;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicHeader;


public class PatientconfirmPasswordPost extends AbsRequestPost<Void, Void> {

    private static final String PATIENT = "patient";
    private static final String PASSWORD = "password";
    private String patientId;

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }


    public AbsRequestPost<Void, Void> withEntry(String password) {

       JsonObject jsonObject = new JsonObject();

        Password passwordObj=new Password();
        passwordObj.setPassword(password);

        jsonObject.add(PATIENT, getGson().toJsonTree(passwordObj));
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {

        String url=Urls.getRuby(Urls.PASSWORD_CONFIRM);
        patientId=  App.getInstance().getPatient().getId();
        url =url.concat(patientId+"/verify");
        return url;
    }

    @Override
    protected AbsResponseProcessor<Void> getResponseProcessor() {
        return new ResponseProcessor();
    }

    @Override
    protected Response<Void> processResponse(HttpResponse httpResponse) {
        Response<Void> response = super.processResponse(httpResponse);
        return response;
    }

    class ResponseProcessor extends AbsResponseProcessor<Void> {

        public ResponseProcessor() {
            super(Void.TYPE);
        }

        @Override
        public Response<Void> getResponse(HttpResponse httpResponse) {
            Response response = new Response();
            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine != null) {
                L.d(AbsRequest.TAG, getStringResponse(httpResponse));
                if (statusLine.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                    response.setStatus(Response.RESULT_OK);
                }
            }
            return response;
        }
    }
}
