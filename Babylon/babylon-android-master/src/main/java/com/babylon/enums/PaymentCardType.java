package com.babylon.enums;

public final class PaymentCardType {
    public static final String VISA_CARD_TYPE = "visa";
    public static final String MASTER_CARD_TYPE = "mastercard";
    public static final String AMERICA_EXPRESS_CARD_TYPE = "american express";
}
