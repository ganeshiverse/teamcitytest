package com.babylon.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.model.payments.PaymentCard;

public class PaymentCardView extends FrameLayout {

    public PaymentCardView(Context context) {
        super(context);
        initView(context);
    }

    public PaymentCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PaymentCardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    public void showPaymentCard(PaymentCardViewController controller, final PaymentCard paymentCard,
                                final OnPaymentCardClickListener listener) {
        setImageResource(controller.findImageId(paymentCard.getCardType()));
        setType(paymentCard.getCardType());

        if(paymentCard.isCreated()) {
            setMaskedNumber(controller.getCardViewNumber(paymentCard));
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPaymentCardClick(paymentCard);
                }
            });
        } else {
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onNewPaymentCardClick();
                }
            });
        }
    }

    private void setImageResource(@DrawableRes int resId) {
        final ImageView cardImageView = (ImageView)findViewById(R.id.credit_card_image_view);
        cardImageView.setImageResource(resId);
    }

    public void setMaskedNumber(String maskedNumber) {
        final TextView titleTextView = (TextView) findViewById(R.id.credit_card_number_text_view);
        titleTextView.setText(maskedNumber);
    }

    public void setType(String type) {
        final TextView typeTextView = (TextView)findViewById(R.id.credit_card_type_text_view);
        typeTextView.setText(type);
    }

    private void initView(final Context context) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.list_item_payment_card, this);
    }

    public interface OnPaymentCardClickListener {
        public void onNewPaymentCardClick();
        public void onPaymentCardClick(PaymentCard paymentCard);
    }

}
