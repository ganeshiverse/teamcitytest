package com.babylon.enums;

import android.text.TextUtils;

import com.babylon.App;
import com.babylon.R;

import java.util.Locale;


public enum Gender {
    MALE(0), FEMALE(1);

    public static final String MALE_STRING = "Male";
    public static final String FEMALE_STRING = "Female";

    private int position = 0;

    Gender(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase(Locale.getDefault());
    }

    public String getDisplayValue() {
        switch (this) {
            case MALE:
                return App.getInstance().getString(R.string.weight_parameters_male);

            case FEMALE:
                return App.getInstance().getString(R.string.weight_parameters_female);

            default:
                throw new IllegalArgumentException("Unknown value = " + String.valueOf(this));
        }
    }

    /**
     * @param position
     * @return
     */
    public static Gender get(int position) {
        switch (position) {
            case 0:
                return MALE;

            case 1:
                return FEMALE;

            default:
                throw new IllegalArgumentException("Unknown index = " + String.valueOf(position));
        }
    }

    /**
     * @param genderValue {@link com.babylon.model.Patient#gender}
     * @return
     */
    public static Gender get(String genderValue) {
        Gender result = MALE;

        if (!TextUtils.isEmpty(genderValue) && genderValue.equalsIgnoreCase(FEMALE_STRING)) {
            result = FEMALE;
        }

        return result;
    }

    public static String getGender(Gender gender) {
        switch (gender) {
            case MALE:
                return MALE_STRING;

            case FEMALE:
                return FEMALE_STRING;

            default:
                throw new IllegalArgumentException("Unknown index = " + String.valueOf(gender));
        }
    }

    public static boolean isGenderFilled(String gender) {
        return !TextUtils.isEmpty(gender) && (gender.equalsIgnoreCase(MALE_STRING) || gender.equalsIgnoreCase(FEMALE_STRING));
    }
}
