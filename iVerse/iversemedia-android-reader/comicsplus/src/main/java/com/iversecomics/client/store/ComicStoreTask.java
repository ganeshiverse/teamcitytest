package com.iversecomics.client.store;

import com.iversecomics.client.refresh.Task;

public abstract class ComicStoreTask extends Task {

//    public static final String FEATURED_ITEMS_UPDATED = "featureditemsupdated";
//    public static final String FEATURED_SLOTS_UPDATED = "featuredslotsupdated";

    protected ComicStore comicStore;

    protected static String SHARED_SECRET = "ldpokedihwefgy34gr74hfwidonhdbgoiwejfoiwehfuieg759hfiohfuidhgrytepihfourwghiuertoer394dhwfdhwiyt3eiw";

    public ComicStoreTask(ComicStore comicStore) {
        this.comicStore = comicStore;
    }

    public static void setSharedSecret(final String secret) {
        SHARED_SECRET = secret;
    }

    public static String getSharedSecret() {
        return SHARED_SECRET;
    }
}
