package com.axsystech.excelicare.loaders;

import android.content.Context;
import android.text.TextUtils;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.db.models.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.exceptions.NoNetworkException;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 23.07.14
 * Time: 12:04
 *
 * @author SomeswarReddy
 */
public class GetUserSiteDetailsDBLoader extends DataAsyncTaskLibLoader<List<ClientUrlDbModel>> {

    private final String mUserEmail;

    public GetUserSiteDetailsDBLoader(final Context context, final String userEmail) {
        super(context, true);
        this.mUserEmail = userEmail;
    }

    @Override
    protected List<ClientUrlDbModel> performLoad() throws Exception {
        if(TextUtils.isEmpty(mUserEmail))
            throw new SQLException("User email should not be empty...");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final Dao<ClientUrlDbModel, Long> clientUrlDbModelDao = helper.getDao(ClientUrlDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<ClientUrlDbModel>>() {
                @Override
                public List<ClientUrlDbModel> call() throws Exception {
                    return clientUrlDbModelDao.queryBuilder().where().eq(ClientUrlDbModel.USER_EMAIL_ID, mUserEmail).query();
                }
            });
        } catch (final Exception e) {
            throw new NoNetworkException();
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
