package com.iversecomics.client.bitmap.loader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.iversecomics.client.bitmap.IBitmapLoader;
import com.iversecomics.io.IOUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;

public class ExternalBitmapLoader implements IBitmapLoader {
    private File externalDir;

    public ExternalBitmapLoader() {
        this.externalDir = Environment.getExternalStorageDirectory();
    }

    @Override
    public String[] getUriSchemes() {
        return new String[]{"sdcard"};
    }

    @Override
    public Bitmap loadBitmap(URI bitmapUri) throws IOException {
        File bitmapFile = new File(externalDir, bitmapUri.getSchemeSpecificPart());

        FileInputStream stream = null;
        try {
            stream = new FileInputStream(bitmapFile);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            return BitmapFactory.decodeStream(stream, null, opts);
        } finally {
            IOUtil.close(stream);
        }
    }
}
