package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ForgotPasswordResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class ForgotPasswordLoader extends DataAsyncTaskLibLoader<ForgotPasswordResponse> {

    private String username;
    private String securityQ1;
    private String securityAns1;
    private String securityQ2;
    private String securityAns2;

    public ForgotPasswordLoader(Context context, String username, String securityQ1, String securityAns1, String securityQ2, String securityAns2) {
        super(context);
        this.username = username;
        this.securityQ1 = securityQ1;
        this.securityAns1 = securityAns1;
        this.securityQ2 = securityQ2;
        this.securityAns2 = securityAns2;
    }

    @Override
    protected ForgotPasswordResponse performLoad() throws Exception {
        return ServerMethods.getInstance().forgotPassword(username, securityQ1, securityAns1, securityQ2, securityAns2);
    }
}
