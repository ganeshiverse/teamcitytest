package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetSiteDetailsFromDBLoader extends DataAsyncTaskLibLoader<List<SiteDetailsDBModel>> {

    public GetSiteDetailsFromDBLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<SiteDetailsDBModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<SiteDetailsDBModel, Long> siteDao = helper.getDao(SiteDetailsDBModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<SiteDetailsDBModel>>() {
                @Override
                public List<SiteDetailsDBModel> call() throws Exception {
                    return siteDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
