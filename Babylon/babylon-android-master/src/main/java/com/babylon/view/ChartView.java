package com.babylon.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.api.request.ChartGet;
import com.babylon.broadcast.SyncFinishedBroadcast;
import com.babylon.chart.OnLoadChartProgressListener;
import com.babylon.enums.TimeInterval;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.UIUtils;
import com.babylon.chart.HttpConnector;
import com.babylon.chart.WebViewLoader;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class ChartView extends LinearLayout implements RadioGroup.OnCheckedChangeListener {
    private static final String TAG = ChartView.class.getSimpleName();

    private WebView chartWebView;
    private TextView descriptionTextView;
    private RadioGroup dateRadioGroup;
    private RadioButton oneWeekRadioButton;
    private RadioButton oneMonthRadioButton;
    private RadioButton oneYearRadioButton;

    private String featureId = null;
    private String defaultPeriodId = null;
    private String currentPeriodId = null;
    private boolean showDateRadioGroup = false;
    private boolean showOneWeekButton = false;
    private boolean showOneMonthButton = false;
    private boolean showOneYearButton = false;
    private Long dateInMillis;

    private WebViewLoader webViewLoader = WebViewLoader.getInstance();
    private OnLoadChartProgressListener loadChartProgressListener;

    public ChartView(Context context) {
        this(context, null);
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ChartView);

        featureId = a.getString(R.styleable.ChartView_featureId);
        defaultPeriodId = a.getString(R.styleable.ChartView_defaultPeriodId);
        showDateRadioGroup = a.getBoolean(R.styleable.ChartView_showDateRadioGroup, false);
        showOneWeekButton = a.getBoolean(R.styleable.ChartView_showOneWeekButton, false);
        showOneMonthButton = a.getBoolean(R.styleable.ChartView_showOneMonthButton, false);
        showOneYearButton = a.getBoolean(R.styleable.ChartView_showOneYearButton, false);

        a.recycle();

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_chart, this);

        chartWebView = (WebView) view.findViewById(R.id.chartWebView);
        chartWebView.getSettings().setAllowFileAccess(true);
        chartWebView.getSettings().setJavaScriptEnabled(true);
        chartWebView.setBackgroundColor(0x00000000);

        descriptionTextView = (TextView) view.findViewById(R.id.descriptionTextView);

        dateRadioGroup = (RadioGroup) view.findViewById(R.id.chartDateRadioGroup);
        oneWeekRadioButton = (RadioButton) view.findViewById(R.id.oneWeekRadioButton);
        oneMonthRadioButton = (RadioButton) view.findViewById(R.id.oneMonthRadioButton);
        oneYearRadioButton = (RadioButton) view.findViewById(R.id.oneYearRadioButton);

        dateRadioGroup.setOnCheckedChangeListener(this);
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.babylon.ACTION_SYNC_FINISHED");
        getContext().registerReceiver(syncFinishedBroadcastReceiver, filter);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getContext().unregisterReceiver(syncFinishedBroadcastReceiver);
    }

    private final BroadcastReceiver syncFinishedBroadcastReceiver = new SyncFinishedBroadcast() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            final int action = intent.getIntExtra(SyncAdapter.KEY_SYNC_ACTION, -1);
            final String type = intent.getStringExtra(SyncAdapter.KEY_SYNC_TYPE);



            if ((type.equals(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY)
                    && (action == SyncAdapter.SYNC_BOTH || action == SyncAdapter.SYNC_PUSH))
                    || (type.equals(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS)
                    && (action == SyncAdapter.SYNC_BOTH || action == SyncAdapter.SYNC_PUSH))) {
                showChart();
            }
        }
    };

    public void setDescription(String description) {
        if (!TextUtils.isEmpty(description)) {
            descriptionTextView.setVisibility(VISIBLE);
            descriptionTextView.setText(description);
        } else {
            descriptionTextView.setVisibility(GONE);
        }
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    public void setDefaultPeriodId(String defaultPeriodId) {
        this.defaultPeriodId = defaultPeriodId;
    }

    public void setShowDateRadioGroup(boolean showDateRadioGroup) {
        this.showDateRadioGroup = showDateRadioGroup;
    }

    public void setDateInMillis(long dateInMillis) {
        this.dateInMillis = dateInMillis;
    }

    public void setOnLoadChartProgressListener(OnLoadChartProgressListener loadChartProgressListener) {
        this.loadChartProgressListener = loadChartProgressListener;
    }

    public void showChart() {
        if (TextUtils.isEmpty(defaultPeriodId)) {
            if (showOneWeekButton) {
                currentPeriodId = TimeInterval.ONE_WEEK.getId();
            } else if (showOneMonthButton) {
                currentPeriodId = TimeInterval.ONE_MONTH.getId();
            } else if (showOneYearButton) {
                currentPeriodId = TimeInterval.ONE_YEAR.getId();
            } else {
                currentPeriodId = TimeInterval.ONE_WEEK.getId();
            }
        } else {
            currentPeriodId = defaultPeriodId;
        }

        updateChart(dateInMillis, currentPeriodId, featureId);
    }

    public void checkRadioButtonByPeriodId(String periodId) {
        if (TimeInterval.ONE_WEEK.getId().equals(periodId)) {
            oneWeekRadioButton.setChecked(true);
        } else if (TimeInterval.ONE_MONTH.getId().equals(periodId)) {
            oneMonthRadioButton.setChecked(true);
        } else if (TimeInterval.ONE_YEAR.getId().equals(periodId)) {
            oneYearRadioButton.setChecked(true);
        }
    }

    public void checkRadioGroupVisibilityState() {
        if (showDateRadioGroup) {
            dateRadioGroup.setVisibility(View.VISIBLE);
            oneWeekRadioButton.setVisibility(showOneWeekButton ? View.VISIBLE : View.GONE);
            oneMonthRadioButton.setVisibility(showOneMonthButton ? View.VISIBLE : View.GONE);
            oneYearRadioButton.setVisibility(showOneYearButton ? View.VISIBLE : View.GONE);
        } else {
            dateRadioGroup.setVisibility(View.GONE);
        }
    }

    public void updateChart(Long dateInMillis, String periodId, String featureId) {
        if (dateInMillis != null && !TextUtils.isEmpty(periodId) && !TextUtils.isEmpty(featureId)) {
            checkRadioGroupVisibilityState();
            checkRadioButtonByPeriodId(periodId);

            long convertInSeconds = DateUtils.getDayStartMills(dateInMillis, 1) / android.text.format.DateUtils.SECOND_IN_MILLIS;
            String url = ChartGet.newChartGet(convertInSeconds, periodId, featureId).getUrl();

            new GetChartHtmlAsyncTask(url).execute();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.oneWeekRadioButton:
                updatePeriod(TimeInterval.ONE_WEEK.getId());
                break;
            case R.id.oneMonthRadioButton:
                updatePeriod(TimeInterval.ONE_MONTH.getId());
                break;
            case R.id.oneYearRadioButton:
                updatePeriod(TimeInterval.ONE_YEAR.getId());
                break;
            default:
                break;
        }

    }

    private void updatePeriod(String newPeriodId) {
        if(!newPeriodId.equals(currentPeriodId)) {
            currentPeriodId = newPeriodId;
            updateChart(dateInMillis, currentPeriodId, featureId);
        }
    }

    private class GetChartHtmlAsyncTask extends AsyncTask<Void, Integer, File> {
        private HttpConnector httpConnector = HttpConnector.getInstance();
        private String url;

        public GetChartHtmlAsyncTask(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            if (showDateRadioGroup) {
                UIUtils.setRadioGroupClickable(dateRadioGroup, false);
            }

            if (loadChartProgressListener != null) {
                loadChartProgressListener.onStartLoadChartFromServer();
            }

            File cachedFiles = webViewLoader.getFileFromCache(url);
            if (cachedFiles.exists()) {
                chartWebView.loadUrl("file:///" + cachedFiles.getPath());
            }
        }

        protected File doInBackground(Void... v) {
            File result = null;
            try {
                saveJsFilesIfNeed();

                if (UIUtils.isOnline(getContext())) {
                    String resultHtml = httpConnector.sendGETRequest(url);
                    if (!TextUtils.isEmpty(resultHtml) && resultHtml.contains("<html>")) {
                        resultHtml = UIUtils.getSuperscriptStringFromUnicode(resultHtml);
                        result = webViewLoader.saveFileFromString(url, resultHtml);
                    }
                }
            } catch (URISyntaxException e) {
                L.e(TAG, e.getMessage(), e);
            } catch (IOException e) {
                L.e(TAG, e.getMessage(), e);
            }
            return result;
        }

        protected void onPostExecute(File resultChartFile) {
            if (resultChartFile != null) {
                chartWebView.loadUrl("file:///" + resultChartFile.getPath());
            }

            if (showDateRadioGroup) {
                UIUtils.setRadioGroupClickable(dateRadioGroup, true);
            }

            if (loadChartProgressListener != null) {
                loadChartProgressListener.onFinishLoadChartFromServer();
            }
        }

    }

    public void saveJsFilesIfNeed() {
        File jsDir = new File(webViewLoader.getCacheDir(), "js");

        if (!jsDir.exists()) {
            jsDir.mkdirs();

            File jsResultFile1 = new File(jsDir, "highcharts.js");
            File jsResultFile2 = new File(jsDir, "jquery.min.js");

            webViewLoader.saveRawResourceInFile(getContext(), R.raw.highcharts, jsResultFile1);
            webViewLoader.saveRawResourceInFile(getContext(), R.raw.jquery_min, jsResultFile2);
        }
    }
}
