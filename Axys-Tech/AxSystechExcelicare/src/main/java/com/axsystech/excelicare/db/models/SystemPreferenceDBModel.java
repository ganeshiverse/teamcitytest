package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 11/07/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_SYSTEM_PREFERENCES)
public class SystemPreferenceDBModel extends DBUtils implements Serializable {

    @DatabaseField(id = true, columnName = _ID)
    private String id;

    @DatabaseField(columnName = DESCRIPTION)
    private String description;

    @DatabaseField(columnName = VALUE)
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
