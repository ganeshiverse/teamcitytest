package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Message;

public class QuestionGet extends AbsRequestGet<Message.Question> {

    private String questionId;

    protected QuestionGet() {
        // not called
    }

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.QUESTION_GET);
        return String.format(url, questionId);
    }

    public static QuestionGet newQuestionGet(String questionId) {
        QuestionGet questionGet = new QuestionGet();
        questionGet.questionId = questionId;
        return questionGet;
    }
}
