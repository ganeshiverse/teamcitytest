package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.iversecomics.client.store.json.IComicStoreParsingEvent;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;

import java.util.ArrayList;
import java.util.List;

public class ComicStoreDBUpdater implements IComicStoreParsingEvent {
    private SQLiteDatabase db;
    private ContentResolver contentResolver;
    private List<ComicDBSubUpdater> comicSubUpdaters = new ArrayList<ComicDBSubUpdater>();
    private BatchInserts batcher;

    public ComicStoreDBUpdater(SQLiteDatabase db, ContentResolver contentResolver) {
        this.db = db;
        this.batcher = new BatchInserts(db);
        batcher.registerContentNotifier(ComicsTable.TABLE, ComicsTable.CONTENT_URI);
        batcher.registerContentNotifier(GenresTable.TABLE, GenresTable.CONTENT_URI);
        batcher.registerContentNotifier(PublishersTable.TABLE, PublishersTable.CONTENT_URI);
        batcher.registerContentNotifier(GroupsTable.TABLE, GroupsTable.CONTENT_URI);
        batcher.registerContentNotifier(FeaturedSlotTable.TABLE, FeaturedSlotTable.CONTENT_URI);
        batcher.registerContentNotifier(PromotionTable.TABLE, PromotionTable.CONTENT_URI);

        this.contentResolver = contentResolver;
        this.comicSubUpdaters = new ArrayList<ComicDBSubUpdater>();
    }

    public void addComicDBSubUpdater(ComicDBSubUpdater comicUpdater) {
        this.comicSubUpdaters.add(comicUpdater);
    }

    @Override
    public void onParsedComic(Comic comic) {
        ContentValues values = ComicsTable.asValues(comic);
        batcher.insert(ComicsTable.TABLE, values);

        for (ComicDBSubUpdater sub : comicSubUpdaters) {
            sub.update(db, batcher, comic);
        }
    }

    @Override
    public void onParsedGenre(Genre genre) {
        ContentValues values = GenresTable.asValues(genre);
        batcher.insert(GenresTable.TABLE, values);
    }

    @Override
    public void onParsedGroup(Group group) {
        ContentValues values = GroupsTable.asValues(group);
        batcher.insert(GroupsTable.TABLE, values);
    }

    @Override
    public void onParsedPublisher(Publisher publisher) {
        ContentValues values = PublishersTable.asValues(publisher);
        batcher.insert(PublishersTable.TABLE, values);
    }

    @Override
    public void onParsedFeaturedSlot(FeaturedSlot slot) {
        ContentValues values = FeaturedSlotTable.asValues(slot);
        batcher.insert(FeaturedSlotTable.TABLE, values);
    }

    @Override
    public void onParsedPromotion(Promotion promotion) {
        ContentValues values = PromotionTable.asValues(promotion);
        batcher.insert(PromotionTable.TABLE, values);
    }

    @Override
    public void onParseEnd() {
        batcher.end();
        batcher.notifyContentResolvers(contentResolver);

        for (ComicDBSubUpdater sub : comicSubUpdaters) {
            sub.onUpdateEnd(batcher);
        }
    }

    @Override
    public void onParseStart() {
        /* ignore */
    }

}
