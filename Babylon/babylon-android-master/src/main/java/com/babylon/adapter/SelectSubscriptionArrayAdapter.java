package com.babylon.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.model.Patient;
import com.babylon.model.payments.Plan;

public class SelectSubscriptionArrayAdapter extends ArrayAdapter<Plan>{
    private final Context context;
    private final Plan[] values;
    private final Patient patient;
    private static final int ITEM_TYPE_COUNT = 2;
    private static final int ITEM_TYPE_SUBSCRIPTION_CELL = 0;

    public SelectSubscriptionArrayAdapter(Context context, Plan[] values, Patient patient) {
        super(context, R.layout.list_item_subscription_select, values);
        this.context = context;
        this.values = values;
        this.patient = patient;
    }

    @Override
    public int getCount() {
        return (values.length * 2);
    }

    @Override
    public int getViewTypeCount() {
        return ITEM_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return position % ITEM_TYPE_COUNT;
    }

    private class ViewHolder {
        public TextView titleTextView;
        public TextView subtitleTextView;
        public TextView priceTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        int type = getItemViewType(position);
        if (type == ITEM_TYPE_SUBSCRIPTION_CELL) {
            int rowIndex = position / ITEM_TYPE_COUNT;
            ViewHolder viewHolder = new ViewHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.list_item_subscription_select, parent, false);
                viewHolder.titleTextView = (TextView)view.findViewById(R.id.subscription_title_textview);
                viewHolder.subtitleTextView = (TextView)view.findViewById(R.id.subscription_subtitle_textview);
                viewHolder.priceTextView = (TextView)view.findViewById(R.id.subscription_price_textview);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder)view.getTag();
            }

            Plan plan = values[rowIndex];
            Double price = (patient.getIsMinor())? plan.getMinorPrice() : plan.getPrice();
            if (price == null || price == 0) {
                price = plan.getGpAppointmentPrice();
            }

            viewHolder.titleTextView.setText(plan.getTitle());
            viewHolder.subtitleTextView.setText(plan.getSubtitle());

            if (price != null) {
                viewHolder.priceTextView.setText(App.getInstance().getCurrentRegion().getFormattedPriceString(price));
            }
        } else {
            if (view == null) {
                view = inflater.inflate(R.layout.list_item_subscription_seperator, parent, false);
            }

            View viewSeparator = view.findViewById(R.id.list_item_subscription_seperator_view);
            viewSeparator.getLayoutParams().height = context.getResources().getDimensionPixelSize( ((position == 1) ? R.dimen.divider_height_highlight : R.dimen.divider_height));
            viewSeparator.requestLayout();
        }
        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) == ITEM_TYPE_SUBSCRIPTION_CELL;
    }

    public Plan getSubscriptionAtPosition(int position) {
        Plan subscription = null;
        if (getItemViewType(position) == ITEM_TYPE_SUBSCRIPTION_CELL) {
            int rowIndex = position / 2;
            if(rowIndex < values.length) {
                subscription = values[rowIndex];
            }
        }
        return subscription;
    }
}
