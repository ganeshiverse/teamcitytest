package com.iversecomics.client.store.db;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.util.text.StringUtils;

public class GenresTableTest extends AbstractComicStoreProviderTestCase {
    private Genre querySingleGenre(String msg, Uri contentUri, String selection, String selectionArgs[]) {
        Cursor c = null;
        try {
            c = resolver.query(contentUri, null, selection, selectionArgs, null);
            StringBuilder msgb = new StringBuilder();
            msgb.append(msg);
            msgb.append(" (where=").append(selection);
            msgb.append(") (whereArgs=").append(StringUtils.join(selectionArgs, "|"));
            msgb.append(")");
            assertDatabaseRowCount(msgb.toString(), c, 1);
            Genre genre = GenresTable.fromCursor(c);
            return genre;
        } finally {
            DBUtil.close(c);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testGenreInsertObject() {
        Genre genre = new Genre();
        genre.setGenreId("test.genre.object");
        genre.setName("TestGenreObject");

        ContentValues values = GenresTable.asValues(genre);

        Uri uri = resolver.insert(GenresTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Genres", uri);
    }

    @MediumTest
    public void testGenreInsertSimple() {
        ContentValues values = new ContentValues();
        values.put(GenresTable.GENREID, "test.genre.simple");
        values.put(GenresTable.NAME, "TestGenre");

        Uri uri = resolver.insert(GenresTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Genres", uri);
    }

    @MediumTest
    public void testGenreInsertThenInsert() {
        String id = "test.genre";

        ContentValues values = new ContentValues();
        values.put(GenresTable.GENREID, id);
        values.put(GenresTable.NAME, "TestGenre");

        Uri uri = resolver.insert(GenresTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Genres", uri);

        // Now try to find it.
        String selection = GenresTable.GENREID + " = ?";
        String selectionArgs[] = new String[] { id };
        String msg = "Genre (initial)";
        Genre g1 = querySingleGenre(msg, GenresTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestGenre", g1.getName());

        // Insert another entry, hopefully overwriting the original due to a proper constraint setup.
        ContentValues second = new ContentValues();
        second.put(GenresTable.GENREID, id);
        second.put(GenresTable.NAME, "Test Genre Post"); // New name (with space)

        Uri uriSecond = resolver.insert(GenresTable.CONTENT_URI, second);
        assertDatabaseUriHasDBId("Genres", uriSecond);

        // Try to find it again.
        msg = "Genre (post insert)";
        Genre g2 = querySingleGenre(msg, GenresTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Genre Post", g2.getName());
    }

    @MediumTest
    public void testGenreInsertThenUpdate() {
        String id = "test.genre";

        ContentValues values = new ContentValues();
        values.put(GenresTable.GENREID, id);
        values.put(GenresTable.NAME, "TestGenre");

        Uri uri = resolver.insert(GenresTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Genres", uri);

        // Now try to find it.
        String selection = GenresTable.GENREID + " = ?";
        String selectionArgs[] = new String[] { id };
        String msg = "Genre (initial)";
        Genre g1 = querySingleGenre(msg, GenresTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestGenre", g1.getName());

        // Update it
        ContentValues update = new ContentValues();
        update.put(GenresTable.GENREID, id);
        update.put(GenresTable.NAME, "Test Genre Post"); // New name (with space)

        String where = GenresTable.GENREID + " = ?";
        String whereArgs[] = new String[] { id };
        int count = resolver.update(GenresTable.CONTENT_URI, update, where, whereArgs);
        assertEquals("Update count", 1, count);

        // Try to find it again.
        msg = "Genre (post update)";
        Genre g2 = querySingleGenre(msg, GenresTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Genre Post", g2.getName());
    }

    @MediumTest
    public void testGenreQueryByDBId() {
        ContentValues values = new ContentValues();
        values.put(GenresTable.GENREID, "test.genre.query.bydbid");
        values.put(GenresTable.NAME, "TestGenre");

        Uri uri = resolver.insert(GenresTable.CONTENT_URI, values);
        long dbId = getDatabaseId("Genres", uri);

        Uri singleUri = ContentUris.withAppendedId(GenresTable.CONTENT_URI, dbId);

        // Try to fetch based on URI.
        String msg = "Query Genre by DB Id";
        Genre g = querySingleGenre(msg, singleUri, null, null);
        assertEquals("Genre.genreId", values.get(GenresTable.GENREID), g.getGenreId());
        assertEquals("Genre.name", values.get(GenresTable.NAME), g.getName());
    }

    @MediumTest
    public void testGenreQueryByGenreId() {
        String genreId = "test.genre.query.bygenreid";
        ContentValues values = new ContentValues();
        values.put(GenresTable.GENREID, genreId);
        values.put(GenresTable.NAME, "TestGenre");

        Uri uri = resolver.insert(GenresTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Genres", uri);

        Uri singleUri = Uri.withAppendedPath(GenresTable.CONTENT_URI_BY_GENREID, genreId);

        // Try to fetch based on URI.
        String msg = "Specific genre by uri+genreId";
        Genre g = querySingleGenre(msg, singleUri, null, null);
        assertEquals("Genre.genreId", genreId, g.getGenreId());
        assertEquals("Genre.name", values.get(GenresTable.NAME), g.getName());
    }
}
