package com.babylon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Account {
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("last_name")
    @Expose
    private String lastName;
    @Expose
    private String avatar;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @Expose
    private String email;
    @Expose
    private String gender;
    @SerializedName("switch_account_access_token")
    @Expose
    private String switchAccountAccessToken;
    @Expose
    private Boolean minor;



    public Account()
    {

    }
    public Account(String id, String firstname)
    {
        this.id = id;
        firstName = firstname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        this.dateOfBirth = dateFormat.format(dateOfBirth);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirthAsDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = dateFormat.parse(dateOfBirth);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String getSwitchAccountAccessToken() {
        return switchAccountAccessToken;
    }

    public void setSwitchAccountAccessToken(String switchAccountAccessToken) {
        this.switchAccountAccessToken = switchAccountAccessToken;
    }

    public Boolean getIsMinor() {
        return minor;
    }

    public void setIsMinor(boolean isMinor) {
        this.minor = isMinor;
    }

}
