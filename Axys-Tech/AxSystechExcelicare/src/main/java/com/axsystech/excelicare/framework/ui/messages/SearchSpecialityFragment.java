package com.axsystech.excelicare.framework.ui.messages;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.LookupsListObject;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * Created by SomeswarReddy on 8/20/2015.
 */
public class SearchSpecialityFragment extends BaseFragment implements View.OnClickListener, TextWatcher {

    private String TAG = SearchSpecialityFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private SearchSpecialityFragment mFragment;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.listView)
    private ListView listView;

    //@InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    private LayoutInflater mLayoutInflator;

    private ArrayList<LookupsListObject> mSpecialityLookupsList;
    private ArrayList<LookupsListObject> mSpecialityLookupsSearchList;
    private SpecialityListAdapter mSpecialityListAdapter;

    private SpeclialitySelectionListener speclialitySelectionListener;

    public void setSpeclialitySelectionListener(SpeclialitySelectionListener speclialitySelectionListener) {
        this.speclialitySelectionListener = speclialitySelectionListener;
    }

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(),mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_message_inbox, container, false);
        searchEditText = (EditText)view.findViewById(R.id.searchEditText);
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        searchEditText.setTypeface(regular);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflator = LayoutInflater.from(mActivity);

        initViews();
        readIntentArgs();
        addEventListeners();
        preloadData();
    }

    private void initViews() {
        searchEditText.setHint(getString(R.string.search_by_speciality));
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }

            if(bundle.containsKey(AxSysConstants.EXTRA_SPECIALITY_LOOKUPS_LIST)) {
                mSpecialityLookupsList = (ArrayList<LookupsListObject>) bundle.getSerializable(AxSysConstants.EXTRA_SPECIALITY_LOOKUPS_LIST);
            }
        }
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);
    }

    private void preloadData() {
        // create List Adapter
        mSpecialityListAdapter = new SpecialityListAdapter(mSpecialityLookupsList);
        listView.setAdapter(mSpecialityListAdapter);
    }


    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                CommonUtils.hideSoftKeyboard(mActivity, searchEditText);
                // update to list adapter with original items
                if(mSpecialityListAdapter != null) {
                    mSpecialityListAdapter.updateData(mSpecialityLookupsList);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if(text != null && text.length() > 0) {
            //TODO : implement search filter
            mSpecialityLookupsSearchList = new ArrayList<LookupsListObject>();
            for(LookupsListObject lookupsListObject : mSpecialityLookupsList){
                if(lookupsListObject.getValue().toLowerCase().contains(text.toString().toLowerCase())){
                    mSpecialityLookupsSearchList.add(lookupsListObject);
                }
            }
            mSpecialityListAdapter.updateData(mSpecialityLookupsSearchList);
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            mSpecialityListAdapter.updateData(mSpecialityLookupsList);
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    private class SpecialityListAdapter extends BaseAdapter {

        private ArrayList<LookupsListObject> specialityLookupsList;

        public SpecialityListAdapter(ArrayList<LookupsListObject> specialityLookupsList) {
            this.specialityLookupsList = specialityLookupsList;
        }

        public void updateData(ArrayList<LookupsListObject> specialityLookupsList) {
            this.specialityLookupsList = specialityLookupsList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (specialityLookupsList != null && specialityLookupsList.size() > 0) ? specialityLookupsList.size() : 0;
        }

        @Override
        public LookupsListObject getItem(int position) {
            return specialityLookupsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final LookupsListObject rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflator.inflate(R.layout.circles_list_row, viewGroup, false);
            }

            TextView specialityTextView = (TextView) convertView.findViewById(R.id.circleNameTextView);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            specialityTextView.setTypeface(regular);

            specialityTextView.setText(rowObject.getValue());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (speclialitySelectionListener != null) {
                        speclialitySelectionListener.onSpecialitySelected(rowObject);
                        mActivity.onBackPressed();
                    }
                }
            });

            return convertView;
        }
    }

    public interface SpeclialitySelectionListener {
        public void onSpecialitySelected(LookupsListObject selectedSpecialityObject);
    }

}
