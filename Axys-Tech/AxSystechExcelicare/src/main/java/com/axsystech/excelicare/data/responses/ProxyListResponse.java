package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by G Ajaykumar on 07/30/2015.
 */
public class ProxyListResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private ProxyListResponseData proxyListResponseData;

    public ProxyListResponseData getProxyListResponseData() {
        return proxyListResponseData;
    }


    public class ProxyListResponseData implements Serializable {
        @SerializedName("Details")
        private ArrayList<ProxieDetails> proxieDetailsAL;

        @SerializedName("LMD")
        private String lmd;

        public ArrayList<ProxieDetails> getProxieDetailsAL() {
            return proxieDetailsAL;
        }

        public String getLmd() {
            return lmd;
        }
    }

}