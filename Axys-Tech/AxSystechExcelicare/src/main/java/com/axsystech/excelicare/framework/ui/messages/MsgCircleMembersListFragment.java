package com.axsystech.excelicare.framework.ui.messages;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.CircleDetailsEditResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.AddCircleMemberFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.CircleDetailEditFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments;
import com.axsystech.excelicare.loaders.CircleDetailsLoader;
import com.axsystech.excelicare.loaders.CircleMembersDetailsLoader;
import com.axsystech.excelicare.loaders.ListMembersInCircleLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class MsgCircleMembersListFragment extends BaseFragment {

    private static final String LIST_MEMBERS_LOADER = "com.axsystech.excelicare.framework.ui.messages.MsgCircleMembersListFragment.LIST_MEMBERS_LOADER ";

    private String TAG = MsgCircleMembersListFragment.class.getSimpleName();
    private MsgCircleMembersListFragment mFragment;
    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.circlesListView)
    private ListView mCircleMembersListView;

    private String mCircleMemberTarget;

    private CircleMembersAdapter mCircleMembersAdapter;

    @Override
    protected void initActionBar() {
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_msg_circles_list, container, false);

        view.setFocusable(true);
//        view.setFocusableInTouchMode(true);
          view.setClickable(true);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        preloadData();
        addEventClickListener();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_CIRCLE_MEMBER_TARGET)) {
                mCircleMemberTarget = bundle.getString(AxSysConstants.EXTRA_CIRCLE_MEMBER_TARGET);
            }
        }
    }

    private void preloadData() {
        if (CommonUtils.hasInternet()) {
            // Reset global data here
            displayProgressLoader(false);
            if (isAdded() && getView() != null) {
                ListMembersInCircleLoader listMembersInCircleLoader = new ListMembersInCircleLoader(mActivity, mActiveUser.getToken(), "", "", 0, 0, "");
                if (isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MEMBERS_LOADER), listMembersInCircleLoader);
                }
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    private void addEventClickListener() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(LIST_MEMBERS_LOADER)) {
            if (result != null && result instanceof ListMembersInCircleResponse) {
                final ListMembersInCircleResponse listMembersInCircleResponse = (ListMembersInCircleResponse) result;

                if (listMembersInCircleResponse.getCircleMembersDataObject() != null &&
                        listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList() != null &&
                        listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList().size() > 0) {

                    ArrayList<ListMembersInCircleResponse.CircleMemberDetails> membersInSelectedCircle =
                            listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList();

                    // Display members in ListView
                    mCircleMembersAdapter = new CircleMembersAdapter(membersInSelectedCircle);
                    mCircleMembersListView.setAdapter(mCircleMembersAdapter);
                } else {
                    showToast(R.string.no_circle_members_exists);
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(LIST_MEMBERS_LOADER)) {
            showToast(R.string.no_circle_members_exists);
        }
    }

    private class CircleMembersAdapter extends BaseAdapter {

        private ArrayList<ListMembersInCircleResponse.CircleMemberDetails> membersInSelectedCircle;

        public CircleMembersAdapter(ArrayList<ListMembersInCircleResponse.CircleMemberDetails> membersInSelectedCircle) {
            this.membersInSelectedCircle = membersInSelectedCircle;
        }

        @Override
        public int getCount() {
            return membersInSelectedCircle != null && membersInSelectedCircle.size() > 0 ? membersInSelectedCircle.size() : 0;
        }

        @Override
        public ListMembersInCircleResponse.CircleMemberDetails getItem(int position) {
            return membersInSelectedCircle.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ListMembersInCircleResponse.CircleMemberDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.msg_circles_members_row, viewGroup, false);
            }

            TextView memberDetailsTextView = (TextView) convertView.findViewById(R.id.memberDetailsTextView);

            if (!TextUtils.isEmpty(rowObject.getUserCircleMemberEmail_ID())) {
                memberDetailsTextView.setText(rowObject.getUserCircleMemberEmail_ID());
            } else {
                memberDetailsTextView.setText(rowObject.getSurName() + " " + rowObject.getForeName());
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();

                    if (!TextUtils.isEmpty(mCircleMemberTarget)) {
                        if (mCircleMemberTarget.equalsIgnoreCase(MessageComposeFragment.class.getSimpleName())) {

                            Fragment fragment = fm.findFragmentByTag(MessageComposeFragment.class.getSimpleName());
                            if (fragment != null && fragment instanceof MessageComposeFragment) {
                                MessageComposeFragment messageComposeFragment = (MessageComposeFragment) fragment;
                                messageComposeFragment.onCircleMemberSelected(rowObject);
                            }
                        } else if (mCircleMemberTarget.equalsIgnoreCase(AddCircleMemberFragment.class.getSimpleName())) {

                            Fragment fragment = fm.findFragmentByTag(AddCircleMemberFragment.class.getSimpleName());
                            if (fragment != null && fragment instanceof AddCircleMemberFragment) {
                                AddCircleMemberFragment addCircleMemberFragment = (AddCircleMemberFragment) fragment;
                                addCircleMemberFragment.onCircleMemberSelected(rowObject);
                            }
                        }
                    }

                    mActivity.onBackPressed();
                }
            });

            return convertView;
        }
    }
}
