package com.babylon;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.babylon.model.Patient;
import com.babylon.model.PhysicalActivity;
import com.babylon.model.Region;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;
import com.babylon.utils.MediaPlayerAppointmentStarted;
import com.babylon.utils.PreferencesManager;
import com.babylonpartners.babylon.HomePageActivity;
import com.parse.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;



public class App extends Application {
    private static final String TAG = App.class.getSimpleName();

    private static App instance;
    private static String parseInstallationObjectId;
    private static MediaPlayerAppointmentStarted mediaPlayer;
    private PreferencesManager preferencesManager;
    private Patient patient;
    private PhysicalActivity walkingActivity;
    private Toast signingOutToast;
    private Location location;
    private String babylonToken;
    private RequestQueue mRequestQueue;


    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null) {
            instance = this;
        }

        mRequestQueue = Volley.newRequestQueue(this);


        new File(BaseConfig.CACHE_DIR).mkdirs();

        /* Initialize 3rd party libraries */
        Parse.initialize(this, Config.PARSE_APPLICATION_KEY, Config.PARSE_CLIENT_KEY);
         /*  Parse push notification signing */

        final ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        PushService.setDefaultPushCallback(this, HomePageActivity.class);
        PushService.startServiceIfRequired(this);
        if (parseInstallation != null) {
            parseInstallation.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    parseInstallationObjectId = parseInstallation.getObjectId();
                }
            });
        }
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        preferencesManager = PreferencesManager.getInstance();
    }

    public boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess
                    .processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public MediaPlayerAppointmentStarted getMediaPlayer() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayerAppointmentStarted();
        }
        return mediaPlayer;
    }

    public void stopMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    public void showWaitingMessage(int string) {
        if (signingOutToast != null) {
            signingOutToast.cancel();
        }
        signingOutToast = Toast.makeText(this, string, Toast.LENGTH_LONG);
        signingOutToast.show();
    }

    private void clearPatientProfile() {
        App.getInstance().setPatient(new Patient());
    }

    public String getDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getParseInstallationObjectId() {
        return parseInstallationObjectId;
    }

    public static App getInstance() {
        return instance;
    }

    public boolean isConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        preferencesManager.setString(Keys.PATIENT, GsonWrapper.getGson().toJson(patient));
    }

    public Patient getPatient() {
        if (patient == null) {
            String patientJson = preferencesManager.getString(Keys.PATIENT, "{}");
            patient = GsonWrapper.getGson().fromJson(patientJson, Patient.class);
        }
        return patient;
    }

    public Region getCurrentRegion() {
        Region[] regions = PreferencesManager.getInstance().getRegions();
        if (patient != null) {
            Integer regionId = patient.getRegionId();
            for (Region region : regions) {
                if (region.getId().equals(regionId)) {
                    return region;
                }
            }
        }
        return null;
    }

    public void setLocation(Location currentLocation) {
        location = currentLocation;
        getAddress();
    }

    public @Nullable Location getLocation() {
        return location;
    }

    public Address getAddress() {
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        if(location!=null) {
            try {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if(addresses.size() > 0) {
                    return addresses.get(0);
                }

            } catch (IOException e) {
                L.e(TAG, e.getMessage(), e);
            }
        }
        return null;
    }


    public PhysicalActivity getWalkingActivity() {

        if (walkingActivity == null) {
            String walkingJson = preferencesManager.getString(Keys.WALKING_ACTIVITY_KEY, "{}");
            walkingActivity = GsonWrapper.getGson().fromJson(walkingJson, PhysicalActivity.class);
        }

        return walkingActivity;
    }

    public void setWalkingActivity(PhysicalActivity walkingActivity) {
        if (walkingActivity != null) {
            this.walkingActivity = walkingActivity;
            preferencesManager.setString(Keys.WALKING_ACTIVITY_KEY, GsonWrapper.getGson().toJson(walkingActivity));
            Integer steps = walkingActivity.getSteps();
            L.d(TAG, "Steps = " + String.valueOf(steps));
        }
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

}
