package com.babylon.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.*;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.babylon.App;
import com.babylon.Config;
import com.babylon.R;
import com.babylon.activity.MonitorMeActivity;
import com.babylon.activity.PhoneNumberDialogActivity;
import com.babylon.activity.RatingScreenActivity;
import com.babylon.activity.payments.RecordTransactionActivity;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Notification;
import com.babylon.model.Patient;
import com.babylon.utils.BackbonePageConstants;
import com.babylon.utils.CommonHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylonpartners.babylon.HomePageActivity;
import com.babylonpartners.babylon.HomePageBackboneActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CarosselAdapter extends PagerAdapter
{
    protected ImageLoader loader;
    private Context mContext;
    private ImageLoader mImageLoader;
    private SimpleDateFormat mFormat;
    private Date mStartDate = null;
    private int mMinutes;
    private int mHours;
    private int mDays;
    private String mMessageString;
    private long mDifference;
    private ArrayList<Notification> mDummyList = new ArrayList<>();
    private Button btnNotification;
    private RequestQueue mQueue;

    public CarosselAdapter(Context act,ArrayList<Notification> dummyList)
    {
        mContext = act;
        mDummyList = dummyList;
        getImageLoader();
        mFormat  = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS", Locale.UK);
    }

    public int getCount()
    {
        return mDummyList.size();
    }

    public Object instantiateItem(ViewGroup container, int position)
    {
        LayoutInflater mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mLayoutInflater.inflate(R.layout.notification_banner,container, false);

        final TextView temp = (TextView) itemView.findViewById(R.id.notification_text);
        ImageView notificationImage = (ImageView) itemView.findViewById(R.id.notification_banner_image);

        //notificationImage.setImageUrl(Config.RUBY_SERVER_URL + "/" + mDummyList.get(position).getImagePath(),mImageLoader);
        Picasso.with(mContext).load(Config.RUBY_SERVER_URL + "/" + mDummyList.get(position).getImagePath())
        .error(R.drawable.heart_logo).placeholder(R.drawable.heart_logo).into(notificationImage);

        //notificationImage.setErrorImageResId(R.drawable.heart_logo);


        //click of notification bar handled
        itemView.setTag(mDummyList.get(position));
        itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                HandleNotification(v);
            }
        });


        //Getting time for appointment
        if(mDummyList.get(position).getItemType().equals("upcoming_appointment"))
        {
            try
            {
                mStartDate = mFormat.parse(mDummyList.get(position).getItemDetails().getDate());
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }

            Date date = new Date();
            //date.setHours(date.getHours() + 8);
            printDifference(date,mStartDate,position);
            temp.setText(mMessageString);
        }
        else
        {
            temp.setText(mDummyList.get(position).getMessage());
        }


       // notificationImage.set(Urls.getRuby(mDummyList.get(position).getImagePath()));
        btnNotification = (Button) itemView.findViewById(R.id.notification_banner_button);
        btnNotification.setText(mDummyList.get(position).getButtonTitle());
        btnNotification.setTag(mDummyList.get(position));

        btnNotification.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                HandleNotification(v);
            }

        });

        container.addView(itemView);

        return itemView;
    }

    public JSONObject getJSONObject(String id)
    {
        JSONObject apiJsonObject = new JSONObject();

        JSONObject passwordObj = new JSONObject();
        try {
            passwordObj.put("stage", "performed");
            apiJsonObject.put("notification", passwordObj);
            apiJsonObject.put("id", id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return apiJsonObject;
    }

    private void patchRequest(String id)
    {
        String mUri= CommonHelper.getUrl(Urls.PATCH_NOTIFICATIONS).concat(id);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, mUri, getJSONObject(id),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Adapter","Error");

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2)
    {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == ((LinearLayout) object);
    }

    public void getImageLoader()
    {
        mImageLoader = new ImageLoader(App.getInstance().getRequestQueue(),new ImageLoader.ImageCache()
        {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(100 * 1024 * 1024);
            public void putBitmap(String url, Bitmap bitmap)
            {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url)
            {
                return mCache.get(url);
            }
        });
    }

    public void calculateDate(Date mAppointmentDate, int position)
    {
        if(mAppointmentDate != null)
        {
            mDifference = mStartDate.getTime() - new Date().getTime();
            mMinutes = (int)TimeUnit.MILLISECONDS.toMinutes(mDifference);
        }

        if(mMinutes > 60)
        {
            //calculate hours and minutes

            //Check if hours > 24 -> then we will do days
            mHours = (int)TimeUnit.MINUTES.toHours(mMinutes);
            if(mHours >= 24)
            {
                mDays = (int)TimeUnit.DAYS.toDays(mDifference);
                mMessageString = mDummyList.get(position).getMessage().substring(0,23) + mDays + " days";
            }
            else
            {
                if(mHours > 1)
                {
                    mMessageString = mDummyList.get(position).getMessage().substring(0, 23) + mHours + " hours";
                }
                else
                {
                    mMessageString = mDummyList.get(position).getMessage().substring(0, 23) + mHours + " hour";
                }

                int temp = mHours % 60;

            }
        }
        else
        {
            if(mHours > 1)
            {
                mMessageString = mDummyList.get(position).getMessage().substring(0, 23) + mHours + " hours";
            }
            else if(mHours == 1)
            {
                mMessageString = mDummyList.get(position).getMessage().substring(0, 23) + mHours + " hour";
            }
            else
            {

            }
        }
    }

    public void printDifference(Date startDate, Date endDate, int position)
    {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        mDays = (int) elapsedDays;
        mHours = (int) elapsedHours;
        mMinutes = (int) elapsedMinutes;
        String minuteString;
        String hourString;

        /*if(mHours > 1)
        {
            hourString = " hours";
        }
        else {
            hourString = " hour";
        }
        if(mDays > 1)
        {
            mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mDays + " days");
        }
        else if(mDays == 1)
        {
            if(mHours < 1)
            {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mDays + " day");
            }
            else {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}", mDays + " day");
            }
        }
        else
        {
            if(mMinutes > 1)
            {
                minuteString = " minutes";
            }
            else
            {
                minuteString = " minute";
            }
            if(mHours > 1)
            {
                if(endDate.getDay() > startDate.getDay())
                {
                    mMessageString = mDummyList.get(position).getMessage().replace("{{time}}", "1 day");
                }
                else
                {
                    mMessageString = mDummyList.get(position).getMessage().replace("{{time}}", mHours + " hours and " + mMinutes + minuteString);
                }
            }
            else if(mHours == 1)
            {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mHours + " hour and " + mMinutes + minuteString);
            }
            else
            {
                if(mMinutes > 1)
                {
                    mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mMinutes + " minutes");
                }
                else if(mMinutes == 1)
                {
                    mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mMinutes + " minute");
                }
                else
                {
                    mMessageString = "Your appointment is now";
                }
            }
        }*/

        if (mDays > 0)
        {
            if (mDays == 1)
            {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mDays + " day");
            }
            else
            {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mDays + " days");
            }
        }
        else
        {
            if (mHours >= 1)
            {
                if (mMinutes == 0)
                {
                    mMessageString = mDummyList.get(position).getMessage().replace("{{time}}", mHours + " hours");
                }
                else
                {
                    mMessageString = mDummyList.get(position).getMessage().replace("{{time}}", mHours + " hours and " + mMinutes + " minutes");
                }
            }
            else if (mMinutes == 0)
            {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}"," less than 1 minute");
            }
            else if(mMinutes < 0)
            {
                mMessageString = "Your appointment is now";
            }
            else
            {
                mMessageString = mDummyList.get(position).getMessage().replace("{{time}}",mMinutes + " minutes");
            }
        }

    }

    public void askQuestion()
    {
        Patient patient = App.getInstance().getPatient();
        if (patient == null) {
            //   noInternetAlert();
        } else {
            App.getInstance().setPatient(patient);
            if (patient.hasPhoneNumber()) {
                ((HomePageActivity)mContext).startAskControllerActivity(null);
            } else
            {
                ((HomePageActivity)mContext).startActivityForResult(new Intent(mContext,PhoneNumberDialogActivity.class),HomePageActivity.ASK_PHONE_NUMBER_REQUEST_CODE);
            }
        }
    }

    public void HandleNotification(View v)
    {
        Notification tempNotification = (Notification) v.getTag();

        if (tempNotification.getItemType().compareToIgnoreCase("new_prescription") == 0)
        {
            //Mix Panel event for creating family account
            JSONObject props = new JSONObject();
            MixPanelHelper helper = new MixPanelHelper(App.getInstance(),props,null);
            helper.sendEvent(MixPanelEventConstants.PRESCRIPTION_OPENED);

            //patchRequest(tempNotification.getId());
            //Start backbone activity with prescription screen
            Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
            homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
            homePageIntent.putExtra("PAGE2", BackbonePageConstants.SHOW_PRESCRIPTIONS);
            homePageIntent.putExtra("TOPBAR", "Prescription");
            homePageIntent.putExtra("URL_FORMAT_NEEDED", tempNotification.getItemId());
            mContext.startActivity(homePageIntent);

        } else if (tempNotification.getItemType().compareToIgnoreCase("ADD_COMMENT") == 0) {
            patchRequest(tempNotification.getId());
            askQuestion();
        } else if (tempNotification.getItemType().compareToIgnoreCase("REJECT_QUESTION") == 0) {
            patchRequest(tempNotification.getId());
            askQuestion();
        } else if (tempNotification.getItemType().compareToIgnoreCase("SUGGEST_CONSULTATION") == 0) {
            patchRequest(tempNotification.getId());
            askQuestion();
        } else if (tempNotification.getItemType().compareToIgnoreCase("upcoming_appointment") == 0) {
            //patchRequest(tempNotification.getId());
            Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
            homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
            homePageIntent.putExtra("PAGE2", BackbonePageConstants.SHOW_APPOINTMENT);
            homePageIntent.putExtra("TOPBAR", "Appointment");
            homePageIntent.putExtra("URL_FORMAT_NEEDED", tempNotification.getItemId());
            mContext.startActivity(homePageIntent);
        } else if (tempNotification.getButtonTitle().compareToIgnoreCase("Dismiss") == 0) {
            patchRequest(tempNotification.getId());
        }
        else if (tempNotification.getItemType().compareToIgnoreCase("SUBSCRIPTION") == 0) {
            Intent intent = new Intent(mContext, RecordTransactionActivity.class);
            intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.SUBSCRIPTIONS_FRAGMENT_TAG);
            mContext.startActivity(intent);
        }
        else if (tempNotification.getItemType().compareToIgnoreCase("BLOOD_ANALYSIS.APPOINTMENT") == 0) {
            patchRequest(tempNotification.getId());
            Intent homePageIntent = new Intent(mContext,HomePageBackboneActivity.class);
            homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
            homePageIntent.putExtra("PAGE2", BackbonePageConstants.BLOOD_ANALYSIS_BOOK_APPOINTMENT);
            mContext.startActivity(homePageIntent);
        } else if (tempNotification.getItemType().equals("BLOOD_ANALYSIS.HEALTHY")) {
            patchRequest(tempNotification.getId());
            mContext.startActivity(new Intent(mContext, MonitorMeActivity.class));
        }
        else if (tempNotification.getItemType().compareToIgnoreCase("PATHOLOGY_TEST") == 0)
        {
            Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
            homePageIntent.putExtra("TOPBAR", "Pathology");
            homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
            homePageIntent.putExtra("PAGE2", BackbonePageConstants.PATHOLOGY_SHOW);
            homePageIntent.putExtra("URL_FORMAT_NEEDED", tempNotification.getItemId());
            mContext.startActivity(homePageIntent);
            //loadEnvironment(HtmlPage.index, "#pathology-tests/" + pushNotification.getItemId());
        } else if (tempNotification.getItemType().compareToIgnoreCase("REFERRAL") == 0) {
            //  loadEnvironment(HtmlPage.index, "#referral-notifications/"+ Uri.encode(pushNotification.getSpecialism()));


        }
        else if (tempNotification.getItemType().compareToIgnoreCase("ACCEPT_UNVERIFIED_PATIENT") == 0) {
            Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
            homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
            homePageIntent.putExtra("PAGE2", BackbonePageConstants.SHOW_APPOINTMENT);
            homePageIntent.putExtra("URL_FORMAT_NEEDED", tempNotification.getItemId());
            mContext.startActivity(homePageIntent);
        }
        else if (tempNotification.getItemType().compareToIgnoreCase("PHARMACY_UNAVAILABLE") == 0) {
            //Start backbone activity with prescription screen
            Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
            homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
            homePageIntent.putExtra("PAGE2", BackbonePageConstants.SHOW_PRESCRIPTIONS);
            homePageIntent.putExtra("URL_FORMAT_NEEDED", tempNotification.getItemId());
            mContext.startActivity(homePageIntent);
        }
        else if (tempNotification.getItemType().compareToIgnoreCase("RATE_APPOINTMENT") == 0) {
            //Start backbone activity with prescription screen
            Intent homePageIntent = new Intent(mContext, RatingScreenActivity.class);
            homePageIntent.putExtra("ITEM_ID",tempNotification.getItemId());
            mContext.startActivity(homePageIntent);
        }

    }

}