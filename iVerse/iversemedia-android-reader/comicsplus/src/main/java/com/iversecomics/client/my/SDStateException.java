package com.iversecomics.client.my;

import java.io.IOException;

/**
 * Thrown when SD is in an undesired state.
 */
public class SDStateException extends IOException {
    private static final long serialVersionUID = 2299551568791089282L;

    public SDStateException(String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
    }
}
