package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;

/**
 * Created by Babylon on 12/05/2015.
 */
public class ListOrder {

    @Expose
    private Order order;

    /**
     *
     * @return
     * The order
     */
    public Order getOrder() {
        return order;
    }

    /**
     *
     * @param order
     * The order
     */
    public void setOrder(Order order) {
        this.order = order;
    }

}