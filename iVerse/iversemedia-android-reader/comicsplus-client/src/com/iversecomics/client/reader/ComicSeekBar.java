package com.iversecomics.client.reader;

import com.iversecomics.client.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class ComicSeekBar extends SeekBar {
	
	static Bitmap bitmapFull = null;
	static Bitmap bitmapBlank = null;
	static Bitmap bitmapButton = null;

	public ComicSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (bitmapFull == null) {
			bitmapFull = ((BitmapDrawable)context.getResources().getDrawable(R.drawable.slider_full)).getBitmap();
			bitmapBlank = ((BitmapDrawable)context.getResources().getDrawable(R.drawable.slider_blank)).getBitmap();
			bitmapButton = ((BitmapDrawable)context.getResources().getDrawable(R.drawable.slider_button)).getBitmap();
		}
	}

	Rect rectSrc = new Rect();
	Rect rectDst = new Rect();
	public void onDraw(Canvas canvas) {
		int width = this.getWidth();
		int height = this.getHeight();
		
		int size = height * 8 / 10;
		int top = (height - size) / 2;
		int sizeButton = height;
		int topButton = (height - sizeButton) / 2;
		int left = sizeButton / 2;
		int right = width - sizeButton / 2;
		int progress = this.getProgress();
		int max = this.getMax();
		int pos = left + (right - left) * progress / max;
		// draw left side
		{
			int bw = bitmapFull.getWidth();
			int bh = bitmapFull.getHeight();
			rectSrc.left = 0; rectSrc.top = 0; rectSrc.right = bw / 2; rectSrc.bottom = bh;
			rectDst.left = left - size / 2; rectDst.top = top; rectDst.right = left; rectDst.bottom = top + size;
			canvas.drawBitmap(bitmapFull, rectSrc, rectDst, null);
			rectSrc.left = bw / 2; rectSrc.top = 0; rectSrc.right = bw / 2 + 1; rectSrc.bottom = bh;
			rectDst.left = left; rectDst.top = top; rectDst.right = pos; rectDst.bottom = top + size;
			canvas.drawBitmap(bitmapFull, rectSrc, rectDst, null);
		}
		// draw right side
		{
			int bw = bitmapBlank.getWidth();
			int bh = bitmapBlank.getHeight();
			rectSrc.left = bw / 2; rectSrc.top = 0; rectSrc.right = bw; rectSrc.bottom = bh;
			rectDst.left = right; rectDst.top = top; rectDst.right = right + size / 2; rectDst.bottom = top + size;
			canvas.drawBitmap(bitmapBlank, rectSrc, rectDst, null);
			rectSrc.left = bw / 2; rectSrc.top = 0; rectSrc.right = bw / 2 + 1; rectSrc.bottom = bh;
			rectDst.left = pos; rectDst.top = top; rectDst.right = right; rectDst.bottom = top + size;
			canvas.drawBitmap(bitmapBlank, rectSrc, rectDst, null);
		}
		// draw button
		{
			int bw = bitmapButton.getWidth();
			int bh = bitmapButton.getHeight();
			rectSrc.left = 0; rectSrc.top = 0; rectSrc.right = bw; rectSrc.bottom = bh;
			rectDst.left = pos - sizeButton / 2; rectDst.top = topButton; rectDst.right = pos + sizeButton / 2; rectDst.bottom = sizeButton;
			canvas.drawBitmap(bitmapButton, rectSrc, rectDst, null);
		}
	}
}
