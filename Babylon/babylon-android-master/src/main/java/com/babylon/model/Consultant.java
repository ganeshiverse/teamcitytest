package com.babylon.model;

public class Consultant {
    private Integer id;
    private String name;
    private Boolean isSelected;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isSelected() {
        return isSelected;
    }

    public void setSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }
}
