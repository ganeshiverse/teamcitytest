package com.axsystech.excelicare.data.responses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class ListMembersInCircleResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private CircleMembersDataObject circleMembersDataObject;

    public CircleMembersDataObject getCircleMembersDataObject() {
        return circleMembersDataObject;
    }

    public class CircleMembersDataObject implements Serializable {

        @SerializedName("Details")
        private ArrayList<CircleMemberDetails> circleMemberDetailsArrayList;

        @SerializedName("LMD")
        private String LMD;

        public ArrayList<CircleMemberDetails> getCircleMemberDetailsArrayList() {
            return circleMemberDetailsArrayList;
        }

        public String getLMD() {
            return LMD;
        }
    }

    public class CircleMemberDetails implements Serializable {

        @SerializedName("EndReasonComment")
        private String endReasonComment;

        @SerializedName("EndReason_LU")
        private String endReason_LU;

        @SerializedName("ForeName")
        private String foreName;

        @SerializedName("IsActive")
        private String isActive;

        @SerializedName("LastModifiedDate")
        private String lastModifiedDate;

        @SerializedName("LastModifiedUserID")
        private String lastModifiedUserID;

        @SerializedName("RoleType_SLU")
        private String roleType_SLU;

        @SerializedName("Speciality")
        private String speciality;

        @SerializedName("StartDate")
        private String startDate;

        @SerializedName("StopDate")
        private String stopDate;

        @SerializedName("SurName")
        private String surName;

        @SerializedName("URL")
        private String URL;

        @SerializedName("UserCircleMemberEmail_ID")
        private String userCircleMemberEmail_ID;

        @SerializedName("UserCircleMember_ID")
        private String userCircleMember_ID;

        @SerializedName("UserCircle_ID")
        private String userCircle_ID;

        @SerializedName("UserID")
        private String userID;

        @SerializedName("UserType")
        private String userType;

        public String getSpeciality() {
            return speciality;
        }

        public String getEndReasonComment() {
            return endReasonComment;
        }

        public String getEndReason_LU() {
            return endReason_LU;
        }

        public String getForeName() {
            return foreName;
        }

        public String getIsActive() {
            return isActive;
        }

        public String getLastModifiedDate() {
            return lastModifiedDate;
        }

        public String getLastModifiedUserID() {
            return lastModifiedUserID;
        }

        public String getRoleType_SLU() {
            return roleType_SLU;
        }

        public String getStartDate() {
            return startDate;
        }

        public String getStopDate() {
            return stopDate;
        }

        public String getSurName() {
            return surName;
        }

        public String getURL() {
            return URL;
        }

        public String getUserCircleMemberEmail_ID() {
            return userCircleMemberEmail_ID;
        }

        public String getUserCircleMember_ID() {
            return userCircleMember_ID;
        }

        public String getUserCircle_ID() {
            return userCircle_ID;
        }

        public String getUserID() {
            return userID;
        }

        public String getUserType() {
            return userType;
        }
    }
}
