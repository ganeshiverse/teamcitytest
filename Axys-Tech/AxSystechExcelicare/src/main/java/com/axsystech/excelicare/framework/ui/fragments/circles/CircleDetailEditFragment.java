package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleDetailsEditResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.data.responses.SaveMyCircleResponse;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.DatePickerFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment;
import com.axsystech.excelicare.loaders.CircleListSearchLoader;
import com.axsystech.excelicare.loaders.GetCircleDetailsDbLoader;
import com.axsystech.excelicare.loaders.GetCircleMemberDetailsFromDbLoader;
import com.axsystech.excelicare.loaders.RemoveCircleLoader;
import com.axsystech.excelicare.loaders.RemoveInvitationLoader;
import com.axsystech.excelicare.loaders.SaveCircleDetailsLoader;
import com.axsystech.excelicare.loaders.SaveMyCircleLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class CircleDetailEditFragment extends BaseFragment implements View.OnClickListener {

    private MainContentActivity mActivity;
    private String TAG = CircleDetailEditFragment.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private CircleDetailEditFragment mFragment;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.noteTextView)
    private TextView mTextViewNote;

    @InjectView(R.id.noteTitleTextView)
    private TextView nNoteTitleTextView;

    @InjectView(R.id.CircleNameEditText)
    private EditText editTextCircleName;

    @InjectView(R.id.CalenderEditText)
    private EditText editTextCalendar;

    @InjectView(R.id.CommentEditText)
    private EditText editTextComment;

    @InjectView(R.id.EndCommentsEditText)
    private EditText editTextEndComments;

    @InjectView(R.id.buttonDone)
    private Button mButtonDone;

    @InjectView(R.id.dateText)
    private  TextView dateText;

    @InjectView(R.id.EndCircleUnCheckbox)
    private CheckBox unCheckBoxEndCircle;


    @InjectView(R.id.EndCircleCheckbox)
    private CheckBox checkBoxEndCircle;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    String userType;
    String userId;

    private int year;
    private int month;
    private int day;

    static final int DATE_PICKER_ID = 1111;
    private CircleDetailsEditResponse.DataObjectCircleDetails mDataObjectCircleDetail;
    private ArrayList<CircleDetailsDbModel> dbCircleDetailsDbModels;
    private Calendar mSelectedStartDateCalendar = Calendar.getInstance();

    private static final String EDIT_CIRCLE_DETAILS = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleDetailEditFragment.EDIT_CIRCLE_DETAILS";
    private static final String REMOVE_CIRCLE_DETAILS = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleDetailEditFragment.REMOVE_CIRCLE_DETAILS";
    private static final String GET_CIRCLE_DETAILS_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleDetailEditFragment.GET_CIRCLE_DETAILS_DB_LOADER";

    @Override
    protected void initActionBar() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_circle, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);
        String Date= DateFormat.getDateTimeInstance().format(new Date());
        dateText.setText(Date);


        getDetailsFromDb();
        readIntentArgs();
        addEventClickListener();
    }

    private void getDetailsFromDb() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        final GetCircleDetailsDbLoader getCircleDetailsDbLoader = new GetCircleDetailsDbLoader(mActivity);
        if (isAdded() && getView() != null) {
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CIRCLE_DETAILS_DB_LOADER), getCircleDetailsDbLoader);

        }
    }


    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID)) {
                userType = bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CIRCLEID)) {
                userId = bundle.getString(AxSysConstants.EXTRA_SELECTED_CIRCLEID);
            }

        }
    }

    /*private void updateUi() {

    }*/


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    public void addEventClickListener() {

        mButtonDone.setOnClickListener(this);
        editTextCalendar.setOnClickListener(this);
        checkBoxEndCircle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (checkBoxEndCircle.isChecked()) {
                    checkBoxEndCircle.setButtonDrawable(R.drawable.newchecked);
                    dateText.setVisibility(View.VISIBLE);
                    mTextViewNote.setVisibility(View.VISIBLE);
                } else {
                    checkBoxEndCircle.setButtonDrawable(R.drawable.newunchecked);
                    dateText.setVisibility(View.GONE);
                    mTextViewNote.setVisibility(View.GONE);
                    nNoteTitleTextView.setVisibility(View.GONE);
                }
            }
        });
       /* unCheckBoxEndCircle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (unCheckBoxEndCircle.isChecked()) {
                    checkBoxEndCircle.setVisibility(View.VISIBLE);
                    unCheckBoxEndCircle.setVisibility(View.INVISIBLE);
                    mTextViewNote.setVisibility(View.GONE);
                } else {
                    checkBoxEndCircle.setVisibility(View.INVISIBLE);
                    mTextViewNote.setVisibility(View.VISIBLE);
                }
            }
        });*/
    }

    private void showDatePicker() {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt("year", mSelectedStartDateCalendar.get(Calendar.YEAR));
        args.putInt("month", mSelectedStartDateCalendar.get(Calendar.MONTH));
        args.putInt("day", mSelectedStartDateCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerFragment.setArguments(args);

        datePickerFragment.setCallBack(onDateSet);
        datePickerFragment.show(getActivity().getFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    private DatePickerDialog.OnDateSetListener onDateSet = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mSelectedStartDateCalendar.set(year, monthOfYear, dayOfMonth);

            editTextCalendar.setText(dayOfMonth + "/" + monthOfYear + "/" + year);

        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.CalenderEditText:
                showDatePicker();
                break;
            case R.id.buttonDone:
                if (CommonUtils.hasInternet()) {
                    if (checkBoxEndCircle.isChecked()) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                getActivity());
                        alert.setMessage(getString(R.string.confirmation_to_delete_circle));
                        alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (CommonUtils.hasInternet()) {
                                    displayProgressLoader(false);
                                    RemoveCircleLoader removeCircleLoader = new RemoveCircleLoader(mActivity, mActiveUser.getToken(), userId, "", "");
                                    if (isAdded() && getView() != null) {
                                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REMOVE_CIRCLE_DETAILS), removeCircleLoader);
                                    }
                                } else {
                                    showToast(R.string.error_no_network);
                                }
                            }
                        });
                        alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        alert.show();

                    } else {
                        if (validateUserInputs()) {
                            // Create SaveCircle request body
                            JSONObject mainObject = new JSONObject();

                            try {
                                JSONObject dataObject = new JSONObject();
                                dataObject.put("UserID", mActiveUser != null ? mActiveUser.getEcUserID() + "" : "");
                                dataObject.put("CircleDescription", editTextComment.getText().toString().trim());
                                dataObject.put("StopDate", "");
                                dataObject.put("StopReason_SLU", "");
                                dataObject.put("StopReasonDesc", "");
                                dataObject.put("UserType", userType);
                                dataObject.put("StartDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
                                dataObject.put("CircleName", editTextCircleName.getText().toString());
                                dataObject.put("UserCircle_ID", userId);
                                dataObject.put("IsProtected", "0");

                                mainObject.put("Data", dataObject.toString());

                                mainObject.put("Token", mActiveUser != null ? mActiveUser.getToken() : "");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // POST saveCircle request
                            if (CommonUtils.hasInternet()) {
                                displayProgressLoader(false);
                                SaveCircleDetailsLoader saveMyCircleLoader = new SaveCircleDetailsLoader(mActivity, mainObject.toString());
                                if (isAdded() && getView() != null) {
                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(EDIT_CIRCLE_DETAILS), saveMyCircleLoader);
                                }
                            } else {
                                showToast(R.string.error_no_network);
                            }
                        }
                    }
                } else {
                    showToast(R.string.error_network_required);
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            case R.id.actionSave:


                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
        if (id == getLoaderHelper().getLoaderId(EDIT_CIRCLE_DETAILS)) {

        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(EDIT_CIRCLE_DETAILS)) {

            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponse = (BaseResponse) result;

                if (baseResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(baseResponse.getMessage());
                            mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(REMOVE_CIRCLE_DETAILS)) {

            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponse = (BaseResponse) result;

                if (baseResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(baseResponse.getMessage());
                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            int stackCount = fm.getBackStackEntryCount();
                            for (int i = 0; i < stackCount; ++i) {
                                int backStackId = fm.getBackStackEntryAt(i).getId();
                                String backStackName = fm.getBackStackEntryAt(i).getName();

                                if (!TextUtils.isEmpty(backStackName)) {
                                    if (!backStackName.equalsIgnoreCase(CirclesListFragment.class.getSimpleName())
                                            ) {
                                        fm.popBackStack(backStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    }
                                }
                            }
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_CIRCLE_DETAILS_DB_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                dbCircleDetailsDbModels = (ArrayList<CircleDetailsDbModel>) result;
                if (dbCircleDetailsDbModels != null && dbCircleDetailsDbModels.size() > 0) {
                    for (int i = 0; i < dbCircleDetailsDbModels.size(); i++) {
                        try {
                            editTextCircleName.setText(dbCircleDetailsDbModels.get(i).getCircleName());
                            editTextComment.setText(dbCircleDetailsDbModels.get(i).getComments());
                            editTextEndComments.setText(dbCircleDetailsDbModels.get(i).getStopReasonDesc());
                            String formatedDate = parseDateToddMMyyyy(dbCircleDetailsDbModels.get(i).getStartDate());
                            editTextCalendar.setText(formatedDate);

                            userType = dbCircleDetailsDbModels.get(i).getUserType();
                            userId = dbCircleDetailsDbModels.get(i).getUserCircleId();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(editTextCircleName.getText().toString().trim())) {
            showToast(R.string.input_circle_name);
            editTextCircleName.requestFocus();
            return false;
        }
        return true;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
