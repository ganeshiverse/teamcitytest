package com.iversecomics.bundle.parse;

import android.text.TextUtils;

import com.iversecomics.bundle.BundleReadException;
import com.iversecomics.bundle.BundleSecurityException;
import com.iversecomics.bundle.ComicContent;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class ParseBundleType5 extends AbstractParseBundle {
    private final static Logger LOG = LoggerFactory.getLogger(ParseBundleType5.class);

    /**
     * Section Types
     */
    private static final short ID = 1;
    private static final short NAME = 3;
    private static final short WRITER = 5;
    private static final short ARTIST = 6;
    private static final short PUBLISHER_NAME = 7;
    private static final short PUBLISHER_URL = 8;
    private static final short PUBLISH_DATE = 9;
    private static final short COPYRIGHT = 11;
    private static final short PANEL_COUNT = 12;
    private static final short VERTICAL_COUNT = 13;
    private static final short CONTENT_COUNT = 14;
    private static final short SYNOPSIS = 15;
    private static final short DESCRIPTION = 16;
    private static final short CATEGORY = 17;
    private static final short PUBLISHER_ID = 18;
    private static final short PRODUCT_TYPE = 19;
    private static final short SERIES_COMIC_ID = 20;
    private static final short ICON = 100;
    private static final short THUMB = 101;
    private static final short IMAGE_PANEL = 110;
    @SuppressWarnings("unused")
    private static final short IMAGE_PANEL_ENCRYPTED = 111;
    private static final short IMAGE_VERTICAL = 112;
    @SuppressWarnings("unused")
    private static final short IMAGE_VERTICAL_ENCRYPTED = 113;
    @SuppressWarnings("unused")
    private static final short COMIC_IMAGE_SMALL = 114;
    @SuppressWarnings("unused")
    private static final short COMIC_IMAGE_MEDIUM = 115;
    @SuppressWarnings("unused")
    private static final short COMIC_IMAGE_LARGE = 116;
    @SuppressWarnings("unused")
    private static final short COVER = 117;
    private static final short COMIC_AD_HORIZONTAL = 120;
    private static final short COMIC_AD_VERTICAL = 121;
    private static final short GENERATED_DATE = 201;
    private static final short GENERATED_BY = 202;
    private static final short OWNER_EMAIL_ENCRYPTED = 203;

    private static final short MANGA_ORIENTATION = 207;               // Short (Signed/16-bit) Flag for manga
    private static final short RESERVED_STRING_1 = 208;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_2 = 209;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_3 = 210;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_4 = 211;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_5 = 212;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_6 = 213;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_7 = 214;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_8 = 215;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_9 = 216;               // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short RESERVED_STRING_10 = 217;              // String (UTF-8)    Reserved for future expansion without requiring a format rev
    private static final short HORIZONTAL_FADES = 218;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_2 = 219;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_3 = 220;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_4 = 221;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_5 = 222;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_6 = 223;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_7 = 224;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_8 = 225;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_9 = 226;                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
    private static final short RESERVED_SHORT_10 = 227;               // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev

    private static final short BUNDLE_CHECKSUM = 299;
    private static final short MAX_TYPE = BUNDLE_CHECKSUM;

    @Override
    public void parse(BundleAccess ba) throws BundleReadException {
        LOG.debug("Parsing Bundle Version 5");
        if (ownership == null) {
            metadata.setStatus(ComicMetadata.STATUS_LIB_FAILURE);
            // Throw an exception, but be vague.
            throw new BundleSecurityException("Ownership not initialized (1).");
        }

        try {
            short type = ba.readShort();
            while (type != 0) {
                LOG.debug("type: %d", type);
                if ((type < 0) || (type > MAX_TYPE)) {
                    throw new IOException("Invalid type <" + type + "> at offset <" + ba.getFilePointer() + ">");
                }
                switch (type) {
                    // Metadata
                    case ID:
                        metadata.setId(ba.readString());
                        break;
                    case SERIES_COMIC_ID:
                        metadata.setSeriesId(ba.readString());
                        break;
                    case NAME:
                        metadata.setName(ba.readString());
                        break;
                    case SYNOPSIS:
                        metadata.setSynopsis(ba.readString());
                        break;
                    case DESCRIPTION:
                        metadata.setDescription(ba.readString());
                        break;
                    case CATEGORY:
                        metadata.addCategory(ba.readString());
                        break;
                    case PUBLISHER_ID:
                        metadata.setPublisherId(ba.readString());
                        break;
                    case PRODUCT_TYPE:
                        metadata.setProductType(ba.readString());
                        break;
                    case WRITER:
                        metadata.addWriter(ba.readString());
                        break;
                    case ARTIST:
                        metadata.addArtist(ba.readString());
                        break;
                    case PUBLISHER_NAME:
                        metadata.setPublisherName(ba.readString());
                        break;
                    case PUBLISHER_URL:
                        metadata.setPublisherURL(ba.readString());
                        break;
                    case PUBLISH_DATE:
                        metadata.setPublishDate(ba.readString());
                        break;
                    case COPYRIGHT:
                        metadata.setCopyright(ba.readString());
                        break;
                    case OWNER_EMAIL:
                        metadata.setRegisteredTo(ba.readString());
                        break;
                    case COMIC_AD_HORIZONTAL:
                        LOG.debug("COMIC_AD_HORIZONTAL: 0x%02x", ba.getFilePointer());
                        LOG.debug("AD [%s]", ba.readString());  // TODO: handle ad
                        // debug("Idx [%d]", ba.readShort());
                        break;
                    case COMIC_AD_VERTICAL:
                        LOG.debug("COMIC_AD_VERTICAL: 0x%02x", ba.getFilePointer());
                        LOG.debug("AD [%s]", ba.readString()); // TODO: handle ad
                        // debug("Idx [%d]", ba.readShort());
                        break;
                    // Icon
                    case ICON:
                        // DEPRECATED getContent().setOffsetIcon(ba.readFileOffsets());
                        break;
                    // Thumb
                    case THUMB:
                        // DEPRECATED getContent().setOffsetThumbslider(ba.readFileOffsets());
                        break;
                    // Comic Registration
                    case OWNER_EMAIL_ENCRYPTED:
                        //encryptedOwner = ba.readEncryptedString();
                        ba.readEncryptedString();
                        break;
                    // Generation Info
                    case GENERATED_DATE:
                        metadata.setGeneratedDate(ba.readDate());
                        break;
                    case GENERATED_BY:
                        metadata.setGeneratedBy(ba.readString());
                        break;
                    // Panels
                    case CONTENT_COUNT:
                        int count = ba.readShort() + 1;
                        LOG.debug("CONTENT COUNT: %d", count);
                        // panelOffsets = new PanelOffsets(count);
                        break;
                    case PANEL_COUNT:
                        int countH = ba.readShort();
                        LOG.debug("PANEL COUNT: %d", countH);
                        // panelOffsets = new PanelOffsets(count);
                        break;
                    case VERTICAL_COUNT:
                        int countV = ba.readShort();
                        LOG.debug("VERTICAL COUNT: %d", countV);
                        // panelOffsets = new PanelOffsets(count);
                        break;
                    case IMAGE_PANEL:
                        getContent().addPanel(ba.readImageOffset(), ComicContent.Type.PANEL);
                        break;
                    case IMAGE_VERTICAL:
                        getContent().addPanel(ba.readImageOffset(), ComicContent.Type.PAGE);
                        break;
                    case BUNDLE_CHECKSUM:
                        /* byte checksum[] = */
                        ba.readChecksum();

                        break;
                    case MANGA_ORIENTATION:
                        metadata.setMangaOrientation(ba.readShort());
                        break;
                    case HORIZONTAL_FADES:
                        short tempShort;
                        tempShort = ba.readShort();
                        if(tempShort ==1){
                            metadata.setWaidFormat(true);
                        }else{
                            metadata.setWaidFormat(false);
                        }
                        break;
                    case RESERVED_SHORT_2:
                    case RESERVED_SHORT_3:
                    case RESERVED_SHORT_4:
                    case RESERVED_SHORT_5:
                    case RESERVED_SHORT_6:
                    case RESERVED_SHORT_7:
                    case RESERVED_SHORT_8:
                    case RESERVED_SHORT_9:
                    case RESERVED_SHORT_10:
                        ba.readShort();
                        break;
                    case RESERVED_STRING_1:
                    case RESERVED_STRING_2:
                    case RESERVED_STRING_3:
                    case RESERVED_STRING_4:
                    case RESERVED_STRING_5:
                    case RESERVED_STRING_6:
                    case RESERVED_STRING_7:
                    case RESERVED_STRING_8:
                    case RESERVED_STRING_9:
                    case RESERVED_STRING_10:
                        ba.readString();
                        break;
                    default:
                        throw new BundleReadException(String.format("Unreadable type: %d.%d%n", type, ba
                                .getFilePointer()));
                }
                type = ba.readShort();
            }

        } catch (EOFException e) {
            // at end of bundle.
        } catch (GeneralSecurityException e) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Internal security error", e);
        } catch (IOException e) {
            metadata.setStatus(ComicMetadata.STATUS_BAD);
            throw new BundleReadException("Unable to open downloaded comic: " + e.getMessage(), e);
        }

        metadata.setStatus(ComicMetadata.STATUS_OK);
        LOG.info("Parsing complete");
        
        /*
        if (encryptedOwner == null) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Comic Not Registered To You");
        }
        

        if (encryptedOwner.equals(metadata.getRegisteredTo())) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Illegal Comic Access");
        }
        */

        if (TextUtils.isEmpty(ownership.getEmailAddress())) {
            metadata.setStatus(ComicMetadata.STATUS_LIB_FAILURE);
            // Throw an exception, but be vague.
            throw new BundleSecurityException("Ownership not initialized (2).");
        }

        if (TextUtils.isEmpty(ownership.getUniqueId())) {
            metadata.setStatus(ComicMetadata.STATUS_LIB_FAILURE);
            // Throw an exception, but be vague.
            throw new BundleSecurityException("Ownership not initialized (3).");
        }

        if (metadata.getRegisteredTo() == null || metadata.getRegisteredTo().equals("")) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Illegal Comic Access");
        }

        if (!metadata.getRegisteredTo().equals(ownership.getEmailAddress())) {
            metadata.setStatus(ComicMetadata.STATUS_NOTYOURS);
            throw new BundleSecurityException("Illegal Comic Access");
        }
    }
}
