package com.iversecomics.client.util;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public final class DebugUtil {
    private static final String excludedPrefixes[] = {
            "java.", "android.", "dalvik.", "junit.", "com.android."
                                                   };

    private static boolean isExcludedClassName(StackTraceElement ste) {
        for (String excluded : excludedPrefixes) {
            if (ste.getClassName().startsWith(excluded)) {
                return true;
            }
        }
        return false;
    }

    public static void showCallStack() {
        RuntimeException e = new RuntimeException();
        e.fillInStackTrace();
        for (StackTraceElement ste : e.getStackTrace()) {
            if (isExcludedClassName(ste)) {
                // excluded, skip it
                continue;
            }
            Log.d("STACK",
                    String.format("   %s.%s(%s:%d)", ste.getClassName(), ste.getMethodName(), ste.getFileName(),
                            ste.getLineNumber()));
        }
    }

    public static CharSequence toString(Intent intent) {
        StringBuilder b = new StringBuilder();
        b.append("Intent[action=");
        b.append(intent.getAction());
        if (intent.getCategories() != null) {
            for (String category : intent.getCategories()) {
                b.append("\n  ,category=").append(category);
            }
        }
        if (intent.getData() != null) {
            b.append("\n  ,data(uri)=").append(intent.getData().toString());
        }
        if (intent.getComponent() != null) {
            ComponentName comp = intent.getComponent();
            b.append("\n  ,component=").append(comp.getClassName()).append("/").append(comp.getShortClassName());
        }
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Object obj;
            for (String key : bundle.keySet()) {
                b.append("\n  ,bundle[").append(key).append("]=");
                obj = bundle.get(key);
                b.append("(").append(obj.getClass().getSimpleName()).append(")").append(obj.toString());
            }
        }
        b.append("]");
        return b;
    }
}
