package com.babylon.utils;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

public class MinMaxInputFilter implements InputFilter {

    public static final String TAG = MinMaxInputFilter.class.getSimpleName();

    private float min, max;

    public MinMaxInputFilter(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        final String curText = dest.toString() + source.toString();

        if (!TextUtils.isEmpty(curText)) {
            try {
                float input = Float.parseFloat(curText);
                if (isInRange(min, max, input)) {
                    return null;
                }
            } catch (NumberFormatException e) {
                L.e(TAG, e.getMessage(), e);
            }
        }

        return "";
    }

    private boolean isInRange(float a, float b, float c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
