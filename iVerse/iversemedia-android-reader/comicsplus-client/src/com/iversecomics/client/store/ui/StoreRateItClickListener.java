package com.iversecomics.client.store.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.R;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.tasks.SubmitProductRatingTask;
import com.iversecomics.client.util.UIUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class StoreRateItClickListener implements View.OnClickListener {
	
	final Activity 	mActivity;
	final Comic					comic;
	private static final Logger LOG = LoggerFactory.getLogger(StoreRateItClickListener.class);
	
	StoreRateItClickListener(final Activity activity, final Comic comic)
	{
		this.mActivity = activity;
		this.comic = comic;
	}
	
	 @Override
     public void onClick(View v) {
		 
		 final MyComicsModel model = new MyComicsModel(mActivity);
		 final IverseApplication app = IverseApplication.getApplication();
		 final MyComic myComic = model.getComicByBundleName(comic.getComicBundleName());
		 final String deviceId = app.getOwnership().getUniqueId();
		
		 final LayoutInflater inflater = (LayoutInflater)mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 final View layout = inflater.inflate(R.layout.dialog_rate, null); 
		 
		 UIUtil.setRating(layout, R.id.ratingBar, myComic.getMyRating().floatValue());
		 
		 final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		 builder.setView(layout); 
		 builder.setTitle(mActivity.getResources().getString(R.string.rate_this_comic));
		 builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() 
		 {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
						final RatingBar rB = (RatingBar)layout.findViewById(R.id.ratingBar); 
						myComic.setMyRating((double) rB.getRating());
						model.update(myComic);
						final int ratingValue = (int) rB.getRating();
						
						//Create and submit the product rating task
						SubmitProductRatingTask task = new SubmitProductRatingTask(app.getComicStore());
						task.setRating(ratingValue);
						task.setDeviceId(deviceId);
						task.setComicBundleName(myComic.getComicId());
						app.getTaskPool().submit(task);
						
					}
				});
         builder.setNegativeButton("Cancel", null);
         builder.create().show();
     }
}
