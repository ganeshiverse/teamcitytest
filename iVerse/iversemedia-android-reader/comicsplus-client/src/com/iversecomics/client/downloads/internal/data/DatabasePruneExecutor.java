package com.iversecomics.client.downloads.internal.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.iversecomics.client.util.DBUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DatabasePruneExecutor extends AbstractDatabaseExecutor {
    private static final Logger LOG = LoggerFactory.getLogger(DatabasePruneExecutor.class);
    private int MAX_DOWNLOAD_FAIL = 3;

    public DatabasePruneExecutor(Uri contentUri, ContentProviderClient client) {
        super(contentUri, client);
    }

    @Override
    public void execute() {
        List<Long> inactiveIds = queryInactiveIds();
        if (inactiveIds.size() > 0) {
            LOG.debug("Found %d inactive ids", inactiveIds.size());
        }

        int count = 0;

        for (long id : inactiveIds) {
            count += purgeInactive(ContentUris.withAppendedId(contentUri, id));
        }

        if (count > 0) {
            LOG.debug("Purged %d inactive ids", count);
        }
    }

    private int purgeInactive(Uri contentUri) {
        try {
            return client.delete(contentUri, null, null);
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to purge inactive id: %s", contentUri);
            return 0;
        }
    }

    private List<Long> queryInactiveIds() {
        List<Long> inactiveIds = new ArrayList<Long>();

        Cursor c = null;
        try {
            c = client.query(contentUri, null, null, null, null);
            int colId = c.getColumnIndexOrThrow(DownloadsTable._ID);
            int colStatus = c.getColumnIndexOrThrow(DownloadsTable.STATUS);
            int colNumFail = c.getColumnIndexOrThrow(DownloadsTable.NUM_FAILED);
            if (c.moveToFirst()) {
                do {
                    int status = c.getInt(colStatus);
                    int numFail = c.getInt(colNumFail);

                    if (status == DownloadStatus.PENDING.getCode() && (numFail < MAX_DOWNLOAD_FAIL)) {
                        continue; // skip pending
                    }
                    if (status == DownloadStatus.ACTIVE.getCode()) {
                        continue; // skip active
                    }
                    
                    inactiveIds.add(c.getLong(colId));
                } while (c.moveToNext());
            }
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to query for inactive ids");
        } finally {
            DBUtil.close(c);
        }

        return inactiveIds;
    }
}
