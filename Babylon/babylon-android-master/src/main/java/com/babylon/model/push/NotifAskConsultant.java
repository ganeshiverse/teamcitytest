package com.babylon.model.push;

public class NotifAskConsultant extends Push {

    private String questionId;

    public String getQuestionId() {
        return questionId;
    }

    public void getQuestionId(String appointment) {
        this.questionId = appointment;
    }
}
