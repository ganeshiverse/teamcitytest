package com.babylon.activity.shop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.android.volley.*;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.babylon.App;
import com.babylon.activity.ShopProfileActivity;
import com.babylon.activity.payments.PaymentTransactionActivity;
import com.babylon.api.request.base.Urls;
import com.babylon.fragment.payments.OrderDetailsListFragment;
import com.babylon.fragment.shop.*;
import com.babylon.model.Kit.*;
import com.babylon.model.Patient;
import com.babylon.model.payments.OrderDetail;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.*;
import com.babylonpartners.babylon.HomePageActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.*;

public class ShopCategoriesActivity extends PaymentTransactionActivity implements ShopCategoriesListFragment
        .KitCategoriesFragmentListener,HealthCheckKitListFragment.HealthCheckKitFragmentListener ,
        OrderDetailsListFragment.OrderAddressFragmentListener,AddressListFragment.AddressFragmentListener,
        ConfirmAddressFragment.ConfirmAddressFragmentListener,ConfirmAddressFragment.DataProtocol,
        PatientAddressListFragment.OnAddClickListener,ShopOrderConfirmFragment.ShopOrderConfirmFragmentListener{

    public static final String TAG = ShopCategoriesActivity.class.getSimpleName();
    public List<OrderProducts> mOrderProductList=new ArrayList<OrderProducts>();
    protected List<OrderDetail> orderDetails;
    protected String orderTotal;
    protected  OrderResponse response_order;
    protected Address address_selected;
    private int OrderId;
    private static final int ON_SUCCESS_SAVING_PROFILE_RESULT = 2;
    boolean PayClicked=false;
    private ProgressDialog mProgressDialog;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, ShopCategoriesActivity.class);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final boolean isProfileValid = ShopProfileActivity.isProfileValid();

        if (!isProfileValid) {
            finish();
            try {
                Intent i = new Intent(this, ShopProfileActivity.class);
                startActivityForResult(i, ON_SUCCESS_SAVING_PROFILE_RESULT);
            }
            catch (Exception ex){
                Log.d(TAG,ex.toString());
            }
        }
        else {
            startFragment(new ShopCategoriesListFragment(), false, ShopCategoriesListFragment.TAG);
        }
    }

    @Override
    public int getColorTheme() {
        return PINK_THEME;
    }


    @Override
    public void onCategoriesListItemClicked(Category category) {
        HealthCheckKitListFragment detailFragment = HealthCheckKitListFragment.newInstance(category.getProducts
                (), category.getName());
        startFragment(detailFragment, HealthCheckKitListFragment.TAG);

    }
    @Override
    public void onCategoriesHelpClicked() {

        ShopCategoryHelpFragment helpFragment = ShopCategoryHelpFragment.newInstance();
        startFragment(helpFragment, ShopCategoryHelpFragment.TAG);

    }

    @Override
    public void onMoreDetailsClicked(String moreDetail) {

        ShopMoreDetailFragment moreDetailFragment = ShopMoreDetailFragment.newInstance(moreDetail);
        startFragment(moreDetailFragment, ShopMoreDetailFragment.TAG);

    }

    @Override
    public void onOrderClicked() {

        Order order=  addDetailsToOrder();
        JSONObject json_params=getParams(order);
        String mUri= CommonHelper.getUrl(Urls.ORDERS);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, mUri, json_params,
                new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        startOrderDetailsFragment(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }

    }

    private Order addDetailsToOrder() {
        String prod= mOrderProductList.toString();
        Order order=new Order();
        List<ProductList> prod_list=new ArrayList<ProductList>();

        for(OrderProducts order_prod :mOrderProductList )
        {
            ProductList prodlistItem=new ProductList();
            prodlistItem.setId(order_prod.getId());
            prod_list.add(prodlistItem);
        }
        order.setProducts(prod_list);
        return order;
    }

    private void startOrderDetailsFragment(JSONObject response) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        String jsonString = response.toString();

        response_order = gson.fromJson(jsonString, OrderResponse.class);
        orderDetails = new LinkedList<>();
        orderTotal=response_order.getTotal();
        OrderId=response_order.getId();
        for(LineItem item:response_order.getLineItems()) {

            final OrderDetail orderDetails = new OrderDetail(item.getName(),
                    item.getPrice());
            ShopCategoriesActivity.this.orderDetails.add(orderDetails);

        }
        startFragment(new OrderDetailsListFragment(), OrderDetailsListFragment.TAG);

    }

    private JSONObject getParams(Order order) {
        String RegObject = null;

        Map<String, Object> objMap = new HashMap<String, Object>();
        objMap.put("order", GsonWrapper.getGson().toJsonTree(order).toString());

        StringBuffer buf = new StringBuffer().append('{');
        Set<String> keys = objMap.keySet();
        Iterator<String> iterator = keys.iterator();
        Object key;
        Object value;

        boolean first = true;
        while (iterator.hasNext())
        {
            if (first)
                first = false;
            else
                buf.append(',');
            key = iterator.next();
            value = objMap.get(key);
            buf.append( key + ":" + value );
        }
        buf.append('}');


        RegObject=buf.toString();


        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(RegObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    @Override
    public void onAddressClicked() {

      if(!TextUtils.isEmpty(PreferencesManager.getInstance().getPatientAddress())){

            startFragment(new PatientAddressListFragment(),PatientAddressListFragment.TAG);
        }
        else {
          getAddressOfPatient();
           // OpenAddressListFragment();
        }

    }


    private void getAddressOfPatient() {

        String mUri=  String.format(CommonHelper.getUrl(Urls.GET_USER_ADDRESS), SyncUtils.getWsse().getId());

        JsonArrayRequest mArrayReq = new JsonArrayRequest(mUri,	new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String jsonString = response.toString();

                Type collectionType = new TypeToken<List<Address>>() { }.getType();
                List<Address> addressResponseList = gson.fromJson(jsonString, collectionType);
                if(addressResponseList.size()>0){
                    startFragment(new PatientAddressListFragment(),PatientAddressListFragment.TAG);
                }
                else{
                    OpenAddressListFragment();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,error.toString());
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                return headers;
            }

        };

        if (App.getInstance().isConnection()){
            mArrayReq.setRetryPolicy(new DefaultRetryPolicy(18000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(mArrayReq);
        }



    }


    @Override
    public void onPatieAddressSelectedForList(Address address) {

        address_selected=address;

        startFragment(new OrderDetailsListFragment(),OrderDetailsListFragment.TAG);

    }



    @Override
    public  void onConfirmAdressClicked(Address address){
        startFragment(new OrderDetailsListFragment(),OrderDetailsListFragment.TAG);
    }

    public void onPayClick()
    {
        //Mix Panel event
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(ShopCategoriesActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.PAYMENT_REQUESTED);

        /*if(!PayClicked) {
            PayClicked = true;*/
        mProgressDialog = UIUtils.getLoadingDialog(this);
        mProgressDialog.show();
            setProgressBarVisible(true);
            if (PreferencesManager.getInstance().getPatientAddressId() != 0) {
                patchOrder();
            } else {
                payForOrder();
            }
        //}


    }

    private void patchOrder() {

        Order order=  addDetailsToOrder();


        String mUri= CommonHelper.getUrl(Urls.ORDERS).concat("/"+OrderId);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, mUri, getJSONObject(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        setProgressBarVisible(true);
                        payForOrder();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG,"error at  patchOrder");

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }

    }


    public JSONObject getJSONObject()
    {

        JSONObject apiJsonObject = new JSONObject();
        JSONObject passwordObj = new JSONObject();
        try  {
            passwordObj.put("address_id",String.valueOf(PreferencesManager.getInstance().getPatientAddressId()));
            apiJsonObject.put("order", passwordObj);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return apiJsonObject;
    }

    private void payForOrder() {


        HashMap<String, Integer> params = new HashMap<String, Integer>();
        params.put("credit_card_id", getPaymentCard().getId());


        String uri= Urls.getRuby(String.format(Urls.ORDERS_PAY, response_order.getId()));
        JsonObjectRequest mReq = new JsonObjectRequest(com.android.volley.Request.Method.POST, uri, new JSONObject
                (params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        //Mix Panel event
                        JSONObject props = new JSONObject();
                        MixPanelHelper helper = new MixPanelHelper(ShopCategoriesActivity.this,props,null);
                        helper.sendEvent(MixPanelEventConstants.PAYMENT_SUCCESS);

                        mOrderProductList=new ArrayList<OrderProducts>();
                        setProgressBarVisible(false);
                        mProgressDialog.hide();
                        ShopOrderConfirmFragment confirmFragment = ShopOrderConfirmFragment.newInstance(orderTotal);
                        startFragment(confirmFragment, false, ShopOrderConfirmFragment.TAG);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                //Mix Panel event
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(ShopCategoriesActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.PAYMENT_FAIL);


                Log.d(TAG, error.toString());
                Log.d(TAG,"error at  payOrder");
            }
        }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            mReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(mReq);
        }


    }

    @Override
    public void onKitListItemClicked(Product product) {

    }

    @Override
    public void onAdressItemClicked(Address address) {

        address_selected=address;
        ConfirmAddressFragment confirmFragment = ConfirmAddressFragment.newInstance();
        startFragment(confirmFragment,false,ConfirmAddressFragment.TAG);
    }

    @Override
    public void onAddClick() {

        OpenAddressListFragment();
    }

    private void OpenAddressListFragment() {
        AddressListFragment addressFragment = AddressListFragment.newInstance();
        startFragment(addressFragment, AddressListFragment.TAG);
    }

    @Override
    public  void onDoneClicked(){

        Intent intent = new Intent(this, HomePageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public String getOrderDetailsSubTitle() {
        return null;
    }
    @Override
    public String getOrderTotal() {
        return orderTotal;
    }

    @Override
    public Patient getPatient() {
        return  patient;
    }


    @Override
    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }


    @Override
    public Address getAddressSelected() {
        return address_selected;
    }

    @Override
    public boolean IsSpecialist() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ON_SUCCESS_SAVING_PROFILE_RESULT:
                    startFragment(new ShopCategoriesListFragment(), false, ShopCategoriesListFragment.TAG);
                    break;
                default:
                    break;
            }
        }
    }

}
