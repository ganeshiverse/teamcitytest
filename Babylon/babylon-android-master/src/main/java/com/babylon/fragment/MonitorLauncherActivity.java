package com.babylon.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.MonitorComingSoonActivity;
import com.babylon.activity.MonitorMeActivity;
import com.babylon.activity.MonitorMeProfileActivity;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.model.Patient;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.RegionSettings;

public class MonitorLauncherActivity extends FragmentActivity {

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, MonitorLauncherActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_monitor_launcher);

        final boolean isMonitorMeEnabled = new RegionSettings().isMonitorMeEnabled();

        if (!isMonitorMeEnabled) {
            MonitorComingSoonActivity.startActivity(this);
            finish();

        }
        else
        {
            if(App.getInstance().isConnection())
            {
                new PatientGet().execute(MonitorLauncherActivity.this, patientRequestListener);
            }
            else
            {
                AlertDialogHelper.getInstance().showMessage(this, getString(R.string.offline_error));
            }

        }
    }



    private final RequestListener<Patient> patientRequestListener = new RequestListener<Patient>() {
        @Override
        public void onRequestComplete(Response<Patient> response) {
            if (!isFinishing())
            {
                App.getInstance().setPatient(response.getData());

                final boolean isProfileValid = MonitorMeProfileActivity.isProfileValid();
                final boolean isFirstMonitorMeLaunch = PreferencesManager.getInstance()
                        .isFirstMonitorMeLaunch();

                if (!isFirstMonitorMeLaunch && !isProfileValid) {
                    MonitorMeProfileActivity.startActivity(MonitorLauncherActivity.this);
                } else {
                    MonitorMeActivity.startActivity(MonitorLauncherActivity.this);
                }

                finish();
            }
        }
    };

}
