package com.iversecomics.ui.slidelist;

import android.support.v4.app.Fragment;
import android.widget.SeekBar;

import com.iversecomics.ui.widget.ComicReaderSeekBar;
import com.jess.ui.TwoWayGridView;

import static com.iversecomics.ui.slidelist.SlideableListConstants.MAX_NUM_ROWS;
import static com.iversecomics.ui.slidelist.SlideableListConstants.MIN_NUM_ROWS;
import static com.iversecomics.ui.slidelist.SlideableListConstants.NUM_PROGRESS_STEPS;
import static com.iversecomics.ui.slidelist.SlideableListConstants.START_NUM_ROWS;

/**
 */
public abstract class SlideableListFragment extends Fragment implements ComicReaderSeekBar.OnSeekBarChangeListener {

    protected TwoWayGridView mGrid;
    ;
    protected ComicReaderSeekBar mSeekBar;
    protected int mNumRows = START_NUM_ROWS;
    protected SlideableComicListAdapter adapter;

    protected void initSeekBar(ComicReaderSeekBar mSeekBar) {
        this.mSeekBar = mSeekBar;
        mSeekBar.setMax(NUM_PROGRESS_STEPS);
        mSeekBar.setProgress(NUM_PROGRESS_STEPS);
        mSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int numRows = (int) (((MAX_NUM_ROWS - MIN_NUM_ROWS) * (1 - (double) progress / NUM_PROGRESS_STEPS)) + MIN_NUM_ROWS);
        if (numRows != mNumRows) {
            mNumRows = numRows;
            if(mGrid !=null)
                mGrid.setNumRows(numRows);
            if(adapter !=null)
                adapter.notifyDataSetInvalidated();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
