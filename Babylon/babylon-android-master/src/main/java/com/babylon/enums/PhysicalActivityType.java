package com.babylon.enums;

import com.babylon.App;
import com.babylon.R;

public enum PhysicalActivityType {
    CYCLING_EASY(0.1f),
    CYCLING_MEDIUM(0.15f),
    CYCLING_HARD(0.21f),
    RUNNING_EASY(0.12f),
    RUNNING_MEDIUM(0.178f),
    RUNNING_HARD(0.24f),
    WALKING_EASY(0.06f),
    WALKING_MEDIUM(0.07f),
    WALKING_HARD(0.08f);

    static {
        String[] activityNames = App.getInstance().getResources().getStringArray(R.array.calories_burnt_activities);
        String[] activityIntensities = App.getInstance().getResources().getStringArray(R.array.calories_burnt_activity_intensity_for_server);
        if (activityNames != null && activityIntensities != null) {
            int ordinal = 0;
            int intensityOrdinal = 0;
            for (String activityName : activityNames) {
                for (String activityIntensity : activityIntensities) {
                    values()[ordinal].name = activityName;
                    values()[ordinal].intensity = activityIntensity;
                    values()[ordinal++].intensityValue = intensityOrdinal + 1;

                    intensityOrdinal++;
                }
            }
        }
    }

    private PhysicalActivityType(float energy) {
        this.energy = energy;
    }

    private float energy;
    private String name;
    private String intensity;
    private Integer intensityValue;

    public float getEnergy() {
        return energy;
    }

    public String getName() {
        return name;
    }

    public Integer getIntensityValue() {
        return intensityValue;
    }

    public int calculateBurntKiloCalories(float weight, int minutes) {
        return (int) (getEnergy() * weight * minutes);
    }

    public static PhysicalActivityType getActivityType(String activityName, String activityIntensity) {
        for (PhysicalActivityType activityType : values()) {
            if (activityType.name.equals(activityName)
                    && activityType.intensity.equals(activityIntensity)) {
                return activityType;
            }
        }
        return null;
    }
}
