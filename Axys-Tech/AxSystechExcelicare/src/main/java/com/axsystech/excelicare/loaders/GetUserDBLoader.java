package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.exceptions.NoNetworkException;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 23.07.14
 * Time: 12:04
 *
 * @author SomeswarReddy
 */
public class GetUserDBLoader extends DataAsyncTaskLibLoader<List<User>> {

    private final String mEmail;
    private final String mPassword;

    public GetUserDBLoader(final Context context, final String email, final String password) {
        super(context, true);
        mEmail = email;
        mPassword = password;
    }

    @Override
    protected List<User> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final Dao<User, Long> userDao = helper.getDao(User.class);
            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<User>>() {
                @Override
                public List<User> call() throws Exception {
                    return userDao.queryBuilder().where().eq(User.EMAIL, mEmail)/*.and().eq(User.PASSWORD, mPassword)*/.query();
                }
            });
        } catch (final Exception e) {
            throw new NoNetworkException();
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
