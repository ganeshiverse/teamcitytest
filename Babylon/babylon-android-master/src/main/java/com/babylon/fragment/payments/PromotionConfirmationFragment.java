package com.babylon.fragment.payments;


import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.api.request.base.Urls;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.payments.Promotion;
import com.babylon.model.payments.Redemption;
import com.babylon.model.payments.Subscription;
import com.babylon.view.TopBarInterface;
import com.squareup.picasso.Picasso;

public class PromotionConfirmationFragment extends BaseFragment
        implements View.OnClickListener {

    public static final String TAG = PromotionConfirmationFragment.class.getSimpleName();

    public interface DataProtocol {
        Redemption getRedemption();
    }

    public interface OnPromotionConfirmationListener {
        void onPromotionDone();
    }

    private OnPromotionConfirmationListener promotionListener;
    private DataProtocol dataProtocol;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            promotionListener = (OnPromotionConfirmationListener) activity;
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPromotionConfirmationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        promotionListener = null;
        dataProtocol = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_promotion_confirmation, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView planNameTextView = (TextView) view.findViewById(R.id.plan_name_text_view);
        TextView planCountTextView = (TextView) view.findViewById(R.id.plan_count_text_view);
        TextView planTypeTextView = (TextView) view.findViewById(R.id.plan_type_text_view);
        //TextView planStateTextView = (TextView) view.findViewById(R.id.plan_state_text_view);
        ImageView logoImageView = (ImageView) view.findViewById(R.id.promo_logo_image_view);
        Button mContinueButton = (Button) view.findViewById(R.id.btnAddPromotionCode);
        mContinueButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                promotionListener.onPromotionDone();
            }
        });

        if (dataProtocol.getRedemption() != null) {
            Redemption redemption = dataProtocol.getRedemption();
            if (redemption.getSubscriptions() != null &&
                    !redemption.getSubscriptions().isEmpty()) {
                Subscription subscription = redemption.getSubscriptions().get(0);
                if (!TextUtils.isEmpty(subscription.getState())) {
                    //planStateTextView.setText(getString(R.string.promotion_state_active));
                    /*planStateTextView.setText(getString(R.string.promotion_state_plan,
                            subscription.getState()));*/
                }
            }
            if (redemption.getPromotion() != null) {
                Promotion promotion = redemption.getPromotion();
                String planName = promotion.getName();
                String planCount = "";
                if (promotion.getTypeAmount() != null) {
                    planCount = promotion.getTypeAmount().toString();
                }
                String planType = promotion.getDescription();

                if (!TextUtils.isEmpty(planName)) {
                    planNameTextView.setText(planName);
                } else {
                    planNameTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(planCount)) {
                    planCountTextView.setText(planCount);
                } else {
                    planCountTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(planType)) {
                    planTypeTextView.setText(planType);
                } else {
                    planTypeTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(promotion.getLogo())) {
                    String imageUrl = Urls.getRuby(promotion.getLogo());
                    Picasso.with(getActivity()).load(imageUrl).fit().centerInside().into(logoImageView);
                } else {
                    logoImageView.setVisibility(View.GONE);
                }
            }
        }

        setTitle(R.string.welcome);
       // setRightButton(this, R.string.done);
        setLeftButton(null,0);
        //setRightButton(this, R.string.done);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    @Override
    public void onClick(View view) {
        if (promotionListener != null) {
            switch (view.getId()) {
                case R.id.topbar_right_text_button:
                    promotionListener.onPromotionDone();
                    break;
                default:
                    break;
            }
        }
    }

}
