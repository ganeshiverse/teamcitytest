package com.babylon.activity.payments;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.AddingCreditCardPost;
import com.babylon.api.request.payments.CreditCardsListGet;
import com.babylon.fragment.payments.AddingPaymentCardFragment;
import com.babylon.fragment.payments.PaymentCardsListFragment;
import com.babylon.model.payments.NewPaymentCard;
import com.babylon.model.payments.PaymentCard;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.HandlerLoaderListener;

public class
        PaymentCardsListActivity extends BaseActivity implements PaymentCardsListFragment
        .OnPaymentCardInteractionListener, AddingPaymentCardFragment.OnCreatingPaymentCardClickListener {

    public static final String PAYMENT_FRAGMENT_TAG = "PAYMENT_FRAGMENT_TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards_list);
        new CreditCardsListGet().execute(this, cardsListRequestListener);
       /* PaymentCardsListFragment detailFragment = PaymentCardsListFragment.newInstance(PAYMENT_FRAGMENT_TAG );
        startFragment(detailFragment, false, PaymentCardsListFragment.TAG);*/


    }

    private final RequestListener<PaymentCard[]> cardsListRequestListener = new
            RequestListenerErrorProcessed<PaymentCard[]>() {
                @Override
                public void onRequestComplete(Response<PaymentCard[]> response, String errorMessage) {
                    if (!PaymentCardsListActivity.this.isFinishing()) {
                        if (!TextUtils.isEmpty(errorMessage)) {
                            Toast.makeText(PaymentCardsListActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        } else {
                            processPaymentCardsResponse(response);
                        }
                    }
                }


                private void processPaymentCardsResponse(Response<PaymentCard[]> response) {
                      PaymentCardsListFragment detailFragment = PaymentCardsListFragment.newInstance
                              (PAYMENT_FRAGMENT_TAG );
                     startFragment(detailFragment, false, PaymentCardsListFragment.TAG);
                   // if (response.getData().length >= 1) {
                       /* Fragment fragment = getSupportFragmentManager().findFragmentByTag(PaymentCardsListFragment.TAG);
                        if (fragment == null) {
                            fragment = new PaymentCardsListFragment();

                        } else {
                            ((BaseFragment) fragment).setProgressBarVisible(false);
                        }

                        startFragment(fragment, PaymentCardsListFragment.TAG);*/
                    /*} else {
                        startFragment(new AddingPaymentCardFragment(), AddingPaymentCardFragment.TAG);
                    }*/
                }
            };

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    @Override
    public void onPaymentCardSelect(PaymentCard paymentCard) {
        //empty method
    }

    @Override
    public void onPaymentCardDelete(int id) {
        //empty method
    }

    @Override
    public void onNewPaymentCardClick() {
        startFragment(new AddingPaymentCardFragment(), AddingPaymentCardFragment.TAG);
    }

    @Override
    public void onCreatingPaymentCardClick(NewPaymentCard paymentCard) {
        setProgressBarVisible(true);

        new AddingCreditCardPost().withEntry(paymentCard).execute(PaymentCardsListActivity.this,
                onCreatingPaymentCardRequestListener);
    }

    /*private final RequestListener<PaymentCard[]> cardsListRequestListener = new
            RequestListenerErrorProcessed<PaymentCard[]>() {
                @Override
                public void onRequestComplete(Response<PaymentCard[]> response, String errorMessage) {
                    if (!PaymentCardsListActivity.this.isFinishing()) {
                        setProgressBarVisible(false);

                        if (!TextUtils.isEmpty(errorMessage)) {
                            Toast.makeText(PaymentCardsListActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };*/

    private final RequestListener<PaymentCard> onCreatingPaymentCardRequestListener = new
            RequestListenerErrorProcessed<PaymentCard>() {
                @Override
                public void onRequestComplete(Response<PaymentCard> response, String errorMessage) {
                    if (!PaymentCardsListActivity.this.isFinishing()) {
                        setProgressBarVisible(false);
                        if(response.isSuccess()) {
                            new HandlerLoaderListener(PaymentCardsListActivity.this).sendEmptyMessage
                                    (HandlerLoaderListener.BACK_PRESS_HANDLER_CODE);
                        } else {
                            AlertDialogHelper.getInstance().showMessage(PaymentCardsListActivity.this, response.getUserResponseError());
                        }
                    }
                }
            };

}
