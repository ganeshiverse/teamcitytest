package com.iversecomics.client.store.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.ui.widget.ComicStoreImageView;

public abstract class AbstractComicCursorAdapter extends CursorAdapter {
    private int           layoutId;
    private BitmapManager bitmapManager;

    public AbstractComicCursorAdapter(Context context, Cursor c, int layoutId) {
        super(context, c);
        this.layoutId = layoutId;
    }

    public BitmapManager getBitmapManager() {
        return bitmapManager;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(layoutId, parent, false);
    }

    public void setBitmapManager(BitmapManager bitmapManager) {
        this.bitmapManager = bitmapManager;
    }

    protected void setImage(View view, int imageViewId, Uri uriImage) {
        ComicStoreImageView img = (ComicStoreImageView) view.findViewById(imageViewId);
        if (img != null) {
            if (uriImage == null) {
                img.setVisibility(View.INVISIBLE);
                return; // Nothing to show
            }
            img.setVisibility(View.VISIBLE);
            img.setImageViaUri(uriImage, bitmapManager);
        }
    }

    protected void setText(View view, int textViewId, String value) {
        TextView text = (TextView) view.findViewById(textViewId);
        if (text != null) {
            text.setText(value);
        }
    }

    protected void setTextRes(View view, int textViewId, int resId) {
        TextView text = (TextView) view.findViewById(textViewId);
        if (text != null) {
            text.setText(resId);
        }
    }
}
