package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SomeswarReddy on 7/13/2015.
 */
public class GetDashBoardCardsResponse extends BaseResponse implements Serializable {

    @SerializedName("header")
    private HeaderFooter dashboardHeader;

    @SerializedName("id")
    private String id;

    @SerializedName("isWizard")
    private String isWizard;

    @SerializedName("name")
    private String name;

    @SerializedName("list")
    private ArrayList<ListObject> listObject;

    @SerializedName("footer")
    private HeaderFooter dashboardFooter;

    public String getId() {
        return id;
    }

    public String getIsWizard() {
        return isWizard;
    }

    public String getName() {
        return name;
    }

    public ArrayList<ListObject> getListObject() {
        return listObject;
    }

    public HeaderFooter getDashboardFooter() {
        return dashboardFooter;
    }

    public HeaderFooter getDashboardHeader() {
        return dashboardHeader;
    }

    // TODO remove later, just added for Unit Testing
    public void setListObject(ArrayList<ListObject> list) {
        if(this.listObject != null) {
            this.listObject.addAll(list);
        }
    }

    public class ListObject implements Serializable {
        @SerializedName("ContentURL")
        private String contentURL;

        @SerializedName("DeleteURL")
        private String deleteURL;

        @SerializedName("Footer")
        private HeaderFooter footer;

        @SerializedName("Header")
        private HeaderFooter header;

        @SerializedName("Id")
        private String id;

        @SerializedName("Name")
        private String name;

        @SerializedName("SaveURL")
        private String saveURL;

        @SerializedName("ServiceURL")
        private String serviceURL;

        @SerializedName("Type")
        private String type;

        @SerializedName("TypeLU")
        private String typeLU;

        @SerializedName("design")
        private Design design;

        @SerializedName("listPosition")
        private String listPostition;

        @SerializedName("width")
        private String width;

        private String cardContentData;

        public void setCardContentData(String cardContentData) {
            this.cardContentData = cardContentData;
        }

        public String getCardContentData() {
            return cardContentData;
        }

        public String getContentURL() {
            return contentURL;
        }

        public String getDeleteURL() {
            return deleteURL;
        }

        public HeaderFooter getFooter() {
            return footer;
        }

        public HeaderFooter getHeader() {
            return header;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getSaveURL() {
            return saveURL;
        }

        public String getServiceURL() {
            return serviceURL;
        }

        public String getType() {
            return type;
        }

        public String getTypeLU() {
            return typeLU;
        }

        public Design getDesign() {
            return design;
        }

        public String getListPostition() {
            return listPostition;
        }

        public String getWidth() {
            return width;
        }

    }

    public class HeaderFooter implements Serializable {
        @SerializedName("htmlstyle")
        private String htmlStyle;

        @SerializedName("style")
        private Style style;

        @SerializedName("rows")
        private ArrayList<Rows> rows;

        public String getHtmlStyle() {
            return htmlStyle;
        }

        public Style getStyle() {
            return style;
        }

        public ArrayList<Rows> getRows() {
            return rows;
        }
    }

    public class Rows implements Serializable {
        @SerializedName("columns")
        private ArrayList<Columns> columns;

        public ArrayList<Columns> getColumns() {
            return columns;
        }
    }

    public class Columns implements Serializable {
        @SerializedName("colspan")
        private String colspan;

        @SerializedName("htmlstyle")
        private String htmlStyle;

        @SerializedName("rowspan")
        private String rowSpan;

        @SerializedName("style")
        private Style style;

        @SerializedName("control")
        private ArrayList<Control> control;

        public String getColspan() {
            return colspan;
        }

        public String getHtmlStyle() {
            return htmlStyle;
        }

        public String getRowSpan() {
            return rowSpan;
        }

        public Style getStyle() {
            return style;
        }

        public ArrayList<Control> getControl() {
            return control;
        }
    }

    public class Control implements Serializable {
        @SerializedName("action")
        private String action;

        @SerializedName("cid")
        private String cid;

        @SerializedName("databind")
        private String databind;

        @SerializedName("edit")
        private String edit;

        @SerializedName("htmlstyle")
        private String htmlstyle;

        @SerializedName("id")
        private String id;

        @SerializedName("idle")
        private String idle;

        @SerializedName("lookupid")
        private String lookupid;

        @SerializedName("lookuptype")
        private String lookuptype;

        @SerializedName("params")
        private Params params;

        @SerializedName("src")
        private String src;

        @SerializedName("style")
        private Style style;

        @SerializedName("text")
        private String text;

        @SerializedName("textAlignment")
        private String textAlignment;

        @SerializedName("type")
        private String type;

        @SerializedName("typename")
        private String typename;

        @SerializedName("width")
        private String width = "0";

        public String getAction() {
            return action;
        }

        public String getCid() {
            return cid;
        }

        public String getDatabind() {
            return databind;
        }

        public String getEdit() {
            return edit;
        }

        public String getHtmlstyle() {
            return htmlstyle;
        }

        public String getId() {
            return id;
        }

        public String getIdle() {
            return idle;
        }

        public String getLookupid() {
            return lookupid;
        }

        public String getLookuptype() {
            return lookuptype;
        }

        public Params getParams() {
            return params;
        }

        public String getSrc() {
            return src;
        }

        public Style getStyle() {
            return style;
        }

        public String getText() {
            return text;
        }

        public String getTextAlignment() {
            return textAlignment;
        }

        public String getType() {
            return type;
        }

        public String getTypename() {
            return typename;
        }

        public String getWidth() {
            return width;
        }
    }

    public class Params implements Serializable {

        @SerializedName("DashboardId")
        private String dashboardId;

        @SerializedName("ModuleId")
        private String moduleId;

        @SerializedName("RecordId")
        private String recordId;

        @SerializedName("OutputType")
        private String outputType;

        public String getOutputType() {
            return outputType;
        }

        public String getDashboardId() {
            return dashboardId;
        }

        public String getModuleId() {
            return moduleId;
        }

        public String getRecordId() {
            return recordId;
        }
    }

    public class Style implements Serializable {
        @SerializedName("color")
        private String color;

        @SerializedName("fontsize")
        private String fontsize;

        @SerializedName("fontweight")
        private String fontweight;

        @SerializedName("width")
        private String width;

        public String getFontsize() {
            return fontsize;
        }

        public String getFontweight() {
            return fontweight;
        }

        public String getWidth() {
            return width;
        }

        public String getColor() {
            return color;
        }
    }

    public class Design implements Serializable {
        @SerializedName("Header")
        private ArrayList<Header> header;

        @SerializedName("Id")
        private String id;

        @SerializedName("Name")
        private String name;

        public ArrayList<Header> getHeader() {
            return header;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public class Header implements Serializable {
        @SerializedName("Header")
        private String header;

        @SerializedName("Hidden")
        private String hidden;

        @SerializedName("Width")
        private String width;

        @SerializedName("colSpan")
        private String colspan;

        @SerializedName("databind")
        private String databind;

        @SerializedName("listPosition")
        private String listPosition;

        @SerializedName("rowSpan")
        private String rowSpan;

        public String getHeader() {
            return header;
        }

        public String getHidden() {
            return hidden;
        }

        public String getWidth() {
            return width;
        }

        public String getColspan() {
            return colspan;
        }

        public String getDatabind() {
            return databind;
        }

        public String getListPosition() {
            return listPosition;
        }

        public String getRowSpan() {
            return rowSpan;
        }
    }
}
