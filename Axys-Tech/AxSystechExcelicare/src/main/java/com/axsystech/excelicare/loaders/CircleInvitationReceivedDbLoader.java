package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/16/2015.
 */
public class CircleInvitationReceivedDbLoader extends DataAsyncTaskLibLoader<ArrayList<CirclesInvitationReceivedDbModel>> {

    private ArrayList<CirclesInvitationReceivedDbModel> circleInvitationReceivedDbLoaders;
    private String TAG = CircleMemberListDbLoader.class.getSimpleName();

    public CircleInvitationReceivedDbLoader(final Context context, final ArrayList<CirclesInvitationReceivedDbModel> mediaItems) {
        super(context, true);
        this.circleInvitationReceivedDbLoaders = mediaItems;
    }

    @Override
    protected ArrayList<CirclesInvitationReceivedDbModel> performLoad() throws Exception {
        if (circleInvitationReceivedDbLoaders == null)
            throw new SQLException("Unable to cache circle Invitation received list in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (circleInvitationReceivedDbLoaders != null && circleInvitationReceivedDbLoaders.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<CirclesInvitationReceivedDbModel>>() {
                    @Override
                    public ArrayList<CirclesInvitationReceivedDbModel> call() throws Exception {
                        for (final CirclesInvitationReceivedDbModel dbModel : circleInvitationReceivedDbLoaders) {
                            final Dao<CirclesInvitationReceivedDbModel, Long> invitationReceivedListDbModelLongDao = helper.getDao(CirclesInvitationReceivedDbModel.class);

                            invitationReceivedListDbModelLongDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved Invitation received in DB...");
                        }
                        return circleInvitationReceivedDbLoaders;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return circleInvitationReceivedDbLoaders;
    }

}