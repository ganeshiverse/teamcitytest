package com.babylon.api.request.base;

import java.util.List;

public class ResponseIds {

    private List<Integer> ids;

    public List<Integer> getIds() {
        return ids;
    }
}
