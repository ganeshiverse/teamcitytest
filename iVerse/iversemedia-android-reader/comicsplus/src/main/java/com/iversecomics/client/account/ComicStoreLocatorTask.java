package com.iversecomics.client.account;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.json.JSONArray;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicStoreLocatorTask extends ComicStoreTask {

    private static final Logger LOG = LoggerFactory.getLogger(SignUpTask.class);

    public final static String FINDSTORE_RESPONSE = "iverse.action.server.findstore.response";
    String zip;
    double longitude;
    double latitude;

    public ComicStoreLocatorTask(ComicStore comicStore) {
        super(comicStore);
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    @Override
    public void execTask() {
        JSONObject stores = null;
        JSONArray storeList = null;
        try {
            final ServerApi server = comicStore.getServerApi();

            //If zip is set use that
            if (zip != null) {
                stores = server.getStoreLocatorByZip(zip);
            } else {
                stores = server.getStoreLocatorByLocation(latitude, longitude);
            }
            storeList = stores.getJSONArray("Items");
        } catch (Exception e) {
            LOG.warn(e.toString());
        } finally {
            final Intent intent = new Intent(FINDSTORE_RESPONSE);
            if (storeList != null) {
                intent.putExtra("body", storeList.toString());
            }
            LocalBroadcastManager.getInstance(IverseApplication.getApplication().getApplicationContext()).sendBroadcast(intent);
        }
    }
}
