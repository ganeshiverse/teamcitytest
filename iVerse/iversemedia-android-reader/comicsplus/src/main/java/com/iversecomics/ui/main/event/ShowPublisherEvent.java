package com.iversecomics.ui.main.event;

import com.iversecomics.client.store.model.Publisher;

/**
 */
public class ShowPublisherEvent {
    public final Publisher publisher;

    public ShowPublisherEvent(Publisher publisher) {
        this.publisher = publisher;
    }
}
