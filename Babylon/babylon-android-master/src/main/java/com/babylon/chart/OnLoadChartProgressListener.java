package com.babylon.chart;

public interface OnLoadChartProgressListener {
    void onStartLoadChartFromServer();
    void onFinishLoadChartFromServer();
}
