package com.babylon.fragment.check;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.PushBroadcastReceiver;
import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.model.check.export.Outcome;
import com.babylon.model.push.Push;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.GsonWrapper;
import com.babylonpartners.babylon.HomePageActivity;

public class OutcomeFragment extends BaseCheckFragment {

    public static final String OUTCOME_FRAGMENT_TAG = "OUTCOME_FRAGMENT_TAG";

    private static final String KEY_OUTCOME = "KEY_OUTCOME";
    private static final int SIMPLE_MEASURMENT_ID = 3;

    private TextView descriptionText;
    private Button okBtn;
    private View ceMarkText;
    private ImageView ceImageView;
    private Outcome outcome;

    public static OutcomeFragment newInstance(Outcome symptomsHash) {
        OutcomeFragment fragment = new OutcomeFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_OUTCOME, symptomsHash);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            outcome = (Outcome) getArguments().getSerializable(KEY_OUTCOME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_outcome, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity) getActivity()).setTitle(outcome.getName());

        descriptionText = (TextView) view.findViewById(R.id.outcome_description);
        ceImageView = (ImageView)view.findViewById(R.id.ceImageView);
        ceMarkText = view.findViewById(R.id.ceMarkText);
        okBtn = (Button) view.findViewById(R.id.okBtn);

        okBtn.setOnClickListener(okClickListener);
        descriptionText.setText(outcome.getDescription());

        boolean isSimpleMeasurement = outcome.getId() == SIMPLE_MEASURMENT_ID;
        ceMarkText.setVisibility(isSimpleMeasurement ? View.VISIBLE : View.GONE);
        ceImageView.setVisibility(isSimpleMeasurement ? View.VISIBLE : View.GONE);
    }

    private void sendSuggestConsultationBroadcast() {
        Push parseData = new Push();
        parseData.setAction(PushBroadcastReceiver.PushAction.SUGGEST_CONSULTATION_AFTER_ARCHIVING.toString());

        Intent intent = new Intent(getActivity(), HomePageActivity.class);
        intent.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, GsonWrapper.getGson().toJson(parseData));

        startActivity(intent);
    }

    private View.OnClickListener okClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (outcome.getBookAppointment() == 1) {
                //TODO: should be clarified
                if (App.getInstance().isConnection()) {
                    sendSuggestConsultationBroadcast();
                } else {
                    AlertDialogHelper.getInstance().showMessage(getActivity(),
                            R.string.offline_error, null);
                }

            } else {
                getActivity().finish();
            }
        }
    };

}
