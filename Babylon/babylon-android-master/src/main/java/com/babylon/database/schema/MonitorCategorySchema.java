package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public interface MonitorCategorySchema {

    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri BASE_CONTENT_URI = Uri.parse("content://" + DatabaseHelper.CONTENT_AUTHORITY);
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.MONITOR_CATEGORY.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        NAME("name", DBType.TEXT),
        SORT("sort", DBType.INT),
        DESCRIPTION("description", DBType.TEXT),
        ICONS_DESCRIPTION("iconsDescription", DBType.TEXT),
        ICONS_REPORT("iconsReport", DBType.TEXT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public final static class Query extends BaseQuery {
        public static final int CURSOR_LOAD_CATEGORY = 45368;

        public static final String CATEGORY_SELECTION = Columns.ID.getName() + " = ?";

        public static final int _ID = 0;
        public static final int ID = 1;
        public static final int NAME = 2;
        public static final int SORT = 3;
        public static final int DESCRIPTION = 4;
        public static final int ICONS_DESCRIPTION = 5;
        public static final int ICONS_REPORT = 6;
    }
}

