package com.iversecomics.client.bitmap.loader;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iversecomics.client.bitmap.IBitmapLoader;
import com.iversecomics.io.IOUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class AssetBitmapLoader implements IBitmapLoader {
    private AssetManager assets;

    public AssetBitmapLoader(AssetManager assets) {
        this.assets = assets;
    }

    public AssetBitmapLoader(Context context) {
        this(context.getAssets());
    }

    @Override
    public String[] getUriSchemes() {
        return new String[]{"asset"};
    }

    @Override
    public Bitmap loadBitmap(URI bitmapUri) throws IOException {
        if (!"asset".equals(bitmapUri.getScheme())) {
            throw new IOException("Unsupported URI: " + bitmapUri.toASCIIString());
        }
        InputStream in = null;
        try {
            in = assets.open(bitmapUri.getSchemeSpecificPart());
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            // opts.inPurgeable = true;
            return BitmapFactory.decodeStream(in, null, opts);
        } finally {
            IOUtil.close(in);
        }
    }

}
