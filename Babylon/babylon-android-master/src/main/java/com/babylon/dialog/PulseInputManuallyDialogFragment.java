package com.babylon.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.babylon.R;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.validator.ValidationController;
import com.babylon.validator.ValueBetweenValidator;
import com.babylon.view.CustomEditText;

public class PulseInputManuallyDialogFragment extends DialogFragment {
    public static final String TAG = PulseInputManuallyDialogFragment.class.getSimpleName();

    private static final int MIN_PULSE_VALUE = 30;
    private static final int MAX_PULSE_VALUE = 200;

    private ValidationController validationController;
    private CustomEditText pulseEditText;

    public static PulseInputManuallyDialogFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        PulseInputManuallyDialogFragment fragment = new PulseInputManuallyDialogFragment();
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new AlertDialog.Builder(getActivity())
                .setCancelable(true)
                .setView(getContentView())
                .create();

        return dialog;
    }

    public View getContentView() {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_pulse_input_manually, null, false);
        initPulseEditText(view);

        final Button okButton = (Button) view.findViewById(R.id.btn_ok);
        okButton.setOnClickListener(onPulseSaveClickListener);

        final Button cancelButton = (Button) view.findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(onCancelClickListener);

        return view;
    }

    private void initPulseEditText(View view) {
        validationController = new ValidationController();
        pulseEditText = (CustomEditText) view.findViewById(R.id.edit_text_pulse);

        pulseEditText.getEditText().setTextColor(R.color.grey);
        pulseEditText.getEditText().setHintTextColor(R.color.grey_light);

        ValueBetweenValidator weightValueValidator = new ValueBetweenValidator(null,
                getActivity().getString(R.string.validator_field_value_between, MIN_PULSE_VALUE, MAX_PULSE_VALUE),
                MIN_PULSE_VALUE, MAX_PULSE_VALUE);


        pulseEditText.validationAfterTextChanged(validationController);
        validationController.addValidation(pulseEditText, weightValueValidator);
    }

    private final View.OnClickListener onPulseSaveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final String pulse = pulseEditText.getText().toString();

            if (validationController.performValidation()) {
                long dayInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
                MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature(
                        MonitorMeParam.RESTING_PULSE_ID.getId(), pulse, dayInMillis);

                DatabaseOperationUtils.getInstance().updateMonitorMeFeatureOperation(feature, getActivity()
                        .getContentResolver());
                SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);
                dismiss();
            }
        }
    };

    private final View.OnClickListener onCancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismiss();
        }
    };

}
