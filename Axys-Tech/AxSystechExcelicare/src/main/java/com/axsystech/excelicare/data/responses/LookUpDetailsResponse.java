package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by someswar on 7/1/2015.
 */
public class LookUpDetailsResponse extends BaseResponse {

    @SerializedName("SystemLookups")
    private ArrayList<Lookups> systemLookupsDataAL;

    @SerializedName("UserLookups")
    private ArrayList<Lookups> userLookupsDataAL;

    public ArrayList<Lookups> getSystemLookupsDataAL() {
        return systemLookupsDataAL;
    }

    public void setSystemLookupsDataAL(ArrayList<Lookups> systemLookupsDataAL) {
        this.systemLookupsDataAL = systemLookupsDataAL;
    }

    public ArrayList<Lookups> getUserLookupsDataAL() {
        return userLookupsDataAL;
    }

    public void setUserLookupsDataAL(ArrayList<Lookups> userLookupsDataAL) {
        this.userLookupsDataAL = userLookupsDataAL;
    }

    public class Lookups {
        @SerializedName("Id")
        private int Id;

        @SerializedName("list")
        private ArrayList<LookupsListObject> lookupsList;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public ArrayList<LookupsListObject> getLookupsList() {
            return lookupsList;
        }

        public void setLookupsList(ArrayList<LookupsListObject> lookupsList) {
            this.lookupsList = lookupsList;
        }
    }

    public ArrayList<LookupsListObject> getSystemLookupsList() {
        if (systemLookupsDataAL != null && systemLookupsDataAL.size() > 0) {
            Lookups lookUpsObj = systemLookupsDataAL.get(0);
            if (lookUpsObj != null) {
                if (lookUpsObj.getLookupsList() != null && lookUpsObj.getLookupsList().size() > 0) {
                    return lookUpsObj.getLookupsList();
                }
            }
        }

        return null;
    }

}
