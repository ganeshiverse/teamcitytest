package com.iversecomics.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.downloads.internal.data.DownloadData;
import com.iversecomics.client.downloads.internal.data.DownloadsTable;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.DownloadProgressChangeEvent;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.otto.Subscribe;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadsFragment extends ListFragment {

    private static final Logger LOG = LoggerFactory.getLogger(DownloadsFragment.class);
    private final String TAG = DownloadsFragment.class.getSimpleName();
    private DownloadListAdapter mAdapter;
    private ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (ViewGroup) inflater.inflate(R.layout.fragment_downloads, container, false);

        rootView.findViewById(R.id.action_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        mListView = (ListView) rootView.findViewById(android.R.id.list);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Cursor cursor = getActivity().getContentResolver().query(DownloadsTable.CONTENT_URI, null, null, null, DownloadsTable._ID);

        mAdapter = new DownloadListAdapter(getActivity(), cursor);
        setListAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
        OttoBusProvider.getInstance().register(mAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        OttoBusProvider.getInstance().unregister(mAdapter);
    }

    @Override
    public void onListItemClick(final ListView l, final View v, final int position, long id) {
        // TODO: do anything on click?
    }


    private class DownloadListAdapter extends CursorAdapter {
        final Pattern comicBundleNamePattern = Pattern.compile("(com\\..+)\\.cbsd");
        LayoutInflater mInflater;
        Context context;
        ContentResolver contentResolver;
        CoverSize coverSize;
        String progressFormat;


        public DownloadListAdapter(Context context, Cursor cursor) {
            super(context, cursor, true);
            this.context = context;
            this.contentResolver = context.getContentResolver();
            this.coverSize = CoverSize.bestFit(context, R.dimen.menu_item_height);

            mInflater = LayoutInflater.from(context);

            String downloadingWord = getResources().getString(R.string.downloading_progress);
            // english example: Downloading (50%)
            progressFormat = downloadingWord + " (%d)%%";

        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.item_download, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            final DownloadData download = DownloadsTable.fromCursor(cursor);

            // see if we already have the correct data in this view
            // if so, ignore unnecessary calls to bindview to prevent row flicker
            if (view.getTag() != null && view.getTag().equals(download.getUri()))
                return;

            // tag the view with the comic name so we can find it and update its progress
            Log.i(TAG, "*** download uri: " + download.getUri());
            view.setTag(download.getUri());

            final ImageView thumb = (ImageView) view.findViewById(R.id.thumb);
            ((TextView) view.findViewById(R.id.name)).setText(download.getName());

            final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
            final TextView progressText = (TextView) view.findViewById(R.id.secondary);
            int progress = download.getPercentageComplete();
            // change progress bar to blue
            progressBar.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
            progressBar.setProgress(progress);
            progressText.setText(String.format(Locale.getDefault(), progressFormat, progress));

            // load cover image
            if (download.getCoverImageFileName() != null) {
                boolean isTablet = getResources().getBoolean(com.iversecomics.archie.android.R.bool.isTablet);
                Uri coverImageUri=null;
                //Added to fix the issue : #5244
                if(isTablet){
                     coverImageUri = CoverSize.TABLET_LARGE.getServerUri(download.getCoverImageFileName());
                }else{
                    coverImageUri = coverSize.getServerUri(download.getCoverImageFileName());
                }
                //Uri coverImageUri = coverSize.getServerUri(download.getCoverImageFileName());
                ImageLoader.getInstance().displayImage(coverImageUri.toString(), thumb);
                thumb.setVisibility(View.VISIBLE);
            } else {
                thumb.setVisibility(View.INVISIBLE);
            }
        }

        @Subscribe
        public void onDownloadProgressChange(DownloadProgressChangeEvent event) {
            final int progress = (int) ((event.getCurrentByte() * 100) / event.getTotalBytes());
            Log.i(TAG, "*** progress: " + progress);

            // Extract bundle name from the download uri so that we
            // can display the download progress if this is a progress broadcast
            // for the downloadData this view is displaying.
            final Matcher m = comicBundleNamePattern.matcher(event.getUri());
            final String bundleName = m.find() ? m.group(1) : "";

            // Loop through visible rows and find one that matches the comicBundle
            // for this progress broadcast.
            for (int i = 0; i < mListView.getChildCount(); i++) {
                View rowView = mListView.getChildAt(i);
                Log.i(TAG, "*** bundle name: " + bundleName);
                Log.i(TAG, "*** row tag: " + rowView.getTag());
                        /* example:
                        *** bundle name:                        com.iversecomics.archie.comics.archie.meets.glee.preview.02272013
                        *** row tag: http://www.iversemedia.com/com.iversecomics.archie.comics.archie.meets.glee.preview.02272013
                         */
                if (rowView.getTag().toString().contains(bundleName)) {
                    updateProgressView(rowView, progress);
                    break;
                }
            }
        }

        /**
         * @param view     row view with R.id.progress_bar and R.id.secondary
         * @param progress 0-100
         */
        private void updateProgressView(View view, int progress) {

            final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
            final TextView progressText = (TextView) view.findViewById(R.id.secondary);

            progressBar.setProgress(progress);
            progressText.setText(String.format(Locale.getDefault(), progressFormat, progress));
        }

    }

}
