package com.babylon.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.model.Region;

public class RegionArrayAdapter extends ArrayAdapter<Region> {


    public RegionArrayAdapter(Context context, int resource, Region[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.spinner_dropdown_item_region, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name_text_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String name = getItem(position).getName();
        if (!TextUtils.isEmpty(name)) {
            holder.name.setText(name);
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView name;
    }
}
