package com.iversecomics.client.downloads.internal.data;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.iversecomics.client.downloads.internal.DownloaderService;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DownloadAddExecutor extends AbstractDatabaseExecutor {
    private static final Logger LOG = LoggerFactory.getLogger(DownloadAddExecutor.class);
    private Context context;
    private Intent serviceIntent;
    private DownloadData download;

    public DownloadAddExecutor(Uri contentUri, ContentProviderClient client, DownloadData download) {
        super(contentUri, client);
        this.download = download;
    }

    public void addServiceIntent(Context context) {
        this.context = context;
        this.serviceIntent = new Intent(context, DownloaderService.class);
    }

    @Override
    public synchronized void execute() {
        LOG.debug(".execute() - %s", download);
        if (!isActiveInDatabase(download)) {
            saveToDatabase(download);
            if (serviceIntent != null) {
                context.startService(serviceIntent);
            }
        }
    }

    private boolean isActiveInDatabase(DownloadData download) {
        Uri uri = download.getUri();
        String projection[] = new String[]{
                DownloadsTable._ID, DownloadsTable.URI
        };
        String where = "( " + DownloadsTable.URI + " == ? )";
        String whereArgs[] = new String[]{
                uri.toString()
        };
        Cursor c = null;

        try {
            c = client.query(contentUri, projection, where, whereArgs, null);

            if (c == null) {
                return false;
            }

            return c.getCount() > 0;
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to query for active downloads");
            return false;
        } finally {
            DBUtil.close(c);
        }
    }

    private void saveToDatabase(DownloadData download) {
        try {
            ContentValues values = DownloadsTable.asValues(download);
            client.insert(contentUri, values);
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to save to database - %s", download);
        }
    }
}