package com.iversecomics.client.bitmap;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public final class BitmapUtil {
    private static final Logger LOG = LoggerFactory.getLogger(BitmapUtil.class);

    /**
     * Render a Drawable onto a bitmap.
     *
     * @param d
     * @param width
     * @param height
     * @return
     */
    public static Bitmap asBitmap(Drawable d, int width, int height) {
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = Bitmap.createBitmap(width, height, config);
        Canvas canvas = new Canvas(bitmap);
        d.draw(canvas);
        return bitmap;
    }

    public static Bitmap load(File file) throws IOException {
        if (!file.exists()) {
            LOG.warn("File does not exist: " + file);
            return null;
        }

        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            return BitmapFactory.decodeStream(in);
        } finally {
            IOUtil.close(in);
        }
    }

    public static void save(Bitmap bitmap, File outputFile) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(outputFile);
            bitmap.compress(CompressFormat.PNG, 100, out);
            out.flush();
        } finally {
            IOUtil.close(out);
        }
    }
}
