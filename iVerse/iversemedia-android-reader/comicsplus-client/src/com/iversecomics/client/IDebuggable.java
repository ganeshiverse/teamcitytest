package com.iversecomics.client;

public interface IDebuggable {
    public boolean isDebug();

    public void setDebug(boolean flag);
}
