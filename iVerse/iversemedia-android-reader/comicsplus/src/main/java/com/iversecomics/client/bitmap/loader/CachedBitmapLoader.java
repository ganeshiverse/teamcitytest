package com.iversecomics.client.bitmap.loader;

import android.graphics.Bitmap;

import com.iversecomics.client.bitmap.IBitmapLoader;
import com.iversecomics.client.util.CacheDir;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class CachedBitmapLoader implements IBitmapLoader {
    private static final Logger LOG = LoggerFactory.getLogger(CachedBitmapLoader.class);
    private CacheDir cacheDir;
    private Map<String, IBitmapLoader> loaderMap;

    public CachedBitmapLoader(CacheDir cacheDir) {
        this.cacheDir = cacheDir;
        this.loaderMap = new HashMap<String, IBitmapLoader>();
    }

    @Override
    public String[] getUriSchemes() {
        return new String[]{"cached"};
    }

    @Override
    public Bitmap loadBitmap(URI bitmapUri) throws IOException {
        URI uri = URI.create(bitmapUri.getSchemeSpecificPart());

        Bitmap bitmap = cacheDir.getBitmap(uri);
        if (bitmap != null) {
            return bitmap;
        } else {
            String scheme = uri.getScheme();
            IBitmapLoader loader = loaderMap.get(scheme);
            if (loader == null) {
                LOG.error("No IBitmapLoader for URI: " + uri.toASCIIString());
                throw new IOException("Unable to load bitmap with URI - " + uri.toASCIIString());
            } else {
                // Already in AsyncBitmapLoader, just reuse it.
                bitmap = loader.loadBitmap(uri);
                cacheDir.addBitmap(uri, bitmap);
                return bitmap;
            }
        }
    }

    public void setBitmapLoaderMap(Map<String, IBitmapLoader> loaderMap) {
        this.loaderMap.putAll(loaderMap);
    }
}
