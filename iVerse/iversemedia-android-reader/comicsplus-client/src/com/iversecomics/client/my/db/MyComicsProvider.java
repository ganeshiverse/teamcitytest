package com.iversecomics.client.my.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.iversecomics.client.util.DBUtil;

public class MyComicsProvider extends ContentProvider {
    // URI Requests
    private static final int        ALLCOMICS       = 1;
    private static final int        SINGLE_COMIC    = 2;
    private static final int        SINGLE_COMIC_ID = 3;

    private static final UriMatcher URIMATCHER;

    static {
        URIMATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URIMATCHER.addURI(MyComicsDB.AUTHORITY, "comics", ALLCOMICS);
        URIMATCHER.addURI(MyComicsDB.AUTHORITY, "comics/#", SINGLE_COMIC);
        URIMATCHER.addURI(MyComicsDB.AUTHORITY, "comics/$", SINGLE_COMIC_ID);
    }

    private SQLiteDatabase          db;

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table;
        String where = selection;
        String whereArgs[] = selectionArgs;

        switch (URIMATCHER.match(uri)) {
        case ALLCOMICS:
            table = MyComicsTable.TABLE;
            break;
        case SINGLE_COMIC:
            table = MyComicsTable.TABLE;
            where = DBUtil.toPrependedWhere(selection, MyComicsTable._ID + " = ?");
            whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
            break;
        case SINGLE_COMIC_ID:
            table = MyComicsTable.TABLE;
            where = DBUtil.toPrependedWhere(selection, MyComicsTable.COMICID + " = ?");
            whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
            break;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        int count = db.delete(table, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (URIMATCHER.match(uri)) {
        case ALLCOMICS:
            return MyComicsTable.CONTENT_TYPE;
        case SINGLE_COMIC:
            return MyComicsTable.CONTENT_ITEM_TYPE;
        case SINGLE_COMIC_ID:
            return MyComicsTable.CONTENT_ITEM_TYPE;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowid = db.insert(MyComicsTable.TABLE, "", values);

        if (rowid > 0) {
            Uri nuri = ContentUris.withAppendedId(MyComicsTable.CONTENT_URI, rowid);
            getContext().getContentResolver().notifyChange(nuri, null);
            return nuri;
        }
        throw new SQLException("Failed to insert row into + " + uri);
    }

    @Override
    public boolean onCreate() {
        db = MyComicsDatabaseHelper.getInstance(getContext()).getWritableDatabase();
        return (db != null);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String tables = null;
        String columns[] = projection;
        String where = selection;
        String whereArgs[] = selectionArgs;
        String groupBy = null;
        String having = null;
        String orderBy = sortOrder;

        switch (URIMATCHER.match(uri)) {
        case ALLCOMICS:
            tables = MyComicsTable.TABLE;
            orderBy = DBUtil.toOrderBy(sortOrder, MyComicsTable.DEFAULT_SORT_ORDER);
            break;
        case SINGLE_COMIC_ID:
            tables = MyComicsTable.TABLE;
            where = DBUtil.toPrependedWhere(selection, MyComicsTable._ID + " = ?");
            whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
            orderBy = DBUtil.toOrderBy(sortOrder, MyComicsTable.DEFAULT_SORT_ORDER);
            break;
        case SINGLE_COMIC:
            tables = MyComicsTable.TABLE;
            where = DBUtil.toPrependedWhere(selection, MyComicsTable.COMICID + " = ?");
            whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
            orderBy = DBUtil.toOrderBy(sortOrder, MyComicsTable.DEFAULT_SORT_ORDER);
            break;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        // Apply the Query
        // DBUtil.debugQuery(uri, tables, columns, where, whereArgs, orderBy);
        Cursor c = db.query(tables, columns, where, whereArgs, groupBy, having, orderBy);

        // Register uri for notifications
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String table = null;
        String where = selection;
        String whereArgs[] = selectionArgs;

        switch (URIMATCHER.match(uri)) {
        case ALLCOMICS:
            table = MyComicsTable.TABLE;
            break;
        case SINGLE_COMIC:
            table = MyComicsTable.TABLE;
            where = DBUtil.toPrependedWhere(selection, MyComicsTable._ID + " = ?");
            whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
            break;
        case SINGLE_COMIC_ID:
            table = MyComicsTable.TABLE;
            where = DBUtil.toPrependedWhere(selection, MyComicsTable.COMICID + " = ?");
            whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
            break;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        int count = db.update(table, values, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
