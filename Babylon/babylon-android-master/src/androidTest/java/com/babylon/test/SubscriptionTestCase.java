package com.babylon.test;

import android.test.AndroidTestCase;
import com.babylon.model.payments.Subscription;

public class SubscriptionTestCase extends AndroidTestCase {
    public void testSubscriptionIsFree1() {
        Subscription subscription = new Subscription();
        subscription.setPrice(0.01);
        assertFalse(subscription.isFree());
    }

    public void testSubscriptionIsFree2() {
        Subscription subscription = new Subscription();
        subscription.setPrice(1.01);
        assertFalse(subscription.isFree());
    }

    public void testSubscriptionIsFree3() {
        Subscription subscription = new Subscription();
        subscription.setPrice(100D);
        assertFalse(subscription.isFree());
    }

    public void testSubscriptionIsFree4() {
        Subscription subscription = new Subscription();
        subscription.setPrice(1.856301);
        assertFalse(subscription.isFree());
    }

    public void testSubscriptionIsFree5() {
        Subscription subscription = new Subscription();
        subscription.setPrice(854.1501);
        assertFalse(subscription.isFree());
    }

    public void testSubscriptionIsFree6() {
        Subscription subscription = new Subscription();
        subscription.setPrice(0.0001501);
        assertFalse(subscription.isFree());
    }
}
