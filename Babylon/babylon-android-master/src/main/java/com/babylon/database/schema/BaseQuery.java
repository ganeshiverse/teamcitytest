package com.babylon.database.schema;

import android.text.TextUtils;

import com.babylon.enums.MonitorMeParam;
import com.babylon.model.NutrientCategory;
import com.babylon.utils.DateUtils;

import java.util.Date;
import java.util.List;

public class BaseQuery {

    public static StringBuilder getMonitorMeParamsSelection(List<MonitorMeParam> params) {
        StringBuilder paramsQuery = new StringBuilder();
        for (MonitorMeParam param : params) {
            if(!TextUtils.isEmpty(paramsQuery)) {
                paramsQuery.append(" OR ");
            }
            paramsQuery.append(MonitorMeParamSchema.Columns.FEATURE_ID).append(" = ")
                    .append(param.getId());
        }
        return paramsQuery;
    }

    public static String[] getDateSelectionArguments(long dateInMillis) {
        long startDay = DateUtils.getDayStartMills(new Date(dateInMillis));
        return new String[]{String.valueOf(startDay),
                String.valueOf(DateUtils.getDayStartMills(dateInMillis, 1))};
    }

    public static String[] getDateSelectionArgumentsByCategory(long dateInMillis, NutrientCategory category) {
        long startDay = DateUtils.getDayStartMills(new Date(dateInMillis));
        return new String[]{String.valueOf(startDay),
                String.valueOf(DateUtils.getDayStartMills(dateInMillis, 1)),
        String.valueOf(category.getValue())};
    }

    public static String[] getDateSelectionArguments(long dateFrom, long dateTo) {
        return new String[]{String.valueOf(dateFrom),
                String.valueOf(dateTo)};
    }

    public static String[] getDateSelectionArguments(long dateInMillis, String featureId) {
        Date date = new Date(dateInMillis);
        long dayStartMills = DateUtils.getDayStartMills(date);
        return new String[]{String.valueOf(dayStartMills), String.valueOf(DateUtils.getDayStartMills
                (dayStartMills, 1)), featureId};
    }
}
