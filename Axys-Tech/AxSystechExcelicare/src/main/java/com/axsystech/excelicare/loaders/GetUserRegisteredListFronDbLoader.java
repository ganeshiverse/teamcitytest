package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by alanka on 9/23/2015.
 */
public class GetUserRegisteredListFronDbLoader extends DataAsyncTaskLibLoader<List<DeviceUsersDbModel>> {

    public GetUserRegisteredListFronDbLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<DeviceUsersDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<DeviceUsersDbModel, Long> registeredUsersListDao = helper.getDao(DeviceUsersDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<DeviceUsersDbModel>>() {
                @Override
                public List<DeviceUsersDbModel> call() throws Exception {
                    return registeredUsersListDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
