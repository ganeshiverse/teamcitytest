package com.iversecomics.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.Account;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Preferences;
import com.iversecomics.client.account.PasswordRequestActivity;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.tasks.ServerConfigTask;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.ServerConfigErrorEvent;
import com.squareup.otto.Subscribe;

import java.util.concurrent.Future;

public class LoginActivity extends Activity {

    Future<?> serverConfigFuture;
    ProgressDialog progressDialog;
    // The login failed/error dialog need to display only at login failed case, but in the app the login failure dialog displaying when the user failed to login and after that when he comes to login scrren, the same dialog is displaying.
    // to fix this issue, we are using a bool value. if the bool - true, we are displaying failure dialog.  bool - false, we not displaying failure dialog.
        boolean isLoginFailed = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.logo);
        isLoginFailed =false;
        makeLoginDialogPrompt();
    }

    @Override
    public void onResume() {
        super.onResume();
        OttoBusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        OttoBusProvider.getInstance().unregister(this);
    }

    void makeLoginDialogPrompt() {

        final LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.login_dialog, null);

        final SharedPreferences preferences = getSharedPreferences(Preferences.NAME, 0);

        final String userEmail = preferences.getString(Preferences.KEY_OWNER_EMAIL, "");
        final String userPassword = preferences.getString(Preferences.KEY_OWNER_PASSWORD, "");

        final EditText emailEditText = (EditText) textEntryView.findViewById(R.id.user_email);
        final EditText passwordEditText = (EditText) textEntryView.findViewById(R.id.user_password);

        emailEditText.setText(userEmail);
        passwordEditText.setText(userPassword);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.icon)
                .setTitle(R.string.login_title)
                .setView(textEntryView)
                .setPositiveButton(R.string.login_positive_button, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        final String userEmail = emailEditText.getText().toString();
                        final String userPassword = passwordEditText.getText().toString();

                        if (userEmail.length() > 0 && userPassword.length() > 0) {
                            commitLoginToPrefs(userEmail, userPassword);
                            hideKeyboard(emailEditText);
                            dialog.dismiss();
                            // means no need to display failure dialog.
                            isLoginFailed = true;
                            login();
                        } else {
                            dialog.dismiss();
                            makeLoginDialogPrompt();
                        }
                    }
                })
                .setNeutralButton(R.string.login_lost_password_button, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        hideKeyboard(emailEditText);

                        final String userEmail = emailEditText.getText().toString();

                        final Intent intent = new Intent(LoginActivity.this, PasswordRequestActivity.class);

                        if (android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())
                            intent.putExtra("email", userEmail);
                        dialog.dismiss();
                        startActivity(intent);
                        finish();

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hideKeyboard(emailEditText);
                        dialog.dismiss();
                        finish();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface arg0) {
                        hideKeyboard(emailEditText);
                        finish();
                    }
                }).create().show();
    }

    private void hideKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

    }


    void commitLoginToPrefs(final String userEmail, final String userPassword) {

        final SharedPreferences preferences = getSharedPreferences(Preferences.NAME, 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString(Preferences.KEY_OWNER_EMAIL, userEmail);
        editor.putString(Preferences.KEY_OWNER_PASSWORD, userPassword);

        final IverseApplication app = IverseApplication.getApplication();
        app.getOwnership().setEmailAddress(userEmail);
        app.getOwnership().setPassword(userPassword);

        editor.commit();
    }

    void login() {

        progressDialog = ProgressDialog.show(LoginActivity.this, "", getResources().getString(R.string.logging_in), true);
        // login is validated by the ServerConfigTask (which reads login credentials from SharedPrefs)
        final IverseApplication app = IverseApplication.getApplication();
        final Task task = new ServerConfigTask(this, app.getComicStore());
        serverConfigFuture = app.getTaskPool().submit(task);
    }


    // Subscribe to Otto Bus Events

    @Subscribe
    public void serverConfigAvailable(ServerConfig config) {
        if (progressDialog != null)
            progressDialog.dismiss();

        final String userAccountStatus = ServerConfig.getDefault().getUserAccountStatus();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //Fixed NPE crash for login
        if(userAccountStatus !=null) {
            switch (Integer.valueOf(userAccountStatus)) {
                case Account.Login.STATUS_SUCCESS: {

                    builder.setTitle(R.string.login_successfull_title)
                            .setIcon(R.drawable.icon)
                            .setMessage(R.string.login_successfull_message)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            finish();
                                        }
                                    }
                            ).create().show();
                }
                return;

                case Account.Login.STATUS_TOO_MANY_DEVICES: {
                    builder.setTitle(R.string.login_too_many_devices_title)
                            .setIcon(android.R.drawable.stat_sys_warning)
                            .setMessage(R.string.login_too_many_devices_message)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();
                                            makeLoginDialogPrompt();
                                        }
                                    }
                            ).create().show();
                }
                break;
                case Account.Login.STATUS_LOGIN_BAD: {
                    if (isLoginFailed) {
                        builder.setTitle(R.string.login_incorrect_title)
                                .setIcon(android.R.drawable.stat_sys_warning)
                                .setMessage(R.string.login_incorrect_message)
                                .setCancelable(false)
                                .setPositiveButton(R.string.login_incorrect_new_account_button, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        //clear shared prefs of bad password
                                        dialog.dismiss();
                                        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                                    }
                                })

                                .setNegativeButton(R.string.login_incorrect_remind_button, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();
                                        startActivity(new Intent(LoginActivity.this, PasswordRequestActivity.class));
                                        finish();
                                    }
                                })
                                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int arg1) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        }
                                ).create().show();
                    }

                }
                break;
                default: {
                    // TODO - Handle the '0' case when server sends userAccountStatus as '0'.
                    // As per the unit test result, We do nothing when server sends "userAccountStatus":0
                    // makeLoginDialogPrompt();
                }
                break;
            }
        }
        commitLoginToPrefs("", "");

    }

    @Subscribe
    public void serverConfigError(ServerConfigErrorEvent errorEvent) {
        if (progressDialog != null)
            progressDialog.dismiss();

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error_title)
                .setMessage(errorEvent.getErrorMessage())
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }
                ).create().show();
    }

}