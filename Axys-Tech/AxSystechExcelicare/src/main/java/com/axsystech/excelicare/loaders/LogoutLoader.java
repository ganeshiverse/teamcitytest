package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/13/2015.
 */
public class LogoutLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private final String token;

    public LogoutLoader(final Context context, String token) {
        super(context);
        this.token = token;

    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        final BaseResponse authResponse = ServerMethods.getInstance().getLogoutresponse(token);

        return authResponse;
    }
}
