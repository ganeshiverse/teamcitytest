package com.babylon.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import com.babylon.R;
import com.babylon.enums.Gender;
import com.babylon.utils.L;

public class GenderView extends LinearLayout {

    private static final String TAG = GenderView.class.getSimpleName();

    private CheckBox femaleCheckBox;
    private CheckBox maleCheckBox;

    public GenderView(Context context) {
        super(context);
        initView(context);
    }

    public GenderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public GenderView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_gender, this);

        maleCheckBox = (CheckBox) findViewById(R.id.chButtonMale);
        maleCheckBox.setOnCheckedChangeListener(onMaleCheckedChangeListener);

        femaleCheckBox = (CheckBox) findViewById(R.id.chButtonFemale);
        femaleCheckBox.setOnCheckedChangeListener(onFemaleCheckedChangeListener);
    }
    
    public void setGender(Gender gender) {
        switch (gender) {
            case FEMALE:
                femaleCheckBox.setChecked(true);
                break;
            case MALE:
                maleCheckBox.setChecked(true);
                break;
            default:
                L.e(TAG, "Unknown Gender value");
        }
    }

    public @Nullable String getSelectedGender() {
        if (femaleCheckBox.isChecked()) {
            return Gender.getGender(Gender.FEMALE);
        } else if (maleCheckBox.isChecked()) {
            return Gender.getGender(Gender.MALE);
        } else {
            return null;
        }
    }

    private final CompoundButton.OnCheckedChangeListener onMaleCheckedChangeListener = new CompoundButton
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (femaleCheckBox.isChecked() && b) {
                femaleCheckBox.setChecked(false);
            } else if(!femaleCheckBox.isChecked() && !maleCheckBox.isChecked()){
                maleCheckBox.setChecked(true);
            }

        }
    };

    private final CompoundButton.OnCheckedChangeListener onFemaleCheckedChangeListener = new CompoundButton
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (maleCheckBox.isChecked() && b) {
                maleCheckBox.setChecked(false);
            } else if(!maleCheckBox.isChecked() && !femaleCheckBox.isChecked()){
                femaleCheckBox.setChecked(true);
            }
        }
    };
}
