package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 01/09/15.
 */
public class AppointmentDetailsResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private ArrayList<AppointmentDetailsData> appointmentDetailsDataAL;

    public ArrayList<AppointmentDetailsData> getAppointmentDetailsDataAL() {
        return appointmentDetailsDataAL;
    }

    public class AppointmentDetailsData implements Serializable {

        @SerializedName("Address")
        private String address;

        @SerializedName("apptId")
        private String apptId;

        @SerializedName("attendance")
        private String attendance;

        @SerializedName("clinicanId")
        private String clinicanId;

        @SerializedName("clinicianName")
        private String clinicianName;

        @SerializedName("ClinicianUserID")
        private String clinicianUserID;

        @SerializedName("clinicid")
        private String clinicid;

        @SerializedName("clinicName")
        private String clinicName;

        @SerializedName("description")
        private String description;

        @SerializedName("Email")
        private String email;

        @SerializedName("enddatetime")
        private String enddatetime;

        @SerializedName("Phone")
        private String phone;

        @SerializedName("startdatetime")
        private String startdatetime;

        @SerializedName("ticketNumber")
        private String ticketNumber;

        @SerializedName("Speciality")
        private String speciality;

        @SerializedName("Fee")
        private String fee;

        @SerializedName("URL")
        private String URL;

        public String getURL() {
            return URL;
        }

        public String getFee() {
            return fee;
        }

        public String getSpeciality() {
            return speciality;
        }

        public String getAddress() {
            return address;
        }

        public String getApptId() {
            return apptId;
        }

        public String getAttendance() {
            return attendance;
        }

        public String getClinicanId() {
            return clinicanId;
        }

        public String getClinicianName() {
            return clinicianName;
        }

        public String getClinicianUserID() {
            return clinicianUserID;
        }

        public String getClinicid() {
            return clinicid;
        }

        public String getClinicName() {
            return clinicName;
        }

        public String getDescription() {
            return description;
        }

        public String getEmail() {
            return email;
        }

        public String getEnddatetime() {
            return enddatetime;
        }

        public String getPhone() {
            return phone;
        }

        public String getStartdatetime() {
            return startdatetime;
        }

        public String getTicketNumber() {
            return ticketNumber;
        }
    }
}
