package com.iversecomics.client.downloads.internal;

import org.apache.http.StatusLine;

import com.iversecomics.client.downloads.internal.data.DownloadStatus;

/**
 * A Recoverable failure during a download.
 */
public class DownloadException extends Exception {
    private static final long serialVersionUID = 5024187356050952159L;
    private DownloadStatus    status;

    public DownloadException(DownloadStatus status, String detailMessage) {
        super(detailMessage);
        this.status = status;
    }

    public DownloadException(DownloadStatus status, String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
        this.status = status;
    }

    public DownloadException(StatusLine statusLine) {
        this(new DownloadStatus(statusLine.getStatusCode()), statusLine.getReasonPhrase());
    }

    public DownloadException(StatusLine statusLine, Throwable throwable) {
        this(new DownloadStatus(statusLine.getStatusCode()), statusLine.getReasonPhrase(), throwable);
    }

    public DownloadStatus getStatus() {
        return status;
    }
}
