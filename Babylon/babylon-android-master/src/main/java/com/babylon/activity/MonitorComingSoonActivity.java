package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.babylon.R;

public class MonitorComingSoonActivity extends Activity {

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, MonitorComingSoonActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_coming_soon);
    }

    public void onOkClick(View view) {
        finish();
    }
}
