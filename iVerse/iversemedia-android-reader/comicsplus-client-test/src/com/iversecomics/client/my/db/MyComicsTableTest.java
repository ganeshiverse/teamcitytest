package com.iversecomics.client.my.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.util.DBUtil;
import com.iversecomics.util.text.StringUtils;

public class MyComicsTableTest extends AbstractMyComicsProviderTest {
    private MyComic querySingleComic(String msg, Uri contentUri, String selection, String selectionArgs[]) {
        Cursor c = null;
        try {
            c = resolver.query(contentUri, null, selection, selectionArgs, null);
            StringBuilder msgb = new StringBuilder();
            msgb.append(msg);
            msgb.append(" (where=").append(selection);
            msgb.append(") (whereArgs=").append(StringUtils.join(selectionArgs, "|"));
            msgb.append(")");
            assertDatabaseRowCount(msgb.toString(), c, 1);
            MyComic comic = MyComicsTable.fromCursor(c);
            return comic;
        } finally {
            DBUtil.close(c);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testComicInsertObject() {
        MyComic comic = new MyComic();
        comic.setComicId("test.comic.object");

        ContentValues values = MyComicsTable.asValues(comic);

        Uri uri = resolver.insert(MyComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("MyComic", uri);
    }

    @MediumTest
    public void testComicInsertSimple() {
        ContentValues values = new ContentValues();
        values.put(MyComicsTable.COMICID, "test.comic.simple");

        Uri uri = resolver.insert(MyComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("MyComic", uri);
    }

    @MediumTest
    public void testComicInsertThenInsert() {
        String id = "test.comic";

        ContentValues values = new ContentValues();
        values.put(MyComicsTable.COMICID, id);
        values.put(MyComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(MyComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("MyComic", uri);

        // Now try to find it
        String selection = String.format("%s.%s = ?", MyComicsTable.TABLE, MyComicsTable.COMICID);
        String selectionArgs[] = new String[] { id };
        String msg = "MyComic (initial)";
        MyComic comic1 = querySingleComic(msg, MyComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestComic", comic1.getName());

        // Insert another entry, hopefully overwriting the original due to a proper table construction.
        ContentValues second = new ContentValues();
        second.put(MyComicsTable.COMICID, id);
        second.put(MyComicsTable.NAME, "Test Comic Post"); // New name (with space)

        Uri uriSecond = resolver.insert(MyComicsTable.CONTENT_URI, second);
        assertDatabaseUriHasDBId("MyComic", uriSecond);

        // Try to find it again.
        msg = "MyComic (post insert)";
        MyComic comic2 = querySingleComic(msg, MyComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Comic Post", comic2.getName());
    }

    @MediumTest
    public void testComicInsertThenUpdate() {
        String id = "test.comic";

        ContentValues values = new ContentValues();
        values.put(MyComicsTable.COMICID, id);
        values.put(MyComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(MyComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("MyComic", uri);

        // Now try to find it
        String selection = String.format("%s.%s = ?", MyComicsTable.TABLE, MyComicsTable.COMICID);
        String selectionArgs[] = new String[] { id };
        String msg = "MyComic (initial)";
        MyComic comic1 = querySingleComic(msg, MyComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestComic", comic1.getName());

        // Update Entry
        ContentValues update = new ContentValues();
        update.put(MyComicsTable.COMICID, id);
        update.put(MyComicsTable.NAME, "Test Comic Post"); // New name (with space)

        String where = MyComicsTable.COMICID + " = ?";
        String whereArgs[] = new String[] { id };
        int count = resolver.update(MyComicsTable.CONTENT_URI, update, where, whereArgs);
        assertEquals("MyComic Update Count", 1, count);

        // Try to find it again.
        msg = "MyComic (post insert)";
        MyComic comic2 = querySingleComic(msg, MyComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Comic Post", comic2.getName());
    }
}
