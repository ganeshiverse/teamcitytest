package com.babylon.api.request;

import android.text.TextUtils;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.MonitorMeReport;
import com.babylon.utils.DateUtils;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class MonitorMeReportGet extends AbsRequestGet<ResponseList<MonitorMeReport>> {

    private static final String DATE_FROM = "dateFrom";
    private final static String DATE_NOW = "dateNow";
    private static final String DATE_TO = "dateTo";

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.MONITOR_ME_REPORT_GET);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param dateFrom date, mills
     * @param dateTo   date, mills
     */
    public static MonitorMeReportGet newMonitorMeReportGet(long dateFrom, long dateTo) {
        List<BasicNameValuePair> params = new ArrayList<>();
        if (dateFrom > 0) {
            params.add(new BasicNameValuePair(DATE_FROM, String.valueOf(dateFrom / android.text.format.DateUtils
                    .SECOND_IN_MILLIS)));
        }
        if (dateTo > 0) {
            params.add(new BasicNameValuePair(DATE_TO, String.valueOf(dateTo / android.text.format.DateUtils
                    .SECOND_IN_MILLIS)));
        }

        long currentTimeInSeconds = DateUtils.getDateNowInSeconds();
        params.add(new BasicNameValuePair(DATE_NOW, String.valueOf(currentTimeInSeconds)));

        return (MonitorMeReportGet) new MonitorMeReportGet().withParams(params);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }
}
