package com.babylon.model.check;

public class SymptomAnswer implements Comparable<SymptomAnswer> {

    public SymptomAnswer(int id, ViewState state, int parentId) {
        this.id = id;
        this.state = state;
        this.parentId = parentId;
    }

    private int parentId;
    private int id;
    private ViewState state;

    public ViewState getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public int getParentId() {
        return parentId;
    }

    @Override
    public String toString() {
        return String.format("%d:%d", id, state.getValue());
    }

    @Override
    public int compareTo(SymptomAnswer another) {
        if (another.id > id) {
            return -1;
        } else if (another.id < id) {
            return 1;
        }
        return 0;
    }
}