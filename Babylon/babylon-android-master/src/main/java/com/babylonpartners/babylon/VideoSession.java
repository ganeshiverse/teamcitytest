package com.babylonpartners.babylon;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class VideoSession implements Publisher.PublisherListener, Subscriber.VideoListener, Session.SessionListener {

    public interface Listener {
        void sessionDroppedStream();
        void videoSessionEnded();
        void subscriberConnected();
    }

    private static final String LOGTAG = "Babylon-VideoSession";
    private static final String API_KEY = "44638112";

    private Publisher publisher;
    private Subscriber subscriber;
    private List<Stream> streams;

    private Activity hostActivity;
    private Session session;
    private RelativeLayout publisherView;
    private RelativeLayout subscriberView;

    private boolean resumeHasRun = false;

    protected Handler handler = new Handler();

    //With getters & setters
    private ViewGroup targetView;
    private VideoSession.Listener delegate;

    public VideoSession(Activity hostActivity, Listener delegate, String sessionId, String token, ViewGroup targetView) {
        streams = new ArrayList<Stream>();
        this.hostActivity = hostActivity;
        this.targetView = targetView;
        this.targetView.setKeepScreenOn(true);
        this.setupPositioning();
        this.delegate = delegate;
        sessionConnect(sessionId, token);
    }

    public void switchCamera() {
        if (publisher != null) {
            publisher.swapCamera();
        }
    }

    public void onPause() {
        if (session != null) {
            session.onPause();

            if (subscriber != null) {
                subscriberView.removeView(subscriber.getView());
            }
        }
    }

    public void onResume() {
        if (!resumeHasRun) {
            resumeHasRun = true;
            return;
        } else {
            if (session != null) {
                session.onResume();
            }
        }

        reloadInterface();
    }

    public void onStop() {
        if(session != null) {
            session.disconnect();
        }
    }

    public void onDestroy() {
        if (session != null) {
            session.disconnect();
        }

        restartAudioMode();
    }

    public void reloadInterface() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (subscriber != null) {
                    attachSubscriberView(subscriber);
                }
            }
        }, 500);
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager)hostActivity.getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        hostActivity.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    private void sessionConnect(String sessionId, String token) {
        if (session == null) {
            session = new Session(hostActivity, API_KEY, sessionId);
            session.setSessionListener(this);
            session.connect(token);
        }
    }

    private void subscribeToStream(Stream stream) {
        subscriber = new Subscriber(hostActivity, stream);
        subscriber.setVideoListener(this);
        session.subscribe(subscriber);
    }

    private void unsubscribeFromStream(Stream stream) {
        streams.remove(stream);
        if (subscriber.getStream().equals(stream)) {
            subscriberView.removeView(subscriber.getView());
            subscriber = null;
            if (!streams.isEmpty()) {
                subscribeToStream(streams.get(0));
            }
        }
    }

    private void attachSubscriberView(Subscriber subscriber) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                hostActivity.getResources().getDisplayMetrics().widthPixels, hostActivity.getResources()
                .getDisplayMetrics().heightPixels);
        subscriberView.addView(this.subscriber.getView(), layoutParams);
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
    }

    private void attachPublisherView(Publisher publisher) {
        this.publisher.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(240, 320);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        layoutParams.bottomMargin = dpToPx(10);
        layoutParams.rightMargin = dpToPx(10);
        publisherView.addView(this.publisher.getView(), layoutParams);
    }



    private void setupPositioning() {
        this.publisherView = new RelativeLayout(hostActivity);
        this.subscriberView = new RelativeLayout(hostActivity);

        this.targetView.addView(subscriberView, 0);
        this.targetView.addView(publisherView, 1);
    }

    private void showPoorInternetAlert() {
        new AlertDialog.Builder(hostActivity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Babylon")
                .setMessage("Your current internet connection is not fast enough to support Video. The consultant is aware and will contact you by phone.")
                .setPositiveButton("OK", null)
                .show();
    }

    public void finishVideoConfirmation() {
        new AlertDialog.Builder(hostActivity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Babylon")
                .setMessage("Are you sure you wish to stop the video session?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishVideo();
                        dialog.cancel();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    public void finishVideo()
    {
        //Mix Panel event for creating family account
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(hostActivity,props,null);
        helper.sendEvent(MixPanelEventConstants.VIDEO_APPOINTMENT_ENDED);

        targetView.setVisibility(View.INVISIBLE);
        delegate.videoSessionEnded();
        session.disconnect();
        this.targetView.setKeepScreenOn(false);
    }

    public void setTargetView(ViewGroup targetView) {
        this.targetView = targetView;
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        showPoorInternetAlert();
    }

    @Override
    public void onConnected(Session session) {
        Log.i(LOGTAG, "Connected to the session.");
        if (publisher == null) {
            publisher = new Publisher(hostActivity, "publisher");
            publisher.setPublisherListener(this);
            attachPublisherView(publisher);
            session.publish(publisher);
        }
    }

    @Override
    public void onDisconnected(Session session) {
        if (publisher != null) {
            publisherView.removeView(publisher.getView());
        }

        if (subscriber != null) {
            subscriberView.removeView(subscriber.getView());
        }

        publisher = null;
        subscriber = null;
        streams.clear();
        this.session = null;
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        boolean isMyStream = session.getConnection().equals(stream.getConnection());

        if (!isMyStream) {
            streams.add(stream);

            if (subscriber == null) {
                subscribeToStream(stream);
            }
        }
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        delegate.sessionDroppedStream();
        unsubscribeFromStream(stream);
    }

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        showPoorInternetAlert();
    }

    @Override
    public void onVideoDataReceived(SubscriberKit subscriberKit) {
        attachSubscriberView(subscriber);
    }

    @Override
    public void onVideoDisabled(SubscriberKit subscriberKit, String s) {

    }

    @Override
    public void onVideoEnabled(SubscriberKit subscriberKit, String s) {

    }

    @Override
    public void onVideoDisableWarning(SubscriberKit subscriberKit) {

    }

    @Override
    public void onVideoDisableWarningLifted(SubscriberKit subscriberKit) {

    }

    /**
     * Converts dp to real pixels, according to the screen density.
     *
     * @param dp A number of density-independent pixels.
     * @return The equivalent number of real pixels.
     */
    private int dpToPx(int dp) {
        double screenDensity = hostActivity.getResources().getDisplayMetrics().density;
        return (int) (screenDensity * (double) dp);
    }
}
