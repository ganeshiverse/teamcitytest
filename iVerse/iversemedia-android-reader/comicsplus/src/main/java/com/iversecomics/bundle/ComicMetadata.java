package com.iversecomics.bundle;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class ComicMetadata {
    // Status indicator for an OK comic (no problems, and belongs to you)
    public static final int STATUS_OK = 1;
    // Status indicator for a BAD comic (malformed, incomplete, etc...)
    public static final int STATUS_BAD = 2;
    // Status indicator for a comic that does not belong to you (failed registration check)
    public static final int STATUS_NOTYOURS = 3;
    // Status indicator for a Comic Parsing Lib issue (malformed, incomplete, etc...)
    public static final int STATUS_LIB_FAILURE = 4;

    private String filename;
    private long filesize;
    private String id;
    private String name;
    private int status = STATUS_OK;
    private String seriesId;
    private String publisherId;
    private String publisherName;
    private String publisherURL;
    private String publishDate;
    private String description;
    private String synopsis;
    private List<String> categories = new ArrayList<String>();
    private String productType;
    private List<String> artists = new ArrayList<String>();
    private List<String> writers = new ArrayList<String>();
    private String copyright;
    private String registeredTo;
    private Calendar generatedDate;
    private String generatedBy;
    private short mangaOrientation;
    private byte[] expectedHash;
    private boolean waidFormat;

    public byte[] getExpectedHash() {
        return expectedHash;
    }

    public void setExpectedHash(byte[] hash) {
        expectedHash = hash;
    }

    public short getMangaOrientation() {
        return this.mangaOrientation;
    }

    public void setMangaOrientation(short orientation) {
        this.mangaOrientation = orientation;
    }

    public void addArtist(String artist) {
        this.artists.add(artist);
    }

    public void addCategory(String category) {
        this.categories.add(category);
    }

    public void addWriter(String writer) {
        this.writers.add(writer);
    }

    public List<String> getArtists() {
        return artists;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getCopyright() {
        return copyright;
    }

    public String getDescription() {
        return description;
    }

    public String getFilename() {
        return filename;
    }

    public long getFilesize() {
        return filesize;
    }

    public String getGeneratedBy() {
        return generatedBy;
    }

    public Calendar getGeneratedDate() {
        if (this.generatedDate != null) {
            return this.generatedDate;
        }

        return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProductType() {
        return productType;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public String getPublisherURL() {
        return publisherURL;
    }

    public String getRegisteredTo() {
        return registeredTo;
    }

    public String getSeriesId() {
        return seriesId;
    }

    public int getStatus() {
        return status;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public List<String> getWriters() {
        return writers;
    }

    public void setArtists(List<String> artists) {
        this.artists = artists;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFilesize(long filesize) {
        this.filesize = filesize;
    }

    public void setGeneratedBy(String generatedBy) {
        this.generatedBy = generatedBy;
    }

    public void setGeneratedDate(Calendar generatedDate) {
        this.generatedDate = generatedDate;
        this.generatedDate.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public void setPublisherURL(String publisherURL) {
        this.publisherURL = publisherURL;
    }

    public void setRegisteredTo(String registeredTo) {
        this.registeredTo = registeredTo;
    }

    public void setSeriesId(String seriesId) {
        this.seriesId = seriesId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public void setWriters(List<String> writers) {
        this.writers = writers;
    }

    public boolean isWaidFormat() {
        return waidFormat;
    }
    public void setWaidFormat(boolean waidFormat) {
        this.waidFormat = waidFormat;
    }



}
