package com.babylon.utils;

import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;

public class WebViewJsManager {

    private List<String> pendingJsCalls = new ArrayList<>();
    private boolean isRequestsPaused = false;
    private WebView webView;

    public WebViewJsManager(WebView webView) {
        this.webView = webView;
    }

    public void pauseJsCalls() {
        isRequestsPaused = true;
    }

    public void resumeJsCalls() {
        isRequestsPaused = false;
        for (String request : pendingJsCalls) {
            callJs(request);
        }
        pendingJsCalls.clear();
    }

    public void callJs(String js) {
        if (!isRequestsPaused) {
            String url = String.format("javascript:( function () {%s} ) ()", js);
            webView.loadUrl(url);
        } else {
            pendingJsCalls.add(js);
        }
    }

    public void startWebSpinner() {
        callJs("Babylon.Vent.trigger('page:loading')");
    }

    public void stopWebSpinner() {
        callJs("Babylon.Vent.trigger('page:loaded')");
    }
}
