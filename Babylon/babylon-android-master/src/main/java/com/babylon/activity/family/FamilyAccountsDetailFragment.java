package com.babylon.activity.family;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.babylon.R;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.family.AccountAuthenticatePost;
import com.babylon.api.request.family.AccountPatch;
import com.babylon.api.request.family.AccountPost;
import com.babylon.fragment.BaseFragment;
import com.babylon.images.ImageLoader;
import com.babylon.model.Account;
import com.babylon.model.Patient;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.CommonHelper;
import com.babylon.view.TopBarInterface;
import com.babylonpartners.babylon.CircleTransform;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class FamilyAccountsDetailFragment extends BaseFragment {

    public static final String TAG = FamilyAccountsDetailFragment.class.getSimpleName();
    private static final String ACCOUNT_BUNDLE_KEY = "account";
    private static final String FIRST_NAME_FIELD = "first_name";
    private static final String LAST_NAME_FIELD = "last_name";
    private static final String DATE_OF_BIRTH_FIELD = "date_of_birth";
    private static final String GENDER_FIELD = "gender";
    private static final String EMAIL_FIELD = "email";
    private static int MIN_AGE =16;
    private Date birthday;
    private  TextView dobTextView;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    public interface FamilyAccountDetailListener {
        void onImagePickerPressed();
        void onSwitchAccountPressed(Account account);
        void onAccountAdded(Patient newPatient);
    }

    private FamilyAccountDetailListener listener;

    private Account account;

    private EditText firstNameField;
    private EditText lastNameField;
    private EditText emailField;
    private ImageView avatarImageView;
    private Button genderButton;
   // private BirthdayViewController dobViewController;
    private ImageButton switchAccountsButton;
    private TextView switchAccountsTextView;
    private String mErrorMessage;
    private static final String[] accountGenders = { "male", "female" };
    private HashMap<String, String> gendersMap = new HashMap<String, String>();
    private String selectedGender = null;
    private List<String> fieldsToPatch = new ArrayList<String>();

    public static FamilyAccountsDetailFragment newInstance(Account account) {
        FamilyAccountsDetailFragment fragment = new FamilyAccountsDetailFragment();
        Bundle args = new Bundle();
        Gson gson = new Gson();
        args.putString(ACCOUNT_BUNDLE_KEY, gson.toJson(account, Account.class));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_family_account_detail, container, false);
        initFields(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myCalendar = Calendar.getInstance();
        Gson gson = new Gson();
        Bundle arguments = getArguments();
        if (arguments != null) {
            String accountJson = getArguments().getString(ACCOUNT_BUNDLE_KEY);
            account = gson.fromJson(accountJson, Account.class);
            /*if (!account.getIsMinor()) {
                switchAccountsButton.setVisibility(View.GONE);
                switchAccountsTextView.setVisibility(View.GONE);
            } else {
                switchAccountsButton.setOnClickListener(onSwitchAccountClickListener);
            }*/
            disableFields();
            setRightButton(onEditPressedListener, R.string.family_details_edit);
        } else {
            account = new Account();
            /*this.switchAccountsButton.setVisibility(View.GONE);
            this.switchAccountsTextView.setVisibility(View.GONE);*/
            setRightButton(onDonePressedListener, R.string.family_details_done);
        }
        String[] localisedGenders = getResources().getStringArray(R.array.genders);
        for (int i = 0; i < accountGenders.length; ++i) {
            gendersMap.put(accountGenders[i], localisedGenders[i]);
        }

        populateFieldsWithAccount(view);

        setTitle(getString(R.string.family_detail_fragment_title));
    }

    @Override
    public void onStop()
    {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(firstNameField.getWindowToken(), 0);
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (FamilyAccountDetailListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FamilyAccountsDetailFragment.Listener");
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    private void initFields(View rootView) {
        firstNameField = (EditText)rootView.findViewById(R.id.first_name);
        lastNameField = (EditText)rootView.findViewById(R.id.last_name);
        emailField = (EditText)rootView.findViewById(R.id.email_field);
        genderButton = (Button)rootView.findViewById(R.id.gender_button);
        avatarImageView = (ImageView)rootView.findViewById(R.id.avatar_image_view);
/*
        switchAccountsButton = (ImageButton)rootView.findViewById(R.id.switch_account_btn);
        switchAccountsTextView = (TextView)rootView.findViewById(R.id.switch_account_text_view);*/
        dobTextView = (TextView)rootView.findViewById(R.id.date_of_birth);
        avatarImageView.setOnClickListener(onAvatarClickListener);

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                birthday = myCalendar.getTime();
                updateLabel();
            }

        };
        dobTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        dobTextView.setTextColor(Color.WHITE);
        dobTextView.setText(sdf.format(birthday));
    }


    private void populateFieldsWithAccount(View rootView) {
        if (account.getFirstName() != null) {
            firstNameField.setText(account.getFirstName());
        }
        if (account.getLastName() != null) {
            lastNameField.setText(account.getLastName());
        }
        if (account.getDateOfBirth() != null) {
            birthday=account.getDateOfBirthAsDate();
            updateLabel();
           // dobViewController.setDate(account.getDateOfBirthAsDate());
        }
        if (account.getGender() != null) {
            String localisedGender = gendersMap.get(account.getGender().toLowerCase());
            genderButton.setText(localisedGender);
            selectedGender = account.getGender();
        }
        if (account.getEmail() != null) {
            emailField.setText(account.getEmail());
        }
        if (account.getAvatar() != null) {
            byte[] decodedString = Base64.decode(account.getAvatar(), Base64.DEFAULT);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            CircleTransform xform = new CircleTransform();
            Bitmap transformedBitmap = xform.transform(decodedBitmap);
            decodedBitmap.recycle();
            avatarImageView.setImageBitmap(transformedBitmap);
        }

        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.genders, android.R.layout.simple_spinner_dropdown_item);
        genderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(FamilyAccountsDetailFragment.this.getActivity())
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                genderButton.setText(adapter.getItem(which).toString());
                                selectedGender = accountGenders[which];
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    private void saveFieldsToAccount()  {
        if (account.getFirstName() == null || !account.getFirstName().equals(firstNameField.getText().toString())) {
            fieldsToPatch.add(FIRST_NAME_FIELD);
            account.setFirstName(firstNameField.getText().toString());
        }
        if (account.getLastName() == null || !account.getLastName().equals(lastNameField.getText().toString())) {
            fieldsToPatch.add(LAST_NAME_FIELD);
            account.setLastName(lastNameField.getText().toString());
        }
      //  if (account.getDateOfBirth() == null || account.getDateOfBirthAsDate().compareTo(dobViewController.getCurrentDate()) != 0) {
        if (account.getDateOfBirth() == null || account.getDateOfBirthAsDate().compareTo(birthday) != 0) {
            fieldsToPatch.add(DATE_OF_BIRTH_FIELD);
            account.setDateOfBirth(birthday);
          //  account.setDateOfBirth(dobViewController.getCurrentDate());
        }
        if (account.getGender() == null || !account.getGender().equals(selectedGender)) {
            fieldsToPatch.add(GENDER_FIELD);
            account.setGender(selectedGender);
        }
        if (emailField.getText().length() > 0 && (account.getEmail() == null || !account.getEmail().equals(emailField.getText().toString()))) {
            fieldsToPatch.add(EMAIL_FIELD);
            account.setEmail(emailField.getText().toString());
        }
    }

    private void disableFields() {
        firstNameField.setEnabled(false);
        lastNameField.setEnabled(false);
        emailField.setEnabled(false);
        dobTextView.setEnabled(false);
        //dobViewController.setEnabled(false);
        genderButton.setEnabled(false);
        avatarImageView.setEnabled(false);
    }

    private void enableFields() {
        firstNameField.setEnabled(true);
        firstNameField.setFocusableInTouchMode(true);
        firstNameField.requestFocus();
        firstNameField.setSelection(firstNameField.getText().length());
        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(firstNameField, InputMethodManager.SHOW_IMPLICIT);

        lastNameField.setEnabled(true);
        emailField.setEnabled(true);
        dobTextView.setEnabled(true);
       // dobViewController.setEnabled(true);
        genderButton.setEnabled(true);
        avatarImageView.setEnabled(true);
    }

    public static float getAge(final Date birthdate) {
        return getAge(Calendar.getInstance().getTime(), birthdate);
    }


    public static float getAge(final Date current, final Date birthdate) {

        if (birthdate == null) {
            return 0;
        }
        if (current == null) {
            return getAge(birthdate);
        } else {
            final Calendar calend = new GregorianCalendar();
            calend.set(Calendar.HOUR_OF_DAY, 0);
            calend.set(Calendar.MINUTE, 0);
            calend.set(Calendar.SECOND, 0);
            calend.set(Calendar.MILLISECOND, 0);

            calend.setTimeInMillis(current.getTime() - birthdate.getTime());

            float result = 0;
            result = calend.get(Calendar.YEAR) - 1970;
            result += (float) calend.get(Calendar.MONTH) / (float) 12;
            return result;
        }

    }



    private boolean validateFields()
    {
        float Age;
        Date date;

        if (firstNameField.getText().length() == 0) {
            firstNameField.setError(getString(R.string.firstname_empty));
            return false;
        }
        if (lastNameField.getText().length() == 0) {
            lastNameField.setError(getString(R.string.lastname_empty));
            return false;
        }
        if (selectedGender == null) {
            return false;
        }
       // if(dobViewController!=null && dobViewController.getCurrentDate()==null)
        if(birthday==null)
        {
            return false;
        }
        else
        {
           // date=(dobViewController.getCurrentDate());
            date=(birthday);
            Calendar c = Calendar.getInstance();
            Age= getAge ( c.getTime(),date);

        }

        if( Age < 0){

            mErrorMessage=getString(R.string.dob_error);
                //emailField.setError();
                return false;

        }
        //if(Age> MIN_AGE )
       // {
            if (TextUtils.isEmpty(emailField.getText()))
            {
                if(Age > MIN_AGE)
                {
                    emailField.setError(getResources().getString(R.string.email_empty));
                    return false;
                }
            }
            else if(!CommonHelper.isEmailValid(emailField.getText()))
            {
                emailField.setError(getResources().getString(R.string.email_not_valid));
                return false;
            }
        //}


        return true;
    }

    public void notifyAvatarImageChanged(final String newImagePath) {
        setProgressBarVisible(true);
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void[] objects) {
                final Bitmap bitmap = ImageLoader.getInstance().getBitmapByPath(newImagePath, ImageLoader.BIG_IMAGE);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                fieldsToPatch.add("avatar");
                account.setAvatar(encoded);
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                avatarImageView.setImageBitmap(result);
                setProgressBarVisible(false);
            }
        }.execute((Void)null);
    }

    private void displayErrorMessage(Response response) {
        final String userResponseError = response.getUserResponseError();
        if (!TextUtils.isEmpty(userResponseError)) {
            AlertDialogHelper.getInstance().showMessage(getActivity(), userResponseError);
        }
    }

    private void displayValidationError() {
        if(TextUtils.isEmpty(mErrorMessage)) {
            AlertDialogHelper.getInstance().showMessage(getActivity(), getString(R.string.family_validation_error));
        }
        else {
            AlertDialogHelper.getInstance().showMessage(getActivity(),mErrorMessage);
            mErrorMessage=null;
        }
    }

    private final View.OnClickListener onEditPressedListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            enableFields();
            setRightButton(onSavePressedListener, R.string.family_details_save);
        }
    };

    private final View.OnClickListener onSavePressedListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validateFields()) {

                saveFieldsToAccount();

                disableFields();
                if (fieldsToPatch.size() > 0) {
                    setProgressBarVisible(true);
                    setRightButtonEnabled(false, ORANGE_THEME);
                    setRightButton(onEditPressedListener, R.string.family_details_edit);
                    new AccountPatch().withEntry(account, fieldsToPatch.toArray(new String[fieldsToPatch.size()]))
                            .execute(getActivity(), accountPatchRequestListener);
                }
                else
                {
                    setRightButton(onEditPressedListener, R.string.family_details_edit);
                }
            } else {
                displayValidationError();
            }
        }
    };

    private final View.OnClickListener onDonePressedListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validateFields()) {
                setProgressBarVisible(true);
                disableFields();
                setRightButtonEnabled(false, ORANGE_THEME);

                    saveFieldsToAccount();

                new AccountPost().withEntry(account).execute(getActivity(), accountPostRequestListener);
            } else {
                displayValidationError();
            }
        }
    };

    private final RequestListener<Account> accountPatchRequestListener = new
            RequestListenerErrorProcessed<Account>() {
                @Override
                public void onRequestComplete(Response<Account> response, String errorMessage) {
                    setProgressBarVisible(false);
                    setRightButtonEnabled(true, ORANGE_THEME);
                    if (!response.isSuccess()) {
                        displayErrorMessage(response);
                    } else {
                        fieldsToPatch.clear();
                    }
                }
            };

    private final RequestListener<AccountPost.AccountResponse> accountPostRequestListener = new
            RequestListenerErrorProcessed<AccountPost.AccountResponse>() {
                @Override
                public void onRequestComplete(Response<AccountPost.AccountResponse> response, String errorMessage) {
                    setProgressBarVisible(false);
                    enableFields();
                    setRightButtonEnabled(true, ORANGE_THEME);
                    if (response.isSuccess()) {
                        listener.onAccountAdded(response.getData().patient);
                    } else {
                        displayErrorMessage(response);
                    }
                }
            };

    private final View.OnClickListener onAvatarClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onImagePickerPressed();
                    }
                }
            };

    private final View.OnClickListener onSwitchAccountClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setProgressBarVisible(true);
                    new AccountAuthenticatePost().withEntry(account).execute(getActivity(), accountAuthenticatePostRequestListener);

                }
            };

    private final RequestListener<Account> accountAuthenticatePostRequestListener = new
            RequestListenerErrorProcessed<Account>() {
                @Override
                public void onRequestComplete(Response<Account> response, String errorMessage) {
                    setProgressBarVisible(false);
                    if (response.isSuccess()) {
                        Account account = response.getData();
                        listener.onSwitchAccountPressed(account);
                    } else {
                        displayErrorMessage(response);
                    }
                }
            };
}
