package com.babylon.chart;

import com.babylon.App;
import com.babylon.BaseConfig;

import java.io.File;


public class FileCache {

    private File cacheDir;

    private static FileCache fileCache;

    public static FileCache getInstance() {
        if( fileCache ==null ) {
            fileCache = new FileCache();
        }
        return fileCache;
    }

    public FileCache() {
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir = new File(BaseConfig.CACHE_DIR, "webpages");
        } else {
            cacheDir = App.getInstance().getCacheDir();
        }

        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }

    public File getFile(String url) {
        //I identify page by hashcode. Not a perfect solution, good for the demo.
        String filename = String.valueOf(url.hashCode());
        File f = new File(cacheDir, filename);
        return f;

    }

    public File getCacheDir(){
        return cacheDir;
    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        for (File f : files)
            f.delete();
    }

}