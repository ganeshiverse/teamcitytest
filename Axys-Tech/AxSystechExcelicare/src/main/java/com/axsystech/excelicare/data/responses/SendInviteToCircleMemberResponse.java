package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 20/08/15.
 */
public class SendInviteToCircleMemberResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private String data;

    public String getData() {
        return data;
    }
}
