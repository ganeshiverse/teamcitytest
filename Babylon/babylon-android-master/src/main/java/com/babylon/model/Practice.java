package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class Practice {
    private String address;
    @SerializedName("cqc_number")
    private String cqcNumber;
    private String email;
    private Integer id;
    private String latitude;
    private String longitude;
    private String name;
    @SerializedName("telephone_number")
    private String telNumber;
    @SerializedName("babylon_partner")
    private Boolean babylonPartner;

    public String getAddress() { return address; }

    public String getCqcNumber() { return cqcNumber; }

    public String getEmail() { return email; }

    public Integer getId() { return id; }

    public Float getLatitude() { return Float.parseFloat(latitude); }

    public Float getLongitude() { return Float.parseFloat(longitude); }

    public String getName() { return name; }

    public String getTelNumber() { return telNumber; }

    public Boolean isBabylonPartner() { return babylonPartner; }
}
