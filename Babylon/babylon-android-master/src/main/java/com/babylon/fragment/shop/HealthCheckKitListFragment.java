package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.android.volley.RequestQueue;
import com.babylon.R;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.adapter.shop.HealthKitCheckListAdapter;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Kit.OrderProducts;
import com.babylon.model.Kit.Product;
import com.babylon.view.TopBarInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class HealthCheckKitListFragment extends BaseFragment implements AbsListView.OnItemClickListener {

    public static final String TAG = HealthCheckKitListFragment.class.getSimpleName();
    private  RequestQueue mQueue;
    private static final String PRODUCT_BUNDLE_KEY = "product";
    private static final String CAT_NAME_KEY = "category_name";
    private static final String ORDERED_TAG="ordered";
    private static final String NOT_ORDERED_TAG="not_ordered";
    private int count_ordered_items=0;
    public interface HealthCheckKitFragmentListener {
        public void onKitListItemClicked(Product product);
        public void onCategoriesHelpClicked();
        public void onMoreDetailsClicked(String details);
        public void onOrderClicked();
    }

    private ListView listView;
    private HealthKitCheckListAdapter adapter;
    private List<Product> products = new ArrayList<Product>();
    private HealthCheckKitFragmentListener listener;
    private TextView tvOrder;
    private RelativeLayout orderLayout;
    private Button btnOrderAll;
    private String title;

    public static HealthCheckKitListFragment newInstance(List<Product> products,String categoryName) {
        HealthCheckKitListFragment fragment = new HealthCheckKitListFragment();
        Bundle args = new Bundle();
        Gson gson = new Gson();
        Type listOfTestObject = new TypeToken<List<Product>>(){}.getType();
        String productsJson = gson.toJson(products, listOfTestObject);
        args.putString(PRODUCT_BUNDLE_KEY, productsJson);
        args.putString(CAT_NAME_KEY,categoryName);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_kit_product_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setProgressBarVisible(true);
        setTitle(getString(R.string.kit_list_fragment_title));

        listView = (ListView)view.findViewById(android.R.id.list);
        tvOrder=(TextView)view.findViewById(R.id.tvOrder);
        orderLayout=(RelativeLayout)view.findViewById(R.id.rlayout);
        btnOrderAll=(Button)view.findViewById((R.id.btnOrderAll));
        listView.setOnItemClickListener(this);
        updateOrderTV();
        Gson gson = new Gson();
        Bundle arguments = getArguments();
        if (arguments != null) {
            String productJson = getArguments().getString(PRODUCT_BUNDLE_KEY);
            title=getArguments().getString(CAT_NAME_KEY);
            setTitle(title);
            Type collectionType = new TypeToken<List<Product>>() { }.getType();
            products = gson.fromJson(productJson, collectionType);
            adapter = new HealthKitCheckListAdapter(getActivity(), products,onPropertyItemClickListener,((ShopCategoriesActivity) getActivity()).mOrderProductList);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            setProgressBarVisible(false);
            updateOrderTV();
        }

        btnOrderAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {

                    listener.onOrderClicked();

                }
            }
        });

    }

    private final View.OnClickListener onItemClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {

                        Button btnOrder=(Button)view;
                        String btnTag= btnOrder.getTag().toString();
                        String[] parts = btnTag.split("-");
                        String Id = parts[0];
                        String OrderTag = parts[1];
                        //If product is ordered change button image and tag
                        if(OrderTag.compareToIgnoreCase(NOT_ORDERED_TAG)==0) {
                            btnOrder.setBackgroundResource(R.drawable.btn_product_order);
                            btnOrder.setText("");
                            btnOrder.setTag(Id + "-" + ORDERED_TAG);
                            count_ordered_items++;
                            OrderProducts product=new OrderProducts(Integer.valueOf(Id));
                            ((ShopCategoriesActivity) getActivity()).mOrderProductList.add(product);
                            updateOrderTV();


                        }
                        else{
                            btnOrder.setBackgroundResource(R.drawable.circle);
                            btnOrder.setTag(Id + "-" + NOT_ORDERED_TAG);
                            count_ordered_items--;
                            btnOrder.setText(getString(R.string.btn_order));
                            OrderProducts product=new OrderProducts(Integer.valueOf(Id));
                            ((ShopCategoriesActivity) getActivity()).mOrderProductList.remove(product);
                            updateOrderTV();
                        }
                    }
                }
            };

    private final View.OnClickListener onMoreItemClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        if(view.getTag()!=null) {
                            listener.onMoreDetailsClicked(view.getTag().toString());

                        }
                    }
                }
            };

    private final View.OnClickListener onOrderClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        if(view.getTag()!=null) {
                            listener.onOrderClicked();

                        }
                    }
                }
            };




    private void updateOrderTV() {
        count_ordered_items= ((ShopCategoriesActivity) getActivity()).mOrderProductList.size();
        if (count_ordered_items>0) {
            String orderText = String.valueOf(count_ordered_items) + getString(R.string.order_count);
            tvOrder.setText(orderText);
            btnOrderAll.setVisibility(View.VISIBLE);
        }
        else {
            tvOrder.setText(getString(R.string.order_tv_title));
            btnOrderAll.setVisibility(View.GONE);

        }


    }

    private HealthKitCheckListAdapter.OnPropertyItemClickListener onPropertyItemClickListener =
            new HealthKitCheckListAdapter.OnPropertyItemClickListener() {

                @Override
                public View.OnClickListener getOnPropertyClickListener() {
                    return onItemClickListener;
                }

                @Override
                public View.OnClickListener getOnMoreDetailClickListener() {
                    return onMoreItemClickListener;
                }




            };





    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (HealthCheckKitFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PINK_THEME;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (listener != null) {
            listener.onKitListItemClicked(adapter.getItem(position));
        }
    }



}