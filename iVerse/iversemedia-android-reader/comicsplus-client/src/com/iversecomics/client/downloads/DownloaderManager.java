package com.iversecomics.client.downloads;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.iversecomics.client.downloads.internal.data.DownloadData;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DownloaderManager {
	private static final Logger LOG = LoggerFactory.getLogger(Request.class);    	

	public static class Request implements Comparable<Request> {
        @SuppressWarnings("unused")
        private static final String TAG                  = Request.class.getSimpleName();
        private Uri                 uri;
        private File                localFile;
        private String              title;
        // private boolean             overwriteDestination = false;
        private Uri                 referenceContentUri;
        private String 			payload;

        public Request(Uri uri) {
            if (uri == null) {
                throw new NullPointerException("uri cannot be null");
            }
            String scheme = uri.getScheme();
            if (!("http".equals(scheme)) && !("https".equals(scheme))) {
                throw new IllegalArgumentException("Can only download http or https URIs: " + uri);
            }
            this.uri = uri;
            this.title = uri.getLastPathSegment();
        }

        @Override
        public int compareTo(Request o) {
            return this.uri.compareTo(o.uri);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Request other = (Request) obj;
            if (uri == null) {
                if (other.uri != null) {
                    return false;
                }
            } else if (!uri.equals(other.uri)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((uri == null) ? 0 : uri.hashCode());	
            return result;
        }

        public void setVerifyPurchasePayload(final String payload)
        {
        	this.payload = payload;
        }
        
        private void setDestinationFromBase(File base, String subPath) {
            if (subPath == null) {
                throw new NullPointerException("subPath cannot be null");
            }
            if (base == null) {
                throw new NullPointerException("File base cannot be null");
            }
            setDestinationUri(Uri.withAppendedPath(Uri.fromFile(base), subPath));
            
        }

        public Request setDestinationInExternalFilesDir(Context context, String dirType, String subPath) {
            setDestinationFromBase(context.getExternalFilesDir(dirType), subPath);
            return this;
        }

        public Request setDestinationInExternalPublicDir(String dirType, String subPath) {
            setDestinationFromBase(Environment.getExternalStoragePublicDirectory(dirType), subPath);
            return this;
        }

        public Request setDestinationUri(Uri destUri) {
            if (!"file".equals(destUri.getScheme())) {
                throw new IllegalArgumentException("Can only use file:// Uris for the destination Uri");
            }
            this.localFile = new File(destUri.getPath());
            LOG.debug("Downloading to file %s", this.localFile);
            return this;
        }

        /*
        public Request setOverwriteDestination(boolean b) {
            this.overwriteDestination = b;
            return this;
        }
        */

        /**
         * Optional reference to the contentUri that this downloader will track and report back to the DOWNLOAD_COMPLETE
         * action on completion of the download.
         */
        public void setReferenceContentUri(Uri contentUri) {
            this.referenceContentUri = contentUri;
        }

        public Request setTitle(String title) {
            this.title = title;
            return this;
        }

        @Override
        public String toString() {
            return uri + " -> " + localFile;
        }
    }

    public static DownloaderManager INSTANCE = new DownloaderManager();

    public static DownloaderManager getInstance() {
        return INSTANCE;
    }

    /**
     * The buffer size used to stream the data
     * <p>
     * Default: 128k
     */
    private int                   bufferSize                  = 1024*128;

    /**
     * The minimum amount of progress that has to be done before the progress bar gets updated
     * <p>
     * Default: 8k
     */
    private int                   notificationMinProgressStep = 1024;

    /**
     * The minimum amount of time that has to elapse before the progress bar gets updated, in ms.
     * <p>
     * Default: 1500ms
     */
    private long                  notificationMinProgressTime = 500;

    /**
     * The number of times that the download manager will retry its network operations when no progress is happening
     * before it gives up.
     * <p>
     * Default: 5
     */
    private int                   retriesMax                  = 5;

    /**
     * The minimum amount of time that the download manager accepts for a Retry-After response header with a parameter
     * in delta-seconds.
     * <p>
     * Default: 30 seconds
     */
    private int                   retriesMinAfter             = 30;

    /**
     * The maximum amount of time that the download manager accepts for a Retry-After response header with a parameter
     * in delta-seconds.
     * <p>
     * Default: 24 hours
     */
    private int                   retriesMaxAfter             = 24 * 60 * 60;

    /**
     * The maximum number of redirects.
     * <p>
     * Default: 5 (can't be more than 7 due to apache http client limitation)
     */
    private int                   redirectsMax                = 5;

    /**
     * The time between a failure and the first retry after an IOException. Each subsequent retry grows exponentially,
     * doubling each time. The time is in seconds.
     * <p>
     * Default: 30 seconds
     */
    private int                   retriesDelay                = 30;
    /**
     * The max simultaneous downloads.
     * <p>
     * Default: 1 active downloads
     */
    private int                   maxSimultaneous             = 1;
    private String                userAgent;

    private Context               context;

    private List<DownloadMonitor> monitors                    = new ArrayList<DownloadMonitor>();

    private DownloaderManager() {
        /* default constructor */
    }

    public void addDownloadMonitor(DownloadMonitor monitor) {
        this.monitors.add(monitor);
    }

    public void enqueue(Request request, String coverImageFileName) {
        DownloadData download = new DownloadData();
        download.setUri(request.uri);
        download.setLocalFile(request.localFile);
        download.setName(request.title);
        download.setStatus(DownloadStatus.PENDING);
        download.setTimestampRetryAfter(0); // make sure we start.
        download.setReferenceContentUri(request.referenceContentUri);
        download.setVerifyPurchasePayload(request.payload);
        download.setNumFailed(0);
        if (coverImageFileName != null)
            download.setCoverImageFileName(coverImageFileName);

        Intent intent = new Intent();
        intent.setPackage(context.getPackageName());
        intent.setAction(DownloaderConstants.ACTION_DOWNLOAD_ADDED);
        intent.setData(request.uri);
        intent.putExtra("download.type", "parcel");
        intent.putExtra("download", download);
        intent.putExtra("userAgent", this.userAgent);
        
        LOG.debug("Download enqueued for file %s for %s", download.getLocalFile(), download.getName());
        
        /*
         * pending download will take care of overwriting the file
         * This causes issues
        if ((request.overwriteDestination) && (request.localFile.exists())) {
            request.localFile.delete();
        }
        */

        this.context.sendBroadcast(intent);
        //LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public List<DownloadMonitor> getDownloadMonitors() {
        return monitors;
    }

    public int getMaxSimultaneous() {
        return maxSimultaneous;
    }

    public int getNotificationMinProgressStep() {
        return notificationMinProgressStep;
    }

    public long getNotificationMinProgressTime() {
        return notificationMinProgressTime;
    }

    public int getRedirectsMax() {
        return redirectsMax;
    }

    public int getRetriesDelay() {
        return retriesDelay;
    }

    public int getRetriesMax() {
        return retriesMax;
    }

    public int getRetriesMaxAfter() {
        return retriesMaxAfter;
    }

    public int getRetriesMinAfter() {
        return retriesMinAfter;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void init(Context context) {
        this.context = context;

        // Establish User Agent
        userAgent = AndroidInfo.getUserAgent(context);
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public void setDownloadMonitors(List<DownloadMonitor> monitors) {
        this.monitors.clear();
        if (monitors != null) {
            this.monitors.addAll(monitors);
        }
    }

    public void setMaxSimultaneous(int maxSimultaneous) {
        this.maxSimultaneous = maxSimultaneous;
    }

    public void setNotificationMinProgressStep(int notificationMinProgressStep) {
        this.notificationMinProgressStep = notificationMinProgressStep;
    }

    public void setNotificationMinProgressTime(long notificationMinProgressTime) {
        this.notificationMinProgressTime = notificationMinProgressTime;
    }

    public void setRedirectsMax(int redirectsMax) {
        this.redirectsMax = redirectsMax;
    }

    public void setRetriesDelay(int retriesDelay) {
        this.retriesDelay = retriesDelay;
    }

    public void setRetriesMax(int retriesMax) {
        this.retriesMax = retriesMax;
    }

    public void setRetriesMaxAfter(int retriesMaxAfter) {
        this.retriesMaxAfter = retriesMaxAfter;
    }

    public void setRetriesMinAfter(int retriesMinAfter) {
        this.retriesMinAfter = retriesMinAfter;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
