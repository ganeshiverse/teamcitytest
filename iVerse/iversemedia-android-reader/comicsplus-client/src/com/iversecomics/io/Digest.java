package com.iversecomics.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.iversecomics.util.text.Hex;

/**
 * Simple Digest methods.
 */
public final class Digest {
    public static final String  ALGO_MD5  = "MD5";
    public static final String  ALGO_SHA1 = "SHA1";

    public static String calc(String algorithm, File file) throws IOException {
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            return calc(algorithm, in);
        } finally {
            IOUtil.close(in);
        }
    }
    
    public static byte[] calc(String algorithm, File file, long length) throws IOException {
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            MessageDigest digest = getDigest(algorithm);
            IOUtil.copy(in, new DigestOutputStream(new NullOutputStream(), digest), length);
            return digest.digest();
        } finally {
            IOUtil.close(in);
        }
    }

    public static String calc(String algorithm, InputStream stream) throws IOException {
        MessageDigest digest = getDigest(algorithm);
        IOUtil.copy(stream, new DigestOutputStream(new NullOutputStream(), digest));
        return Hex.asHex(digest.digest());
    }

    public static String calc(String algorithm, String str) throws IOException {
        MessageDigest digest = getDigest(algorithm);
        return Hex.asHex(digest.digest(str.getBytes()));
    }

    private static MessageDigest getDigest(String algorithm) throws IOException
    {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            String err = String.format("Unable to calculate %s digest: %s: %s", algorithm, e.getClass().getName(), e
                    .getMessage());
            throw new IOException(err);
        }
    }

    public static String md5(File file) throws IOException {
        return calc(ALGO_MD5, file);
    }

    public static String md5(InputStream stream) throws IOException {
        return calc(ALGO_MD5, stream);
    }

    public static String md5(String str) throws IOException {
        return calc(ALGO_MD5, str);
    }

    public static String sha1(File file) throws IOException {
        return calc(ALGO_SHA1, file);
    }
    
    public static byte[] sha1(File file, long length) throws IOException {
    	return calc(ALGO_SHA1, file, length);
    }

    public static String sha1(InputStream stream) throws IOException {
        return calc(ALGO_SHA1, stream);
    }
    
    public static String sha1(String str) throws IOException {
        return calc(ALGO_SHA1, str);
    }

    private Digest() {
        /* prevent instantiation */
    }
}
