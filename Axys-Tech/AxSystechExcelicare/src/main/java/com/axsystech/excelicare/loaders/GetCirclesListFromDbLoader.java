package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/10/2015.
 */
public class GetCirclesListFromDbLoader extends DataAsyncTaskLibLoader<List<CirclesListDbModel>> {

    public GetCirclesListFromDbLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<CirclesListDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<CirclesListDbModel, Long> circleListDao = helper.getDao(CirclesListDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<CirclesListDbModel>>() {
                @Override
                public List<CirclesListDbModel> call() throws Exception {
                    return circleListDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
