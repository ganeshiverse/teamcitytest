package com.axsystech.excelicare.framework.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CardServiceDataResponse;
import com.axsystech.excelicare.data.responses.DashBoardCardContentResponse;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.data.responses.SaveActionResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogForCardsFragment;
import com.axsystech.excelicare.loaders.DashBoardCardContentLoader;
import com.axsystech.excelicare.loaders.DashBoardCardServiceLoader;
import com.axsystech.excelicare.loaders.DeleteCardDetailsLoader;
import com.axsystech.excelicare.loaders.GetDashBoardCardsLoader;
import com.axsystech.excelicare.loaders.SaveCardDetailsLoader;
import com.axsystech.excelicare.loaders.UrlCardAsyncTask;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Date: 06.05.14
 * Time: 14:31
 *
 * @author SomeswarReddy
 */
public class DashboardFragment extends BaseFragment implements View.OnClickListener,
        ConfirmationDialogForCardsFragment.OnConfirmListener, UrlCardAsyncTask.LoadUrlCardListener {

    private static final String DASHBOARD_CARDS_LOADER = "com.axsystech.excelicare.framework.ui.activities.DashBoardActivity.DASH_BOARD_CARDS_LOADER";
    private static final String DASHBOARD_CARD_SERVICE_LOADER = "com.axsystech.excelicare.framework.ui.activities.DashBoardActivity.DASH_BOARD_CARD_SERVICE_LOADER";
    private static final String DASHBOARD_CARD_CONTENT_LOADER = "com.axsystech.excelicare.framework.ui.activities.DashBoardActivity.DASHBOARD_CARD_CONTENT_LOADER";

    private static final String CARD_ACTION_SAVE_LOADER = "com.axsystech.excelicare.framework.ui.activities.DashBoardActivity.CARD_ACTION_SAVE_LOADER";
    private static final String CARD_ACTION_DELETE_LOADER = "com.axsystech.excelicare.framework.ui.activities.DashBoardActivity.CARD_ACTION_DELETE_LOADER";
    private static final String GET_CARD_MENU_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.DashBoardActivity.GET_CARD_MENU_LIST_LOADER";

    private String TAG = DashboardFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private DashboardFragment mFragment;

    private static final int DELETE_CARD_DATA_ID = 505;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mSelectedMenuDashboardId = "";

    @InjectSavedState
    private String mSelectedMenuModuleId = "";

    @InjectSavedState
    private String mSelectedCardRecordId = "";

    @InjectView(R.id.topContainer)
    private LinearLayout topContainer;

    @InjectView(R.id.dynamicContentLayout)
    private LinearLayout dynamicContentLayout;

    @InjectView(R.id.singleCardFooterLayout)
    private LinearLayout singleCardFooterLayout;

    @InjectView(R.id.mainDashboardFooterLayout)
    private LinearLayout mainDashboardFooterLayout;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    private LayoutInflater mLayoutInflater;

    private GetDashBoardCardsResponse mDashBoardCardsResponse;
    private int mCardIndex = 0;

    private Button mExecutedActionButton;

    private int mScreenWidth;
    private int mScreenHeight;
    private int mHeaderWidth;

    private LinearLayout sideBySideCardsContainer;
    private int sideBySideCardsWidth;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        mLayoutInflater = LayoutInflater.from(mActivity);

        initViews();
        readIntentArgs();
        preloadData();

        // get screen width & height
        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;

        Trace.d(TAG, "mScreenWidth:" + mScreenWidth + "||mScreenHeight:" + mScreenHeight);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();

        setHasOptionsMenu(true);
    }

    private void initViews() {

    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MENU_MODULE_ID)) {
                mSelectedMenuModuleId = bundle.getString(AxSysConstants.EXTRA_SELECTED_MENU_MODULE_ID, "");
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MENU_DASHBOARD_ID)) {
                mSelectedMenuDashboardId = bundle.getString(AxSysConstants.EXTRA_SELECTED_MENU_DASHBOARD_ID, "");
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CARD_RECORD_ID)) {
                mSelectedCardRecordId = bundle.getString(AxSysConstants.EXTRA_SELECTED_CARD_RECORD_ID, "");
            }
        }
    }

    public void preloadData() {
        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            GetDashBoardCardsLoader dashBoardLoader = new GetDashBoardCardsLoader(mActivity, mActiveUser.getToken(), mSelectedMenuDashboardId, "1");
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DASHBOARD_CARDS_LOADER), dashBoardLoader);
            }
        }
    }

    @Override
    protected void initActionBar() {
        // update actionbar title
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));
        mActivity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.changepasswordactionbar_color)));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        FragmentManager fm = getFragmentManager();

        Trace.d(TAG, "BackStack Count:" + fm.getBackStackEntryCount());
        if (fm.getBackStackEntryCount() <= 1) {
            mActivity.enableDraweToggle();
        }

        // remove header in toolbar if added
        removePreviousCardHeaderInToolBar();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_empty, menu);
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;
            default:
                break;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            default:
                break;
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(DASHBOARD_CARDS_LOADER)) {
            showToast(R.string.invalid_server_response);
            // Reset global data here
            mCardIndex = 0;
            mDashBoardCardsResponse = null;

        } else if (id == getLoaderHelper().getLoaderId(DASHBOARD_CARD_SERVICE_LOADER)) {
            showToast(String.format(getString(R.string.failure_card_header_data), mCardIndex));
        } else if (id == getLoaderHelper().getLoaderId(DASHBOARD_CARD_CONTENT_LOADER)) {
            showToast(String.format(getString(R.string.failure_card_content_data), mCardIndex));
        } else if (id == getLoaderHelper().getLoaderId(CARD_ACTION_SAVE_LOADER)) {
            if (!TextUtils.isEmpty(exception.getMessage())) {
                showToast(exception.getMessage());
            } else {
                showToast(R.string.error_saving_datacard_details);
            }
        } else if (id == getLoaderHelper().getLoaderId(CARD_ACTION_DELETE_LOADER)) {
            if (!TextUtils.isEmpty(exception.getMessage())) {
                showToast(exception.getMessage());
            } else {
                showToast(R.string.error_deleting_card_details);
            }
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(DASHBOARD_CARDS_LOADER)) {
            if (result != null && result instanceof GetDashBoardCardsResponse) {
                // Reset global data here
                mCardIndex = 0;
                mDashBoardCardsResponse = null;
                mDashBoardCardsResponse = (GetDashBoardCardsResponse) result;

                if (mDashBoardCardsResponse.getListObject() != null && mDashBoardCardsResponse.getListObject().size() > 0) {
                    // TODO remove later, just added for Unit Testing
//                    mDashBoardCardsResponse.setListObject(mDashBoardCardsResponse.getListObject());

                    // Create dashboard header if received from server and display it in ActionBar
                    if (mDashBoardCardsResponse.getDashboardHeader() != null) {
                        LinearLayout dashboardHeaderContainer = prepareDashboardHeaderOrFooter(mDashBoardCardsResponse.getDashboardHeader(), true);
                        dashboardHeaderContainer.setLayoutParams(new LinearLayout.LayoutParams(mScreenWidth - getToolbarLeftMenuIconWidth(),
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        removePreviousCardHeaderInToolBar();
                        mActivity.getToolBar().addView(dashboardHeaderContainer);
                    }

//                    // Creating dashboard footer if received from server and display in main dashboard footer layout
                    if (mDashBoardCardsResponse.getDashboardFooter() != null) {
                        LinearLayout dashboardFooterContainer = prepareDashboardHeaderOrFooter(mDashBoardCardsResponse.getDashboardFooter(), false);
                        dashboardFooterContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        mainDashboardFooterLayout.addView(dashboardFooterContainer);
                    }

                    // prepare and display actual dashboard contents
                    GetDashBoardCardsResponse.ListObject cardObject = mDashBoardCardsResponse.getListObject().get(mCardIndex);

                    if (cardObject != null && !TextUtils.isEmpty(cardObject.getTypeLU())) {
                        if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_URL_DATACRD)) {
                            // If card type is URL CARD , just prepare the header, footer and load webview with Service URL
                            hideProgressLoader();
                            prepareDashboardURLCard(mCardIndex, cardObject.getServiceURL());
                        } else /*if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_DATAENTRYCARD_ID) ||
                                cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_LISTCARD_ID) ||
                                cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_COMPOSITECARD_ID))*/ {
                            // If card type is DATA ENTRY or LIST CARD then start loading the flow
                            getSingleCardServiceData(mCardIndex);

                        }
                    }


                } else {
                    showToast(R.string.invalid_server_response);
                    hideProgressLoader();
                }
            } else {
                hideProgressLoader();
            }
        } else if (id == getLoaderHelper().getLoaderId(DASHBOARD_CARD_SERVICE_LOADER)) {
            if (result != null && result instanceof CardServiceDataResponse) {
                CardServiceDataResponse cardServiceDataResponse = (CardServiceDataResponse) result;

                // Prepare Header, Footer and load 'Service data' in Card WebView then after webView page loading finished load 'Content Data'
                prepareDashboardListOrDataEntryCard(mCardIndex, cardServiceDataResponse);
            }
        } else if (id == getLoaderHelper().getLoaderId(DASHBOARD_CARD_CONTENT_LOADER)) {
            if (result != null && result instanceof String) {
                String cardContentData = (String) result;
                // Set card content data in-order to use it for header mode management
                if (mDashBoardCardsResponse != null && mDashBoardCardsResponse.getListObject() != null &&
                        mDashBoardCardsResponse.getListObject().size() > mCardIndex) {
                    mDashBoardCardsResponse.getListObject().get(mCardIndex).setCardContentData(cardContentData);
                }

                View cardContainer = dynamicContentLayout.getChildAt(mCardIndex);
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    webView.loadUrl("javascript:setData(" + cardContentData + ");");
                }

                // WIZARDS : restrict app without loading next card if it is wizard type
                if (mDashBoardCardsResponse != null && !TextUtils.isEmpty(mDashBoardCardsResponse.getIsWizard())) {
                    if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                        // restrict app without loading next card if it is wizard type
                    } else {
                        // Do mode management for Next/Previous buttons in the header
                        modeManagementForNextPreviousButton(cardContentData);

                        // check if given index card service and content data is loaded or not
                        if (mDashBoardCardsResponse != null && mDashBoardCardsResponse.getListObject() != null) {
                            if (mDashBoardCardsResponse.getListObject().size() > (mCardIndex + 1)) {
                                // load next card service data & after that cycle re-starts again
                                mCardIndex++;

                                // get next card object
                                GetDashBoardCardsResponse.ListObject cardObject = mDashBoardCardsResponse.getListObject().get(mCardIndex);

                                if (cardObject != null && !TextUtils.isEmpty(cardObject.getTypeLU())) {
                                    if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_URL_DATACRD)) {
                                        // If card type is URL CARD , just prepare the header, footer and load webview with Service URL
                                        hideProgressLoader();
                                        prepareDashboardURLCard(mCardIndex, cardObject.getServiceURL());
                                    } else /*if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_DATAENTRYCARD_ID) ||
                                            cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_LISTCARD_ID) ||
                                            cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_COMPOSITECARD_ID))*/ {
                                        // If card type is DATA ENTRY or LIST CARD then start looing the flow
                                        getSingleCardServiceData(mCardIndex);
                                    }
                                }
                            } else {
                                hideProgressLoader();
                            }
                        }
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CARD_ACTION_SAVE_LOADER)) {
            hideProgressLoader();

            if (result != null && result instanceof SaveActionResponse) {
                SaveActionResponse baseResponse = (SaveActionResponse) result;

                if (mExecutedActionButton != null) {
                    GetDashBoardCardsResponse.Control controlObj = (GetDashBoardCardsResponse.Control) mExecutedActionButton.getTag(R.id.control_object);

                    if (controlObj != null && !TextUtils.isEmpty(controlObj.getAction())) {
                        if (controlObj.getAction().trim().equalsIgnoreCase("ValidateSave")) {
                            // Stay in this screen

                            if (mDashBoardCardsResponse != null && mDashBoardCardsResponse.getListObject() != null &&
                                    mDashBoardCardsResponse.getListObject().size() > mCardIndex) {
                                // Do mode management for Next/Previous buttons in the header
                                modeManagementForNextPreviousButton(mDashBoardCardsResponse.getListObject().get(mCardIndex).getCardContentData());
                            }
                        } else if (controlObj.getAction().trim().equalsIgnoreCase("ValidateSaveNavigate")) {
                            /*
                             Get Params object for the executed action and check for the moduleID.
                             If moduleId is empty, just stay in this screen
                              otherwise navigate to appropriate screen based on moduleID and dashboardID
                            */
                            Trace.d(TAG, "CURRENT# ModuleId:" + mSelectedMenuModuleId + "||DashboardId:" + mSelectedMenuDashboardId + "||RecordId:" + mSelectedCardRecordId);
                            final GetDashBoardCardsResponse.Params paramsObj = controlObj.getParams();
                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    if (paramsObj != null) {
                                        if (!TextUtils.isEmpty(paramsObj.getModuleId()) && !TextUtils.isEmpty(paramsObj.getDashboardId())) {
                                            mActivity.navigateBasedOnModuleId(mActionBarTitle, "NavigationFragment", paramsObj.getModuleId(),
                                                    !TextUtils.isEmpty(paramsObj.getDashboardId()) ? paramsObj.getDashboardId() : "",
                                                    !TextUtils.isEmpty(paramsObj.getRecordId()) ? paramsObj.getRecordId() : "", false, true);

                                        } else {
                                            getActivity().getSupportFragmentManager().popBackStack();
                                        }
                                    } else {
                                        getActivity().getSupportFragmentManager().popBackStack();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CARD_ACTION_DELETE_LOADER)) {
            hideProgressLoader();

            if (result != null && result instanceof BaseResponse) {
                BaseResponse baseResponse = (BaseResponse) result;

                /*
                 Get Params object for the executed action and check for the moduleID.
                 If moduleId is empty, just stay in this screen
                  otherwise navigate to appropriate screen based on moduleID and dashboardID
                */
                Trace.d(TAG, "CURRENT# ModuleId:" + mSelectedMenuModuleId + "||DashboardId:" + mSelectedMenuDashboardId + "||RecordId:" + mSelectedCardRecordId);
                if (mExecutedActionButton != null) {
                    GetDashBoardCardsResponse.Control controlObj = (GetDashBoardCardsResponse.Control) mExecutedActionButton.getTag(R.id.control_object);
                    if (controlObj != null) {
                        final GetDashBoardCardsResponse.Params paramsObj = controlObj.getParams();

                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                if (paramsObj != null) {
                                    if (!TextUtils.isEmpty(paramsObj.getModuleId()) && !TextUtils.isEmpty(paramsObj.getDashboardId())) {
                                        mActivity.navigateBasedOnModuleId(mActionBarTitle, "NavigationFragment", paramsObj.getModuleId(),
                                                !TextUtils.isEmpty(paramsObj.getDashboardId()) ? paramsObj.getDashboardId() : "",
                                                !TextUtils.isEmpty(paramsObj.getRecordId()) ? paramsObj.getRecordId() : "", false, true);

                                    } else {
                                        getActivity().getSupportFragmentManager().popBackStack();
                                    }
                                } else {
                                    getActivity().getSupportFragmentManager().popBackStack();
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    private void getSingleCardServiceData(final int cardIndex) {
        if (mDashBoardCardsResponse.getListObject() != null && mDashBoardCardsResponse.getListObject().size() > cardIndex) {
            // get given indexed card HTML header data
            GetDashBoardCardsResponse.ListObject cardData = mDashBoardCardsResponse.getListObject().get(cardIndex);

            if (!TextUtils.isEmpty(cardData.getServiceURL())) {
                DashBoardCardServiceLoader cardServiceLoader = new DashBoardCardServiceLoader(mActivity, cardData.getServiceURL());
                if (isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DASHBOARD_CARD_SERVICE_LOADER), cardServiceLoader);
                }
            }
        }
    }

    private void getSingleCardContentData(final int cardIndex, String dataCardParamData) {
        if (mDashBoardCardsResponse.getListObject().size() > cardIndex) {

            // send request for 'Content data' for the given index card
            DashBoardCardContentLoader cardContentLoader = null;
            try {
                cardContentLoader = new DashBoardCardContentLoader(mActivity,
                        mDashBoardCardsResponse.getListObject().get(cardIndex).getContentURL() + URLEncoder.encode(dataCardParamData, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DASHBOARD_CARD_CONTENT_LOADER), cardContentLoader);
            }
        }
    }

    private void prepareDashboardListOrDataEntryCard(final int cardIndex, CardServiceDataResponse cardServiceDataResponse) {
        if (mDashBoardCardsResponse != null && mDashBoardCardsResponse.getListObject().size() > cardIndex) {
            LinearLayout.LayoutParams mainLLParams = null;

            LinearLayout singleCardContainer = new LinearLayout(mActivity);
            int cardWidth = getDashboardCardWidth(mDashBoardCardsResponse.getListObject().get(cardIndex).getWidth());

            if (mDashBoardCardsResponse.getListObject().size() > 1) {
                mHeaderWidth = mScreenWidth;

                mainLLParams = new LinearLayout.LayoutParams(cardWidth - 10, LinearLayout.LayoutParams.WRAP_CONTENT);
            } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                mHeaderWidth = mScreenWidth - getToolbarLeftMenuIconWidth();

                mainLLParams = new LinearLayout.LayoutParams(cardWidth - 10, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            }
            mainLLParams.setMargins(5, 10, 5, 10);
            singleCardContainer.setLayoutParams(mainLLParams);
            singleCardContainer.setBackgroundResource(R.drawable.white_form);
            singleCardContainer.setOrientation(LinearLayout.VERTICAL);

            // Creating header
            LinearLayout headerContainer = prepareCardHeaderOrFooter(cardIndex, true);

            // Creating footer
            LinearLayout footerContainer = prepareCardHeaderOrFooter(cardIndex, false);
            footerContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));

            // Creating webview container
            WebView webView = new WebView(mActivity);
            LinearLayout.LayoutParams wvParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            wvParams.setMargins(5, 5, 5, 5);
            webView.setLayoutParams(wvParams);

            // set card object as tag to webview so that it will be used in webview interface whenever required
            webView.setTag(R.id.cardobject, mDashBoardCardsResponse.getListObject().get(cardIndex));

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.addJavascriptInterface(new WebViewInterface(mActivity, mDashBoardCardsResponse.getListObject().get(cardIndex), cardIndex), "android");
            webView.getSettings().setAllowFileAccess(true);

            MyWebViewClient webViewClient = new MyWebViewClient(mActivity, mDashBoardCardsResponse.getListObject().get(cardIndex), cardIndex);
            webView.setWebViewClient(webViewClient);

            MyWebChromeClient webChromeClient = new MyWebChromeClient(mActivity, mDashBoardCardsResponse.getListObject().get(cardIndex));
            webView.setWebChromeClient(webChromeClient);

            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                fixNewAndroid(webView);
            }

            String cardHtmlData = cardServiceDataResponse.getData();
            webView.loadDataWithBaseURL("x-data://base", cardHtmlData, "text/html", "UTF-8", null);

            // if dashboard is having only single card we need to display the card header in ActionBar and footer at bottom of the screen

            // IMPORTANT: do not change the order of adding views to 'singleCardContainer',
            // As there is a logic dependent on this, see 'onLoaderResult()' in this class
            /*
                NOTE: if there are multiple cards in Dashboard we will add all in 'dynamicContentLayout (scrollView child)' in this case
                'singleCardContainer' will have three childs (Header, webview, footer)

                else if we have only one card in dashboard then we will add header and webview to singleCardContainer (2 childs) and then
                footer will be added to 'footerContentLayout' layout
             */
            if (headerContainer != null && headerContainer.getChildCount() > 0) {
                if (mDashBoardCardsResponse.getListObject().size() > 1) {
                    headerContainer.setLayoutParams(new LinearLayout.LayoutParams(mScreenWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
                    singleCardContainer.addView(headerContainer);

                } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                    Trace.d(TAG, "getToolbarLeftMenuIconWidth:" + getToolbarLeftMenuIconWidth());
                    headerContainer.setLayoutParams(new LinearLayout.LayoutParams(mHeaderWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
                    removePreviousCardHeaderInToolBar();
                    mActivity.getToolBar().addView(headerContainer);
                }
            }

            singleCardContainer.addView(webView);

            if (mDashBoardCardsResponse.getListObject().size() > 1) {
                if (footerContainer != null && footerContainer.getChildCount() > 0) {
                    singleCardContainer.addView(footerContainer);
                }
            }

            if (cardWidth < mScreenWidth) {
                if (sideBySideCardsContainer == null) {
                    sideBySideCardsContainer = new LinearLayout(mActivity);

                    sideBySideCardsContainer.setOrientation(LinearLayout.HORIZONTAL);

                    dynamicContentLayout.addView(sideBySideCardsContainer);
                }

                sideBySideCardsContainer.addView(singleCardContainer);

                // ******************
                sideBySideCardsWidth += cardWidth;
                if (sideBySideCardsWidth >= mScreenWidth) {
                    sideBySideCardsWidth = 0;
                    sideBySideCardsContainer = null;
                }
            } else {
                dynamicContentLayout.addView(singleCardContainer);
            }

            if (mDashBoardCardsResponse.getListObject().size() == 1) {
                if (footerContainer != null && footerContainer.getChildCount() > 0) {
                    singleCardFooterLayout.addView(footerContainer);
                }
            }
        }
    }

    private int getDashboardCardWidth(String widthValue) {

        if (!TextUtils.isEmpty(widthValue) && !widthValue.equals("null")) {
            if (widthValue.contains("%")) {
                widthValue = widthValue.replace("%", "");
            }
        }

        int cardWidth = 0;
        if (!TextUtils.isEmpty(widthValue)) {
            float perc = Float.parseFloat(widthValue);

            cardWidth = (int) (mScreenWidth * (perc / 100f));
        } else {
            cardWidth = (int) (mScreenWidth * (100f / 100f));
        }

        return cardWidth;
    }

    private LinearLayout prepareCardHeaderOrFooter(int cardIndex, boolean isHeader) {
        LinearLayout headerOrFooterContainer = null;

        if (mDashBoardCardsResponse != null && mDashBoardCardsResponse.getListObject().size() > cardIndex) {
            // Creating header or footer
            headerOrFooterContainer = new LinearLayout(mActivity);
            headerOrFooterContainer.setBackgroundColor(Color.TRANSPARENT);
            headerOrFooterContainer.setOrientation(LinearLayout.VERTICAL);

            GetDashBoardCardsResponse.HeaderFooter headerOrFooter = null;
            if (isHeader) {
                headerOrFooter = mDashBoardCardsResponse.getListObject().get(cardIndex).getHeader();
            } else {
                headerOrFooter = mDashBoardCardsResponse.getListObject().get(cardIndex).getFooter();
            }
            if (headerOrFooter != null && headerOrFooter.getRows() != null && headerOrFooter.getRows().size() > 0) {
                for (GetDashBoardCardsResponse.Rows rowsObj : headerOrFooter.getRows()) {

                    if (rowsObj != null) {
                        ArrayList<GetDashBoardCardsResponse.Columns> columnsArrayList = rowsObj.getColumns();
                        if (columnsArrayList != null && columnsArrayList.size() > 0) {
                            LinearLayout rowLayout = createHeaderOrFooterViews(isHeader, columnsArrayList, cardIndex);

                            if (rowLayout != null && rowLayout.getChildCount() > 0) {
                                headerOrFooterContainer.addView(rowLayout);
                            }
                        }
                    }
                }
            }
        }

        return headerOrFooterContainer;
    }

    private LinearLayout prepareDashboardHeaderOrFooter(GetDashBoardCardsResponse.HeaderFooter headerOrFooter, boolean isHeader) {
        LinearLayout headerOrFooterContainer = null;

        if (headerOrFooter != null) {
            // Creating header or footer
            headerOrFooterContainer = new LinearLayout(mActivity);
            LinearLayout.LayoutParams mainLLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            headerOrFooterContainer.setLayoutParams(mainLLParams);
            mainLLParams.gravity = Gravity.CENTER_VERTICAL;
            headerOrFooterContainer.setBackgroundColor(Color.TRANSPARENT);
            headerOrFooterContainer.setOrientation(LinearLayout.VERTICAL);

            if (headerOrFooter.getRows() != null && headerOrFooter.getRows().size() > 0) {
                for (GetDashBoardCardsResponse.Rows rowsObj : headerOrFooter.getRows()) {

                    if (rowsObj != null) {
                        ArrayList<GetDashBoardCardsResponse.Columns> columnsArrayList = rowsObj.getColumns();
                        if (columnsArrayList != null && columnsArrayList.size() > 0) {
                            LinearLayout rowLayout = createHeaderOrFooterViews(isHeader, columnsArrayList, -1);

                            if (rowLayout != null && rowLayout.getChildCount() > 0) {
                                headerOrFooterContainer.addView(rowLayout);
                            }
                        }
                    }
                }
            }
        }

        return headerOrFooterContainer;
    }

    private void prepareDashboardURLCard(final int cardIndex, String serviceUrl) {
        if (mDashBoardCardsResponse != null && mDashBoardCardsResponse.getListObject().size() > cardIndex) {
            LinearLayout.LayoutParams mainLLParams = null;

            LinearLayout singleCardContainer = new LinearLayout(mActivity);
            int cardWidth = getDashboardCardWidth(mDashBoardCardsResponse.getListObject().get(cardIndex).getWidth());

            if (mDashBoardCardsResponse.getListObject().size() > 1) {
                mHeaderWidth = mScreenWidth;

                mainLLParams = new LinearLayout.LayoutParams(cardWidth - 10, LinearLayout.LayoutParams.WRAP_CONTENT);
            } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                mHeaderWidth = mScreenWidth - getToolbarLeftMenuIconWidth();

                mainLLParams = new LinearLayout.LayoutParams(cardWidth - 10, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            }
            mainLLParams.setMargins(5, 10, 5, 10);
            singleCardContainer.setLayoutParams(mainLLParams);
            singleCardContainer.setBackgroundResource(R.drawable.white_form);
            singleCardContainer.setOrientation(LinearLayout.VERTICAL);

            // Creating header
            LinearLayout headerContainer = prepareCardHeaderOrFooter(cardIndex, true);

            // Creating footer
            LinearLayout footerContainer = prepareCardHeaderOrFooter(cardIndex, false);
            footerContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));

            // Creating webview container
            WebView webView = new WebView(mActivity);

            if (mDashBoardCardsResponse.getListObject().size() == 1) {
                LinearLayout.LayoutParams wvParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
                wvParams.setMargins(5, 5, 5, 5);
                webView.setLayoutParams(wvParams);
            }
            // set card object as tag to webview so that it will be used in webview interface whenever required
            webView.setTag(R.id.cardobject, mDashBoardCardsResponse.getListObject().get(cardIndex));

            webView.getSettings().setDisplayZoomControls(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.addJavascriptInterface(new WebViewInterface(mActivity, mDashBoardCardsResponse.getListObject().get(cardIndex), cardIndex), "android");
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            if (Build.VERSION.SDK_INT >= 11) {
                webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            MyWebViewClient webViewClient = new MyWebViewClient(mActivity, mDashBoardCardsResponse.getListObject().get(cardIndex), cardIndex);
            webView.setWebViewClient(webViewClient);

            MyWebChromeClient webChromeClient = new MyWebChromeClient(mActivity, mDashBoardCardsResponse.getListObject().get(cardIndex));
            webView.setWebChromeClient(webChromeClient);

            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                fixNewAndroid(webView);
            }

            if (!TextUtils.isEmpty(serviceUrl)) {
                if (serviceUrl.contains("pdf") || serviceUrl.contains("PDF")) {
                    String googleDocs = "https://docs.google.com/gview?embedded=true&url=";
                    webView.loadUrl(googleDocs + serviceUrl);
                } else {
                    webView.loadUrl(serviceUrl);
                }
            } else {
                webView.loadUrl("");
            }

            // TODO: if dashboard is having only single card we need to display the card header in ActionBar and footer at bottom of the screen

            // IMPORTANT: do not change the order of adding views to 'singleCardContainer',
            // As there is a logic dependent on this, see 'onLoaderResult()' in this class
             /*
                NOTE: if there are multiple cards in Dashboard we will add all in 'dynamicContentLayout (scrollView child)' in this case
                'singleCardContainer' will have three childs (Header, webview, footer)

                else if we ave only one card in dashboard then we will add header and webview to singleCardContainer (2 childs) and then
                footer will be added to 'footerContentLayout' layout
             */
            if (headerContainer != null && headerContainer.getChildCount() > 0) {
                if (mDashBoardCardsResponse.getListObject().size() > 1) {
                    headerContainer.setLayoutParams(new LinearLayout.LayoutParams(mScreenWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
                    singleCardContainer.addView(headerContainer);

                } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                    Trace.d(TAG, "getToolbarLeftMenuIconWidth:" + getToolbarLeftMenuIconWidth());
                    headerContainer.setLayoutParams(new LinearLayout.LayoutParams(mHeaderWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
                    removePreviousCardHeaderInToolBar();
                    mActivity.getToolBar().addView(headerContainer);
                }
            }
            singleCardContainer.addView(webView);

            if (mDashBoardCardsResponse.getListObject().size() > 1) {
                if (footerContainer != null && footerContainer.getChildCount() > 0) {
                    singleCardContainer.addView(footerContainer);
                }
            }

            if (cardWidth < mScreenWidth) {
                if (sideBySideCardsContainer == null) {
                    sideBySideCardsContainer = new LinearLayout(mActivity);

                    sideBySideCardsContainer.setOrientation(LinearLayout.HORIZONTAL);

                    dynamicContentLayout.addView(sideBySideCardsContainer);
                }

                sideBySideCardsContainer.addView(singleCardContainer);

                // ******************
                sideBySideCardsWidth += cardWidth;
                if (sideBySideCardsWidth >= mScreenWidth) {
                    sideBySideCardsWidth = 0;
                    sideBySideCardsContainer = null;
                }
            } else {
                dynamicContentLayout.addView(singleCardContainer);
            }

            if (mDashBoardCardsResponse.getListObject().size() == 1) {
                if (footerContainer != null && footerContainer.getChildCount() > 0) {
                    singleCardFooterLayout.addView(footerContainer);
                }
            }
        }
    }

    private void removePreviousCardHeaderInToolBar() {
        if (mActivity.getToolBar() != null && mActivity.getToolBar().getChildCount() > 0) {
            for (int idx = 0; idx < mActivity.getToolBar().getChildCount(); idx++) {
                View childView = mActivity.getToolBar().getChildAt(idx);

                if (childView != null && childView instanceof LinearLayout) {
                    mActivity.getToolBar().removeViewAt(idx);
                }
            }
        }
    }

    private LinearLayout getHeaderAddedInToolBar() {
        if (mActivity.getToolBar() != null && mActivity.getToolBar().getChildCount() > 0) {
            for (int idx = 0; idx < mActivity.getToolBar().getChildCount(); idx++) {
                View childView = mActivity.getToolBar().getChildAt(idx);

                if (childView != null && childView instanceof LinearLayout) {
                    return (LinearLayout) mActivity.getToolBar().getChildAt(idx);
                }
            }
        }

        return null;
    }

    private int getToolbarLeftMenuIconWidth() {
        int width = 0;

        if (mActivity.getToolBar() != null && mActivity.getToolBar().getChildCount() > 0) {
            for (int idx = 0; idx < mActivity.getToolBar().getChildCount(); idx++) {
                View childView = mActivity.getToolBar().getChildAt(idx);

                if (childView != null && childView instanceof ImageButton) {
                    return mActivity.getToolBar().getChildAt(idx).getMeasuredWidth();
                }
            }
        }

        return width;
    }

    private void modeManagementForNextPreviousButton(String cardContentData) {
        // Parse 'RecordCount' && 'RecordPosition'
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        DashBoardCardContentResponse contentResponse = null;
        try {
            contentResponse = gson.fromJson(cardContentData, DashBoardCardContentResponse.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (contentResponse != null && contentResponse.getContentDataObject() != null) {
            if (contentResponse.getContentDataObject().getContentBodyArrayList() != null &&
                    contentResponse.getContentDataObject().getContentBodyArrayList().size() > 0) {
                DashBoardCardContentResponse.ContentBody contentBody = contentResponse.getContentDataObject().getContentBodyArrayList().get(0);

                if (contentBody != null && contentBody.getFormObjectArrayList() != null &&
                        contentBody.getFormObjectArrayList().size() > 0) {
                    DashBoardCardContentResponse.FormObject formObject = contentBody.getFormObjectArrayList().get(0);

                    if (formObject != null) {
                        int recordCount = formObject.getRecordCount();
                        int recordPosition = formObject.getRecordPosition() - 1;

                        // Toggle Next / Previous button based on record count & position
                        if (mCardIndex >= 0) {
                            if (dynamicContentLayout.getChildCount() > mCardIndex) {
                                if (mDashBoardCardsResponse.getListObject().size() > 1) {
                                    View currentCardView = dynamicContentLayout.getChildAt(mCardIndex); // LL - Vertical

                                    if (currentCardView != null && currentCardView instanceof ViewGroup) {
                                        ViewGroup currentCardViewGroup = (ViewGroup) currentCardView;

                                        if (currentCardViewGroup != null && currentCardViewGroup.getChildCount() > 0) {
                                            // Get Header view if exists
                                            View headerContainer = currentCardViewGroup.getChildAt(0);
                                            updateModeManagement(headerContainer, recordCount, recordPosition);
                                        }
                                    }
                                } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                                    LinearLayout headerContainer = getHeaderAddedInToolBar();
                                    if (headerContainer != null) {
                                        updateModeManagement(headerContainer, recordCount, recordPosition);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void modeManagementWhileEditing(final int cardIndex, String mode) {
        if (!TextUtils.isEmpty(mode) && cardIndex >= 0) {

            if (dynamicContentLayout.getChildCount() > cardIndex) {
                View currentCardView = dynamicContentLayout.getChildAt(cardIndex); // LL - Vertical

                if (currentCardView != null && currentCardView instanceof ViewGroup) {
                    ViewGroup currentCardViewGroup = (ViewGroup) currentCardView;

                    if (currentCardViewGroup != null && currentCardViewGroup.getChildCount() > 0) {
                        // Get Header view if exists - As per iOS behavior we should not do mode management for header
//                        View headerContainer = currentCardViewGroup.getChildAt(0);
//                        updateModeManagement(headerContainer, mode);

                        // Get Footer view if exists
                        View footerContainer = null;
                        if (mDashBoardCardsResponse.getListObject().size() > 1) {
                            footerContainer = currentCardViewGroup.getChildAt(2);
                        } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                            footerContainer = singleCardFooterLayout.getChildAt(0);
                        }
                        updateModeManagement(footerContainer, mode);
                    }
                }
            }
        }
    }

    private void updateModeManagement(View headerContainer, String mode) {
        if (headerContainer != null && headerContainer instanceof ViewGroup) {
            ViewGroup headerContainerViewGroup = (ViewGroup) headerContainer; // LL - Vertical

            if (headerContainerViewGroup != null && headerContainerViewGroup.getChildCount() > 0) {
                for (int rowIdx = 0; rowIdx <= headerContainerViewGroup.getChildCount(); rowIdx++) {
                    View rowView = headerContainerViewGroup.getChildAt(rowIdx); // LL - Horizontal

                    if (rowView != null && rowView instanceof ViewGroup) {
                        ViewGroup rowViewGroup = (ViewGroup) rowView;

                        if (rowViewGroup != null && rowViewGroup.getChildCount() > 0) {
                            for (int vIdx = 0; vIdx <= rowViewGroup.getChildCount(); vIdx++) {
                                View child = rowViewGroup.getChildAt(vIdx); // Actual View, May be Button, ImageView, TextView

                                if (child != null) {
                                    GetDashBoardCardsResponse.Control controlObj = (GetDashBoardCardsResponse.Control) child.getTag(R.id.control_object);

                                    if (controlObj != null) {
                                        if (child instanceof Button) {
                                            if (mode.equalsIgnoreCase("edit")) {
                                                // EDIT MODE MANAGEMENT
                                                if (controlObj.getEdit() != null) {
                                                    if (controlObj.getEdit().trim().equalsIgnoreCase("0")) {
                                                        // Disable click
                                                        child.setEnabled(false);

                                                    } else if (controlObj.getEdit().trim().equalsIgnoreCase("1")) {
                                                        // Enable click
                                                        child.setEnabled(true);
                                                    }
                                                }
                                            } else if (mode.equalsIgnoreCase("idle")) {
                                                // DEFAULT MODE MANAGEMENT
                                                if (controlObj.getIdle() != null) {
                                                    if (controlObj.getIdle().trim().equalsIgnoreCase("0")) {
                                                        // Disable click
                                                        child.setEnabled(false);

                                                    } else if (controlObj.getIdle().trim().equalsIgnoreCase("1")) {
                                                        // Enable click
                                                        child.setEnabled(true);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateModeManagement(View headerContainer, int recordCount, int recordPosition) {
        if (headerContainer != null && headerContainer instanceof ViewGroup) {
            ViewGroup headerContainerViewGroup = (ViewGroup) headerContainer; // LL - Vertical

            if (headerContainerViewGroup != null && headerContainerViewGroup.getChildCount() > 0) {
                for (int rowIdx = 0; rowIdx <= headerContainerViewGroup.getChildCount(); rowIdx++) {
                    View rowView = headerContainerViewGroup.getChildAt(rowIdx); // LL - Horizontal

                    if (rowView != null && rowView instanceof ViewGroup) {
                        ViewGroup rowViewGroup = (ViewGroup) rowView;

                        if (rowViewGroup != null && rowViewGroup.getChildCount() > 0) {
                            for (int vIdx = 0; vIdx <= rowViewGroup.getChildCount(); vIdx++) {
                                View child = rowViewGroup.getChildAt(vIdx); // Actual View, May be Button, ImageView, TextView

                                if (child != null) {
                                    GetDashBoardCardsResponse.Control controlObj = (GetDashBoardCardsResponse.Control) child.getTag(R.id.control_object);

                                    if (controlObj != null) {
                                        if (child instanceof Button) {
                                            if (!TextUtils.isEmpty(controlObj.getId())) {
                                                if (controlObj.getId().equalsIgnoreCase(AxSysConstants.DBC_PREV_BTN_ID)) {
                                                    //For previous button
                                                    if (recordPosition == 0 || recordPosition == -1) {
                                                        // Disable click
                                                        child.setEnabled(false);
                                                    } else {
                                                        // Enable click
                                                        child.setEnabled(true);
                                                    }
                                                } else if (controlObj.getId().equalsIgnoreCase(AxSysConstants.DBC_NEXT_BTN_ID)) {
                                                    if (recordPosition == recordCount - 1 || recordCount == 0) {
                                                        // Disable click
                                                        child.setEnabled(false);
                                                    } else {
                                                        // Enable click
                                                        child.setEnabled(true);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class MyWebChromeClient extends WebChromeClient {

        private Context context;
        private GetDashBoardCardsResponse.ListObject cardObject;

        private MyWebChromeClient(Context context, GetDashBoardCardsResponse.ListObject cardObject) {
            this.context = context;
            this.cardObject = cardObject;
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            Trace.d(TAG, "url:" + url + "||message:" + message + "||JsResult:" + result);
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            Trace.d(TAG, "onConsoleMessage:" + consoleMessage);
            return super.onConsoleMessage(consoleMessage);
        }
    }

    private class MyWebViewClient extends WebViewClient {

        private Context context;
        private GetDashBoardCardsResponse.ListObject cardObject;
        private int cardIndex;

        private MyWebViewClient(Context context, GetDashBoardCardsResponse.ListObject cardObject, final int cardIndex) {
            this.context = context;
            this.cardObject = cardObject;
            this.cardIndex = cardIndex;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            Trace.d(TAG, "shouldOverrideUrlLoading::url:" + url);

            if (!TextUtils.isEmpty(url)) {
                if (url.startsWith("event:")) {
                    String eventParms = url.replace("event:", "");

                    try {
                        JSONObject eventJsonObj = new JSONObject(eventParms.trim());
                        String action = "";

                        if (eventJsonObj.has("action")) {
                            action = eventJsonObj.optString("action");
                        }

                        if (TextUtils.isEmpty(action)) {
                            if (eventJsonObj.has("acton")) {
                                action = eventJsonObj.optString("acton");
                            }
                        }

                        // perform action based on value in 'action' tag
                        if (!TextUtils.isEmpty(action)) {
                            if (action.trim().equalsIgnoreCase("Navigate") || action.trim().equalsIgnoreCase("NavigateTo")) {
                                // event:{"action":"Navigate","params":{"ModuleId":"1357","DashboardId":"12","RecordId":"1036"}}

                                String moduleId = "";
                                String dashboardId = "";
                                String recordId = "";

                                if (eventJsonObj.has("params")) {
                                    JSONObject params = eventJsonObj.optJSONObject("params");

                                    if (params != null) {
                                        if (params.has("ModuleId")) {
                                            moduleId = params.optString("ModuleId");
                                        }

                                        if (params.has("DashboardId")) {
                                            dashboardId = params.optString("DashboardId");
                                        }

                                        if (params.has("RecordId")) {
                                            recordId = params.optString("RecordId");
                                        }
                                    }
                                }

                                mActivity.navigateBasedOnModuleId(mActionBarTitle, "NavigationFragment", moduleId, dashboardId, recordId, false, true);

                            } else if (action.trim().equalsIgnoreCase("ModeManagement")) {
                                // event:{"acton":"ModeManagement","params":{"mode":"edit"}}

                                if (eventJsonObj.has("params")) {
                                    JSONObject params = eventJsonObj.optJSONObject("params");
                                    String mode = "";

                                    if (params != null) {
                                        if (params.has("mode")) {
                                            mode = params.optString("mode");
                                        }
                                    }

                                    // MODE MANAGEMENT while editing
                                    modeManagementWhileEditing(cardIndex, mode);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            return true;
        }

        @Override
        public void onPageStarted(WebView webView, String url, Bitmap favicon) {
            super.onPageStarted(webView, url, favicon);
        }

        @Override
        public void onPageFinished(WebView webView, String url) {
            super.onPageFinished(webView, url);
            Trace.d(TAG, "onPageFinished::url:" + url);

            if (!TextUtils.isEmpty(url)) {
                if (!url.startsWith("event:")) {

                    if (cardObject != null && !TextUtils.isEmpty(cardObject.getTypeLU())) {
                        if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_URL_DATACRD)) {
                            // do nothing
                        } else if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_DATAENTRYCARD_ID)) {

//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                                // In KitKat+ you should use the evaluateJavascript method - IOS Style
//                                webView.evaluateJavascript("javascript:getParamData(" + mSelectedCardRecordId + ");", new ValueCallback<String>() {
//                                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//                                    @Override
//                                    public void onReceiveValue(String message) {
//                                        Trace.d(TAG, "onReceiveValue:" + message);
//
//                                        getSingleCardContentData(cardIndex, (message != null && !message.trim().equalsIgnoreCase("null")) ? message : "");
//                                    }
//                                });
//                            } else {
                            // Android Style
                            hideProgressLoader();
//                                webView.loadUrl("javascript:getParamData(" + mSelectedCardRecordId + ", 1);");
                            webView.loadUrl("javascript:getParamData(" + mSelectedCardRecordId + ");");
//                            }
                        } else /*if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_LISTCARD_ID) ||
                                cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_COMPOSITECARD_ID))*/ {
                            // Android Style
                            hideProgressLoader();

                            getSingleCardContentData(cardIndex, "");
                        }
                    }
                }
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
        }
    }

    public class WebViewInterface {

        private String TAG = WebViewInterface.class.getSimpleName();
        private MainContentActivity mActivity;
        private GetDashBoardCardsResponse.ListObject cardObject;
        private int cardIndex;

        /**
         * Instantiate the interface and set the context
         */
        public WebViewInterface(MainContentActivity activity, GetDashBoardCardsResponse.ListObject cardObject, final int cardIndex) {
            this.mActivity = activity;
            this.cardObject = cardObject;
            this.cardIndex = cardIndex;
        }

        @JavascriptInterface
        public void onError(String error) {
            hideProgressLoader();
            throw new Error(error);
        }

        @JavascriptInterface
        public void getParamData(final String message) {
            Trace.d(TAG, "getParamData:" + message);

//            if(message != null && message.trim().length() > 0) {
            Trace.d(TAG, "Need to call card content data method in Dashboard Fragment...");

            mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    getSingleCardContentData(cardIndex, (message != null && !message.trim().equalsIgnoreCase("null")) ? message : "");
                }
            });
//            }
        }

        @JavascriptInterface
        public void ValidateDataCardFields(final String message) {
            Trace.d(TAG, "ValidateDataCardFields:" + message);
            /*
                {"Status":"Success","message":""}

                If json status == success then call save API method
		                       == failure - Show an alert to the user with message
             */

            if (message != null && message.trim().length() > 0) {
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        BaseResponse baseResponse = new BaseResponse();
                        try {
                            // {"status":"Failed","message":"Please select the Source."}
                            // {"status":"Success","message":""}

                            JSONObject jsonObject = new JSONObject(message.trim());
                            if (jsonObject.has("status")) {
                                baseResponse.setStatus(jsonObject.optString("status"));
                            }
                            if (jsonObject.has("Status")) {
                                baseResponse.setStatus(jsonObject.optString("Status"));
                            }
                            if (jsonObject.has("message")) {
                                baseResponse.setMessage(jsonObject.optString("message"));
                            }

                            if (baseResponse != null) {
                                if (baseResponse.isSuccess()) {
                                    // Call JS method on WebView
                                    View cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                                    WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                                    if (webView != null) {
                                        if (mExecutedActionButton != null) {
                                            String action = (String) mExecutedActionButton.getTag(R.id.cardaction);
                                            GetDashBoardCardsResponse.Control controlObj = (GetDashBoardCardsResponse.Control) mExecutedActionButton.getTag(R.id.control_object);

                                            if (!TextUtils.isEmpty(action)) {
                                                if (action.equalsIgnoreCase("ValidateNavigatePage") || action.equalsIgnoreCase("NavigatePage")) {
                                                    // WIZARDS
                                                    // This will load next card in webview
                                                    webView.loadUrl("javascript:NavigatePage(" + controlObj.getParams().getDashboardId() + ");");

                                                    // now we need to remove previous card header & footer then need to add new card header footer
                                                    if (cardContainer != null && cardContainer instanceof ViewGroup) {
                                                        ViewGroup cardLayout = (ViewGroup) cardContainer;

                                                        // Find HEADER
                                                        View headerView = cardLayout.getChildAt(0);
                                                        if (headerView != null && headerView instanceof LinearLayout) {
                                                            // which means header is there at 0'th position
                                                            // REMOVE header
                                                            cardLayout.removeViewAt(0);
                                                            // CREATE & ADD new next card header
                                                            int cardIndex = 0;
                                                            if (!TextUtils.isEmpty(controlObj.getParams().getDashboardId())) {
                                                                cardIndex = Integer.parseInt(controlObj.getParams().getDashboardId().trim()) - 1;
                                                            }
                                                            cardLayout.addView(prepareCardHeaderOrFooter(cardIndex, true), 0);
                                                        }

                                                        // Find FOOTER
                                                        View footerView = null;
                                                        int footerPosition = 0;
                                                        if (mDashBoardCardsResponse.getListObject().size() > 1) {
                                                            footerView = cardLayout.getChildAt(1);
                                                            footerPosition = 1;
                                                            if (footerView != null) {
                                                                if (!(footerView instanceof LinearLayout)) {
                                                                    // which means footer not placed at 1'st position, then check if added at 2'nd position
                                                                    footerView = cardLayout.getChildAt(2);
                                                                    footerPosition = 2;
                                                                }
                                                            }
                                                        } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                                                            footerView = singleCardFooterLayout.getChildAt(0);
                                                            footerPosition = 0;
                                                        }
                                                        // CREATE & ADD new next card footer
                                                        if (footerView != null && footerView instanceof LinearLayout) {
                                                            // which means footer is there at 1'st position
                                                            // REMOVE footer
                                                            cardLayout.removeViewAt(1);
                                                            // CREATE & ADD new next card footer
                                                            int cardIndex = 0;
                                                            if (!TextUtils.isEmpty(controlObj.getParams().getDashboardId())) {
                                                                cardIndex = Integer.parseInt(controlObj.getParams().getDashboardId().trim()) - 1;
                                                            }
                                                            cardLayout.addView(prepareCardHeaderOrFooter(cardIndex, false), footerPosition);
                                                        }
                                                    }
                                                } else {
                                                    webView.loadUrl("javascript:getData(1);");
                                                    //webView.loadUrl("javascript:getData();");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    showToast(baseResponse.getMessage());
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void getData(final String message) {
            Trace.d(TAG, "getData:" + message);

            if (message != null && message.trim().length() > 0) {
                // Call SAVE
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        View cardContainer = dynamicContentLayout.getChildAt(cardIndex);

                        WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                        if (webView != null) {
                            String action = (String) webView.getTag(R.id.cardaction);

                            if (action != null && (action.trim().equalsIgnoreCase("ValidateSave") || action.trim().equalsIgnoreCase("ValidateSaveNavigate"))) {
                                // IF ACTION IS SAVE, call save method by using url provided in card response
                                try {
                                    displayProgressLoader(false);
                                    cardActionSave(cardObject.getSaveURL(), message);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                            } else if (action.trim().equalsIgnoreCase("DeleteNavigate")) {
                                // IF action is DELETE, call Delete method by using url provide in card response
                                try {
                                    displayProgressLoader(false);
                                    cardActionDelete(cardObject.getDeleteURL(), message);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void getDataPrevious(final String message) {
            Trace.d(TAG, "getDataPrevious:" + message);

//            if(message != null && message.trim().length() > 0) {
            // action: PREVIOUS
            mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    getSingleCardContentData(cardIndex, (message != null && !message.trim().equalsIgnoreCase("null")) ? message : "");
                }
            });
//            }
        }

        @JavascriptInterface
        public void getDataNext(final String message) {
            Trace.d(TAG, "getDataNext:" + message);

//            if(message != null && message.trim().length() > 0) {
            // action: PREVIOUS
            mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    getSingleCardContentData(cardIndex, (message != null && !message.trim().equalsIgnoreCase("null")) ? message : "");
                }
            });
//            }
        }
    }

    private void cardActionSave(String saveUrl, String strParamData) throws UnsupportedEncodingException {
        if (!TextUtils.isEmpty(saveUrl) && !TextUtils.isEmpty(strParamData)) {
            // SAVE card details
            SaveCardDetailsLoader saveCardDetailsLoader = new SaveCardDetailsLoader(mActivity, saveUrl, mActiveUser.getToken(),
                    "1", /*URLEncoder.encode(*/strParamData/*, "UTF-8")*/);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CARD_ACTION_SAVE_LOADER), saveCardDetailsLoader);
            }
        }
    }

    private void cardActionDelete(String deleteUrl, String message) throws UnsupportedEncodingException {
        if (!TextUtils.isEmpty(deleteUrl) && !TextUtils.isEmpty(message)) {
            // DELETE card details
            DeleteCardDetailsLoader deleteCardDetailsLoader = new DeleteCardDetailsLoader(mActivity, deleteUrl, URLEncoder.encode(message, "UTF-8"));
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CARD_ACTION_DELETE_LOADER), deleteCardDetailsLoader);
            }
        }
    }

    private LinearLayout createHeaderOrFooterViews(boolean isHeader, ArrayList<GetDashBoardCardsResponse.Columns> columnsArrayList, final int cardIndex) {
        LinearLayout headerLayout = null;

        if (columnsArrayList != null && columnsArrayList.size() > 0) {

            headerLayout = new LinearLayout(mActivity);
//            headerLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.login_rememberme_text_color));
            LinearLayout.LayoutParams mainLLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            headerLayout.setLayoutParams(mainLLParams);
            mainLLParams.gravity = Gravity.CENTER_VERTICAL;
            headerLayout.setOrientation(LinearLayout.HORIZONTAL);

            for (int colIdx = 0; colIdx < columnsArrayList.size(); colIdx++) {
                GetDashBoardCardsResponse.Columns columnBean = columnsArrayList.get(colIdx);

                if (columnBean.getControl() != null && columnBean.getControl().size() > 0) {
                    final GetDashBoardCardsResponse.Control controlObj = columnBean.getControl().get(0);

                    if (controlObj.getType().equalsIgnoreCase(AxSysConstants.DBC_COMPONENT_IMAGE_ID)) {
                        //Image
                        final ImageView headerImage = new ImageView(mActivity);

                        Picasso.with(mActivity)
                                .load(controlObj.getSrc())
                                .placeholder(R.drawable.white_form)
                                .error(R.drawable.white_form)
                                .into(headerImage);

                        headerImage.setLayoutParams(setViewLayoutParamsForImageview(isHeader ? mHeaderWidth : mScreenWidth, controlObj));

                        // Set Control object as tag to view, this tag value will be used for Mode Management while editing data on cards
                        headerImage.setTag(R.id.control_object, controlObj);

                        // set card index to button so that we can get it dynamically upon click event
                        headerImage.setTag(R.id.card_index, cardIndex);

                        // DEFAULT MODE MANAGEMENT
                        if (controlObj.getIdle() != null) {
                            if (controlObj.getIdle().trim().equalsIgnoreCase("0")) {
                                // Disable click
                                headerImage.setEnabled(false);

                            } else if (controlObj.getIdle().trim().equalsIgnoreCase("1")) {
                                // Enable click
                                headerImage.setEnabled(true);
                            }
                        }

                        headerLayout.addView(headerImage);

                    } else if (controlObj.getType().equalsIgnoreCase(AxSysConstants.DBC_COMPONENT_LABLE_ID)) {
                        //Label
                        TextView headerLable = new TextView(mActivity);

                        headerLable.setText(controlObj.getText());

                        headerLable.setPadding(10, 10, 10, 10);
                        headerLable.setTextSize(getViewTextSize(controlObj.getStyle()));
                        headerLable.setLayoutParams(setViewLayoutParams(isHeader ? mHeaderWidth : mScreenWidth, controlObj));

                        if (isHeader) {
                            // if we are receive cardindex as -1, which means we are displaying UI components in DashBoard Actionbar
                            if (cardIndex == -1) {
                                headerLable.setTextColor(getResources().getColor(R.color.white));
                            } else {
                                if (mDashBoardCardsResponse.getListObject().size() > 1) {
                                    headerLable.setTextColor(getResources().getColor(R.color.actionbar_color));
                                } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                                    headerLable.setTextColor(getResources().getColor(R.color.white));
                                }
                            }
                        } else {
                            headerLable.setTextColor(getResources().getColor(android.R.color.black));
                        }

                        // Set Control object as tag to view, this tag value will be used for Mode Management while editing data on cards
                        headerLable.setTag(R.id.control_object, controlObj);

                        // set card index to button so that we can get it dynamically upon click event
                        headerLable.setTag(R.id.card_index, cardIndex);

                        // DEFAULT MODE MANAGEMENT
                        if (controlObj.getIdle() != null) {
                            if (controlObj.getIdle().trim().equalsIgnoreCase("0")) {
                                // Disable click
                                headerLable.setEnabled(false);

                            } else if (controlObj.getIdle().trim().equalsIgnoreCase("1")) {
                                // Enable click
                                headerLable.setEnabled(true);
                            }
                        }

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                        params.gravity = Gravity.CENTER_VERTICAL;
                        headerLable.setLayoutParams(params);

                        headerLayout.addView(headerLable);

                    } else if (controlObj.getType().equalsIgnoreCase(AxSysConstants.DBC_COMPONENT_BUTTON_ID)) {
                        //Button
                        Button headerButton = new Button(mActivity);

                        if (isHeader) {
                            // if we are receive cardindex as -1, which means we are displaying UI components in DashBoard Actionbar
                            if (cardIndex == -1) {
                                headerButton.setTextColor(getResources().getColor(R.color.white));
                            } else {
                                if (mDashBoardCardsResponse.getListObject().size() > 1) {
                                    headerButton.setTextColor(getResources().getColor(R.color.actionbar_color));
                                } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
                                    headerButton.setTextColor(getResources().getColor(R.color.white));
                                }
                            }
                        } else {
                            headerButton.setTextColor(getResources().getColor(android.R.color.black));
                        }

                        // Set Control object as tag to view, this tag value will be used for Mode Management while editing data on cards
                        headerButton.setTag(R.id.control_object, controlObj);

                        // set card index to button so that we can get it dynamically upon click event
                        headerButton.setTag(R.id.card_index, cardIndex);

                        // DEFAULT MODE MANAGEMENT
                        if (controlObj.getIdle() != null) {
                            if (controlObj.getIdle().trim().equalsIgnoreCase("0")) {
                                // Disable click
                                headerButton.setEnabled(false);

                            } else if (controlObj.getIdle().trim().equalsIgnoreCase("1")) {
                                // Enable click
                                headerButton.setEnabled(true);
                            }
                        }

                        if (!TextUtils.isEmpty(controlObj.getId())) {
                            if (controlObj.getId().trim().equalsIgnoreCase(AxSysConstants.DBC_MENU_BTN_ID)) {
                                // if we are receive cardindex as -1, which means we are displaying UI components in DashBoard Actionbar
                                if (cardIndex == -1) {
//                                    headerButton.setBackgroundResource(R.drawable.menu_white);
                                    headerButton.setBackgroundResource(0);
                                    headerButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.menu_white, 0);
                                } else {
                                    if (mDashBoardCardsResponse.getListObject().size() > 1) {
//                                        headerButton.setBackgroundResource(R.drawable.menu_apptheme);
                                        headerButton.setBackgroundResource(0);
                                        headerButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.menu_apptheme, 0);
                                    } else if (mDashBoardCardsResponse.getListObject().size() == 1) {
//                                        headerButton.setBackgroundResource(R.drawable.menu_white);
                                        headerButton.setBackgroundResource(0);
                                        headerButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.menu_white, 0);
                                    }
                                }
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                params.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                                headerButton.setLayoutParams(params);
                            } else if (controlObj.getId().trim().equalsIgnoreCase(AxSysConstants.DBC_ADDNEW_BTN_ID)) {
                                // TODO: get assets for this
                                headerButton.setText(controlObj.getText());
                                headerButton.setTextSize(getViewTextSize(controlObj.getStyle()));
                                headerButton.setTypeface(headerButton.getTypeface(), Typeface.BOLD); // TODO need to make it dynamic
                                headerButton.setLayoutParams(setViewLayoutParams(isHeader ? mHeaderWidth : mScreenWidth, controlObj));
                            } else {
                                headerButton.setText(controlObj.getText());
                                headerButton.setTextSize(getViewTextSize(controlObj.getStyle()));
                                headerButton.setTypeface(headerButton.getTypeface(), Typeface.BOLD); // TODO need to make it dynamic
                                headerButton.setLayoutParams(setViewLayoutParams(isHeader ? mHeaderWidth : mScreenWidth, controlObj));
                            }
                        }

                        headerLayout.addView(headerButton);

                        headerButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (CommonUtils.hasInternet()) {
                                    if (view instanceof Button) {
                                        String action = controlObj.getAction();
                                        // this method can be re-used for action on Menu items in the case of Menu in card header
                                        executeActionOnCards(view, action);
                                    }
                                } else {
                                    showToast(R.string.error_network_required);
                                }
                            }
                        });
                    }
                }
            }
        }

        return headerLayout;
    }

    private void executeActionOnCards(View view, String action) {
        if (!TextUtils.isEmpty(action)) {
            GetDashBoardCardsResponse.Control controlObj = (GetDashBoardCardsResponse.Control) view.getTag(R.id.control_object);
            int cardIndex = (int) view.getTag(R.id.card_index);

            if (action.trim().equalsIgnoreCase("Navigate") || action.trim().equalsIgnoreCase("NavigateTo")) {
                // ADD
                GetDashBoardCardsResponse.Params params = controlObj.getParams();

                if (params != null) {

                    // Call JS method on WebView
                    View cardContainer;
                    if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                        cardContainer = dynamicContentLayout.getChildAt(0);
                    } else {
                        // TODO sometimes app is crashing in this line
                        cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                    }
                    WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                    if (webView != null) {
                        // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                        webView.setTag(R.id.cardaction, action);
                        // set executed event control object to webview so that it will be used once action response if received from server
                        webView.setTag(R.id.executed_control_object, controlObj);

                        // This is an easy way to identify the current executed action once server response is received
                        mExecutedActionButton = (Button) view;
                        mExecutedActionButton.setTag(R.id.cardaction, action);
                    }

                    // If Param object contains 'OutputType' then display card based on it (may be normal, browser, popup)
                    if (!TextUtils.isEmpty(params.getOutputType())) {
                        if (params.getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_BROWSER)) {

                            displayProgressLoader(false);
                            UrlCardAsyncTask urlCardAsyncTask = new UrlCardAsyncTask(!TextUtils.isEmpty(params.getDashboardId()) ? params.getDashboardId() : "",
                                    "1", params.getOutputType().trim(), mFragment);
                            if (isAdded() && getView() != null) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    urlCardAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    urlCardAsyncTask.execute();
                                }
                            }
                        } else if (params.getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_POPUP)) {

                            displayProgressLoader(false);
                            UrlCardAsyncTask urlCardAsyncTask = new UrlCardAsyncTask(!TextUtils.isEmpty(params.getDashboardId()) ? params.getDashboardId() : "",
                                    "1", params.getOutputType().trim(), mFragment);
                            if (isAdded() && getView() != null) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    urlCardAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    urlCardAsyncTask.execute();
                                }
                            }
                        } else /*if(params.getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_NORMAL))*/ {
                            // Display card in dashboard screen
                            mActivity.navigateBasedOnModuleId(mActionBarTitle, "NavigationFragment",
                                    !TextUtils.isEmpty(params.getModuleId()) ? params.getModuleId() : "",
                                    !TextUtils.isEmpty(params.getDashboardId()) ? params.getDashboardId() : "",
                                    !TextUtils.isEmpty(params.getRecordId()) ? params.getRecordId() : "", false, true);
                        }
                    } else {
                        // Display card in dashboard screen
                        mActivity.navigateBasedOnModuleId(mActionBarTitle, "NavigationFragment",
                                !TextUtils.isEmpty(params.getModuleId()) ? params.getModuleId() : "",
                                !TextUtils.isEmpty(params.getDashboardId()) ? params.getDashboardId() : "",
                                !TextUtils.isEmpty(params.getRecordId()) ? params.getRecordId() : "", false, true);
                    }
                }
            } else if (action.trim().equalsIgnoreCase("ClearControls")) {
                // CANCEL
                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);

                    webView.loadUrl("javascript:ClearControls();");
                }
            } else if (action.trim().equalsIgnoreCase("ValidateSave") || action.trim().equalsIgnoreCase("ValidateSaveNavigate")) {
                // SAVE
                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);

                    webView.loadUrl("javascript:ValidateDataCardFields(1);"); // it will return JSON String as response
//                                                webView.loadUrl("javascript:ValidateDataCardFields();"); // it will return JSON String as response
                }
            } else if (action.trim().equalsIgnoreCase("DeleteNavigate")) {
                // DELETE

                // This is an easy way to getParaidentify the current executed action once server response is received
                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);
                }

                // Display confirmation dialog
                final ConfirmationDialogForCardsFragment confirmationDialogFragment = new ConfirmationDialogForCardsFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(ConfirmationDialogForCardsFragment.MESSAGE_KEY, getString(R.string.delete_media_confirmation));
                bundle.putInt(ConfirmationDialogForCardsFragment.DIALOG_ID_KEY, DELETE_CARD_DATA_ID);
                bundle.putString(ConfirmationDialogForCardsFragment.DIALOG_ACTION_KEY, action);
                bundle.putInt(ConfirmationDialogForCardsFragment.DIALOG_CARD_INDEX_KEY, cardIndex);
                bundle.putSerializable(ConfirmationDialogForCardsFragment.DIALOG_CONTROL_OBJ_KEY, controlObj);
                bundle.putBoolean(ConfirmationDialogForCardsFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                confirmationDialogFragment.setArguments(bundle);
                showDialog(confirmationDialogFragment);

            } else if (action.trim().equalsIgnoreCase("NavigatePrevious")) {
                // PREVIOUS action
                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);

                    webView.loadUrl("javascript:getDataPrevious(1);"); // it will return JSON String as response
//                                                webView.loadUrl("javascript:getDataPrevious();"); // it will return JSON String as response
                }

            } else if (action.trim().equalsIgnoreCase("NavigateNext")) {
                // NEXT action
                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);

                    webView.loadUrl("javascript:getDataNext(1);"); // it will return JSON String as response
//                                                webView.loadUrl("javascript:getDataNext();"); // it will return JSON String as response
                }
            } else if (action.trim().equalsIgnoreCase("ValidateNavigatePage") || action.equalsIgnoreCase("NavigatePage")) {
                // WIZARDS : button action
                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }

                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);

                    webView.loadUrl("javascript:ValidateDataCardFields(1);"); // it will return JSON String as response
//                                                webView.loadUrl("javascript:ValidateDataCardFields();"); // it will return JSON String as response
                }
            } else if (action.trim().equalsIgnoreCase("ShowMenu")) {
                // Menu button action

                // Call JS method on WebView
                View cardContainer;
                if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                    cardContainer = dynamicContentLayout.getChildAt(0);
                } else {
                    cardContainer = dynamicContentLayout.getChildAt(cardIndex);
                }
                WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
                if (webView != null) {
                    // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                    webView.setTag(R.id.cardaction, action);
                    // set executed event control object to webview so that it will be used once action response if received from server
                    webView.setTag(R.id.executed_control_object, controlObj);

                    // This is an easy way to identify the current executed action once server response is received
                    mExecutedActionButton = (Button) view;
                    mExecutedActionButton.setTag(R.id.cardaction, action);
                }

                GetCardMenuListTask getCardMenuListTask = new GetCardMenuListTask((Button) view,
                        mActiveUser.getToken(),
                        controlObj.getParams() != null ? (controlObj.getParams().getDashboardId() != null ? controlObj.getParams().getDashboardId() : "") : "");
                if (isAdded() && getView() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        getCardMenuListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        getCardMenuListTask.execute();
                    }
                }
            }
        }
    }

    private class GetCardMenuListTask extends AsyncTask<Void, Void, AppMenuListResponse> {

        private Button clickedButton;
        private String token;
        private String dashboardId;

        private GetDashBoardCardsResponse.Control controlObj;
        private int cardIndex;

        public GetCardMenuListTask(Button clickedButton, String token, String dashboardId) {
            this.clickedButton = clickedButton;
            this.token = token;
            this.dashboardId = dashboardId;

            controlObj = (GetDashBoardCardsResponse.Control) clickedButton.getTag(R.id.control_object);
            cardIndex = (int) clickedButton.getTag(R.id.card_index);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            displayProgressLoader(false);
        }

        @Override
        protected AppMenuListResponse doInBackground(Void... params) {
            try {
                return ServerMethods.getInstance().getCardMenuList(token, dashboardId);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(AppMenuListResponse appMenuListResponse) {
            super.onPostExecute(appMenuListResponse);

            hideProgressLoader();
            if (appMenuListResponse != null) {
                PopupWindow popupWindow = new PopupWindow(mActivity);

                popupWindow.setFocusable(true);
//              popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                popupWindow.setWidth(CommonUtils.dpToPx(250));
                popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

                ListView menuListView = new ListView(mActivity);
                menuListView.setDivider(new ColorDrawable(Color.GRAY));
                menuListView.setDividerHeight(1);
                menuListView.setBackgroundColor(getResources().getColor(R.color.white));

                menuListView.setAdapter(new CardMenuListAdapter(appMenuListResponse, popupWindow));

                // set the list view as pop up window content
                popupWindow.setContentView(menuListView);

                popupWindow.showAsDropDown(clickedButton, 0, 0);
            }
        }

        private class CardMenuListAdapter extends BaseAdapter {

            private AppMenuListResponse appMenuListResponse;
            private PopupWindow popupWindow;

            public CardMenuListAdapter(AppMenuListResponse appMenuListResponse, PopupWindow popupWindow) {
                this.appMenuListResponse = appMenuListResponse;
                this.popupWindow = popupWindow;
            }

            @Override
            public int getCount() {
                return (appMenuListResponse != null && appMenuListResponse.getMenuListDataAL() != null) ?
                        appMenuListResponse.getMenuListDataAL().size() : 0;
            }

            @Override
            public AppMenuListResponse.MenuListData getItem(int position) {
                return appMenuListResponse.getMenuListDataAL().get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final AppMenuListResponse.MenuListData menuListData = getItem(position);

                Button listItem = new Button(mActivity);
                listItem.setBackgroundResource(Color.TRANSPARENT);
                listItem.setPadding(15, 10, 15, 10);

                listItem.setText(menuListData.getText());
                listItem.setTag(menuListData);

                // Set Control object as tag to view, this tag value will be used for Mode Management while editing data on cards
                listItem.setTag(R.id.control_object, controlObj);

                // set card index to button so that we can get it dynamically upon click event
                listItem.setTag(R.id.card_index, cardIndex);

                listItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        executeActionOnCards(view, menuListData.getAction());
                        popupWindow.dismiss();
                    }
                });

                return listItem;
            }
        }
    }

    private int getViewTextSize(GetDashBoardCardsResponse.Style style) {
        int font = 14;

        if (style != null) {
            if (style.getFontsize() != null && !style.getFontsize().equals("null")) {
                if (style.getFontsize().contains("pt")) {
                    String width = style.getFontsize().replace("pt", "");
                    try {
                        width = width.replaceAll("\\s+", "");
                        if (!TextUtils.isEmpty(width)) {
                            font = Integer.parseInt(width.toString());
                        }
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                } else if (style.getFontsize().contains("px")) {
                    String width = style.getFontsize().replace("px", "");
                    try {
                        width = width.replaceAll("\\s+", "");
                        if (!TextUtils.isEmpty(width)) {
                            font = Integer.parseInt(width.toString());
                        }
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                }
            }
        }

        return font;
    }

    private LinearLayout.LayoutParams setViewLayoutParams(int headerWidth, GetDashBoardCardsResponse.Control control) {
        String width = "";

        if (control.getWidth() != null && !control.getWidth().equals("null")) {
            if (!TextUtils.isEmpty(control.getWidth()) && control.getWidth().contains("%")) {
                width = control.getWidth().replace("%", "");
            }
        }

        int viewWidth = 0;
        if (!TextUtils.isEmpty(width)) {
            float perc = Float.parseFloat(width);

            viewWidth = (int) (headerWidth * (perc / 100f));
        } else {
            viewWidth = (int) (headerWidth * (75f / 100f));
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(viewWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        if (control.getTextAlignment() != null) {
            layoutParams.gravity = CommonUtils.mAlignmentHashmap.get(control.getTextAlignment());
        }

        return layoutParams;
    }

    private LinearLayout.LayoutParams setViewLayoutParamsForImageview(int headerWidth, GetDashBoardCardsResponse.Control control) {
        String width = "";

        if (control.getWidth() != null && !control.getWidth().equals("null")) {
            if (!TextUtils.isEmpty(control.getWidth()) && control.getWidth().contains("%")) {
                width = control.getWidth().replace("%", "");
            }
        }

        int viewWidth = 0;
        if (!TextUtils.isEmpty(width)) {
            float perc = Float.parseFloat(width);

            viewWidth = (int) (headerWidth * (perc / 100f));
        } else {
            viewWidth = (int) (headerWidth * (75f / 100f));
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(viewWidth, viewWidth);
        layoutParams.gravity = Gravity.CENTER;
        if (control.getTextAlignment() != null) {
            layoutParams.gravity = CommonUtils.mAlignmentHashmap.get(control.getTextAlignment());
        }

        return layoutParams;
    }

    @SuppressLint("NewApi")
    protected void fixNewAndroid(WebView webView) {
        try {
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        } catch (NullPointerException e) {
        }
    }

    @Override
    public void onConfirm(int dialogId, int cardIndex, String action, GetDashBoardCardsResponse.Control controlObj) {
        if (dialogId == DELETE_CARD_DATA_ID) {
            // Call JS method on WebView
            View cardContainer;
            if (mDashBoardCardsResponse.getIsWizard().trim().equalsIgnoreCase(AxSysConstants.WIZARDS_YES)) {
                cardContainer = dynamicContentLayout.getChildAt(0);
            } else {
                cardContainer = dynamicContentLayout.getChildAt(cardIndex);
            }
            WebView webView = getEmbeddedWebviewFromCardLayout(cardContainer);
            if (webView != null) {
                // set action of the card to webview so that it will be used in WebView Interface to distinguish actions on the webview
                webView.setTag(R.id.cardaction, action);
                // set executed event control object to webview so that it will be used once action response if received from server
                webView.setTag(R.id.executed_control_object, controlObj);

                webView.loadUrl("javascript:getData(1);"); // it will return JSON String as response
//                webView.loadUrl("javascript:getData();"); // it will return JSON String as response
            }
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == DELETE_CARD_DATA_ID) {

        }
    }

    private WebView getEmbeddedWebviewFromCardLayout(View cardContainer) {
        if (cardContainer != null && cardContainer instanceof LinearLayout) {
            LinearLayout cardContainerLayout = (LinearLayout) cardContainer;

            if (cardContainerLayout != null && cardContainerLayout.getChildCount() > 0) {
                for (int idx = 0; idx < cardContainerLayout.getChildCount(); idx++) {
                    View childView = cardContainerLayout.getChildAt(idx);

                    if (childView != null && childView instanceof WebView) {
                        return (WebView) childView;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public void onUrlCardLoadSuccess(GetDashBoardCardsResponse dashBoardCardsResponse, String outputType) {
        hideProgressLoader();

        GetDashBoardCardsResponse.ListObject cardObject = dashBoardCardsResponse.getListObject().get(0);
        if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_URL_DATACRD)) {
            // If card type is URL CARD , just prepare the header, footer and load webview with Service URL
            if (outputType.equalsIgnoreCase(AxSysConstants.URL_CARD_BROWSER)) {
                // Load URL in Browser
                String cardUrl = cardObject.getServiceURL();
                if (cardUrl != null && !cardUrl.startsWith("http")) {
                    cardUrl = "http://" + cardUrl;
                }

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(cardUrl));
                startActivity(browserIntent);

            } else if (outputType.equalsIgnoreCase(AxSysConstants.URL_CARD_POPUP)) {
                // Load Url Card as popup
                String cardUrl = cardObject.getServiceURL();
                if (cardUrl != null && !cardUrl.startsWith("http")) {
                    cardUrl = "http://" + cardUrl;
                }

                CommonUtils.displayWebViewInDialog(mActivity, cardUrl, dashBoardCardsResponse.getName());
            }
        }
    }

    @Override
    public void onUrlCardLoadFailure(String errorMessage) {
        hideProgressLoader();
        showToast(errorMessage);
    }
}
