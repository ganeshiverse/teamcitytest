package com.iversecomics.client.refresh;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class NoopTask extends Task {
    private static final Logger LOG         = LoggerFactory.getLogger(NoopTask.class);
    private long                delayMillis = 0L;

    public NoopTask() {
        this(0L);
    }

    public NoopTask(long delayMillis) {
        this.delayMillis = delayMillis;
    }

    @Override
    public void execTask() {
        LOG.debug("execTask() delay = %d", delayMillis);
        if (delayMillis > 0) {
            try {
                Thread.sleep(delayMillis);
                LOG.debug("delay complete");
            } catch (InterruptedException ignore) {
                /* ignore */
            }
        }
    }
}
