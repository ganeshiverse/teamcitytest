package com.iversecomics.bundle;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.iversecomics.bundle.parse.BundleAccess;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicContent implements Closeable {
    public static class ImageRef {
        /**
         * The offset in the comic content to load the image from.
         */
        public long offset;
        /**
         * reference index.
         * <p>
         * If a page, this references the index in panelRefs pointing to the start of the panels for this page.
         * <p>
         * If a panel, this references the index of the page that this panel belongs to.
         */
        public int  refIndex;
    }

    public class Nav {
        private int            currentIndex = 0;
        private Type           type;
        private List<ImageRef> refs;

        public Nav(Type type) {
            this.type = type;
            this.refs = new ArrayList<ImageRef>();
            this.currentIndex = 0;
        }

        public void add(ImageRef ref) {
            this.refs.add(ref);
        }

        public int getCurrentIndex() {
            return currentIndex;
        }

        public InputStream getImageStream(int index) throws IOException {
            assertContentOpen();
            ImageRef ref = getImageRef(index);
            return getRawImageStream(ref.offset);
        }

        public ImageRef getImageRef(int index) throws IOException {
            if ((index < 0) || (index >= refs.size())) {
                throw new IOException("No such " + type.name() + " index: " + index);
            }

            ImageRef ref = refs.get(index);
            if (ref == null) {
                throw new ArrayIndexOutOfBoundsException("Not a valid " + type.name() + " index [" + index
                        + "] (refs size " + refs.size() + ")");
            }
            
            return ref;
        }

        public InputStream getActive() throws IOException {
            return navigateTo(currentIndex);
        }

        public int getRefIndex() throws IOException {
            ImageRef ref = getImageRef(currentIndex);
            return ref.refIndex;
        }

        public InputStream getNext() throws IOException {
            return navigateTo(currentIndex + 1);
        }

        public InputStream getPrevious() throws IOException {
            return navigateTo(currentIndex - 1);
        }

        public boolean isValidIndex(int index) {
            if (index < 0) {
                return false;
            }

            if (index >= refs.size()) {
                return false;
            }

            return true;
        }

        public InputStream navigateTo(int index) throws IOException {
            InputStream in = getImageStream(index);
            currentIndex = index;
            return in;
        }

        public void reset() {
            currentIndex = -1;
        }

        public int size() {
            return this.refs.size();
        }
    }

    public static enum Type {
        PAGE, PANEL;
    }

    private static final Logger    LOG              = LoggerFactory.getLogger(ComicContent.class);
    private File                   file;
    private transient BundleAccess access;
    private long                   firstImageOffset = -1;
    private Nav                    pageNav          = new Nav(Type.PAGE);
    private Nav                    panelNav         = new Nav(Type.PANEL);

    private int                    status           = ComicMetadata.STATUS_OK;

    public void addPanel(long offset, Type type) {
        ImageRef ref = new ImageRef();
        if (firstImageOffset == (-1)) {
            firstImageOffset = offset; // used for bad AES recovery (ancient bug from 2009)
        }
        ref.offset = offset;
        switch (type) {
            case PAGE:
                ref.refIndex = panelNav.size();
                LOG.debug("Adding page %d [offset 0x%X]", pageNav.size(), offset);
                pageNav.add(ref);
                break;
            case PANEL:
                ref.offset = offset;
                ref.refIndex = pageNav.size();
                LOG.debug("Adding panel %d [offset 0x%X]", panelNav.size(), offset);
                panelNav.add(ref);
                break;
        }
    }

    private void assertContentOpen() {
        if (access == null) {
            throw new IllegalStateException("Content is not opened yet.  Did you use #open() yet?");
        }
    }

    @Override
    public void close() throws IOException {
        IOUtil.close(access);
    }

    public InputStream getCoverStream() throws IOException {
        // Default to page mode
        if(pageNav.isValidIndex(0)) {
            return pageNav.getImageStream(0);
        } else {
            // Fall back to panel mode
            return panelNav.getImageStream(0);
        }
    }

    public File getFile() {
        return file;
    }

    public long getFirstImageOffset() {
        return firstImageOffset;
    }

    public Nav getPageNav() {
        return pageNav;
    }

    public Nav getPanelNav() {
        return panelNav;
    }

    public InputStream getRawImageStream(long offset) throws IOException {
        InputStream in = access.getRawImageStream(offset);
        return in;
    }

    public int getStatus() {
        return status;
    }

    public void open() throws IOException {
        access = new BundleAccess(file, "r");
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
