package com.iversecomics.client.bitmap;

import java.net.URI;

public class LateBitmapRef {
    private String              tag;
    private URI                 bitmapUri;
    private ILateBitmapListener listener;

    public LateBitmapRef(URI bitmapUri, ILateBitmapListener listener) {
        super();
        this.bitmapUri = bitmapUri;
        this.listener = listener;
    }

    public URI getBitmapUri() {
        return bitmapUri;
    }

    public ILateBitmapListener getListener() {
        return listener;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LateBitmapRef [");
        builder.append(bitmapUri);
        if (tag != null) {
            builder.append(" tag=");
            builder.append(tag);
        }
        builder.append(" listener=");
        builder.append(listener.getClass().getName());
        builder.append("]");
        return builder.toString();
    }
}
