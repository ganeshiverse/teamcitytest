package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.InvitationAcceptLoader;
import com.axsystech.excelicare.loaders.InvitationDeclineLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

/**
 * Created by vvydesai on 9/1/2015.
 */
public class InvitationDetailScreen extends BaseFragment implements View.OnClickListener {

    private String TAG = InvitationDetailScreen.class.getSimpleName();
    private InvitationDetailScreen mFragment;
    private MainContentActivity mActivity;

    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mMemberName;

    @InjectSavedState
    private String mDate;

    @InjectSavedState
    private String mMessageText;

    @InjectSavedState
    private String mInvitationType;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mStatus;

    @InjectSavedState
    private String mInviteId;

    @InjectView(R.id.TextViewMemberName)
    private TextView mTextViewMemberName;

    @InjectView(R.id.TextViewinvitationHeader)
    private TextView mTextViewinvitationHeader;

    @InjectView(R.id.textViewFrom)
    private TextView mTextViewFrom;

    @InjectView(R.id.TextViewDate)
    private TextView mTextViewDate;

    @InjectView(R.id.TextViewinvitationDetails)
    private TextView mTextViewInvitationDetails;

    @InjectView(R.id.relativeLayoutFooter)
    private LinearLayout mRelativeLayoutFooter;

    @InjectView(R.id.buttonAccept)
    private ImageButton mButtonAccept;

    @InjectView(R.id.buttonDecline)
    private ImageButton mButtonDecline;

    @InjectView(R.id.memberImageView)
    private ImageView mMemberImageView;

    private static final String INVITATION_ACCEPT = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationDetailScreen.INVITATION_ACCEPT";
    private static final String INVITATION_DECLINE = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationDetailScreen.INVITATION_DECLINE";
    private InvitationStatusChangeListener invitationStatusChangeListener;

    @Override
    protected void initActionBar() {
        Bundle bundle = getArguments();
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_invitation_details, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        updateUi();
        addEventClickListener();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_INVITATION_DATE)) {
                mMemberName = bundle.getString(AxSysConstants.EXTRA_BOOK_INVITATION_MEMBER_NAME);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_INVITATION_DATE)) {
                mDate = bundle.getString(AxSysConstants.EXTRA_BOOK_INVITATION_DATE);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_INVITATION_TEXT)) {
                mMessageText = bundle.getString(AxSysConstants.EXTRA_BOOK_INVITATION_TEXT);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_INVITATION_SENT)) {
                mInvitationType = bundle.getString(AxSysConstants.EXTRA_BOOK_INVITATION_SENT);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_INVITATION_ID)) {
                mInviteId = bundle.getString(AxSysConstants.EXTRA_BOOK_INVITATION_ID);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_MESSAGE_STATUS)) {
                mStatus = bundle.getString(AxSysConstants.EXTRA_BOOK_MESSAGE_STATUS);

            }
        }
    }

    private void updateUi() {
        Picasso.with(getActivity()).load(R.drawable.user_icon).transform(new CircleTransform()).into(mMemberImageView);
        mTextViewMemberName.setText(mMemberName);
        //Split Date
        String string = mDate;
        String[] parts = string.split(",");
        String part1 = parts[0]; // month
        String part2 = parts[1]; // date
        mTextViewDate.setText(part1);
        mTextViewInvitationDetails.setText(mMessageText);

        if (mInvitationType.equals("Invitation Sent")) {
            mRelativeLayoutFooter.setVisibility(View.GONE);
        } else if (mInvitationType.equals("Invitation Received")) {
            mRelativeLayoutFooter.setVisibility(View.VISIBLE);
        }
        if (mStatus.equals("Accepted") || mStatus.equals("Declined")) {
            mRelativeLayoutFooter.setVisibility(View.GONE);
        }
        if (mInvitationType.equals("Invitation Sent")) {
            mTextViewFrom.setText("To:");
        }
    }

    public void addEventClickListener() {
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

        mTextViewFrom.setTypeface(regular);
        mTextViewMemberName.setTypeface(regular);
        mTextViewDate.setTypeface(regular);
        mTextViewinvitationHeader.setTypeface(bold);
        mTextViewInvitationDetails.setTypeface(regular);

        mButtonAccept.setOnClickListener(this);
        mButtonDecline.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonAccept:
                if (CommonUtils.hasInternet()) {
                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                    displayProgressLoader(false);
                    InvitationAcceptLoader invitationAcceptLoader = new InvitationAcceptLoader(mActivity, mActiveUser.getToken(),
                            mInviteId);
                    if (mFragment.isAdded() && mFragment.getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_ACCEPT), invitationAcceptLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;
            case R.id.buttonDecline:
                if (CommonUtils.hasInternet()) {
                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                    displayProgressLoader(false);
                    InvitationDeclineLoader invitationDeclineLoader = new InvitationDeclineLoader(mActivity, mActiveUser.getToken(),
                            mInviteId);
                    if (mFragment.isAdded() && mFragment.getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_DECLINE), invitationDeclineLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }

                break;
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(INVITATION_ACCEPT)) {

            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponse = (BaseResponse) result;

                if (baseResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            if (baseResponse.getMessage() == null) {
                                showToast(R.string.invitation_accepted);
                            } else {
                                showToast(baseResponse.getMessage());
                            }
                            mActivity.onBackPressed();

//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            invitationStatusChangeListener.onInvitationStateChanged();
                        }
                    }, 500);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(INVITATION_DECLINE)) {

            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponse = (BaseResponse) result;

                if (baseResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(baseResponse.getMessage());
                            mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            invitationStatusChangeListener.onInvitationStateChanged();
                        }
                    }, 500);
                }
            }
        }
    }

    public void setInvitationStatusChangeListener(InvitationStatusChangeListener invitationStatusChangeListener) {
        this.invitationStatusChangeListener = invitationStatusChangeListener;
    }

    public interface InvitationStatusChangeListener {
        void onInvitationStateChanged();
    }


}