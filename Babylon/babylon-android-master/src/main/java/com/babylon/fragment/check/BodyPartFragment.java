package com.babylon.fragment.check;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.babylon.R;
import com.babylon.activity.check.CheckActivity;
import com.babylon.database.schema.check.BodyPartColorSchema;
import com.babylon.database.schema.check.BodyPartSchema;
import com.babylon.enums.CheckGender;
import com.babylon.model.check.export.BodyPart;
import com.babylon.utils.L;
import com.babylon.view.TouchImageView;

import java.io.IOException;
import java.io.InputStream;

public class BodyPartFragment extends BaseCheckFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>, TouchImageView.OnTouchImageViewListener {
    private static final String KEY_BODY_SIDE = "body_side";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_COLOR_HEX = "color_hex";
    private static final String KEY_BODY_PART_ID = "body_part_id";

    private static final String TAG = BodyPartFragment.class.getSimpleName();
    private static final int BODY_PART_BY_COLOR_LOADER = 1013;
    private static final int BODY_PART_BY_ID_LOADER = 1014;

    private CheckGender gender;
    private TouchImageView bodyImage;

    private BodySide currentBodySide;

    private int mapDrawableId;

    private String clickedBodyPartName;

    public BodyPartFragment() {
        // Required empty public constructor
    }

    public static BodyPartFragment newInstance(CheckGender gender) {
        BodyPartFragment fragment = new BodyPartFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_GENDER, gender);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            gender = (CheckGender) getArguments().getSerializable(KEY_GENDER);
            if (gender == null) {
                gender = CheckGender.ALL;
            }
        }
        if (savedInstanceState != null) {
            updateFromSavedState(savedInstanceState);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_body_part, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bodyImage = (TouchImageView) view.findViewById(R.id.bodyImage);
        View rotateLeft = view.findViewById(R.id.rotateLeftBtn);
        View rotateRight = view.findViewById(R.id.rotateRightBtn);

        bodyImage.setMaxZoom(6f);
        bodyImage.setOnTouchImageViewListener(this);
        rotateLeft.setOnClickListener(this);
        rotateRight.setOnClickListener(this);

        currentBodySide = currentBodySide != null ? currentBodySide : BodySide.FRONT;
        updateBodyResources(currentBodySide);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateZoomOutButtonButton();
    }

    ///////////////////////////////////////////////////////////////////////////
    // Save state
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(KEY_BODY_SIDE, currentBodySide);
        super.onSaveInstanceState(outState);
    }

    private void updateFromSavedState(Bundle savedInstanceState) {
        currentBodySide = (BodySide) savedInstanceState.getSerializable(KEY_BODY_SIDE);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Loader callbacks
    ///////////////////////////////////////////////////////////////////////////

    @Nullable
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        CursorLoader loader;
        switch (i) {
            case BODY_PART_BY_COLOR_LOADER:
                String colorHex = bundle.getString(KEY_COLOR_HEX);
                String[] selectionArgs = new String[]{colorHex};
                String selection = BodyPartColorSchema.Columns.RGB + "=?";
                loader = new CursorLoader(getActivity(),
                        BodyPartColorSchema.BODY_PART_BY_RGB_CONTENT_URI, BodyPartSchema.Query.PROJECTION, selection, selectionArgs, null);
                break;
            case BODY_PART_BY_ID_LOADER:
                String bodyPartId = bundle.getString(KEY_BODY_PART_ID);
                selectionArgs = new String[]{bodyPartId};
                selection = BodyPartSchema.Columns.ID + "=?";
                loader = new CursorLoader(getActivity(),
                        BodyPartSchema.CONTENT_URI, null, selection, selectionArgs, null);
                break;
            default:
                loader = null;
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (cursor != null) {
            while (cursor.moveToNext()) {
                final BodyPart bodyPart = new BodyPart();
                bodyPart.createFromCursor(cursor);
                getLoaderManager().destroyLoader(BODY_PART_BY_COLOR_LOADER);
                if (TextUtils.isEmpty(clickedBodyPartName)) {
                    clickedBodyPartName = bodyPart.getName();
                }
                if (bodyPart.isUseParentSymptoms()) {
                    findBodyPartById(bodyPart.getParentId());
                } else {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            startSymptomsAnalyzer(bodyPart.getId(), clickedBodyPartName);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    ///////////////////////////////////////////////////////////////////////////
    // UI manipulations
    ///////////////////////////////////////////////////////////////////////////

    public void updateGender(CheckGender gender) {
        this.gender = gender;
        updateBodyResources(currentBodySide);
    }

    public void updateBodyResources(BodySide bodySide) {
        currentBodySide = bodySide;
        int[] bodyResources = getBodyResources(gender, bodySide);
        int bodyDrawableId = bodyResources[0];
        mapDrawableId = bodyResources[1];
        bodyImage.setImageResource(bodyDrawableId);
    }

    private int[] getBodyResources(@NonNull CheckGender gender, @NonNull BodySide bodySide) {
        String packageName = getActivity().getPackageName();
        String genderDescriptor = gender.equals(CheckGender.FEMALE) ? "f" : "m";
        String mapName = String.format("map_%s_%s", genderDescriptor, bodySide.descriptor);
        String bodyName = String.format("body_%s_%s", genderDescriptor, bodySide.descriptor);
        int bodyRes = getResources().getIdentifier(bodyName, "drawable", packageName);
        int mapRes = getResources().getIdentifier(mapName, "drawable", packageName);
        return new int[]{bodyRes, mapRes};
    }

    ///////////////////////////////////////////////////////////////////////////
    // BODY PART PROCESSING LOGIC
    ///////////////////////////////////////////////////////////////////////////

    private void startSymptomsAnalyzer(int bodyPartId, String bodyPartName) {
//    private void startSymptomsAnalyzer(@NonNull BodyPart bodyPart) {
//        int bodyPartId;
//        if (bodyPart.isUseParentSymptoms()) {
//            bodyPartId = bodyPart.getParentId();
//        } else {
//            bodyPartId = bodyPart.getId();
//        }

        BodyPartQuestionFragment questionsFragment
                = BodyPartQuestionFragment.newInstance(gender, bodyPartId, bodyPartName);
        ((CheckActivity) getActivity()).startFragment(questionsFragment,
                true, BodyPartQuestionFragment.BODY_PART_FRAGMENT_TAG,
                R.id.main_activity_fragment_container_layout);
    }

    private void findBodyPartByColor(int color) {
        String hexColor = Integer.toHexString(color);
        if (hexColor.length() >= 8)
            hexColor = hexColor.substring(2);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_COLOR_HEX, hexColor);
        getLoaderManager().restartLoader(BODY_PART_BY_COLOR_LOADER, bundle, this);
    }

    private void findBodyPartById(int bodyPartId) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_BODY_PART_ID, String.valueOf(bodyPartId));
        getLoaderManager().restartLoader(BODY_PART_BY_ID_LOADER, bundle, this);
    }

    ///////////////////////////////////////////////////////////////////////////
    // WORK WITH BITMAP
    ///////////////////////////////////////////////////////////////////////////

    final int TOUCH_WIDTH = 100;
    final int TOUCH_HEIGHT = 100;

    private Bitmap decodeTouchedMapPart(Rect decodeRect) {
        Bitmap bodyMap = null;
        InputStream mapInputStream = getResources().openRawResource(mapDrawableId);
        try {
            BitmapRegionDecoder bitmapRegionDecoder = BitmapRegionDecoder.newInstance(
                    mapInputStream, false);

            BitmapFactory.Options options = new BitmapFactory.Options();
            bodyMap = bitmapRegionDecoder.decodeRegion(decodeRect, options);
            bitmapRegionDecoder.recycle();
            mapInputStream.close();
        } catch (IOException e) {
            L.e(TAG, e.toString());
        }
        return bodyMap;
    }

    private boolean isShouldBeZoomed(Bitmap bodyMapPart) {
        int[] pixels = new int[TOUCH_WIDTH * TOUCH_HEIGHT];
        boolean shouldBeZoomed = false;
        try {
            bodyMapPart.getPixels(pixels, 0, TOUCH_WIDTH,
                    0, 0,
                    TOUCH_WIDTH, TOUCH_HEIGHT);
            boolean alreadyZoomed = bodyImage.getCurrentZoom() == bodyImage.getMaxZoom();
            if (!alreadyZoomed) {
                shouldBeZoomed = hasMultipleColors(pixels);
            }
        } catch (IllegalArgumentException e) {
            L.w(TAG, "Pixels getting are skipped: " + e.toString());
        }
        return shouldBeZoomed;
    }

    @Nullable
    private Rect getTouchRect(PointF pixel) {
        int left = (int) (pixel.x - TOUCH_WIDTH / 2);
        int top = (int) (pixel.y - TOUCH_HEIGHT / 2);
        int right = left + TOUCH_WIDTH;
        int bottom = top + TOUCH_HEIGHT;

        Drawable drawable = bodyImage.getDrawable();
        int originalWidth = drawable.getIntrinsicWidth();
        int originalHeight = drawable.getIntrinsicHeight();

        left = left < 0 ? 0 : left;
        top = top < 0 ? 0 : top;
        right = right > originalWidth ? originalWidth - 1 : right;
        bottom = bottom > originalHeight ? originalHeight - 1 : bottom;

        if (right <= 0 || bottom <= 0 || left >= originalWidth
                || top >= originalHeight) {
            return null;
        } else {
            return new Rect(left, top, right, bottom);
        }
    }

    private boolean hasMultipleColors(int[] pixels) {
        boolean moreThenOneColor = false;
        int lastColor = pixels[0];
        for (int color : pixels) {
            if (color != lastColor) {
                moreThenOneColor = true;
                break;
            }
        }
        return moreThenOneColor;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rotateLeftBtn:
                rotateBody(true);
                break;
            case R.id.rotateRightBtn:
                rotateBody(false);
                break;
        }
    }

    @Override
    public void onMove() {
    }

    @Override
    public void onImageClick(PointF pixel) {
        processBodyImageClick(pixel);
    }

    @Override
    public void onScaleChanged(float scale) {
        updateZoomOutButtonButton();
    }

    private void updateZoomOutButtonButton() {
        CheckActivity activity = (CheckActivity) getActivity();
        if (activity != null) {//check for "qa's chaotic actions" when fragment doesn't have activity yet
            if (bodyImage.getCurrentZoom() > 1) {
                activity.setRightImageButton(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        zoomOut();
                    }
                }, R.drawable.ic_zoom_out);
            } else {
                activity.setRightImageButtonVisible(false);
            }
        }
    }

    private void rotateBody(boolean toLeft) {
        int changeOrdinalDelta = toLeft ? -1 : 1;
        int newOrdinal = currentBodySide.ordinal() + changeOrdinalDelta;
        int lastOrdinal = BodySide.values().length - 1;
        if (newOrdinal > lastOrdinal) {
            newOrdinal = 0;
        }
        if (newOrdinal < 0) {
            newOrdinal = lastOrdinal;
        }
        updateBodyResources(BodySide.values()[newOrdinal]);
    }

    private void processBodyImageClick(PointF pixel) {
        Rect decodeRect = getTouchRect(pixel);
        if (decodeRect != null) {
            Bitmap bodyMapPart = decodeTouchedMapPart(decodeRect);

            boolean shouldBeZoomed = isShouldBeZoomed(bodyMapPart);
            if (shouldBeZoomed) {
                bodyImage.zoomToPixel(pixel);
            } else {
                clickedBodyPartName = null;
                try {
                    int touchedColor = bodyMapPart.getPixel(TOUCH_WIDTH / 2, TOUCH_HEIGHT / 2);
                    findBodyPartByColor(touchedColor);
                } catch (IllegalArgumentException e) {
                    L.e(TAG, e.toString());
                }
            }
        }
    }

    public void zoomOut() {
        bodyImage.animateZoomOut();
    }

    ///////////////////////////////////////////////////////////////////////////
    // Models
    ///////////////////////////////////////////////////////////////////////////

    enum BodySide {
        FRONT("f"), RIGHT("r"), BACK("b"), LEFT("l");
        private final String descriptor;

        BodySide(String descriptor) {
            this.descriptor = descriptor;
        }
    }
}
