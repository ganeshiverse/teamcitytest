package com.iversecomics.client.iab;

/**
 * Created by Gabriel Dogaru.
 */
public class AppStoreAdapterFactory {
    private static AppStoreBuilder appStoreBuilder;

    public static AbstractAppStoreAdapter newInstance() {
        if (appStoreBuilder == null) {
            return null;
        } else {
            return appStoreBuilder.newInstance();
        }
    }

    public static interface AppStoreBuilder {
        AbstractAppStoreAdapter newInstance();
    }

    public static void setAppStoreBuilder(AppStoreBuilder builder){
        appStoreBuilder = builder;
    }
}

