package com.babylon.activity.family;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.activity.BaseActivity;
import com.babylon.activity.PhoneNumberDialogActivity;
import com.babylon.activity.QuestionAttachmentsActivity;
import com.babylon.activity.payments.PaymentTransactionActivity;
import com.babylon.activity.payments.SelectSubscriptionTransactionActivity;
import com.babylon.model.Account;
import com.babylon.model.Patient;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.google.gson.Gson;

import org.json.JSONObject;

public class FamilyAccountsActivity extends BaseActivity implements FamilyAccountsListFragment.FamilyListFragmentListener,
        FamilyAccountsDetailFragment.FamilyAccountDetailListener {

    private static final int PHONE_NUMBER_REQUEST_CODE = 1;
    private static final int IMAGE_PICKER_REQUEST_CODE = 2;
    private static final int PAYMENT_REQUEST_CODE = 3;

    public static final int SWITCH_ACCOUNT_RESULT_CODE = RESULT_FIRST_USER;
    public static final String ACCOUNT_EXTRA_KEY = "ACCOUNT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startFragment(new FamilyAccountsListFragment(), false, FamilyAccountsListFragment.TAG);
    }

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    @Override
    public void onFamilyAccountListItemClicked(Account account) {
        FamilyAccountsDetailFragment detailFragment = FamilyAccountsDetailFragment.newInstance(account);
        startFragment(detailFragment, FamilyAccountsDetailFragment.TAG);
    }

    @Override
    public void onAddFamilyAccountPressed()
    {
        //Mix Panel event for creating family account
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(FamilyAccountsActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.FAMILY_ACCOUNT_CREATED);

        Patient patient = App.getInstance().getPatient();
        if (patient.getPhoneNumber() == null || patient.getPhoneNumber().length() == 0) {
            showPhoneNumberDialog();
        } else {
            startFragment(new FamilyAccountsDetailFragment(), FamilyAccountsDetailFragment.TAG);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PHONE_NUMBER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    startFragment(new FamilyAccountsDetailFragment(), FamilyAccountsDetailFragment.TAG);
                }
                break;
            case IMAGE_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    final String imagePath = data.getStringExtra(QuestionAttachmentsActivity.EXTRA_IMAGE_PATH);
                    if (!TextUtils.isEmpty(imagePath)) {
                        FamilyAccountsDetailFragment fragment = (FamilyAccountsDetailFragment) getSupportFragmentManager().findFragmentByTag(FamilyAccountsDetailFragment.TAG);
                        fragment.notifyAvatarImageChanged(imagePath);
                    }
                }
                break;
            case PAYMENT_REQUEST_CODE:
                getSupportFragmentManager().popBackStackImmediate();
                break;
        }
    }

    private void showPhoneNumberDialog() {
        Intent intent = new Intent(this, PhoneNumberDialogActivity.class);
        startActivityForResult(intent, PHONE_NUMBER_REQUEST_CODE);
    }

    @Override
    public void onImagePickerPressed() {
        Intent i = new Intent(this, QuestionAttachmentsActivity.class);
        i.putExtra(QuestionAttachmentsActivity.ENABLE_AUDIO_EXTRA, false);
        startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);
    }

    @Override
    public void onSwitchAccountPressed(Account account) {
        Intent intent = new Intent();
        Gson gson = new Gson();
        String accountJson = gson.toJson(account, Account.class);
        intent.putExtra(ACCOUNT_EXTRA_KEY, accountJson);
        setResult(SWITCH_ACCOUNT_RESULT_CODE, intent);
        finish();
    }

    @Override
    public void onAccountAdded(Patient newPatient) {
        //Start the payment process
        Intent intent = new Intent(this, SelectSubscriptionTransactionActivity.class);
        intent.putExtra(PaymentTransactionActivity.PATIENT_EXTRA, newPatient);
        startActivityForResult(intent, PAYMENT_REQUEST_CODE);
    }
}
