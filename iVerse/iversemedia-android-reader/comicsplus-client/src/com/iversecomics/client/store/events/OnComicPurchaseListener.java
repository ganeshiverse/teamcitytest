package com.iversecomics.client.store.events;

import com.iversecomics.client.store.model.Comic;

public interface OnComicPurchaseListener {
    public void onComicPurchase(Comic comic);
}
