package com.babylon.model.push;

import com.google.gson.annotations.SerializedName;

public class ItemIdPush extends Push {

    @SerializedName("item_id")
    private String itemId;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
