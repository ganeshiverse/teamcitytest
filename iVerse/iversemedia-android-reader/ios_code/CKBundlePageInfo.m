//
//  CKBundlePageInfo.m
//  Comics
//
//  Created by Derek Stutsman on 7/8/14.
//  Copyright (c) 2014 iVerse Media. All rights reserved.
//

#import "CKBundlePageInfo.h"

@implementation CKBundlePageInfo

#pragma mark - Init/Dealloc
- (instancetype)initWithLocalImageFile:(NSString*)filePath range:(NSRange)range orientation:(CKBundlePageOrientation)orientation
{
    self = [super init];
    if (self)
    {
        self.binaryRange = range;
        self.pageType = CKBundlePageTypeLocalImage;
        self.filePath = filePath;
        self.orientation = orientation;
    }
    return self;
}

- (instancetype)initWithRemoteImageURL:(NSURL*)url orientation:(CKBundlePageOrientation)orientation
{
    self = [super init];
    if (self)
    {
        self.url = url;
        self.pageType = CKBundlePageTypeRemoteImage;
        self.orientation = orientation;        
    }
    return self;
}

- (instancetype)initWithURL:(NSURL*)url
{
    self = [super init];
    if (self)
    {
        self.url = url;
        self.pageType = CKBundlePageTypeURL;
    }
    return self;
}

- (instancetype)initWithVideoURL:(NSURL*)url
{
    self = [super init];
    if (self)
    {
        self.url = url;
        self.pageType = CKBundlePageTypeVideo;
    }
    return self;
}

#pragma mark - NSObject
- (NSString*)description
{
    switch (self.pageType)
    {
        case CKBundlePageTypeLocalImage: return [NSString stringWithFormat:@"LocalPage Orientation: %@ Range: %@", @(self.orientation), NSStringFromRange(self.binaryRange)];
        case CKBundlePageTypeRemoteImage: return [NSString stringWithFormat:@"RemotePage Orientation: %@ URL: %@", @(self.orientation), self.url];
        case CKBundlePageTypeURL: return [NSString stringWithFormat:@"Ad/URL URL: %@", self.url];
        case CKBundlePageTypeVideo: return [NSString stringWithFormat:@"Video URL: %@", self.url];
    }
    return [super description];
}

@end
