package com.babylon.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.babylon.Keys;
import com.babylon.model.Patient;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.PreferencesManager;

public class DatabaseHelper extends BaseSQLiteOpenHelper {

    public static final String CONTENT_AUTHORITY = "com.babylon";
    public static final String TAG = DatabaseHelper.class.getSimpleName();
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String DATABASE_NAME = "com.babylon";
    private static final int DATABASE_VERSION = 35;
    private static DatabaseHelper instance;

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context, getUserDatabaseName(context));
        }
        return instance;
    }

    private DatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder(1024);
        for (Table table : Table.values()) {
            if (appendCreateTableSQL(sql, table)) continue;
            db.execSQL(sql.toString());
            sql.setLength(0);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(DatabaseHelper.class.getSimpleName(), "Upgrading database from v." + oldVersion + " to v." + newVersion);
        /* drop all tables */
        for (Table table : Table.values()) {
            db.execSQL("DROP TABLE IF EXISTS " + table);
        }
        onCreate(db);
    }

    private static String getUserDatabaseName(Context c) {
        String patientJson = PreferencesManager.getInstance(c).getString(Keys.PATIENT, "{}");
        Patient patient = GsonWrapper.getGson().fromJson(patientJson, Patient.class);
        if (patient != null) {
            String userDatabaseName = patient.getEmail();
            if (!TextUtils.isEmpty(userDatabaseName)) {
                return userDatabaseName;
            }
        }

        return DATABASE_NAME;
    }

    public static synchronized void recreateDatabase(Context context) {
        if (instance != null) {
            instance.close();
        }
        String name = getUserDatabaseName(context);
        instance = new DatabaseHelper(context, name);
    }

    public static void deleteDataBase(Context context) {
        context.deleteDatabase(getUserDatabaseName(context));
    }
}