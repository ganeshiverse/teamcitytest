package com.iversecomics.client.refresh;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TaskCollection extends Task {
    private static final Logger LOG = LoggerFactory.getLogger(TaskCollection.class);
    private List<Task> subtasks;

    public TaskCollection() {
        this.subtasks = new ArrayList<Task>();
    }

    public void addTask(Task task) {
        this.subtasks.add(task);
    }

    @Override
    public void execTask() {
        LOG.debug("Executing %d tasks", subtasks.size());
        for (Task subtask : subtasks) {
            LOG.debug("Executing task: %s", subtask.getClass().getSimpleName());
            subtask.execTask();
        }
    }

    public List<Task> getTasks() {
        return subtasks;
    }

    public boolean hasTasks() {
        return !subtasks.isEmpty();
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        for (Task subtask : subtasks) {
            LOG.debug("%s.isFresh() = %b", subtask.getClass().getSimpleName(), subtask.isFresh(freshness));
            if (subtask.isFresh(freshness) == false) {
                return false;
            }
        }
        LOG.debug("All %d tasks report as fresh", subtasks.size());
        return true;
    }

    /**
     * Slightly different logic than others {@link com.iversecomics.client.refresh.Task#execTask()}.
     * <p/>
     * Will pass the refresh() logic into each subtask.
     */
    @Override
    public void refresh(Freshness freshness, boolean force) {
        LOG.debug("Refreshing %d tasks", subtasks.size());
        for (Task subtask : subtasks) {
            LOG.debug("Refreshing task: %s - isFresh=%b", subtask.getClass().getSimpleName(),
                    subtask.isFresh(freshness));
            subtask.refresh(freshness, force);
        }
    }
}
