package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.Response;
import com.babylon.model.Errors;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Response processor for handle and parse facebook alliants requests
 * Such request not follow standard of alliants response structure, especially errors
 */
public class FacebookResponseProcessor<O> extends RubyResponseProcessor<O> {

    public static final String TAG = FacebookResponseProcessor.class.getSimpleName();

    public FacebookResponseProcessor(Type outputType) {
        super(outputType);
    }

    @Override
    protected void addParsedErrors(Response<O> result, String jsonErrors) {
        try {
            Errors errorsResponse = GsonWrapper.getGson().fromJson(jsonErrors, Errors.class);
            if (errorsResponse != null) {
                List<String> errors = errorsResponse.getErrors();
                if (errors != null && errors.size() != 0) {
                    for (String error : errors) {
                        if (!TextUtils.isEmpty(error)) {
                            addError(result, error);
                        }
                    }
                }
            }
        } catch (JsonSyntaxException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }
}
