package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class FacebookGet extends AbsRequestGet<Patient> {

    private String facebookToken;

    public FacebookGet(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }


    @Override
    protected String getUrl() {
        String url = Urls.getRuby(Urls.FACEBOOK_FIELDS);
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("facebook_access_token", facebookToken));
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    @Override
    protected AbsResponseProcessor<Patient> getResponseProcessor() {
        return new FacebookResponseProcessor<Patient>(getOutputType());
    }
}