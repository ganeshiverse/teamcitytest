package com.axsystech.excelicare.framework.ui.messages;

import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;

/**
 * Created by someswarreddy on 02/09/15.
 */
public interface ProvidersSelectionListener {

    public void onProviderSelected(SearchDoctorsResponse.DoctorDetails selectedClinicianObject,
                                   ClinicianDetailsResponse.ClinicianDetailsData clinicianDetailsData);
}
