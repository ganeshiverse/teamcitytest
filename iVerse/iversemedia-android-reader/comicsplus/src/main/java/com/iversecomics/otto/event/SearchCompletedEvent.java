package com.iversecomics.otto.event;

/**
 */
public class SearchCompletedEvent {

    private final String query;

    public SearchCompletedEvent(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
