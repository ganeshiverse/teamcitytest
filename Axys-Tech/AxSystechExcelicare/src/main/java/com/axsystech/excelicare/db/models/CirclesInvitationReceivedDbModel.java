package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.axsystech.excelicare.util.CryptoEngine;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by vvydesai on 9/10/2015.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CIRCLES_INVITATION_RECEIVED)
public class CirclesInvitationReceivedDbModel extends DBUtils implements Serializable {

    private static final long serialVersionUID = 2L;

    @DatabaseField(id = true, columnName = CIRCLES_INVITATION_RECEIVED_USERID)
    private String invitationUserId;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_INVITEID)
    private String invioteId;

    @DatabaseField(columnName = CIRCLES_INVITATION_MESSAGE_RECEIVED_STATUS_SLU)
    private String messageReceivedStatusSlu;

    @DatabaseField(columnName = CIRCLES_INVITATION_MESSAGE_SENT_STATUS_SLU)
    private String messageSentStatusSlu;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_MESSAGE_TEXT)
    private String messageText;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_RECIPIENT_MAILADDRESS)
    private String recipientMailAddress;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_RECIPIENT_FORENAME)
    private String recipientForenameAddress;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_RECIPIENT_SURNAME)
    private String recipientSurnameAddress;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_ROLESLU)
    private String roleSlu;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_SENDDATE)
    private String sendDate;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_SENDER_FORENAME)
    private String senderForeName;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_SENDER_NAME)
    private String senderName;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_SENDER_SURNAME)
    private String senderSurName;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_SENDER_USERID)
    private String senderUserId;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_SITEID)
    private String siteId;

    @DatabaseField(columnName = CIRCLES_INVITATION_STATUS_UPDATED_DATE)
    private String updatedDate;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_URL)
    private String receivedUrl;

    @DatabaseField(columnName = CIRCLES_INVITATION_RECEIVED_USERCIRCLEID)
    private String receivedUserCircleUrl;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getInvitationUserId() throws Exception {
        return CryptoEngine.decrypt(invitationUserId);
    }

    public void setInvitationUserId(String invitationUserId) throws Exception {
        this.invitationUserId = CryptoEngine.encrypt(invitationUserId);
    }

    public String getInvioteId() throws Exception {
        return CryptoEngine.decrypt(invioteId);
    }

    public void setInvioteId(String invioteId) throws Exception {
        this.invioteId = CryptoEngine.encrypt(invioteId);
    }

    public String getMessageReceivedStatusSlu() throws Exception {
        return CryptoEngine.decrypt(messageReceivedStatusSlu);
    }

    public void setMessageReceivedStatusSlu(String messageReceivedStatusSlu) throws Exception {
        this.messageReceivedStatusSlu = CryptoEngine.encrypt(messageReceivedStatusSlu);
    }

    public String getMessageSentStatusSlu() throws Exception {
        return CryptoEngine.decrypt(messageSentStatusSlu);
    }

    public void setMessageSentStatusSlu(String messageSentStatusSlu) throws Exception {
        this.messageSentStatusSlu = CryptoEngine.encrypt(messageSentStatusSlu);
    }

    public String getMessageText() throws Exception {
        return CryptoEngine.decrypt(messageText);
    }

    public void setMessageText(String messageText) throws Exception {
        this.messageText = CryptoEngine.encrypt(messageText);
    }

    public String getRecipientMailAddress() throws Exception {
        return CryptoEngine.decrypt(recipientMailAddress);
    }

    public void setRecipientMailAddress(String recipientMailAddress) throws Exception {
        this.recipientMailAddress = CryptoEngine.encrypt(recipientMailAddress);
    }

    public String getRecipientForenameAddress() throws Exception {
        return CryptoEngine.decrypt(recipientForenameAddress);
    }

    public void setRecipientForenameAddress(String recipientForenameAddress) throws Exception {
        this.recipientForenameAddress = CryptoEngine.encrypt(recipientForenameAddress);
    }

    public String getRecipientSurnameAddress() throws Exception {
        return CryptoEngine.decrypt(recipientSurnameAddress);
    }

    public void setRecipientSurnameAddress(String recipientSurnameAddress) throws Exception {
        this.recipientSurnameAddress = CryptoEngine.encrypt(recipientSurnameAddress);
    }

    public String getRoleSlu() throws Exception {
        return CryptoEngine.decrypt(roleSlu);
    }

    public void setRoleSlu(String roleSlu) throws Exception {
        this.roleSlu = CryptoEngine.encrypt(roleSlu);
    }

    public String getSendDate() throws Exception {
        return CryptoEngine.decrypt(sendDate);
    }

    public void setSendDate(String sendDate) throws Exception {
        this.sendDate = CryptoEngine.encrypt(sendDate);
    }

    public String getSenderForeName() throws Exception {
        return CryptoEngine.decrypt(senderForeName);
    }

    public void setSenderForeName(String senderForeName) throws Exception {
        this.senderForeName = CryptoEngine.encrypt(senderForeName);
    }

    public String getSenderName() throws Exception {
        return CryptoEngine.decrypt(senderName);
    }

    public void setSenderName(String senderName) throws Exception {
        this.senderName = CryptoEngine.encrypt(senderName);
    }

    public String getSenderSurName() throws Exception {
        return CryptoEngine.decrypt(senderSurName);
    }

    public void setSenderSurName(String senderSurName) throws Exception {
        this.senderSurName = CryptoEngine.encrypt(senderSurName);
    }

    public String getSenderUserId() throws Exception {
        return CryptoEngine.decrypt(senderUserId);
    }

    public void setSenderUserId(String senderUserId) throws Exception {
        this.senderUserId = CryptoEngine.encrypt(senderUserId);
    }

    public String getSiteId() throws Exception {
        return CryptoEngine.decrypt(siteId);
    }

    public void setSiteId(String siteId) throws Exception {
        this.siteId = CryptoEngine.encrypt(siteId);
    }

    public String getUpdatedDate() throws Exception {
        return CryptoEngine.decrypt(updatedDate);
    }

    public void setUpdatedDate(String updatedDate) throws Exception {
        this.updatedDate = CryptoEngine.encrypt(updatedDate);
    }

    public String getReceivedUrl() throws Exception {
        return CryptoEngine.decrypt(receivedUrl);
    }

    public void setReceivedUrl(String receivedUrl) throws Exception {
        this.receivedUrl = CryptoEngine.encrypt(receivedUrl);
    }

    public String getReceivedUserCircleUrl() throws Exception {
        return CryptoEngine.decrypt(receivedUserCircleUrl);
    }

    public void setReceivedUserCircleUrl(String receivedUserCircleUrl) throws Exception {
        this.receivedUserCircleUrl = CryptoEngine.encrypt(receivedUserCircleUrl);
    }
}
