package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.InvitationReceivedDataResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/27/2015.
 */
public class CircleMembersListSearchLoader extends DataAsyncTaskLibLoader<ListMembersInCircleResponse> {

    private String token;
    private String criteria;
    private int pageIndex;
    private String pageSize;
    private String userCircleid;

    public CircleMembersListSearchLoader(Context context, String criteria, int pageIndex, String pageSize, String token, String userCircleid) {
        super(context);
        this.token = token;
        this.criteria = criteria;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.userCircleid = userCircleid;
    }

    @Override
    protected ListMembersInCircleResponse performLoad() throws Exception {
        return ServerMethods.getInstance().circleMembersListSearch(criteria, pageIndex, pageSize, token, userCircleid);
    }
}