package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.ProxyListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by bhargav on 7/29/2015.
 */
public class GetMsgComposeLoader extends DataAsyncTaskLibLoader<ProxyListResponse> {

    private String token;

    public GetMsgComposeLoader(Context context, String token) {
        super(context);
        this.token = token;

    }

    @Override
    protected ProxyListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMessageComposeList(token);
    }
}
