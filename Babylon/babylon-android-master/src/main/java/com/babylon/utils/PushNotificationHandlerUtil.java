package com.babylon.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.PushBroadcastReceiver;
import com.babylon.activity.BaseActivity;
import com.babylon.activity.QuestionActivity;
import com.babylon.activity.SignInActivity;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.AppointmentGet;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.VideoSessionGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.model.payments.Appointment;
import com.babylon.model.push.AppointmentPush;
import com.babylon.model.push.ItemIdPush;
import com.babylon.model.push.NotifAskConsultant;
import com.babylon.model.push.SpecialismPush;
import com.babylon.sync.SyncUtils;
import com.babylonpartners.babylon.HomePageBackboneActivity;
import com.babylonpartners.babylon.VideoSessionActivity;
import com.google.gson.Gson;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahulshah on 04/06/15.
 */
public class PushNotificationHandlerUtil
{
    private Context mContext;
    public static Activity homePageActivityInstance;

    //Constants
    private final static int REQUEST_CODE_VIDEO_SESSION = 2;

    public PushNotificationHandlerUtil(Context context)
    {
        mContext = context;
    }

    //Check if user sign in(execute server request for get 401 status of authorized or not
    public void processParsePushNotificationData(Bundle extras)
    {
        final String data = extras.getString(PushBroadcastReceiver.KEY_PARSE_DATA);
        if (!TextUtils.isEmpty(data))
        {
            String action = PushBroadcastReceiver.getAction(data);
            if (!TextUtils.isEmpty(action))
            {
                final PushBroadcastReceiver.PushAction pushAction = PushBroadcastReceiver.PushAction.getValue(action.replace("com.babylon.", ""));
                if (pushAction != null)
                {
                    if (App.getInstance().isConnection())
                    {
                        PreferencesManager.getInstance().setManualHandlingAutoSignOut(true);
                        new PatientGet().execute((FragmentActivity)mContext, new RequestListener<Patient>()
                        {
                            @Override
                            public void onRequestComplete(Response<Patient> response)
                            {
                                boolean isUnauthorized = response.getStatus() == HttpStatus.SC_UNAUTHORIZED;
                                if (isUnauthorized)
                                {
                                    SyncUtils.signOut();
                                }
                                PreferencesManager.getInstance().setManualHandlingAutoSignOut(false);
                                handlePushNotification(pushAction, data, isUnauthorized);
                            }
                        });
                    }
                    else
                    {
                        handlePushNotification(pushAction, data, false);
                    }
                }
            }
        }
    }

    private void handlePushNotification(PushBroadcastReceiver.PushAction pushAction,String parseData, boolean isUnauthorized)
    {
        //Mix Panel event
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(mContext,props,null);
        helper.sendEvent(MixPanelEventConstants.NOTIFICATION_READ);

        if(!(SyncUtils.isLoggedIn() && !isUnauthorized))
        {
            startSignInActivity(pushAction, parseData);
        }
        else
        {
            switch (pushAction)
            {
                case APPOINTMENT_START:
                    App.getInstance().stopMediaPlayer();
                    videoAppointmentView(parseData);
                    break;

                case APPOINTMENT_REMINDER:
                    bookAnAppointmentView(parseData);
                    break;

                case SUGGEST_CONSULTATION_AFTER_ARCHIVING:
                    bookAnAppointmentView(null);
                    break;

                case SUGGEST_CONSULTATION:
                case ADD_COMMENT:
                case REJECT_QUESTION:
                case APPROVE_QUESTION:

                    //Send Event to MixPanel for ASK
                    /*JSONObject props = new JSONObject();
                    MixPanelHelper helper = new MixPanelHelper(mContext,props);
                    helper.sendEvent(MixPanelEventConstants.USER_RECEIVES_REPLY);*/

                    final NotifAskConsultant data = new Gson().fromJson(parseData, NotifAskConsultant.class);
                    String questionId = data.getQuestionId();

                    //Patch request to remove home screen notification
                    clearNotifications("ask");

                    startQuestionActivityWithQuestion(questionId);
                    break;

                case BLOOD_ANALYSIS_HEALTHY:

                    //Patch request to remove home screen notification
                    clearNotifications("monitor");


                    Intent intent = new Intent(mContext,BaseActivity.class);
                    intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.MONITOR_ME_FRAGMENT_TAG);
                    mContext.startActivity(intent);
                    break;

                case BLOOD_ANALYSIS_APPOINTMENT:

                    //Patch request to remove home screen notification
                    clearNotifications("consultation");


                    bookAnAppointmentBloodResultView();
                    break;

                case SUBSCRIPTION:
                    showSubscriptionPage();
                    break;

                case NEW_PRESCRIPTION:
                    startNewPrescriptionScreen(parseData);
                    break;

                case REFERRAL:
                    startReferralScreen(parseData);
                    break;

                case PATHOLOGY_TEST:
                    startPathologyTestScreen(parseData);
                    break;

                case ACCEPT_UNVERIFIED_PATIENT:
                    bookAnAppointmentView(null);
                    break;
                default:
                    ((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index,"");
                    //loadEnvironment(HomePageBackboneActivity.HtmlPage.index);
                    break;
            }
        }
    }

    //if null open view for booking appointment else open already created appointment with id
    public void bookAnAppointmentView(String data)
    {
        AppointmentPush parseData = null;
        if (data != null)
        {
            parseData = GsonWrapper.getGson().fromJson(data,AppointmentPush.class);
        }

        StringBuilder params = new StringBuilder("#appointments/");
        if (parseData == null || TextUtils.isEmpty(parseData.getAppointmentId()))
        {
            params.append("new");
        }
        else
        {
            params.append(parseData.getAppointmentId());
        }
        ((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index, params.toString());
        //loadEnvironment(HomePageBackboneActivity.HtmlPage.index, params.toString());
    }

    public void videoAppointmentView(String data)
    {
        AppointmentPush parseData = null;
        if (data != null)
        {
            parseData = GsonWrapper.getGson().fromJson(data, AppointmentPush.class);
        }

        boolean dataEmpty = (parseData == null || TextUtils.isEmpty(parseData.getAppointmentId()));
        if (dataEmpty)
        {
            return;
        }

        String appointmentId = parseData.getAppointmentId();

        final AppointmentGet appointmentGet = new AppointmentGet(appointmentId);
        appointmentGet.execute((FragmentActivity)mContext, new RequestListener<Appointment>()
        {
            @Override
            public void onRequestComplete(Response<Appointment> response)
            {
                if (response.isSuccess())
                {
                    Appointment appointment = response.getData();
                    fetchAndStartVideoSession(appointment);
                }
                ((FragmentActivity)mContext).getSupportLoaderManager().destroyLoader(appointmentGet.hashCode());
            }
        });
    }

    private void fetchAndStartVideoSession(Appointment appointment)
    {
        final VideoSessionGet videoSessionGet = new VideoSessionGet(appointment.videoSessionId());

        videoSessionGet.execute((FragmentActivity) mContext, new RequestListener<com.babylon.model.VideoSession>() {
            @Override
            public void onRequestComplete(Response<com.babylon.model.VideoSession> videoResponse) {
                if (videoResponse.isSuccess()) {
                    com.babylon.model.VideoSession videoSession = videoResponse.getData();
                    startVideoSession(videoSession);
                }
                ((FragmentActivity) mContext).getSupportLoaderManager().destroyLoader(videoSessionGet.hashCode());
            }
        });
    }

    private void startVideoSession(com.babylon.model.VideoSession videoSession)
    {
        Intent videoIntent = new Intent(mContext, VideoSessionActivity.class);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_SESSION_ID, videoSession.session());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_TOKEN, videoSession.uniqueToken());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_IDENTIFIER, videoSession.id());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_NAME, videoSession.consultantName());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_AVATAR_URL, videoSession.consultantAvatarUrl());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_ENABLE_VOICE_CALLS, videoSession.enabledVoiceCalls());

        //startedVideoSession = videoSession;
        ((FragmentActivity)mContext).startActivityForResult(videoIntent,REQUEST_CODE_VIDEO_SESSION);
    }

    public void startNewPrescriptionScreen(String notificationJsonData)
    {
        if (notificationJsonData != null)
        {
            //Mix Panel event for creating family account
            JSONObject props = new JSONObject();
            MixPanelHelper helper = new MixPanelHelper(App.getInstance(),props,null);
            helper.sendEvent(MixPanelEventConstants.PRESCRIPTION_OPENED);

            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.PRESCRIPTION_OPENED);
            ItemIdPush pushNotification = GsonWrapper.getGson().fromJson(notificationJsonData,ItemIdPush.class);
            if (pushNotification != null && !TextUtils.isEmpty(pushNotification.getItemId())) {
                /*((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index, "#prescriptions/" + pushNotification
                        .getItemId());*/
                //loadEnvironment(HomePageBackboneActivity.HtmlPage.index, "#prescriptions/" + pushNotification.getItemId());


                Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
                homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
                homePageIntent.putExtra("PAGE2", BackbonePageConstants.SHOW_PRESCRIPTIONS);
                homePageIntent.putExtra("TOPBAR", "Prescription");
                homePageIntent.putExtra("URL_FORMAT_NEEDED", pushNotification.getItemId());
                mContext.startActivity(homePageIntent);
            }
        }
    }

    public void startReferralScreen(String notificationJsonData)
    {
        if (notificationJsonData != null)
        {
            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.REFERRAL_OPENED);
            SpecialismPush pushNotification = GsonWrapper.getGson().fromJson(notificationJsonData,SpecialismPush.class);
            if (pushNotification != null && !TextUtils.isEmpty(pushNotification.getSpecialism()))
            {
                ((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index,"#referral-notifications/" + Uri.encode
                        (pushNotification.getSpecialism()));
                //loadEnvironment(HomePageBackboneActivity.HtmlPage.index, "#referral-notifications/" + Uri.encode(pushNotification.getSpecialism()));
            }
        }
    }

    public void startPathologyTestScreen(String notificationJsonData)
    {
        if (notificationJsonData != null)
        {
            ItemIdPush pushNotification = GsonWrapper.getGson().fromJson(notificationJsonData,ItemIdPush.class);
            if (pushNotification != null && !TextUtils.isEmpty(pushNotification.getItemId()))
            {
                Intent homePageIntent = new Intent(mContext, HomePageBackboneActivity.class);
                homePageIntent.putExtra("TOPBAR", "Pathology");
                homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
                homePageIntent.putExtra("PAGE2", BackbonePageConstants.PATHOLOGY_SHOW);
                homePageIntent.putExtra("URL_FORMAT_NEEDED", pushNotification.getItemId());
                mContext.startActivity(homePageIntent);
                /*((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index,"#pathology-tests/" + pushNotification
                        .getItemId());*/
                //loadEnvironment(HomePageBackboneActivity.HtmlPage.index, "#pathology-tests/" + pushNotification.getItemId());
            }
        }
    }

    private void startSignInActivity(PushBroadcastReceiver.PushAction pushAction, String parseData)
    {
        //TO DO: check if this is needed
        //hideWebAppProgressBar();

        Intent intent = new Intent(mContext, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Keys.NOTIFICATION_DATA, pushAction);
        intent.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, parseData);
        mContext.startActivity(intent);
    }

    public void bookAnAppointmentBloodResultView()
    {
       /* final String params = "#appointments/new?type=bloodtest";
        ((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index,params);*/
        Intent homePageIntent = new Intent(mContext,HomePageBackboneActivity.class);
        homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
        homePageIntent.putExtra("PAGE2", BackbonePageConstants.BLOOD_ANALYSIS_BOOK_APPOINTMENT);
        mContext.startActivity(homePageIntent);
        //loadEnvironment(HomePageBackboneActivity.HtmlPage.index, params);
    }

    public void showSubscriptionPage()
    {
        ((IHomePageControl) mContext).webViewLoadUrl(HomePageBackboneActivity.HtmlPage.index, "#subscriptions");
        //loadEnvironment(HomePageBackboneActivity.HtmlPage.index, "#subscriptions");
    }

    public void startQuestionActivityWithQuestion(String questionId)
    {
        Intent intent = new Intent(mContext, QuestionActivity.class);
        intent.putExtra(Keys.QUESTION_ID, questionId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

    public interface IHomePageControl
    {
        void webViewLoadUrl(HomePageBackboneActivity.HtmlPage url,String params);
    }

    private void clearNotifications(String groupname)
    {
        String mUri= CommonHelper.getUrl(Urls.CLEAR_NOTIFICATIONS);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, mUri, getJSONObject(groupname),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Adapter", "Error");

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }
    }

    public JSONObject getJSONObject(String groupName)
    {
        JSONObject apiJsonObject = new JSONObject();

        try
        {
            apiJsonObject.put("group",groupName);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return apiJsonObject;
    }
}
