package com.babylon.chart;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.webkit.WebView;

import com.babylon.utils.L;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Stack;
import java.util.WeakHashMap;


public class WebViewLoader {
    public static final String TAG = "WebViewLoader";
    private final String defaultPage = "<html><body>PAGE NOT FOUND</body></html>";
    private FileCache fileCache;
    private static WebViewLoader instance = null;
    private MemoryCache memoryCache = new MemoryCache();
    private Map<WebView, String> webViews = Collections.synchronizedMap(new WeakHashMap<WebView, String>());

    public static synchronized WebViewLoader getInstance() {
        if (instance == null) {
            instance = new WebViewLoader();
        }
        return instance;
    }

    public WebViewLoader() {
        webPageLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);
        fileCache = new FileCache();
    }

    public void LoadWebView(Activity activity, String url, WebView webView) {
        webViews.put(webView, url);

        String htmlData = memoryCache.get(url);
        if (htmlData != null && !htmlData.equals("")) {
            Log.i(TAG, "Load page from memory cache");
            webView.loadDataWithBaseURL(url, htmlData, "text/html", "utf-8", url);
        } else {
            queueWebView(url, activity, webView);

        }
    }

    private void queueWebView(String url, Activity activity, WebView webView) {
        // This WebView may be used for other pages before. So there may be
        // some old tasks in the queue. We need to discard them.
        webViewQueue.Clean(webView);
        WebViewToLoad p = new WebViewToLoad(url, webView);
        synchronized (webViewQueue.webViewToLoad) {
            webViewQueue.webViewToLoad.push(p);
            webViewQueue.webViewToLoad.notifyAll();
        }

        // start thread if it's not started yet
        if (webPageLoaderThread.getState() == Thread.State.NEW)
            webPageLoaderThread.start();
    }

    public void stopThread() {
        webPageLoaderThread.interrupt();
    }

    WebViewQueue webViewQueue = new WebViewQueue();

    // stores list of pages to download
    class WebViewQueue {
        private Stack<WebViewToLoad> webViewToLoad = new Stack<WebViewToLoad>();

        // removes all instances of this WebViewView
        public void Clean(WebView webView) {
            for (int j = 0; j < webViewToLoad.size(); ) {
                if (webViewToLoad.get(j).webView == webView)
                    webViewToLoad.remove(j);
                else
                    ++j;
            }
        }
    }

    WebPageLoader webPageLoaderThread = new WebPageLoader();

    class WebPageLoader extends Thread {
        public void run() {
            try {
                while (true) {
                    // thread waits until there are any pages to load in the
                    // queue
                    if (webViewQueue.webViewToLoad.size() == 0)
                        synchronized (webViewQueue.webViewToLoad) {
                            webViewQueue.webViewToLoad.wait();
                        }
                    if (webViewQueue.webViewToLoad.size() != 0) {
                        WebViewToLoad webViewToLoad;
                        synchronized (webViewQueue.webViewToLoad) {
                            webViewToLoad = webViewQueue.webViewToLoad.pop();
                        }

                        String data = getHTML(webViewToLoad.url);
                        memoryCache.put(webViewToLoad.url, data);
                        String tag = webViews.get(webViewToLoad.webView);
                        if (tag != null && tag.equals(webViewToLoad.url)) {
                            WebViewDisplayer bd = new WebViewDisplayer(data, webViewToLoad);
                            Activity a = (Activity) webViewToLoad.webView.getContext();
                            a.runOnUiThread(bd);
                        }
                    }
                    if (Thread.interrupted())
                        break;
                }
            } catch (InterruptedException e) {
                // allow thread to exit
                L.e(TAG, e.toString(), e);
            }
        }
    }

    // Used to display page data in the UI thread
    class WebViewDisplayer implements Runnable {
        String data;
        WebViewToLoad webViewToLoad;

        public WebViewDisplayer(String d, WebViewToLoad w) {
            data = d;
            webViewToLoad = w;
        }

        public void run() {
            String tag = webViews.get(webViewToLoad.webView);
            if (tag == null || !tag.equals(webViewToLoad.url))
                return;

            if (data != null && !data.equals(""))
                webViewToLoad.webView.loadDataWithBaseURL(webViewToLoad.url, data, "text/html", "utf-8", webViewToLoad.url);
            else
                webViewToLoad.webView.loadData(defaultPage, "text/html", "utf-8");

        }
    }

    // Task for the queue
    private class WebViewToLoad {
        public String url;
        public WebView webView;

        public WebViewToLoad(String u, WebView w) {
            url = u;
            webView = w;
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    public File getCacheDir() {
        return fileCache.getCacheDir();
    }

    public File getFileFromCache(String url) {
        return fileCache.getFile(url);
    }

    private String getHTML(String url) {

        File f = fileCache.getFile(url);

        // from SD cache
        if (f != null && f.exists()) {
            Log.i(TAG, "Load page from file cache");
            return readFileToString(f);
        }
        Log.i(TAG, "Load page from server");
        try {
            URL webViewUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) webViewUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            copyStream(is, os);
            os.close();
            return readFileToString(f);

        } catch (Exception e) {
            L.e(TAG, "Error load file. URL: " + url, e);
            return "";
        }
    }

    public File saveFileFromString(String url, String data) {
        File f = fileCache.getFile(url);

        OutputStream os = null;
        try {
            os = new FileOutputStream(f);
            os.write(data.getBytes());
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        }

        return f;
    }

    private String readFileToString(File file) {
        InputStream is;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            L.e(TAG, "Error open file: " + file.getAbsolutePath(), e);
            return "";
        }

        InputStreamReader in = new InputStreamReader(is);
        BufferedReader r = new BufferedReader(in);

        try {
            in.close();
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }

        StringBuilder result = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                result.append(line);
            }
            try {
                r.close();
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        } catch (IOException e) {
            L.e(TAG, "Error read file: " + file.getAbsolutePath(), e);
        }
        return result.toString();
    }

    public static void copyStream(InputStream is, OutputStream os) {
        final int bufferSize = 1024;
        try {
            // Transfer bytes from in to out
            byte[] buf = new byte[bufferSize];
            int len;
            while ((len = is.read(buf)) > 0) {
                os.write(buf, 0, len);
            }
        } catch (Exception e) {
            L.e(TAG, "CopyStream error" + e.getMessage(), e);
        }
    }

    public File saveRawResourceInFile(Context context, int resId, File resultFile) {
        InputStream is;
        OutputStream os;
        try {
            is = context.getResources().openRawResource(resId);
            os = new FileOutputStream(resultFile);
            copyStream(is, os);
            os.close();
        } catch (Exception e) {
            L.e(TAG, "error copyRsourceToFile", e);
        }

        return resultFile;
    }

    public Map<WebView, String> getWabView(String url) {
        return webViews;
    }

}
