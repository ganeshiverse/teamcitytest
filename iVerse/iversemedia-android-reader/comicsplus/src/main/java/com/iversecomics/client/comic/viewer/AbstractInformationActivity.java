package com.iversecomics.client.comic.viewer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public abstract class AbstractInformationActivity extends Activity {
//    private static final int    CLOSE                   = 1;
//    private static final int    MORE_TITLES             = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setFullscreen();

        //setContentView(R.layout.comic_info);
    }

    private void setFullscreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case CLOSE:
//                finish();
//                break;
//            case MORE_TITLES:
//                MarketUtil.openMoreTitles(this);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(final Menu menu) {
//        super.onCreateOptionsMenu(menu);
//
//        // Create menus (in order)
//        final MenuItem menuClose = menu.add(0, CLOSE, 0, R.string.menu_close);
//        final MenuItem menuMoreTitles = menu.add(0, MORE_TITLES, 1, R.string.menu_more_comics);
//
//        // Establish icons for menus
//        menuClose.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
//        menuMoreTitles.setIcon(R.drawable.iverse_blue_i);
//
//        return true;
//    }
}
