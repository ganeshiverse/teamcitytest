package com.babylon.database;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import com.babylon.App;
import com.babylon.database.schema.PaymentCardScheme;
import com.babylon.model.payments.PaymentCard;

import java.util.ArrayList;
import java.util.List;

public class PaymentCardOperationUtils {

    private static PaymentCardOperationUtils instance;

    private PaymentCardOperationUtils() {
        //singleton
    }

    public static PaymentCardOperationUtils getInstance() {
        if (instance == null) {
            instance = new PaymentCardOperationUtils();
        }
        return instance;
    }

    public void save(PaymentCard creditCard) {
        App.getInstance().getContentResolver().insert(PaymentCardScheme.CONTENT_URI, creditCard.toContentValues());
    }

    public void save(PaymentCard[] listOfCreditCards) {
        List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        batch.add(ContentProviderOperation.newDelete(PaymentCardScheme.CONTENT_URI).build());

        for (PaymentCard creditCard : listOfCreditCards) {
            ContentValues contentValues = creditCard.toContentValues();
            batch.add(ContentProviderOperation.newInsert(PaymentCardScheme.CONTENT_URI).withValues(contentValues)
                    .build());
        }

        DatabaseOperationUtils.getInstance().applyBatch(App.getInstance().getContentResolver(), batch);
    }

    public void delete(int creditCardId) {
        App.getInstance().getContentResolver().delete(PaymentCardScheme.CONTENT_URI,
                PaymentCardScheme.Columns.ID.getName() + "=?", new String[]{
                        String.valueOf(creditCardId)
                });
    }
}
