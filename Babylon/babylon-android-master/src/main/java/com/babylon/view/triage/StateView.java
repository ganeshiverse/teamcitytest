package com.babylon.view.triage;

import com.babylon.model.check.SymptomAnswer;

import java.util.List;

public interface StateView {
    public List<SymptomAnswer> getState();
    public void setState(List<SymptomAnswer> answers);
    public boolean isFinalAnswer();
}
