package com.axsystech.excelicare.db;

import android.content.Context;
import android.util.Log;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.db.models.CircleInvitationSentDbModel;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.MemoryUtil;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import net.sqlcipher.database.SQLiteDatabase;

import java.sql.SQLException;

/**
 * Database helper class to create or upgrade tables based on database version changes.
 * Date: 06.05.14
 * Time: 14:36
 *
 * @author SomeswarReddy
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "excelicare.db";
    private static final int DATABASE_VERSION = 3;

    public DatabaseHelper(final Context context) {
        super(context, MemoryUtil.getAppDirectoryPath(context) + DATABASE_NAME,
                null, DATABASE_VERSION, R.raw.ormlite_config, AxSysTechApplication.getPassword());
        SQLiteDatabase.loadLibs(AxSysTechApplication.getAppContext());
    }

    @Override
    public void onCreate(final SQLiteDatabase db, final ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, SiteDetailsDBModel.class);
            TableUtils.createTable(connectionSource, MediaFoldersModel.class);
            TableUtils.createTable(connectionSource, MediaItemDbModel.class);
            TableUtils.createTable(connectionSource, CirclesListDbModel.class);
            TableUtils.createTable(connectionSource, CircleMemberListDbModel.class);
            TableUtils.createTable(connectionSource, CirclesMemberDetailsDbModel.class);
            TableUtils.createTable(connectionSource, CircleDetailsDbModel.class);
            TableUtils.createTable(connectionSource, CirclesInvitationReceivedDbModel.class);
            TableUtils.createTable(connectionSource, CircleInvitationSentDbModel.class);
            TableUtils.createTable(connectionSource, DeviceUsersDbModel.class);
            TableUtils.createTable(connectionSource, SystemPreferenceDBModel.class);
            TableUtils.createTable(connectionSource, ClientUrlDbModel.class);
            TableUtils.createTable(connectionSource, ClientUrlAndUserDetailsDbModel.class);
            TableUtils.createTable(connectionSource, MessageListItemDBModel.class);

        } catch (SQLException e) {
            Log.e(getClass().getSimpleName(), "can't create db", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final ConnectionSource connectionSource, final int oldVersion, final int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, SiteDetailsDBModel.class, true);
            TableUtils.dropTable(connectionSource, MediaFoldersModel.class, true);
            TableUtils.dropTable(connectionSource, MediaItemDbModel.class, true);
            TableUtils.dropTable(connectionSource, CirclesListDbModel.class, true);
            TableUtils.dropTable(connectionSource, CircleMemberListDbModel.class, true);
            TableUtils.dropTable(connectionSource, CirclesMemberDetailsDbModel.class, true);
            TableUtils.dropTable(connectionSource, CircleDetailsDbModel.class, true);
            TableUtils.dropTable(connectionSource, CirclesInvitationReceivedDbModel.class, true);
            TableUtils.dropTable(connectionSource, CircleInvitationSentDbModel.class, true);
            TableUtils.dropTable(connectionSource, DeviceUsersDbModel.class, true);
            TableUtils.dropTable(connectionSource, SystemPreferenceDBModel.class, true);
            TableUtils.dropTable(connectionSource, ClientUrlDbModel.class, true);
            TableUtils.dropTable(connectionSource, ClientUrlAndUserDetailsDbModel.class, true);
            TableUtils.dropTable(connectionSource, MessageListItemDBModel.class, true);

            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(getClass().getSimpleName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
    }
}
