package com.babylon.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;

import com.babylon.R;
import com.babylon.utils.FontUtils;


public class TypefaceEditText extends EditText {

    public TypefaceEditText(Context context) {
        super(context);
    }

    public TypefaceEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initialize(context, attrs);
        }
    }

    public TypefaceEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void initialize(Context context, AttributeSet attrs) {
        TypedArray attrsArray = context.obtainStyledAttributes(attrs, R.styleable.TypefaceEditText);
        if (attrsArray != null) {
            final int index = attrsArray.getIndexCount();
            if (index > 0) {
                for (int i = 0; i < index; ++i) {
                    int attr = attrsArray.getIndex(i);
                    if (attr == R.styleable.TypefaceEditText_fontEditText) {
                        switch (attrsArray.getInteger(R.styleable.TypefaceEditText_fontEditText, 1)) {
                            case 1:
                                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoRegular));
                                break;
                            case 2:
                                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoLight));
                                break;
                            case 3:
                                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoItalic));
                                break;
                            case 4:
                                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoBold));
                                break;
                            case 5:
                                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoMedium));
                                break;
                            case 6:
                                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoThin));
                                break;
                            default:
                                break;
                        }
                    }
                }
            } else {
                setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoRegular));
            }
            attrsArray.recycle();
        }
    }
}
