package com.babylon.fragment.nutrient;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.adapter.NutrientsCategoriesArrayAdapter;
import com.babylon.broadcast.SyncFinishedBroadcast;
import com.babylon.database.Table;
import com.babylon.database.schema.FoodSchema;
import com.babylon.fragment.BaseMonitorMeParamsFragment;
import com.babylon.model.NutrientByCategory;
import com.babylon.model.NutrientCategory;
import com.babylon.sync.AbsSyncStrategy;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class ConsumedNutrientsByCategoryFragment extends BaseMonitorMeParamsFragment implements android.support.v4.app
        .LoaderManager.LoaderCallbacks<Cursor> {

    private NutrientsCategoriesArrayAdapter adapter;
    private boolean isSynced = true;

    public static ConsumedNutrientsByCategoryFragment getInstance(long beginOfTheDayInMillis) {
        final Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        final ConsumedNutrientsByCategoryFragment fragment = new ConsumedNutrientsByCategoryFragment();
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, dateInMillis);
        b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(dateInMillis, 1));
        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_FOOD, b, SyncAdapter.SYNC_PULL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_consumed_nutrients_category, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.babylon.ACTION_SYNC_FINISHED");
        getActivity().registerReceiver(syncFinishedBroadcastReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(syncFinishedBroadcastReceiver);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showCurrentDate();
        setTitle(R.string.calories_in_title);

        final ListView list = (ListView) getView().findViewById(android.R.id.list);
        list.setOnItemClickListener(onCategoryItemClickListener);

        List<NutrientByCategory> category = fillInCategories();
        initAdapter(category);
        setProgressBarVisible(isSynced);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(FoodSchema.Query.CURSOR_LOAD_TODAY_CATEGORY, null, this);
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id) {
            case FoodSchema.Query.CURSOR_LOAD_TODAY_CATEGORY:
                return new CursorLoader(getActivity(),
                        FoodSchema.CategoriesQuery.getByCategoryUri(dateInMillis),
                        null, null, null, null);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<NutrientByCategory> category;

        if (cursor.moveToFirst()) {
            category = getCategories(cursor);
        } else {
            category = fillInCategories();
        }

        adapter.setCategory(category);
        adapter.notifyDataSetChanged();
        showDailyCalories(category);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private List<NutrientByCategory> fillInCategories() {
        final List<NutrientByCategory> nutrientByCategories = new ArrayList<NutrientByCategory>();

        nutrientByCategories.add(new NutrientByCategory(NutrientCategory.BREAKFAST, 0));
        nutrientByCategories.add(new NutrientByCategory(NutrientCategory.LUNCH, 0));
        nutrientByCategories.add(new NutrientByCategory(NutrientCategory.DINNER, 0));
        nutrientByCategories.add(new NutrientByCategory(NutrientCategory.SNACKS, 0));

        return nutrientByCategories;
    }

    private void initAdapter(List<NutrientByCategory> category) {
        adapter = new NutrientsCategoriesArrayAdapter(getActivity(), category, onAddNutrientClick);

        final ListView list = (ListView) getView().findViewById(android.R.id.list);
        list.setAdapter(adapter);
    }

    private List<NutrientByCategory> getCategories(Cursor cursor) {
        final List<NutrientByCategory> categories = new ArrayList<NutrientByCategory>();

        categories.add(getCategory(cursor, NutrientCategory.BREAKFAST));
        categories.add(getCategory(cursor, NutrientCategory.LUNCH));
        categories.add(getCategory(cursor, NutrientCategory.DINNER));
        categories.add(getCategory(cursor, NutrientCategory.SNACKS));

        return categories;
    }

    private NutrientByCategory getCategory(Cursor cursor, NutrientCategory category) {
        final int columnInd = cursor.getColumnIndex(category.getTitle());
        final int calories = cursor.getInt(columnInd);

        final NutrientByCategory nutrientByCategory = new NutrientByCategory(category, calories);

        return nutrientByCategory;
    }

    private int getCaloriesSum(List<NutrientByCategory> categories) {
        int sum = 0;

        for (int i = 0; i < categories.size(); i++) {
            sum += categories.get(i).getCalories();
        }

        return sum;
    }

    private void showDailyCalories(List<NutrientByCategory> categories) {
        final int sum = getCaloriesSum(categories);
        final TextView sumTextView = (TextView) getView().findViewById(R.id.text_view_day_calories_sum);
        sumTextView.setText(String.valueOf(sum));
    }


    @Override
    protected void onTopBarButtonPressed() {
        Cursor notSyncActivityCursor = AbsSyncStrategy.getUpdateCursor(getActivity().getContentResolver(), Table.FOOD,
                FoodSchema.CONTENT_URI, null);

        if (notSyncActivityCursor.moveToFirst()) {
            Bundle b = new Bundle();
            b.putLong(Constants.EXTRA_MIN_DATE, dateInMillis);
            b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(dateInMillis, 1));
            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_FOOD, b, SyncAdapter.SYNC_PUSH);
        }

        super.onTopBarButtonPressed();
    }

    private NutrientsCategoriesArrayAdapter.OnAddNutrientClick onAddNutrientClick = new
            NutrientsCategoriesArrayAdapter.OnAddNutrientClick() {
                @Override
                public void onAddNutrientClick(NutrientCategory category) {
                    startFragment(ConsumedNutrientFragment.getInstance(dateInMillis, category), true);
                }
            };

    private final AdapterView.OnItemClickListener onCategoryItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final NutrientByCategory nutrientByCategory = (NutrientByCategory) adapter.getItem(position);

            startFragment(ConsumedNutrientFragment.getInstance(dateInMillis, nutrientByCategory.getCategory()), true);
        }
    };

    private final BroadcastReceiver syncFinishedBroadcastReceiver = new SyncFinishedBroadcast() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            final int action = intent.getIntExtra(SyncAdapter.KEY_SYNC_ACTION, -1);
            final String type = intent.getStringExtra(SyncAdapter.KEY_SYNC_TYPE);

            if (type.equals(Constants.Sync_Keys.SYNC_FOOD) && action == SyncAdapter.SYNC_PULL) {
                isSynced = false;
                setProgressBarVisible(false);
            }
        }
    };

}
