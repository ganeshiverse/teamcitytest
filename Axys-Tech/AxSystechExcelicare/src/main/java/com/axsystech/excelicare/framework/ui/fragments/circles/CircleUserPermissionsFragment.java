package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleUserPermissionsDataResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.data.responses.SaveMyCircleResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.CircleUserPermissionsLoader;
import com.axsystech.excelicare.loaders.ListMyCirclesLoader;
import com.axsystech.excelicare.loaders.SaveCirclePermissionsLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by alanka on 10/12/2015.
 */
public class CircleUserPermissionsFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = PermissionsFragment.class.getSimpleName();
    private CircleUserPermissionsFragment mFragment;
    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectView(R.id.linearLayoutHolder)
    private LinearLayout mLinearLayoutHolder;

    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();

    @InjectView(R.id.textViewPermissionsHeader)
    private TextView mTextViewPermissionsHeader;

    @InjectView(R.id.linearLayoutHolderModule)
    private LinearLayout mLinearLayoutHolderModule;

    @InjectView(R.id.textViewPermissionsModuleHeader)
    private TextView mTextViewPermissionsModuleHeader;

    @InjectView(R.id.textViewCancel)
    private TextView textViewCancel;

    @InjectView(R.id.textViewDone)
    private TextView textViewDone;

    @InjectSavedState
    private User mActiveUser;

    int toggleState;

    String memberId;
    ArrayList<String> accessGroupId;
    HashMap<String, Integer> hashMapIdAl = new HashMap<String, Integer>();

    JSONObject dataObject;

    JSONObject mainObject;
    JSONArray jsonArray;

    private static final String GET_CIRCLE_USER_PERMISSIONS = "com.axsystech.excelicare.framework.ui.messages.CircleUserPermissionsFragment.GET_CIRCLE_USER_PERMISSIONS";
    private static final String SAVE_CIRCLE_PERMISSIONS = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleUserPermissionsFragment.SAVE_CIRCLE_PERMISSIONS";

    ArrayList<String> idArrayListAl = new ArrayList<String>();


    @Override
    protected void initActionBar() {
        // update actionbar title
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity,getString(R.string.title_circle_permissions)));


        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.circle_permissions_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        preloadData();
        addEventClickListener();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public void addEventClickListener() {
        textViewCancel.setOnClickListener(this);
        textViewDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewCancel: {
                break;
            }

            case R.id.textViewDone: {

                mainObject = new JSONObject();
                jsonArray = new JSONArray();
                StringBuffer sb = new StringBuffer();
                ArrayList<JSONObject> arl = new ArrayList<JSONObject>();

               /* //Menu Hide options
                for (int i = 0; i < mLinearLayoutHolderModule.getChildCount(); i++) {
                    View childView = mLinearLayoutHolderModule.getChildAt(i);
                    ToggleButton toggleButton = (ToggleButton) childView.findViewById(R.id.togglePermissionsModuleList);

                    if (toggleButton.isChecked() == true) {
                        toggleState = 1;
                    } else {
                        toggleState = 0;
                    }
                    String toggleTag = toggleButton.getTag().toString();
                    hashMapIdAl.put(toggleTag, toggleState);

                }

                try {
                    for (Map.Entry m : hashMapIdAl.entrySet()) {
                        JSONObject dataObject = new JSONObject();
                        dataObject.put("CirclePermission_SLU", "");
                        dataObject.put("CirclePermissionValue_SLU", m.getValue().toString());
                        dataObject.put("UserCircleMember_ID", memberId.trim());
                        dataObject.put("SysAccessGroupItem_ID", m.getKey());

                        System.out.println("ToggleState" + m.getValue() + m.getKey());

                        arl.add(dataObject);
                        jsonArray.put(dataObject);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                //Hide options
                for (int i = 0; i < mLinearLayoutHolder.getChildCount(); i++) {
                    View childView = mLinearLayoutHolder.getChildAt(i);
                    ToggleButton toggleButton = (ToggleButton) childView.findViewById(R.id.togglePermissions);

                    if (toggleButton.isChecked() == true) {
                        toggleState = 1;
                    } else {
                        toggleState = 0;
                    }
                    String toggleTag = toggleButton.getTag().toString();
                    hashMapIdAl.put(toggleTag, toggleState);
                }
                try {


                    for (Map.Entry m : hashMapIdAl.entrySet()) {
                        JSONObject dataObject = new JSONObject();
                        dataObject.put("CirclePermission_SLU", "");
                        dataObject.put("CirclePermissionValue_SLU", m.getValue().toString());
                        dataObject.put("UserCircleMember_ID", memberId.trim());
                        dataObject.put("SysAccessGroupItem_ID", m.getKey());

                        arl.add(dataObject);
                        jsonArray.put(dataObject);
                    }

                    mainObject.put("Permissions", jsonArray.toString());

                    mainObject.put("Token", mActiveUser != null ? mActiveUser.getToken() : "");
                    mainObject.put("UserCircleMemberID", memberId.trim());
                    mainObject.put("TemplateID", "");
                    mainObject.put("SameAsMemberID", "");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);
                    SaveCirclePermissionsLoader saveCirclePermissionsLoader = new SaveCirclePermissionsLoader(mActivity, mainObject.toString().trim());
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_CIRCLE_PERMISSIONS), saveCirclePermissionsLoader);
                    }
                }
            }
            break;
        }
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_ID)) {
                memberId = bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_ID);
            }
        }
    }


    private void preloadData() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            CircleUserPermissionsLoader circleUserPermissionsLoader = new CircleUserPermissionsLoader(mActivity, mActiveUser.getToken(), memberId);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CIRCLE_USER_PERMISSIONS), circleUserPermissionsLoader);
            }
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_CIRCLE_USER_PERMISSIONS)) {
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_CIRCLE_USER_PERMISSIONS)) {
            if (result != null && result instanceof CircleUserPermissionsDataResponse) {
                CircleUserPermissionsDataResponse circleUserPermissionsDataResponse = (CircleUserPermissionsDataResponse) result;

                if (circleUserPermissionsDataResponse.getDataObject() != null) {

                    ArrayList<CircleUserPermissionsDataResponse.ModuleList> permissionToModuleListAl = circleUserPermissionsDataResponse.getDataObject().getPermissionToModuleListsAl();
                    ArrayList<CircleUserPermissionsDataResponse.ModuleList> groupsModuleListAl = circleUserPermissionsDataResponse.getDataObject().getModuleListsAl();

                    mTextViewPermissionsHeader.setText("ModuleList");
                    inflateRows(permissionToModuleListAl);
                    mTextViewPermissionsModuleHeader.setText("PermissionToModuleList");
                    inflateRowsModuleList(groupsModuleListAl);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_CIRCLE_PERMISSIONS)) {
            final BaseResponse baseResponse = (BaseResponse) result;

            if (baseResponse.isSuccess()) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        showToast(baseResponse.getMessage());
                        mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                    }
                });
            }
        }
    }

    private void inflateRows(ArrayList<CircleUserPermissionsDataResponse.ModuleList> moduleListAl) {
        if (moduleListAl != null && moduleListAl.size() > 0) {

            for (int i = 0; i < moduleListAl.size(); i++) {

                CircleUserPermissionsDataResponse.ModuleList moduleList = moduleListAl.get(i);
                ArrayList<CircleUserPermissionsDataResponse.AreaModuleList> areaModuleLists = moduleList.getAreaModuleListAl();

                if (areaModuleLists != null && areaModuleLists.size() > 0) {
                    accessGroupId = new ArrayList<>();
                    for (int j = 0; j < areaModuleLists.size(); j++) {
                        LayoutInflater rowinflater = (LayoutInflater) getActivity()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View myView = rowinflater.inflate(R.layout.circle_permissions_rowlayout,
                                null);

                        TextView histroyTitle = (TextView) myView
                                .findViewById(R.id.textViewPermissionsText);
                        final ToggleButton toggleButton = (ToggleButton) myView.findViewById(R.id.togglePermissions);

                        histroyTitle.setText(areaModuleLists.get(j).getAreaName());
                        toggleButton.setTag(areaModuleLists.get(j).getSysAccessGroupitemID());

                        CircleUserPermissionsDataResponse.AreaModuleList areaModuleList = areaModuleLists.get(j);
                        ArrayList<CircleUserPermissionsDataResponse.GroupsModuleList> groupsModuleListsAl = areaModuleList.getGroupsModuleListAl();

                        for (int k = 0; k < groupsModuleListsAl.size(); k++) {

                            String defaultValue = groupsModuleListsAl.get(k).getPermissionsModuleListAL().get(k).getDefaultValue();
                            if (defaultValue.equals("0")) {
                                toggleButton.setChecked(false);
                            } else if (defaultValue.equals("1")) {
                                toggleButton.setChecked(true);
                            }
                        }
                        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (toggleButton.isChecked()) {
                                    toggleButton.setButtonDrawable(R.drawable.on);
                                } else {
                                    toggleButton.setButtonDrawable(R.drawable.off);
                                }
                            }
                        });
                        mLinearLayoutHolder.addView(myView);
                    }
                }
            }
        }
    }

    private void inflateRowsModuleList(ArrayList<CircleUserPermissionsDataResponse.ModuleList> moduleListAl) {
        if (moduleListAl != null && moduleListAl.size() > 0) {

            for (int i = 0; i < moduleListAl.size(); i++) {

                CircleUserPermissionsDataResponse.ModuleList moduleList = moduleListAl.get(i);
                ArrayList<CircleUserPermissionsDataResponse.AreaModuleList> areaModuleLists = moduleList.getAreaModuleListAl();

                if (areaModuleLists != null && areaModuleLists.size() > 0) {

                    for (int j = 0; j < areaModuleLists.size(); j++) {
                        LayoutInflater rowinflater = (LayoutInflater) getActivity()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View myView = rowinflater.inflate(R.layout.circles_permissions_modulerowlayout,
                                null);

                        TextView histroyTitle = (TextView) myView
                                .findViewById(R.id.textViewPermissionsModuleText);
                        final ToggleButton toggleButton = (ToggleButton) myView.findViewById(R.id.togglePermissionsModuleList);

                        histroyTitle.setText(areaModuleLists.get(j).getAreaName());
                        toggleButton.setTag(areaModuleLists.get(j).getSysAccessGroupitemID());

                        CircleUserPermissionsDataResponse.AreaModuleList areaModuleList = areaModuleLists.get(j);
                        ArrayList<CircleUserPermissionsDataResponse.GroupsModuleList> groupsModuleListsAl = areaModuleList.getGroupsModuleListAl();

                        for (int k = 0; k < groupsModuleListsAl.size(); k++) {

                            String defaultValue = groupsModuleListsAl.get(k).getPermissionsModuleListAL().get(k).getDefaultValue();
                            if (defaultValue.equals("0")) {
                                toggleButton.setChecked(false);
                            } else if (defaultValue.equals("1")) {
                                toggleButton.setChecked(true);
                            }
                        }

                        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (toggleButton.isChecked()) {
                                    toggleButton.setButtonDrawable(R.drawable.on);
                                } else {
                                    toggleButton.setButtonDrawable(R.drawable.off);
                                }
                            }
                        });

                        mLinearLayoutHolderModule.addView(myView);
                    }
                }
            }
        }
    }
}
