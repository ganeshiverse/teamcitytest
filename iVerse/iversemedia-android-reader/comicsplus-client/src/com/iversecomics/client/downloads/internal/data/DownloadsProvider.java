package com.iversecomics.client.downloads.internal.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Provider for Downloads persistence, tracking, and history.
 */
public class DownloadsProvider extends ContentProvider {
    static final String             TAG                = DownloadsProvider.class.getSimpleName();

    // URI Requests
    private static final int        ALLDOWNLOADS       = 1;
    private static final int        SINGLE_DOWNLOAD_ID = 2;
    private static final UriMatcher URIMATCHER;

    static {
        URIMATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URIMATCHER.addURI(DownloadsDB.AUTHORITY, "downloads", ALLDOWNLOADS);
        URIMATCHER.addURI(DownloadsDB.AUTHORITY, "downloads/#", SINGLE_DOWNLOAD_ID);
    }

    private SQLiteDatabase          db;

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        int count;

        switch (URIMATCHER.match(uri)) {
        case ALLDOWNLOADS:
            count = db.delete(DownloadsTable.TABLE, where, whereArgs);
            break;
        case SINGLE_DOWNLOAD_ID:
            String downloadId = uri.getLastPathSegment();
            StringBuilder wherebuf2 = new StringBuilder();
            wherebuf2.append(DownloadsTable._ID).append('=').append(downloadId);
            if (!TextUtils.isEmpty(where)) {
                wherebuf2.append(" AND (").append(where).append(")");
            }
            count = db.delete(DownloadsTable.TABLE, wherebuf2.toString(), whereArgs);
            break;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (URIMATCHER.match(uri)) {
        case ALLDOWNLOADS:
            return DownloadsTable.CONTENT_TYPE;
        case SINGLE_DOWNLOAD_ID:
            return DownloadsTable.CONTENT_TYPE;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        values.put(DownloadsTable.TIMESTAMP_CREATED, System.currentTimeMillis());
        long rowid = db.insert(DownloadsTable.TABLE, "", values);

        if (rowid > 0) {
            Uri nuri = ContentUris.withAppendedId(DownloadsTable.CONTENT_URI, rowid);
            getContext().getContentResolver().notifyChange(nuri, null);
            return nuri;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public boolean onCreate() {
        DownloadsDatabaseHelper dbhelper = new DownloadsDatabaseHelper(getContext());
        db = dbhelper.getWritableDatabase();
        return (db != null);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (URIMATCHER.match(uri)) {
        case ALLDOWNLOADS:
            qb.setTables(DownloadsTable.TABLE);
            break;
        case SINGLE_DOWNLOAD_ID:
            String dbId = uri.getLastPathSegment();
            qb.setTables(DownloadsTable.TABLE);
            qb.appendWhere(DownloadsTable._ID + "=" + dbId);
            break;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        // If no sort order is specified use the default
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = DownloadsTable.DEFAULT_SORT_ORDER;
        } else {
            orderBy = sortOrder;
        }

        // Apply the query
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);

        // Register the contexts ContentResolver to be notified if the cursor result set changes.
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int count;

        switch (URIMATCHER.match(uri)) {
        case ALLDOWNLOADS:
            count = db.update(DownloadsTable.TABLE, values, where, whereArgs);
            break;
        case SINGLE_DOWNLOAD_ID:
            values.put(DownloadsTable.TIMESTAMP_UPDATED, System.currentTimeMillis());
            String downloadId = uri.getPathSegments().get(1);
            StringBuilder wherebuf2 = new StringBuilder();
            wherebuf2.append(DownloadsTable._ID).append('=').append(downloadId);
            if (!TextUtils.isEmpty(where)) {
                wherebuf2.append(" AND (").append(where).append(")");
            }
            count = db.update(DownloadsTable.TABLE, values, wherebuf2.toString(), whereArgs);
            break;
        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        // Notify of changes.
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
