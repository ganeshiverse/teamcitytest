package com.babylon.api.request.base;

import com.babylon.api.request.BaseResponseProcessor;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public abstract class AbsRequestGet<O> extends AbsRequest<HttpGet, O> {

    protected List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

    public AbsRequestGet withParams(List<BasicNameValuePair> params) {
        this.params = params;
        return this;
    }

    @Override
    protected HttpGet acquireHttpRequest() {
        return new HttpGet();
    }

    @Override
    protected AbsResponseProcessor<O> getResponseProcessor() {
        return new BaseResponseProcessor<O>(getOutputType());
    }
}
