package com.babylon.utils.samsung.health;

import android.support.annotation.Nullable;
import android.util.Log;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.samsung.android.sdk.healthdata.HealthDataObserver;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import java.util.Date;

public abstract class BaseReporter {
    protected static String TAG = SamsungHealthManager.class.getSimpleName();

    protected long currentSyncDateInMillis;

    private HealthDataStore store;
    private boolean isRunning;

    protected abstract String getDataType();

    protected abstract void readData(@Nullable String dataTypeName);

    public void start(HealthDataStore store) {
        this.store = store;
        isRunning = true;

        // Register an observer to listen health data changes
        HealthDataObserver.addObserver(store, getDataType(), observer);
        currentSyncDateInMillis = System.currentTimeMillis();
        readData(getDataType());
    }

    public void stop() {
        if (isRunning) {
            isRunning = false;
            try {
                HealthDataObserver.removeObserver(store, observer);
            } catch (IllegalStateException e) {
                L.e(TAG, e.getMessage(), e);
            }
        }
    }

    protected void requestData(HealthDataResolver.ReadRequest request,
                 HealthResultHolder.ResultListener<HealthDataResolver.ReadResult> resultListener) {
        HealthDataResolver resolver = new HealthDataResolver(store, null);
        try {
            resolver.read(request).setResultListener(resultListener);
            setSyncDateFromInMillis(currentSyncDateInMillis);
        } catch (IllegalArgumentException | SecurityException | IllegalStateException e) {
            Log.e(TAG, e.getClass().getName() + " - " + e.getMessage(), e);
        }
    }

    protected long getSyncDateFromInMillis() {
        long dateFromInMillis = PreferencesManager.getInstance().getLong(getDataType(), 0);
        if (dateFromInMillis == 0) {
            dateFromInMillis = DateUtils.getDayStartMills(new Date(), -DateUtils.DAYS_IN_YEAR);
        }
        return dateFromInMillis;
    }

    private void setSyncDateFromInMillis(long syncDateFromInMillis) {
        PreferencesManager.getInstance().setLong(getDataType(), syncDateFromInMillis);
    }

    private final HealthDataObserver observer = new HealthDataObserver(null) {

        // Update the step data when a change event is received
        @Override
        public void onChange(String dataTypeName) {
            L.d(TAG, "S Health data changed with type : " + dataTypeName);
            currentSyncDateInMillis = System.currentTimeMillis();
            readData(dataTypeName);
        }
    };
}
