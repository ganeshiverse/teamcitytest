package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.fragment.BaseFragment;
import com.babylon.view.TopBarInterface;

public class ShopOrderConfirmFragment extends BaseFragment  implements View.OnClickListener {

    public static final String TAG = ShopOrderConfirmFragment.class.getSimpleName();
    private static String PRICE_BUNDLE_KEY="price";
    private ShopOrderConfirmFragmentListener listener;

    public interface ShopOrderConfirmFragmentListener {

        public void onDoneClicked();
    }
    public static ShopOrderConfirmFragment newInstance(String price) {
        ShopOrderConfirmFragment fragment = new ShopOrderConfirmFragment();
        Bundle args = new Bundle();
        args.putString(PRICE_BUNDLE_KEY, price);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shop_order_confirmation, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String price=null;
        Bundle arguments = getArguments();
        if (arguments != null) {
              price = getArguments().getString(PRICE_BUNDLE_KEY,"0.00");
        }
        setBackButtonVisible(false);
        TextView costTextView = (TextView) view.findViewById(R.id.subscription_cost_text_view);
        TextView familyPlanTextView = (TextView) view.findViewById(R.id.appointment_family_plan_text_view);

        costTextView.setText(App.getInstance().getCurrentRegion().getFormattedPriceString(Double.parseDouble(price)));
        setTitle(R.string.subscription_confirmation_title);
        setRightButton(this, R.string.done);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (ShopOrderConfirmFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public void onClick(View view) {

            switch (view.getId()) {
                case R.id.topbar_right_text_button:
                    listener.onDoneClicked();
                    break;

        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }


}






