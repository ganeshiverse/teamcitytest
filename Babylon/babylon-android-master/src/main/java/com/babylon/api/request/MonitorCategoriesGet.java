package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.MonitorMeCategory;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class MonitorCategoriesGet extends AbsRequestGet<ResponseList<MonitorMeCategory>> {

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.MONITOR_CATEGORY_LIST_GET);
        return TextUtils.concat(url, urlParams).toString();
    }

    public static MonitorCategoriesGet newCategoryListGet(String... names) {

        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (names != null && names.length != 0) {
            for (int i = 0; i < names.length; i++) {
                params.add(new BasicNameValuePair("names[]", names[i]));
            }
        }
        return (MonitorCategoriesGet) new MonitorCategoriesGet().withParams(params);
    }
}
