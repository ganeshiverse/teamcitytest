package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 15/09/15.
 */
public class CityListResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private ArrayList<CityDetails> cityDetailsAL;

    @SerializedName("LMD")
    private String LMD;

    public ArrayList<CityDetails> getCityDetailsAL() {
        return cityDetailsAL;
    }

    public String getLMD() {
        return LMD;
    }

    public class CityDetails implements Serializable {

        @SerializedName("City")
        private String city;

        @SerializedName("Code")
        private String code;

        @SerializedName("ID")
        private String ID;

        @SerializedName("State")
        private String state;

        public String getCity() {
            return city;
        }

        public String getCode() {
            return code;
        }

        public String getID() {
            return ID;
        }

        public String getState() {
            return state;
        }
    }

}
