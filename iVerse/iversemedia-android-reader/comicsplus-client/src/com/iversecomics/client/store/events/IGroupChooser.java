package com.iversecomics.client.store.events;

public interface IGroupChooser {
    OnGroupClickListener getOnGroupClickListener();

    void setOnGroupClickListener(OnGroupClickListener listener);
}
