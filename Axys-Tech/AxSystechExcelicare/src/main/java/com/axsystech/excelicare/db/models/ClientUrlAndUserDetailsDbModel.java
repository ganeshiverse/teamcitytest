package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 29/09/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CLIENT_URL_USER_DETAILS)
public class ClientUrlAndUserDetailsDbModel extends DBUtils implements Serializable {

    @DatabaseField(id = true, columnName = COMPOSITE_KEY)
    private String userNameAndSiteId;

    @DatabaseField(columnName = EC_USER_ID)
    private String ecUserId;

    @DatabaseField(columnName = LOGIN_NAME)
    private String loginName;

    @DatabaseField(columnName = SITE_ID)
    private String siteId;

    @DatabaseField(columnName = SITE_NAME)
    private String siteName;

    @DatabaseField(columnName = SITE_PHOTO_URL)
    private String sitePhotoUrl;

    @DatabaseField(columnName = CLIENT_URL)
    private String clientUrl;

    @DatabaseField(columnName = SITE_DESCRIPTION)
    private String siteDescription;

    @DatabaseField(columnName = SITE_INFO_LINK)
    private String SiteInfoLink;

    @DatabaseField(columnName = FORENAME)
    private String forename;

    @DatabaseField(columnName = DOB)
    private String dob;

    @DatabaseField(columnName = SURNAME)
    private String surname;

    @DatabaseField(columnName = AGE)
    private String age;

    @DatabaseField(columnName = GENDER)
    private String gender;

    @DatabaseField(columnName = GENDER_VALUE)
    private String genderValue;

    @DatabaseField(columnName = PHOTO)
    private String photo;

    @DatabaseField(columnName = TOKEN)
    private String token;

    public String getUserNameAndSiteId() {
        return userNameAndSiteId;
    }

    public void setUserNameAndSiteId(String userNameAndSiteId) {
        this.userNameAndSiteId = userNameAndSiteId;
    }

    public String getEcUserId() {
        return ecUserId;
    }

    public void setEcUserId(String ecUserId) {
        this.ecUserId = ecUserId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSitePhotoUrl() {
        return sitePhotoUrl;
    }

    public void setSitePhotoUrl(String sitePhotoUrl) {
        this.sitePhotoUrl = sitePhotoUrl;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }

    public String getSiteDescription() {
        return siteDescription;
    }

    public void setSiteDescription(String siteDescription) {
        this.siteDescription = siteDescription;
    }

    public String getSiteInfoLink() {
        return SiteInfoLink;
    }

    public void setSiteInfoLink(String siteInfoLink) {
        SiteInfoLink = siteInfoLink;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderValue() {
        return genderValue;
    }

    public void setGenderValue(String genderValue) {
        this.genderValue = genderValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return siteName;
    }
}
