package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/4/2015.
 */
public class CircleListSearchLoader extends DataAsyncTaskLibLoader<ListMyCirclesResponse> {

    private String token;
    private String criteria;

    public CircleListSearchLoader(Context context,String token, String criteria) {
        super(context);
        this.token = token;
        this.criteria = criteria;
    }

    @Override
    protected ListMyCirclesResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCircleListSearchResponse(token, criteria);
    }
}