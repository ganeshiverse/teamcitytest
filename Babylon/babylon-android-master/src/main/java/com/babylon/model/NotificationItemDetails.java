package com.babylon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NotificationItemDetails
{
    @SerializedName("end_message")
    @Expose
    private String end_message;

    public String getNotificationMessage()
    {
        return end_message;
    }

    @SerializedName("date")
    @Expose
    private String date;

    public String getDate()
    {
        return date;
    }
}