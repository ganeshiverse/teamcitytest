package com.iversecomics.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ActivityUnitTestCase;

import com.iversecomics.client.net.HTTPClient;
import com.iversecomics.client.refresh.TaskExecutor;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.refresh.IRefreshable;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.db.ComicStoreDatabaseHelper;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.client.util.Time;
import com.iversecomics.io.IOUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.json.JSONTokener;

public abstract class AbstractStubActivityTestCase extends ActivityUnitTestCase<StubActivity> {
    public static void clearFreshnessCache(Context context) {
        // Clear out any shared preferences for the freshness cache
        Freshness freshness = new Freshness(context);
        freshness.clearExpired(System.currentTimeMillis() + Time.YEAR);
    }

    private Intent stubIntent;

    public AbstractStubActivityTestCase() {
        super(StubActivity.class);
    }

    protected void assertDatabaseUriHasDBId(String type, Uri uri) {
        getDatabaseId(type, uri);
    }

    protected void assertRowCount(String type, ContentResolver resolver, Uri contentUri, int expectedCount) {
        assertEquals(type + " in database", expectedCount, getRowCount(resolver, contentUri));
    }

    protected void assertRowCount(String type, String tableName, int expectedCount) {
        assertEquals(type + " in database", expectedCount, getRowCount(tableName));
    }

    protected ServerConfig createTestServerConfig() throws IOException, JSONException {
        JSONObject json = loadAssetJsonObject("api2-getConfig.json");
        ServerConfig config = ServerConfig.getDefault();
        ServerConfig.getDefault().clear();
        ServerConfig.getDefault().setFromServerResponse(json);
        return config;
    }

    protected void freshDB(Context context) {
        getEmptyDB(context);
    }

    public ComicStore getComicStore() throws IOException, JSONException {
        Context context = getActivity();
        HTTPClient http = new HTTPClient(context);
        createTestServerConfig();
        return new ComicStore(context, http);
    }

    protected long getDatabaseId(String type, Uri uri) {
        assertNotNull(type + " should have had a uri", uri);
        long dbId = Long.parseLong(uri.getLastPathSegment());
        assertTrue(type + " DB Id [" + dbId + "] should be >= 0", dbId >= 0);
        return dbId;
    }

    protected SQLiteDatabase getDB(Context context) {
        ComicStoreDatabaseHelper helper = ComicStoreDatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        return db;
    }

    protected SQLiteDatabase getEmptyDB(Context context) {
        ComicStoreDatabaseHelper helper = ComicStoreDatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        helper.rebuild(db); // force rebuild
        return db;
    }

    protected int getRowCount(ContentResolver resolver, Uri contentUri) {
        return getRowCount(resolver, contentUri, null, null);
    }

    protected int getRowCount(ContentResolver resolver, Uri contentUri, String where, String whereArgs[]) {
        Cursor c = null;
        try {
            c = resolver.query(contentUri, null, where, whereArgs, null);
            assertTrue("Should have a row", c.moveToFirst());
            return c.getCount();
        } finally {
            DBUtil.close(c);
        }
    }

    protected int getRowCount(SQLiteDatabase db, String tableName) {
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT COUNT(_ID) AS ROWCOUNT FROM " + tableName, null);
            assertTrue("Should have a row", c.moveToFirst());
            int count = c.getInt(c.getColumnIndexOrThrow("ROWCOUNT"));
            return count;
        } finally {
            DBUtil.close(c);
        }
    }

    protected int getRowCount(String tableName) {
        ComicStoreDatabaseHelper helper = ComicStoreDatabaseHelper.getInstance(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        return getRowCount(db, tableName);
    }

    protected JSONObject loadAssetJsonObject(String assetName) throws IOException, JSONException {
        AssetManager am = getInstrumentation().getContext().getAssets();
        InputStream stream = null;
        InputStreamReader reader = null;
        try {
            stream = am.open(assetName);
            reader = new InputStreamReader(stream);
            JSONTokener jsontokener = new JSONTokener(reader);
            return new JSONObject(jsontokener);
        } finally {
            IOUtil.close(reader);
            IOUtil.close(stream);
        }
    }

    protected void runTaskWaitForCompletion(ComicStoreTask task) throws Exception {
        Freshness freshness = new Freshness(getActivity());

        IRefreshable refreshable = null;
        if (task instanceof IRefreshable) {
            refreshable = task;
            assertFalse("Task is on a fresh db, data should not be fresh (but oddly, is reporting as already fresh!?)",
                    refreshable.isFresh(freshness));
        }

        TaskExecutor executor = new TaskExecutor();
        executor.execute(task); // start task
        executor.get(30, TimeUnit.SECONDS); // wait for task to finish, or fail if it takes too long.

        if (refreshable != null) {
            assertTrue("Task just ran, data should now be fresh (but it isn't reporting as such)",
                    refreshable.isFresh(freshness));
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        stubIntent = new Intent();
        String pkg = "com.iversecomics.client";
        String cls = pkg + ".StubActivity";
        ComponentName name = new ComponentName(pkg, cls);
        stubIntent.setComponent(name);
    }

    protected void startStubActivity() {
        startActivity(stubIntent, null, null);
        clearFreshnessCache(getActivity());
    }
}
