package com.iversecomics.client.store.db;

import junit.framework.TestCase;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;

import com.iversecomics.client.OurAsserts;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ContentChangeTracker extends ContentObserver {
    private static final Logger LOG = LoggerFactory.getLogger(ContentChangeTracker.class);
    private ContentResolver     resolver;
    private int                 countChanges;
    private String              testClass;

    public ContentChangeTracker(ContentResolver resolver) {
        super(null);
        this.resolver = resolver;
        this.countChanges = 0;
    }

    public void assertGotContentChangeNotification() {
        resolver.unregisterContentObserver(this);
        OurAsserts.assertAtLeast("ContentObserver.onChange() count during " + testClass, 1, countChanges);
    }

    @Override
    public boolean deliverSelfNotifications() {
        return true;
    }

    public int getCountChanges() {
        return countChanges;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        countChanges++;
        LOG.debug("Got onChange()");
    }

    public void registerSelf(Uri contentUri, TestCase test) {
        testClass = test.getName();
        resolver.registerContentObserver(contentUri, true, this);
    }
}
