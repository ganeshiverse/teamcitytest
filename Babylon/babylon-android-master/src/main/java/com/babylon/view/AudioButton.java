package com.babylon.view;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.babylon.R;
import com.babylon.utils.AudioManager;

public class AudioButton extends Button implements View.OnClickListener {
    public static final int STATUS_PLAY = 11;
    public static final int STATUS_STOP = 12;
    private String filePath;
    private int status;

    public AudioButton(Context context) {
        super(context);
        init();
    }

    public AudioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AudioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void init() {
//        setPadding(40,0,40,0);
        setOnClickListener(this);
        setStatus(STATUS_PLAY);
    }

    public void setStatus(int status) {
        this.status = status;
        switch (status) {
            case STATUS_PLAY:
                setText(R.string.play);
                break;
            case STATUS_STOP:
                setText(R.string.stop);
                break;
        }
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void onClick(View v) {
        switch (status) {
            case STATUS_PLAY:
                AudioManager.getInstance().playRecord(filePath, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        setStatus(STATUS_PLAY);
                    }
                });
                setStatus(STATUS_STOP);
                break;
            case STATUS_STOP:
                AudioManager.getInstance().stopPlaying();
                setStatus(STATUS_PLAY);
                break;

        }
    }
}
