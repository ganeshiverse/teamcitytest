package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Babylon on 12/05/2015.
 */
public class OrderResponse {

    @Expose
    private Integer id;
    @SerializedName("line_items")
    @Expose
    private List<LineItem> lineItems = new ArrayList<LineItem>();
    @SerializedName("patient_id")
    @Expose
    private Integer patientId;
    @Expose
    private String total;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The lineItems
     */
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    /**
     *
     * @param lineItems
     * The line_items
     */
    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     *
     * @return
     * The patientId
     */
    public Integer getPatientId() {
        return patientId;
    }

    /**
     *
     * @param patientId
     * The patient_id
     */
    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    /**
     *
     * @return
     * The total
     */
    public String getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(String total) {
        this.total = total;
    }

}
