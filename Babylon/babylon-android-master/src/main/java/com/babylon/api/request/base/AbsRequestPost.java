package com.babylon.api.request.base;

import com.babylon.api.request.BaseResponseProcessor;
import com.babylon.utils.L;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;

public abstract class AbsRequestPost<I, O> extends AbsRequest<HttpPost, O> {

    protected String jsonBody;

    public AbsRequestPost<I, O> withEntry(I entry) {
        jsonBody = toJson(entry);
        return this;
    }

    private StringEntity getStringEntity(String jsonBody) {
        StringEntity entity = null;
        if(jsonBody != null) {
            try {
                entity = new StringEntity(jsonBody);
                L.d(TAG, "Request json: " + jsonBody);
            } catch (UnsupportedEncodingException e) {
                L.e(TAG, e.toString(), e);
            }
        } else {
            L.d(TAG, "jsonBody was null in AbsRequestPost");
        }
        return entity;
    }

    public HttpEntity getHttpEntity() {
        StringEntity entity = getStringEntity(jsonBody);
        return entity;
    }

    @Override
    protected HttpPost prepareRequest() {
        super.prepareRequest();
        if (!getParamClass(0).equals(Void.class)) {
            httpRequest.setEntity(getHttpEntity());
        }
        return httpRequest;
    }

    @Override
    protected HttpPost acquireHttpRequest() {
        return new HttpPost();
    }

    @Override
    protected AbsResponseProcessor<O> getResponseProcessor() {
        return new BaseResponseProcessor<O>(getOutputType());
    }
}
