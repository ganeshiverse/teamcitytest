package com.babylon.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

public class ActivityRecognitionService extends IntentService {

    private static final String TAG = ActivityRecognitionService.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public ActivityRecognitionService() {
        super("ActivityRecognitionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            logActivityRecognitionResult(result);
            DetectedActivity mostProbableActivity = result.getMostProbableActivity();
            int confidence = mostProbableActivity.getConfidence();
            int activityType = mostProbableActivity.getType();
            if (activityChanged(activityType) && (confidence >= 50)) {
                sendBroadCastNotification(activityType);
            }
        }
    }

    private void sendBroadCastNotification(int activityType) {
        PreferencesManager.getInstance().setCurrentActivityType(activityType);
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constants.KEY_ACTION_REFRESH_STATUS);
        broadcastIntent.addCategory(Constants.KEY_CATEGORY_LOCATION_SERVICES);
        broadcastIntent.putExtra(Constants.KEY_ACTIVITY_TYPE, activityType);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private String getNameFromType(int activityType) {
        switch (activityType) {
            case DetectedActivity.IN_VEHICLE:
                return "in_vehicle";
            case DetectedActivity.ON_BICYCLE:
                return "on_bicycle";
            case DetectedActivity.ON_FOOT:
                return "on_foot";
            case DetectedActivity.STILL:
                return "still";
            case DetectedActivity.UNKNOWN:
                return "unknown";
            case DetectedActivity.TILTING:
                return "tilting";
            case DetectedActivity.WALKING:
                return "walking";
            case DetectedActivity.RUNNING:
                return "running";
        }
        return "unknown";
    }

    private void logActivityRecognitionResult(ActivityRecognitionResult result) {
        for (DetectedActivity detectedActivity : result.getProbableActivities()) {
            int activityType = detectedActivity.getType();
            int confidence = detectedActivity.getConfidence();
            String activityName = getNameFromType(activityType);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("activityType = ").append(activityType)
                    .append(", activityName = ").append(activityName)
                    .append(", confidence = ").append(confidence);
            L.i(TAG, stringBuilder.toString());
        }
    }

    private boolean activityChanged(int currentType) {
        int previousType = PreferencesManager.getInstance().getCurrentActivityType();
        if (previousType == -1) {
            PreferencesManager.getInstance().setCurrentActivityType(currentType);
            return true;
        } else {
            return previousType != currentType;
        }
    }
}
