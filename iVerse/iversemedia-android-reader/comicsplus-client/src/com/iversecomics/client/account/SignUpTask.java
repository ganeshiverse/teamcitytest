package com.iversecomics.client.account;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.io.Digest;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class SignUpTask extends ComicStoreTask {

	String userName;
	String userPassword;
	boolean marketingOptIn;
	private static final Logger LOG = LoggerFactory.getLogger(SignUpTask.class);
	
	public final static String SIGN_UP_SERVER_RESPONSE  = "iverse.action.server.signup.response";
	
	public SignUpTask(ComicStore comicStore) {
		super(comicStore);
	}
	
	public void setUserName(final String userName) {
		this.userName = userName;
	}
	public void setUserPassword(final String userPassword) {
		this.userPassword = userPassword;
	}
	public void setMarketingOptIn(final boolean marketingOptIn) {
		this.marketingOptIn = marketingOptIn;
	}
	
    @Override
    public void execTask() {

        try {
            final JSONObject payload = new JSONObject();
            payload.put("userName", userName);
            payload.put("userPassword", userPassword);
            payload.put("action", "create");
            
            final StringBuilder verifyCode = new StringBuilder();
            verifyCode.append(userName);
            verifyCode.append(SHARED_SECRET);
            payload.put("verifyCode", Digest.sha1(verifyCode.toString()));
            
            final ServerApi server = comicStore.getServerApi();
            final JSONObject json = server.submitUserAccount(payload);

            final Intent intent = new Intent(SIGN_UP_SERVER_RESPONSE);
            
            intent.putExtra("response", json.toString());
            intent.putExtra("userName", userName);
            intent.putExtra("userPassword", userPassword);
            
            LocalBroadcastManager.getInstance(IverseApplication.getApplication().getApplicationContext()).sendBroadcast(intent); 
        }
        catch (Exception e) 
        {
        	LOG.warn(e.toString());
        }

    }
}
