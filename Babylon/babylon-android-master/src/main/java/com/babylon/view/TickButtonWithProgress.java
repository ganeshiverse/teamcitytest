package com.babylon.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.babylon.R;

public class TickButtonWithProgress extends LinearLayout {
    private LayoutInflater inflater;
    private ProgressBar pB;

    public TickButtonWithProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
        pB = (ProgressBar) inflater.inflate(R.layout.btn_progress_tick, this).findViewById(R.id.progressBar);
    }

    public void setProgressState(boolean isProgress) {
        setEnabled(!isProgress);
        if (isProgress) {
            setBackground(getResources().getDrawable(R.drawable.green_circle));
            pB.setVisibility(View.VISIBLE);
        } else {
            setBackground(getResources().getDrawable(R.drawable.btn_tick));
            pB.setVisibility(View.GONE);
        }
    }
}
