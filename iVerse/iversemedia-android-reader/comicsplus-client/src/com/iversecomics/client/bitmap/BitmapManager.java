package com.iversecomics.client.bitmap;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.content.Context;

import com.iversecomics.client.bitmap.loader.AssetBitmapLoader;
import com.iversecomics.client.bitmap.loader.CachedBitmapLoader;
import com.iversecomics.client.bitmap.loader.ExternalBitmapLoader;
import com.iversecomics.client.bitmap.loader.FileBitmapLoader;
import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.client.net.HTTPClient;
import com.iversecomics.client.util.CacheDir;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class BitmapManager {
    private static final Logger           LOG        = LoggerFactory.getLogger(BitmapManager.class);
    private static final int              QUEUE_SIZE = 4;
    @SuppressWarnings("unused")
    private Context                       context;
    private Map<String, IBitmapLoader>    loaderMap;
    private CacheDir                      cacheDir;
    private LinkedBlockingQueue<Runnable> queue;
    private ThreadPoolExecutor            pool;
    private LateBitmapAssignmentHandler   handler;

    public BitmapManager(Context context, HTTPClient http) {
        this.context = context;
        this.queue = new LinkedBlockingQueue<Runnable>();
        this.pool = new ThreadPoolExecutor(QUEUE_SIZE, QUEUE_SIZE, 0L, TimeUnit.MILLISECONDS, this.queue);
        this.cacheDir = new CacheDir(context);
        this.loaderMap = new HashMap<String, IBitmapLoader>();
        this.handler = new LateBitmapAssignmentHandler();

        addBitmapLoader(new ExternalBitmapLoader());
        addBitmapLoader(new FileBitmapLoader());
        addBitmapLoader(new WebBitmapLoader(http));
        addBitmapLoader(new AssetBitmapLoader(context));

        // Special loader "cached:${uri}" (Always Last!)
        CachedBitmapLoader cached = new CachedBitmapLoader(cacheDir);
        cached.setBitmapLoaderMap(loaderMap);
        addBitmapLoader(cached);
    }

    public void addBitmapLoader(IBitmapLoader bitmapLoader) {
        for (String schema : bitmapLoader.getUriSchemes()) {
            loaderMap.put(schema, bitmapLoader);
        }
    }

    public void fetch(LateBitmapRef ref) {
        if (ref != null) {
            FetchRequest request = new FetchRequest(ref, handler);
            synchronized (queue) {
                if (queue.contains(request)) {
                    // Already present, don't add again.
                    return;
                }
                String scheme = ref.getBitmapUri().getScheme();
                IBitmapLoader loader = loaderMap.get(scheme);
                if (loader == null) {
                    LOG.error("No IBitmapLoader for URI scheme (%s) : %s", scheme, ref.getBitmapUri().toASCIIString());
                    return;
                }

                request.setLoader(loader);
                pool.execute(request);
            }
        }
    }

    public CacheDir getCacheDir() {
        return cacheDir;
    }
}
