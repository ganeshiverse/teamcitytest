package com.babylon.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.enums.PhysicalActivityType;
import com.babylon.enums.ActivityType;
import com.babylon.model.Patient;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.UIUtils;

import java.util.Date;

public class AddActivityFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = AddActivityFragment.class.getSimpleName();

    private RadioGroup activityTypeRadioGroup;
    private RadioGroup activityForceRadioGroup;
    private TextView timeButton;
    private TextView distanceButton;
    private long dateInMillis = 0;
    private long durationInMillis = 0;
    private float distanceInMeter = 0;

    public static AddActivityFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        AddActivityFragment fragment = new AddActivityFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_activity, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.add_activity_title));
        setProgressBarVisible(false);

        if (getArguments() != null) {
            dateInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
        }

        String dateString = new MonitorMeDateViewController().getFormattedDay(new Date(dateInMillis));

        ((TextView) view.findViewById(R.id.dateTextView)).setText(dateString);

        activityTypeRadioGroup = (RadioGroup) view.findViewById(R.id.activityTypeRadioGroup);
        activityForceRadioGroup = (RadioGroup) view.findViewById(R.id.activityForceRadioGroup);

        timeButton = (TextView) view.findViewById(R.id.timeButton);
        distanceButton = (TextView) view.findViewById(R.id.distanceButton);

        timeButton.setOnClickListener(this);
        distanceButton.setOnClickListener(this);

        view.findViewById(R.id.acceptButton).setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.timeButton:
                showSetDurationDialog();
                break;
            case R.id.distanceButton:
                showSetDistanceDialog();
                break;
            case R.id.acceptButton:
                if (activityTypeRadioGroup.getCheckedRadioButtonId() == -1) {
                    UIUtils.showSimpleToast(getActivity(), R.string.add_activity_choose_type);
                } else if (activityForceRadioGroup.getCheckedRadioButtonId() == -1) {
                    UIUtils.showSimpleToast(getActivity(), R.string.add_activity_choose_intensity);
                } else if (durationInMillis == 0) {
                    UIUtils.showSimpleToast(getActivity(), R.string.add_activity_choose_duration);
                } else {
                    PhysicalActivity physicalActivity = createPhysicalActivity();
                    setProgressBarVisible(true);
                    addActivity(physicalActivity);
                }
                break;
            default:
                break;
        }
    }

    public void showSetDurationDialog() {
        AlertDialogHelper.getInstance().showHoursMinsChoiceAlertDialog(getActivity(),
                new AlertDialogHelper.OnDurationChoiceListener() {
                    @Override
                    public void onDurationChoice(long durationInMillis) {
                        AddActivityFragment.this.durationInMillis = durationInMillis;

                        int minutes = DateUtils.getAllInMinutes(durationInMillis);

                        timeButton.setText(String.valueOf(minutes));
                    }
                }, durationInMillis
        );
    }

    public void showSetDistanceDialog() {
        AlertDialogHelper.getInstance().showDistanceChoiceAlertDialog(getActivity(),
                new AlertDialogHelper.OnDistanceChoiceListener() {
                    @Override
                    public void onDistanceChoice(final float distanceInMeter) {
                        AddActivityFragment.this.distanceInMeter = distanceInMeter;

                        distanceButton.setText(String.valueOf(distanceInMeter / Constants.KILOMETER_IN_METERS));
                    }
                }, distanceInMeter
        );
    }

    public PhysicalActivity createPhysicalActivity() {
        PhysicalActivity physicalActivity = new PhysicalActivity();

        switch (activityTypeRadioGroup.getCheckedRadioButtonId()) {
            case R.id.walkButton:
                physicalActivity = new PhysicalActivity(dateInMillis, ActivityType.WALKING);
                break;
            case R.id.runButton:
                physicalActivity = new PhysicalActivity(dateInMillis, ActivityType.RUNNING);
                break;
            case R.id.cycleButton:
                physicalActivity = new PhysicalActivity(dateInMillis, ActivityType.CYCLING);
                break;
            default:
                L.e(TAG, "Unknown activity type");
        }

        final String[] intensityNames = getActivity().getResources()
                .getStringArray(R.array.calories_burnt_activity_intensity_for_server);
        switch (activityForceRadioGroup.getCheckedRadioButtonId()) {
            case R.id.easyButton:
                physicalActivity.setIntensity(intensityNames[0]);
                break;
            case R.id.mediumButton:
                physicalActivity.setIntensity(intensityNames[1]);
                break;
            case R.id.hardButton:
                physicalActivity.setIntensity(intensityNames[2]);
                break;
        }

        return physicalActivity;
    }

    private void addActivity(PhysicalActivity activity) {
        if (activity != null) {

            activity.setStartDate(DateUtils.findOutSavingDate(dateInMillis));

            final long endDateInMilliseconds = activity.getStartDate().getTime() + durationInMillis;
            final Date endDate = new Date(endDateInMilliseconds);
            activity.setEndDate(endDate);

            activity.setDistanceInMeter(distanceInMeter);

            Patient patient = App.getInstance().getPatient();
            if (patient != null) {
                float patientWeight = 0;
                if (patient.getWeight() != null) {
                    patientWeight = patient.getWeight();
                }

                PhysicalActivityType activityType = PhysicalActivityType.getActivityType(
                        activity.getTitle(),
                        activity.getIntensity());
                if (activityType != null) {
                    int durationInMinutes = DateUtils.getAllInMinutes(durationInMillis);

                    final int BurntKiloCalories = activityType.calculateBurntKiloCalories(
                            patientWeight, durationInMinutes);
                    activity.setCalories(BurntKiloCalories);
                }
            }

            DatabaseOperationUtils.getInstance().insertPhysicalActivity(activity,
                    getActivity().getContentResolver(), asyncQueryListener);
        }
    }

    private void syncActivities() {
        Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, dateInMillis);
        b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(dateInMillis, 1));
        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY, b,
                SyncAdapter.SYNC_PUSH);
    }

    private final DatabaseOperationUtils.AsyncQueryListener
            asyncQueryListener = new DatabaseOperationUtils.AsyncQueryListener() {
        @Override
        public void onQueryComplete(int token, Object cookie, Cursor cursor) {

        }

        @Override
        public void onChangeComplete(int token, Object cookie, int result) {

        }

        @Override
        public void onInsertComplete(int token, Object cookie, Uri uri) {
            syncActivities();
            UIUtils.showSimpleToast(getActivity(), R.string.add_activity_success);
            setProgressBarVisible(false);
            getActivity().onBackPressed();
        }
    };
}
