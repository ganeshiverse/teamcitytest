package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.TimeSlotsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class GetTimeSlotsLoader extends DataAsyncTaskLibLoader<TimeSlotsResponse> {

    private String clinicID;
    private String clinicanID;
    private String startDate;
    private String token;

    public GetTimeSlotsLoader(Context context, String clinicID, String clinicanID, String startDate, String token) {
        super(context);
        this.clinicID = clinicID;
        this.clinicanID = clinicanID;
        this.startDate = startDate;
        this.token = token;
    }

    @Override
    protected TimeSlotsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getTimeSlots(clinicID, clinicanID, startDate, token);
    }
}
