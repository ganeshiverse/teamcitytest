package com.axsystech.excelicare.loaders;

import android.content.Context;
import android.text.TextUtils;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class UpdateMediaItemDBLoader extends DataAsyncTaskLibLoader<MediaItemDbModel> {

    private String TAG = UpdateMediaItemDBLoader.class.getSimpleName();
    private final MediaItemDbModel mediaItem;
    private final String newMediaId;

    public UpdateMediaItemDBLoader(final Context context, final MediaItemDbModel mediaItem, final String newMediaId) {
        super(context, true);
        this.mediaItem = mediaItem;
        this.newMediaId = newMediaId;
    }

    @Override
    protected MediaItemDbModel performLoad() throws Exception {
        if (mediaItem == null) throw new SQLException("Unable to update media item details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (mediaItem != null) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<MediaItemDbModel>() {
                    @Override
                    public MediaItemDbModel call() throws Exception {
                        final Dao<MediaItemDbModel, Long> mediaItemDbModelDao = helper.getDao(MediaItemDbModel.class);

                        // Delete media item with old media id in DB
                        mediaItemDbModelDao.delete(mediaItem);

                        // Update new media id and then re-insert it in DB
                        if (!TextUtils.isEmpty(newMediaId) && newMediaId.trim().length() > 0) {
                            mediaItem.setMediaId(Integer.parseInt(newMediaId));
                        }

                        mediaItemDbModelDao.createOrUpdate(mediaItem);
                        Trace.d(TAG, "Updated media item Details in DB...");

                        return mediaItem;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return mediaItem;
    }
}
