package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.UploadMediaItemResponse;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class UploadMediaItemLoader extends DataAsyncTaskLibLoader<UploadMediaItemResponse> {

    private MediaItemDbModel mediaItemDbModel;
    private String token;
    private String patientId;

    public UploadMediaItemLoader(Context context, MediaItemDbModel mediaItemDbModel, String token, String patientId) {
        super(context);
        this.mediaItemDbModel = mediaItemDbModel;
        this.token = token;
        this.patientId = patientId;
    }

    @Override
    protected UploadMediaItemResponse performLoad() throws Exception {
        return ServerMethods.getInstance().uploadMediaItem(mediaItemDbModel, token, patientId);
    }
}
