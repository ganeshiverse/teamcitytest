package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Date: 06.05.14
 * Time: 17:40
 *
 * @author SomeswarReddy
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_USER)
public class User extends DBUtils implements Serializable {

    private static final long serialVersionUID = -6180354832259562105L;

    @DatabaseField(id = true, columnName = EC_USER_ID)
    private long ecUserID;

    @DatabaseField(columnName = FORENAME)
    private String forename;

    @DatabaseField(columnName = PATIENT_ID)
    private String patientID;

    @DatabaseField(columnName = PHOTO)
    private String photo;

    @DatabaseField(columnName = SITE_ID)
    private String siteID;

    @DatabaseField(columnName = SITE_NAME)
    private String sitename;

    @DatabaseField(columnName = SURNAME)
    private String surname;

    @DatabaseField(columnName = TOKEN)
    private String token;

    @DatabaseField(columnName = USER_DEVICE_ID)
    private String userDeviceID;

    @DatabaseField(columnName = EMAIL)
    private String email;

    @DatabaseField(columnName = PASSWORD)
    private String password;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getEcUserID() {
        return ecUserID;
    }

    public void setEcUserID(long ecUserID) {
        this.ecUserID = ecUserID;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSiteID() {
        return siteID;
    }

    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserDeviceID() {
        return userDeviceID;
    }

    public void setUserDeviceID(String userDeviceID) {
        this.userDeviceID = userDeviceID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String mEmail) {
        this.email = mEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
