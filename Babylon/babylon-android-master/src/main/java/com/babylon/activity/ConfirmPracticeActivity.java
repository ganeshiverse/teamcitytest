package com.babylon.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.api.request.PatientPatch;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.controllers.BirthdayViewController;
import com.babylon.controllers.OnDateSetListener;
import com.babylon.model.Patient;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.view.TickButtonWithProgress;
import com.babylon.view.TopBarInterface;

import org.apache.http.HttpStatus;

import java.util.ArrayList;
import java.util.Date;

public class ConfirmPracticeActivity extends BaseActivity {

    private Patient patient;
    private String practiceName;
    private String practiceAddress;

    private EditText patientFirstNameField;
    private EditText patientLastNameField;
    private EditText patientEmailField;
    private EditText dobErrorEditText;
    private BirthdayViewController birthdayViewController;
    private Date selectedDateOfBirth;
    private Boolean isDoBValid = true;
    private TickButtonWithProgress submitButton;

    private TextWatcher firstNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

        @Override
        public void afterTextChanged(Editable editable) {
            validateFirstNameField();
        }
    };

    private TextWatcher lastNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

        @Override
        public void afterTextChanged(Editable editable) {
            validateLastNameField();
        }
    };

    private TextWatcher emailTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

        @Override
        public void afterTextChanged(Editable editable) {
            validateEmailField();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_practice);

        Bundle extras = this.getIntent().getExtras();
        //This activity should never be created with null extras
        if (extras == null) {
            throw new UnsupportedOperationException("ConfirmPracticeActivity must be created with practice_name and practice_address extras set");
        }

        practiceName = extras.getString(Keys.PRACTICE_NAME_KEY);
        practiceAddress = extras.getString(Keys.PRACTICE_ADDRESS_KEY);
        patient = App.getInstance().getPatient();

        initViews();
    }

    private void initViews() {
        initializeTopBar();

        TextView practiceNameLabel = (TextView)findViewById(R.id.practice_name_label);
        TextView practiceAddressLabel = (TextView)findViewById(R.id.practice_address_label);
        TextView dobTextView = (TextView)findViewById(R.id.patient_dob_field);

        practiceNameLabel.setText(practiceName);

        practiceAddress = practiceAddress.replace("\\n", "\n");
        practiceAddressLabel.setText(practiceAddress);

        patientFirstNameField = (EditText)findViewById(R.id.patient_first_name_field);
        patientFirstNameField.setText(patient.getFirstName());
        patientFirstNameField.addTextChangedListener(firstNameTextWatcher);

        patientLastNameField = (EditText)findViewById(R.id.patient_last_name_field);
        patientLastNameField.setText(patient.getLastName());
        patientLastNameField.addTextChangedListener(lastNameTextWatcher);

        patientEmailField = (EditText)findViewById(R.id.patient_email_field);
        patientEmailField.setText(patient.getEmail());
        patientEmailField.addTextChangedListener(emailTextWatcher);

        dobErrorEditText = (EditText)findViewById(R.id.patient_dob_error_field);
        dobErrorEditText.setInputType(InputType.TYPE_NULL);

        submitButton = (TickButtonWithProgress)findViewById(R.id.submitButton);

        birthdayViewController = new BirthdayViewController(this, dobTextView, new OnDateSetListener() {
            @Override
            public void onDateSet(Date date) {
                selectedDateOfBirth = date;
                validateDateOfBirthField();
            }
        });
        birthdayViewController.setDate(patient.getBirthday());
        selectedDateOfBirth = patient.getBirthday();

        setTitle(getString(R.string.confirm_practice_title));
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PURPLE_THEME;
    }

    private Boolean validateFirstNameField() {
        if (patientFirstNameField.getText().toString().trim().length() == 0) {
            patientFirstNameField.setError(getString(R.string.confirm_practice_first_name_error));
            return false;
        }
        return true;
    }

    private Boolean validateLastNameField() {
        if (patientLastNameField.getText().toString().trim().length() == 0) {
            patientLastNameField.setError(getString(R.string.confirm_practice_last_name_error));
            return false;
        }
        return true;
    }

    private Boolean validateEmailField() {
        if (patientEmailField.getText().toString().trim().length() == 0 ||
                Patterns.EMAIL_ADDRESS.matcher(patientEmailField.getText().toString()).matches() == false) {
            patientEmailField.setError(getString(R.string.confirm_practice_email_error));
            return false;
        }
        return true;
    }

    private Boolean validateDateOfBirthField() {
        isDoBValid = birthdayViewController.validateBirthday(selectedDateOfBirth, dobErrorEditText);
        return isDoBValid;
    }

    public void onConfirmPracticeClick(View view) {
        String newPatientFirstName = patientFirstNameField.getText().toString();
        String newPatientLastName = patientLastNameField.getText().toString();
        String newPatientEmail = patientEmailField.getText().toString();
        if (validateFirstNameField() && validateLastNameField() && validateEmailField() && validateDateOfBirthField()) {
            ArrayList<String> fieldsToUpdate = new ArrayList<>();
            if (newPatientFirstName.equals(patient.getFirstName()) == false) {
                patient.setFirstName(newPatientFirstName);
                fieldsToUpdate.add("first_name");
            }
            if (newPatientLastName.equals(patient.getLastName()) == false) {
                patient.setLastName(newPatientLastName);
                fieldsToUpdate.add("last_name");
            }
            if (newPatientEmail.equals(patient.getEmail()) == false) {
                patient.setEmail(newPatientEmail);
                fieldsToUpdate.add("email");
            }
            if (selectedDateOfBirth.compareTo(patient.getBirthday()) != 0) {
                patient.setBirthday(selectedDateOfBirth);
                fieldsToUpdate.add("date_of_birth");
            }
            if (fieldsToUpdate.size() > 0) {
                submitButton.setProgressState(true);
                submitButton.setEnabled(false);
                patient.setIsUpdated(true);
                new PatientPatch().withEntry(patient, fieldsToUpdate.toArray(new String[fieldsToUpdate.size()])).execute(this, new RequestListener<Patient>() {
                    @Override
                    public void onRequestComplete(Response<Patient> response) {
                        submitButton.setEnabled(true);
                        submitButton.setProgressState(false);
                        if (response.getStatus() == HttpStatus.SC_OK) {
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            AlertDialogHelper.getInstance().showMessage(ConfirmPracticeActivity.this, response.getUserResponseError());
                        }
                    }
                });
            } else {
                setResult(Activity.RESULT_OK);
                finish();
            }
        }
    }
}
