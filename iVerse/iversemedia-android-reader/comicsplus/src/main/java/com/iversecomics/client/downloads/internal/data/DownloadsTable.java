package com.iversecomics.client.downloads.internal.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.iversecomics.client.store.db.AbstractTable;

import java.io.File;

public class DownloadsTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(DownloadsDB.CONTENT_URI, "downloads");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of downloads.
     */
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/vnd.iversecomics.downloads";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a downloads.
     */
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/vnd.iversecomics.downloads";

    public static final String DEFAULT_SORT_ORDER = "name ASC";
    public static final String TABLE = "downloads";

    /* COLUMNS */
    public static final String NAME = "name";
    public static final String TIMESTAMP_CREATED = "timestampCreated";
    public static final String URI = "uri";
    public static final String STATUS = "status";
    public static final String STATUS_MESSAGE = "statusMessage";
    public static final String LOCAL_FILE = "localFile";
    public static final String CONTENT_LENGTH = "contentLength";
    public static final String CONTENT_PROGRESS = "contentProgress";
    public static final String ETAG = "etag";
    public static final String NUM_FAILED = "numFailed";
    public static final String TIMESTAMP_RETRY_AFTER = "timestampRetryAfter";
    public static final String TIMESTAMP_UPDATED = "timestampUpdated";
    public static final String NOTIFICATIONS_VISIBLE = "notificationsVisible";
    public static final String SERVER_PAYLOAD = "serverPayload";
    public static final String COVER_IMAGE_FILENAME = "coverImageFileName";

    public static ContentValues asValues(DownloadData download) {
        ContentValues values = new ContentValues();

        if (download.getId() >= 0) {
            values.put(_ID, download.getId());
        } else {
            values.putNull(_ID);
        }
        values.put(NAME, download.getName());
        values.put(TIMESTAMP_CREATED, download.getTimestampCreated());
        if (download.getUri() == null) {
            values.putNull(URI);
        } else {
            values.put(URI, download.getUri().toString());
        }
        values.put(STATUS, download.getStatus().getCode());
        values.put(STATUS_MESSAGE, download.getStatusMessage());
        if (download.getLocalFile() == null) {
            values.putNull(LOCAL_FILE);
        } else {
            values.put(LOCAL_FILE, download.getLocalFile().getAbsolutePath());
        }
        values.put(CONTENT_LENGTH, download.getContentLength());
        values.put(CONTENT_PROGRESS, download.getContentProgress());
        values.put(ETAG, download.getEtag());
        values.put(NUM_FAILED, download.getNumFailed());
        values.put(TIMESTAMP_RETRY_AFTER, download.getTimestampRetryAfter());
        values.put(TIMESTAMP_UPDATED, download.getTimestampUpdated());
        values.put(SERVER_PAYLOAD, download.getVerifyPurchasePayload());
        values.put(COVER_IMAGE_FILENAME, download.getCoverImageFileName());

        return values;
    }

    public static DownloadData fromCursor(Cursor c) {
        final DownloadData download = new DownloadData();

        download.setId(c.getInt(c.getColumnIndexOrThrow(_ID)));
        download.setName(c.getString(c.getColumnIndexOrThrow(NAME)));
        download.setTimestampCreated(c.getLong(c.getColumnIndexOrThrow(TIMESTAMP_CREATED)));
        String uris = c.getString(c.getColumnIndexOrThrow(URI));
        if (!TextUtils.isEmpty(uris)) {
            download.setUri(Uri.parse(uris));
        }
        download.setStatus(c.getInt(c.getColumnIndexOrThrow(STATUS)));
        download.setStatusMessage(c.getString(c.getColumnIndexOrThrow(STATUS_MESSAGE)));
        String filename = c.getString(c.getColumnIndexOrThrow(LOCAL_FILE));
        if (!TextUtils.isEmpty(filename)) {
            download.setLocalFile(new File(filename));
        }
        download.setContentLength(c.getLong(c.getColumnIndexOrThrow(CONTENT_LENGTH)));
        download.setContentProgress(c.getLong(c.getColumnIndexOrThrow(CONTENT_PROGRESS)));
        download.setEtag(c.getString(c.getColumnIndexOrThrow(ETAG)));
        download.setNumFailed(c.getInt(c.getColumnIndexOrThrow(NUM_FAILED)));
        download.setTimestampRetryAfter(c.getLong(c.getColumnIndexOrThrow(TIMESTAMP_RETRY_AFTER)));
        download.setTimestampUpdated(c.getLong(c.getColumnIndexOrThrow(TIMESTAMP_UPDATED)));
        String payload = c.getString(c.getColumnIndexOrThrow(SERVER_PAYLOAD));
        download.setVerifyPurchasePayload(payload);
        download.setCoverImageFileName(c.getString(c.getColumnIndexOrThrow(COVER_IMAGE_FILENAME)));

        return download;
    }

    private DownloadsTable() {
        /* prevent instantiation */
    }
}