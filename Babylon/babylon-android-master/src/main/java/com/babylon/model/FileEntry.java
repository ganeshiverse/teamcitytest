package com.babylon.model;

import android.text.TextUtils;

import com.babylon.BaseConfig;

import java.util.Arrays;

public class FileEntry extends ServerModel {

    @SkipFieldInContentValues
    private static final long serialVersionUID = 43L;

    public enum FileType {
        AUDIO, PHOTO
    }

    private String id;
    private String mimetype;
    private String name;
    private String extention;
    private String size;
    private Long date;
    transient private String questionId;
    @SkipFieldInContentValues
    private transient byte[] data;

    public boolean isAudio() {
        final String audioType = "audio";
        final String videoType = "video";

        boolean result = mimetype.contains(audioType) || mimetype.contains(videoType);

        return result;
    }

    public boolean isPhoto() {
        final String audioType = "image";

        boolean result = mimetype.contains(audioType);

        return result;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data != null ? Arrays.copyOf(data, data.length) : null;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameById() {
        String type = isAudio() ? "audio_" : "image_";
        this.name = type + id + "." + extention;
    }

    public String getExtention() {
        return extention;
    }

    public void setExtention(String extention) {
        this.extention = extention;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getQuestionId() {
        return questionId;
    }

    public Long getDate() {
        return getLongMills(date);
    }

    public void setDate(Long date) {
        this.date = getLongSec(date);
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getFilePath() {
        return TextUtils.concat(BaseConfig.CACHE_DIR, "/", getName()).toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRandomUUID() {
        id = getRandomUUIDStr();
    }

    public static class File {
        private FileEntry file;

        public FileEntry getFile() {
            return file;
        }
    }
}
