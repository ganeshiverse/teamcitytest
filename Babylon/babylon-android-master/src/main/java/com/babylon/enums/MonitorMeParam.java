package com.babylon.enums;

import java.util.ArrayList;
import java.util.List;

public enum MonitorMeParam {
    STEPS_ID("1", false),
    DISTANCE_ID("2", false),
    CALORIES_IN_ID("3"),
    CALORIES_BURNT_ID("4"),
    CALORIES_REMAINING_ID("5"),
    WEIGHT_ID("6"),
    BMI_ID("7", false),
    BODY_FAT_ID("8", false),
    RESTING_PULSE_ID("9"),
    ECG_ID("10"),
    HEART_STROKE_RISK_ID("11"),
    BLOOD_PRESSURE_ID("12"),
    CHOLESTEROL_ID("13"),
    CHOLESTEROL_HDL_ID("14"),
    CHOLESTEROL_LDL_ID("15"),
    TRIGLYCERIDE_ID("16"),
    SYSTOLIC_BP_ID("17"),
    DIASTOLIC_BP_ID("18"),
    CURRENT_HEIGHT_ID("19"),
    GOAL_WEIGHT_ID("20"),
    DIABETIC_ID("21"),
    HEART_TREATMENT_ID("22"),
    SLEEP_ID("24"),
    STRESS_ID("25"),
    REFRESHED_ID("26"),
    SMOKER_ID("27"),
    UREA_ID("31"),
    CREATININE_ID("32"),
    URIC_ACID_ID("33"),
    PC_RATIO_ID("34"),
    BILIRUBIN_ID("35"),
    ALP_ID("36"),
    AST_ID("37"),
    ALT_ID("38"),
    LDH_ID("39"),
    GAMMA_GT_ID("40"),
    PROTEIN_ID("41"),
    ALBUMIN_ID("42"),
    VIT_B_ID("43"),
    FOLATE_ID("44"),
    VIT_D_ID("45"),
    CALCIUM_ID("47"),
    PHOSPHATE_ID("48"),
    GLUCOSE_ID("49"),
    HBA1C_ID("50"),
    TSH_ID("51"),
    FT4_ID("52"),
    T3_ID("53"),
    HB_ID("54"),
    WCC_ID("55"),
    PIT_ID("56"),
    MCV_ID("57"),
    ESR_ID("58"),
    FERRITINE_ID("59"),
    IRON_ID("60"),
    TIBC_ID("61"),
    CYCLE_ID("62", false),
    WALK_AND_RUN_ID("69", false),
    BMR("65"),
    WEARABLE_CALORIES("66");

    private String id;
    /* Member which show order kit functionality in monitor screen */
    private boolean isMedicalKitExist;

    MonitorMeParam(String id) {
        this(id, true);
    }

    MonitorMeParam(String id, boolean isOrderExist) {
        this.id = id;
        this.isMedicalKitExist = isOrderExist;
    }

    public String getId() {
        return id;
    }

    public boolean isMedicalKitExist() {
        return isMedicalKitExist;
    }

    public static List<String> getParameterList(List<MonitorMeParam> params) {
        List<String> parameterList = new ArrayList<String>();
        for (MonitorMeParam param : params) {
            parameterList.add(param.getId());
        }
        return parameterList;
    }

    public static List<String> getParameterList() {
        List<String> parameterList = new ArrayList<String>();
        for (MonitorMeParam param : values()) {
            parameterList.add(param.getId());
        }
        return parameterList;
    }

    public static MonitorMeParam getMonitorMeParameter(String id) {
        for (MonitorMeParam param : values()) {
            if (param.getId().equalsIgnoreCase(id)) {
                return param;
            }
        }
        return null;
    }


}
