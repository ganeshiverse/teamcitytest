package com.iversecomics.client.downloads.internal.data;

public class DownloadStatus {
    /**
     * Pending Download.
     * <p>
     * Can be a download that hasn't started yet, or one that has already started, but has gone back into pending for
     * one reason or another.
     * <p>
     * A download can re-enter this state under a few conditions, such as:
     * <p>
     * <ul>
     * <li>Network failure during download. (out of cell coverage)</li>
     * <li>Server Busy with "Retry-After" response header</li>
     * <li>Redirect response to a different URI</li>
     * <li>Etc.</li>
     * </ul>
     * <p>
     * Download will begin at a time after {@link DownloadData#getTimestampRetryAfter()}
     */
    public static final DownloadStatus PENDING         = new DownloadStatus(1);
    
    /**
     * Download is being verified if it was purchased.
     * <p>
     * VerifyPurchaseTask is currently is processing the download. After this is done, it will 
     * go to Active status on success, or pending status on failure. The code 30 is just a number I picked.
     * 
     */
    public static final DownloadStatus VERIFYING         = new DownloadStatus(30);

    /**
     * Download is active.
     * <p>
     * Some thread currently is processing the download.
     */
    public static final DownloadStatus ACTIVE          = new DownloadStatus(2);

    /**
     * Download has successfully completed.
     * <p>
     * Warning: there might be other status values that indicate success in the future. Use .isSuccess() to capture the
     * entire possible list of success.
     */
    public static final DownloadStatus SUCCESS         = new DownloadStatus(200);

    /**
     * Permanent Failure.
     * <p>
     * Download request couldn't be parsed.
     * <p>
     * This is also used when processing requests with unknown / unsupported URI schemes.
     */
    public static final DownloadStatus BAD_REQUEST     = new DownloadStatus(400);

    /**
     * Permanent Failure.
     * <p>
     * This download cannot be performed because the length cannot be determined accurately.
     * <p>
     * This is the code for the HTTP error "Length Required", which is typically used when making requests that require
     * a content length but don't have one, and it is also used in the client when a response is received whose length
     * cannot be determined accurately (therefore making it impossible to know when a download completes).
     */
    public static final DownloadStatus LENGTH_REQUIRED = new DownloadStatus(411);

    /**
     * Permanent Failure.
     * <p>
     * Download was canceled.
     */
    public static final DownloadStatus CANCELED        = new DownloadStatus(491);

    /**
     * Permanent Failure.
     * <p>
     * Download couldn't be completed because of a storage issue.
     * <p>
     * Typically, that's because the filesystem is missing or full.
     */
    public static final DownloadStatus FILE_ERROR      = new DownloadStatus(492);

    /**
     * Permanent Failure.
     * <p>
     * Download couldn't be completed because of an error receiving or processing data at the HTTP level.
     */
    public static final DownloadStatus HTTP_DATA_ERROR = new DownloadStatus(495);

    private final int                  code;

    public DownloadStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public boolean isActive() {
        return (code == ACTIVE.code);
    }

    /**
     * Has the download reached a point where it is considered completed, either in success, or in failure.
     * 
     * @return
     */
    public boolean isCompleted() {
        return isSuccess() || isPermanentFailure();
    }

    public boolean isPending() {
        return (code == PENDING.code);
    }

    public boolean isVerifying() {
        return (code == VERIFYING.code);
    }
    
    public boolean isPermanentFailure() {
        return ((code >= 400) && (code < 600));
    }

    public boolean isSuccess() {
        return ((code >= 200) && (code < 300));
    }

    @Override
    public String toString() {
        if (code == PENDING.code) {
            return "PENDING";
        }

        if (code == ACTIVE.code) {
            return "ACTIVE";
        }

        if (code == SUCCESS.code) {
            return "SUCCESS";
        }

        if (code == BAD_REQUEST.code) {
            return "BAD_REQUEST";
        }

        if (code == LENGTH_REQUIRED.code) {
            return "LENGTH_REQUIRED";
        }

        if (code == CANCELED.code) {
            return "CANCELED";
        }

        if (code == FILE_ERROR.code) {
            return "FILE_ERROR";
        }

        if (code == HTTP_DATA_ERROR.code) {
            return "HTTP_DATA_ERROR";
        }

        if (isSuccess()) {
            return "SUCCESS(" + Integer.toString(code) + ")";
        }

        if (isPermanentFailure()) {
            return "PERMANENT_FAILURE(" + Integer.toString(code) + ")";
        }

        return "unknown_status(" + code + ")";
    }
}
