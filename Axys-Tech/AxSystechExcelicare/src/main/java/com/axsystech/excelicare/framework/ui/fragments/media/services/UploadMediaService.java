package com.axsystech.excelicare.framework.ui.fragments.media.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;

import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.DeleteMediaItemResponse;
import com.axsystech.excelicare.data.responses.UploadMediaItemResponse;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;

import java.util.concurrent.Callable;

/**
 * Created by someswarreddy on 20/09/15.
 */
public class UploadMediaService extends IntentService {

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    private static final String TAG = UploadMediaService.class.getSimpleName();

    private User mActiveUser;

    private MediaItemDbModel mediaItemToDelete = null;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public UploadMediaService() {
        super(UploadMediaService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Trace.d(TAG, "Upload media service started!");

        ResultReceiver receiver = null;
        if(intent.hasExtra(AxSysConstants.SERVICE_MEDIA_RECEIVER)) {
            receiver = intent.getParcelableExtra(AxSysConstants.SERVICE_MEDIA_RECEIVER);
        }

        MediaItemDbModel mediaItemToUpload = null;
        if(intent.hasExtra(AxSysConstants.SERVICE_UPLOAD_MEDIA_OBJECT)) {
            mediaItemToUpload = (MediaItemDbModel) intent.getSerializableExtra(AxSysConstants.SERVICE_UPLOAD_MEDIA_OBJECT);
        }

        if(intent.hasExtra(AxSysConstants.SERVICE_DELETE_MEDIA_OBJECT)) {
            mediaItemToDelete = (MediaItemDbModel) intent.getSerializableExtra(AxSysConstants.SERVICE_DELETE_MEDIA_OBJECT);
        }

        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = new Bundle();

        if (mActiveUser != null) {
            if(mediaItemToUpload != null) {

                try {
                    if(!mediaItemToUpload.isUploaded()) {
                        /* Update UI: UPLOAD Service is Running */
                        bundle.putSerializable(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT, mediaItemToUpload);
                        mediaItemToUpload.setIsUploading(true);
                        if (receiver != null) receiver.send(STATUS_RUNNING, bundle);

                        // if selected media is not uploaded to server, upload it
                        UploadMediaItemResponse uploadMediaItemResponse = ServerMethods.getInstance().uploadMediaItem(mediaItemToUpload,
                                mActiveUser.getToken(), mActiveUser.getPatientID());

                        /* Sending result back to activity */
                        if (uploadMediaItemResponse != null && !TextUtils.isEmpty(uploadMediaItemResponse.getData())) {
                            bundle.putSerializable(AxSysConstants.SERVICE_UPLOADED_MEDIA_OBJECT_RESPONSE, uploadMediaItemResponse);

                            if (receiver != null) receiver.send(STATUS_FINISHED, bundle);
                        }
                    } else {
                        /* Update UI: UPLOAD Service is Running */
                        bundle.putSerializable(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT, mediaItemToUpload);
                        mediaItemToUpload.setIsUploading(true);
                        if (receiver != null) receiver.send(STATUS_RUNNING, bundle);

                        UploadMediaItemResponse uploadMediaItemResponse = new UploadMediaItemResponse();
                        uploadMediaItemResponse.setData(mediaItemToUpload.getMediaId() + "");
                        bundle.putSerializable(AxSysConstants.SERVICE_UPLOADED_MEDIA_OBJECT_RESPONSE, uploadMediaItemResponse);

                        // if selected media is already uploaded to server, do not upload it again
                        if (receiver != null) receiver.send(STATUS_FINISHED, bundle);
                    }
                } catch (Exception e) {
                    /* Sending error message back to activity */
                    bundle.putString(AxSysConstants.SERVICE_ERROR_UPLOADING_MEDIA, e.toString());
                    if (receiver != null) receiver.send(STATUS_ERROR, bundle);
                }

            } else if(mediaItemToDelete != null) {


                /* DELETE media based on media id */
                bundle.putSerializable(AxSysConstants.SERVICE_CURRENT_DELETING_MEDIA_OBJECT, mediaItemToDelete);
                mediaItemToDelete.setIsUploading(true);
                if (receiver != null) receiver.send(STATUS_RUNNING, bundle);

                try {
                    // Delete media from loacal DB first
                    try {
                        final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(AxSysTechApplication.getAppContext(), DatabaseHelper.class);
                        final Dao<MediaItemDbModel, Long> mediaItemDbModelDao = helper.getDao(MediaItemDbModel.class);
                        mediaItemToDelete = TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<MediaItemDbModel>() {
                            @Override
                            public MediaItemDbModel call() throws Exception {
                                mediaItemDbModelDao.delete(mediaItemToDelete);

                                return mediaItemToDelete;
                            }
                        });
                    } finally {
                        OpenHelperManager.releaseHelper();
                    }

                    // if media id is > 0, then delete from server as well
                    if (mediaItemToDelete.getMediaId() > 0) {
                        DeleteMediaItemResponse deleteMediaItemResponse = ServerMethods.getInstance().deleteMediaItem(mediaItemToDelete.getMediaId(),
                                mActiveUser.getToken());

                        /* Sending result back to activity */
                        bundle.putSerializable(AxSysConstants.SERVICE_DELETED_MEDIA_OBJECT_RESPONSE, mediaItemToDelete);

                        if (receiver != null) receiver.send(STATUS_FINISHED, bundle);
                    } else {
                        /* Sending result back to activity */

                        bundle.putSerializable(AxSysConstants.SERVICE_DELETED_MEDIA_OBJECT_RESPONSE, mediaItemToDelete);

                        if (receiver != null) receiver.send(STATUS_FINISHED, bundle);

                    }
                } catch (Exception e) {
                    /* Sending error message back to activity */
                    bundle.putString(AxSysConstants.SERVICE_ERROR_DELETING_MEDIA, e.toString());
                    if (receiver != null) receiver.send(STATUS_ERROR, bundle);
                }


            }
        }
        Trace.d(TAG, "Upload media service Stopping!");

        this.stopSelf();
    }
}
