package com.babylon.utils;

import android.app.FragmentManager;
import android.os.Handler;
import android.os.Message;

import com.babylon.activity.BaseActivity;

import java.lang.ref.WeakReference;

public class HandlerLoaderListener extends Handler {
    public static final int BACK_PRESS_HANDLER_CODE = 2;
    public static final int POP_BACK_HANDLER_CODE = 3;

    private WeakReference<BaseActivity> activity;

    public HandlerLoaderListener(BaseActivity activity) {
        this.activity = new WeakReference<BaseActivity>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        if (msg.what == BACK_PRESS_HANDLER_CODE) {
            if (activity.get() != null && !activity.get().isFinishing()) {
                activity.get().onBackPressed();
            }
        }
        else if (msg.what == POP_BACK_HANDLER_CODE)
        {
            if (activity.get() != null && !activity.get().isFinishing()) {
               activity.get().popBackToFirstFragment();
            }
        }
    }
}
