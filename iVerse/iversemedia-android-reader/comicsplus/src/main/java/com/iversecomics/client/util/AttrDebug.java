package com.iversecomics.client.util;

import android.content.res.TypedArray;
import android.util.TypedValue;

import com.iversecomics.archie.android.R;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public final class AttrDebug {
    private static final Logger LOG = LoggerFactory.getLogger(AttrDebug.class);

    private static void dump(String prefix, String attrName, TypedValue val) {
        StringBuilder b = new StringBuilder();
        b.append(prefix).append(" {").append(attrName).append("} ");
        b.append("TypedValue = ");
        if (val == null) {
            b.append("<null>");
        } else {
            b.append("type=");
            switch (val.type) {
                case TypedValue.TYPE_ATTRIBUTE:
                    b.append("ATTRIBUTE");
                    break;
                case TypedValue.TYPE_DIMENSION:
                    b.append("DIMENSION");
                    break;
                case TypedValue.TYPE_FLOAT:
                    b.append("FLOAT");
                    break;
                case TypedValue.TYPE_FRACTION:
                    b.append("FRACTION");
                    break;
                case TypedValue.TYPE_INT_BOOLEAN:
                    b.append("INT_BOOLEAN");
                    break;
                case TypedValue.TYPE_INT_COLOR_ARGB4:
                    b.append("INT_COLOR_ARGB4");
                    break;
                case TypedValue.TYPE_INT_COLOR_ARGB8:
                    b.append("INT_COLOR_ARGB8");
                    break;
                case TypedValue.TYPE_INT_COLOR_RGB4:
                    b.append("INT_COLOR_RGB4");
                    break;
                case TypedValue.TYPE_INT_COLOR_RGB8:
                    b.append("INT_COLOR_RGB8");
                    break;
                case TypedValue.TYPE_INT_DEC:
                    b.append("INT_DEC");
                    break;
                case TypedValue.TYPE_INT_HEX:
                    b.append("INT_HEX");
                    break;
                case TypedValue.TYPE_NULL:
                    b.append("NULL");
                    break;
                case TypedValue.TYPE_REFERENCE:
                    b.append("REFERENCE");
                    break;
                case TypedValue.TYPE_STRING:
                    b.append("STRING");
                    break;
            }
        }
        b.append(",data=").append(val.data);
        b.append("(0x").append(Integer.toHexString(val.data)).append(")");
        b.append(",resourceId=").append(val.resourceId);
        b.append("(0x").append(Integer.toHexString(val.resourceId)).append(")");
        b.append(",string=").append(val.string);
        b.append(",density=").append(val.density);
        LOG.debug(b.toString());
    }

    public static void dump(TypedArray a, String attrPrefix) {
        int len = a.getIndexCount();
        Map<Integer, String> attrMap = loadAttrsViaPrefix(attrPrefix);
        LOG.debug(a.getPositionDescription());
        for (int i = 0; i < len; i++) {
            int attr = a.getIndex(i);
            TypedValue val = new TypedValue();
            a.getValue(attr, val);
            dump("Attr[" + i + "][" + attr + "]", attrMap.get(attr), val);
        }
    }

    private static Map<Integer, String> loadAttrsViaPrefix(String attrPrefix) {
        Map<Integer, String> attrMap = new HashMap<Integer, String>();
        Class<?> styleable = R.styleable.class;
        for (Field fld : styleable.getDeclaredFields()) {
            if (fld.getName().startsWith(attrPrefix)) {
                try {
                    int val = fld.getInt(styleable);
                    attrMap.put(val, fld.getName().substring(attrPrefix.length()));
                } catch (Throwable ignore) {
                    /* ignore */
                }
            }
        }
        return attrMap;
    }
}
