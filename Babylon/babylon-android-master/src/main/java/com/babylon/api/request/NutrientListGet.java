package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Food;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class NutrientListGet extends AbsRequestGet<ResponseList<Food>> {

    private static final String DATE_FROM = "dateFrom";
    private static final String DATE_TO = "dateTo";

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.NUTRIENT_LIST);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param dateFrom date, timestamp mills
     * @param dateTo   date, timestamp mills
     */
    public static NutrientListGet newNutrientGet(long dateFrom, long dateTo) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (dateFrom > 0) {
            params.add(new BasicNameValuePair(DATE_FROM, String.valueOf(dateFrom / 1000)));
        }
        if (dateTo > 0) {
            params.add(new BasicNameValuePair(DATE_TO, String.valueOf(dateTo / 1000)));
        }
        return (NutrientListGet) new NutrientListGet().withParams(params);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }

}
