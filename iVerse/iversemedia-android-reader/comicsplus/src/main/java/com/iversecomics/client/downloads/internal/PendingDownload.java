package com.iversecomics.client.downloads.internal;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;

import com.iversecomics.app.SplaschScreenActivity;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.downloads.DownloaderConstants;
import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.downloads.internal.android.NetLock;
import com.iversecomics.client.downloads.internal.data.DownloadData;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.data.DownloadsTable;
import com.iversecomics.client.downloads.internal.net.DownloadClient;
import com.iversecomics.client.downloads.internal.net.DownloadFactory;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.client.downloads.internal.net.DownloadMonitorCollection;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.io.IOUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.SubscriptionSaveOffLineErrorEvent;

import org.acra.ErrorReporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Random;

/**
 * A pending download, what is submitted to the {@link com.iversecomics.client.downloads.internal.DownloadPool} for eventual download.
 */
public class PendingDownload implements Runnable {
    private static final Logger LOG = LoggerFactory
            .getLogger(PendingDownload.class);
    private static final String TAG = PendingDownload.class.getSimpleName();
    private DownloadData download;
    private DownloadMonitorCollection monitors;
    private NetLock netlock;
    private ContentProviderClient dbclient;

    public PendingDownload(DownloadData download) {
        this.download = download;
        this.monitors = new DownloadMonitorCollection();
    }

    public void addMonitor(DownloadMonitor monitor) {
        this.monitors.add(monitor);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PendingDownload other = (PendingDownload) obj;
        if (download == null) {
            if (other.download != null) {
                return false;
            }
        } else if (download.getUri() == null) {
            if (other.download.getUri() != null) {
                return false;
            }
        } else if (!download.getUri().equals(other.download.getUri())) {
            return false;
        }
        return true;
    }

    public NetLock getNetlock() {
        return netlock;
    }

    private ContentValues getStatusAsContentValues() {
        ContentValues values = new ContentValues();
        values.put(DownloadsTable.STATUS, download.getStatus().getCode());
        values.put(DownloadsTable.STATUS_MESSAGE, download.getStatusMessage());
        values.put(DownloadsTable.NUM_FAILED, download.getNumFailed());
        return values;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        int multiplier = 0;
        if (download != null) {
            Uri uri = download.getUri();
            if (uri != null) {
                multiplier = uri.hashCode();
            }
        }
        result = prime * result + multiplier;
        return result;
    }

    @Override
    public void run() {

        final ServerApi server = IverseApplication.getApplication().getComicStore().getServerApi();
        Uri uri = null;

        if (DownloaderReceiver.isNetworkAvail() == false) {
            LOG.warn("Network is down. Will not attempt to download comic: " + download.getName());
            return;
        }


        download.setStatus(DownloadStatus.VERIFYING);
        download.setNumFailed(download.getNumFailed() + 1);
        // Update Database
        updateDatabaseEntry(getStatusAsContentValues());


        try {

            JSONObject payload = new JSONObject(download.getVerifyPurchasePayload());
            LOG.debug("Sending JSON to server for purchase verification: %s", payload.toString());
            final JSONObject json = server.verifyPurchase(payload);
            final String status = json.optString("status");
            LOG.debug("Received JSON purchase response from server: %s", json.toString());

            if (status.equals("ok")) {
                //Pull the download URL, name and product ID of the downloaded product from the JSON response
                final String productId = json.optString("productId");
                final String productName = json.optString("productName");
                final String productUrl = json.optString("productUrl");
                uri = Uri.parse(productUrl);

                //Set the name
                download.setName(productName);

                //If this is already installed, silently cancel the download
                final MyComicsModel model = new MyComicsModel(IverseApplication.getApplication().getApplicationContext());
                final MyComic myComic = model.getComicByBundleName(productId);
                if (myComic != null) {
                    //Fail out the database entry
                    LOG.debug("Skipping download for %s - already installed.", productId);
                    download.setStatusMessage("Already installed");
                    download.setStatus(DownloadStatus.CANCELED);
                    download.setNumFailed(999);
                    updateDatabaseEntry(getStatusAsContentValues());
                    return;
                }
            } else {
                Context context = IverseApplication.getApplication().getApplicationContext();
                if(status.equals("error")){
                    JSONObject error = json.getJSONObject("error");
                    String errorCode = error.optString("errorCode");
                    if(errorCode.equals("10981") ){
                        OttoBusProvider.getInstance().post(new SubscriptionSaveOffLineErrorEvent(errorCode, error.optString("errorMessage")));
                        return;
                    }


                }
                //The server says we failed, so don't retry
                LOG.debug("Server rejected the download request: %s", json.toString());

                download.setStatusMessage("Purchase authentication failure");
                download.setStatus(DownloadStatus.CANCELED);
                download.setNumFailed(999);
                updateDatabaseEntry(getStatusAsContentValues());

                //Display a notification informing the user of the failure

                final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                //this is optional - on touching the notification go back to the purchases list
                // TODO: joliver: PreviousPurchasesActivity is dead, this will need to be migrated to the comicsplus project,
                // change PreviousPurchasesActivity to MainActivity
//                final Intent notificationIntent = new Intent(context, PreviousPurchasesActivity.class);
//                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                // TODO: Ganesh : Need to change in future, it may be launch PreviousPurchasesActivity, but not splash screen. As of now we are launching Splashscreen to fix the Null pointer exception.
                final Intent notificationIntent = new Intent(context, SplaschScreenActivity.class);

                final Resources res = context.getResources();
                final Notification notification =
                        new Notification(R.drawable.icon,
                                res.getString(R.string.notification_download_failed_title),
                                System.currentTimeMillis());

                if(context !=null && notificationIntent !=null) {
                    final PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    final String comicTitle = res.getString(R.string.notification_download_failed_title);
                    notification.setLatestEventInfo(context, comicTitle, "Unable to authorize download.  Restore Purchases to retry.", contentIntent);
                    mNotificationManager.notify(0, notification);
                }
                return;
            }

        } catch (JSONException e) {
            LOG.debug("Couldn't parse purchase verification: %s", e.toString());

            // notify monitor
            DownloadException dexception = new DownloadException(download.getStatus(), e.getMessage());
            monitors.downloadFailure(uri, dexception);

            download.setStatusMessage(e.getMessage());
            download.setStatus(DownloadStatus.PENDING);

            // Save into database.
            updateDatabaseEntry(getStatusAsContentValues());
            return;
        } catch (ComicStoreException e) {
            LOG.debug("Comic store error %s", e.toString());

            // notify monitor
            DownloadException dexception = new DownloadException(download.getStatus(), e.getMessage());
            monitors.downloadFailure(uri, dexception);

            download.setStatusMessage(e.getMessage());
            download.setStatus(DownloadStatus.PENDING);

            // Save into database.
            updateDatabaseEntry(getStatusAsContentValues());

            return;
        } finally {

            //Purge bad database entries from the db
            Intent dbCleanupIntent = new Intent();
            dbCleanupIntent.setAction(DownloaderConstants.ACTION_CLEANUP);
            IverseApplication.getApplication().getApplicationContext().sendBroadcast(dbCleanupIntent);
            //LocalBroadcastManager.getInstance(IverseApplication.getApplication().getApplicationContext()).sendBroadcast(dbCleanupIntent);

        }

        // Be polite
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        // Set as active
        download.setStatus(DownloadStatus.ACTIVE);
        download.setStatusMessage("");

        // Update Database
        updateDatabaseEntry(getStatusAsContentValues());

        // Acquire Wakelock
        netlock.acquire("PendingDownload-" + hashCode());

        // Get download client from common httpclient configuration
        DownloadClient client = DownloadFactory.getDownloadClient();

        // Figure out what to do with regards to output file.
        FileOutputStream out = null;
        try {
            File outputFile = download.getLocalFile();

            // Uri uri = download.getUri();
            client.setMonitor(monitors);
            //TODO fix this at some point
            /*
             * This code does not work commenting out for now
            if (outputFile.exists()) {
                long fileLength = outputFile.length();
                if (fileLength == 0) {
                    // Download hasn't actually started.
                    // Download from scratch.
                    if (!outputFile.delete()) {
                        Log.w(TAG, "Unable to delete " + outputFile);
                    }
                } else {
                    // Resume download from last known length.
                    client.setStartRange(fileLength);
                    if (download.hasEtag()) {
                        // Resume download from ETag
                        client.setEtag(download.getEtag());
                    }
                }
            }
            */

            if (outputFile.exists()) {
                if (!outputFile.delete()) {
                    Log.w(TAG, "Unable to delete " + outputFile);
                }

            }

            // Establish output stream to file
            out = new FileOutputStream(outputFile);
            // Initiate download
            client.download(uri, out, download.getName());
        } catch (ServiceUnavailableException e) {
            // Server reported that service is not available.
            long retryAfter = e.getRetryAfter();
            if (retryAfter < 0) {
                // No specified retry after, use a random one.
                Random random = new Random(SystemClock.uptimeMillis());
                int minRetry = DownloaderManager.getInstance().getRetriesMinAfter();
                retryAfter = System.currentTimeMillis() + random.nextInt(minRetry + 1);
            }
            download.setStatus(DownloadStatus.PENDING);
            download.setStatusMessage("Service Unavailable, Retrying");
            download.setTimestampRetryAfter(retryAfter);

            // Save into database.
            ContentValues values = getStatusAsContentValues();
            values.put(DownloadsTable.TIMESTAMP_RETRY_AFTER, download.getTimestampRetryAfter());
            updateDatabaseEntry(values);
        } catch (FileNotFoundException e) {
            // Unable to create file for output
            download.setStatus(DownloadStatus.PENDING);
            download.setStatusMessage("Unable to create file: " + download.getLocalFile());

            // Save into database.
            updateDatabaseEntry(getStatusAsContentValues());

            // Report to ACRA
            ErrorReporter.getInstance().handleSilentException(e);
        } catch (FileWriteException e) {
            // Unable to write to disk
            download.setStatus(e.getStatus());
            download.setStatusMessage(e.getMessage());

            // Save into database.
            updateDatabaseEntry(getStatusAsContentValues());
        } catch (DownloadFailedException e) {
            // Generic unrecoverable exception
            download.setStatus(e.getStatus());
            download.setStatusMessage(e.getMessage());

            // Save into database.
            updateDatabaseEntry(getStatusAsContentValues());
        } catch (DownloadException e) {
            // Generic recoverable exception
            download.setStatus(DownloadStatus.PENDING);
            download.setStatusMessage(e.getMessage());

            // Retry after a set time.
            int minRetry = DownloaderManager.getInstance().getRetriesMinAfter();
            download.setTimestampRetryAfter(System.currentTimeMillis() + minRetry);

            // Save into database.
            ContentValues values = getStatusAsContentValues();
            values.put(DownloadsTable.TIMESTAMP_RETRY_AFTER, download.getTimestampRetryAfter());
            updateDatabaseEntry(values);
        } catch (IllegalStateException e) {
            download.setStatus(DownloadStatus.HTTP_DATA_ERROR);
            download.setStatusMessage(e.getMessage());

            // Save into database.
            updateDatabaseEntry(getStatusAsContentValues());

            // Report to ACRA
            ErrorReporter.getInstance().handleSilentException(e);
        } finally {
            IOUtil.close(out);
            client.close();
            netlock.release();
        }
    }

    public void setDatabaseClient(ContentProviderClient cpclient) {
        this.dbclient = cpclient;
    }

    public void setNetlock(NetLock netlock) {
        this.netlock = netlock;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("PendingDownload[");
        b.append(download);
        b.append("]");
        return b.toString();
    }

    private void updateDatabaseEntry(ContentValues values) {
        Uri uri = ContentUris.withAppendedId(DownloadsTable.CONTENT_URI, download.getId());
        try {
            dbclient.update(uri, values, null, null);
        } catch (RemoteException e) {
            Log.e(TAG, "Unable to update Database entry", e);
        }
    }
}
