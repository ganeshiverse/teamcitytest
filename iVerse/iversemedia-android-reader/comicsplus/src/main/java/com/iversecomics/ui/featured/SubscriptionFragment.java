package com.iversecomics.ui.featured;

import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.store.AppStoreAdapterLocator;
import com.iversecomics.store.ComicBuyer;
import com.iversecomics.ui.main.MainActivity;
import com.iversecomics.ui.widget.ComicSlot;
import com.iversecomics.ui.widget.NavigationBar;
import com.iversecomics.util.Util;
import com.iversecomics.util.ui.UiUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.squareup.otto.Bus;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SubscriptionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SubscriptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class SubscriptionFragment extends android.support.v4.app.Fragment implements AbstractFeaturedContentAdapter.Listener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public static final String TAG = FeaturedComicsFragment.class.getSimpleName();
    public static final String ARG_ON_DEMAND = "com.iverse.ON_DEMAND";
    private final static Logger LOG = LoggerFactory.getLogger(FeaturedComicsFragment.class);

    private ComicBuyer mComicBuyer;
    private AbstractAppStoreAdapter appStoreAdapter;

    ViewGroup rootView;
    ViewGroup featureFrame;
    ViewPager featurePager;
    View processingOverlay;
    AbstractFeaturedContentAdapter featureContent;
    Bus bus = OttoBusProvider.getInstance();

    private Button readOnDemandBtn;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment SubscriptionFragment.
     */
    // TODO: Rename and change types and number of parameters

    public static SubscriptionFragment newInstance() {
        Bundle args = new Bundle();
        SubscriptionFragment frag = new SubscriptionFragment();
        frag.setArguments(args);
        return frag;
    }
    public SubscriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        boolean flagOnDemand  =true;
        featureContent = flagOnDemand ? new OnDemandFeaturedContentAdapter() : new FeaturedContentAdapter();
        featureContent.setListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_subscription, null, false);

        processingOverlay = rootView.findViewById(R.id.processing_overlay);
        initSlots();
        NavigationBar navBar = (NavigationBar) rootView.findViewById(R.id.navbar);
//        navBar.setNavigationListener((NavigationBar.NavigationListener) getActivity());
        navBar.setBackButtonVisibility(View.GONE);

        if (getActivity() instanceof AppStoreAdapterLocator) {
            appStoreAdapter = ((AppStoreAdapterLocator) getActivity()).getAppStoreAdapter();
        } else {
            throw new IllegalArgumentException("Invalid activity. Must implement AppStoreAdapterLocator.");
        }
        mComicBuyer = new ComicBuyer(getActivity(), appStoreAdapter);
        // Inflate the layout for this fragment
        return rootView;
    }

    public void initSlots() {
        featurePager = (ViewPager) (rootView.findViewById(R.id.feature));
        featureFrame = (ViewGroup) (rootView.findViewById(R.id.feature_frame));
        readOnDemandBtn =  (Button)rootView.findViewById(R.id.btn_readondemand);
        readOnDemandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }



    private void refreshSlots() {
        if (featurePager != null) {
            hideSlots();
            ImageLoader imageLoader = ImageLoader.getInstance();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        showLoadingDialog();
        hideSlots();
        bus.register(featureContent);
    }

    @Override
    public void onPause() {
        bus.unregister(featureContent);
        super.onPause();
    }

    private void hideSlots() {
        featureFrame.setVisibility(View.INVISIBLE);
    }
    private void animateSlot(View slot, boolean flagFeature) {

        int durationTranslate = (int) (300 + 200 * Util.getRandom());
        int durationRotate1 = durationTranslate + 300;
        int durationRotate2 = 50;

        TranslateAnimation translation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, -(2.3f + 1.7f * Util.getRandom())
                , TranslateAnimation.RELATIVE_TO_SELF, 0.0f);
        translation.setDuration(durationTranslate);
        translation.setRepeatCount(0);
        translation.setFillAfter(true);

        if (flagFeature) {
            translation.setDuration(durationTranslate * 2);
            slot.startAnimation(translation);
        } else {
            float degree = 10 * (Util.getRandom() + 0.2f);
            float rx = 1.0f;
            if ((int) (Util.getRandom() * 10000) % 2 == 0) {
                degree = -degree;
                rx = 0.0f;
            }
            RotateAnimation rotation1 = new RotateAnimation(
                    degree, degree
                    , RotateAnimation.RELATIVE_TO_SELF, rx
                    , RotateAnimation.RELATIVE_TO_SELF, 1.0f);
            rotation1.setDuration(durationRotate1);
            rotation1.setRepeatCount(0);
            rotation1.setFillAfter(true);
            RotateAnimation rotation2 = new RotateAnimation(
                    0, -degree
                    , RotateAnimation.RELATIVE_TO_SELF, rx
                    , RotateAnimation.RELATIVE_TO_SELF, 1.0f);
            rotation2.setDuration(durationRotate2);
            rotation2.setStartOffset(durationRotate1);
            rotation2.setFillAfter(true);

            AnimationSet set = new AnimationSet(true);
            set.addAnimation(rotation1);
            set.addAnimation(rotation2);
            set.addAnimation(translation);

            set.setDuration(durationRotate1 + durationRotate2);
            set.setFillAfter(true);

            slot.startAnimation(set);
            slot.setVisibility(View.VISIBLE);
        }
    }

    public void showLoadingDialog() {
        processingOverlay.setVisibility(View.VISIBLE);
    }

    private void hideLoadingDialog() {
        processingOverlay.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSlotsUpdated(List<FeaturedSlot> slots) {
        refreshSlots();
    }

    @Override
    public void onFeaturedUpdated(List<Comic> comics) {
        featurePager.setAdapter(new ComicPagerAdapter(comics));
    }

    class ComicPagerAdapter extends PagerAdapter {

        private final List<Comic> comics;

        ComicPagerAdapter(List<Comic> comics) {
            this.comics = comics;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            final View view = inflater.inflate(
                    R.layout.fragment_comic_cover, container, false);

            ImageView cover = (ImageView) view.findViewById(R.id.cover);
            ImageView ondeamand = (ImageView) view.findViewById(R.id.ondeamand);
            Comic comic = comics.get(position);
            view.setTag(comic);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ((MainActivity) getActivity()).onComicClicked((Comic) v.getTag());
                }
            });
            if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                int onDemandVisibility = comic.getSubscription() ? View.VISIBLE : View.GONE;
                ondeamand.setVisibility(onDemandVisibility);
            }
            int height = UiUtil.getScreenHeight(getActivity());
            CoverSize coverSize = CoverSize.bestFit(height / 2);
            Uri coverImageUri = coverSize.getServerUri(comic.getImageFileName());

            ImageLoader.getInstance().displayImage(coverImageUri.toString(), cover, new MainImageListener());

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            int size = comics.size();
            Log.i(TAG, "featureContent size: " + size);
            return size;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private class MainImageListener implements ImageLoadingListener {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                animate();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                animate();
            }

            private void animate() {
                if (featureFrame.getVisibility() == View.INVISIBLE) {
                    hideLoadingDialog();
                    animateSlot(featureFrame, true);
                    featureFrame.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }
    }

    private class FeaturedImageListener implements ImageLoadingListener {
        View animatingView;

        private FeaturedImageListener(View animatingView) {
            this.animatingView = animatingView;
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {

        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            showSlot();
        }

        private void showSlot() {
            hideLoadingDialog();
            animateSlot(animatingView, false);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            showSlot();
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }
    }

}
