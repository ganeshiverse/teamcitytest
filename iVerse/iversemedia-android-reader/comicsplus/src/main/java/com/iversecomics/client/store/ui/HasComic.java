package com.iversecomics.client.store.ui;

import com.iversecomics.client.store.model.Comic;

public interface HasComic {
    Comic getComic();

    String getComicBundleName();
}
