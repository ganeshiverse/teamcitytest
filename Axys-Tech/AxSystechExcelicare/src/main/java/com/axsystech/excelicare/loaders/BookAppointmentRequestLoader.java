package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.BookAppointmentResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 25/08/15.
 */
public class BookAppointmentRequestLoader extends DataAsyncTaskLibLoader<BookAppointmentResponse> {

    private String postData;

    public BookAppointmentRequestLoader(Context context, String postData) {
        super(context);
        this.postData = postData;
    }

    @Override
    protected BookAppointmentResponse performLoad() throws Exception {
        return ServerMethods.getInstance().bookAppointment(postData);
    }
}
