package com.babylon.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.PasswordsPatientPost;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.model.Patient;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.UIUtils;
import com.babylon.view.TickButtonWithProgress;
import com.babylon.view.TopBarInterface;

public class ResetPasswordActivity extends BaseActivity
        implements View.OnClickListener, TextWatcher {

    private EditText emailEdt;
    private TickButtonWithProgress resetPasswordBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        getActionBar().show();
        setupActionBar(ActionBarType.RESET_PASSWORD);
        initViews();
    }

    public void initViews() {
        emailEdt = (EditText) findViewById(R.id.emailEdt);
        resetPasswordBtn = (TickButtonWithProgress) findViewById(R.id.resetPasswordBtn);
        //View topBarLeftImageButton = findViewById(R.id.topbar_left_image_button);

        resetPasswordBtn.setOnClickListener(this);
        emailEdt.addTextChangedListener(this);
//        topBarLeftImageButton.setOnClickListener(this);

        /*setTitle(getString(R.string.reset_password_title));*/
        resetPasswordBtn.setEnabled(false);
        /*initializeTopBar();*/
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PURPLE_THEME;
    }

    @Override
    public void onTopBarButtonPressed() {
        super.onTopBarButtonPressed();
        UIUtils.hideSoftKeyboard(this, emailEdt);
    }

    private Patient getPatient() {
        Patient patient = new Patient();
        patient.setEmail(emailEdt.getText().toString());
        return patient;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.resetPasswordBtn:
                submit();
                break;
            case R.id.topbar_left_image_button:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable text) {
        resetPasswordBtn.setEnabled(!TextUtils.isEmpty(text.toString()));
    }

    private void submit() {
        if (App.getInstance().isConnection()) {
            PasswordsPatientPost passwordsPatientPost = new PasswordsPatientPost();
            passwordsPatientPost.withEntry(getPatient());
            resetPasswordBtn.setProgressState(true);
            passwordsPatientPost.execute(this, new RequestListener<Void>() {
                @Override
                public void onRequestComplete(Response<Void> response) {
                    processResponse(response);
                }
            });
        } else {
            App.getInstance().showWaitingMessage(R.string.offline_error);
        }
    }

    private void processResponse(Response<Void> response) {
        resetPasswordBtn.setProgressState(false);
        if (response.isSuccess()) {
            AlertDialogHelper.getInstance().showMessage(this,
                    R.string.reset_password_success, new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            resetSuccess();
                        }
                    }
            );
        } else {
            AlertDialogHelper.getInstance().showMessage(ResetPasswordActivity.this,
                    getString(R.string.reset_password_failure));
        }
    }

    private void resetSuccess() {
        setResult(RESULT_OK);
        finish();
    }
}
