package com.iversecomics.client.downloads.internal;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.acra.ErrorReporter;

import android.app.Service;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;

import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.downloads.internal.android.BroadcastCompletionMonitor;
import com.iversecomics.client.downloads.internal.android.NetLock;
import com.iversecomics.client.downloads.internal.android.RetryMonitor;
import com.iversecomics.client.downloads.internal.data.DatabaseDownloadMonitor;
import com.iversecomics.client.downloads.internal.data.DownloadData;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.data.DownloadsDB;
import com.iversecomics.client.downloads.internal.data.DownloadsTable;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DownloaderService extends Service {
    public class DownloadInitiator implements Runnable {
        @Override
        public void run() {
            LOG.debug("DownloadInitiator.run()");
            String projection[] = null;
            String selection = "( " + DownloadsTable.TIMESTAMP_RETRY_AFTER + " < ? )";
            String selectionArgs[] = {
                Long.toString(System.currentTimeMillis())
            };

            Uri baseContentUri = DownloadsTable.CONTENT_URI;
            Cursor cursor = null;

            try {
                cursor = cpclient.query(baseContentUri, projection, selection, selectionArgs, DownloadsTable._ID);
            } catch (RemoteException e) {
                LOG.warn("Unable to query for downloads", e);
                ErrorReporter.getInstance().handleSilentException(e);
            }

            if (cursor == null) {
                LOG.warn("cursor is null?");
                return;
            }

            try {
                scanForDownloads(baseContentUri, cursor);
            } finally {
                DBUtil.close(cursor);
            }
        }

        private void scanForDownloads(Uri baseContentUri, Cursor cursor) {
            Context context = getApplicationContext();
            int addedCount = 0;

            cursor.moveToFirst();

            boolean isAfterLast = cursor.isAfterLast();
            while (!isAfterLast) {
                DownloadData download = DownloadsTable.fromCursor(cursor);

                if (!download.getStatus().isSuccess() && !download.getStatus().isPermanentFailure() &&!download.getStatus().isVerifying() &&!download.getStatus().isActive()) {
                	
                    PendingDownload pending = new PendingDownload(download);
                    pending.setDatabaseClient(cpclient);
                    pending.setNetlock(new NetLock(context));
                    Uri contentUri = ContentUris.withAppendedId(baseContentUri, download.getId());
                    pending.addMonitor(new DatabaseDownloadMonitor(cpclient, contentUri));
                    pending.addMonitor(new BroadcastCompletionMonitor(context, download));
                    pending.addMonitor(retryMonitor);

                    for (DownloadMonitor monitor : DownloaderManager.getInstance().getDownloadMonitors()) {
                        pending.addMonitor(monitor);
                    }

                    pool.execute(pending);
                    addedCount++;
                }

                // Iterate
                cursor.moveToNext();
                isAfterLast = cursor.isAfterLast();
            }

            if (addedCount > 0) {
                LOG.warn("Queued %d new download(s)", addedCount);
            }
        }
    }

    private static final Logger   LOG = LoggerFactory.getLogger(DownloaderService.class);
    private DownloadPool          pool;
    private ContentProviderClient cpclient;
    private RetryMonitor          retryMonitor;
    private ExecutorService       initiatorPool;
    private Runnable              initiator;
    private static boolean onBoot = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initiatorPool = Executors.newSingleThreadExecutor();
        pool = new DownloadPool(DownloaderManager.getInstance());
        cpclient = getContentResolver().acquireContentProviderClient(DownloadsDB.AUTHORITY);
        retryMonitor = new RetryMonitor(getApplicationContext());
        initiator = new DownloadInitiator();
        cleanDB();
    }

    @Override
    public void onDestroy() {
        if (pool != null) {
            pool.shutdown();
        }
        if (cpclient != null) {
            cpclient.release();
        }
        super.onDestroy();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        initiatorPool.execute(initiator);
    }
    
    public static void setOnBoot(){
    	onBoot = true;
    }
    
    
    //We are starting fresh.... lets clean up any downloads in bad state
    private void cleanDB() {
        String projection[] = null;
        String selection = "( " + DownloadsTable.TIMESTAMP_RETRY_AFTER + " < ? )";
        String selectionArgs[] = {
            Long.toString(System.currentTimeMillis())
        };

        Uri baseContentUri = DownloadsTable.CONTENT_URI;
        Cursor cursor = null;


        try {
            cursor = cpclient.query(baseContentUri, projection, selection, selectionArgs, DownloadsTable._ID);
        } catch (RemoteException e) {
            LOG.warn("Unable to query for downloads", e);
            ErrorReporter.getInstance().handleSilentException(e);
        }

        if (cursor == null) {
            LOG.warn("cursor is null?");
            return;
        }

        cursor.moveToFirst();

        boolean isAfterLast = cursor.isAfterLast();
        while (!isAfterLast) {
            DownloadData download = DownloadsTable.fromCursor(cursor);

            if (download.getStatus().isVerifying() || download.getStatus().isActive()) {
                LOG.debug("Cleaning up the following download: " + download.getName());
            	download.setStatus(DownloadStatus.PENDING);
            	updateDatabaseEntry(download);
            }

            // Iterate
            cursor.moveToNext();
            isAfterLast = cursor.isAfterLast();
        }
        DBUtil.close(cursor);
    }
    
    
    private void updateDatabaseEntry(DownloadData download) {
        Uri uri = ContentUris.withAppendedId(DownloadsTable.CONTENT_URI, download.getId());
        try {
        	cpclient.update(uri, getStatusAsContentValues(download), null, null);
        } catch (RemoteException e) {
            LOG.error("Unable to update Database entry: " + e);
        }
    }
    
    private ContentValues getStatusAsContentValues(DownloadData download) {
        ContentValues values = new ContentValues();
        values.put(DownloadsTable.STATUS, DownloadStatus.PENDING.getCode());
        values.put(DownloadsTable.STATUS_MESSAGE, "");
        values.put(DownloadsTable.NUM_FAILED, 0);
        return values;
    }
}
