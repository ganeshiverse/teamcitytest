package com.iversecomics.client.downloads.internal.data;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.net.Uri;
import android.os.RemoteException;

import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DatabaseDownloadMonitor implements DownloadMonitor {
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseDownloadMonitor.class);
    private ContentProviderClient client;
    private Uri contentUri;

    public DatabaseDownloadMonitor(ContentProviderClient client, Uri contentUri) {
        this.client = client;
        this.contentUri = contentUri;
    }

    @Override
    public void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        ContentValues values = new ContentValues();
        values.put(DownloadsTable.ETAG, etag);
        values.put(DownloadsTable.CONTENT_LENGTH, totalBytes);
        try {
            int updateCount = client.update(contentUri, values, null, null);
            if (updateCount != 1) {
                LOG.debug("Updated %d database entries", updateCount);
            }
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to update database: %s", contentUri);
        }
    }

    @Override
    public void downloadFailure(Uri uri, DownloadException failure) {
        /* ignore */
    }

    @Override
    public void downloadProgress(Uri uri, long currentByte) {
        ContentValues values = new ContentValues();
        values.put(DownloadsTable.CONTENT_PROGRESS, currentByte);
        try {
            int updateCount = client.update(contentUri, values, null, null);
            if (updateCount != 1) {
                LOG.debug("Updated %d database entries", updateCount);
            }
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to update database: %s", contentUri);
        }
    }

    @Override
    public void downloadState(Uri uri, DownloadStatus state, String message) {
        ContentValues values = new ContentValues();
        values.put(DownloadsTable.STATUS, state.getCode());
        values.put(DownloadsTable.STATUS_MESSAGE, message);
        try {
            int updateCount = client.update(contentUri, values, null, null);
            if (updateCount != 1) {
                LOG.debug("Updated %d database entries", updateCount);
            }
        } catch (RemoteException e) {
            LOG.warn(e, "Unable to update database: %s", contentUri);
        }
    }
}
