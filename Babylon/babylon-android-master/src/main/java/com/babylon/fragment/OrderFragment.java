package com.babylon.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.adapter.OrderAdapter;
import com.babylon.model.payments.Order;
import com.babylon.utils.Constants;
import com.babylon.utils.UIUtils;
import com.babylon.validator.ValidationController;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Fragment {@link android.support.v4.app.Fragment} for viewing and changing user parameters
 */
public class OrderFragment extends BaseFragment implements View.OnClickListener {

    private List<Order> orderList;
    private ListView orderListView;
    private OrderAdapter orderAdapter;
    private TextView subtotalPrice;
    private TextView subtotalQty;
    private TextView totalTax;

    public static OrderFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        OrderFragment fragment = new OrderFragment();
        fragment.setArguments(b);
        return fragment;
    }

    private ValidationController validationController = new ValidationController();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        orderList = new ArrayList<Order>();
        for (int i = 1; i < 10; i++) {
            orderList.add(new Order("Title " + i, "Description " + i, (double) i, "$"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.order_title);

        view.findViewById(R.id.acceptButton).setOnClickListener(this);

        orderAdapter = new OrderAdapter(getActivity(), orderList, textWatcher);

        orderListView = (ListView) view.findViewById(R.id.orderList);
        orderListView.setAdapter(orderAdapter);

        subtotalPrice = (TextView) view.findViewById(R.id.subtotalPrice);
        subtotalQty = (TextView) view.findViewById(R.id.subtotalQty);
        totalTax = (TextView) view.findViewById(R.id.totalTax);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptButton:
                saveNewData();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isBackAllowed() {
        if (orderAdapter.getCount() > 0) {
            View view = orderListView.getChildAt(0);
            EditText qty = (EditText) view.findViewById(R.id.qty);
            UIUtils.hideSoftKeyboard(getActivity(), qty);
        }
        return true;
    }

    public void saveNewData() {
        if (Integer.valueOf(subtotalQty.getText().toString()) > 0
                && validationController.performValidation()) {

            TransactionResultFragment fragment = TransactionResultFragment
                    .getInstance(getString(R.string.transaction_success_text));

            startFragment(fragment, true);
        } else {
            UIUtils.showSimpleToast(getActivity(), R.string.order_no_quantity);
        }
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            for (int i = 0; i < orderListView.getChildCount(); i++) {
                View view = orderListView.getChildAt(i);
                EditText qtyEditText = (EditText) view.findViewById(R.id.qty);
                if (qtyEditText.hasFocus()) {

                    Integer position = (Integer) qtyEditText.getTag(R.id.position);
                    String qtyText = qtyEditText.getText().toString();

                    if (!TextUtils.isEmpty(qtyText)) {
                        orderList.get(position).setQty(Integer.valueOf(qtyText));
                    } else {
                        orderList.get(position).setQty(null);

                    }
                }
            }

            recalculateSubtotalAndTax();
        }
    };

    public void recalculateSubtotalAndTax() {
        int resultSubtotalQty = 0;
        double resultSubtotalPrice = 0;
        double resultTotalTax = 0;
        String currency = "";

        for (Order order : orderList) {
            if (order.getQty() != null) {
                resultSubtotalQty += order.getQty();
                resultSubtotalPrice += (order.getPrice() * order.getQty());
            }

            if (TextUtils.isEmpty(currency)) {
                currency = order.getCurrency();
            }
        }

        subtotalQty.setText(String.valueOf(resultSubtotalQty));
        subtotalPrice.setText(currency + String.format(Locale.US, "%.2f", resultSubtotalPrice));
        totalTax.setText(currency + String.format(Locale.US, "%.2f", (resultSubtotalPrice + resultTotalTax)));

    }
}