package com.iversecomics.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Storage;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.PreviousPurchase;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.tasks.ServerConfigTask;
import com.iversecomics.client.store.tasks.VerifyPurchaseTask;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.ServerConfigErrorEvent;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.otto.Subscribe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyPurchasesFragment extends ListFragment {

    public final static String PREVIOUS_PURCHASES_AUTOREFRESH = "iversecomics.client.preiouspurchases.autorefresh";

    private static final Logger LOG = LoggerFactory.getLogger(MyPurchasesFragment.class);

    private EditText mSearch;
    private PurchaseListAdapter mAdapter;
    private List<PreviousPurchase> mAllPurchases;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (ViewGroup) inflater.inflate(R.layout.fragment_my_purchases, container, false);

        // filter purchase list for comics with titles matching the search value
        mSearch = (EditText) rootView.findViewById(R.id.search);
        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                // NA
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                // NA
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Fix to - #5454
                if(mAdapter !=null) {
                    mAdapter.getFilter().filter(editable.toString());
                }
            }
        });

        rootView.findViewById(R.id.action_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        rootView.findViewById(R.id.action_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshPurchases();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ServerConfig sc = ServerConfig.getDefault();
        mAllPurchases = sc.getPreviousPurchases();

        if (mAllPurchases != null) {
            loadPreviousPurchases();
        } else {
            //lets try to refresh in case there are some previous purchases
            refreshPurchases();

            //TODO JCM
            //dialog directing the user to go purchase something
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        OttoBusProvider.getInstance().register(this);

        Bundle extras = getActivity().getIntent().getExtras();
        Boolean needToRefresh = false;
        if (extras != null) {
            needToRefresh = extras.getBoolean(PREVIOUS_PURCHASES_AUTOREFRESH, false);
        }

        if (needToRefresh) {
            refreshPurchases();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        OttoBusProvider.getInstance().unregister(this);
    }


    private void loadPreviousPurchases() {
        if (mAllPurchases != null) {
            mAdapter = new PurchaseListAdapter(getActivity(), mAllPurchases);
            setListAdapter(mAdapter);
        }
    }

    // Subscribe to Otto events

    @Subscribe
    public void serverConfigAvailable(ServerConfig config) {
        mAllPurchases = config.getPreviousPurchases();
        loadPreviousPurchases();

    }

    @Subscribe
    public void serverConfigError(ServerConfigErrorEvent errorEvent) {
        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_LONG).show();
    }


    void makeReadItDialog(final MyComic myComic, final View v) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final BitmapDrawable coverIcon = makeIconizedComicCover(v);
        if (coverIcon != null)
            builder.setIcon(coverIcon);
        else
            builder.setIcon(R.drawable.icon);

        builder.setTitle(R.string.read_download_now_title)
                .setMessage(R.string.read_download_now_message)
                .setCancelable(true)
                .setPositiveButton(R.string.read_now, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        final Intent intent = new Intent(getActivity(), ComicReaderPhotoViewActivity.class);
                        intent.putExtra(ComicReaderPhotoViewActivity.EXTRA_COMIC_BUNDLE_NAME, myComic.getComicId());
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.cancel, null);

        builder.create().show();

    }

    BitmapDrawable makeIconizedComicCover(final View v) {
        final View cover = v.findViewById(R.id.cover_layout);
        if (cover != null) {
            cover.setDrawingCacheEnabled(true);
            cover.buildDrawingCache(true);

            final Bitmap bm = cover.getDrawingCache(true);
            return new BitmapDrawable(bm);
        }
        return null;
    }

    public boolean storageAvailable() {
        boolean storageAvailable = true;

        try {
            Storage.assertAvailable();
        } catch (Exception e) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.storage_unavailable_dialog_title)
                    .setIcon(R.drawable.microsd_warn)
                    .setMessage(getString(R.string.storage_unavailable_dialog_message))
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNegativeButton(R.string.settings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_INTERNAL_STORAGE_SETTINGS);
                            startActivity(intent);
                        }
                    });
            builder.create().show();
            storageAvailable = false;
        }
        return storageAvailable;
    }

    @Override
    public void onListItemClick(final ListView l, final View v, final int position, long id) {

        final PreviousPurchase previousPurchase = (PreviousPurchase) (getListAdapter().getItem(position));
        final MyComicsModel model = new MyComicsModel(getActivity());
        final MyComic myComic = model.getComicByBundleName(previousPurchase.getComic_bundle_name());

        //head to the reader if we alread possess this comic
        if (myComic != null) {
            makeReadItDialog(myComic, v);
            return;
        }

        //Ok, we're going to have to download it.  Check for SD storage and bail on fail.
        if (storageAvailable() == false) {
            return;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final BitmapDrawable coverIcon = makeIconizedComicCover(v);
        if (coverIcon != null) {
            builder.setIcon(coverIcon);
        } else {
            builder.setIcon(R.drawable.icon);
        }

        builder.setTitle(R.string.download_restore)
                .setCancelable(true)
                .setPositiveButton(R.string.download_comic, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        try {
                            final ComicStore comicStore = ((IverseApplication) getActivity().getApplication()).getComicStore();
                            final VerifyPurchaseTask task = new VerifyPurchaseTask(comicStore);
                            task.setComicBundleName(previousPurchase.getComic_bundle_name());
                            task.setComicName(previousPurchase.getName());
                            task.setDeviceId(AndroidInfo.getUniqueDeviceID(getActivity()));
                            task.setArtifactType(AndroidInfo.getArtifactForDevice(getActivity()));
                            task.setCoverImageFileName(previousPurchase.getImage_filename());
                            ((IverseApplication) getActivity().getApplication()).getTaskPool().submit(task);

                            Toast.makeText(getActivity(), getResources().getString(R.string.download_requested) + previousPurchase.getName(), Toast.LENGTH_LONG).show();
                        }catch (NullPointerException e){
                            Log.e(MyPurchasesFragment.class.getSimpleName(), "error in Downloading comic", e);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null);

        builder.create().show();
    }


    private void refreshPurchases() {
        final IverseApplication app = IverseApplication.getApplication();
        final Task task = new ServerConfigTask(getActivity(), app.getComicStore());

        app.getTaskPool().submit(task);
    }


    private class PurchaseListAdapter extends BaseAdapter implements Filterable {

        Context context;
        List<PreviousPurchase> filteredPurchases;
        CoverSize coverSize;

        public PurchaseListAdapter(final Context context, final List<PreviousPurchase> previousPurchases) {
            this.context = context;
            this.filteredPurchases = previousPurchases;
            this.coverSize = CoverSize.bestFit(context, R.dimen.menu_item_height);
        }

        private String formatServerDate(String dateString) {
            String newDateString;
            try {
                //Attempt the date conversion from string to date
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                Date convertedDate = dateFormat.parse(dateString);

                //Then format it for the user's locale
                newDateString = DateFormat.getDateInstance().format(convertedDate);
            } catch (Exception e) {
                //Return the original value on fail
                return dateString;
            }

            return newDateString;
        }

        @Override
        public int getCount() {
            return filteredPurchases.size();
        }

        @Override
        public PreviousPurchase getItem(final int position) {
            return filteredPurchases.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                view = inflater.inflate(R.layout.item_previous_purchase, parent, false);
            }

            final PreviousPurchase purchase = getItem(position);
            String purchaseDate = formatServerDate(purchase.getPurchase_date());

            final ImageView thumb = (ImageView) view.findViewById(R.id.thumb);
            ((TextView) view.findViewById(R.id.name)).setText(purchase.getName());
            ((TextView) view.findViewById(R.id.date)).setText(purchaseDate);

            Cursor cursor = null;
            try {

                Comic comic = purchase.toComic();

                if (comic != null) {
                    Uri coverImageUri = coverSize.getServerUri(comic.getImageFileName());
                    ImageLoader.getInstance().displayImage(coverImageUri.toString(), thumb);
                    thumb.setVisibility(View.VISIBLE);
                } else {
                    thumb.setVisibility(View.INVISIBLE);
                }

            } catch (Exception e) {
                Log.e(MyPurchasesFragment.class.getSimpleName(), "error in getView", e);
            } finally {
                DBUtil.close(cursor);
            }
            return view;
        }

        // Filterable
        @Override
        public Filter getFilter() {

            Filter filter = new Filter() {

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                    filteredPurchases = (List<PreviousPurchase>) results.values;
                    notifyDataSetChanged();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    FilterResults results = new FilterResults();
                    List<PreviousPurchase> filteredPurchases = new ArrayList<PreviousPurchase>();

                    // perform your search here using the searchConstraint String.
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mAllPurchases.size(); i++) {
                        PreviousPurchase purchase = mAllPurchases.get(i);
                        if (purchase.getName().toLowerCase().contains(constraint))
                            filteredPurchases.add(purchase);
                    }

                    results.count = filteredPurchases.size();
                    results.values = filteredPurchases;
                    Log.e("VALUES", results.values.toString());

                    return results;
                }
            };

            return filter;
        }

    }

}
