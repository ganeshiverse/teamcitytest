package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.SaveMyCircleResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class SaveMyCircleLoader extends DataAsyncTaskLibLoader<SaveMyCircleResponse> {

    private String requestBody;

    public SaveMyCircleLoader(Context context, String requestBody) {
        super(context);
        this.requestBody = requestBody;
    }

    @Override
    protected SaveMyCircleResponse performLoad() throws Exception {
        return ServerMethods.getInstance().saveMyCircle(requestBody);
    }
}
