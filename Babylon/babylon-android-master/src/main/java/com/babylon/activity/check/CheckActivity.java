package com.babylon.activity.check;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.enums.CheckGender;
import com.babylon.enums.Gender;
import com.babylon.fragment.check.BodyPartFragment;
import com.babylon.fragment.check.GeneralQuestionsFragment;
import com.babylon.fragment.check.OutcomeFragment;
import com.babylon.fragment.check.QuestionsFragment;
import com.babylon.fragment.check.SkinFragment;
import com.babylon.model.Patient;
import com.babylon.service.TriageSyncService;
import com.babylon.sync.TriageSync;
import com.babylon.utils.PreferencesManager;

public class CheckActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    public static final String BODY_PART_FRAGMENT_TAG = "BODY_PART_FRAGMENT_TAG";
    public static final String SKIN_FRAGMENT_TAG = "SKIN_FRAGMENT_TAG";
    public static final String GENERAL_FRAGMENT_TAG = "GENERAL_FRAGMENT_TAG";

    private static final int ON_SUCCESS_SAVING_PROFILE_RESULT = 1;
    private RadioGroup tabGroup;
    private CheckGender gender;
    private View progressLayout;
    private ExportStatusReceiver exportDoneReceiver;
    private boolean isTabGroupHidden;
    private AlertDialog errorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        initializeTopBar();
        setTitle(R.string.check_me_screen_title);

        progressLayout = findViewById(R.id.progressLayout);
        initialProgressSetup();

        tabGroup = (RadioGroup) findViewById(R.id.tabGroup);
        tabGroup.setEnabled(false);
        if (App.getInstance().isConnection()) {
            new PatientGet().execute(this, new RequestListener<Patient>() {
                @Override
                public void onRequestComplete(Response<Patient> response) {
                    Patient data = response.getData();
                    if (data != null) {
                        App.getInstance().setPatient(data);
                    }
                    setUpGender();
                    tabGroup.setOnCheckedChangeListener(CheckActivity.this);
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.bodyTab).performClick();
                        }
                    });

                }
            });
        } else {
            setUpGender();
            tabGroup.setOnCheckedChangeListener(CheckActivity.this);
            findViewById(R.id.bodyTab).performClick();
        }
    }

    private void initialProgressSetup() {
        boolean isExportInProgress = PreferencesManager
                .getInstance()
                .getBoolean(TriageSync.KEY_EXPORT_IN_PROGRESS, false);
        if (isExportInProgress != (progressLayout.getVisibility() == View.VISIBLE)) {
            enableProgress(isExportInProgress);
            if (!App.getInstance().isConnection()) {
                showConnectionError(getString(R.string.check_connection_problem));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        exportDoneReceiver = new ExportStatusReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TriageSync.ACTION_EXPORT_IN_PROGRESS);
        registerReceiver(exportDoneReceiver, intentFilter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initialProgressSetup();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(exportDoneReceiver);
        super.onStop();
    }

    private void enableProgress(boolean enable) {
        if (enable) {
            progressLayout.setVisibility(View.VISIBLE);
        } else {
            progressLayout.setVisibility(View.GONE);
        }
    }

    private void setUpGender() {
        String genderString = App.getInstance().getPatient().getGender();
        boolean genderFilled = Gender.isGenderFilled(genderString);
        if (genderFilled) {
            gender = CheckGender.values()[Gender.get(genderString).ordinal()];
            tabGroup.setEnabled(true);
        } else {
            showProfileActivity();
        }
    }

    @Override
    public void onCheckedChanged(final RadioGroup group, int checkedId) {
        enableRadioButtons(false);// hack for prevent fast fragment switching
        switchFragment(checkedId);
        tabGroup.postDelayed(new Runnable() {
            @Override
            public void run() {
                enableRadioButtons(true);
            }
        }, getResources().getInteger(R.integer.check_tabs_anim_duration));
    }

    private void enableRadioButtons(boolean enabled) {
        for (int i = 0; i < tabGroup.getChildCount(); i++) {
            tabGroup.getChildAt(i).setEnabled(enabled);
        }
    }

    private void switchFragment(int checkedId) {
        switch (checkedId) {
            case R.id.bodyTab:
                startFragment(BodyPartFragment.newInstance(gender),
                        false, BODY_PART_FRAGMENT_TAG,
                        R.id.main_activity_fragment_container_layout);
                break;
            case R.id.skinTab:
                startFragment(SkinFragment.newInstance(), false, SKIN_FRAGMENT_TAG,
                        R.id.main_activity_fragment_container_layout);
                break;
            case R.id.generalTab:
                startFragment(GeneralQuestionsFragment.newInstance(gender, -1, getString(R.string.wellness)), true,
                        GENERAL_FRAGMENT_TAG, R.id.main_activity_fragment_container_layout);
                break;
        }
    }

    /**
     * method calling from check fragments onStart
     */
    public void updateActivity() {
        //TODO should be reworked with Broadcast
        tabGroup.setOnCheckedChangeListener(null);
        final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id
                .main_activity_fragment_container_layout);
        if (fragment instanceof BodyPartFragment) {
            setTitle(R.string.body);
            ((RadioButton) findViewById(R.id.bodyTab)).setChecked(true);
            showTabBar();
        } else {
            setRightImageButtonVisible(false);
        }
        if (fragment instanceof SkinFragment) {
            setTitle(R.string.skin);
            ((RadioButton) findViewById(R.id.skinTab)).setChecked(true);
            showTabBar();
        }
        if (fragment instanceof QuestionsFragment) {
            hideTabBar();
        }
        if (fragment instanceof OutcomeFragment) {
            hideTabBar();
        }
        tabGroup.setOnCheckedChangeListener(this);
    }

    public void hideTabBar() {
        if (!isTabGroupHidden) {
            tabGroup.animate()
                    .translationYBy(tabGroup.getMeasuredHeight())
                    .setDuration(getResources().getInteger(R.integer.check_tabs_anim_duration))
                    .setListener(new SimpleAnimatorListener() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            tabGroup.setVisibility(View.GONE);
                        }
                    });
            isTabGroupHidden = true;
        }
    }

    public void showTabBar() {
        if (isTabGroupHidden) {
            tabGroup.clearAnimation();
            tabGroup.setVisibility(View.VISIBLE);
            int tabGroupHeight = tabGroup.getMeasuredHeight();
            tabGroup.animate()
                    .translationYBy(-tabGroupHeight)
                    .setDuration(getResources().getInteger(R.integer.check_tabs_anim_duration))
                    .setListener(new SimpleAnimatorListener() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            tabGroup.setTranslationY(0);
                            tabGroup.setVisibility(View.VISIBLE);
                        }
                    });
            isTabGroupHidden = false;
        }
    }

    private void showProfileActivity() {
        Intent i = new Intent(this, CheckProfileActivity.class);
        startActivityForResult(i, ON_SUCCESS_SAVING_PROFILE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ON_SUCCESS_SAVING_PROFILE_RESULT:
                    setUpGender();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id
                            .main_activity_fragment_container_layout);
                    if (fragment != null && fragment instanceof BodyPartFragment) {
                        if (gender != null) {
                            ((BodyPartFragment) fragment).updateGender(gender);
                        }
                    }
                    break;
                default:
                    break;
            }
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (progressLayout.getVisibility() == View.VISIBLE) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    class ExportStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isExportInProgress = intent.getBooleanExtra(TriageSync.KEY_EXPORT_IN_PROGRESS, false);
            String error = intent.getStringExtra(TriageSync.KEY_ERROR);

            enableProgress(isExportInProgress);
            if (!TextUtils.isEmpty(error)) {
                showConnectionError(error);
            } else if (errorDialog != null && errorDialog.isShowing()) {
                errorDialog.dismiss();
            }
        }
    }

    private void showConnectionError(final String error) {
        errorDialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(error)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (App.getInstance().isConnection()) {
                            TriageSyncService.startFirstSync();
                        } else {
                            showConnectionError(error);
                        }
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();
    }

    class SimpleAnimatorListener implements Animator.AnimatorListener {
        @Override
        public void onAnimationStart(Animator animation) {
        }

        @Override
        public void onAnimationEnd(Animator animation) {
        }

        @Override
        public void onAnimationCancel(Animator animation) {
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
        }
    }
}