package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by G Ajaykumar on 07/30/2015.
 */
public class FindDoctorResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private ProxyListResponseData proxyListResponseData;

    public ProxyListResponseData getProxyListResponseData() {
        return proxyListResponseData;
    }


    public class ProxyListResponseData implements Serializable {
        @SerializedName("Details")
        private ArrayList<ProxieDetails> proxieDetailsAL;

        @SerializedName("LMD")
        private String lmd;

        public ArrayList<ProxieDetails> getProxieDetailsAL() {
            return proxieDetailsAL;
        }

        public String getLmd() {
            return lmd;
        }
    }

    public class ProxieDetails implements Serializable {

        @SerializedName("ForeName")
        private String foreName;

        @SerializedName("UserCircleMemberEmail_ID")
        private String userCircleMemberEmail_ID;

        @SerializedName("URL")
        private String imageURL;

        public String getForeName() {
            return foreName;
        }

        public String getUserCircleMemberEmail_ID() {
            return userCircleMemberEmail_ID;
        }

        public String getURL() {
            return imageURL;
        }
    }
}