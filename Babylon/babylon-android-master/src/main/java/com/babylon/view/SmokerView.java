package com.babylon.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.enums.SmokeStatus;
import com.babylon.utils.AlertDialogHelper;

public class SmokerView extends LinearLayout {

    public static final String SPACE_SYMBOL = " ";
    public static final String UNDERLINE_SYMBOL = "_";

    private TextView smokerTextView;

    private String newSmokerStatus;

    public SmokerView(Context context) {
        super(context);
        initView(context);
    }

    public SmokerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SmokerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    public void setSmokerStatus(String prevSmokerStatus) {
        if(TextUtils.isEmpty(prevSmokerStatus)) {
            setDefaultSmokerStatus();
        } else {
            newSmokerStatus = getServerFormatSmokeStatus(prevSmokerStatus);
            smokerTextView.setText(getDisplayFormatSmokeStatus(prevSmokerStatus));
        }
    }

    public String getNewServerSmokerStatus() {
        return newSmokerStatus;
    }

    public SmokeStatus getNewSmokerStatus() {
        return SmokeStatus.valueOf(newSmokerStatus.toUpperCase());
    }

    private void initView(Context context) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_smoker, this);

        smokerTextView = (TextView) findViewById(R.id.text_view_smoker);
        smokerTextView.setOnClickListener(onSmokerClickListener);
    }

    private void setDefaultSmokerStatus() {
        final String[] smokeValues = getResources().getStringArray(R.array.smoker_statuses);
        final int defaultIndex = 0;
        final String smokeValue = smokeValues[defaultIndex];

        newSmokerStatus = getServerFormatSmokeStatus(smokeValue);
        smokerTextView.setText(smokeValue);
    }

    private void showSmokerDialog() {
        final String[] smokeValues = getResources().getStringArray(R.array.smoker_statuses);
        int position = 0;
        for (int i = 0; i < smokeValues.length; i++) {
            String smokeStatus = getServerFormatSmokeStatus(smokeValues[i]);
            if (smokeStatus.equalsIgnoreCase(newSmokerStatus)) {
                position = i;
                break;
            }
        }
        AlertDialogHelper.getInstance().showListSingleChoiceAlert(getContext(), position,
                smokeValues, smokeStatusSelectedListener);
    }

    private String getServerFormatSmokeStatus(String smokeStatus) {
        return smokeStatus.replaceAll(SPACE_SYMBOL, UNDERLINE_SYMBOL).toLowerCase();
    }

    private String getDisplayFormatSmokeStatus(String smokeStatus) {
        final String[] smokeValues = getResources().getStringArray(R.array.smoker_statuses);
        for (String smokeValue : smokeValues) {
            if (smokeStatus != null &&
                    smokeValue.equalsIgnoreCase(smokeStatus.replaceAll(UNDERLINE_SYMBOL, SPACE_SYMBOL))) {
                return smokeValue;
            }
        }
        return null;
    }

    private final OnClickListener onSmokerClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            showSmokerDialog();
        }
    };

    private final DialogInterface.OnClickListener smokeStatusSelectedListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            final String[] smokeValues = getResources().getStringArray(R.array.smoker_statuses);
            final int selectPosition = ((AlertDialog) dialog).getListView()
                    .getCheckedItemPosition();
            final String smoker = smokeValues[selectPosition];

            newSmokerStatus = getServerFormatSmokeStatus(smoker);
            smokerTextView.setText(smoker);
        }

    };
}
