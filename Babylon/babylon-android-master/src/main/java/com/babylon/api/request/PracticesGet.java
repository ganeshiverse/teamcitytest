package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Practice;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class PracticesGet extends AbsRequestGet<Practice[]> {

    private String regionId;

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if(wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(String.format(Urls.PRACTICES, regionId));
    }

    @Override
    protected AbsResponseProcessor<Practice[]> getResponseProcessor() {
        return new RubyResponseProcessor<Practice[]>(getOutputType());
    }

    public static PracticesGet newAlliantsPracticesGet(String regionId) {
        PracticesGet practicesGet = new PracticesGet();
        practicesGet.regionId = regionId;
        return practicesGet;
    }

    public static PracticesGet newAlliantsPracticesGet(int regionId) {
        String regionIdStr = String.valueOf(regionId);
        return newAlliantsPracticesGet(regionIdStr);
    }
}
