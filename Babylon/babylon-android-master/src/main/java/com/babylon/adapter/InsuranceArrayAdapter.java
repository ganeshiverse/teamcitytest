package com.babylon.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.model.Insurance;

import java.util.ArrayList;

public class InsuranceArrayAdapter  extends BaseAdapter {

    private ImageView imgIcon;
    private TextView txtTitle;
    private ArrayList<Insurance> spinnerNavItem;
    private Context context;

    public InsuranceArrayAdapter (Context context,
                                  ArrayList<Insurance> spinnerNavItem) {
        this.spinnerNavItem = spinnerNavItem;
        this.context = context;
    }

    @Override
    public int getCount() {
        return spinnerNavItem.size();
    }

    @Override
    public Object getItem(int index) {
        return spinnerNavItem.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.spinner_dropdown_item_insurance, null);
        }


        txtTitle = (TextView) convertView.findViewById(R.id.name_text_view);


        txtTitle.setText(spinnerNavItem.get(position).getName());
        return convertView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.spinner_dropdown_item_region, null);
        }

        txtTitle = (TextView) convertView.findViewById(R.id.name_text_view);

        txtTitle.setText(spinnerNavItem.get(position).getName());
        return convertView;
    }

}