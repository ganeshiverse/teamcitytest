package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.MessageDetailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by grajarapu on 8/20/2015.
 */
public class MessageDeleteLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String messageId;


    public MessageDeleteLoader(Context context, String messageId, String token) {
        super(context);
        this.token = token;
        this.messageId = messageId;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMessageDeleteResponse(messageId, token);
    }
}
