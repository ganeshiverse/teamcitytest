package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.VideoSession;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class VideoSessionGet extends AbsRequestGet<VideoSession> {

  private Integer videoSessionId;

  public VideoSessionGet(Integer videoSessionId) {
      this.videoSessionId = videoSessionId;
  }

  @Override
  protected BasicHeader getAuthHeader() {
      BasicHeader result = null;
      Wsse wsse = SyncUtils.getWsse();
      if (wsse != null) {
          result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
      }
      return result;
  }

  @Override
  protected String getUrl() {
      String url = Urls.getRuby(Urls.VIDEO_SESSIONS);
      return TextUtils.concat(url, "/", String.valueOf(videoSessionId)).toString();
  }

  @Override
  protected AbsResponseProcessor<VideoSession> getResponseProcessor() {
      return new RubyResponseProcessor<VideoSession>(getOutputType());
  }

}
