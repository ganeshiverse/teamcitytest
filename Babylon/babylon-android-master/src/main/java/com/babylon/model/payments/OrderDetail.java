package com.babylon.model.payments;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderDetail implements Parcelable {
    private String name;
    private Double price;

    public OrderDetail(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public boolean isFree() {
        return Double.compare(price, 0) == 0;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeDouble(this.price);
    }

    private OrderDetail(Parcel in) {
        this.name = in.readString();
        this.price = in.readDouble();
    }

    public static final Parcelable.Creator<OrderDetail> CREATOR = new Parcelable.Creator<OrderDetail>() {
        public OrderDetail createFromParcel(Parcel source) {return new OrderDetail(source);}

        public OrderDetail[] newArray(int size) {return new OrderDetail[size];}
    };
}
