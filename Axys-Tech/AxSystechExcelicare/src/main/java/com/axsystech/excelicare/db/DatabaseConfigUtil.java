package com.axsystech.excelicare.db;

import com.axsystech.excelicare.db.models.CircleInvitationSentDbModel;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteConfigUtil;

import java.io.File;

/**
 * This class will be used and executed when there is any new table or existing table schema has been
 * changed according to the requirements. All database table model classes should be included here to
 * prepare configuration file.
 * Date: 24.06.14
 * Time: 14:06
 *
 * @author SomeswarReddy
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    public static final Class<?>[] MODELS = {User.class, SiteDetailsDBModel.class,
            MediaFoldersModel.class, MediaItemDbModel.class, CirclesListDbModel.class, CircleMemberListDbModel.class,
            CirclesMemberDetailsDbModel.class, CircleDetailsDbModel.class,
            CirclesInvitationReceivedDbModel.class, CircleInvitationSentDbModel.class,
            DeviceUsersDbModel.class, SystemPreferenceDBModel.class, ClientUrlDbModel.class,
            ClientUrlAndUserDetailsDbModel.class, MessageListItemDBModel.class};

    public static void main(String[] args) throws Exception {
        writeConfigFile(new File("/Users/someswarreddy/Desktop/Projects/AxSys-Excelicare-Sprint01/AxSystechExcelicare/src/main/res/raw/ormlite_config.txt"), MODELS);
    }
}
