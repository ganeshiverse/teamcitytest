package com.iversecomics.client.refresh;

public abstract class Task implements IRefreshable, Runnable {
    public abstract void execTask();

    @Override
    public boolean isFresh(Freshness freshness) {
        return false; // always run.
    }

    @Override
    public void refresh(Freshness freshness, boolean force) {
        if (!isFresh(freshness) || force) {
            execTask();
        }
    }

    @Override
    public final void run() {
        execTask();
    }
}
