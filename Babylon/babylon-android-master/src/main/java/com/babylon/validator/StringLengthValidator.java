package com.babylon.validator;

public class StringLengthValidator extends BaseValidator {
    private int length;

    public StringLengthValidator(BaseValidator nextValidator, String errorMessage, int length) {
        super(nextValidator, errorMessage);
        this.length = length;
    }

    @Override
    public String validate(String string) {
        return string.length() != length ? errorMessage : null;
    }
}
