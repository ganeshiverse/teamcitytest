package com.iversecomics.client.store.events;

import android.support.v4.app.Fragment;

public interface OnComicClickListener {
    void onComicClick(Fragment fragment, String comicBundleName);
}