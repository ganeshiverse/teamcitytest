package com.babylon.adapter.check;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.model.check.export.Symptom;

import java.util.List;


public class ChildSymptomAdapter extends BaseAdapter {

    private final List<Symptom> symptoms;
    private final LayoutInflater inflater;
    private final int itemLayoutRes;

    public ChildSymptomAdapter(Context context, List<Symptom> symptoms) {
        this(context, symptoms, R.layout.list_item_child_symptom);
    }

    public ChildSymptomAdapter(Context context, List<Symptom> symptoms, int itemLayoutRes) {
        this.symptoms = symptoms;
        this.inflater = LayoutInflater.from(context);
        this.itemLayoutRes = itemLayoutRes;
    }

    @Override
    public int getCount() {
        return symptoms.size();
    }

    @Override
    public Symptom getItem(int position) {
        return symptoms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(itemLayoutRes, null, false);
        }
        Symptom symptom = getItem(position);

        TextView symptomName = (TextView) convertView.findViewById(R.id.symptomText);
        symptomName.setText(symptom.getName());

        return convertView;
    }
}
