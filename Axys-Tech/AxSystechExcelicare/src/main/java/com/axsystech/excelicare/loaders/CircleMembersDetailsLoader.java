package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 8/26/2015.
 */
public class CircleMembersDetailsLoader extends DataAsyncTaskLibLoader<CircleMemberDetailDataResponse> {

    private String token;
    private String userCircleId;
    private String userCircleMemberId;


    public CircleMembersDetailsLoader(Context context, String token, String userCircleId, String userCircleMemberId) {
        super(context);
        this.token = token;
        this.userCircleId = userCircleId;
        this.userCircleMemberId = userCircleMemberId;
    }

    @Override
    protected CircleMemberDetailDataResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCircleMembersDetailsContent(token, userCircleId, userCircleMemberId);
    }
}
