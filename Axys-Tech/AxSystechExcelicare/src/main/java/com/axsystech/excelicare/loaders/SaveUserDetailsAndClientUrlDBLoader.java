package com.axsystech.excelicare.loaders;

import android.content.Context;
import android.text.TextUtils;

import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.data.responses.RegistrationDetailsByUserResponse;
import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SaveUserDetailsAndClientUrlDBLoader extends DataAsyncTaskLibLoader<Boolean> {

    private String TAG = SaveUserDetailsAndClientUrlDBLoader.class.getSimpleName();
    private String loginName;
    private ArrayList<UsersDetailsObject> userDetailsAL;

    public SaveUserDetailsAndClientUrlDBLoader(final Context context, String loginName, ArrayList<UsersDetailsObject> userDetailsAL) {
        super(context, true);
        this.loginName = loginName;
        this.userDetailsAL = userDetailsAL;
    }

    @Override
    protected Boolean performLoad() throws Exception {
        if (userDetailsAL == null)
            throw new SQLException("Unable to cache user details in device database...");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (userDetailsAL != null && userDetailsAL.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        for (final UsersDetailsObject responseModel : userDetailsAL) {
                            final Dao<ClientUrlAndUserDetailsDbModel, Long> userDetailsModelDao = helper.getDao(ClientUrlAndUserDetailsDbModel.class);

                            // Imp: Do not cache the user, site id & client url details if we receive empty EcUserId
                            // If we receive empty EcUserId, it means that particular user is not registered with server using this device
                            if(!TextUtils.isEmpty(responseModel.getEcUserID())) {
                                ClientUrlAndUserDetailsDbModel clientUrlDbModel = ConverterResponseToDbModel.getUserDetailsDbModel(loginName, responseModel);

                                if (clientUrlDbModel != null) {
                                    userDetailsModelDao.createOrUpdate(clientUrlDbModel);
                                    Trace.d(TAG, "Saved user & client url Details in DB...");
                                }
                            }
                        }

                        return true;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return true;
    }
}
