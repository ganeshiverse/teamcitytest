package com.babylon.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import com.babylon.R;
import com.babylon.adapter.GuidePagerAdapter;
import com.viewpagerindicator.LinePageIndicator;

public class MonitorMeGuideActivity extends BaseActivity {
    public static final int LINE_WIDTH = 30;
    public static final int STROKE_WIDTH = 4;

    private GuidePagerAdapter adapter;
    private ViewPager pager;
    private LinePageIndicator indicator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_me_guide);

        initViews();
    }

    private void initViews() {
        adapter = new GuidePagerAdapter(getSupportFragmentManager());

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        indicator = (LinePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setSelectedColor(getResources().getColor(R.color.progress_default_progress_color));
        indicator.setUnselectedColor(getResources().getColor(android.R.color.white));
        indicator.setStrokeWidth(STROKE_WIDTH * density);
        indicator.setLineWidth(LINE_WIDTH * density);

        indicator.setOnPageChangeListener(onPageChangeListener);
    }

    public void onSkipClickListener(View view) {
        finish();
    }

    private final ViewPager.SimpleOnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener
            () {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);

            final ViewGroup viewGroupIndicator = (ViewGroup) findViewById(R.id.layout_indicator);
            if (position + 1 == GuidePagerAdapter.PAGE_NUMBER) {
                viewGroupIndicator.setVisibility(View.GONE);
            } else {
                if (viewGroupIndicator.getVisibility() != View.VISIBLE) {
                    viewGroupIndicator.setVisibility(View.VISIBLE);
                }
            }
        }
    };
}
