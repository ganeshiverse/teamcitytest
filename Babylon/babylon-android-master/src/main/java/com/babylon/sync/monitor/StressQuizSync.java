package com.babylon.sync.monitor;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.os.Bundle;

import com.babylon.api.request.StressQuizGet;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.model.StressQuestion;
import com.babylon.sync.AbsSyncStrategy;

import java.util.ArrayList;
import java.util.List;

public class StressQuizSync extends AbsSyncStrategy {
    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        //no implementation
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        final StressQuizGet stressQuizGet = new StressQuizGet();
        final Response<ResponseList<StressQuestion>> response = stressQuizGet.execute();

        if (response != null && response.isSuccess()) {
            List<StressQuestion> questions = response.getData().getList();

            final ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

            for (final StressQuestion question : questions) {
                final ContentProviderOperation operation = DatabaseOperationUtils.getInstance()
                        .getQuestionInsertOperation
                                (question);

                operations.add(operation);
            }

            DatabaseOperationUtils.applyBatch(contentResolver, operations);
        }
    }
}
