package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by G Ajaykumar on 08/11/2015.
 */
public class CityOrLocationDetailsResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private ArrayList<LocationDetails> locationListResponses;

    public ArrayList<LocationDetails> getLocationListResponses() {
        return locationListResponses;
    }

    public class LocationDetails implements Serializable {

        @SerializedName("City")
        private String city;

        @SerializedName("ID")
        private String id;

        @SerializedName("LOCID")
        private String locid;

        @SerializedName("LOCIDNUMBER")
        private String locidnum;

        @SerializedName("LOCaddr1")
        private String locadd;

        @SerializedName("Locality")
        private String locality;

        public String getCity() {
            return city;
        }

        public String getId() {
            return id;
        }

        public String getLocid() {
            return locid;
        }

        public String getLocidnum() {
            return locidnum;
        }

        public String getLocadd() {
            return locadd;
        }

        public String getLocality() {
            return locality;
        }
    }
}