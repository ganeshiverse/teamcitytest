package com.babylon.adapter.check;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.fragment.check.QuestionsFragment;
import com.babylon.model.check.export.Symptom;
import com.babylon.model.check.ViewState;

import java.util.List;


public class QuestionsAdapter extends BaseAdapter {
    private final int VIEW_TYPE_COUNT = 2;

    private final int SELECTOR_VIEW = 0;
    private final int SWITCHER_VIEW = 1;


    private final List<QuestionsFragment.SymptomViewItem> fields;
    private final LayoutInflater inflater;
    private final Context context;

    public QuestionsAdapter(Context context, List<QuestionsFragment.SymptomViewItem> fields) {
        this.fields = fields;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return fields.size();
    }

    @Override
    public QuestionsFragment.SymptomViewItem getItem(int position) {
        return fields.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        Holder holder;
        if (convertView == null) {
            switch (getItemViewType(position)) {
                case SWITCHER_VIEW:
                    view = inflater.inflate(R.layout.triage_symptom_switcher_view, null, false);
                    holder = new SwitcherHolder();
                    holder.saveUIReferences(view);
                    Resources resources = context.getResources();
                    break;
                default:
                case SELECTOR_VIEW:
                    view = inflater.inflate(R.layout.triage_symptom_view, null, false);
                    holder = new SelectorHolder();
                    holder.saveUIReferences(view);
                    break;
            }
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (Holder) view.getTag();
        }

        final QuestionsFragment.SymptomViewItem symptom = getItem(position);

        holder.setTitle(symptom.getName());
        int itemViewType = getItemViewType(position);
        if (itemViewType == SWITCHER_VIEW) {
            holder.getSwitcher().setOnCheckedChangeListener(null);
            if (ViewState.YES.equals(symptom.getAnswerState())) {
                symptom.setAnswerState(ViewState.YES);
                holder.setState(true);
            } else {
                symptom.setAnswerState(ViewState.NO);
                holder.setState(false);
            }
            holder.getSwitcher().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        symptom.setAnswerState(ViewState.YES);
                    } else {
                        symptom.setAnswerState(ViewState.NO);
                    }
                }
            });
        } else {
            ViewState answerState = symptom.getAnswerState();
            Symptom selectedSymptom = symptom.getSelectedSymptom();
            if (answerState != ViewState.UNSELECTED
                    && selectedSymptom != null) {
                holder.setDescription(selectedSymptom.getName());
            } else {
                holder.setDescription(null);
            }
        }
        return view;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        int type = SELECTOR_VIEW;
        QuestionsFragment.SymptomViewItem stepItem = getItem(position);
        if (stepItem.getChildrenCount() == 0) {
            type = SWITCHER_VIEW;
        }
        return type;
    }

    private class SwitcherHolder implements Holder {
        TextView name;
        Switch answerSwitcher;

        public void saveUIReferences(View view) {
            name = (TextView) view.findViewById(R.id.name);
            answerSwitcher = (Switch) view.findViewById(R.id.answer_switcher);
        }

        @Override
        public Switch getSwitcher() {
            return answerSwitcher;
        }

        @Override
        public void setTitle(String title) {
            name.setText(title);
        }

        @Override
        public void setDescription(String description) {

        }

        @Override
        public void setState(boolean selected) {
            answerSwitcher.setChecked(selected);
        }
    }

    private class SelectorHolder implements Holder {
        private TextView name;
        private TextView answer;

        @Override
        public void saveUIReferences(View view) {
            name = (TextView) view.findViewById(R.id.name);
            answer = (TextView) view.findViewById(R.id.answer);
        }

        @Override
        public void setTitle(String title) {
            name.setText(title);
        }

        @Override
        public void setDescription(String description) {
            answer.setText(description);
        }

        @Override
        public void setState(boolean selected) {

        }

        @Override
        public Switch getSwitcher() {
            return null;
        }
    }

    private interface Holder {
        public void saveUIReferences(View view);

        public void setTitle(String title);

        public void setDescription(String description);

        public void setState(boolean selected);

        public Switch getSwitcher();
    }
}
