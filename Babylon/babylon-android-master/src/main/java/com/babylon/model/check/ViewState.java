package com.babylon.model.check;

import com.babylon.R;

public enum ViewState {
    YES(1, R.string.view_state_yes),
    NO(0, R.string.view_state_no),
    UNSELECTED(-1, R.string.view_state_unselected);

    private final int value;
    private final int title;
    private ViewState(int value, int title) {
        this.value = value;
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public int getTitle() {
        return title;
    }
}
