package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.model.FacebookToken;
import com.babylon.model.Patient;
import com.babylon.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHeader;

public class FacebookPost extends AbsRequestPost<FacebookToken, Patient> {

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.FACEBOOK_FIELDS);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        return null;
    }

    @Override
    protected AbsResponseProcessor<Patient> getResponseProcessor() {
        return new FacebookResponseProcessor<>(getOutputType());
    }
    @Override
    protected Response<Patient> processResponse(HttpResponse httpResponse) {
        String token = getToken(httpResponse);
        Response<Patient> result = super.processResponse(httpResponse);
        if (result.isSuccess()) {
            result.getData().setToken(token);
        }
        return result;
    }

    private String getToken(HttpResponse httpResponse) {
        String token = null;
        if (httpResponse.containsHeader(Constants.Header_RequestKeys.X_AUTH_TOKEN)) {
            token = httpResponse.getLastHeader(Constants.Header_RequestKeys.X_AUTH_TOKEN).getValue();
        }
        return token;
    }

}
