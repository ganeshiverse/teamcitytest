package com.babylon.controllers;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.babylon.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BirthdayViewController extends DateViewController {
    public static final String BIRTH_DATE_FORMAT = "MMM dd yyyy";
    private static final int MIN_AGE = 16;
    private static final int DEFAULT_AGE = 0;
    private int minAge = MIN_AGE;

    public BirthdayViewController() {}

    public BirthdayViewController(Activity activity, TextView birthDateTextView,
                                  OnDateSetListener onDateSet, Date currentBirthday) {
        super(activity, birthDateTextView, onDateSet, currentBirthday);

        if (currentBirthday != null)
            setDate(currentBirthday);
    }

    public BirthdayViewController(Activity activity, TextView birthDateTextView,
                                  OnDateSetListener onDateSet) {
        this(activity, birthDateTextView, onDateSet, null);
    }

    public String getFormattedDay(Date date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BIRTH_DATE_FORMAT,
                Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    @Override
    public String getFormatDate() {
        return BIRTH_DATE_FORMAT;
    }

    public long getMinDate() {
        Calendar c = Calendar.getInstance();
        c.roll(Calendar.YEAR, -minAge);
        return c.getTimeInMillis();
    }

    public Date getDefaultDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.YEAR, -DEFAULT_AGE);
        return calendar.getTime();
    }

    public void setDate(Date date) {
        Date currentDate = date == null ? getDefaultDate() : date;
        setCurrentDate(currentDate);
        dateTextView.setText(getFormattedDay(currentDate));
    }

    public boolean validateBirthday(Date date, EditText errorTextView) {
        boolean result = true;

        if (isValid(date)) {
            if (!TextUtils.isEmpty(errorTextView.getError())) {
                errorTextView.setError(null);
            }
        } else {
            Drawable drawable = context.getResources().getDrawable(R.drawable.ic_reqfield_error);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            errorTextView.setError(context.getString(R.string.error_age, minAge), drawable);

            result = false;
        }

        return result;
    }

    public boolean isValid(Date date) {
        boolean result = true;

        long minDate = getMinDate();

        if (date.getTime() > minDate) {
            result = false;
        }

        return result;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }
}