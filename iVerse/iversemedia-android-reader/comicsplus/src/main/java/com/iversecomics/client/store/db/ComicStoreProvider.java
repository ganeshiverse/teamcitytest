package com.iversecomics.client.store.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.iversecomics.client.util.DBUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicStoreProvider extends ContentProvider {
    private static final Logger LOG = LoggerFactory.getLogger(ComicStoreProvider.class);

    // URI Publisher Requests
    private static final int ALL_PUBLISHERS = 10;
    private static final int SINGLE_PUBLISHER = 11;
    private static final int SINGLE_PUBLISHERID = 12;

    // URI Publisher Requests
    private static final int ALL_GROUPS = 20;
    private static final int SINGLE_GROUP = 21;
    private static final int SINGLE_GROUPID = 22;

    // URI Genre Requests
    private static final int ALL_GENRES = 30;
    private static final int SINGLE_GENRE = 31;
    private static final int SINGLE_GENREID = 32;

    // URI Comic Requests
    private static final int ALL_COMICS = 40;
    private static final int SINGLE_COMIC = 41;
    private static final int SINGLE_COMICID = 42;

    // URI Featured Requests
    private static final int ALL_FEATURED = 50;
    private static final int SINGLE_FEATURED = 51;

    // URI ServerConfig Requests
    private static final int SERVER_CONFIG = 60;

    // URI Freshness Requests
    private static final int FRESHNESS = 70;

    // URI Ordering Requests
    private static final int ORDERING = 80;

    // URI Featured Slots
    private static final int ALL_FEATUREDSLOTS = 90;
    private static final int SINGLE_FEATUREDSLOT = 91;

    // URI Promotions
    private static final int ALL_PROMOTIONS = 100;
    private static final int SINGLE_PROMOTION = 101;

    private static final UriMatcher URIMATCHER;

    static {
        URIMATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "publishers", ALL_PUBLISHERS);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "publishers/byid/*", SINGLE_PUBLISHERID);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "publishers/#", SINGLE_PUBLISHER);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "groups", ALL_GROUPS);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "groups/byid/*", SINGLE_GROUPID);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "groups/#", SINGLE_GROUP);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "genres", ALL_GENRES);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "genres/byid/*", SINGLE_GENREID);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "genres/#", SINGLE_GENRE);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "comics", ALL_COMICS);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "comics/byid/*", SINGLE_COMICID);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "comics/#", SINGLE_COMIC);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "featured", ALL_FEATURED);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "featured/#", SINGLE_FEATURED);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "config", SERVER_CONFIG);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "freshness", FRESHNESS);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "ordering", ORDERING);

        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "featuredslots", ALL_FEATUREDSLOTS);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "featuredslots/byid/*", SINGLE_FEATUREDSLOT);

        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "promotions", ALL_PROMOTIONS);
        URIMATCHER.addURI(ComicStoreDB.AUTHORITY, "promotions/byid/*", SINGLE_PROMOTION);
    }

    private SQLiteDatabase db;

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table;
        String where = selection;
        String whereArgs[] = selectionArgs;

        switch (URIMATCHER.match(uri)) {
            case ALL_COMICS:
                table = ComicsTable.TABLE;
                break;
            case SINGLE_COMIC:
                table = ComicsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, ComicsTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_COMICID:
                table = ComicsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, ComicsTable.COMICID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case ALL_GENRES:
                table = GenresTable.TABLE;
                break;
            case SINGLE_GENRE:
                table = GenresTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, GenresTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_GENREID:
                table = GenresTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, GenresTable.GENREID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case ALL_PUBLISHERS:
                table = PublishersTable.TABLE;
                break;
            case SINGLE_PUBLISHER:
                table = PublishersTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, PublishersTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_PUBLISHERID:
                table = PublishersTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, PublishersTable.PUBLISHERID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case ALL_GROUPS:
                table = GroupsTable.TABLE;
                break;
            case SINGLE_GROUP:
                table = GroupsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, GroupsTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_GROUPID:
                table = GroupsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, GroupsTable.GROUPID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SERVER_CONFIG:
                table = ServerConfigTable.TABLE;
                break;
            case FRESHNESS:
                table = FreshnessTable.TABLE;
                break;
            case ORDERING:
                table = OrderingTable.TABLE;
                uri = ComicsTable.CONTENT_URI; // so that comics lists get updated automatically
                break;

            case ALL_FEATUREDSLOTS:
                table = FeaturedSlotTable.TABLE;
                break;
            case SINGLE_FEATUREDSLOT:
                table = FeaturedSlotTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, FeaturedSlotTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;

            case ALL_PROMOTIONS:
                table = PromotionTable.TABLE;
                break;
            case SINGLE_PROMOTION:
                table = PromotionTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, PromotionTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;

            default:
                throw new IllegalArgumentException("Unsupported content URI: " + uri);
        }

        int count = db.delete(table, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        LOG.debug("Deleted %d entries from %s", count, table);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (URIMATCHER.match(uri)) {
            case ALL_COMICS:
                return ComicsTable.CONTENT_TYPE;
            case SINGLE_COMIC:
            case SINGLE_COMICID:
                return ComicsTable.CONTENT_ITEM_TYPE;
            case ALL_GENRES:
                return GenresTable.CONTENT_TYPE;
            case SINGLE_GENRE:
            case SINGLE_GENREID:
                return GenresTable.CONTENT_ITEM_TYPE;
            case ALL_PUBLISHERS:
                return PublishersTable.CONTENT_TYPE;
            case SINGLE_PUBLISHER:
            case SINGLE_PUBLISHERID:
                return PublishersTable.CONTENT_ITEM_TYPE;
            case ALL_GROUPS:
                return GroupsTable.CONTENT_TYPE;
            case SINGLE_GROUP:
            case SINGLE_GROUPID:
                return GroupsTable.CONTENT_ITEM_TYPE;
            case SERVER_CONFIG:
                return GroupsTable.CONTENT_TYPE;
            case FRESHNESS:
                return FreshnessTable.CONTENT_TYPE;
            case ORDERING:
                return OrderingTable.CONTENT_TYPE;

            case ALL_FEATUREDSLOTS:
                return FeaturedSlotTable.CONTENT_TYPE;
            case SINGLE_FEATUREDSLOT:
                return FeaturedSlotTable.CONTENT_ITEM_TYPE;

            case ALL_PROMOTIONS:
                return PromotionTable.CONTENT_TYPE;
            case SINGLE_PROMOTION:
                return PromotionTable.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unsupported content URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowid = -1;
        Uri baseUri = null;

        switch (URIMATCHER.match(uri)) {
            case ALL_COMICS:
            case SINGLE_COMIC:
            case SINGLE_COMICID:
                rowid = db.insert(ComicsTable.TABLE, "", values);
                baseUri = ComicsTable.CONTENT_URI;
                break;
            case ALL_GENRES:
            case SINGLE_GENRE:
            case SINGLE_GENREID:
                rowid = db.insert(GenresTable.TABLE, "", values);
                baseUri = GenresTable.CONTENT_URI;
                break;
            case ALL_PUBLISHERS:
            case SINGLE_PUBLISHER:
            case SINGLE_PUBLISHERID:
                rowid = db.insert(PublishersTable.TABLE, "", values);
                baseUri = PublishersTable.CONTENT_URI;
                break;
            case ALL_GROUPS:
            case SINGLE_GROUP:
            case SINGLE_GROUPID:
                rowid = db.insert(GroupsTable.TABLE, "", values);
                baseUri = GroupsTable.CONTENT_URI;
                break;
            case SERVER_CONFIG:
                rowid = db.insert(ServerConfigTable.TABLE, "", values);
                baseUri = ServerConfigTable.CONTENT_URI;
                break;
            case FRESHNESS:
                rowid = db.insert(FreshnessTable.TABLE, "", values);
                baseUri = FreshnessTable.CONTENT_URI;
                break;

            case ALL_FEATUREDSLOTS:
            case SINGLE_FEATUREDSLOT:
                rowid = db.insert(FeaturedSlotTable.TABLE, "", values);
                baseUri = FeaturedSlotTable.CONTENT_URI;
                break;

            case ALL_PROMOTIONS:
            case SINGLE_PROMOTION:
                rowid = db.insert(PromotionTable.TABLE, "", values);
                baseUri = PromotionTable.CONTENT_URI;
                break;

            default:
                throw new IllegalArgumentException("Unsupported content URI: " + uri);
        }

        if (rowid > 0) {
            Uri nuri = ContentUris.withAppendedId(baseUri, rowid);
            getContext().getContentResolver().notifyChange(nuri, null);
            return nuri;
        }

        throw new SQLException("Failed to insert row into + " + uri);
    }

    @Override
    public boolean onCreate() {
        db = ComicStoreDatabaseHelper.getInstance(getContext()).getWritableDatabase();
        return (db != null);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String tables = null;
        String columns[] = projection;
        String where = selection;
        String whereArgs[] = selectionArgs;
        String groupBy = null;
        String having = null;
        String orderBy = sortOrder;

        switch (URIMATCHER.match(uri)) {
            case ALL_COMICS:
                tables = ComicsTable.TABLE;
                if (DBUtil.hasSelectionFor(selection, OrderingTable.TABLE)) {
                    tables += ", " + OrderingTable.TABLE;
                }
                if (columns == null) {
                    columns = ComicsTable.PROJECTION_FULL;
                }
                orderBy = DBUtil.toOrderBy(sortOrder, ComicsTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_COMIC:
                tables = ComicsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, "%s.%s = ?", ComicsTable.TABLE, ComicsTable._ID);
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, ComicsTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_COMICID:
                tables = ComicsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, "%s.%s = ?", ComicsTable.TABLE, ComicsTable.COMICID);
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, ComicsTable.DEFAULT_SORT_ORDER);
                break;
            case ALL_GENRES:
                tables = GenresTable.TABLE;
                orderBy = DBUtil.toOrderBy(sortOrder, GenresTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_GENRE:
                tables = GenresTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GenresTable._ID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, GenresTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_GENREID:
                tables = GenresTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GenresTable.GENREID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, GenresTable.DEFAULT_SORT_ORDER);
                break;
            case ALL_PUBLISHERS:
                tables = PublishersTable.TABLE;
                orderBy = DBUtil.toOrderBy(sortOrder, PublishersTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_PUBLISHER:
                tables = PublishersTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (PublishersTable._ID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, PublishersTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_PUBLISHERID:
                tables = PublishersTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (PublishersTable.PUBLISHERID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, PublishersTable.DEFAULT_SORT_ORDER);
                break;
            case ALL_GROUPS:
                tables = GroupsTable.TABLE;
                orderBy = DBUtil.toOrderBy(sortOrder, GroupsTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_GROUP:
                tables = GroupsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GroupsTable._ID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, GroupsTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_GROUPID:
                tables = GroupsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GroupsTable.GROUPID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, GroupsTable.DEFAULT_SORT_ORDER);
                break;
            case SERVER_CONFIG:
                tables = ServerConfigTable.TABLE;
                orderBy = DBUtil.toOrderBy(sortOrder, ServerConfigTable.DEFAULT_SORT_ORDER);
                break;
            case FRESHNESS:
                tables = FreshnessTable.TABLE;
                orderBy = DBUtil.toOrderBy(sortOrder, FreshnessTable.DEFAULT_SORT_ORDER);
                break;


            case ALL_FEATUREDSLOTS:
                tables = FeaturedSlotTable.TABLE;
                if (columns == null) {
                    columns = FeaturedSlotTable.PROJECTION_FULL;
                }
                orderBy = DBUtil.toOrderBy(sortOrder, FeaturedSlotTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_FEATUREDSLOT:
                tables = FeaturedSlotTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, "%s.%s = ?", FeaturedSlotTable.TABLE, FeaturedSlotTable._ID);
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, FeaturedSlotTable.DEFAULT_SORT_ORDER);
                break;


            case ALL_PROMOTIONS:
                tables = PromotionTable.TABLE;
                if (columns == null) {
                    columns = PromotionTable.FULL_PROJECTION;
                }
                orderBy = DBUtil.toOrderBy(sortOrder, PromotionTable.DEFAULT_SORT_ORDER);
                break;
            case SINGLE_PROMOTION:
                tables = PromotionTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, "%s.%s = ?", PromotionTable.TABLE, PromotionTable._ID);
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                orderBy = DBUtil.toOrderBy(sortOrder, PromotionTable.DEFAULT_SORT_ORDER);
                break;

            default:
                throw new IllegalArgumentException("Unsupported content URI: " + uri);
        }

        // Apply the Query
        // DBUtil.debugQuery(uri, tables, columns, where, whereArgs, orderBy);
        Cursor c = null;
        try {
           c = db.query(tables, columns, where, whereArgs, groupBy, having, orderBy);
        // Register uri for notifications
        c.setNotificationUri(getContext().getContentResolver(), uri);
        }catch (RuntimeException e){

        }
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String table = null;
        String where = selection;
        String whereArgs[] = selectionArgs;

        switch (URIMATCHER.match(uri)) {
            case ALL_COMICS:
                table = ComicsTable.TABLE;
                break;
            case SINGLE_COMIC:
                table = ComicsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, ComicsTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_COMICID:
                table = ComicsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, ComicsTable.COMICID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case ALL_GENRES:
                table = GenresTable.TABLE;
                break;
            case SINGLE_GENRE:
                table = GenresTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GenresTable._ID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_GENREID:
                table = GenresTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GenresTable.GENREID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case ALL_PUBLISHERS:
                table = PublishersTable.TABLE;
                break;
            case SINGLE_PUBLISHER:
                table = PublishersTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (PublishersTable._ID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_PUBLISHERID:
                table = PublishersTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (PublishersTable.PUBLISHERID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case ALL_GROUPS:
                table = GroupsTable.TABLE;
                break;
            case SINGLE_GROUP:
                table = GroupsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GroupsTable._ID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SINGLE_GROUPID:
                table = GroupsTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, (GroupsTable.PUBLISHERID + " = ?"));
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            case SERVER_CONFIG:
                table = ServerConfigTable.TABLE;
                break;
            case FRESHNESS:
                table = FreshnessTable.TABLE;
                break;

            case ALL_FEATUREDSLOTS:
                table = FeaturedSlotTable.TABLE;
                break;
            case SINGLE_FEATUREDSLOT:
                table = FeaturedSlotTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, FeaturedSlotTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;

            case ALL_PROMOTIONS:
                table = PromotionTable.TABLE;
                break;
            case SINGLE_PROMOTION:
                table = PromotionTable.TABLE;
                where = DBUtil.toPrependedWhere(selection, PromotionTable._ID + " = ?");
                whereArgs = DBUtil.toPrependedWhereArgs(selectionArgs, uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unsupported content URI: " + uri);
        }

        int count = db.update(table, values, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
