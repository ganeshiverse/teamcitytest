package com.babylon.model.payments;

import com.babylon.enums.SubscriptionStatus;
import com.babylon.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Subscription extends BaseModel {
    @SkipFieldInContentValues
    private static final long serialVersionUID = 915L;

    private Double price;

    private Plan plan;
    private String state;

    private Integer id;
    private String title;
    private String subtitle;
    @SerializedName("patient_id")
    private Integer patientId;
    @SerializedName("next_billing_date")
    private Date nextBillingDate;
    @SerializedName("credit_card")
    private PaymentCard creditCard;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getPatientId() {
        return patientId;
    }


    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public boolean isFree() {
        return Double.compare(price, 0) == 0;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isPending() {
        return state.equalsIgnoreCase(SubscriptionStatus.PENDING);
    }

    public Date getNextBillingDate() {
        return nextBillingDate;
    }

    public void setNextBillingDate(Date nextBillingDate) {
        this.nextBillingDate = nextBillingDate;
    }

    public PaymentCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(PaymentCard creditCard) {
        this.creditCard = creditCard;
    }
}
