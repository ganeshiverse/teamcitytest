package com.babylon.activity;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.PushBroadcastReceiver;
import com.babylon.R;
import com.babylon.api.request.FacebookSessionsPost;
import com.babylon.api.request.RegionsGet;
import com.babylon.api.request.SessionsPost;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.database.DatabaseHelper;
import com.babylon.model.FacebookToken;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.model.Wsse;
import com.babylon.social.FacebookManager;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.sync.auth.BabylonAuthenticator;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.UIUtils;
import com.babylon.view.TickButtonWithProgress;
import com.babylon.view.TopBarInterface;
import com.babylonpartners.babylon.HomePageActivity;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

public class SignInActivity extends BaseActivity implements View.OnClickListener,
        TextWatcher, FacebookManager.OnFbLoginStatusListener {



    public enum SignInType {
        EMAIL,
        FACEBOOK;
    }
    private AccountAuthenticatorResponse mAccountAuthenticatorResponse = null;

    private Bundle mResultBundle = null;
    private EditText emailEdt;

    private EditText passwordEdt;
    private AccountManager mAccountManager;

    private TickButtonWithProgress submitBtn;

    private View resetPasswordTxt;

    private Intent authIntent;

    /**
     * Set the result that is to be sent as the result of the request that caused this
     * Activity to be launched. If result is null or this method is never called then
     * the request will be canceled.
     *
     * @param result this is returned as the result of the AbstractAccountAuthenticator request
     */
    public final void setAccountAuthenticatorResult(Bundle result) {
        mResultBundle = result;
    }

    /**
     * Sends the result or a Constants.ERROR_CODE_CANCELED error if a result isn't present.
     */
    public void finish() {
        if (mAccountAuthenticatorResponse != null) {
            // send the result bundle back if set, otherwise send an error.
            if (mResultBundle != null) {
                mAccountAuthenticatorResponse.onResult(mResultBundle);
            } else {
                mAccountAuthenticatorResponse.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled");
            }
            mAccountAuthenticatorResponse = null;
        }
        super.finish();
    }

    /**
     * Retreives the AccountAuthenticatorResponse from either the intent of the icicle, if the
     * icicle is non-zero.
     *
     * @param savedInstanceState the save instance data of this Activity, may be null
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccountAuthenticatorResponse =
                getIntent().getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);

        if (mAccountAuthenticatorResponse != null) {
            mAccountAuthenticatorResponse.onRequestContinued();
        }

        setContentView(R.layout.activity_sign_in);
        mAccountManager = AccountManager.get(this);

        getActionBar().show();
        setupActionBar(ActionBarType.SIGNIN);

        /*setTitle(getString(R.string.sign_in));
        initializeTopBar();*/

        initViews();
        fillInEmail();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        boolean isFieldsFilled = !TextUtils.isEmpty(s.toString())
                && !TextUtils.isEmpty(emailEdt.getText().toString())
                && !TextUtils.isEmpty(passwordEdt.getText().toString());
        submitBtn.setEnabled(isFieldsFilled);
    }



    @Override
    public int getColorTheme() {
        return TopBarInterface.PURPLE_THEME;
    }

    public void onBackPressed(View view) {
        finish();
    }

    private void initViews() {
        emailEdt = (EditText) findViewById(R.id.emailEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);

        passwordEdt.setOnEditorActionListener(onPasswordEditorActionListener);

        resetPasswordTxt = findViewById(R.id.resetPasswordTxt);
        //LoginButton facebookButton = (LoginButton) findViewById(R.id.facebookBtn);
        submitBtn = (TickButtonWithProgress) findViewById(R.id.signInBtn);

        submitBtn.setOnClickListener(this);
        resetPasswordTxt.setOnClickListener(this);
        //facebookButton.setOnClickListener(this);

        //******ToCheck-Added by prathibha*******************

       /* emailEdt.addTextChangedListener(this);
        passwordEdt.addTextChangedListener(this);

        if(TextUtils.isEmpty(emailEdt.getText().toString())
                && TextUtils.isEmpty(passwordEdt.getText().toString())) {
            submitBtn.setEnabled(false);
        }*/
    }

    private void fillInEmail() {
        final String email = App.getInstance().getPatient().getEmail();
        if (!TextUtils.isEmpty(email)) {
            emailEdt.setText(email);
            passwordEdt.requestFocus();
        }
    }

    private void onLoggedInFinish() {

        if(PreferencesManager.getInstance().getRegions() == null) {
            loadRegions();
        } else {
            onAuthFinish();
        }
    }

    private void loadRegions() {
        RegionsGet regionsGet = new RegionsGet();
        regionsGet.execute(this, new RequestListener<Region[]>() {
            @Override
            public void onRequestComplete(Response<Region[]> response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    if (response.getData() != null) {
                        onLoginFinish(response);
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.some_error, Toast.LENGTH_LONG).show();
                    }
                }
            }

        });
    }

    private void onLoginFinish(Response<Region[]> response) {
        Region[] regions = response.getData();
        PreferencesManager.getInstance().setRegions(regions);
        onAuthFinish();
    }

    private void onAuthFinish()
    {
        String accountName = authIntent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String accountPassword = authIntent.getStringExtra(AccountManager.KEY_PASSWORD);

        final Account account = SyncUtils.createSyncAccount(this, accountName, accountPassword);

        if (getIntent().getBooleanExtra(AccountManager.LOGIN_ACCOUNTS_CHANGED_ACTION, false))
        {
            mAccountManager.setPassword(account, accountPassword);
        }
        else
        {
            String authToken = authIntent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword, null);
            mAccountManager.setAuthToken(account, BabylonAuthenticator.AUTH_TOKEN_TYPE,
                    App.getInstance().getPatient().getToken());
            SyncUtils.setWsse(new Wsse(App.getInstance().getPatient().getId(), authToken));
        }

        setAccountAuthenticatorResult(authIntent.getExtras());
        submitBtn.setEnabled(true);

        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PATIENT, SyncAdapter.SYNC_PUSH);

        Bundle arguments = authIntent.getExtras();
        boolean isLaunchFromNotification = isLaunchFromNotification(arguments);

        if (isLaunchFromNotification)
        {
            String parseData = arguments.getString(PushBroadcastReceiver.KEY_PARSE_DATA);
            Intent i = new Intent(this, HomePageActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, parseData);
            i.putExtra(Keys.IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, true);
            i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
            startActivity(i);
        }
        else if (arguments != null && arguments.getBoolean(Keys.SHOULD_START_HOME_ACTIVITY)) {
            arguments.putString(PushBroadcastReceiver.KEY_PARSE_DATA, null);
            UIUtils.hideSoftKeyboard(this, emailEdt);
            Intent baseIntent = new Intent(this, HomePageActivity.class);
            baseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TOP);
            baseIntent.putExtras(arguments);
            startActivity(baseIntent);
        }
        else {
            Intent i = new Intent(this, HomePageActivity.class);
            i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(Keys.IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, true);
            startActivity(i);
        }

        finish();
    }

    private boolean isLaunchFromNotification(Bundle arguments) {
        boolean isLaunchFromNotification = false;
        if (arguments != null) {
            PushBroadcastReceiver.PushAction pushAction = (PushBroadcastReceiver.PushAction) arguments
                    .getSerializable(Keys.NOTIFICATION_DATA);
            isLaunchFromNotification = pushAction != null;
        }
        return isLaunchFromNotification;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.resetPasswordTxt:
                Intent intent = new Intent(this, ResetPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.signInBtn:
                if (App.getInstance().isConnection()) {
                    signIn();
                } else {
                    App.getInstance().showWaitingMessage(R.string.offline_error);
                }
                break;
            /*case R.id.facebookBtn:
                if (App.getInstance().isConnection()) {
                    FacebookManager.getInstance().login(this, this);
                } else {
                    App.getInstance().showWaitingMessage(R.string.offline_error);
                }
                break;*/
            default:
                break;
        }
    }


    public void signIn() {

        submitBtn.setEnabled(false);
        Patient authRequestEntity = getRequestEntity();
        UIUtils.hideSoftKeyboard(this, emailEdt);
        SessionsPost sessionsPost = new SessionsPost();
        sessionsPost.withEntry(authRequestEntity);
        submitBtn.setProgressState(true);
        sessionsPost.execute(this, new RequestListener<Patient>() {
            @Override
            public void onRequestComplete(Response<Patient> response) {
                processResponse(response, SignInType.EMAIL);
            }
        });
    }

    private Patient getRequestEntity(String email) {
        final String userEmail = !TextUtils.isEmpty(email) ?
                email : emailEdt.getText().toString();
        final String userPass = passwordEdt.getText().toString();
        Patient patient = new Patient();
        patient.setEmail(userEmail);
        patient.setPassword(userPass);
        return patient;
    }

    private Patient getRequestEntity() {
        return getRequestEntity(null);
    }

    void processResponse(Response<Patient> response, @NonNull SignInType signInType) {
        final Patient data = response.getData();
        submitBtn.setProgressState(false);
        if (response.isSuccess() && response.getData() != null) {

            String currentEmail = response.getData().getEmail();
            Patient previousPatient = App.getInstance().getPatient();

            if (previousPatient == null || (!TextUtils.isEmpty(previousPatient.getEmail())
                    && !previousPatient.getEmail().equals(currentEmail))) {
                /** Clear notification center from previous user */
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancelAll();
            }

            PreferencesManager.getInstance().setUserActive(data.isActive());
            data.setInstallationId(App.getInstance().getParseInstallationObjectId());
            data.setDeviceToken(App.getInstance().getDeviceId());
            data.setIsUpdated(true);
            App.getInstance().setPatient(data);

            Patient patient = getRequestEntity(currentEmail);

            initAuthIntent(data, patient);

            //Initialize mix panel
            //Send Event to MixPanel
            try
            {
                JSONObject props = new JSONObject();
                props.put("Install Date",PreferencesManager.getInstance().getDateString());
                MixPanelHelper helper = new MixPanelHelper(SignInActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.SIGN_IN);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            PreferencesManager.getInstance().setIsFirstLogin(false);

            DatabaseHelper.recreateDatabase(this);
            onLoggedInFinish();
        } else {
            String errorMessage = getString(R.string.login_failure);
            if (signInType.equals(SignInType.FACEBOOK)
                    && !TextUtils.isEmpty(response.getUserResponseError())) {
                errorMessage = response.getUserResponseError();
            }
            AlertDialogHelper.getInstance().showMessage(SignInActivity.this,
                    "Please check your details and try again");
            submitBtn.setEnabled(true);
        }
    }

    private void initAuthIntent(Patient data, Patient patient) {
        authIntent = getIntent();
        authIntent.putExtra(AccountManager.KEY_AUTHTOKEN, data.getToken());

        authIntent.putExtra(AccountManager.KEY_ACCOUNT_NAME, patient.getEmail());
        authIntent.putExtra(AccountManager.KEY_PASSWORD, patient.getPassword());
    }

    private TextView.OnEditorActionListener onPasswordEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            signIn();
            return true;
        }
    };

    @Override
    public void onFbLoginSuccess(String facebookAccessToken, String action) {
        if (!TextUtils.isEmpty(facebookAccessToken)) {
            FacebookToken facebookToken = new FacebookToken();
            facebookToken.setToken(facebookAccessToken);
            FacebookSessionsPost facebookPost = new FacebookSessionsPost();
            facebookPost.withEntry(facebookToken).execute(this, new RequestListener<Patient>() {
                @Override
                public void onRequestComplete(Response<Patient> response) {
                    processResponse(response, SignInType.FACEBOOK);
                }
            });
        }
    }

    @Override
    public void onFbLoginFail(String result) {
        //empty
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == FacebookManager.FACEBOOK_REQUEST_CODE) {
            FacebookManager.getInstance().onActivityResult(requestCode, resultCode, intent);
        }
    }
}
