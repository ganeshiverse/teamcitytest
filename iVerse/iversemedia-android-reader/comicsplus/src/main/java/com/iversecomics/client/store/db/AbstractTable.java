package com.iversecomics.client.store.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

public abstract class AbstractTable {
    protected static boolean getBoolean(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        String val = c.getString(colIdx);
        if (val == null) {
            return false;
        }
        if (val.length() < 1) {
            return false;
        }
        char ch = Character.toLowerCase(val.charAt(0));
        return ((ch == '1') || (ch == 't') || (ch == 'y'));
    }

    protected static double getDouble(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        return c.getDouble(colIdx);
    }

    protected static int getInteger(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        return c.getInt(colIdx);
    }

    protected static long getLong(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        return c.getLong(colIdx);
    }

    protected static boolean getOptionalBoolean(Cursor c, String colName, boolean defaultValue) {
        int colIdx = c.getColumnIndex(colName);
        if (colIdx == (-1)) {
            return defaultValue;
        }
        String val = c.getString(colIdx);
        return Boolean.parseBoolean(val);
    }

    protected static int getOptionalInteger(Cursor c, String colName, int defaultValue) {
        int colIdx = c.getColumnIndex(colName);
        if (colIdx == (-1)) {
            return defaultValue;
        }
        return c.getInt(colIdx);
    }

    protected static String getOptionalString(Cursor c, String colName) {
        int colIdx = c.getColumnIndex(colName);
        if (colIdx == (-1)) {
            return null;
        }
        return c.getString(colIdx);
    }

    protected static long getOptionalUtcTimestamp(Cursor c, String colName) {
        int colIdx = c.getColumnIndex(colName);
        if (colIdx == (-1)) {
            // Not found.
            return 0;
        }
        // Assume data in tablet is already set to UTC.
        return c.getLong(colIdx);
    }

    protected static String getString(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        return c.getString(colIdx);
    }

    protected static String[] getStringList(Cursor c, String colName) {
        int colIdx = c.getColumnIndex(colName);
        if (colIdx == (-1)) {
            return null;
        }
        String val = c.getString(colIdx);
        if (TextUtils.isEmpty(val)) {
            return null;
        }
        return val.split(", ");
    }

    protected static Uri getUri(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        String value = c.getString(colIdx);
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        return Uri.parse(value);
    }

    /**
     * Get a long timestamp (milliseconds since epoch), adjusted for UTC.
     */
    protected static long getUtcTimestamp(Cursor c, String colName) {
        int colIdx = c.getColumnIndexOrThrow(colName);
        // Assume data in tablet is already set to UTC.
        return c.getLong(colIdx);
    }

    protected static void setBoolean(ContentValues values, String key, boolean flag) {
        String value = Boolean.toString(flag);
        values.put(key, value);
    }

    protected static void setStringList(ContentValues values, String key, String[] strs) {
        if ((strs == null) || (strs.length <= 0)) {
            values.put(key, "");
            return;
        }
        StringBuilder value = new StringBuilder();
        boolean delim = false;
        for (String str : strs) {
            if (delim) {
                value.append(", ");
            }
            value.append(str);
            delim = true;
        }
        values.put(key, value.toString());
    }

    protected static void setUri(ContentValues values, String key, Uri uri) {
        String value = null;

        if (uri != null) {
            value = uri.toString();
        }

        values.put(key, value);
    }
}
