package com.babylon.api.request.base;

import com.babylon.api.request.BaseResponseProcessor;
import com.babylon.utils.L;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;

public abstract class AbsRequestPatch<I, O> extends AbsRequest<HttpPatch, O> {

    protected String jsonBody;

    public AbsRequestPatch<I, O> withEntry(I entry) {
        jsonBody = toJson(entry);
        return this;
    }

    private StringEntity getStringEntity(String jsonBody) {
        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonBody);
            L.d(TAG, "Request json: " + jsonBody);
        } catch (UnsupportedEncodingException e) {
            L.e(TAG, e.toString(), e);
        }
        return entity;
    }

    public HttpEntity getHttpEntity() {
        StringEntity entity = getStringEntity(jsonBody);
        return entity;
    }

    @Override
    protected HttpPatch prepareRequest() {
        super.prepareRequest();
        httpRequest.setEntity(getHttpEntity());
        return httpRequest;
    }

    @Override
    protected HttpPatch acquireHttpRequest() {
        return new HttpPatch();
    }

    @Override
    protected AbsResponseProcessor<O> getResponseProcessor() {
        return new BaseResponseProcessor<O>(getOutputType());
    }
}
