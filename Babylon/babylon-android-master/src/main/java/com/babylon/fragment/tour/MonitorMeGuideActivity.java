package com.babylon.fragment.tour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.adapter.GuidePagerAdapter;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.utils.PreferencesManager;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Date;

public class MonitorMeGuideActivity extends BaseActivity implements OnCloseTourEventListener {
    public static final int START_MONITOR_FOR_RESULT = 100;

    private GuidePagerAdapter adapter;
    private ViewPager pager;
    private CirclePageIndicator indicator;

    public static void startActivityForResult(Activity activity, int code) {
        Intent intent = new Intent(activity, MonitorMeGuideActivity.class);
        PreferencesManager.getInstance().setFirstMonitorMeLaunch(false);
        activity.startActivityForResult(intent, code);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
      //  getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_me_guide);
     //   getActionBar().hide();
        initViews();
    }


    private void initViews() {
        adapter = new GuidePagerAdapter(getSupportFragmentManager());

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(onPageChangeListener);
    }

    public void onSkipClickListener(View view) {
        finish();
        onCloseTourEvent();
    }

    private void setHistoryVisibility(int visibility) {
        final ViewGroup viewGroupHistory = (ViewGroup) findViewById(R.id.layout_history_navigation);

        if (viewGroupHistory.getVisibility() != visibility) {

            final ViewGroup viewGroupIndicator = (ViewGroup) findViewById(R.id.layout_indicator);

            if (visibility == View.VISIBLE) {
                viewGroupIndicator.setVisibility(View.INVISIBLE);
                viewGroupHistory.setVisibility(View.VISIBLE);
            } else {
                viewGroupIndicator.setVisibility(View.VISIBLE);
                viewGroupHistory.setVisibility(View.INVISIBLE);
            }
        }
    }

    private final ViewPager.SimpleOnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener
            () {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);

            if (position + 1 == GuidePagerAdapter.PAGE_NUMBER) {
                setHistoryVisibility(View.VISIBLE);

                final TextView dateTextView = (TextView) findViewById(R.id.text_view_date);
                dateTextView.setText(new MonitorMeDateViewController().getFormattedDay(new Date()));

            } else {
                setHistoryVisibility(View.INVISIBLE);
            }
        }
    };

    @Override
    public void onBackPressed() {
        onCloseTourEvent();
    }


    @Override
    public void onCloseTourEvent() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

interface OnCloseTourEventListener {
    public void onCloseTourEvent();
}