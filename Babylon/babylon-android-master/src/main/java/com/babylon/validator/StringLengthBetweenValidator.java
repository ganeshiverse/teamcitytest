package com.babylon.validator;

public class StringLengthBetweenValidator extends BaseValidator {
    private int minimum;
    private int maximum;

    public StringLengthBetweenValidator(BaseValidator nextValidator, String errorMessage, int minimum, int maximum) {
        super(nextValidator, errorMessage);
        this.minimum = minimum;
        this.maximum = maximum;
    }

    @Override
    public String validate(String string) {
        return (string.length() < minimum || string.length() > maximum) ? errorMessage : null;
    }
}
