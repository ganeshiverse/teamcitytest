package com.babylon.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.babylon.Keys;
import com.babylon.R;
import com.babylon.images.ImageLoader;

public class ImageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        Bundle extras = getIntent().getExtras();
        if (extras != null && !TextUtils.isEmpty(extras.getString(Keys.KEY_PHOTO_PATH))) {
            String path = extras.getString(Keys.KEY_PHOTO_PATH);
            final Bitmap bitmap = ImageLoader.getInstance().getBitmapByPath(path, ImageLoader.BIG_IMAGE);
            imageView.setImageBitmap(bitmap);
        } else {
            finish();
        }
    }

    public void onImageClick(View view) {
        finish();
    }
}
