package com.axsystech.excelicare.framework.ui.activities;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.framework.ui.fragments.media.MediaDetailsFragment;
import com.axsystech.excelicare.util.views.TypefaceSpan;

/**
 * Created by someswarreddy on 21/07/15.
 */
public class MediaDetailsActivity extends BaseActivity {

    private String TAG = MediaDetailsActivity.class.getSimpleName();
    private MediaDetailsActivity mActivity;

    private MediaDetailsFragment fragment;

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity,getString(R.string.text_add_details)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        mActivity = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        readIntentData();
        preloadData();
        addEventListeners();

        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

        fragment = new MediaDetailsFragment();
        fragment.setArguments(getIntent().getExtras());
        ft.add(R.id.media_container_layout, fragment, TAG);
//        ft.addToBackStack(null);
        ft.commit();

    }

    private void readIntentData() {

    }

    private void preloadData() {

    }

    private void addEventListeners() {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
    }

}
