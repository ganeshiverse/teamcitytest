package com.iversecomics.client.store.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.LargeTest;

import com.iversecomics.client.store.model.Comic;

public class ComicDetailsActivityTest extends AbstractStoreActivityTestCase<ComicDetailsActivity> {
    public ComicDetailsActivityTest() {
        super(ComicDetailsActivity.class);

        Intent intent = new Intent();
        intent.putExtra("testing", true);
        // 2905 = com.iversecomics.idw.star.trek.year.four.enterprise.experiment.one
        // 4193 = com.iversecomics.red.five.comics.atomic.robo.dogs.of.war.two
        intent.putExtra(ComicDetailsActivity.EXTRA_COMIC_BUNDLE_NAME,
                "com.iversecomics.red.five.comics.atomic.robo.dogs.of.war.two");
        setActivityIntent(intent);
    }

    @LargeTest
    public void testOnResumeCurrentIssue_LoadFromServer() throws Throwable {
        ComicDetailsActivity activity = getActivity();
        assertNotNull("Activity should have launched", activity);

        waitForRefreshTasks(activity);

        Comic comic = activity.getComic();
        assertNotNull("Should have fetched a comic", comic);
        assertEquals("Comic.id", "4193", comic.getComicId());
        assertEquals("Comic.comicBundleName", "com.iversecomics.red.five.comics.atomic.robo.dogs.of.war.two",
                comic.getComicBundleName());

        Fragment fragment = activity.getActiveFragment();
        MoreAsserts.assertAssignableFrom(CurrentIssueFragment.class, fragment);
        CurrentIssueFragment currentIssueFrag = (CurrentIssueFragment) fragment;

        Comic fragComic = currentIssueFrag.getComic();
        assertEquals("Comic.id", comic.getComicId(), fragComic.getComicId());
        assertEquals("Comic.comicBundleName", comic.getComicBundleName(), fragComic.getComicBundleName());
    }
}
