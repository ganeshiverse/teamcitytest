package com.babylon.database.schema;

import android.net.Uri;
import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;
import com.babylon.enums.ActivityType;
import com.babylon.sync.SyncUtils;

public interface PhysicalActivitySchema {

    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri BASE_CONTENT_URI = Uri.parse("content://" + DatabaseHelper.CONTENT_AUTHORITY);
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table
            .PHYSICAL_ACTIVITY.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        INTENSITY("intensity", DBType.TEXT),
        TITLE("title", DBType.INT),
        CALORIES("calories", DBType.INT),
        STEPS("steps", DBType.INT),
        REFRESHED("refreshed", DBType.INT),
        TYPE("type", DBType.INT),
        SOURCE("source", DBType.TEXT),
        START_DATE("startDate", DBType.INT),
        END_DATE("endDate", DBType.INT),
        DURATION("duration", DBType.INT),
        DISTANCE("distance", DBType.FLOAT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public final static class Query extends BaseQuery {
        public static final int CURSOR_LOAD_TODAY_PHYSICAL_ACTIVITIES = 7846;
        public static final int CURSOR_LOAD_TODAY_SUM_CYCLE = 7847;
        public static final int CURSOR_LOAD_TODAY_SUM_WALK_AND_RUN = 7849;
        public static final int CURSOR_LOAD_TODAY_SLEEP = 7850;
        public static final int CURSOR_LOAD_TODAY_SUM_OTHERS = 7878;

        public static final String NOT_ZERO = Columns.DURATION.getName() + " != 0";
        public static final String DAY_SELECTION = Columns.START_DATE.getName() + ">= ? AND "
                + Columns.START_DATE.getName() + "< ? AND " + Columns.SYNC_WITH_SERVER + " != "
                + String.valueOf(SyncUtils.SYNC_TYPE_DELETE);

        public static final String DAY_SELECTION_WITHOUT_SLEEPING = DAY_SELECTION
                + " AND " + Columns.TYPE + " != " + ActivityType.SLEEP.getValue();

        public static final String SORT_DATE_ASC = Columns.START_DATE.getName() + " ASC";

        public static final String SELECTION_SYNC_NEEDED = Columns.SYNC_WITH_SERVER.getName() + " != " + SyncUtils
                .SYNC_TYPE_ADD;

        public static final String SELECTION_DELETE = SELECTION_SYNC_NEEDED + " AND " + Columns.ID.getName() + " NOT " +
                "IN ";

        public static final String DAY_SELECTION_OTHERS = DAY_SELECTION
                + " AND " + Columns.TYPE.getName() + " != " + ActivityType.RUNNING.getValue()
                + " AND " + Columns.TYPE.getName() + " != " + ActivityType.WALKING.getValue()
                + " AND " + Columns.TYPE.getName() + " != " + ActivityType.CYCLING.getValue();

        public static final String SUM_KILOCALORIES_COLUMN = "SUM(" + Columns.CALORIES.getName() + ")";

        public static final String DAY_SELECTION_CYCLE = DAY_SELECTION
                + " AND " + Columns.TYPE.getName() + " = " + ActivityType.CYCLING.getValue();

        public static final String DAY_SELECTION_WALK_AND_RUN = DAY_SELECTION
                + " AND (" + Columns.TYPE.getName() + " = " + ActivityType.RUNNING.getValue() + " OR "
                + Columns.TYPE.getName() + " = " + ActivityType.WALKING.getValue() + ")";

        //---------------------- Selections for sleep
        public static final String DAY_SELECTION_SLEEP = Columns.END_DATE.getName() + ">= ? AND "
                + Columns.END_DATE.getName() + "< ? AND " + Columns.SYNC_WITH_SERVER + " != "
                + String.valueOf(SyncUtils.SYNC_TYPE_DELETE);

        public static final String NOT_NULL_AND_PERIOD_SELECTION_STRING_SLEEP = DAY_SELECTION_SLEEP
                + " AND " + Columns.DURATION.getName() + " IS NOT NULL ";

        public static final String DAY_SELECTION_SLEEPING =
                DAY_SELECTION_SLEEP + " AND  " + Columns.TYPE + " = " + ActivityType.SLEEP.getValue();

        public static final String SLEEP_NOT_NULL_AND_DATE_SELECTION =
                Columns.TYPE.getName() + " = " + ActivityType.SLEEP.getValue()
                        + " AND " + NOT_ZERO
                        + " AND " + NOT_NULL_AND_PERIOD_SELECTION_STRING_SLEEP;
        //------------------------------------------

        public static final int _ID = 0;
        public static final int ID = 1;
        public static final int SYNC_WITH_SERVER = 2;
        public static final int INTENSITY = 3;
        public static final int TITLE = 4;
        public static final int CALORIES = 5;
        public static final int STEPS = 6;
        public static final int REFRESHED = 7;
        public static final int TYPE = 8;
        public static final int SOURCE = 9;
        public static final int START_DATE = 10;
        public static final int END_DATE = 11;
        public static final int DURATION = 12;
        public static final int DISTANCE = 13;
    }
}

