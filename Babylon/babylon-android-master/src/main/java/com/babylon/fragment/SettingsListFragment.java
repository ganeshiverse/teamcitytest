package com.babylon.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.SettingsAdapter;
import com.babylon.model.ItemData;
import com.babylon.utils.ActionBarType;

import java.util.ArrayList;

public class SettingsListFragment extends BaseFragment  {

    public static final String TAG = SettingsListFragment.class.getSimpleName();
    private static final String ns = null;
    private SettingsFragmentListener listener;
    String mItemSelected;

    public interface SettingsFragmentListener {
        public void onItemClicked(String title);

    }

    private SettingsAdapter mAdapter;
    private String[] navMenuTitles;

    public static SettingsListFragment newInstance() {
        SettingsListFragment fragment = new SettingsListFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

      // setTitle(getString(R.string.btn_settings_title));
        getActivity().getActionBar().show();
        setupActionBar(ActionBarType.SETTINGS);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        ArrayList<ItemData> itemsData = new ArrayList<ItemData>();

            navMenuTitles = getResources().getStringArray(R.array.settings_items);
            for(String title:navMenuTitles)
            {
                String[] split = title.split(";");
                itemsData.add(new ItemData(split[0],split[1]));
            }
        if(App.getInstance().getPatient() != null && App.getInstance().getPatient().getAccountState() != null)
        {
            if (App.getInstance().getPatient().getAccountState().compareToIgnoreCase("master") != 0) {
                itemsData.remove(0);
            }
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        mAdapter = new SettingsAdapter(getActivity().getApplicationContext(),itemsData,onPropertyItemClickListener);

        recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }


    private final View.OnClickListener onItemClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null && (!TextUtils.isEmpty(view.getTag().toString()))) {
                        listener.onItemClicked(view.getTag().toString());
                    }
                }
            };


    private SettingsAdapter.OnPropertyItemClickListener onPropertyItemClickListener =
            new SettingsAdapter.OnPropertyItemClickListener() {

                @Override public View.OnClickListener getOnPropertyClickListener() {
                    return onItemClickListener;
                }
            };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (SettingsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

}






