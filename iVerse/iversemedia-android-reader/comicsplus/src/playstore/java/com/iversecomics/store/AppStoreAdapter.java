package com.iversecomics.store;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.tasks.VerifySubscriptionTask;
import com.iversecomics.client.store.ui.HasComic;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.store.iab.IabHelper;
import com.iversecomics.store.iab.IabResult;
import com.iversecomics.store.iab.Inventory;
import com.iversecomics.store.iab.Purchase;
import com.iversecomics.ui.ComicDetailsFragment;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Gabriel Dogaru.
 * <p/>
 * Google Play app store adapter.
 */
public class AppStoreAdapter extends AbstractAppStoreAdapter implements IabHelper.OnIabSetupFinishedListener, IabHelper.OnIabPurchaseFinishedListener{
    Activity activity;
    public IabHelper billingHelper;
    private String productSKUId = null;
    private String mComicbundleName=null;
    private static int BILLING_REQUEST_CODE = 1001;
    private String TAG = this.getClass().getSimpleName();
    private boolean isFromSubscription =false;

    @Override
    public void onCreate(Bundle savedInstanceState, final Activity activity) {
        this.activity = activity;
        billingHelper = new IabHelper(activity, ServerConfig.getDefault().getAndroidMarketPublicKey());
        billingHelper.enableDebugLogging(false);
        billingHelper.startSetup(this);
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        if (billingHelper != null) {
            billingHelper.dispose();
        }
        billingHelper = null;
    }

    @Override
    public void purchaseComic(final Comic comic) {
        // Buy comic from Google Play
        if (billingHelper != null) {
            billingHelper.flagEndAsync();
        }
        productSKUId = comic.getSku();
        mComicbundleName = comic.getComicBundleName();

        if (billingHelper != null) {
            try {
                billingHelper.launchPurchaseFlow(activity, comic.getSku(), BILLING_REQUEST_CODE, this,productSKUId+"paid");
            } catch (IllegalStateException ex) {
                Toast.makeText(activity, "Please retry in a few seconds.", Toast.LENGTH_SHORT).show();
                billingHelper.flagEndAsync();
            }
        }

    }

    @Override
    public void purchaseSubscription(final String subscriptionSku) {
        // Buy comic from Google Play
        isFromSubscription = true;
        if (billingHelper != null) {
            billingHelper.flagEndAsync();
        }
        if (!billingHelper.subscriptionsSupported()) {
            Toast.makeText(activity,activity.getString(R.string.subscriptions_not_supported_onyour_device),Toast.LENGTH_SHORT).show();
            return;
        }
        productSKUId = subscriptionSku;
        mComicbundleName = subscriptionSku;

        // Need to Qurey the subscription for owned state.
        ArrayList<String> subSkuList = new ArrayList<String>();
        subSkuList.add(productSKUId);
        try {
            if (billingHelper != null) {
                billingHelper.flagEndAsync();
            }
            billingHelper.queryInventoryAsync(true, subSkuList, msubSKUListener);
        }catch (Exception ex){
            // TODO - Handle if any crash occurs.
        }
        //billingHelper.launchPurchaseFlow(activity,subscriptionSku, IabHelper.ITEM_TYPE_SUBS, BILLING_REQUEST_CODE, this, subscriptionSku+"subscription");
    }

    private String replaceLanguageAndRegion(String str) {
        // Substitute language and or region if present in string
        if (str.contains("%lang%") || str.contains("%region%")) {
            Locale locale = Locale.getDefault();
            str = str.replace("%lang%", locale.getLanguage().toLowerCase());
            str = str.replace("%region%", locale.getCountry().toLowerCase());
        }
        return str;
    }

    @Override
    public boolean canHandle(int requestCode) {
        return true;

    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent intent) {
        if (billingHelper == null) {
            return;
        }

        billingHelper.handleActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void restoreTransactions() {
        //TODO add billing supported condition and dialog
    }

    @Override
    public boolean checkSkuIdinGooglePlay(String SKUId) {
        productSKUId = SKUId;
        ArrayList<String> skuList = new ArrayList<String>();
        skuList.add(SKUId);
        try {
            if (billingHelper != null) {
                billingHelper.flagEndAsync();
            }
            billingHelper.queryInventoryAsync(true, skuList, mGotInventoryListener);
        }catch (Exception ex){
            // TODO - Handle if any crash occurs.
        }
        return false;
    }

    /**
     * Security Recommendation: When you receive the purchase response from Google Play, make sure to check the returned data
     * signature, the orderId, and the developerPayload string in the Purchase object to make sure that you are getting the
     * expected values. You should verify that the orderId is a unique value that you have not previously processed, and the
     * developerPayload string matches the token that you sent previously with the purchase request. As a further security
     * precaution, you should perform the verification on your own secure server.
     */
    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (billingHelper == null) {
            return;
        }
        if (result.isFailure()) {
            Log.d(TAG, "IAB Error purchasing: "+ result.toString());
            if(((result.getResponse()+"").equalsIgnoreCase("1"))||((result.getResponse()+"").equalsIgnoreCase("-1005"))){
                return;
            }else{
                final  String helpUrl = replaceLanguageAndRegion(activity.getResources().getString(R.string.billing_help_url));
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(R.string.billing_not_supported_title).setIcon(android.R.drawable.stat_sys_warning).setMessage(R.string.billing_not_supported_message)
                        .setCancelable(false).setPositiveButton(android.R.string.ok, null)
                        .setNegativeButton(R.string.learn_more, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(helpUrl));
                                activity.startActivity(intent);
                            }
                        });
                builder.create().show();
                return;
            }
        } else if (productSKUId !=null && productSKUId.equals(info.getSku())) {
            // We consume the item straight away so we can test multiple purchases
            //billingHelper.consumeAsync(info, null);
            if (productSKUId !=null && productSKUId.equals(info.getSku())) {
                Log.d(TAG, "onPurchaseStateChange() itemId: %s %s" + productSKUId);
                IverseApplication app = IverseApplication.getApplication();
                Comic comic = null;
                if(!isFromSubscription) {
                    comic= app.getComicStore().getDatabaseApi().getComicByBundleName(/*info.getSku()*/mComicbundleName);
                    if (comic == null && activity instanceof HasComic) {
                        comic = ((HasComic) activity).getComic();
                    }
                    if (comic == null) {
                        Log.w(TAG, "todo later");//TODO
                        Toast.makeText(activity,  activity.getResources().getString(R.string.could_not_find_comic_name), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                    boolean didPurchaseSubscription = false;
                    for (String subscriptionSku : ServerConfig.getDefault().getSubscriptionProducts()) {
                        if (subscriptionSku.equalsIgnoreCase(info.getSku())) {
                            didPurchaseSubscription = true;
                        }
                    }

                    // Notify iVerse Server that user has purchased a subscription
                    if (didPurchaseSubscription) {
                        String installId = ComicPreferences.getInstance().getInstallId();
                        VerifySubscriptionTask task = new VerifySubscriptionTask(activity.getApplicationContext(),
                                app.getComicStore(), info.getSku(), installId, info.getOriginalJson().trim().toString(),info.getSignature(), activity);
                        app.getTaskPool().submit(task);
                    } else {// Issue Purchase Verification to Our Server
                        try {
                            com.iversecomics.client.store.tasks.VerifyPurchaseTask task = new com.iversecomics.client.store.tasks.VerifyPurchaseTask(app.getComicStore());
                            task.setComicBundleName(comic.getComicBundleName());
                            task.setComicName(comic.getName());
                            task.setDeviceId(AndroidInfo.getUniqueDeviceID(activity.getApplicationContext()));
                            task.setArtifactType(AndroidInfo.getArtifactForDevice(activity.getApplicationContext()));
                            task.setReceiptData(info.getOriginalJson().trim().toString());
                            task.setSignature(info.getSignature());
                            app.getTaskPool().submit(task);
                        }catch (Exception e){
                            //Handle if any exception raised
                        }
                    }

                }
        }

    }
    @Override
    public void onIabSetupFinished(IabResult result) {
        if (billingHelper == null) {
            return;
        }
        if (result.isSuccess()) {
            Log.d(TAG,"In-app Billing set up"+result.toString());
            if(productSKUId !=null) {
                ArrayList<String> itemSkuList = new ArrayList<String>();
                itemSkuList.add(productSKUId);
                billingHelper.queryInventoryAsync(true, itemSkuList, mGotInventoryListener);
            }
        } else {
            Log.d(TAG, "Problem setting up In-app Billing: "+result.toString());
        }
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "In-app Billing Query inventory finished.");
            // Have we been disposed of in the meantime? If so, quit.
            if (billingHelper == null) {
                return;
            }
            // Is it a failure?
            if (result.isFailure()) {
                Log.d(TAG,"Failed to query inventory:"+result.getMessage());
                FragmentManager fm = ((FragmentActivity)activity).getSupportFragmentManager();
                fm.executePendingTransactions();
                Fragment fragemnt = fm.findFragmentById(R.id.pageContainer);
                if(fragemnt instanceof ComicDetailsFragment) {
                    ((ComicDetailsFragment)fragemnt).updateComingSoon(false);
                }
                // Toast.makeText(activity, "Not Available",Toast.LENGTH_SHORT).show();
                return;
            }else {
                if(inventory.getSkuDetails(productSKUId) !=null){
                    String comicPrice =inventory.getSkuDetails(productSKUId).getSku();
                    if(comicPrice.length()>0){
                        FragmentManager fm = ((FragmentActivity)activity).getSupportFragmentManager();
                        fm.executePendingTransactions();
                        Fragment fragemnt = fm.findFragmentById(R.id.pageContainer);
                        if(fragemnt instanceof ComicDetailsFragment) {
                            ((ComicDetailsFragment)fragemnt).updateComingSoon(true);
                        }
                        //Toast.makeText(activity, "Product Available",Toast.LENGTH_SHORT).show();
                        //Log.d(TAG, "Query inventory Product details:"+inventory.getSkuDetails(productSKUId).getTitle());
                    }
                }else{
                    FragmentManager fm = ((FragmentActivity)activity).getSupportFragmentManager();
                    fm.executePendingTransactions();
                    Fragment fragemnt = fm.findFragmentById(R.id.pageContainer);
                    if(fragemnt instanceof ComicDetailsFragment) {
                        ((ComicDetailsFragment)fragemnt).updateComingSoon(false);
                    }
                    //Toast.makeText(activity, "Product Not Available",Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Query inventory Product details not available:");

                }
                return;
            }

        }
    };
        //TODO - handle after testing.
        IabHelper.OnConsumeFinishedListener  mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener(){
            @Override
            public void onConsumeFinished(Purchase info, IabResult result) {
                // if we were disposed of in the meantime, quit.
                IverseApplication app = IverseApplication.getApplication();
                if (billingHelper == null) return;
                if (result.isSuccess()) {
                }
                else {
                    Log.d(TAG,"Error while consuming: " +result);
                }

            }
        };


    IabHelper.QueryInventoryFinishedListener msubSKUListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "In-app Billing Query inventory finished.");
            IverseApplication app = IverseApplication.getApplication();
            // Have we been disposed of in the meantime? If so, quit.
            if (billingHelper == null) {
                return;
            }
            // Is it a failure?
            if (result.isFailure()) {
                Log.d(TAG, "Failed to query inventory:" + result.getMessage());
                return;
            } else {
                if (ServerConfig.getDefault().getSubscriptionProducts().size() > 0) {
                    int ownedState = -1;
                    String subscriptionProductId = ServerConfig.getDefault().getSubscriptionProducts().get(0);
                    if (inventory.getSkuDetails(subscriptionProductId) != null) {
                        Purchase purchase =  inventory.getPurchase(subscriptionProductId);
                        //Means User already buy the subscription
                        if(purchase !=null){
                            ownedState = purchase.getPurchaseState();
                            if (ownedState == 0) {
                                Log.d(" OWNED", ownedState+"");
                                String installId = ComicPreferences.getInstance().getInstallId();
                                VerifySubscriptionTask task = new VerifySubscriptionTask(activity.getApplicationContext(),
                                        app.getComicStore(), purchase.getSku(), installId, purchase.getOriginalJson().trim().toString(), purchase.getSignature(),activity);
                                app.getTaskPool().submit(task);
                            } else {
                                // User need to buy the new subscription - Purchase not owned or expired.
                                Log.d("NOT OWNED", ownedState + "");
                                buyNewSubscription(subscriptionProductId);
                            }
                        }else{
                            // User need to buy the new subscription
                            buyNewSubscription(subscriptionProductId);
                        }

                    } else {
                        //subscription not available in GooglePlay
                        Toast.makeText(activity,activity.getString(R.string.billing_not_supported_message),Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    };
    public void buyNewSubscription(String subscriptionProductId){
        if (billingHelper != null) {
            billingHelper.flagEndAsync();
        }

        if (billingHelper != null) {
            try {
                billingHelper.launchPurchaseFlow(activity, subscriptionProductId, IabHelper.ITEM_TYPE_SUBS, BILLING_REQUEST_CODE, this, subscriptionProductId + "subscription");
            } catch (IllegalStateException ex) {
                Toast.makeText(activity, "Please retry in a few seconds.", Toast.LENGTH_SHORT).show();
                billingHelper.flagEndAsync();
            }
        }

    }

}
