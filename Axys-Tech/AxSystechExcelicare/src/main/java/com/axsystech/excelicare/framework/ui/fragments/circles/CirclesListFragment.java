package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.InvitationReceivedDetails;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.CircleListSearchLoader;
import com.axsystech.excelicare.loaders.CircleMemberListDbLoader;
import com.axsystech.excelicare.loaders.CirclesListDBLoader;
import com.axsystech.excelicare.loaders.GetCirclesListFromDbLoader;
import com.axsystech.excelicare.loaders.GetMediaItemsFromDBLoader;
import com.axsystech.excelicare.loaders.ListMembersInCircleLoader;
import com.axsystech.excelicare.loaders.ListMyCirclesLoader;
import com.axsystech.excelicare.loaders.MsgInboxAndSentSearchLoader;
import com.axsystech.excelicare.loaders.RemoveCircleLoader;
import com.axsystech.excelicare.loaders.RemoveCircleResponse;
import com.axsystech.excelicare.loaders.SaveMediaItemsDBLoader;
import com.axsystech.excelicare.util.AESHelper;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.CryptoEngine;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class CirclesListFragment extends BaseFragment implements View.OnClickListener, TextWatcher {

    private String TAG = CirclesListFragment.class.getSimpleName();
    private CirclesListFragment mFragment;
    private MainContentActivity mActivity;

    //byte[] keyStart = "AXSYS".getBytes();

    private static final String LIST_MY_CIRCLES_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesListFragment.LIST_MY_CIRCLES_LOADER";
    private static final String LIST_MEMBERS_IN_CIRCLE_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesListFragment.LIST_MEMBERS_IN_CIRCLE_LOADER";
    private static final String REMOVE_CIRCLE_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesListFragment.REMOVE_CIRCLE_LOADER";
    private static final String CIRCLE_LIST_SEARCH = "com.axsystech.excelicare.framework.ui.messages.CirclesListFragment.CIRCLE_LIST_SEARCH";
    private static final String SAVE_CIRCLE_LIST_LOADER = "com.axsystech.excelicare.framework.ui.messages.CirclesListFragment.SAVE_CIRCLE_LIST_LOADER";
    private static final String SAVE_CIRCLE_MEMBER_LIST_LOADER = "com.axsystech.excelicare.framework.ui.messages.CirclesListFragment.SAVE_CIRCLE_MEMBER_LIST_LOADER";
    private static final String GET_CIRCLE_LIST_LOADER = "com.axsystech.excelicare.framework.ui.messages.CirclesListFragment.GET_CIRCLE_LIST_LOADER";

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    private LayoutInflater mLayoutInflater;

    @InjectView(R.id.circlesListView)
    private ListView mCirclesListView;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    String circleaName;
    String circledecrypt;

    String systemCircleId;
    boolean menu = true;

//    CirclesListDbModel rowObject;

    @InjectSavedState
    private CirclesListDbModel selectedCirclesListDbModel = new CirclesListDbModel();

    private ArrayList<CirclesListDbModel> mCirclesDbModelAL;
    private ArrayList<CircleMemberListDbModel> circleMemberListDbModels;

    private MyCirclesListAdapter mCirclesListAdapter;
//    private ListMyCirclesResponse.CircleDetails mSelectedCircleObject;

    private int pageIndex;
    private int PAGE_SIZE = 10;
    //ArrayList<ListMyCirclesResponse.CircleDetails> completeMainCirclesAL = new ArrayList<ListMyCirclesResponse.CircleDetails>();
    ArrayList<CirclesListDbModel> completeCirclesListDbModels = new ArrayList<CirclesListDbModel>();
    ArrayList<CirclesListDbModel> circlesListDbModels;
    //    ArrayList<ListMyCirclesResponse.CircleDetails> mainCirclesAL;
    int previousFirstVisibleItem = 0;

    @Override
    protected void initActionBar() {
        // update actionbar title
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, "My Care Circles"));

        mActivity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.changepasswordactionbar_color)));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_circles_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        mLayoutInflater = LayoutInflater.from(mActivity);

        preloadData();
        readIntentArgs();
        getCirclesListFromServer(true);
        addEventListener();

    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_circle));

        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Add Content")) {
                    if (m.getValue() == 1) {
                        menu = true;
                    } else if (m.getValue() == 0) {
                        menu = false;
                    }
                }
            }
        }
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void getCirclesListFromServer(boolean showDialog) {
        if (CommonUtils.hasInternet()) {
            if (showDialog)
                displayProgressLoader(false);
            pageIndex = pageIndex + 1;

            ListMyCirclesLoader listMyCirclesLoader = new ListMyCirclesLoader(mActivity, mActiveUser.getToken(), "0", "", pageIndex, PAGE_SIZE);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MY_CIRCLES_LOADER), listMyCirclesLoader);
            }
        } else {
            final GetCirclesListFromDbLoader getCirclesListFromDbLoader = new GetCirclesListFromDbLoader(mActivity);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CIRCLE_LIST_LOADER), getCirclesListFromDbLoader);
            }
        }
    }

    private void addEventListener() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String searchEdit = null;

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        try {
                            searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
//                        CircleListSearchLoader msgSentLoader = null;
//                        msgSentLoader = new CircleListSearchLoader(mActivity, mActiveUser.getToken(),
//                                searchEditText.getText().toString().trim());
                        ListMyCirclesLoader listMyCirclesLoader = new ListMyCirclesLoader(mActivity, mActiveUser.getToken(), "0", searchEdit, pageIndex, PAGE_SIZE);
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_LIST_SEARCH), listMyCirclesLoader);
                        }
//                        if (isAdded() && getView() != null) {
//                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_LIST_SEARCH), listMyCirclesLoader);
//                        }
                    } else {
                        searchEditText.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                                // When user changed the Text
                                String text = searchEditText.getText().toString().toLowerCase(Locale.getDefault());
                            }

                            @Override
                            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                          int arg3) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void afterTextChanged(Editable arg0) {
                                // TODO Auto-generated method stub
                            }
                        });
                    }
                }
                return false;
            }
        });

        mCirclesListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem >= previousFirstVisibleItem) {
                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if ((lastInScreen == totalItemCount)) {

                        if (circlesListDbModels != null && circlesListDbModels.size() != 0) {
                            getCirclesListFromServer(false);
                        }
                    }
                    previousFirstVisibleItem = firstVisibleItem;
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_circles, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            case R.id.actionAdd:
                if (menu) {
                    if (CommonUtils.hasInternet()) {
                        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                        AddCircleFragment addCircleFragment = new AddCircleFragment();
                        Bundle bundle = new Bundle();
                        addCircleFragment.setArguments(bundle);
                        ft.replace(R.id.container_layout, addCircleFragment, AddCircleFragment.class.getSimpleName());
                        ft.addToBackStack(AddCircleFragment.class.getSimpleName());
                        ft.commit();
                        pageIndex = 0;
                        previousFirstVisibleItem = 0;
                        completeCirclesListDbModels.clear();
                        circlesListDbModels.clear();
                        //mCirclesListAdapter = new MyCirclesListAdapter(completeCirclesListDbModels);
                    } else {
                        showToast(R.string.error_no_network);
                    }
                } else {
                    showToast("You Have no access to add a circle");
                }
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(LIST_MY_CIRCLES_LOADER)) {

        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(LIST_MY_CIRCLES_LOADER)) {
            if (result != null && result instanceof ListMyCirclesResponse) {
                ListMyCirclesResponse listMyCirclesResponse = (ListMyCirclesResponse) result;

                // Separate System Circles & User Circles from the response
                if (listMyCirclesResponse.getCirclesDataObject() != null &&
                        listMyCirclesResponse.getCirclesDataObject().getCircleDetailsArrayList() != null &&
                        listMyCirclesResponse.getCirclesDataObject().getCircleDetailsArrayList().size() > 0) {
                    // Main Circles list - includes System Circles first and then User Circles next
                    ArrayList<ListMyCirclesResponse.CircleDetails> mainCirclesAL = listMyCirclesResponse.getCirclesDataObject().getCircleDetailsArrayList();
                    ArrayList<CirclesListDbModel> circlesListDbModels = new ArrayList<CirclesListDbModel>();

                    for (int i = 0; i < mainCirclesAL.size(); i++) {

                        ListMyCirclesResponse.CircleDetails indexedObj = mainCirclesAL.get(i);
                        CirclesListDbModel circlesListDbModel = new CirclesListDbModel();
                        try {
                            circleaName = CryptoEngine.encrypt(indexedObj.getCircleName());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            circlesListDbModel.setCircleName(circleaName);
                            circlesListDbModel.setIsProtected(indexedObj.isProtected());
                            circlesListDbModel.setMemberCount(indexedObj.getCircleMemberCount());
                            circlesListDbModel.setStartDate(indexedObj.getStartDate());
                            circlesListDbModel.setStopDate(indexedObj.getStopDate());
                            circlesListDbModel.setSystemCircleId(indexedObj.getSystemCircle_Id());
                            circlesListDbModel.setUserCircleId(indexedObj.getUserCircle_ID());
                            circlesListDbModel.setUserType(indexedObj.getUserType());
                            circlesListDbModel.setStopReasonDesc(indexedObj.getStopReasonDesc());
                            circlesListDbModel.setStopReasonSlu(indexedObj.getStopReason_SLU());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        circlesListDbModels.add(circlesListDbModel);
                    }

                    final CirclesListDBLoader circlesListDBLoader = new CirclesListDBLoader(mActivity, circlesListDbModels);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_CIRCLE_LIST_LOADER), circlesListDBLoader);
                    }
                } else {
                    if (completeCirclesListDbModels.size() == 0) {
                        showToast(R.string.circles_no_circles_found);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(LIST_MEMBERS_IN_CIRCLE_LOADER)) {
            if (result != null && result instanceof ListMembersInCircleResponse) {
                final ListMembersInCircleResponse listMembersInCircleResponse = (ListMembersInCircleResponse) result;

                if (listMembersInCircleResponse.getCircleMembersDataObject() != null &&
                        listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList() != null &&
                        listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList().size() > 0) {

                    ArrayList<ListMembersInCircleResponse.CircleMemberDetails> circleMemberDetailses = listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList();

                    circleMemberListDbModels = new ArrayList<CircleMemberListDbModel>();

                    for (int i = 0; i < circleMemberDetailses.size(); i++) {

                        ListMembersInCircleResponse.CircleMemberDetails indexedObj = circleMemberDetailses.get(i);
                        CircleMemberListDbModel circleMemberListDbModel = new CircleMemberListDbModel();

                        try {
                            circleMemberListDbModel.setUserId(indexedObj.getUserID());
                            circleMemberListDbModel.setForeName(indexedObj.getForeName());
                            circleMemberListDbModel.setSurName(indexedObj.getSurName());
                            circleMemberListDbModel.setUserCircleId(indexedObj.getUserCircle_ID());
                            circleMemberListDbModel.setUserMemberMail(indexedObj.getUserCircleMemberEmail_ID());
                            circleMemberListDbModel.setUserMemberId(indexedObj.getUserCircleMember_ID());
                            circleMemberListDbModel.setUserType(indexedObj.getUserType());
                            circleMemberListDbModel.setUserUrl(indexedObj.getURL());
                            circleMemberListDbModel.setUserMemberSpeciality(indexedObj.getSpeciality());
                            circleMemberListDbModel.setMemberRoleTypeSlu(indexedObj.getRoleType_SLU());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        circleMemberListDbModels.add(circleMemberListDbModel);
                    }
                    final CircleMemberListDbLoader circleMemberListDBLoader = new CircleMemberListDbLoader(mActivity, circleMemberListDbModels);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_CIRCLE_MEMBER_LIST_LOADER), circleMemberListDBLoader);
                    }
                } else {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                                final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                                CircleMembersListFragment circleMembersListFragment = new CircleMembersListFragment();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, selectedCirclesListDbModel.getUserCircleId());
                                bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLENAME, selectedCirclesListDbModel.getCircleName());
                                showToast(selectedCirclesListDbModel.getCircleName());

                                circleMembersListFragment.setArguments(bundle);
                                ft.replace(R.id.container_layout, circleMembersListFragment, CircleMembersListFragment.class.getSimpleName());
                                ft.addToBackStack(CircleMembersListFragment.class.getSimpleName());
                                ft.commit();
                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }
                    });

                }
            }
        } else if (id == getLoaderHelper().getLoaderId(REMOVE_CIRCLE_LOADER)) {
            if (result != null && result instanceof RemoveCircleResponse) {
                final RemoveCircleResponse baseResponseRemove = (RemoveCircleResponse) result;
                if (baseResponseRemove.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
//                            completeCirclesListDbModels.remove(selectedCirclesListDbModel);
//                            selectedCirclesListDbModel = null;
//                            mCirclesListAdapter.notifyDataSetChanged();
                            pageIndex = 0;
                            previousFirstVisibleItem = 0;
                            if (completeCirclesListDbModels != null && completeCirclesListDbModels.size() > 0 && circlesListDbModels != null && circlesListDbModels.size() > 0) {
                                completeCirclesListDbModels.clear();
                                circlesListDbModels.clear();
                            }
                            mCirclesListAdapter = new MyCirclesListAdapter(completeCirclesListDbModels);
                            mCirclesListView.setAdapter(mCirclesListAdapter);
                            if (!TextUtils.isEmpty(baseResponseRemove.getMessage())) {
                                showToast(baseResponseRemove.getMessage());
                            } else if (!TextUtils.isEmpty(baseResponseRemove.getData())) {
                                showToast(baseResponseRemove.getData());
                            }

                            getCirclesListFromServer(true);

//                            if (CommonUtils.hasInternet()) {
//                                displayProgressLoader(false);
//                                ListMyCirclesLoader listMyCirclesLoader = new ListMyCirclesLoader(mActivity, mActiveUser.getToken(), "0","",pageIndex,PAGE_SIZE);
//                                if (isAdded() && getView() != null) {
//                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MY_CIRCLES_LOADER), listMyCirclesLoader);
//                                }
//                            } else {
//                                showToast(R.string.error_no_network);
//                            }
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CIRCLE_LIST_SEARCH)) {
            if (result != null && result instanceof ListMyCirclesResponse) {
                ListMyCirclesResponse mMsgSentResponse = (ListMyCirclesResponse) result;

                if (mMsgSentResponse.getCirclesDataObject() != null) {
                    final ArrayList<ListMyCirclesResponse.CircleDetails> searchResultsDataObjectsAL = mMsgSentResponse.getCirclesDataObject().getCircleDetailsArrayList();

                    if (searchResultsDataObjectsAL != null && searchResultsDataObjectsAL.size() > 0) {
                        // update to list adapter with search items
                        if (mCirclesListAdapter != null) {
                            //mCirclesListAdapter.updateData(searchResultsDataObjectsAL);
                        }
                    } else {
                        showToast(R.string.no_search_results_found);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_CIRCLE_LIST_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                circlesListDbModels = (ArrayList<CirclesListDbModel>) result;

                if (circlesListDbModels != null) {
                    // Display Circles in ListView
                    // System Circles
                    ArrayList<CirclesListDbModel> systemCirclesAL = new ArrayList<CirclesListDbModel>();
                    // User Circles
                    ArrayList<CirclesListDbModel> userCirclesAL = new ArrayList<CirclesListDbModel>();

                    try {
                        // separate Pending & Accepted invitations from total invitations list
                        if (circlesListDbModels != null && circlesListDbModels.size() > 0) {
                            for (int idx = 0; idx < circlesListDbModels.size(); idx++) {
                                if (!TextUtils.isEmpty(circlesListDbModels.get(idx).getSystemCircleId()) &&
                                        circlesListDbModels.get(idx).getSystemCircleId().trim().equalsIgnoreCase("1")) {
                                    // its a System Circle
                                    systemCirclesAL.add(circlesListDbModels.get(idx));
                                } else {
                                    // its not
                                    // if we dont received SystemCircleID from circle, put empty for that
                                    circlesListDbModels.get(idx).setIsSystemCircle("");

                                    userCirclesAL.add(circlesListDbModels.get(idx));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ArrayList<CirclesListDbModel> circlesListDbModelsAL = new ArrayList<CirclesListDbModel>();
                    // Add System Circles first and then User Circles next
                    Collections.sort(systemCirclesAL, new CircleNameComparator());
                    //circlesListDbModelsAL.addAll(systemCirclesAL);
                    if (completeCirclesListDbModels == null) {
                        completeCirclesListDbModels = new ArrayList<CirclesListDbModel>();
                    }
                    //TODO: check and add system circles
                    completeCirclesListDbModels.addAll(0, systemCirclesAL);
                    Collections.sort(userCirclesAL, new CircleNameComparator());
                    circlesListDbModelsAL.addAll(userCirclesAL);
                    completeCirclesListDbModels.addAll(circlesListDbModelsAL);
                    // update to global object so that it will be used once user cancel the search
                    mCirclesDbModelAL = circlesListDbModelsAL;

                    if (mCirclesListAdapter == null) {
                        mCirclesListAdapter = new MyCirclesListAdapter(completeCirclesListDbModels);
                        mCirclesListView.setAdapter(mCirclesListAdapter);
                    } else {
                        if (mCirclesListView.getAdapter() == null)
                            mCirclesListView.setAdapter(mCirclesListAdapter);
                    }
                    mCirclesListAdapter.notifyDataSetChanged();
                } else {
                    if (completeCirclesListDbModels.size() == 0) {
                        showToast(R.string.circles_no_circles_found);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_CIRCLE_MEMBER_LIST_LOADER)) {
            circleMemberListDbModels = (ArrayList<CircleMemberListDbModel>) result;

            new Handler().post(new Runnable() {
                @Override
                public void run() {

                    try {
                        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                        CircleMembersListFragment circleMembersListFragment = new CircleMembersListFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, selectedCirclesListDbModel.getUserCircleId());
                        bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLENAME, selectedCirclesListDbModel.getCircleName());
                        circleMembersListFragment.setArguments(bundle);
                        ft.replace(R.id.container_layout, circleMembersListFragment, CircleMembersListFragment.class.getSimpleName());
                        ft.addToBackStack(CircleMembersListFragment.class.getSimpleName());
                        ft.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } else if (id == getLoaderHelper().getLoaderId(GET_CIRCLE_LIST_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                ArrayList<CirclesListDbModel> circlesListDbModels = (ArrayList<CirclesListDbModel>) result;

                if (circlesListDbModels != null) {
                    // Display Circles in ListView
                    // System Circles
                    ArrayList<CirclesListDbModel> systemCirclesAL = new ArrayList<CirclesListDbModel>();
                    // User Circles
                    ArrayList<CirclesListDbModel> userCirclesAL = new ArrayList<CirclesListDbModel>();

                    try {
                        // separate Pending & Accepted invitations from total invitations list
                        if (circlesListDbModels != null && circlesListDbModels.size() > 0) {
                            for (int idx = 0; idx < circlesListDbModels.size(); idx++) {
                                if (!TextUtils.isEmpty(circlesListDbModels.get(idx).getSystemCircleId()) &&
                                        circlesListDbModels.get(idx).getSystemCircleId().trim().equalsIgnoreCase("1")) {
                                    // its a System Circle
                                    systemCirclesAL.add(circlesListDbModels.get(idx));
                                } else {
                                    // its not
                                    // if we dont received SystemCircleID from circle, put empty for that
                                    circlesListDbModels.get(idx).setIsSystemCircle("");
                                    userCirclesAL.add(circlesListDbModels.get(idx));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ArrayList<CirclesListDbModel> circlesListDbModelsAL = new ArrayList<CirclesListDbModel>();

                    // Add System Circles first and then User Circles next
                    Collections.sort(systemCirclesAL, new CircleNameComparator());
                    circlesListDbModelsAL.addAll(systemCirclesAL);

                    Collections.sort(userCirclesAL, new CircleNameComparator());
                    circlesListDbModelsAL.addAll(userCirclesAL);

                    // update to global object so that it will be used once user cancel the search
                    mCirclesDbModelAL = circlesListDbModelsAL;

                    mCirclesListAdapter = new MyCirclesListAdapter(circlesListDbModelsAL);
                    mCirclesListView.setAdapter(mCirclesListAdapter);

                } else {
                    if (completeCirclesListDbModels.size() == 0) {
                        showToast(R.string.circles_no_circles_found);
                    }
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");

                if (mCirclesListAdapter != null) {
                    mCirclesListAdapter = new MyCirclesListAdapter(completeCirclesListDbModels);
                    mCirclesListView.setAdapter(mCirclesListAdapter);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    public class CircleNameComparator implements Comparator<CirclesListDbModel> {
        @Override
        public int compare(CirclesListDbModel obj1, CirclesListDbModel obj2) {
            return obj1.getCircleName().compareToIgnoreCase(obj2.getCircleName());
        }
    }

    private class MyCirclesListAdapter extends BaseAdapter {

        private ArrayList<CirclesListDbModel> mainCirclesAL;

        public MyCirclesListAdapter(ArrayList<CirclesListDbModel> mainCirclesAL) {
            this.mainCirclesAL = mainCirclesAL;
        }

        @Override
        public int getCount() {
            return mainCirclesAL != null && mainCirclesAL.size() > 0 ? mainCirclesAL.size() : 0;
        }

        @Override
        public CirclesListDbModel getItem(int position) {
            return mainCirclesAL.get(position);
        }

        public void updateData(ArrayList<CirclesListDbModel> mainCirclesAL) {
            this.mainCirclesAL = mainCirclesAL;
            notifyDataSetChanged();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final CirclesListDbModel rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.circles_list_row, viewGroup, false);
            }


            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            TextView circleNameTextView = (TextView) convertView.findViewById(R.id.circleNameTextView);
            circleNameTextView.setTypeface(regular);

            if (rowObject.getMemberCount() > 0) {
                try {
                    circledecrypt = CryptoEngine.decrypt(rowObject.getCircleName());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                circleNameTextView.setText(circledecrypt + " (" + rowObject.getMemberCount() + ")");
            } else {
                try {
                    circledecrypt = CryptoEngine.decrypt(rowObject.getCircleName());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                circleNameTextView.setText(circledecrypt);
            }
            try {
                if (rowObject.getSystemCircleId().equalsIgnoreCase("1")) {
                    // its System Circle
                    circleNameTextView.setTextColor(getResources().getColor(R.color.textcolor22d2c3));
                } else {
                    // its user circle
                    circleNameTextView.setTextColor(getResources().getColor(R.color.textcolor373d52));
                }
            } catch (Exception e) {
                e.printStackTrace();

            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                    selectedCirclesListDbModel = rowObject;
                    pageIndex = 0;
                    previousFirstVisibleItem = 0;
                    if (completeCirclesListDbModels != null && completeCirclesListDbModels.size() > 0 && circlesListDbModels != null && circlesListDbModels.size() > 0) {
                        completeCirclesListDbModels.clear();
                        circlesListDbModels.clear();
                    }
                    //mCirclesListAdapter = new MyCirclesListAdapter(completeCirclesListDbModels);
                    try {
                        if (selectedCirclesListDbModel.getSystemCircleId().equalsIgnoreCase("1")) {
                            final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                            final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                            CircleMembersListFragment circleMembersListFragment = new CircleMembersListFragment();
                            Bundle bundle = new Bundle();
                            circleaName = CryptoEngine.decrypt(selectedCirclesListDbModel.getCircleName());
                            bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, selectedCirclesListDbModel.getUserCircleId());
                            bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLENAME, circleaName);
                            bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLETYPE, AxSysConstants.EXTRA_SELECTED_CIRCLE_SYSTEMCIRCLE);
                            circleMembersListFragment.setArguments(bundle);
                            ft.replace(R.id.container_layout, circleMembersListFragment, CircleMembersListFragment.class.getSimpleName());
                            ft.addToBackStack(CircleMembersListFragment.class.getSimpleName());
                            ft.commit();
                        } else {
                            final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                            final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                            CircleMembersListFragment circleMembersListFragment = new CircleMembersListFragment();
                            Bundle bundle = new Bundle();
                            circleaName = CryptoEngine.decrypt(selectedCirclesListDbModel.getCircleName());
                            bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, selectedCirclesListDbModel.getUserCircleId());
                            bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLENAME, circleaName);
                            bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLETYPE, AxSysConstants.EXTRA_SELECTED_CIRCLE_CARECIRCLE);
                            circleMembersListFragment.setArguments(bundle);
                            ft.replace(R.id.container_layout, circleMembersListFragment, CircleMembersListFragment.class.getSimpleName());
                            ft.addToBackStack(CircleMembersListFragment.class.getSimpleName());
                            ft.commit();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                                                   @Override
                                                   public boolean onLongClick(View view) {
                                                       // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                                                       selectedCirclesListDbModel = rowObject;

                                                       try {
                                                           systemCircleId = rowObject.getSystemCircleId();
                                                       } catch (Exception e) {
                                                           e.printStackTrace();

                                                       }
                                                       if (!systemCircleId.equalsIgnoreCase("1")) {
                                                           // Allow user to delete only user circles

                                                           if (CommonUtils.hasInternet()) {

                                                               AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                               alert.setTitle(getString(R.string.text_alert));
                                                               alert.setMessage(getString(R.string.confirmation_to_delete_circle));
                                                               alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                                                                   @Override
                                                                   public void onClick(DialogInterface dialog, int which) {

                                                                       try {
                                                                           if (CommonUtils.hasInternet()) {
                                                                               displayProgressLoader(false);
                                                                               RemoveCircleLoader removeCircleLoader = new RemoveCircleLoader(mActivity, mActiveUser.getToken(),
                                                                                       rowObject.getUserCircleId(), rowObject.getStopReasonDesc(), rowObject.getStopReasonSlu());
                                                                               if (mFragment.isAdded() && mFragment.getView() != null) {
                                                                                   getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REMOVE_CIRCLE_LOADER), removeCircleLoader);
                                                                               }
                                                                           } else {
                                                                               showToast(R.string.error_no_network);
                                                                           }
                                                                       } catch (Exception e) {
                                                                           e.printStackTrace();

                                                                       }
                                                                   }
                                                               });
                                                               alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {

                                                                   @Override
                                                                   public void onClick(DialogInterface dialog, int which) {

                                                                       dialog.dismiss();
                                                                   }
                                                               });

                                                               alert.show();
                                                           } else {

                                                           }

                                                           return false;
                                                       } else

                                                       {
                                                           return true;
                                                       }
                                                   }
                                               }
            );
            return convertView;
        }
    }
}
