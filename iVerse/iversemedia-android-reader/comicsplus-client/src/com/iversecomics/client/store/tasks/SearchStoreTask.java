package com.iversecomics.client.store.tasks;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.db.OrderingUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class SearchStoreTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(SearchStoreTask.class);
    private String              query;
    private String              listId;

    public SearchStoreTask(ComicStore comicStore, String query) {
        super(comicStore);
        this.query = query;
        this.listId = ListID.SEARCH;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SearchStoreTask other = (SearchStoreTask) obj;
        if (listId == null) {
            if (other.listId != null) {
                return false;
            }
        } else if (!listId.equals(other.listId)) {
            return false;
        }
        if (query == null) {
            if (other.query != null) {
                return false;
            }
        } else if (!query.equals(other.query)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getComicsSearch(query);

            // Update from new results.
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();

            // For the purposes of the DB, this is intentionally a simple ListID (with no query terms)
            // This limits the DB to only holding 1 set of search results at a time.
            updater.addComicDBSubUpdater(new OrderingUpdater(ListID.SEARCH));

            ResponseParser parser = new ResponseParser();
            parser.parseComics(json, updater);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to search comics with query: " + query);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listId == null) ? 0 : listId.hashCode());
        result = prime * result + ((query == null) ? 0 : query.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        // Search results are not stored in Freshness DB
        return false;
    }
}