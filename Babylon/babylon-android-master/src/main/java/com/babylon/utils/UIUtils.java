package com.babylon.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.babylon.model.BaseModel;
import com.babylon.model.Icon;
import com.babylon.view.CustomEditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class UIUtils {
    private static final String TAG = "UIUtils";

    public static float getFloat(CustomEditText editText) {
        final String text = editText.getEditText().getText().toString();

        return BaseModel.getFloat(text);
    }

    public static void showSimpleToast(Context context, int resId) {
        if (context != null) {
            Toast toast = Toast.makeText(context, resId, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        }
    }

    public static void showSimpleToast(Context context, int resId, int duration) {
        if (context != null) {
            Toast toast = Toast.makeText(context, resId, duration);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        }
    }

    public static void showSimpleToast(Context context, String msg) {
        if (context != null && msg != null) {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        }
    }

    public static void hideSoftKeyboard(final Activity activity, final EditText editText) {
        if (activity != null && editText != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (imm != null && editText.getWindowToken() != null) {
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.RESULT_SHOWN, 0);
    }

    public static String getAppVersion(Context context) {
        String curentVersion = "";
        try {
            curentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            L.e(TAG, "Error get app version", e);
        }
        return curentVersion;
    }

    public static boolean isApplicationRuning(Context context) {
        if (context != null) {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningTaskInfo> tasks = am.getRunningTasks(1);
            if (!tasks.isEmpty()) {
                ComponentName topActivity = tasks.get(0).topActivity;
                if (topActivity.getPackageName().equals(context.getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int getDpFromPx(Context context, int pixels) {
        final float d = context.getResources().getDisplayMetrics().density;
        return (int) (pixels / d);
    }

    public static int getPxFromDp(Context context, int dp) {
        final float d = context.getResources().getDisplayMetrics().density;
        return (int) (dp * d);
    }

    public static float round(float unrounded, int precision, int roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.floatValue();
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        int x = a % b;
        return gcd(b, x);
    }

    public static long getMemorySize(int type) {
        try {
            Runtime info = Runtime.getRuntime();
            switch (type) {
                case 1:
                    return info.freeMemory();
                case 2:
                    return info.totalMemory() - info.freeMemory();
                case 3:
                    return info.totalMemory();
                default:
                    break;
            }
        } catch (Exception e) {
            L.e(TAG, e.getMessage(), e);
        }
        return 0;
    }

    public static boolean isOnline(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static boolean isWiFi(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static String formatAmount(int num) {
        DecimalFormat decimalFormat = new DecimalFormat();
        DecimalFormatSymbols decimalFormateSymbol = new DecimalFormatSymbols();
        decimalFormateSymbol.setGroupingSeparator(',');
        decimalFormat.setDecimalFormatSymbols(decimalFormateSymbol);
        return decimalFormat.format(num);
    }

    public static void setFakeAlpha(View view, float alpha) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(alpha, alpha);
        alphaAnimation.setDuration(0); // Make animation instant
        alphaAnimation.setFillAfter(true); // Tell it to persist after the animation ends
        view.startAnimation(alphaAnimation);
    }

    public static void unSetFakeAlpha(View view) {
        view.clearAnimation();
    }

    @SuppressLint("NewApi")
    public static boolean copyToClipboard(Context context, String text) {
        try {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
                return true;
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Opinion", text);
                clipboard.setPrimaryClip(clip);
                return true;
            }
        } catch (Exception e) {
            L.e(TAG, e.getMessage(), e);
            return false;
        }
    }

    public static String getDeviceId(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public static String getNumbersOnly(CharSequence s) {
        return s.toString().replaceAll("[^0-9]", "");
    }

    public static void logCursor(Cursor c) {
        if (c != null) {
            L.e("TAG", "Cursor item: " + c.getCount());
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    L.d("TAG", str);
                } while (c.moveToNext());
            }
        } else {
            L.d("TAG", "Cursor is null");
        }
    }

    public static String getSuperscriptStringFromUnicode(String string) {
        string = string.replaceAll("u2074", "<sup> 4</sup>");
        string = string.replaceAll("u2075", "<sup> 5</sup>");
        string = string.replaceAll("u2076", "<sup> 6</sup>");
        string = string.replaceAll("u2077", "<sup> 7</sup>");
        string = string.replaceAll("u2078", "<sup> 8</sup>");
        string = string.replaceAll("u2079", "<sup> 9</sup>");
        return string;
    }

    public static String getSuperscriptStringFromHtmlCodes(String string) {
        string = string.replaceAll("&#8304;", "<sup>0</sup>");
        string = string.replaceAll("&#8305;", "<sup>1</sup>");
        string = string.replaceAll("&#8306;", "<sup>2</sup>");
        string = string.replaceAll("&#8307;", "<sup>3</sup>");
        string = string.replaceAll("&#8308;", "<sup>4</sup>");
        string = string.replaceAll("&#8309;", "<sup>5</sup>");
        string = string.replaceAll("&#8310;", "<sup>6</sup>");
        string = string.replaceAll("&#8311;", "<sup>7</sup>");
        string = string.replaceAll("&#8312;", "<sup>8</sup>");
        string = string.replaceAll("&#8313;", "<sup>9</sup>");
        return string;
    }

    public static void setRadioGroupClickable(RadioGroup radioGroup, boolean isClickable) {
        if (radioGroup != null) {
            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                radioGroup.getChildAt(i).setEnabled(isClickable);
            }
        }
    }

    public static float getKilometers(float distanceInMeter) {
        return (float) Math.floor(distanceInMeter / Constants.KILOMETER_IN_METERS);
    }

    public static float getMeters(float distanceInMeter) {
        float kilometers = (float) Math.floor(distanceInMeter / Constants.KILOMETER_IN_METERS);
        return (float) (distanceInMeter - Constants.KILOMETER_IN_METERS * kilometers);
    }

    public static int getLongFromKilometersAndMeters(int kilometers, int meters) {
        return (int) (kilometers * Constants.KILOMETER_IN_METERS + meters);
    }

    public static String  getImageUrl(List<Icon> icons, Context context) {
        String imageUrl = null;
        Map<String, String> imageUrls = new HashMap<String, String>();
        for (Icon icon : icons) {
            if (icon.getType() != null && icon.getSrc() != null) {
                imageUrls.put(icon.getType(), icon.getSrc());
            }
        }
        if (!imageUrls.isEmpty()) {
            switch (context.getResources().getDisplayMetrics().densityDpi) {
                case DisplayMetrics.DENSITY_XHIGH:
                    imageUrl = imageUrls.get("xhdpi");
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    imageUrl = imageUrls.get("xxhdpi");
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH:
                    imageUrl = imageUrls.get("xxxhdpi");
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                default:
                    imageUrl = imageUrls.get("hdpi");
                    break;
            }
            if (imageUrl == null) {
                imageUrl = imageUrls.get("xhdpi");
                if (imageUrl == null) {
                    imageUrl = imageUrls.get("hdpi");
                }
            }
        }
        return imageUrl;
    }

    public static int getAge(long birthDate) {
        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTimeInMillis(birthDate);
        int age = y - calendar.get(Calendar.YEAR);

        boolean smallerCurrentMonth = m < calendar.get(Calendar.MONTH);
        boolean smallerCurrentDay = d < calendar.get(Calendar.DAY_OF_MONTH);
        boolean isCurrentMonth = (m == calendar.get(Calendar.MONTH));

        if (smallerCurrentMonth || (isCurrentMonth && smallerCurrentDay)) {
            --age;
        }
        return age;
    }

    public static ProgressDialog getLoadingDialog(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        progressDialog.setMessage("Loading");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        return progressDialog;
    }
}
