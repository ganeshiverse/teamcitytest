package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPatch;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Appointment;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.http.message.BasicHeader;

public class AppointmentPatch extends AbsRequestPatch<Appointment, Appointment> {

    private Integer entryId;

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    public AbsRequestPatch<Appointment, Appointment> withEntry(Appointment entry) {
        entryId = entry.getId();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("appointment", getGson().toJsonTree(entry));
        jsonBody = jsonObject.toString();
        return this;
    }

    public AbsRequestPatch<Appointment, Appointment> withEntry(Appointment entry, String[] fieldsToPatch) {
        entryId = entry.getId();
        JsonObject jsonObject = new JsonObject();
        JsonElement appointmentElement = getGson().toJsonTree(entry);
        JsonObject appointmentObject = appointmentElement.getAsJsonObject();
        JsonObject filteredObject = new JsonObject();
        //Copy properties from the 'full' object that match the field arguments into the 'filtered' object
        for(String fieldName : fieldsToPatch) {
            filteredObject.add(fieldName, appointmentObject.get(fieldName));
        }
        jsonObject.add("appointment", filteredObject);
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(String.format(Urls.PATCH, entryId));
    }

    @Override
    protected AbsResponseProcessor<Appointment> getResponseProcessor() {
        return new RubyResponseProcessor<Appointment>(getOutputType());
    }
}
