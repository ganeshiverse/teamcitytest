package com.babylon.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.R;
import com.babylon.broadcast.SyncFinishedBroadcast;
import com.babylon.model.Patient;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;

public class MonitorMeProfileActivity extends ProfileActivity {

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, MonitorMeProfileActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.babylon.ACTION_SYNC_FINISHED");
        registerReceiver(syncFinishedBroadcastReceiver, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(syncFinishedBroadcastReceiver);
    }

    @Override
    public ProfileField[] getFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.HEIGHT,
                ProfileField.WEIGHT,
                ProfileField.GENDER,
                ProfileField.DATE_OF_BIRTH};
    }

    @Override
    public ProfileField[] getRequiredFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.HEIGHT,
                ProfileField.WEIGHT,
                ProfileField.GENDER,
                ProfileField.DATE_OF_BIRTH};
    }

    @Override
    public void setTopBar() {
        setTitle(R.string.monitor_me_fragment_title);
        initializeTopBar();
    }

    @Override
    public int getLayoutResID() {
        return R.layout.activity_monitor_me_profile;
    }

    @Override
    public int getBackgroundResId() {
        return R.drawable.monitor_me_background;
    }

    @Override
    public void onProfileSent() {
        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PATIENT,
                SyncAdapter.SYNC_PUSH);
        setProgressBarVisible(true);
    }

    public static boolean isProfileValid() {
        final Patient patient = App.getInstance().getPatient();

        boolean result = true;
            if(patient!=null) {
                if (patient.getWeight() == 0 || patient.getHeight() == 0 || TextUtils.isEmpty(patient.getGender()) ||
                        patient.getBirthday() == null) {
                    result = false;
                }
            }
             else{
                result=false;
            }

        return result;
    }

    private final BroadcastReceiver syncFinishedBroadcastReceiver = new SyncFinishedBroadcast() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            final int action = intent.getIntExtra(SyncAdapter.KEY_SYNC_ACTION, -1);
            final String type = intent.getStringExtra(SyncAdapter.KEY_SYNC_TYPE);

            if ((type.equals(Constants.Sync_Keys.SYNC_PATIENT)
                    && (action == SyncAdapter.SYNC_BOTH || action == SyncAdapter.SYNC_PUSH))) {
                finish();
               // ShopCategoriesActivity.startActivity(MonitorMeProfileActivity.this);
                MonitorMeActivity.startActivity(MonitorMeProfileActivity.this);
            }
        }
    };
}
