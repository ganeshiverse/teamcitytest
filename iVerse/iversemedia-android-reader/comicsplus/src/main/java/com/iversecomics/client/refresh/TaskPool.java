package com.iversecomics.client.refresh;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TaskPool {
    static class HashedThreadExecutor extends ThreadPoolExecutor {
        private static final int MAX_THREADS = 3;
        private RecentHashes recentHashes;

        public HashedThreadExecutor() {
            super(MAX_THREADS, MAX_THREADS, 0L, TimeUnit.MILLISECONDS, new HashedBlockingQueue(),
                    new DiscardPolicy());
            recentHashes = new RecentHashes();
        }

        public Future<?> submitHashed(final Task task, final TaskCallback callback) {
            /*
            if (recentHashes.hasSeen(task.hashCode())) {
                LOG.debug("Not adding recently seen task: %s", task);
                return null;
            }
            */
            HashedFutureTask future = new HashedFutureTask(task, callback);
            LOG.debug("Submitting task: %s (to %s @%x)", task, this.getClass().getSimpleName(), this.hashCode());
            execute(future);
            return future;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(TaskPool.class);
    private HashedThreadExecutor executor;

    public TaskPool() {
        executor = new HashedThreadExecutor();
    }

    public Future<?> submit(Task task) {
        return submit(task, null);
    }

    public Future<?> submit(Task task, TaskCallback callback) {
        Future<?> future = null;
        if (task instanceof TaskCollection) {
            TaskCollection tcoll = (TaskCollection) task;
            for (Task t : tcoll.getTasks()) {
                future = executor.submitHashed(t, callback);
            }
        } else {
            future = executor.submitHashed(task, callback);
        }
        return future;
    }

    /**
     * Similar to {@link #submit(com.iversecomics.client.refresh.Task)} but checks the {@link com.iversecomics.client.refresh.IRefreshable#isFresh(com.iversecomics.client.refresh.Freshness)} and submits only those
     * tasks that are expired (not fresh).
     *
     * @param task the task to submit
     * @param
     * @return possible Future of the submitted task, null if not submitted.
     */
    public Future<?> submitIfExpired(Freshness freshness, Task task) {
        return submitIfExpired(freshness, task, null);
    }

    /**
     * Similar to {@link #submit(com.iversecomics.client.refresh.Task)} but checks the {@link com.iversecomics.client.refresh.IRefreshable#isFresh(com.iversecomics.client.refresh.Freshness)} and submits only those
     * tasks that are expired (not fresh).
     *
     * @param task     the task to submit
     * @param callback callback is invoked upon completion of the task, or immediately if task is fresh. It will
     *                 be invoked on the UI thread.
     * @return possible Future of the submitted task, null if not submitted.
     */
    public Future<?> submitIfExpired(Freshness freshness, Task task, final TaskCallback callback) {
        if (task instanceof TaskCollection) {
            TaskCollection tcoll = (TaskCollection) task;
            Future<?> future = null;
            for (Task t : tcoll.getTasks()) {
                future = submitIfExpired(freshness, t, callback);
            }
            return future;
        } else {
            if (task.isFresh(freshness)) {
                LOG.debug("Not submitting fresh task: %s", task);
                //Invoke callback immediately.
                if (callback != null) {
                    HashedFutureTask.HANDLER.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onTaskCompleted(null);
                        }
                    });
                }
                return null;
            } else {
                LOG.debug("Submitting task: %s", task);
                return submit(task, callback);
            }
        }
    }
}
