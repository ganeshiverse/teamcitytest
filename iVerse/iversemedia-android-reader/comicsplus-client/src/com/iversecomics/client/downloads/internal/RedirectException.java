package com.iversecomics.client.downloads.internal;


import org.apache.http.StatusLine;

import android.net.Uri;

/**
 * A Redirect occurred.
 */
public class RedirectException extends DownloadException {
    private static final long serialVersionUID = 4529235072008469852L;
    private Uri               location;

    public RedirectException(StatusLine redirectStatus, Uri location) {
        super(redirectStatus);
        this.location = location;
    }

    public Uri getLocation() {
        return location;
    }
}
