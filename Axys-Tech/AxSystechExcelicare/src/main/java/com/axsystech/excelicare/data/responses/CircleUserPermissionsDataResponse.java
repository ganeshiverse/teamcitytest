package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alanka on 10/12/2015.
 */
public class CircleUserPermissionsDataResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private DataObjectUserPermissions dataObject;

    public DataObjectUserPermissions getDataObject() {
        return dataObject;
    }

    public class DataObjectUserPermissions implements Serializable {

        @SerializedName("Circle_ID")
        private String circle_ID;

        @SerializedName("ModuleList")
        private ArrayList<ModuleList> moduleListsAl;

        @SerializedName("PermissionToModuleList")
        private ArrayList<ModuleList> permissionToModuleListsAl;

        public String getCircle_ID() {
            return circle_ID;
        }

        public ArrayList<ModuleList> getModuleListsAl() {
            return moduleListsAl;
        }

        public ArrayList<ModuleList> getPermissionToModuleListsAl() {
            return permissionToModuleListsAl;
        }
    }

    //ModuleList Array In data
    public class ModuleList implements Serializable {

        @SerializedName("Area")
        private ArrayList<AreaModuleList> areaModuleListAl;

        @SerializedName("ModuleID")
        private String moduleID;

        @SerializedName("ModuleName")
        private String moduleName;

        @SerializedName("ModuleType")
        private String moduleType;

        @SerializedName("ModuleType_SLU")
        private String moduleType_SLU;

        @SerializedName("SysAccessGroupItemID")
        private String sysAccessGroupItemID;

        public ArrayList<AreaModuleList> getAreaModuleListAl() {
            return areaModuleListAl;
        }

        public String getModuleID() {
            return moduleID;
        }

        public String getModuleName() {
            return moduleName;
        }

        public String getModuleType() {
            return moduleType;
        }

        public String getModuleType_SLU() {
            return moduleType_SLU;
        }

        public String getSysAccessGroupItemID() {
            return sysAccessGroupItemID;
        }
    }

    //Area Array for the ModuleList
    public class AreaModuleList implements Serializable {

        @SerializedName("AreaName")
        private String areaName;

        @SerializedName("SysAccessGroupItemID")
        private String sysAccessGroupitemID;

        @SerializedName("Groups")
        private ArrayList<GroupsModuleList> groupsModuleListAl;

        public String getAreaName() {
            return areaName;
        }

        public String getSysAccessGroupitemID() {
            return sysAccessGroupitemID;
        }

        public ArrayList<GroupsModuleList> getGroupsModuleListAl() {
            return groupsModuleListAl;
        }
    }

    //Groups Array for the ModuleList
    public class GroupsModuleList implements Serializable {

        @SerializedName("GroupName")
        private String groupName;

        @SerializedName("Permissions")
        private ArrayList<PermissionsModuleList> permissionsModuleListAL;

        public String getGroupName() {
            return groupName;
        }

        public ArrayList<PermissionsModuleList> getPermissionsModuleListAL() {
            return permissionsModuleListAL;
        }
    }

    //Permissions Array for the ModuleList
    public class PermissionsModuleList implements Serializable {

        @SerializedName("DefaultValue")
        private String defaultValue;

        @SerializedName("PermissionName")
        private String permissionName;

        public String getDefaultValue() {
            return defaultValue;
        }

        public String getPermissionName() {
            return permissionName;
        }
    }
}
