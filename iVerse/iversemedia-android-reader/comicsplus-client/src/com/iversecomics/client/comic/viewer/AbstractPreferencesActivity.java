package com.iversecomics.client.comic.viewer;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.iversecomics.client.R;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;

public abstract class AbstractPreferencesActivity extends Activity {    // implements OnTransitionListener {
//    private static final String TAG                          = "AbstractComicPreferencesActivity";
//    private static final int    ANIM_STEPS                   = 6;
//    private static final int    ANIM_FRAME_DURATION          = 200;
//    private static final int    PING_PONG_DELAY              = 1000;
//    private static final int    DIALOG_INVALID_EMAIL_ADDRESS = 1;
    private static final int    SAVE                         = 1;
    private static final int    CANCEL                       = 2;
//    private TransitionView      slideView;
//    private TransitionView      fadeView;
//    private TransitionView      deckView;
//    private Handler             handler;
//    private TransitionType      selectedTransition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setFullscreen();

       // setContentView(R.layout.preferences);

//        EditText textEmail = (EditText) findViewById(R.id.email);
        //CheckBox checkPanelNums = (CheckBox) findViewById(R.id.transition_panel_nums);

        SharedPreferences settings = ComicPreferences.getPrivatePrefs(this);
//        String email = settings.getString(ComicPreferences.PREF_EMAIL, "");
        boolean visiblePanelNums = settings.getBoolean(ComicPreferences.PANEL_NUMS, false);
//        String transitionId = settings.getString(ComicPreferences.PREF_TRANSITION, "slide");
//        TransitionType transition = TransitionType.getTransitionType(transitionId);

//        textEmail.setText(email);
       // checkPanelNums.setChecked(visiblePanelNums);

//        handler = new Handler();
//        Drawable focusedDrawable = getResources().getDrawable(R.drawable.btn_default_selected);
//        Drawable normalDrawable = getResources().getDrawable(R.drawable.btn_default_normal);
//        RadioBackgroundToggler transitionToggle = new RadioBackgroundToggler(normalDrawable, focusedDrawable);

        // Slide Transition
//        slideView = setupTransitionView(R.id.slide_anim, new SlideAnimation(false), R.id.slide_layout, transitionToggle,
//                new LinearLayout.OnClickListener() {
//                    public void onClick(final View v) {
//                        onClickSlide();
//                    }
//                });
//
//        // Fade Transition
//        fadeView = setupTransitionView(R.id.fade_anim, new FadeAnimation(false), R.id.fade_layout, transitionToggle,
//                new LinearLayout.OnClickListener() {
//                    public void onClick(final View v) {
//                        onClickFade();
//                    }
//                });
//
//        // Deck Transition
//        deckView = setupTransitionView(R.id.deck_anim, new DeckAnimation(false), R.id.deck_layout, transitionToggle,
//                new LinearLayout.OnClickListener() {
//                    public void onClick(final View v) {
//                        onClickDeck();
//                    }
//                });
//
//        switch (transition) {
//            case FADE:
//                transitionToggle.selectView(findViewById(R.id.fade_layout));
//                break;
//            case DECK:
//                transitionToggle.selectView(findViewById(R.id.deck_layout));
//                break;
//            case SLIDE:
//            default:
//                transitionToggle.selectView(findViewById(R.id.slide_layout));
//                break;
//        }

//        final Button btnSave = (Button) findViewById(R.id.save);
//        final Button btnCancel = (Button) findViewById(R.id.cancel);
//
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            public void onClick(final View v) {
//                save();
//            }
//        });
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            public void onClick(final View v) {
//                cancel();
//            }
//        });
    }

//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case DIALOG_INVALID_EMAIL_ADDRESS:
//                AlertDialog.Builder dlg = new AlertDialog.Builder(this);
//                dlg.setTitle(R.string.preferences_not_saved);
//                dlg.setIcon(android.R.drawable.ic_dialog_alert);
//                dlg.setMessage(R.string.email_address_invalid);
//                dlg.setCancelable(false);
//                dlg.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        /* do nothing */
//                    }
//                });
//                return dlg.create();
//        }
//        return super.onCreateDialog(id);
//    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);

        //final MenuItem saveMenu = menu.add(0, SAVE, 0, R.string.save);
        final MenuItem cancelMenu = menu.add(0, CANCEL, 1, R.string.cancel);

        //saveMenu.setIcon(android.R.drawable.ic_menu_save);
        cancelMenu.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case SAVE:
                save();
                break;
            case CANCEL:
                cancel();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

//    private TransitionView setupTransitionView(final int viewId, final TransitionAnimation anim, final int layoutId,
//            RadioBackgroundToggler transitionToggle, final LinearLayout.OnClickListener onClickListener) {
//        // Fetch TransitionView
//        TransitionView tv = (TransitionView) findViewById(viewId);
//
//        // Setup TransitionView
//        tv.setPanelProvider(new SamplePanelProvider(getResources()));
//        anim.setVisiblePanelNumber(false);
//        tv.setTransitionAnimation(anim);
//        tv.setTiming(new StepsTiming(ANIM_STEPS, ANIM_FRAME_DURATION));
//        tv.setOnTransitionListener(this);
//
//        // Fetch Transition Layout
//        LinearLayout tvLayout = (LinearLayout) findViewById(layoutId);
//        transitionToggle.addView(tvLayout);
//
//        tvLayout.setOnClickListener(onClickListener);
//
//        tvLayout.setFocusable(true);
//        tvLayout.setOnTouchListener(transitionToggle);
//        tvLayout.setOnFocusChangeListener(transitionToggle);
//
//        return tv;
//    }
//
//    private void onClickDeck() {
//        Log.i(TAG, "Click - Deck Layout");
//        selectedTransition = TransitionType.DECK;
//    }
//
//    private void onClickFade() {
//        Log.i(TAG, "Click - Fade Layout");
//        selectedTransition = TransitionType.FADE;
//    }
//
//    private void onClickSlide() {
//        Log.i(TAG, "Click - Slide Layout");
//        selectedTransition = TransitionType.SLIDE;
//    }

    private void setFullscreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    protected void save() {
        SharedPreferences settings = ComicPreferences.getPrivatePrefs(this);
//        String email = "";

//        EditText emailEdit = (EditText) findViewById(R.id.email);
//        if (emailEdit != null) {
//            if (emailEdit.getText() != null) {
//                email = emailEdit.getText().toString().trim();
//                if (StringUtils.isNoBlank(email) && !EmailValidator.getInstance().isValid(email)) {
//                    showDialog(DIALOG_INVALID_EMAIL_ADDRESS);
//                    return;
//                }
//            }
//        }

//        if (selectedTransition == null) {
//            // set default.
//            selectedTransition = TransitionType.SLIDE;
//        }

        //CheckBox checkPanelNums = (CheckBox) findViewById(R.id.transition_panel_nums);

        final Editor editor = settings.edit();
//        editor.putString(ComicPreferences.PREF_EMAIL, email);
//        editor.putString(ComicPreferences.PREF_TRANSITION, selectedTransition.getName());
        //editor.putBoolean(ComicPreferences.PANEL_NUMS, checkPanelNums.isChecked());

        if (!ComicPreferences.save(editor)) {
            Toast.makeText(this, "Unable to save preferences", Toast.LENGTH_SHORT).show();
        }

        setResult(RESULT_OK);
        finish();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        slideView.start();
//        fadeView.start();
//        deckView.start();
//
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                // Start Ping Pong
//                slideView.showNext();
//                fadeView.showNext();
//                deckView.showNext();
//            }
//        }, PING_PONG_DELAY);
//    }

//    @Override
//    protected void onResume() {
//        final Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            EditText emailEdit = (EditText) findViewById(R.id.email);
//            emailEdit.requestFocus();
            
//            handler.postDelayed(new Runnable() {
//                public void run() {
//                    View registrationView = findViewById(R.id.layout_registration);
//                    String scroll = extras.getString("scroll");
//                    if ("email".equals(scroll)) {
//                        ScrollView scrollView = (ScrollView) findViewById(R.id.scroll_layout);
//                        Point p = getPoint(registrationView, scrollView);
//                        scrollView.smoothScrollTo(p.x, p.y);
//                    }
//                }
//            }, 250);
//        }
//
//        super.onResume();
//    }

    /**
     * Calculate the point of the child view in relation to a specific parent view.
     * 
     * @param childView
     * @param parentView
     * @return
     */
//    private Point getPoint(View childView, ViewParent parentView) {
//        Point p = new Point();
//        p.x = childView.getLeft() + childView.getPaddingLeft();
//        p.y = childView.getTop() + childView.getPaddingLeft();
//
//        ViewParent vp = childView.getParent();
//        while (vp != null && !vp.equals(parentView)) {
//            if (vp instanceof View) {
//                View v = (View) vp;
//                p.x += v.getLeft();
//                p.y += v.getTop();
//            }
//            if (vp instanceof ViewGroup) {
//                ViewGroup vg = (ViewGroup) vp;
//                if (vg.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
//                    ViewGroup.MarginLayoutParams marginparams = (MarginLayoutParams) vg.getLayoutParams();
//                    p.x += marginparams.leftMargin;
//                    p.y += marginparams.topMargin;
//                }
//                p.x += vg.getPaddingLeft();
//                p.y += vg.getPaddingTop();
//            }
//            vp = vp.getParent();
//        }
//
//        return p;
//    }

//    @Override
//    protected void onStop() {
//        slideView.stop();
//        fadeView.stop();
//        deckView.stop();
//
//        super.onStop();
//    }
//    
//    public void onFinishedAllPanels(TransitionView view) {
//        /* do nothing */
//    }
//
//    public void onTransitionEnd(TransitionView view) {
//        /* do nothing */
//    }
//
//    public void onTransitionNextComplete(final TransitionView transitionView) {
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                // Ping Pong!
//                transitionView.showPrevious();
//            }
//        }, PING_PONG_DELAY);
//    }
//
//    public void onTransitionPreviousComplete(final TransitionView transitionView) {
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                // Ping Pong!
//                transitionView.showNext();
//            }
//        }, PING_PONG_DELAY);
//    }

}
