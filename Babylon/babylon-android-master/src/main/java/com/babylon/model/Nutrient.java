package com.babylon.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Nutrient implements Serializable {

    private Integer id;

    private String name;

    @SerializedName("kcal")
    private Integer kilocalories;

    private Float portionWeight;

    private Float addPortionWeight;

    private String portionDetails;

    private Date date;

    private Integer count;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKilocalories() {
        return kilocalories;
    }

    public void setKilocalories(Integer kilocalories) {
        this.kilocalories = kilocalories;
    }

    public Float getPortionWeight() {
        return portionWeight;
    }

    public void setPortionWeight(Float portionWeight) {
        this.portionWeight = portionWeight;
    }

    public Float getAddPortionWeight() {
        return addPortionWeight;
    }

    public void setAddPortionWeight(Float addPortionWeight) {
        this.addPortionWeight = addPortionWeight;
    }

    public String getPortionDetails() {
        return portionDetails;
    }

    public void setPortionDetails(String portionDetails) {
        this.portionDetails = portionDetails;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setFoodFromNutrient(Food food) {
        food.setNutrientId(id);
        food.setCalories(kilocalories);
        food.setTitle(name);
        food.setPortionDetails(portionDetails);
        food.setPortionWeight(portionWeight);
        food.setAddPortionWeight(addPortionWeight);
    }

    public static class NutrientServer extends Food {
        private Nutrient nutrient;
        private Integer  id;

        public Nutrient getNutrient() {
            return nutrient;
        }

        public Integer  getId() {
            return id;
        }

        public void setId(Integer  id) {
            this.id = id;
        }
    }
}
