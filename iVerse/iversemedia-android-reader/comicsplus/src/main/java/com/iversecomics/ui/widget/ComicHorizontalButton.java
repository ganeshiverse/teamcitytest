package com.iversecomics.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.iversecomics.archie.android.R;
import com.iversecomics.util.ui.HorizontalBitmapSegmentDrawable;
import com.iversecomics.util.ui.UiUtil;

public class ComicHorizontalButton extends TextView {

    public ComicHorizontalButton(Context context, AttributeSet attrs) {

        super(context, attrs);

        Drawable drawable = this.getBackground();
        if (drawable != null && drawable instanceof BitmapDrawable) {

            BitmapDrawable back = (BitmapDrawable) drawable;

            TypedArray ta = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.ComicHorizontalButton, 0, 0);
            try {
                int width = ta.getInt(R.styleable.ComicHorizontalButton_width, 100);
                if (width <= 0) {
                    width = 100;
                }
                int left = ta.getInt(R.styleable.ComicHorizontalButton_left, 0);
                int right = ta.getInt(R.styleable.ComicHorizontalButton_right, 0);
                this.setBackgroundDrawable(new HorizontalBitmapSegmentDrawable(back.getBitmap(), width, left, right));

                UiUtil.applyButtonEffect(this);

            } finally {
                ta.recycle();
            }
        }
    }


}
