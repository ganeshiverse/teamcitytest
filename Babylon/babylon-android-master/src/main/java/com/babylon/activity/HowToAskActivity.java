package com.babylon.activity;

import android.os.Bundle;
import android.view.View;

import com.babylon.R;
import com.babylon.view.TopBarInterface;

public class HowToAskActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_ask);

        initializeTopBar();
        setTitle(R.string.ask_how_to_title);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.GREEN_THEME;
    }

    public void onAcceptClick(View view) {
        onBackPressed();
    }
}
