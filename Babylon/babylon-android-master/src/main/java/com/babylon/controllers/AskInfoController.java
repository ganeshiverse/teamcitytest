package com.babylon.controllers;

import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.text.TextUtils;

import com.babylon.database.schema.MessageSchema;

public class AskInfoController extends AskController {

    public AskInfoController(FragmentActivity activity) {
        super(activity);
    }

    public interface OnExistQuestionLoadListener {
        void onExistQuestionLoad(boolean isQuestionExist);
    }

    private OnExistQuestionLoadListener questionLoadListener;

    public void start(OnExistQuestionLoadListener onExistQuestionLoadListener) {
        questionLoadListener = onExistQuestionLoadListener;
        start();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        String questionId = null;
        if (cursor.moveToFirst()) {
            final int questionIdIndex = cursor.getColumnIndex(MessageSchema.Columns.ID.getName());
            questionId = cursor.getString(questionIdIndex);
        }

        boolean isQuestionExist = !TextUtils.isEmpty(questionId);

        if (questionLoadListener != null) {
            questionLoadListener.onExistQuestionLoad(isQuestionExist);
        }

        activity.get().getSupportLoaderManager().destroyLoader(LOADER_ID_PENDING_MESSAGE);
    }
}

