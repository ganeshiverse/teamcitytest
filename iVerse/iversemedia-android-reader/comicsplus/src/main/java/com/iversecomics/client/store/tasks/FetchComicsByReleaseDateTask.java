package com.iversecomics.client.store.tasks;

import com.google.gson.Gson;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.db.OrderingUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.store.tasks.FetchComicsByReleaseDateTask.ReleaseDates.ReleaseDate;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.ArrayList;

public class FetchComicsByReleaseDateTask extends ComicStoreTask {

    private static final Logger LOG = LoggerFactory.getLogger(FetchComicsByReleaseDateTask.class);
    private final String listId;

    public FetchComicsByReleaseDateTask(ComicStore comicStore) {
        super(comicStore);
        this.listId = ListID.RELEASE_BY_DATE;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchComicsByReleaseDateTask other = (FetchComicsByReleaseDateTask) obj;
        if (listId == null) {
            if (other.listId != null) {
                return false;
            }
        } else if (!listId.equals(other.listId)) {
            return false;
        }
        return true;
    }

    static class ReleaseDates {
        static class ReleaseDate {
            public String release_date;
        }

        public ArrayList<ReleaseDate> results;
    }

    @Override
    public void execTask() {
        try {
            final ServerApi server = comicStore.getServerApi();

            final JSONObject json = server.getProductReleaseDatesUri();
            final ReleaseDates rd = new Gson().fromJson(json.toString(), ReleaseDates.class);

            final ResponseParser parser = new ResponseParser();

            for (ReleaseDate r : rd.results) {
                final String orderingListId = ListID.getComicsByReleaseDate(r.release_date);
                final ComicStoreDBUpdater updater = comicStore.newDBUpdater();
                updater.addComicDBSubUpdater(new OrderingUpdater(orderingListId));
                parser.parseComics(server.getProductsByReleaseDateUri(r.release_date), updater);
            }

            server.updateFreshness(listId, Time.HOUR * 3);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update new releases list");
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listId == null) ? 0 : listId.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(listId);
    }

}
