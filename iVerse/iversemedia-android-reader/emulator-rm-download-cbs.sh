#!/bin/bash

function emuCmd()
{
    EMUCMD="$@"
    echo "[adb shell]\$ $EMUCMD"
    adb shell $EMUCMD
    echo ""
}

emuCmd rm /sdcard/comics/com.iversecomics.boom.studios.hexed.one.cb
emuCmd rm /sdcard/comics/com.iversecomics.boom.studios.hexed.two.cb
emuCmd rm /sdcard/comics/com.iversecomics.boom.studios.hexed.three.cb

