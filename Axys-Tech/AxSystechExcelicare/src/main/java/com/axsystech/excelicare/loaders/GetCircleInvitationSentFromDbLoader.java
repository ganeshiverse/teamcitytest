package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleInvitationSentDbModel;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/21/2015.
 */
public class GetCircleInvitationSentFromDbLoader extends DataAsyncTaskLibLoader<List<CircleInvitationSentDbModel>> {

    public GetCircleInvitationSentFromDbLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<CircleInvitationSentDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<CircleInvitationSentDbModel, Long> circleInvitationSentListDao = helper.getDao(CircleInvitationSentDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<CircleInvitationSentDbModel>>() {
                @Override
                public List<CircleInvitationSentDbModel> call() throws Exception {
                    return circleInvitationSentListDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
