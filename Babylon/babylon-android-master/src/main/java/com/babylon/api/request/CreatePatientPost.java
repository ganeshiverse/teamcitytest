package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.utils.Constants;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHeader;

public class CreatePatientPost extends AbsRequestPost<Patient, Patient> {

    @Override
    protected BasicHeader getAuthHeader() {
        return null;
    }

    @Override
    public AbsRequestPost<Patient, Patient> withEntry(Patient entry) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("patient", getGson().toJsonTree(entry));
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.PATIENTS);
    }

    @Override
    protected AbsResponseProcessor<Patient> getResponseProcessor() {
        return new RubyResponseProcessor<Patient>(getOutputType());
    }

    @Override
    protected Response<Patient> processResponse(HttpResponse httpResponse) {
        String token = getToken(httpResponse);
        Response<Patient> result = super.processResponse(httpResponse);
        if (result.isSuccess()) {
            result.getData().setToken(token);
        }
        return result;
    }

    private String getToken(HttpResponse httpResponse) {
        String token = null;
        if (httpResponse.containsHeader(Constants.Header_RequestKeys.X_AUTH_TOKEN)) {
            token = httpResponse.getLastHeader(Constants.Header_RequestKeys.X_AUTH_TOKEN).getValue();
        }
        return token;
    }
}
