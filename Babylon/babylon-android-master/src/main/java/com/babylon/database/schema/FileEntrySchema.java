package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public interface FileEntrySchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT IGNORE";

    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.FILE_ENTRY.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.INT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        DATE("date", DBType.INT),
        EXTENTION("extention", DBType.TEXT),
        MIMETYPE("mimetype", DBType.TEXT),
        NAME("name", DBType.TEXT),
        SIZE("size", DBType.TEXT),
        QUESTION_ID("questionId", DBType.INT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public interface Query {
        int TOKEN_QUERY = 400;

        String[] PROJECTION = {
                Table.FILE_ENTRY.getName() + "."
                        + Columns._ID.getName(),
                Columns.ID.getName(),
                Columns.SYNC_WITH_SERVER.getName(),
                Columns.DATE.getName(),
                Columns.EXTENTION.getName(),
                Columns.MIMETYPE.getName(),
                Columns.NAME.getName(),
                Columns.SIZE.getName(),
                Columns.QUESTION_ID.getName(),
        };
        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int DATE = 3;
        int EXTENTION = 4;
        int MIMETYPE = 5;
        int NAME = 6;
        int SIZE = 7;
        int QUESTION_ID = 8;
    }
}
