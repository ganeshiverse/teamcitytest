package com.axsystech.excelicare.framework.ui.messages;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.CityOrLocationDetailsResponse;
import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.LocationUtils;
import com.axsystech.excelicare.util.Trace;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesClient;
//import com.google.android.gms.common.api.GoogleApiClient;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
//import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
//import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by SomeswarReddy on 8/20/2015.
 */
public class SearchLocationFragment extends BaseFragment implements View.OnClickListener, TextWatcher {//},GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    private String TAG = SearchLocationFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private SearchLocationFragment mFragment;
    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.tv_usehomeloc)
    private TextView mUseHomeLoc;

    @InjectView(R.id.tv_usecurrentloc)
    private TextView mUseCurrentLoc;

    @InjectView(R.id.listView)
    private ListView listView;

    // @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    private LayoutInflater mLayoutInflator;

    private ArrayList<CityOrLocationDetailsResponse.LocationDetails> mLocationsList;
    private ArrayList<CityOrLocationDetailsResponse.LocationDetails> mSearchLocationsList;
    private LocationsListAdapter mLocationsListAdapter;

    private LocationSelectionListener locationSelectionListener;

    public void setLocationSelectionListener(LocationSelectionListener locationSelectionListener) {
        this.locationSelectionListener = locationSelectionListener;
    }

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_inbox, container, false);
        searchEditText = (EditText) view.findViewById(R.id.searchEditText);
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        searchEditText.setTypeface(regular);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflator = LayoutInflater.from(mActivity);

        initViews();
        readIntentArgs();
        addEventListeners();
        preloadData();
    }

    private void initViews() {
        searchEditText.setHint(getString(R.string.search_by_location));
        // mUseHomeLoc.setVisibility(View.VISIBLE);
        // mUseCurrentLoc.setVisibility(View.VISIBLE);
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_LOCATION_LOOKUPS_LIST)) {
                mLocationsList = (ArrayList<CityOrLocationDetailsResponse.LocationDetails>) bundle.getSerializable(AxSysConstants.EXTRA_LOCATION_LOOKUPS_LIST);
            }
        }
    }

    private void preloadData() {
        mLocationsListAdapter = new LocationsListAdapter(mLocationsList);
        listView.setAdapter(mLocationsListAdapter);
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                CommonUtils.hideSoftKeyboard(mActivity, searchEditText);
                // update to list adapter with original items
                if (mLocationsListAdapter != null) {
                    mLocationsListAdapter.updateData(mLocationsList);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            //mLocationsListAdapter.filter(text);
            //mLocationsList = (ArrayList<CityOrLocationDetailsResponse.LocationDetails>)
            //mSearchLocationsList = new ArrayList<CityOrLocationDetailsResponse.LocationDetails>();
            for (CityOrLocationDetailsResponse.LocationDetails locationDetails : mLocationsList) {
                if (locationDetails.getCity().contains(text) || locationDetails.getLocality().contains(text)) {
                    //TODO: search list has to be created
                    //mSearchLocationsList.add(locationDetails);
                    System.out.println("search location city - - - " + locationDetails.getCity());
                    System.out.println("search location locality - - - " + locationDetails.getLocality());
                }
            }
            //mLocationsListAdapter.updateData(mSearchLocationsList);
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            //mLocationsListAdapter.updateData(mLocationsList);
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    //Sravan for getting location
//    @Override
//    public void onConnected(Bundle bundle) {
//
//    }

    //Sravan for getting location
//    @Override
//    public void onDisconnected() {
//
//    }

    //Sravan for getting location
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//
//    }

//    protected synchronized void buildGoogleApiClient() {
//        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//    }

    private class LocationsListAdapter extends BaseAdapter {

        private ArrayList<CityOrLocationDetailsResponse.LocationDetails> locationsList;

        public LocationsListAdapter(ArrayList<CityOrLocationDetailsResponse.LocationDetails> locationsList) {
            this.locationsList = locationsList;
        }

        public void updateData(ArrayList<CityOrLocationDetailsResponse.LocationDetails> locationsList) {
            this.locationsList = locationsList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (locationsList != null && locationsList.size() > 0) ? locationsList.size() : 0;
        }

        @Override
        public CityOrLocationDetailsResponse.LocationDetails getItem(int position) {
            return locationsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final CityOrLocationDetailsResponse.LocationDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflator.inflate(R.layout.circles_list_row, viewGroup, false);
            }

            TextView specialityTextView = (TextView) convertView.findViewById(R.id.circleNameTextView);
            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            specialityTextView.setTypeface(regular);

            //Sravan added switch case to set icons to current location and home location
            switch (position) {
                case 0:
                    specialityTextView.setTextColor(getResources().getColor(R.color.sidemenu_selectaccount_color));
                    specialityTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.newlocation, 0, 0, 0);
                    specialityTextView.setCompoundDrawablePadding(15);
                    break;
                case 1:
                    specialityTextView.setTextColor(getResources().getColor(R.color.sidemenu_selectaccount_color));
                    specialityTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.appointments_home, 0, 0, 0);
                    specialityTextView.setCompoundDrawablePadding(15);
                    break;
                default:
                    specialityTextView.setTextColor(getResources().getColor(R.color.circles_text_color));
                    specialityTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    specialityTextView.setCompoundDrawablePadding(0);
            }

            specialityTextView.setText((!TextUtils.isEmpty(rowObject.getLocality()) ? rowObject.getLocality() : "") +
                    (!TextUtils.isEmpty(rowObject.getCity()) ? ", " + rowObject.getCity() : ""));
            //Sravan added switch case for onclick listeners only code in default block was used.

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (position) {
                        case 0:
                            if (LocationUtils.isLocationEnabled(getActivity())) {
                                // get current location of the device and get city from the lat lng and show.
                            } else {
                                showToast("Please turn on your location");
                            }
                            break;
                        case 1:
                            if (locationSelectionListener != null) {
                                // selected home location so send true
                                locationSelectionListener.onLocationSelected(rowObject, true);
                                mActivity.onBackPressed();
                            }
                            break;
                        default:
                            if (locationSelectionListener != null) {
                                //not selected home location so send false
                                locationSelectionListener.onLocationSelected(rowObject, false);
                                mActivity.onBackPressed();
                            }
                            break;
                    }
                }
            });
            return convertView;
        }
    }

    public interface LocationSelectionListener {
        public void onLocationSelected(CityOrLocationDetailsResponse.LocationDetails selectedLocationObject, boolean isHomeSelected);
    }
}
