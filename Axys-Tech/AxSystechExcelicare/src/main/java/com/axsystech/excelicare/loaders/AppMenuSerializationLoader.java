package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.Trace;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by SomeswarReddy on 7/9/2015.
 */
public class AppMenuSerializationLoader extends DataAsyncTaskLibLoader<Boolean> {

    private String TAG = AppMenuSerializationLoader.class.getSimpleName();
    private AppMenuListResponse mAppMenuListResponse;

    public AppMenuSerializationLoader(Context context, AppMenuListResponse appMenuListResponse) {
        super(context);
        this.mAppMenuListResponse = appMenuListResponse;
    }

    @Override
    protected Boolean performLoad() throws Exception {
        try {
            if (mAppMenuListResponse != null) {
                FileOutputStream fileOut = new FileOutputStream(AxSysConstants.APP_MENU_LIST_FILE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(mAppMenuListResponse);

                out.close();
                fileOut.close();

                Trace.d(TAG, "Serialized data is saved in " + AxSysConstants.APP_MENU_LIST_FILE.getAbsolutePath());

                return true;
            }
        } catch (IOException i) {
            i.printStackTrace();
        } catch (Exception i) {
            i.printStackTrace();
        }

        return false;
    }
}
