package com.babylon.api.request.base;

import java.util.ArrayList;

public class ResponseTree<T> {
    private ArrayList<T> tree;

    public ArrayList<T> getTree() {
        return tree == null ? tree = new ArrayList<T>() : tree;
    }
}