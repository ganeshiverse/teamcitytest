package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.babylon.R;
import com.babylon.fragment.BaseFragment;
import com.babylon.view.TopBarInterface;

public class ShopCategoryHelpFragment extends BaseFragment {

    public static final String TAG = ShopCategoryHelpFragment.class.getSimpleName();

    public static ShopCategoryHelpFragment newInstance() {
        ShopCategoryHelpFragment fragment = new ShopCategoryHelpFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.fragment_help_title));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PINK_THEME;
    }


}






