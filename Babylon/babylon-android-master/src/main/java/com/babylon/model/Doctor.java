package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class Doctor extends BaseModel {
    @SkipFieldInContentValues
    private static final long serialVersionUID = 1321L;

    private Integer id;
    private String name;
    private String email;
    private String role;
    private String avatar;
    private String bio;
    @SerializedName("medical_identifier")
    private String medicalIdentifier;
    @SerializedName("medical_identifier_label")
    private String medicalIdentifierLabel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getMedicalIdentifier() {
        return medicalIdentifier;
    }

    public void setMedicalIdentifier(String medicalIdentifier) {
        this.medicalIdentifier = medicalIdentifier;
    }

    public String getMedicalIdentifierLabel() {
        return medicalIdentifierLabel;
    }

    public void setMedicalIdentifierLabel(String medicalIdentifierLabel) {
        this.medicalIdentifierLabel = medicalIdentifierLabel;
    }
}
