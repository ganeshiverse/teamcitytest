package com.iversecomics.bundle;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

import com.iversecomics.bundle.parse.AbstractParseBundle;
import com.iversecomics.bundle.parse.BundleAccess;
import com.iversecomics.bundle.parse.ParseBundleType3;
import com.iversecomics.bundle.parse.ParseBundleType4;
import com.iversecomics.bundle.parse.ParseBundleType5;
import com.iversecomics.io.Digest;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicBundle {
    private final static Logger LOG = LoggerFactory.getLogger(ComicBundle.class);
    private ComicMetadata metadata = new ComicMetadata();
    private ComicContent  content  = new ComicContent();
    private Ownership     ownership;
    private short         version;
    private static final int SHA1_HASH_LENGTH = 20;
    private static final int SIZEOF_SHORT = 2;
    public static final int PREVIEW_PAGE_SIZE = 4;
    

    public ComicContent getContent() {
        return content;
    }

    public ComicMetadata getMetadata() {
        return metadata;
    }

    public Ownership getOwnership() {
        return ownership;
    }

    public short getVersion() {
        return version;
    }
    
    public void setContent(File bundleFile) throws BundleReadException {
        if (!bundleFile.exists()) {
            throw new BundleReadException("Unable to find comic bundle: " + bundleFile);
        }
        content.setFile(bundleFile);
    }
    
    public void parse() throws BundleReadException {
    	parse(content.getFile());
    }
    
    public void parse(File bundleFile) throws BundleReadException {
        if (!bundleFile.exists()) {
            throw new BundleReadException("Unable to find comic bundle: " + bundleFile);
        }

        BundleAccess bundle = null;
        try {
        	        	
            bundle = new BundleAccess(bundleFile, "r");
            
            this.version = bundle.readHeader();
            AbstractParseBundle parsebundle = null;
            LOG.debug("Bundle Version: %d - %s", version, bundleFile);
            switch (version) {
                case 1: // Introduced by original Android standalone app saving to SDCard
                	throw new BundleReadException("This bundle type is unsupported.");
                case 2:
                    throw new BundleReadException("Unable to parse unreleased version 2 bundles");
                case 3: // Introduced by iPhone Client (SD Comic Bundles)
                    parsebundle = new ParseBundleType3();
                    break;
                case 4:
                    parsebundle = new ParseBundleType4();
                    break;
                case 5:
                	parsebundle = new ParseBundleType5();
                	break;
                default:
                    throw new BundleReadException("This product requires a newer version of the application.");
            }
            this.metadata.setFilename(bundleFile.getName());
            this.metadata.setFilesize(bundleFile.length());
            parsebundle.setMetadata(this.metadata);
            this.content.setFile(bundleFile);
            parsebundle.setContent(this.content);
            parsebundle.setOwnership(ownership);
            try {
                parsebundle.parse(bundle);
            } catch (BundleSecurityException e) {
                LOG.warn(e, e.getMessage());
                parsebundle.getMetadata().setStatus(ComicMetadata.STATUS_NOTYOURS);
                throw e;
            } catch (BundleReadException e) {
                LOG.warn(e, e.getMessage());
                parsebundle.getMetadata().setStatus(ComicMetadata.STATUS_BAD);
            }
            this.metadata = parsebundle.getMetadata();
            this.content = parsebundle.getContent();
            
            //Check the checksum (or the signature)
            boolean passed = false;
            try { 
            	isSignatureValid();
            	passed = true;
            } catch (Exception e) {
			}
            
            if(passed == false ) {
            	checksumIsValid();
            }
            
        } catch (IOException e) {
            LOG.warn("Bundle failed parsing: %s", e.getMessage());
            throw new BundleReadException("Unable to open downloaded comic: " + e.getMessage(), e);
        } finally {
            IOUtil.close(bundle);
        }
    }
    
    private void setExpectedHash() throws BundleReadException {
    	BundleAccess bundle = null;
    	try {
    		bundle = new BundleAccess(this.getContent().getFile(), "r");

    		// I couldn't find a defined constant for the length, so I'm hard-coding 20 for the hash length
    		long pos = bundle.length() - SHA1_HASH_LENGTH;
    		bundle.seek(pos);
    		byte buf[] = new byte[SHA1_HASH_LENGTH];
    		bundle.readFully(buf, 0, SHA1_HASH_LENGTH);
    		bundle.seek(0);
    		this.metadata.setExpectedHash(buf);
    	} catch (IOException e) {
    		LOG.warn("Bundle failed parsing: %s", e.getMessage());
    		throw new BundleReadException("Unable to open downloaded comic: " + e.getMessage(), e);
    	} finally {
    		try {
    		bundle.close();
    		} catch (Exception e) {
			}
    	}
    }
    
    
    public void setHash() throws IOException {
    	byte[] hash;
    	BundleAccess bundle = null;

    	try  {
    		File hashFile = this.content.getFile();
    		// The file should be hashed up to the end (minus the hashed string).  The end has the SHA1 hash string,
    		// a short storing the length of that string, and a short storing the field tag
			hash = Digest.sha1(hashFile, hashFile.length()-SHA1_HASH_LENGTH-(SIZEOF_SHORT*2));
			
    		bundle = new BundleAccess(hashFile, "rw");

    		// I couldn't find a defined constant for the length, so I'm hard-coding 20 for the hash length
    		long pos = bundle.length() - SHA1_HASH_LENGTH;
    		bundle.seek(pos);
    		bundle.write(hash, 0, SHA1_HASH_LENGTH);

		} catch (IOException e) {
			LOG.error(e, e.getMessage());
			throw e;
		} finally {
			bundle.close();
		}
    }
    
    
    public void checksumIsValid() throws IOException {
    	byte[] hash;
    	setExpectedHash();
    	try  {
    		File hashFile = this.content.getFile();
    		// The file should be hashed up to the end (minus the hashed string).  The end has the SHA1 hash string,
    		// a short storing the length of that string, and a short storing the field tag
			hash = Digest.sha1(hashFile, hashFile.length()-SHA1_HASH_LENGTH-(SIZEOF_SHORT*2));
		} catch (IOException e) {
			LOG.error(e, e.getMessage());
			throw e;
		}
		if (!Arrays.equals(hash, this.metadata.getExpectedHash())) {
        	LOG.warn("Bundle failed signature checks");
        	throw new BundleReadException("Bundle is invalid or corrupt");
		}
    }
    
    
    public void signBundle() throws IOException {
    	RandomAccessFile bundleFile = null;
    	try {
	    	bundleFile = new RandomAccessFile(this.content.getFile(), "rw");
	    	bundleFile.seek(bundleFile.length()-SHA1_HASH_LENGTH);

	    	String hashString = this.ownership.getUniqueId() + this.metadata.getId();
	    	String hashed = Digest.sha1(hashString);
	    	byte[] bytes = hashed.getBytes();
	    	//Hashed array is 40 bytes, just write the first 20 (lazy way to avoid converting string to binary)
	        bundleFile.write(bytes, 0, SHA1_HASH_LENGTH);
    	} finally {
    		bundleFile.close();
    	}
    }
    
    public void isSignatureValid() throws IOException
    {
    	RandomAccessFile bundleFile = null;
    	String hashString = this.ownership.getUniqueId() + this.metadata.getId();
    	
        byte buf[] = new byte[SHA1_HASH_LENGTH];
    	byte hashedBytes[] = new byte[SHA1_HASH_LENGTH];

    	try {
	    	bundleFile = new RandomAccessFile(this.content.getFile(), "r");
	    	bundleFile.seek(bundleFile.length()-SHA1_HASH_LENGTH);
	    	
	    	//Hash the string and convert first 20 bytes to byte array
	    	String hashed = Digest.sha1(hashString);
	    	System.arraycopy(hashed.getBytes(), 0, hashedBytes, 0, SHA1_HASH_LENGTH);
	    			
	    	//Read the 20 byte hash from the bundle
	        bundleFile.readFully(buf, 0, SHA1_HASH_LENGTH);
	        
		} catch (IOException e) {
			LOG.error("IOException parsing bundle: %s", e, e.getMessage());
			throw e;
		} finally {
			IOUtil.close(bundleFile);
		}
    	
        //Check them
        if (!Arrays.equals(buf, hashedBytes)) {
        	LOG.warn("Bundle failed signature checks");
        	throw new BundleReadException("Bundle is invalid or corrupt");
        }    
    }

    public void setOwnership(Ownership ownership) {
        this.ownership = ownership;
    }
}
