package com.babylon.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.babylon.BaseConfig;
import com.babylon.R;
import com.babylon.fragment.tour.MonitorMeTourHistoryFragment;
import com.babylon.fragment.tour.TourMonitorMeSimpleFragment;
import com.babylon.utils.L;

public class GuidePagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = GuidePagerAdapter.class.getSimpleName();

    public static  final int PAGE_NUMBER = 5;

    private final int[] LAYOUT_IDS = {
            R.layout.fragment_tour_monitor_me,
            R.layout.fragment_tour_monitor_me_sections,
            R.layout.fragment_tour_monitor_me_circles,
            R.layout.fragment_tour_monitor_link_devices
    };

    public GuidePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {

        if (BaseConfig.LOG_ENABLED) {
            assert (position < PAGE_NUMBER);
        }

        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
                return TourMonitorMeSimpleFragment.getInstance(LAYOUT_IDS[position]);
            case 4:
                return new MonitorMeTourHistoryFragment();
            default:
                L.d(TAG, "Unknown page position:" + String.valueOf(position));
                return null;
        }

    }

    @Override
    public int getCount() {
        return PAGE_NUMBER;
    }
}