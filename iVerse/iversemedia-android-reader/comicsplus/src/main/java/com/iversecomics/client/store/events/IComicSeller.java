package com.iversecomics.client.store.events;

public interface IComicSeller {
    public OnComicPurchaseListener getOnComicPurchaseListener();

    public void setOnComicPurchaseListener(OnComicPurchaseListener onComicPurchaseListener);
}
