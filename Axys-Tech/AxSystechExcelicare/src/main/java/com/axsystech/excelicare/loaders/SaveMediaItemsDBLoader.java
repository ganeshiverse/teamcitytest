package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SaveMediaItemsDBLoader extends DataAsyncTaskLibLoader<ArrayList<MediaItemDbModel>> {

    private String TAG = SaveMediaItemsDBLoader.class.getSimpleName();
    private final ArrayList<MediaItemDbModel> mediaItemsAL;

    public SaveMediaItemsDBLoader(final Context context, final ArrayList<MediaItemDbModel> mediaItems) {
        super(context, true);
        this.mediaItemsAL = mediaItems;
    }

    @Override
    protected ArrayList<MediaItemDbModel> performLoad() throws Exception {
        if (mediaItemsAL == null)
            throw new SQLException("No media found to cache in device database...");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (mediaItemsAL != null && mediaItemsAL.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<MediaItemDbModel>>() {
                    @Override
                    public ArrayList<MediaItemDbModel> call() throws Exception {
                        for (final MediaItemDbModel dbModel : mediaItemsAL) {
                            final Dao<MediaItemDbModel, Long> mediaItemDao = helper.getDao(MediaItemDbModel.class);

                            mediaItemDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved media item Details in DB...");
                        }

                        return mediaItemsAL;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return mediaItemsAL;
    }
}
