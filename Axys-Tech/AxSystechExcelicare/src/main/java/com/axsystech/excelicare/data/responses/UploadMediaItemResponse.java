package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hkyerra on 7/8/2015.
 */
public class UploadMediaItemResponse extends BaseResponse implements Serializable {

    public void setData(String data) {
        this.data = data;
    }

    @SerializedName("data")
    private String data;

    public String getData() {
        return data;
    }
}
