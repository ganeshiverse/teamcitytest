package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hkyerra on 7/8/2015
 */
public class GetFoldersListResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private FoldersDataObject foldersDataObject;

    public FoldersDataObject getFoldersDataObject() {
        return foldersDataObject;
    }

    public class FoldersDataObject implements Serializable {
        @SerializedName("Folder")
        private ArrayList<Folder> foldersList;

        public ArrayList<Folder> getFoldersList() {
            return foldersList;
        }
    }

    public class Folder implements Serializable {
        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        @SerializedName("SubFolder")
        private ArrayList<SubFolder> subFoldersList;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public ArrayList<SubFolder> getSubFolderList() {
            return subFoldersList;
        }
    }

    public class SubFolder implements Serializable {
        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
