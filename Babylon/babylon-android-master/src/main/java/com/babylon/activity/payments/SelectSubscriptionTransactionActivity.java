package com.babylon.activity.payments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.api.request.AppointmentCancelGet;
import com.babylon.api.request.AppointmentGet;
import com.babylon.api.request.PlansGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.PaymentAppointmentPost;
import com.babylon.api.request.payments.PaymentSubscribtionPost;
import com.babylon.api.request.payments.SubscribePatch;
import com.babylon.controllers.payments.PromoCodeController;
import com.babylon.fragment.payments.OrderDetailsListFragment;
import com.babylon.fragment.payments.PaymentConfirmationFragment;
import com.babylon.fragment.payments.SelectSubscriptionFragment;
import com.babylon.model.Patient;
import com.babylon.model.payments.*;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylonpartners.babylon.HomePageActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Patient should be loaded before
 */
public class SelectSubscriptionTransactionActivity extends PaymentTransactionActivity implements
        SelectSubscriptionFragment.OnSubscriptionSelectedListener, SelectSubscriptionFragment.DataProtocol,
        PaymentConfirmationFragment.OnPaymentConfirmationListener, PaymentConfirmationFragment
        .DataProtocol,OrderDetailsListFragment.OrderAddressFragmentListener {


    Plan mSelectedPlan;
    public static final int NEW_SUBSCRIPTION_SELECTED_RESULT = 1513;

    private final PromoCodeController promoCodeController = new PromoCodeController();

    private Plan[] plans;
    private Subscription subscription;
    private Promotion promotion;
    private Appointment appointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appointment = (Appointment) getIntent().getSerializableExtra(Keys.APPOINTMENT_KEY);

        findViewById(R.id.base_layout).setBackgroundColor(getResources().getColor(R.color.orange_bright));
        showProgressBar();

        new PlansGet().execute(SelectSubscriptionTransactionActivity.this, plansRequestListener);
    }

    protected int selectedPatientId()
    {
        int patientId = Integer.parseInt(patient.getId());
        if (appointment != null) {
            patientId = Integer.parseInt(appointment.getPatientId());
        }

        return patientId;
    }

    private void retryDialog(final Runnable runnable) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SelectSubscriptionTransactionActivity.this);
        alertDialogBuilder.setMessage(R.string.offline_error);
        alertDialogBuilder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                runnable.run();
            }
        });
        AlertDialog confirmationAlertDialog = alertDialogBuilder.create();
        confirmationAlertDialog.show();
    }

    private void ensureAppointmentPaidAndContinue(final Runnable completion) {
        final Runnable retryRunnable = new Runnable() {
            @Override
            public void run() {
                ensureAppointmentPaidAndContinue(completion);
            }
        };

        final RequestListenerErrorProcessed<Appointment> onAppointmentPayingReceiver = new RequestListenerErrorProcessed<Appointment>() {
            @Override
            public void onRequestComplete(Response<Appointment> response, String errorMessage) {
                if (SelectSubscriptionTransactionActivity.this.isFinishing()) return;
                setProgressBarVisible(false);
                if (TextUtils.isEmpty(errorMessage)) {
                    completion.run();
                } else {
                    retryDialog(retryRunnable);
                }
            }
        };

        if (appointment == null) {
            completion.run();
            return;
        }
        setProgressBarVisible(true);
        String appointmentId = String.valueOf(appointment.getId());
        final AppointmentGet appointmentGet = new AppointmentGet(appointmentId);
        appointmentGet.execute(SelectSubscriptionTransactionActivity.this, new RequestListener<Appointment>() {
            @Override
            public void onRequestComplete(Response<Appointment> response) {
                if (SelectSubscriptionTransactionActivity.this.isFinishing()) return;
                setProgressBarVisible(false);
                if (response.isSuccess()) {
                    Appointment appointment = response.getData();
                    if (appointment.getIsPaid()) { // If appointment is paid, we can finish
                        completion.run();
                    } else {
                        int cardId = getPaymentCard().getId();
                        int patientId = Integer.parseInt(patient.getId());
                        if (appointment != null) {
                            if (appointment.getIsFree())
                                cardId = -1; // If appointment is free, we need to make a transaction with no card to
                            // transition the state to paid
                            patientId = Integer.parseInt(appointment.getPatientId());
                        }
                        setProgressBarVisible(true);
                        PaymentAppointmentPost.getPaymentAppointmentPost(
                                cardId,
                                appointment.getId(),
                                patientId).execute(
                                SelectSubscriptionTransactionActivity.this,
                                onAppointmentPayingReceiver);
                    }
                } else {
                    retryDialog(retryRunnable);
                }
                getSupportLoaderManager().destroyLoader(appointmentGet.hashCode());
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PromotionTransactionActivity.PROMOTION_REQUEST_CODE) {

            Redemption redemption = (Redemption) data.getSerializableExtra(PromotionTransactionActivity.REDEMPTION);
            if (redemption != null) {
                promotion = redemption.getPromotion();
                patient.setPromotion(promotion);
              onPaymentDone();
              /*ensureAppointmentPaidAndContinue(new Runnable() {
                    @Override
                    public void run() {
                        onPaymentDone();
                    }
                });*/
            }
        }
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        new PlansGet().execute(SelectSubscriptionTransactionActivity.this, plansRequestListener);
    }*/

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    @Override
    public void onSubscriptionSelected(Plan selectedPlan)
    {
        mSelectedPlan=selectedPlan;

        //Promotion
        if(patient.getPromotion()!=null ||(appointment!=null && appointment.getIsPromotion()))
        {
            //Promotion MixPanel Event
            JSONObject props = new JSONObject();
            try
            {
                props.put("promo code", patient.getPromotion().getName());
                props.put("Date",PreferencesManager.getInstance().getDateString());
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            Bundle fb_parameters = new Bundle();
            fb_parameters.putString("promo code", patient.getPromotion().getName());



            MixPanelHelper helper = new MixPanelHelper(SelectSubscriptionTransactionActivity.this,props,null);
            helper.sendEvent(MixPanelEventConstants.PROMOTION);

            new SubscribePatch().withEntry(new SubscribePatch.BodyParams(getPaymentCard().getId(), selectedPatientId())).execute(this, onPayPatchRequestListener);

        }
        //Either PAYG or Subscription
        else
        {
            //Subscription MixPanel Event
            if(!mSelectedPlan.isDefault())
            {
                JSONObject props = new JSONObject();
                try
                {
                    props.put("Date",PreferencesManager.getInstance().getDateString());
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                Bundle fb_parameters = new Bundle();
                fb_parameters.putString("Date",PreferencesManager.getInstance().getDateString());



                MixPanelHelper helper = new MixPanelHelper(SelectSubscriptionTransactionActivity.this,props,fb_parameters);
                helper.sendEvent(MixPanelEventConstants.SUBSCRIPTION);
            }
            else
            {
                JSONObject props = new JSONObject();
                try
                {
                    props.put("Date",PreferencesManager.getInstance().getDateString());
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                MixPanelHelper helper = new MixPanelHelper(SelectSubscriptionTransactionActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.PAY_AS_YOU_GO);
            }

          new PaymentSubscribtionPost().withEntry(new PaymentSubscribtionPost.PlanParams(selectedPlan.getId(),
              String.valueOf(this.selectedPatientId()) )).execute(this,onSubscriptionSelectedRequestListener);
            setProgressBarVisible(true);
        }
    }

    private void fillInOrderDetails(Subscription selectedSubscription) {
        orderDetails = new LinkedList<>();

        final OrderDetail orderDetails = new OrderDetail(selectedSubscription.getPlan().getTitle(),
                selectedSubscription.getPrice());
        this.orderDetails.add(orderDetails);
    }

    @Override
    public void onPromoCodeEntered(String promoCode) {
        setProgressBarVisible(true);

        int patientId = this.selectedPatientId();
        promoCodeController.sendPromoCode(this, promoCode, patientId, onPromoCodeValidationListener);
    }

    @Override
    public Plan[] getPlans() {
        return plans;
    }

    @Override
    public Patient getPatient() {
        return patient;
    }

    @Override
    public Boolean shouldShowCancelButton() {
        return (appointment != null);
    }

    @Override
    public void onBackPressed() {
        final Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);

        if (appointment != null && fragment != null && fragment instanceof SelectSubscriptionFragment) {
            cancelAppointment();
        } else {
            super.onBackPressed();
        }
    }

    public void cancelAppointment()
    {
        if (appointment == null)
            return;

        AlertDialogHelper.getInstance().showSimpleConfirmationDialog(this, getString(R.string.select_subscription_cancel_appointment_title), getString(R.string.select_subscription_cancel_appointment_message), new AlertDialogHelper.ConfirmationDialogListener() {
            @Override
            public void confirm()
            {
                //Promotion MixPanel Event
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(SelectSubscriptionTransactionActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.APPOINTMENTS_CANCELLED);

                setProgressBarVisible(true);
                new AppointmentCancelGet(String.valueOf(appointment.getId())).execute(SelectSubscriptionTransactionActivity.this, onAppointmentRequestListener);

            }

            @Override
            public void cancel() {

            }
        }, getString(R.string.select_subscription_cancel_appointment_button_dont_cancel), getString(R.string.select_subscription_cancel_appointment_button_cancel_appointment));
    }

    private final RequestListenerErrorProcessed<Appointment> onAppointmentRequestListener = new
        RequestListenerErrorProcessed<Appointment>() {
            @Override
            public void onRequestComplete(Response<Appointment> response, String errorMessage) {
                setProgressBarVisible(false);
                if (!SelectSubscriptionTransactionActivity.this.isFinishing()) {
                    if (response.isSuccess()) {

                        Intent i = new Intent(SelectSubscriptionTransactionActivity.this, HomePageActivity.class);
                        i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                                .FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    } else {
                        AlertDialogHelper.getInstance().showSimpleConfirmationDialog
                                (SelectSubscriptionTransactionActivity.this, getString(R.string
                                        .select_subscription_cancel_appointment_title), getString(R.string
                                        .select_subscription_cancel_appointment_error_message), null, null, getString
                                        (android.R.string.ok));
                    }
                }
            }
        };

    @Override
    public void onPayClick() {
        if (!isPaymentInProgress)
        {
            setProgressBarVisible(true);
            isPaymentInProgress = true;

            new SubscribePatch().withEntry(new SubscribePatch.BodyParams(getPaymentCard().getId(), selectedPatientId())).execute(this, onPayRequestListener);
        }
    }

    @Override
    public void onPaymentDone() {
        Intent intent = getIntent();
        intent.putExtra(PaymentTransactionActivity.PATIENT_EXTRA, patient);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    @Override
    public String getOrderDetailsSubTitle() {
        return getString(R.string.order_details_billed_monthly);
    }

    @Override
    public String getOrderTotal() {
        //total is not needed
        return null;
    }

    @Override
    public boolean IsSpecialist() {
        return false;
    }



    @Override
    public void onAddFamilyToPlanClick() {
        //TODO will be implemented later.
    }

    @Override
    public
    @Nullable
    Double getSubscriptionPrice() {
        Double price = null;

        if (subscription != null) {
            price = subscription.getPrice();
        }

        return price;
    }

    private final RequestListener<Plan[]> plansRequestListener = new RequestListener<Plan[]>() {

        @Override
        public void onRequestComplete(Response<Plan[]> response) {
            plans = response.getData();

            if (plans == null) {
                //This case is not possible according requirements
                Toast.makeText(SelectSubscriptionTransactionActivity.this, R.string.some_error,
                        Toast.LENGTH_SHORT).show();
            } else if (plans.length == 1 && plans[0].isFreeForPatient(patient)) {
                onPaymentDone();
            } else {
                // sort by order index
                Arrays.sort(plans, Collections.reverseOrder());
             startFragment(SelectSubscriptionFragment.newInstance(patient), false,
                      SelectSubscriptionFragment.TAG);
            }
        }
    };

    private final RequestListenerErrorProcessed<Subscription> onSubscriptionSelectedRequestListener = new
            RequestListenerErrorProcessed<Subscription>
                    () {
                @Override
                public void onRequestComplete(Response<Subscription> response, String errorMessage) {
                    if (!SelectSubscriptionTransactionActivity.this.isFinishing()) {
                        setProgressBarVisible(false);

                        if (TextUtils.isEmpty(errorMessage)) {
                            subscription = response.getData();
                            fillInOrderDetails(subscription);
                            setSubscriptions();

                            if (subscription.isFree()) {
                                      onPaymentDone();
                            } else {
                                startFragment(new OrderDetailsListFragment(), OrderDetailsListFragment.TAG);

                            }
                        } else {
                            Toast.makeText(SelectSubscriptionTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                private void setSubscriptions() {
                    patient.setSubscription(subscription);
                }
            };

    private final RequestListenerErrorProcessed<Subscription> onPayRequestListener = new
            RequestListenerErrorProcessed<Subscription>() {
                @Override
                public void onRequestComplete(Response<Subscription> response, String errorMessage) {

                    if (!SelectSubscriptionTransactionActivity.this.isFinishing()) {
                        isPaymentInProgress = false;

                        setProgressBarVisible(false);

                        if (TextUtils.isEmpty(errorMessage)) {
                            new HandlerLoaderListener().sendEmptyMessage(HandlerLoaderListener
                                    .BACK_PRESS_HANDLER_CODE);

                        } else {
                            Toast.makeText(SelectSubscriptionTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

    private final RequestListenerErrorProcessed<Subscription> onPayPatchRequestListener = new
            RequestListenerErrorProcessed<Subscription>() {
                @Override
                public void onRequestComplete(Response<Subscription> response, String errorMessage) {

                    if (!SelectSubscriptionTransactionActivity.this.isFinishing()) {
                        isPaymentInProgress = false;

                        setProgressBarVisible(false);

                        if (TextUtils.isEmpty(errorMessage)) {
                          /*  new HandlerLoaderListener().sendEmptyMessage(HandlerLoaderListener
                                    .BACK_PRESS_HANDLER_CODE);*/
                            new PaymentSubscribtionPost().withEntry(new PaymentSubscribtionPost.PlanParams
                                    (mSelectedPlan.getId(), String.valueOf(SelectSubscriptionTransactionActivity.this
                                            .selectedPatientId()) )).execute
                                    (SelectSubscriptionTransactionActivity.this,
                              onSubscriptionSelectedRequestListener);
                              setProgressBarVisible(true);

                        } else {
                            Toast.makeText(SelectSubscriptionTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

    public static void startActivityForResult(Activity context, Appointment appointment, Patient patient, int resultId) {
        Intent intent = new Intent(context, SelectSubscriptionTransactionActivity.class);

        if (appointment != null)
            intent.putExtra(Keys.APPOINTMENT_KEY, appointment);

        if (patient != null)
            intent.putExtra(PATIENT_EXTRA, patient);

        context.startActivityForResult(intent, resultId);
    }

    public class HandlerLoaderListener extends Handler {
        public static final int BACK_PRESS_HANDLER_CODE = 2;

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == BACK_PRESS_HANDLER_CODE) {
                if (!SelectSubscriptionTransactionActivity.this.isFinishing()) {
                    ensureAppointmentPaidAndContinue(new Runnable() {
                        @Override
                        public void run() {
                            startFragment(new PaymentConfirmationFragment(), PaymentConfirmationFragment.TAG);
                        }
                    });
                }
            }
        }
    }

    private final PromoCodeController.PromoCodeValidationListener onPromoCodeValidationListener = new
            PromoCodeController.PromoCodeValidationListener() {
                @Override
                public void onValidPromoCode(@Nullable Redemption redemption) {
                    setProgressBarVisible(false);
                    PreferencesManager.getInstance().setIsFirstPromo(false);
                    PromotionTransactionActivity.startPromotionTransactionActivity
                            (SelectSubscriptionTransactionActivity.this,
                            redemption);
                }

                @Override
                public void onInvalidPromoCode(@Nullable String error) {
                    setProgressBarVisible(false);

                    if (!TextUtils.isEmpty(error)) {
                        Toast.makeText(SelectSubscriptionTransactionActivity.this, error, Toast.LENGTH_SHORT).show();
                    }

                }
            };


    @Override
    public void onAddressClicked() {

    }



}
