package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class DeleteMediaItemDbLoader extends DataAsyncTaskLibLoader<MediaItemDbModel> {

    private final MediaItemDbModel mediaItemDbModel;

    public DeleteMediaItemDbLoader(final Context context, final MediaItemDbModel mediaItemDbModel) {
        super(context, true);
        this.mediaItemDbModel = mediaItemDbModel;
    }

    @Override
    protected MediaItemDbModel performLoad() throws Exception {
        if (mediaItemDbModel == null)
            throw new SQLException("Unable to delete media details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final Dao<MediaItemDbModel, Long> mediaItemDbModelDao = helper.getDao(MediaItemDbModel.class);
            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<MediaItemDbModel>() {
                @Override
                public MediaItemDbModel call() throws Exception {
                    mediaItemDbModelDao.delete(mediaItemDbModel);

                    return mediaItemDbModel;
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
