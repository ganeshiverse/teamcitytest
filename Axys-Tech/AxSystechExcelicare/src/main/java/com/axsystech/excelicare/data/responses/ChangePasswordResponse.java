package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by someswar on 7/1/2015.
 */
public class ChangePasswordResponse extends BaseResponse {

    @SerializedName("ClientURL")
    private String clientURL;

    @SerializedName("data")
    private ChangePasswordDataObject changePasswordDataObject;

    public String getClientURL() {
        return clientURL;
    }

    public ChangePasswordDataObject getChangePasswordDataObject() {
        return changePasswordDataObject;
    }

    public class ChangePasswordDataObject {
        @SerializedName("EcUserID")
        private String ecUserID;

        @SerializedName("Forename")
        private String forename;

        @SerializedName("loginName")
        private String loginName;

        @SerializedName("SiteID")
        private String siteID;

        @SerializedName("Sitename")
        private String siteName;

        @SerializedName("SitePhotoURL")
        private String sitePhotoURL;

        @SerializedName("ClientURL")
        private String clientURL;

        @SerializedName("SiteDescription")
        private String siteDescription;

        @SerializedName("SiteInfoLink")
        private String siteInfoLink;

        @SerializedName("DOB")
        private String dob;

        @SerializedName("Age")
        private String age;

        @SerializedName("Sex")
        private String gender;

        @SerializedName("SexValue")
        private String genderValue;

        @SerializedName("Photo")
        private String photo;

        @SerializedName("token")
        private String token;

        @SerializedName("Surname")
        private String surname;

        @SerializedName("UserDeviceID")
        private String userDeviceID;

        public String getEcUserID() {
            return ecUserID;
        }

        public String getForename() {
            return forename;
        }

        public String getLoginName() {
            return loginName;
        }

        public String getSiteID() {
            return siteID;
        }

        public String getSiteName() {
            return siteName;
        }

        public String getSitePhotoURL() {
            return sitePhotoURL;
        }

        public String getClientURL() {
            return clientURL;
        }

        public String getSiteDescription() {
            return siteDescription;
        }

        public String getSiteInfoLink() {
            return siteInfoLink;
        }

        public String getDob() {
            return dob;
        }

        public String getAge() {
            return age;
        }

        public String getGender() {
            return gender;
        }

        public String getGenderValue() {
            return genderValue;
        }

        public String getPhoto() {
            return photo;
        }

        public String getToken() {
            return token;
        }

        public String getSurname() {
            return surname;
        }

        public String getUserDeviceID() {
            return userDeviceID;
        }
    }
}
