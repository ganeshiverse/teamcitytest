package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.shop.AddressResponseListAdapter;
import com.babylon.api.request.base.Urls;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Kit.Address;
import com.babylon.model.payments.Order;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.CommonHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.view.TopBarInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PatientAddressListFragment extends BaseFragment implements AbsListView.OnItemClickListener {

    public static final String TAG = PatientAddressListFragment.class.getSimpleName();



    private ListView listView;
    private AddressResponseListAdapter adapter;
    private List<Address> addressList = new ArrayList<Address>();
    private OnAddClickListener onaddClickListener;
    private Order mOrderList;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_view, container, false);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getString(R.string.select_address_title));
        listView = (ListView)view.findViewById(android.R.id.list);

        setProgressBarVisible(true);
        listView.setOnItemClickListener(this);
        initRightImageButton();
        if(addressList.size()==0) {
            getAddressOfPatient();
        }
        else  {
            adapter = new AddressResponseListAdapter(getActivity(), addressList);
            listView.setAdapter(adapter);
            setProgressBarVisible(false);
        }

    }

    private void initRightImageButton() {
        setRightButton(onAddMenuClickListener, R.string.family_list_add);
        // boolean isPaymentCardCreated = dataProtocol.getPaymentCard().isCreated();
         setRightButtonEnabled(true , R.color
                .orange_bright);
    }



    private final View.OnClickListener onItemClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            };




    private void getAddressOfPatient() {

        String mUri=  String.format(CommonHelper.getUrl(Urls.GET_USER_ADDRESS), SyncUtils.getWsse().getId());

        JsonArrayRequest mArrayReq = new JsonArrayRequest(mUri,	new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String jsonString = response.toString();

                Type collectionType = new TypeToken<List<Address>>() { }.getType();
                List<Address> addressResponseList = gson.fromJson(jsonString, collectionType);
                addressList= addressResponseList;

                adapter = new AddressResponseListAdapter(getActivity(), addressList);

                listView.setAdapter(adapter);
                   setProgressBarVisible(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,error.toString());
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                return headers;
            }

        };

      if (App.getInstance().isConnection()){
            mArrayReq.setRetryPolicy(new DefaultRetryPolicy(18000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

           App.getInstance().getRequestQueue().add(mArrayReq);
         }



    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            onaddClickListener = (OnAddClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPayClickListener");
        }


    }


    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (onaddClickListener != null) {
          Address address=adapter.getItem(position);
            onaddClickListener.onPatieAddressSelectedForList(address);

            PreferencesManager.getInstance().setPatientAddress(address.getFirstLine()+"," +
                    ""+address.getSecondLine()+"\n"+address.getCity()+", " +
                    ""+address.getPostCode());
             PreferencesManager.getInstance().setPatientAddressId(address.getId());
        }
    }

    public interface OnAddClickListener {

        public void onAddClick();
        public void onPatieAddressSelectedForList(Address address);
    }

    private final View.OnClickListener onAddMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onaddClickListener.onAddClick();
        }
    };



}