package com.babylon.validator;

public class PhoneValidator extends BaseValidator {

    public PhoneValidator(BaseValidator nextValidator, String errorMessage) {
        super(nextValidator, errorMessage);
    }

    @Override
    protected String validate(String string) {
        String error = null;

        int count = 0;
        for (int i = 0, len = string.length(); i < len; i++) {
            if (Character.isDigit(string.charAt(i))) {
                count++;
            }
        }

        if (count < 10 || count > 12) {
            error = errorMessage;
        }

        return error;
    }

}
