package com.iversecomics.client.store.tasks;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONArray;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchGroupsTask extends ComicStoreTask {
    public static final String GROUPS_UPDATED = "groupsupdated";
    private static final Logger LOG = LoggerFactory.getLogger(FetchGroupsTask.class);
    private final String listId;

    public FetchGroupsTask(ComicStore comicStore) {
        super(comicStore);
        this.listId = ListID.GROUPS;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchGroupsTask other = (FetchGroupsTask) obj;
        if (listId == null) {
            if (other.listId != null) {
                return false;
            }
        } else if (!listId.equals(other.listId)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getGroupListUri();
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            ResponseParser parser = new ResponseParser();

            JSONArray jarray = null;
            try {
                jarray = json.getJSONArray("results");
            } catch (JSONException e) {
                LOG.error("Couldn't parse json array.");
            }

            parser.parseGroups(jarray, updater);

            final Intent intent = new Intent(GROUPS_UPDATED);
            LocalBroadcastManager.getInstance(IverseApplication.getApplication()).sendBroadcast(intent);

            server.updateFreshness(listId, Time.HOUR * 3);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update groups list");
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listId == null) ? 0 : listId.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(listId);
    }
}
