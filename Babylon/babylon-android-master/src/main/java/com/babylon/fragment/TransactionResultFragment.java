package com.babylon.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.utils.Constants;

/**
 * Fragment {@link android.support.v4.app.Fragment} for viewing and changing user parameters
 */
public class TransactionResultFragment extends BaseFragment implements View.OnClickListener {

    public static TransactionResultFragment getInstance(String resultText) {
        Bundle b = new Bundle();
        b.putString(Constants.TRANSUCTION_RESULT, resultText);

        TransactionResultFragment fragment = new TransactionResultFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_transaction_result, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.transaction_title);

        String resultText = getArguments().getString(Constants.TRANSUCTION_RESULT);

        TextView resultTextView = (TextView) view.findViewById(R.id.resultText);
        resultTextView.setText(resultText);

        view.findViewById(R.id.acceptButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptButton:
                onTopBarButtonPressed();
                break;
            default:
                break;
        }
    }
}