package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CityOrLocationDetailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 08.11.2015
 *
 * @author G Ajaykumar
 */
public class CityOrLocationsLoader extends DataAsyncTaskLibLoader<CityOrLocationDetailsResponse> {

    private String token;
    private String dictID;
    private String searchString;

    public CityOrLocationsLoader(final Context context, String token, String systemLookups, String userLookups) {
        super(context);
        this.token = token;
        this.dictID = systemLookups;
        this.searchString = userLookups;
    }

    @Override
    protected CityOrLocationDetailsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCityOrLocationsList(token, dictID, searchString);
    }
}