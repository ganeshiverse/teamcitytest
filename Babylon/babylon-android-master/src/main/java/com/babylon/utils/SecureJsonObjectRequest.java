package com.babylon.utils;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

/**
 * A request for retrieving a {@link JSONObject} response body at a given URL, allowing for an
 * optional {@link JSONObject} to be passed in as part of the request body.
 */
public class SecureJsonObjectRequest extends JsonObjectRequest
{
	Context mContext;
	
	public SecureJsonObjectRequest(int method,String url,Context context,JSONObject jsonRequest,Listener<JSONObject> listener, ErrorListener errorListener)
	{
		super(method, url, jsonRequest, listener, errorListener);
		
		mContext = context;
	}
	
	public SecureJsonObjectRequest(String url,Context context,JSONObject jsonRequest, Listener<JSONObject> listener, ErrorListener errorListener)
	{
        this(jsonRequest == null ? Method.GET : Method.POST, url,context,jsonRequest,listener,errorListener);

    	mContext = context;
    }


	@Override
	public Map<String, String> getHeaders() throws AuthFailureError
	{
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
		headers.put("Content-Type", "application/json");

		return headers;
	}

	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		try {
			if (response.data.length == 1) {
				byte[] responseData = "{}".getBytes("UTF8");
				response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return super.parseNetworkResponse(response);
	}

}