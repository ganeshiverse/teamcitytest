package com.iversecomics.client.store.db;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.util.text.StringUtils;

public class PublishersTableTest extends AbstractComicStoreProviderTestCase {
    private Publisher querySinglePublisher(String msg, Uri contentUri, String selection, String selectionArgs[]) {
        Cursor c = null;
        try {
            c = resolver.query(contentUri, null, selection, selectionArgs, null);
            StringBuilder msgb = new StringBuilder();
            msgb.append(msg);
            msgb.append(" (where=").append(selection);
            msgb.append(") (whereArgs=").append(StringUtils.join(selectionArgs, "|"));
            msgb.append(")");
            assertDatabaseRowCount(msgb.toString(), c, 1);
            Publisher publisher = PublishersTable.fromCursor(c);
            return publisher;
        } finally {
            DBUtil.close(c);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testPublisherInsertObject() {
        Publisher publisher = new Publisher();
        publisher.setPublisherId("test.publisher.object");
        publisher.setName("TestPublisherObject");

        ContentValues values = PublishersTable.asValues(publisher);

        Uri uri = resolver.insert(PublishersTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Publisher", uri);
    }

    @MediumTest
    public void testPublisherInsertSimple() {
        ContentValues values = new ContentValues();
        values.put(PublishersTable.PUBLISHERID, "test.publisher.simple");
        values.put(PublishersTable.NAME, "TestPublisher");

        Uri uri = resolver.insert(PublishersTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Publisher", uri);
    }

    @MediumTest
    public void testPublisherInsertThenInsert() {
        String id = "test.publisher";

        ContentValues values = new ContentValues();
        values.put(PublishersTable.PUBLISHERID, id);
        values.put(PublishersTable.NAME, "TestPublisher");

        Uri uri = resolver.insert(PublishersTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Publishers", uri);

        // Now try to find it.
        String selection = PublishersTable.PUBLISHERID + " = ?";
        String selectionArgs[] = new String[] { id };
        String msg = "Publisher (initial)";
        Publisher p1 = querySinglePublisher(msg, PublishersTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestPublisher", p1.getName());

        // Insert another entry, hopefully overwriting the original due to a proper constraint setup.
        ContentValues second = new ContentValues();
        second.put(PublishersTable.PUBLISHERID, id);
        second.put(PublishersTable.NAME, "Test Publisher Post"); // New name (with space)

        Uri uriSecond = resolver.insert(PublishersTable.CONTENT_URI, second);
        assertDatabaseUriHasDBId("Publishers", uriSecond);

        // Try to find it again.
        msg = "Publisher (post insert)";
        Publisher p2 = querySinglePublisher(msg, PublishersTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Publisher Post", p2.getName());
    }

    @MediumTest
    public void testPublisherInsertThenUpdate() {
        String id = "test.publisher";

        ContentValues values = new ContentValues();
        values.put(PublishersTable.PUBLISHERID, id);
        values.put(PublishersTable.NAME, "TestPublisher");

        Uri uri = resolver.insert(PublishersTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Publishers", uri);

        // Now try to find it.
        String selection = PublishersTable.PUBLISHERID + " = ?";
        String selectionArgs[] = new String[] { id };
        String msg = "Publisher (initial)";
        Publisher p1 = querySinglePublisher(msg, PublishersTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestPublisher", p1.getName());

        // Update entry
        ContentValues update = new ContentValues();
        update.put(PublishersTable.PUBLISHERID, id);
        update.put(PublishersTable.NAME, "Test Publisher Post"); // New name (with space)

        String where = PublishersTable.PUBLISHERID + " = ?";
        String whereArgs[] = new String[] { id };
        int count = resolver.update(PublishersTable.CONTENT_URI, update, where, whereArgs);
        assertEquals("Update count", 1, count);

        // Try to find it again.
        msg = "Publisher (post update)";
        Publisher p2 = querySinglePublisher(msg, PublishersTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Publisher Post", p2.getName());
    }

    @MediumTest
    public void testPublisherQueryByDBId() {
        String id = "test.publisher.query.bydbid";
        ContentValues values = new ContentValues();
        values.put(PublishersTable.PUBLISHERID, id);
        values.put(PublishersTable.NAME, "TestPublisher");

        Uri uri = resolver.insert(PublishersTable.CONTENT_URI, values);
        long dbId = getDatabaseId("Publishers", uri);

        Uri singleUri = ContentUris.withAppendedId(PublishersTable.CONTENT_URI, dbId);

        String msg = "Query Publisher by DB Id";
        Publisher p1 = querySinglePublisher(msg, singleUri, null, null);
        assertEquals(msg, id, p1.getPublisherId());
        assertEquals(msg, "TestPublisher", p1.getName());
    }

    @MediumTest
    public void testPublisherQueryByPublisherId() {
        String publisherId = "test.publisher.query.bypublisherid";
        ContentValues values = new ContentValues();
        values.put(PublishersTable.PUBLISHERID, publisherId);
        values.put(PublishersTable.NAME, "TestPublisher");

        Uri uri = resolver.insert(PublishersTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Publishers", uri);

        Uri singleUri = Uri.withAppendedPath(PublishersTable.CONTENT_URI_BY_PUBLISHERID, publisherId);

        String msg = "Query Publisher by Publisher Id";
        Publisher p1 = querySinglePublisher(msg, singleUri, null, null);
        assertEquals(msg, publisherId, p1.getPublisherId());
        assertEquals(msg, "TestPublisher", p1.getName());
    }
}
