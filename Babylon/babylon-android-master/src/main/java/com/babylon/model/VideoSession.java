package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class VideoSession extends BaseModel {

    public VideoSession() {

    }

    private String id;

    @SerializedName("unique_token")
    private String uniqueToken;

    private String session;

    @SerializedName("consultant_name")
    private String consultantName;

    @SerializedName("consultant_avatar_url")
    private String consultantAvatarUrl;

    @SerializedName("appointment_id")
    private String appointmentId;

    @SerializedName("enable_voice_calls")
    private boolean enableVoiceCalls;

    public String uniqueToken() {
        return uniqueToken;
    }

    public String session() {
        return session;
    }

    public String id() {
        return id;
    }

    public String consultantName() {
        return consultantName;
    }

    public String consultantAvatarUrl() {
        return consultantAvatarUrl;
    }

    public void setUniqueToken(String uniqueToken) {
        this.uniqueToken = uniqueToken;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String appointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public boolean enabledVoiceCalls() {
        return enableVoiceCalls;
    }
}
