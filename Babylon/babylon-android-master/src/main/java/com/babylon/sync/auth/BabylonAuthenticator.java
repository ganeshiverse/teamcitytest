package com.babylon.sync.auth;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.babylon.activity.SignInActivity;
import com.babylon.utils.L;

public class BabylonAuthenticator extends AbstractAccountAuthenticator {
    private static final String TAG = BabylonAuthenticator.class.getSimpleName();


    public static final String AUTH_TOKEN_TYPE = "WSSE_TOKEN";
    public static final String ACCOUNT_TYPE = "com.babylon.account";

    private final Context mContext;

    public BabylonAuthenticator(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse accountAuthenticatorResponse,
                                 String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse accountAuthenticatorResponse,
                             String s, String s2, String[] strings, Bundle options)
        throws NetworkErrorException {
        L.d(TAG, "start addAccount()");
        final Intent intent = new Intent(mContext, SignInActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);

        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse,
                                     Account account, Bundle bundle)
        throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse accountAuthenticatorResponse,
                               Account account, String authTokenType, Bundle bundle)
        throws NetworkErrorException {
//        final AccountManager accountManager = AccountManager.get(mContext);

//        String authToken = accountManager.peekAuthToken(account, authTokenType);
//        if (TextUtils.isEmpty(authToken)) {
//            pullAuthToken(account, authTokenType, accountManager);
//        }
        return null;
    }

//    private void pullAuthToken(Account account, String authTokenType, AccountManager accountManager) {
//        String authToken;
//        AuthRequest authRequest = new AuthRequest();
//        authRequest.setEmail(account.name);
//        authRequest.setPassword(accountManager.getPassword(account));
//
//        Patient response = (Patient) new AlliantsSessionsPost().withEntry(authRequest).execute().getData();
//        authToken = response.getWSSE();
//        accountManager.setAuthToken(account, authTokenType, authToken);
//        accountManager.setUserData(account, Constants.Header_RequestKeys.HEADER_WSSE_KEY, authToken);
//    }


    @Override
    public String getAuthTokenLabel(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse,
                                    Account account, String s, Bundle bundle)
        throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse accountAuthenticatorResponse,
                              Account account, String[] strings)
        throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }
}