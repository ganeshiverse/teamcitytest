package com.axsystech.excelicare.util;

/**
 * Created by someswarreddy on 30/06/15.
 */
public enum NavigationUtils {

    LOGIN("1"), /*Login*/
    CHANGE_PASSWORD("2"), /*Change Password*/
    SIGNUP("3"), /*SignUp*/
    VERIFY_OPT("4"), /*Verify OTP*/
    SITE_SELECTOR("5") /*Site Selector*/;

    private final String name;

    private NavigationUtils(String s) {
        this.name = s;
    }

    public boolean containsString(String otherName) {
        return (otherName == null) ? false : otherName.toLowerCase().contains(name.toLowerCase());
    }

    public NavigationUtils getNavigationType(String typeString) {
        if (LOGIN.containsString(typeString)) {
            return NavigationUtils.LOGIN;
        } else if (SIGNUP.containsString(typeString)) {
            return NavigationUtils.SIGNUP;
        } else if (CHANGE_PASSWORD.containsString(typeString)) {
            return NavigationUtils.CHANGE_PASSWORD;
        } else if(VERIFY_OPT.containsString(typeString)) {
            return NavigationUtils.VERIFY_OPT;
        } else if(SITE_SELECTOR.containsString(typeString)) {
            return NavigationUtils.SITE_SELECTOR;
        }

        return NavigationUtils.LOGIN;
    }

    public String toString() {
        return this.name.toLowerCase();
    }
}
