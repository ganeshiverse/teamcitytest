package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequest;
import com.babylon.api.request.base.Response;
import com.babylon.utils.L;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import java.lang.reflect.Type;

public class RubyResponseProcessor<O> extends BaseResponseProcessor<O> {

    public RubyResponseProcessor(Type outputType) {
        super(outputType);
    }

    @Override
    public Response<O> getResponse(HttpResponse httpResponse) {
        String stringResponse = getStringResponse(httpResponse);

        Response<O> result = new Response<O>();

        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine != null) {
            int statusCode = statusLine.getStatusCode();
            result.setStatus(statusCode);
            if (statusCode >= 300) {
                addParsedErrors(result, stringResponse);
            } else {
                Type type = getType();
                O data = getGson().fromJson(stringResponse, type);
                result.setData(data);
            }
        } else {
            addUnknownError(result);
        }
        L.d(AbsRequest.TAG, statusLine + " " + stringResponse);
        return result;
    }

    protected void addParsedErrors(Response<O> result, String jsonErrors) {
        addError(result, jsonErrors);
    }
}
