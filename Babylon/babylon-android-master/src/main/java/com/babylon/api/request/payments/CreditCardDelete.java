package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestDelete;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.database.PaymentCardOperationUtils;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;

public class CreditCardDelete extends AbsRequestDelete<Void> {
    private int paymentCardId;

    @Override
    protected String getUrl() {
        return Urls.getRuby(String.format(Urls.CREDIT_CARD_LIST_DELETE, paymentCardId));
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }


    public static CreditCardDelete getDeleteCreditCard(int paymentCardId) {
        final CreditCardDelete deleteCreditCard = new CreditCardDelete();

        deleteCreditCard.paymentCardId = paymentCardId;

        return deleteCreditCard;
    }

    @Override
    protected Response<Void> processResponse(HttpResponse httpResponse) {

        if(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT) {
            PaymentCardOperationUtils.getInstance().delete(paymentCardId);
        }

        return super.processResponse(httpResponse);
    }

    @Override
    protected AbsResponseProcessor<Void> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }
}
