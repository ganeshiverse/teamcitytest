package com.babylon.model;

public class NutrientByCategory {
    private NutrientCategory category;
    private int calories;

    public NutrientByCategory() {

    }

    public NutrientByCategory(NutrientCategory category, int calories) {
        this.calories = calories;
        this.category = category;
    }

    public NutrientCategory getCategory() {
        return category;
    }

    public void setCategory(NutrientCategory category) {
        this.category = category;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }
}
