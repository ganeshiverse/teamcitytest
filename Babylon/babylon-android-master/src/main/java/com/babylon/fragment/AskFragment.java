package com.babylon.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.MessageListAdapter;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.MessageSchema;
import com.babylon.model.FileEntry;
import com.babylon.model.Message;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.AudioManager;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.babylon.utils.SelectImageManager;
import com.babylon.utils.UIUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Show doctor client dialog
 */

public class AskFragment extends BaseFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    public static final String ASK_DATE_PATTERN = "dd MMM yyyy, HH:mm";
    private static final int MESSAGE_COUNT = 20;
    private static final String TAG = AskFragment.class.getSimpleName();
    private ListView messageListView;

    private EditText messageEditText;
    private LinearLayout mediaLinearLayout;
    private ImageView photoImageView;
    private Button takePhotoButton;
    private Button audioButton;
    private Button deleteAudioButton;
    private MessageListAdapter messageListAdapter;
    private byte[] photoByteArray;
    private byte[] audioByteArray;
    private AudioManager audioManager;
    private boolean isAudioRecording;
    private boolean isAudioPlaying;
    private int messageToShowCount = MESSAGE_COUNT;
    private List<Message> messages = new ArrayList<Message>();

    private static final int LOAD_MORE = 113;
    private static final int LOAD_WITH_SCROLL_BOTTOM = 114;

    private View headerView;

    private Toast toast;

    private ContentObserver observer = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            update();
            L.d(TAG, "onChange: " + uri);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ask, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        messageListView = (ListView) getView().findViewById(R.id.fragment_ask_list_view);

        headerView = getHeaderView();
        messageListView.addHeaderView(headerView);

        messageListAdapter = new MessageListAdapter(getActivity(), messages);
        messageListView.setAdapter(messageListAdapter);

        messageEditText = (EditText) getView().findViewById(R.id.fragment_ask_edit_text);
        mediaLinearLayout = (LinearLayout) getView().findViewById(R.id.fragment_ask_add_media_layout);
        photoImageView = (ImageView) getView().findViewById(R.id.fragment_ask_image_view);
        Button sendButton = (Button) getView().findViewById(R.id.fragment_ask_send_button);
        ImageButton mediaButton = (ImageButton) getView().findViewById(R.id.fragment_ask_photo_button);
        takePhotoButton = (Button) getView().findViewById(R.id.fragment_ask_take_photo_button);
        audioButton = (Button) getView().findViewById(R.id.fragment_ask_take_audio_button);
        deleteAudioButton = (Button) getView().findViewById(R.id.fragment_ask_take_delete_audio_button);

        sendButton.setOnClickListener(this);
        mediaButton.setOnClickListener(this);
        takePhotoButton.setOnClickListener(this);
        audioButton.setOnClickListener(this);
        deleteAudioButton.setOnClickListener(this);

        setTitle(getString(R.string.ask_fragment_title));

        audioManager = AudioManager.getInstance();
        getLoaderManager().initLoader(LOAD_WITH_SCROLL_BOTTOM, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        getActivity().getContentResolver().registerContentObserver(MessageSchema.CONTENT_URI, false, observer);
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        getActivity().getContentResolver().unregisterContentObserver(observer);
        super.onDetach();
    }

    private View getHeaderView() {
        View view = View.inflate(getActivity(), R.layout.button_more, null);
        view.findViewById(R.id.loadMoreBtn).setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioManager.onDestroy();
    }

    @Override
    protected void onTopBarButtonPressed() {
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loadMoreBtn:
                messageToShowCount = messageToShowCount + MESSAGE_COUNT;
                loadMore();
                break;
            case R.id.fragment_ask_send_button:
                if (App.getInstance().isConnection()) {
                    tryToSendMessage();
                } else {
                    App.getInstance().showWaitingMessage(R.string.offline_error);
                }
                break;
            case R.id.fragment_ask_photo_button:
                mediaLinearLayout.setVisibility(mediaLinearLayout.isShown() ? View.GONE : View.VISIBLE);
                break;
            case R.id.fragment_ask_take_photo_button:
                takePhoto();
                break;
            case R.id.fragment_ask_take_audio_button:
                manageAudio();
                break;
            case R.id.fragment_ask_take_delete_audio_button:
                deleteAudio();
                break;
            default:
                break;
        }
    }

    /* Listeners */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id) {
            case LOAD_MORE:
            case LOAD_WITH_SCROLL_BOTTOM:
                return new CursorLoader(getActivity(),
                        MessageSchema.CONTENT_URI_QUESTIONS_WITH_FILES, MessageSchema.QueryWithFiles.PROJECTION,
                        null, null,
                        MessageSchema.SORT_TIME_DESC + " LIMIT 0," + messageToShowCount);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        int id = cursorLoader.getId();
        boolean shouldScrollToBottom = id == LOAD_WITH_SCROLL_BOTTOM;
        switch (id) {
            case LOAD_WITH_SCROLL_BOTTOM:
            case LOAD_MORE:
                if (cursor != null && cursor.getCount() > 0) {
                    final List<Message> messagesList = Message.fromCursorWithFilesList(cursor);

                    int delta = messagesList.size() - messageListAdapter.getCount();
                    if (delta <= 0) {
                        messageListView.removeHeaderView(headerView);
                        delta = 0;
                    }

                    messages.clear();
                    messages.addAll(messagesList);
                    messageListAdapter.notifyDataSetChanged();

                    int scrollPosition = delta;
                    if (shouldScrollToBottom) {
                        scrollPosition = messagesList.size();
                    }
                    messageListView.setSelection(scrollPosition);
                    cursor.close();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        /* data is not available anymore*/
    }

    /* Methods */
    private void tryToSendMessage() {
        if (messageEditText != null && messageEditText.getText() != null) {
            final Message message = getMessage();

            final ValidationInfo validationInfo = verifyMessage(message);
            if(validationInfo.isValid) {
                showConfirmationDialog(message);
            } else {
                showErrorMessage(validationInfo);
            }
        }
    }

    private void showErrorMessage(ValidationInfo validationInfo) {
        if (toast!=null) {
            toast.cancel();
        }

        toast = Toast.makeText(getActivity(), validationInfo.errorMessage, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void syncMessage(Message message) {
        DatabaseOperationUtils.getInstance().saveMessage(message, getActivity().getContentResolver());
        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MESSAGES, SyncAdapter.SYNC_PUSH);
        messageEditText.setText("");
        deletePhoto();
        deleteAudio();
        mediaLinearLayout.setVisibility(View.GONE);
        UIUtils.hideSoftKeyboard(getActivity(), messageEditText);
        update();
    }

    private Message getMessage() {
        final String textMessage = messageEditText.getText().toString().trim();

        final Message message = new Message();
        message.setRandomUUID();
        message.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
        message.setText(textMessage);
        message.setDate(System.currentTimeMillis());
        message.setFiles(getFileEntries(message));
        return message;
    }

    private ValidationInfo verifyMessage(Message message) {
        final ValidationInfo validationInfo = new ValidationInfo(false);

        if (TextUtils.isEmpty(message.getText())) {
            final boolean hasAudio = containsAudio(message);

            if (hasAudio) {
                validationInfo.isValid = true;
            } else {
                validationInfo.errorMessage = getString(R.string.message_is_required);
            }
        } else {
            validationInfo.isValid = true;
        }

        return validationInfo;
    }

    private boolean containsAudio(Message message) {
        boolean containsAudio = false;

        for (int i = 0; i < message.getFiles().size(); i++) {
            final FileEntry file = message.getFiles().get(i);
            if (file.isAudio()) {
                containsAudio = true;
                break;
            }
        }

        return containsAudio;
    }

    private void showConfirmationDialog(final Message message) {
        AlertDialogHelper.getInstance().showAgreeDialog(getActivity(), getString(R.string.ask_fragment_notification),
                R.string.confirm_sending, R.string.cancel_sending,  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        syncMessage(message);
                    }
                }
        );
    }

    private void loadMore() {
        getLoaderManager().restartLoader(LOAD_MORE, null, this);
    }

    private void update() {
        getLoaderManager().restartLoader(LOAD_WITH_SCROLL_BOTTOM, null, this);
    }

    private List<FileEntry> getFileEntries(Message message) {
        List<FileEntry> files = new ArrayList<FileEntry>();
        if (photoByteArray != null) {
            FileEntry photo = new FileEntry();
            photo.setRandomUUID();
            photo.setData(photoByteArray);
            photo.setQuestionId(message.getId());
            photo.setDate(message.getDate());
            photo.setName("photo_" + photo.getId() + ".jpeg");
            photo.setMimetype("image");
            files.add(photo);
        }
        if (audioByteArray != null) {
            FileEntry audio = new FileEntry();
            audio.setRandomUUID();
            audio.setData(audioByteArray);
            audio.setQuestionId(message.getId());
            audio.setDate(message.getDate());
            audio.setName("audio_" + audio.getId() + ".m4a");
            audio.setMimetype("audio");
            files.add(audio);

        }
        return files;
    }

    private void takePhoto() {
        if (photoImageView.getDrawable() != null) {
            deletePhoto();
        } else {
            SelectImageManager.getInstance().showSelectImageDialog(getActivity(), false, false,
                    new SelectImageManager.OnImageCompleteListener() {
                @Override
                public void onImageComplete(final Bitmap bitmap) {
                    if (bitmap != null) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                setProgressBarVisible(true);
                            }

                            @Override
                            protected Void doInBackground(Void... params) {
                                photoByteArray = SelectImageManager.getInstance().getCompressedByByteSize(bitmap);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                photoImageView.setVisibility(View.VISIBLE);
                                photoImageView.setImageBitmap(bitmap);
                                setProgressBarVisible(false);
                            }
                        }.execute();
                        takePhotoButton.setText(R.string.ask_fragment_delete_photo);
                    }
                }
            });
        }
    }

    private void deletePhoto() {
        photoImageView.setImageDrawable(null);
        takePhotoButton.setText(R.string.ask_fragment_photo);
        photoByteArray = null;
    }

    private void manageAudio() {
        if (isAudioPlaying) {
            stopAudioPlaying();
        } else if (audioByteArray != null) {
            audioManager.playRecord(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    isAudioPlaying = false;
                    audioButton.setText(R.string.play);
                }
            });
            audioButton.setText(R.string.stop);
            isAudioPlaying = true;
        } else if (isAudioRecording) {
            audioManager.stopRecord(onRecordCompleteListener);
        } else {
            audioManager.startRecord(onRecordCompleteListener);
            isAudioRecording = true;
            audioButton.setText(R.string.stop);
        }
    }

    private AudioManager.OnRecordCompleteListener onRecordCompleteListener = new AudioManager
            .OnRecordCompleteListener() {
        @Override
        public void onRecordComplete(byte[] audioRecord) {
            if (audioRecord != null) {
                audioByteArray = Arrays.copyOf(audioRecord, audioRecord.length);
                audioButton.setText(R.string.play);
                deleteAudioButton.setVisibility(View.VISIBLE);
                isAudioRecording = false;
            }
        }

        @Override
        public void onRecordFail() {
            isAudioRecording = false;
            audioButton.setText(R.string.ask_fragment_audio);
        }
    };

    private void deleteAudio() {
        audioManager.stopPlaying();
        audioByteArray = null;
        audioButton.setText(R.string.ask_fragment_audio);
        deleteAudioButton.setVisibility(View.GONE);
        isAudioRecording = false;
        isAudioPlaying = false;
    }

    private void stopAudioPlaying() {
        audioManager.stopPlaying();
        isAudioPlaying = false;
        audioButton.setText(R.string.play);
    }



    class ValidationInfo {
        boolean isValid;
        String errorMessage;

        public ValidationInfo(boolean isValid) {
            this.isValid = isValid;
        }

    }
}
