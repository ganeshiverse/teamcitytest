package com.babylon.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.babylon.R;
import com.babylon.chart.OnLoadChartProgressListener;
import com.babylon.utils.Constants;
import com.babylon.view.ChartView;

public class OnlyChartFragment extends BaseMonitorMeParamsFragment
        implements OnLoadChartProgressListener {

    public interface OnMedicalKitClickListener {
        View.OnClickListener getOnKitClickListener();
    }

    private ChartView chartView;

    public static OnlyChartFragment getInstance(String titleName, boolean isMedicalKitExist,
                                                String description, long beginOfTheDayInMillis,
                                                String featureId) {
        return getInstance(titleName, isMedicalKitExist, description, beginOfTheDayInMillis,
                featureId, null, null);
    }

    public static OnlyChartFragment getInstance(String titleName, boolean isMedicalKitExist,
                                                String description, long beginOfTheDayInMillis,
                                                String featureId, Boolean showDateRadioGroup,
                                                String defaultPeriodId) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TITLE_NAME, titleName);
        bundle.putBoolean(Constants.IS_KIT_BUTTON_EXIST, isMedicalKitExist);
        bundle.putString(Constants.DESCRIPTION, description);
        bundle.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        bundle.putString(Constants.CHART_FEATURE_ID, featureId);
        if (defaultPeriodId != null) {
            bundle.putString(Constants.DEFAULT_PERIOD_ID, defaultPeriodId);
        }
        if (showDateRadioGroup != null) {
            bundle.putBoolean(Constants.SHOW_DATE_RADIO_GROUP, showDateRadioGroup);
        }

        OnlyChartFragment fragment = new OnlyChartFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_only_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String titleName = getArguments().getString(Constants.TITLE_NAME);
        boolean isMedicalKitExist = getArguments().getBoolean(Constants.IS_KIT_BUTTON_EXIST);
        String description = getArguments().getString(Constants.DESCRIPTION);
        String featureId = getArguments().getString(Constants.CHART_FEATURE_ID);
        String defaultPeriodId = getArguments().getString(Constants.DEFAULT_PERIOD_ID);
        boolean showDateRadioGroup = getArguments().getBoolean(Constants.SHOW_DATE_RADIO_GROUP, true);

        chartView = (ChartView) view.findViewById(R.id.chartView);
        chartView.setOnLoadChartProgressListener(this);
        chartView.setDateInMillis(dateInMillis);
        chartView.setFeatureId(featureId);
        chartView.setDefaultPeriodId(defaultPeriodId);
        chartView.setShowDateRadioGroup(showDateRadioGroup);
        chartView.setDescription(description);

        if (!isChartLoaded) {
            chartView.showChart();
        }
        isChartLoaded = true;

        if (!TextUtils.isEmpty(titleName)) {
            setTitle(titleName);
        }

        if (isMedicalKitExist && onMedicalKitClickListener != null) {
            setRightImageButton(onMedicalKitClickListener.getOnKitClickListener(),
                    R.drawable.ic_menu_kit);
        }
    }

    @Override
    public void onStartLoadChartFromServer() {
        setProgressBarVisible(true);
    }

    @Override
    public void onFinishLoadChartFromServer() {
        setProgressBarVisible(false);
    }

    /* Listeners */
    @Override
    protected void onTopBarButtonPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }



}