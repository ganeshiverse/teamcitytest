package com.babylon.model.push;

/**
 * Push notification json body model
 */
public class Push {

    private String action;
    private String title;
    private String alert;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    @Override
    public String toString() {
        return "ParseData{"
                + "action='" + action + '\''
                + ", title='" + title + '\''
                + ", alert='" + alert + '\''
                + '}';
    }
}
