package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ContentChangeTracker;
import com.iversecomics.client.store.db.PublishersTable;

public class FetchPublishersTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testFetchPublishers() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(PublishersTable.CONTENT_URI, this);

        ComicStore comicStore = getComicStore();
        FetchPublishersTask task = new FetchPublishersTask(comicStore);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, PublishersTable.CONTENT_URI);
        OurAsserts.assertGreaterThan("Publishers table rows", 5, actualCount);
    }
}
