package com.iversecomics.client.downloads.internal.net;

import android.net.Uri;

import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;

public interface DownloadMonitor {
    void downloadConnected(Uri uri, String title, long totalBytes, String etag);

    void downloadFailure(Uri uri, DownloadException failure);

    void downloadProgress(Uri uri, long currentByte);

    void downloadState(Uri uri, DownloadStatus state, String message);
}
