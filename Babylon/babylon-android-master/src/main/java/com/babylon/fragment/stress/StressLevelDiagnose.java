package com.babylon.fragment.stress;

import com.babylon.App;
import com.babylon.R;

public class StressLevelDiagnose {
    private final int NORMAL_STRESS_LEVEL = 20;
    private final int AVERAGE_STRESS_LEVEL = 52;

    private int selectedAnswers;
    private int numberOfQuestions;

    public StressLevelDiagnose(int selectedAnswers, int numberOfQuestions) {
        this.selectedAnswers = selectedAnswers;
        this.numberOfQuestions = numberOfQuestions;
    }

    public String getResultMessage() {
        final double percentage = getPercentage();
        final String percentageStr = String.valueOf((int) percentage);

        final String diagnose = getDiagnose(percentage);

        String result = String.format(App.getInstance().getString(R.string.stress_quiz_result), percentageStr,
                diagnose);
        return result;
    }

    private String getDiagnose(double percentage) {
        String diagnose;

        if (percentage < NORMAL_STRESS_LEVEL) {
            diagnose = App.getInstance().getString(R.string.stress_levels_are_most_likely_healthy);
        } else if (percentage > NORMAL_STRESS_LEVEL && percentage <= AVERAGE_STRESS_LEVEL) {
            diagnose = App.getInstance().getString(R.string.benefit_from_some_lifestyle_changes);
        } else {
            diagnose = App.getInstance().getString(R.string.benefit_from_significant_changes);
        }

        return diagnose;
    }

    private double getPercentage() {
        double oneHundrentPercent = 100;
        double result = (double) selectedAnswers / (double) numberOfQuestions * oneHundrentPercent;

        return result;
    }
}
