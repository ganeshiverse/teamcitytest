package com.iversecomics.app;

import com.iversecomics.archie.android.BuildConfig;

/**
 * These are Archie specific constants that are used in {@link com.iversecomics.app.ComicApp}.
 */
public class AppConstants {

    public static final String SOCIAL_NETWORKING_APP_NAME = "OVNI";

    public static final String DEV_SERVER_URL = "https://ovni.iverseapps.com/getConfig.php";
    public static final String PROD_SERVER_URL = "https://ovni.iverseapps.com/getConfig.php";
    public static final String SHARED_SECRET = "D57D9C76-7ADA-4905-8AD2-29D3D94121FD";


    public static final String SKU_PACKAGE_NAME = "com.iversecomics.ovni.android";

    public static final boolean DEBUG = BuildConfig.DEBUG;

    public static final String HOCKEY_APP_ID = "cd4fe3ea35dc2334015091d7b3fe73ff";

    public static final boolean PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU = false;

//    public static final boolean USE_SUBSCRIPTIONS = false;
//    public static final String DEFAULT_SUBSCRIPTION_SKUS = "";


    public static final boolean DISABLE_LOGGING = true;
    public static final boolean USE_SUBSCRIPTIONS = false;
    public static final String DEFAULT_SUBSCRIPTION_SKUS = "com.iversecomics.archie.unlimited.subscription.monthly";
    public static boolean SKU_SPECIFIC_TITLES = true;
    public static String ONDEMAND_BUTTON_TITLE = "Read Unlimited!";

    // As per the Sofia request and bug No : https://redmine.iverseapps.com/issues/5251#change-20490, we are enabling the subscription service for Archie and disabling in Comic plus.
    // True -  Enabled , False - didabled
    public static boolean ENABLE_SUBSCRIPTIONS_SERVICE = false;

    public static boolean ENABLE_TAPJOY = false;
    public static final boolean ENABLE_CRASH_REPORT = true;
}
