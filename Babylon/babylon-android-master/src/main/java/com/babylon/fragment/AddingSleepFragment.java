package com.babylon.fragment;

import android.app.TimePickerDialog;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.text.InputType;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.babylon.R;
import com.babylon.api.request.ActivityListGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.dialog.TimePickerDialogFragment;
import com.babylon.enums.ActivityType;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.monitor.PhysicalActivitySync;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.babylon.utils.UIUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddingSleepFragment extends BaseMonitorMeParamsLoaderFragment {

    private static final String TAG = AddingSleepFragment.class.getSimpleName();

    private final SimpleDateFormat sleepTimeFormat = new SimpleDateFormat("h:mm a", Locale.UK);

    private Date wentToBedTime;
    private Date gotUpTime;
    private PhysicalActivity physicalActivity;
    private ImageButton acceptButton;


    public static AddingSleepFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        AddingSleepFragment fragment = new AddingSleepFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_adding_sleep, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.add_sleep_record);

        if (getArguments() != null) {
            dateInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
        }

        String dateString = new MonitorMeDateViewController().getFormattedDay(new Date(dateInMillis));

        ((TextView) view.findViewById(R.id.dateTextView)).setText(dateString);
        ((RadioGroup) view.findViewById(R.id.radio_group_day)).setOnCheckedChangeListener(onDayCheckedListener);

        view.findViewById(R.id.edit_text_went_to_bed_time).setOnClickListener(onAddWentToBedClickListener);
        view.findViewById(R.id.edit_text_got_up_time).setOnClickListener(onGotUpClickListener);
        acceptButton = (ImageButton) view.findViewById(R.id.acceptButton);
        acceptButton.setOnClickListener(onAcceptClickListener);

        initErrorEditTexts(view);
    }

    private void initErrorEditTexts(View view) {
        final EditText gotUpEditText = (EditText) view.findViewById(R.id.edit_text_got_up_error);
        gotUpEditText.setInputType(InputType.TYPE_NULL);

        final EditText wentToBedEditText = (EditText) view.findViewById(R.id.edit_text_went_to_bed_error);
        wentToBedEditText.setInputType(InputType.TYPE_NULL);
    }

    private void addSleep() {
        DatabaseOperationUtils.getInstance().insertPhysicalActivity(physicalActivity, getActivity()
                .getContentResolver());
        syncSleep();
    }

    private void updateSleep() {
        DatabaseOperationUtils.getInstance().updatePhysicalActivity(physicalActivity,
                getActivity().getContentResolver());
        syncSleep();
    }

    private void syncSleep() {
        final Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, dateInMillis);
        b.putLong(Constants.EXTRA_MAX_DATE, com.babylon.utils.DateUtils.getDayStartMills(dateInMillis, 1));

        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY, b,
                SyncAdapter.SYNC_PUSH);
    }

    private boolean wasUpdated() {
        boolean result = true;

        if (physicalActivity != null) {
            boolean isGotUpTimeTheSame = physicalActivity.getEndDate().equals(gotUpTime);
            boolean isWentToBedTimeTheSame = physicalActivity.getStartDate().equals(wentToBedTime);
            boolean isRefreshedTheSame = isRefreshed() == physicalActivity.isRefreshed();

            if (isGotUpTimeTheSame && isWentToBedTimeTheSame
                    && isRefreshedTheSame) {
                result = false;
            }
        }
        return result;
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(long dateFrom) {
        long maxDate = com.babylon.utils.DateUtils.getDayStartMills(dateInMillis, 1);

            ActivityListGet.newActivityListGet(dateInMillis, maxDate).execute(getActivity(), activitiesResponseListener);

    }




    @Override
    protected void startLoaderManager()
    {
        getLoaderManager().initLoader(PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SLEEP, null, sleepLoader);
    }

    private void saveNewPhysicalActivity() {
        physicalActivity.setStartDate(wentToBedTime);
        physicalActivity.setEndDate(gotUpTime);

        boolean isRefreshed = isRefreshed();

        physicalActivity.setRefreshed(isRefreshed ? 1 : 0);
    }

    private boolean isRefreshed() {
        CheckBox checkbox = (CheckBox) getView().findViewById(R.id.check_box_refreshed);
        return checkbox.isChecked();
    }

    private boolean isTimeValid() {
        boolean isValid = false;

        if (wentToBedTime != null && gotUpTime != null) {
            isValid = wentToBedTime.before(gotUpTime) && !wentToBedTime.equals(gotUpTime);
        }

        return isValid;
    }

    private void onDaySetClickListener(RadioGroup group) {
        int selectedId = group.getCheckedRadioButtonId();
        switch (selectedId) {
            case R.id.radio_button_yesterday:
                if (gotUpTime != null && wentToBedTime != null && isToday(wentToBedTime)) {
                    setWentToBedDay(-DateUtils.DAY_IN_MILLIS);
                    verifyTimeValue();
                }
                setRadioButton(R.id.radio_button_today, android.R.color.white);
                setRadioButton(R.id.radio_button_yesterday, android.R.color.darker_gray);
                break;
            case R.id.radio_button_today:
                if (gotUpTime != null && wentToBedTime != null && !isSelectedDate(wentToBedTime.getTime())) {
                    setWentToBedDay(DateUtils.DAY_IN_MILLIS);
                    verifyTimeValue();
                }
                setRadioButton(R.id.radio_button_yesterday, android.R.color.white);
                setRadioButton(R.id.radio_button_today, android.R.color.darker_gray);
                break;
            default:
                L.d(TAG, "Unknown RadioButton ID + " + String.valueOf(selectedId));
        }
    }

    public boolean isSelectedDate(long wentToBedTime) {
        return dateInMillis == com.babylon.utils.DateUtils.getDayStartMills(wentToBedTime);
    }

    private void setWentToBedDay(long dayInMillis) {
        wentToBedTime.setTime(wentToBedTime.getTime() + dayInMillis);
    }

    private void setRadioButton(int radioButtonId, int textColorId) {
        RadioButton radio = (RadioButton) getView().findViewById(radioButtonId);
        radio.setTextColor(getResources().getColor(textColorId));
    }

    private void showTimePickerDialog(TimePickerDialog.OnTimeSetListener onWentToBedTimeListener,
                                      Date date) {
        final int hours = com.babylon.utils.DateUtils.getFieldValue(date, Calendar.HOUR_OF_DAY);
        final int minutes = com.babylon.utils.DateUtils.getFieldValue(date, Calendar.MINUTE);

        TimePickerDialogFragment dialog = TimePickerDialogFragment.getInstance(hours,
                minutes);
        dialog.setOnTimeSetListener(onWentToBedTimeListener);
        dialog.show(getFragmentManager(), TimePickerDialogFragment.TAG);
    }

    private Date getTime(int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(dateInMillis);

        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);

        return c.getTime();
    }

    private void showTime(Date date, int editTextId) {
        EditText textView = (EditText) getView().findViewById(editTextId);
        String timeText = sleepTimeFormat.format(date).toUpperCase();
        textView.setText(timeText);
    }

    private boolean isWentToBedYesterday() {
        RadioButton button = (RadioButton) getView().findViewById(R.id.radio_button_yesterday);
        return button.isChecked();
    }

    private View.OnClickListener onAddWentToBedClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showTimePickerDialog(onWentToBedTimeListener, wentToBedTime);
        }
    };

    private View.OnClickListener onGotUpClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showTimePickerDialog(onGotUpTimeListener, gotUpTime);
        }
    };

    private View.OnClickListener onAcceptClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isTimeFilledIn()) {
                verifyTimeValue();
                if (isTimeValid()) {
                    if (wasUpdated()) {
                        if (physicalActivity == null) {
                            physicalActivity = new PhysicalActivity(ActivityType.SLEEP);
                            saveNewPhysicalActivity();
                            addSleep();
                        } else if (physicalActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADD) {
                            saveNewPhysicalActivity();
                            updateSleep();
                        } else {
                            saveNewPhysicalActivity();
                            physicalActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_PATCH);
                            updateSleep();
                        }

                    }
                    AddingSleepFragment.super.onTopBarButtonPressed();
                }
            }
        }
    };

    private final TimePickerDialog.OnTimeSetListener
            onWentToBedTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            wentToBedTime = getTime(hourOfDay, minute);

            boolean isPm = isPm(wentToBedTime);

            if (isPm && !isWentToBedYesterday()) {
                RadioButton button = (RadioButton) getView().findViewById(R.id.radio_button_yesterday);
                button.setChecked(true);
            } else if (!isPm && isWentToBedYesterday()) {
                RadioButton button = (RadioButton) getView().findViewById(R.id.radio_button_today);
                button.setChecked(true);
            } else if (isPm && isWentToBedYesterday()) {
                setWentToBedDay(-DateUtils.DAY_IN_MILLIS);
            }

            showTime(wentToBedTime, R.id.edit_text_went_to_bed_time);

            if (wentToBedTime != null && gotUpTime != null) {
                verifyTimeValue();
            }
        }

        private boolean isPm(Date wentToBedTime) {
            boolean isPm = true;

            Calendar c = Calendar.getInstance();
            c.setTime(wentToBedTime);
            int value = c.get(Calendar.AM_PM);

            if (value == 0) {
                isPm = false;
            }

            return isPm;
        }
    };

    private final TimePickerDialog.OnTimeSetListener onGotUpTimeListener
            = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            gotUpTime = getTime(hourOfDay, minute);
            showTime(gotUpTime, R.id.edit_text_got_up_time);

            if (gotUpTime != null && wentToBedTime != null) {
                verifyTimeValue();
            }
        }
    };

    private boolean isTimeFilledIn() {
        boolean isTimeFilled = false;
        View view = getView();
        if (view != null) {
            final EditText wentToBedEditTextError = (EditText) view.findViewById(
                    R.id.edit_text_went_to_bed_error);
            final EditText gotUpEditTextError = (EditText) view.findViewById(
                    R.id.edit_text_got_up_error);

            final String wentToBedErrorText = getString(
                    R.string.error_went_to_bed_time_is_required);
            final String gotUpErrorText = getString(R.string.error_got_up_time_is_required);

            boolean isBadTimeFilled = verifyTimeIsRequired(wentToBedErrorText,
                    wentToBedEditTextError, wentToBedTime);
            boolean isGotUpTimeFilled = verifyTimeIsRequired(gotUpErrorText,
                    gotUpEditTextError, gotUpTime);
            isTimeFilled = isBadTimeFilled && isGotUpTimeFilled;
        }
        return isTimeFilled;
    }

    private boolean verifyTimeIsRequired(String errorText, EditText editText, Date date) {
        boolean isTimeFilled = true;
        if (date == null) {
            showEditTextError(errorText, editText);
            isTimeFilled = false;
        } else if (!TextUtils.isEmpty(editText.getError())) {
            editText.setError(null);
            acceptButton.setEnabled(true);
        }
        return isTimeFilled;
    }

    private void verifyTimeValue() {
        final EditText editText = (EditText) getView().findViewById(R.id.edit_text_got_up_error);

        if (DateUtils.isToday(dateInMillis)) {
            verifyTimeLessThanCurrent();
        }
        if (isTimeValid()) {
            editText.setError(null);
            acceptButton.setEnabled(true);
        } else {
            editText.setInputType(InputType.TYPE_NULL);
            String errorText = getString(R.string.error_sleep_time);
            showEditTextError(errorText, editText);
        }
    }

    private void verifyTimeLessThanCurrent() {
        final String timeLessThanCurrent = getString(R.string.error_select_time_less_than_current);

        final EditText gotUpEditTextError = (EditText) getView().findViewById(R.id.edit_text_got_up_error);
        final EditText wentToBedEditTextError = (EditText) getView().findViewById(R.id.edit_text_went_to_bed_error);

        verifyTimeEarlierThenNow(gotUpEditTextError, timeLessThanCurrent, gotUpTime);
        verifyTimeEarlierThenNow(wentToBedEditTextError, timeLessThanCurrent, wentToBedTime);
    }

    private void verifyTimeEarlierThenNow(EditText editTextError, String errorText, Date date) {
        if (date != null) {
            if (date.after(new Date())) {
                showEditTextError(errorText, editTextError);
            } else if (!TextUtils.isEmpty(editTextError.getError())) {
                editTextError.setError(null);
                acceptButton.setEnabled(true);
            }
        }
    }

    private void showEditTextError(String errorText, EditText editText) {
        final Drawable errorDrawable = getActivity().getResources()
                .getDrawable(R.drawable.ic_reqfield_error);
        errorDrawable.setBounds(0, 0, errorDrawable.getIntrinsicWidth(), errorDrawable.getIntrinsicHeight());
        editText.setError(errorText, errorDrawable);
        acceptButton.setEnabled(false);
    }

    private final RadioGroup.OnCheckedChangeListener onDayCheckedListener = new RadioGroup
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            onDaySetClickListener(group);
        }
    };

    private final LoaderManager.LoaderCallbacks<Cursor> sleepLoader = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(),
                    PhysicalActivitySchema.CONTENT_URI, null,
                    PhysicalActivitySchema.Query.DAY_SELECTION_SLEEPING,
                    PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                    null);
        }

        @Override
        public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {

            if (!UIUtils.isOnline(getActivity())) {
                getView().findViewById(R.id.radio_button_yesterday).setEnabled(true);
                getView().findViewById(R.id.radio_button_today).setEnabled(true);
            }

            if (data.moveToFirst()) {
                physicalActivity = new PhysicalActivity();
                physicalActivity.createFromCursor(data);

                configDayRadioButton();

                final CheckBox refreshedCheckBox = (CheckBox) getView().findViewById(R.id.check_box_refreshed);
                refreshedCheckBox.setChecked(physicalActivity.isRefreshed());

                if (physicalActivity.getStartDate() != null) {
                    wentToBedTime = new Date(physicalActivity.getStartDate().getTime());
                    showTime(wentToBedTime, R.id.edit_text_went_to_bed_time);
                }

                if (physicalActivity.getEndDate() != null) {
                    gotUpTime = new Date(physicalActivity.getEndDate().getTime());
                    showTime(gotUpTime, R.id.edit_text_got_up_time);
                }
            }

        }

        @Override
        public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {

        }
    };

    private void configDayRadioButton() {
        if (isToday(physicalActivity.getStartDate())) {
            RadioButton todayRadioButton = (RadioButton) getView().findViewById(R.id.radio_button_today);
            todayRadioButton.setChecked(true);
        }
    }


    private boolean isToday(Date date) {
        boolean result = false;

        if (date.getTime() >= dateInMillis) {
            result = true;
        }

        return result;
    }

    private class SaveActivitiesAsyncTask extends AsyncTask<Response<ResponseList<PhysicalActivity>>, Void, Void> {

        @Override
        protected Void doInBackground(Response<ResponseList<PhysicalActivity>>... params) {

            if (getActivity() != null && isVisible()) {
                long maxDate = com.babylon.utils.DateUtils.getDayStartMills(dateInMillis, 1);
                PhysicalActivitySync.processResponse(getActivity().getContentResolver(), params[0], dateInMillis,
                        maxDate);

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (getActivity() != null && isVisible()) {
                startLoaderManager();

                onLoadedMonitorMeParamsFromServer();
                setProgressBarVisible(false);
                setRightImageButtonVisible(true);
            }
        }
    }

    private final RequestListener<ResponseList<PhysicalActivity>> activitiesResponseListener =
            new RequestListener<ResponseList<PhysicalActivity>>() {
                @Override
                public void onRequestComplete(Response<ResponseList<PhysicalActivity>>
                                                      response) {
                    if (getActivity() != null) {
                        new SaveActivitiesAsyncTask().execute(response);
                    }
                }
            };

}
