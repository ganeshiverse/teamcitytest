package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by G Ajaykumar on 07/30/2015.
 */
public class MessageSendingResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private String messageResponseData;

    public String getMessageResponseData() {
        return messageResponseData;
    }
}