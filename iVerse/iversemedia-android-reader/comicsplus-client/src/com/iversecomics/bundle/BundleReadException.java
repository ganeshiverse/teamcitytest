package com.iversecomics.bundle;

import java.io.IOException;

public class BundleReadException extends IOException {
    private static final long serialVersionUID = -4696863272545273060L;
    private Throwable         cause;

    public BundleReadException(String message) {
        super(message);
    }

    public BundleReadException(String msgFormat, Object ... args) {
        super(String.format(msgFormat, args));
    }

    public BundleReadException(String message, Throwable cause) {
        super(message);
        this.cause = cause;
    }

    @Override
    public Throwable getCause() {
        return (cause==this ? null : cause);
    }
}
