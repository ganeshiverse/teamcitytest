package com.babylon.validator;

public class OnlyLetterAndDigitValidator extends BaseValidator {

    public OnlyLetterAndDigitValidator(BaseValidator nextValidator, String errorMessage) {
        super(nextValidator, errorMessage);
    }

    @Override
    public String validate(String string) {
        char[] charArray = string.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (!Character.isLetter(charArray[i]) && !Character.isDigit(charArray[i])) {
                return errorMessage;
            }
        }
        return null;
    }
}