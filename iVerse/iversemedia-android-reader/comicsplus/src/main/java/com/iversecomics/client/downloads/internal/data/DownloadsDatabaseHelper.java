package com.iversecomics.client.downloads.internal.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

class DownloadsDatabaseHelper extends SQLiteOpenHelper {
    private static final Logger LOG = LoggerFactory.getLogger(DownloadsDatabaseHelper.class);
    private Context context;


    public DownloadsDatabaseHelper(Context context) {
        super(context, DownloadsDB.DB_NAME, null, DownloadsDB.DB_VERSION);
        this.context = context;
        Log.i(DownloadsProvider.TAG, "DownloadsDatabaseHelper()");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(DownloadsProvider.TAG, "DownloadDatabaseHelper.onCreate()");
        StringBuilder sql;

        // Downloads Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(DownloadsTable.TABLE);
        sql.append(" ( ").append(DownloadsTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(DownloadsTable.NAME).append(" TEXT");
        sql.append(" , ").append(DownloadsTable.TIMESTAMP_CREATED).append(" LONG");
        sql.append(" , ").append(DownloadsTable.TIMESTAMP_UPDATED).append(" LONG");
        sql.append(" , ").append(DownloadsTable.TIMESTAMP_RETRY_AFTER).append(" LONG");
        sql.append(" , ").append(DownloadsTable.URI).append(" TEXT");
        sql.append(" , ").append(DownloadsTable.STATUS).append(" INTEGER");
        sql.append(" , ").append(DownloadsTable.STATUS_MESSAGE).append(" TEXT");
        sql.append(" , ").append(DownloadsTable.LOCAL_FILE).append(" TEXT");
        sql.append(" , ").append(DownloadsTable.CONTENT_LENGTH).append(" LONG");
        sql.append(" , ").append(DownloadsTable.CONTENT_PROGRESS).append(" LONG");
        sql.append(" , ").append(DownloadsTable.ETAG).append(" TEXT");
        sql.append(" , ").append(DownloadsTable.NUM_FAILED).append(" INTEGER");
        sql.append(" , ").append(DownloadsTable.SERVER_PAYLOAD).append(" TEXT");
        sql.append(" , ").append(DownloadsTable.COVER_IMAGE_FILENAME).append(" TEXT");
        sql.append(" );");

        db.execSQL(sql.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DownloadsProvider.TAG, "Upgrading from version " + oldVersion + " to " + newVersion
                + ", which will destroy all old data.");
        db.execSQL("DROP TABLE IF EXISTS " + DownloadsTable.TABLE);
        onCreate(db);
    }


    @Override
    public synchronized SQLiteDatabase getWritableDatabase() {
        try {
            return super.getWritableDatabase();
        } catch (SQLiteException e) {
            // that's our notification
            LOG.warn("Found an issue opening the database: " + e);
        }

        // try to delete the file
        context.deleteDatabase(DownloadsDB.DB_NAME);

        // now return a freshly created database
        return super.getWritableDatabase();
    }

}