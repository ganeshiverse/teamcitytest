package com.babylon.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import com.babylon.activity.BaseActivity;
import com.babylon.activity.RegistrationActivity;
import com.babylon.activity.TermsAndConditionsActivity;
import com.babylon.activity.family.FamilyAccountsActivity;
import com.babylon.activity.payments.PaymentCardsListActivity;
import com.babylon.activity.payments.RecordTransactionActivity;
import com.babylon.fragment.AboutAppFragment;
import com.babylon.fragment.InsuranceDetailsFragment;
import com.babylon.fragment.SettingsListFragment;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.Constants;
import com.babylon.utils.PreferencesManager;

public class SettingsActivity extends BaseActivity implements SettingsListFragment.SettingsFragmentListener {


    private static  String FAMILY_ACCOUNT="Family Accounts";
    private static  String PAYMENT_DETAILS="PaymentDetails";
    private static  String BILLING_INFO="Billing Information";
    private static  String SUBSCRIPTIONS="Subscriptions";
    private static  String INSURANCE_DETAILS="Insurance Details";
    private static  String TERMS="Terms and Conditions";
    private static  String SignOut="Sign Out";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().show();
        setupActionBar(ActionBarType.SETTINGS);
        startFragment(new SettingsListFragment(), false, SettingsListFragment.TAG);
    }

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

        }
    }


    private void loggedOut() {
        getIntent().putExtra(com.babylon.Keys
                .IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, false);

        PreferencesManager.getInstance().setPatientAddress(null);
        PreferencesManager.getInstance().setFirstMonitorMeLaunch(true);
        PreferencesManager.getInstance().resetAnswersCount();
        PreferencesManager.getInstance().resetAppLaunchCount();
        PreferencesManager.getInstance().resetNotificationsCount();
        PreferencesManager.getInstance().resetQuestionsAskedCount();

        SyncUtils.signOut();

        Intent intent=new Intent(this, RegistrationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @Override
    public void onItemClicked(String title) {

        Intent intent=null;
        switch(title){
            case "Family Accounts":  intent = new Intent(this, FamilyAccountsActivity.class);
                  startActivity(intent);
                  break;
            case "Payment Details":
                intent = new Intent(this, PaymentCardsListActivity.class);
                startActivity(intent);
                break;
            case "Billing Information":
                intent = new Intent(this, RecordTransactionActivity.class);
                intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.BILLINGS_FRAGMENT_TAG);
                startActivity(intent);
                break;
            case "Subscriptions":
                intent = new Intent(this, RecordTransactionActivity.class);
                intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.SUBSCRIPTIONS_FRAGMENT_TAG);
                startActivity(intent);
                break;
            case "Insurance Details":
                InsuranceDetailsFragment detailFragment = InsuranceDetailsFragment.newInstance();
                startFragment(detailFragment, InsuranceDetailsFragment.TAG);
                break;
            case "Terms and Conditions":
                intent = new Intent(this, TermsAndConditionsActivity.class);
                startActivity(intent);
                break;
            case "App Info":
                AboutAppFragment aboutFragment = AboutAppFragment.newInstance();
                startFragment(aboutFragment, AboutAppFragment.TAG);
                break;

            case "Sign Out":loggedOut();
                break;

        }

    }


}
