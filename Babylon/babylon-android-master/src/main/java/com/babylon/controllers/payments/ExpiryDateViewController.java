package com.babylon.controllers.payments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.controllers.DateViewController;
import com.babylon.controllers.OnDateSetListener;
import com.babylon.utils.DateUtils;
import com.babylon.view.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

public class ExpiryDateViewController extends DateViewController {
    public static final String EXPIRY_DATE_FORMAT = "MM/yy";

    public ExpiryDateViewController(Context context, TextView birthDateTextView, OnDateSetListener onDateSet, Date
            currentDate) {
        super(context, birthDateTextView, onDateSet, currentDate);
    }

    @Override
    public String getFormatDate() {
        return EXPIRY_DATE_FORMAT;
    }

    @Override
    protected String[] getVisiblePickers() {
        return new String[] {
                DatePickerDialog.MONTH,
                DatePickerDialog.YEAR };
    }

    public String getMonth() {
        final int month = DateUtils.getFieldValue(currentDate, Calendar.MONTH) + 1;
        return String.valueOf(month);
    }

    public String getYear() {
        final int year = DateUtils.getFieldValue(currentDate, Calendar.YEAR);
        return String.valueOf(year);
    }

    public boolean validateDate(EditText errorTextView) {
        boolean result = true;

        if (currentDate == null) {
            showError(errorTextView, context.getString(R.string.validator_field_is_empty_default));
            result = false;
        } else if (currentDate.after(DateUtils.getStartDate(new Date()))) {
            if (!TextUtils.isEmpty(errorTextView.getError())) {
                errorTextView.setError(null);
            }
        } else {
            showError(errorTextView, context.getString(R.string.add_payment_card_expiry_date_is_not_valid));
            result = false;
        }

        return result;
    }

    private void showError(EditText errorTextView, String text) {
        Drawable drawable = context.getResources().getDrawable(R.drawable.ic_reqfield_error);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        errorTextView.setError(text, drawable);
    }
}
