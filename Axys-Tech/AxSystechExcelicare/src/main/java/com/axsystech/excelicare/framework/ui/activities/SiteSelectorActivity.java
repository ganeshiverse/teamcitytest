package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.PrivacyPolicyResponse;
import com.axsystech.excelicare.data.responses.RegistrationDetailsByUserResponse;
import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.data.responses.ValidateSiteResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.loaders.CacheSystemPreferencesLoader;
import com.axsystech.excelicare.loaders.GetRegistrationDetailsByUserLoader;
import com.axsystech.excelicare.loaders.GetUserSiteDetailsDBLoader;
import com.axsystech.excelicare.loaders.GetUserSiteDetailsFromDBLoader;
import com.axsystech.excelicare.loaders.MsgInboxAndSentSearchLoader;
import com.axsystech.excelicare.loaders.PrivacyPolicyLoader;
import com.axsystech.excelicare.loaders.SaveUserDetailsAndClientUrlDBLoader;
import com.axsystech.excelicare.loaders.ValidateSiteCodeLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.NavigationUtils;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by someswar on 6/30/2015.
 */
public class SiteSelectorActivity extends BaseActivity implements View.OnClickListener, ConfirmationDialogFragment.OnConfirmListener {

    private SiteSelectorActivity mActivity;

    private static final String CACHE_USER_DETAILS_CLIENT_URL_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.CACHE_USER_DETAILS_CLIENT_URL_LOADER";
    private static final String CACHE_SYSTEM_PREFERENCES_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.CACHE_SYSTEM_PREFERENCES_LOADER";
    private static final String REGISTRATION_DETAILS_BY_USER_LOADER = SiteSelectorActivity.class.getName() + ".REGISTRATION_DETAILS_BY_USER_LOADER";
    private static final String VALIDATE_SITE_CODE_LOADER = SiteSelectorActivity.class.getName() + ".VALIDATE_SITE_CODE_LOADER";
    private static final String GET_USER_SITE_DETAILS_FROM_DB_LOADER = SiteSelectorActivity.class.getName() + ".GET_USER_SITE_DETAILS_FROM_DB_LOADER";

    private static final int SIGNUP_CONFIRMATION_DIALOG_ID = 102;

    private static final int CONFIRM_SITE_SELECTOR_DIALOG_ID = 103;

    private NavigationUtils mNavigatationUtils = NavigationUtils.SIGNUP;
    private String mInputEmailID = "";

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.searchGoBtn)
    private Button searchGoBtn;

    @InjectView(R.id.listView)
    private ListView sitesListView;

    @InjectView(R.id.nextBtn)
    private Button nextBtn;

    private RegistrationDetailsByUserResponse registerResponse;

    private ClientUrlAndUserDetailsDbModel selectedSiteDetailsObject;
    private UserSiteListAdapter userSiteListAdapter;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, getString(R.string.registered_sites_text)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_site_selector);
        mActivity = this;

        readIntentData();
        addEventListeners();

        preloadData();
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_NAVIGATE_TO)) {
                mNavigatationUtils = mNavigatationUtils.getNavigationType(bundle.getString(AxSysConstants.EXTRA_NAVIGATE_TO, mNavigatationUtils.SIGNUP.toString()));
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }
        }
    }

    private void addEventListeners() {
        searchGoBtn.setOnClickListener(mActivity);
        nextBtn.setOnClickListener(mActivity);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchEdit = null;

                    if (CommonUtils.hasInternet()) {
                        if (validateUserInputs()) {
                            // Validate the site id entered by user
                            if (!isFinishing()) {
                                displayProgressLoader(false);
                                try {
                                    searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                ValidateSiteCodeLoader validateSiteCodeLoader = new ValidateSiteCodeLoader(mActivity, mInputEmailID,
                                        CommonUtils.getDeviceId(getApplicationContext()), searchEdit);
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(VALIDATE_SITE_CODE_LOADER), validateSiteCodeLoader);
                            }
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                return false;
            }
        });
    }

    private void preloadData() {
        nextBtn.setEnabled(false);

        /*
         Get site and clientUrl details from Database.
          If there are multiple sites available display SiteId selector otherwise hide it.
          */
        if (!TextUtils.isEmpty(mInputEmailID)) {
            GetUserSiteDetailsFromDBLoader userSiteDetailsFromDBLoader = new GetUserSiteDetailsFromDBLoader(mActivity, mInputEmailID);
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER), userSiteDetailsFromDBLoader);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER)) {
            if (result != null && result instanceof List) {
                ArrayList<ClientUrlAndUserDetailsDbModel> clientUrlAndUserDetailsDbModelAL = (ArrayList<ClientUrlAndUserDetailsDbModel>) result;

                if (clientUrlAndUserDetailsDbModelAL != null && clientUrlAndUserDetailsDbModelAL.size() > 0) {
                    userSiteListAdapter = new UserSiteListAdapter(clientUrlAndUserDetailsDbModelAL);
                    sitesListView.setAdapter(userSiteListAdapter);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(VALIDATE_SITE_CODE_LOADER)) {
            if (result != null && result instanceof ValidateSiteResponse) {
                ValidateSiteResponse validateSiteResponse = (ValidateSiteResponse) result;

                if (validateSiteResponse.getUserSiteDetailsObject() != null) {
                    UsersDetailsObject usersDetailsObject = validateSiteResponse.getUserSiteDetailsObject();
                    usersDetailsObject.setLoginName(mInputEmailID);

                    // Convert the site details response model to DB model and then display it in Sites ListView
                    ClientUrlAndUserDetailsDbModel clientUrlDbModel = ConverterResponseToDbModel.getUserDetailsDbModel(mInputEmailID, usersDetailsObject);

                    if (clientUrlDbModel != null) {
                        if (userSiteListAdapter == null) {
                            ArrayList<ClientUrlAndUserDetailsDbModel> clientUrlAndUserDetailsDbModelAL = new ArrayList<ClientUrlAndUserDetailsDbModel>();
                            clientUrlAndUserDetailsDbModelAL.add(clientUrlDbModel);

                            userSiteListAdapter = new UserSiteListAdapter(clientUrlAndUserDetailsDbModelAL);
                            sitesListView.setAdapter(userSiteListAdapter);
                        } else {
                            userSiteListAdapter.updateNewSiteDetails(clientUrlDbModel);
                        }
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER)) {
            if (result != null && result instanceof RegistrationDetailsByUserResponse) {
                registerResponse = (RegistrationDetailsByUserResponse) result;

                // Cache user details in device DB
                if (registerResponse.getDataObject() != null &&
                        registerResponse.getDataObject().getUserDetailsAL() != null &&
                        registerResponse.getDataObject().getUserDetailsAL().size() > 0) {

                    SaveUserDetailsAndClientUrlDBLoader saveUserDetailsAndClientUrlDBLoader = new SaveUserDetailsAndClientUrlDBLoader(mActivity,
                            mInputEmailID, registerResponse.getDataObject().getUserDetailsAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER), saveUserDetailsAndClientUrlDBLoader);

                } else if (registerResponse.getDataObject() != null &&
                        registerResponse.getDataObject().getSystemPreferences() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL().size() > 0) {

                    // Start caching System Preference details in DB so that it will be used in offline mode
                    CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                            registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
                } else {
                    // Here decide navigation to further screens based on 'NavigateTo' value received from server
                    decideNavigation(registerResponse);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER)) {
            if (result != null && result instanceof Boolean) {
                Boolean statusFlag = (Boolean) result;

                // Start caching System Preference details in DB so that it will be used in offline mode
                if (registerResponse.getDataObject() != null &&
                        registerResponse.getDataObject().getSystemPreferences() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL().size() > 0) {

                    CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                            registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
                } else {
                    // Here decide navigation to further screens based on 'NavigateTo' value received from server
                    decideNavigation(registerResponse);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                ArrayList<SystemPreferenceDBModel> systemPreferenceDBModelAL = (ArrayList<SystemPreferenceDBModel>) result;

                // Cache SystemPreference data in Global Object
                GlobalDataModel.getInstance().setSystemPreferencesAL(systemPreferenceDBModelAL);
            }

            // Here decide navigation to further screens based on 'NavigateTo' value received from server
            decideNavigation(registerResponse);
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if (id == getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER)) {
            // do nothing
        } else if (id == getLoaderHelper().getLoaderId(VALIDATE_SITE_CODE_LOADER)) {
            showToast(getString(R.string.error_validating_sitecode));
        } else if (id == getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER)) {
            showToast(R.string.invalid_server_response);
        } else if (id == getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER)) {
            // Start caching System Preference details in DB so that it will be used in offline mode
            if (registerResponse.getDataObject() != null &&
                    registerResponse.getDataObject().getSystemPreferences() != null &&
                    registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL() != null &&
                    registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL().size() > 0) {

                CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
            } else {
                // Here decide navigation to further screens based on 'NavigateTo' value received from server
                decideNavigation(registerResponse);
            }
        } else if (id == getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER)) {
            // Here decide navigation to further screens based on 'NavigateTo' value received from server
            decideNavigation(registerResponse);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.searchGoBtn:
                String searchEdit = null;
                if (validateUserInputs()) {
                    // Validate the site id entered by user
                    if (!isFinishing()) {
                        displayProgressLoader(false);
                        try {
                            searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        ValidateSiteCodeLoader validateSiteCodeLoader = new ValidateSiteCodeLoader(mActivity, mInputEmailID,
                                CommonUtils.getDeviceId(getApplicationContext()), searchEdit);
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(VALIDATE_SITE_CODE_LOADER), validateSiteCodeLoader);
                    }
                }
                break;

            case R.id.nextBtn:
                if (selectedSiteDetailsObject == null) {
                    showToast(getString(R.string.select_site));
                } else {
                    final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                    final Bundle bundle = new Bundle();
                    bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, String.format(getString(R.string.confirm_site_selection), selectedSiteDetailsObject.getSiteName()));
                    bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, CONFIRM_SITE_SELECTOR_DIALOG_ID);
                    bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                    confirmationDialogFragment.setArguments(bundle);
                    showDialog(confirmationDialogFragment);
                }
                break;
        }
    }

    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(searchEditText.getText().toString().trim())) {
            showToast(R.string.input_site_code);
            searchEditText.requestFocus();

            return false;
        }

        return true;
    }

    private class UserSiteListAdapter extends BaseAdapter {

        private View previousSelectedView;

        private ArrayList<ClientUrlAndUserDetailsDbModel> clientUrlAndUserDetailsDbModelAL;

        public UserSiteListAdapter(ArrayList<ClientUrlAndUserDetailsDbModel> clientUrlAndUserDetailsDbModelAL) {
            this.clientUrlAndUserDetailsDbModelAL = clientUrlAndUserDetailsDbModelAL;
        }

        public ArrayList<ClientUrlAndUserDetailsDbModel> getAdapterDataSource() {
            return this.clientUrlAndUserDetailsDbModelAL;
        }

        public void updateNewSiteDetails(ClientUrlAndUserDetailsDbModel newSiteDetailsObj) {
            // check if new site details already added to listview, if not add it
            if (clientUrlAndUserDetailsDbModelAL != null && clientUrlAndUserDetailsDbModelAL.size() > 0) {
                boolean isAlreadyAdded = false;

                for (int idx = 0; idx < clientUrlAndUserDetailsDbModelAL.size(); idx++) {
                    ClientUrlAndUserDetailsDbModel indexedObj = clientUrlAndUserDetailsDbModelAL.get(idx);

                    if (!TextUtils.isEmpty(indexedObj.getSiteId())) {
                        if (indexedObj.getSiteId().trim().equalsIgnoreCase(newSiteDetailsObj.getSiteId())) {
                            isAlreadyAdded = true;
                            break;
                        }
                    }
                }

                // if not added already
                if (!isAlreadyAdded) {
                    clientUrlAndUserDetailsDbModelAL.add(newSiteDetailsObj);
                    notifyDataSetChanged();
                }
            }
        }

        @Override
        public int getCount() {
            return (clientUrlAndUserDetailsDbModelAL != null && clientUrlAndUserDetailsDbModelAL.size() > 0) ?
                    clientUrlAndUserDetailsDbModelAL.size() : 0;
        }

        @Override
        public ClientUrlAndUserDetailsDbModel getItem(int position) {
            return clientUrlAndUserDetailsDbModelAL.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ClientUrlAndUserDetailsDbModel rowObj = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.user_site_list_row, parent, false);
            }

            ImageView siteImageView = (ImageView) convertView.findViewById(R.id.siteImageView);
            TextView siteNameTextView = (TextView) convertView.findViewById(R.id.siteNameTextView);
            TextView siteInfoTextView = (TextView) convertView.findViewById(R.id.siteInfoTextView);
            TextView siteDescriptionTextView = (TextView) convertView.findViewById(R.id.siteDescriptionTextView);

            Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

            siteNameTextView.setTypeface(bold);

            if (!TextUtils.isEmpty(rowObj.getSitePhotoUrl())) {
                Picasso.with(mActivity)
                        .load(rowObj.getSitePhotoUrl())
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .into(siteImageView);
            }

            siteNameTextView.setText(!TextUtils.isEmpty(rowObj.getSiteName()) ? rowObj.getSiteName() : "");
            siteDescriptionTextView.setText(!TextUtils.isEmpty(rowObj.getSiteDescription()) ? rowObj.getSiteDescription() : "");

            siteInfoTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // navigate device default browser to view site details
                    if (!TextUtils.isEmpty(rowObj.getSiteInfoLink())) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rowObj.getSiteInfoLink()));
                        startActivity(browserIntent);
                    }
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedSiteDetailsObject = rowObj;
                    nextBtn.setEnabled(true);

                    if (previousSelectedView != null) {
                        previousSelectedView.setSelected(false);
                    }
                    view.setSelected(true);
                    previousSelectedView = view;
                }
            });

            return convertView;
        }
    }

    private void decideNavigation(RegistrationDetailsByUserResponse registerResponse) {
        try {
            if (registerResponse != null) {
                if (!TextUtils.isEmpty(registerResponse.getDataObject().getNavigateTo())) {
                    if (NavigationUtils.LOGIN.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        startActivity(loginIntent);
                        finish();

                    } else if (NavigationUtils.CHANGE_PASSWORD.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to CHANGE PASSWORD screen
                        Intent changePwdIntent = new Intent(this, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID,
                                selectedSiteDetailsObject != null ? selectedSiteDetailsObject.getSiteId() : AxSysConstants.DEFAULT_SITE_ID);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_change_password));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, getTokenForGivenUser(mInputEmailID, registerResponse));
                        startActivity(changePwdIntent);

                    } else if (NavigationUtils.SIGNUP.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Alert user for signup confirmation
                        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                        final Bundle bundle = new Bundle();
                        bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.signup_confirmation_message));
                        bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, SIGNUP_CONFIRMATION_DIALOG_ID);
                        bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                        confirmationDialogFragment.setArguments(bundle);
                        showDialog(confirmationDialogFragment);

                    } else if (NavigationUtils.VERIFY_OPT.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to CHANGE PASSWORD screen for Verifying OTP
                        Intent changePwdIntent = new Intent(this, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID,
                                selectedSiteDetailsObject != null ? selectedSiteDetailsObject.getSiteId() : AxSysConstants.DEFAULT_SITE_ID);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_verify_otp));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, getTokenForGivenUser(mInputEmailID, registerResponse));
                        startActivity(changePwdIntent);

                    } else if (NavigationUtils.SITE_SELECTOR.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to Site Selector Screen
                        Intent siteSelctionIntent = new Intent(this, SiteSelectorActivity.class);
                        siteSelctionIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.registered_sites_text));
                        siteSelctionIntent.putExtra(AxSysConstants.EXTRA_NAVIGATE_TO, NavigationUtils.SIGNUP.toString());
                        siteSelctionIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        startActivity(siteSelctionIntent);
                    } else {
                        // IMPORTANT: Default case
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        startActivity(loginIntent);
                        finish();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getTokenForGivenUser(String emailId, RegistrationDetailsByUserResponse registerResponse) {
        if (registerResponse != null && registerResponse.getDataObject() != null) {
            if (registerResponse.getDataObject().getUserDetailsAL() != null &&
                    registerResponse.getDataObject().getUserDetailsAL().size() > 0) {

                ArrayList<UsersDetailsObject> userDetailsAL = registerResponse.getDataObject().getUserDetailsAL();
                for (int idx = 0; idx < userDetailsAL.size(); idx++) {
                    UsersDetailsObject indexedObj = userDetailsAL.get(idx);

                    if (!TextUtils.isEmpty(indexedObj.getLoginName()) && indexedObj.getLoginName().trim().equalsIgnoreCase(emailId)) {
                        return (!TextUtils.isEmpty(indexedObj.getToken())) ? indexedObj.getToken() : "";
                    }
                }
            }
        }

        return "";
    }

    private void navigateToPrivacyPolicyOrSignUp() {
        if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                CommonUtils.shouldDisplayPrivacyPolicy(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            // Navigate to PRIVACY POLICY screen
            Intent signUpIntent = new Intent(this, PrivacyPolicyActivity.class);
            signUpIntent.putExtra(AxSysConstants.EXTRA_NAVIGATE_TO, NavigationUtils.SIGNUP.toString());
            signUpIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, selectedSiteDetailsObject.getSiteId());
            signUpIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
            startActivity(signUpIntent);
        } else {
            // Navigate user to SIGNUP screen
            Intent signUpIntent = new Intent(this, SignUpActivity.class);
            signUpIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
            signUpIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, selectedSiteDetailsObject.getSiteId());
            startActivity(signUpIntent);
        }
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == SIGNUP_CONFIRMATION_DIALOG_ID) {
            // Check SystemPreferences in-order to navigate Privacy Policy or SignUp Screen
            navigateToPrivacyPolicyOrSignUp();
        } else if (dialogId == CONFIRM_SITE_SELECTOR_DIALOG_ID) {
            // Again call 'GetRegistrationDetailsByUser' api to proceed further with the selected SiteID
            if (!isFinishing()) {
                displayProgressLoader(false);

                final GetRegistrationDetailsByUserLoader registrationDetailsByUserLoader = new GetRegistrationDetailsByUserLoader(mActivity,
                        mInputEmailID, CommonUtils.getDeviceId(getApplicationContext()),
                        selectedSiteDetailsObject.getSiteId(), "0");
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER), registrationDetailsByUserLoader);
            }
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == SIGNUP_CONFIRMATION_DIALOG_ID) {

        } else if (dialogId == CONFIRM_SITE_SELECTOR_DIALOG_ID) {

        }
    }

}
