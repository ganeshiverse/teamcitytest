package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.database.PaymentCardOperationUtils;
import com.babylon.model.Wsse;
import com.babylon.model.payments.PaymentCard;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;

public class CreditCardsListGet extends AbsRequestGet<PaymentCard[]> {
    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.CREDIT_CARD_LIST_GET);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<PaymentCard[]> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }

    @Override
    protected Response<PaymentCard[]> processResponse(HttpResponse httpResponse) {
        Response<PaymentCard[]> result = super.processResponse(httpResponse);

        if(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            PaymentCardOperationUtils.getInstance().save(result.getData());
        }

        return result;
    }
}
