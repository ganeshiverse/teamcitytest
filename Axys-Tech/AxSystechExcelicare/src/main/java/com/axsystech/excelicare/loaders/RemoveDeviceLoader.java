package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 9/23/2015.
 */
public class RemoveDeviceLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String userDeviceId;
    private String deviceID;

    public RemoveDeviceLoader(Context context, String deviceID, String userDeviceId, String token) {
        super(context);
        this.token = token;
        this.deviceID = deviceID;
        this.userDeviceId = userDeviceId;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getRemoveDevice(deviceID, userDeviceId, token);
    }
}
