package com.babylonpartners.babylon;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;

import com.babylon.R;

import java.util.Calendar;
import java.util.Date;

public class TimePickerDialog {

    private int hours;
    private int minutes;
    private int year;
    private int month;
    private int day;

    private TimePicker timePicker;
    private DatePicker datePicker;

    private TimePickerDialogHandler handler;

    private Calendar minDate = null;
    private Calendar maxDate = null;

    public TimePickerDialog(Activity activity, Date startDate, TimePickerDialogHandler timePickerDialogHandler) {
        handler = timePickerDialogHandler;

        createTenMinutePickerDialog(activity).show();
        setDateTime(startDate);
    }

    private void setDateTime(Date dateTime) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(dateTime);
        setYear(calendar.get(Calendar.YEAR));
        setMonth(calendar.get(Calendar.MONTH));
        setDay(calendar.get(Calendar.DAY_OF_MONTH));
        setHours(calendar.get(Calendar.HOUR_OF_DAY));
        setMinutes(calendar.get(Calendar.MINUTE));

        updateDisplay();
    }

    private void updateDisplay() {
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            }
        });

        timePicker.setCurrentHour(getHours());
        timePicker.setCurrentMinute(getMinutes() / 10);

        datePicker.init(getYear(), getMonth(), getDay(), mDateChangedListener);

        timePicker.setOnTimeChangedListener(mTimePickerListener);
    }

    public Dialog createTenMinutePickerDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        try {
            view = inflater.inflate(R.layout.dialog_datetime_picker, null);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        timePicker = (TimePicker) view.findViewById(R.id.dialogTimePicker);
        datePicker = (DatePicker) view.findViewById(R.id.dialogDatePicker);

        timePicker.setIs24HourView(true);

        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, getYear());
                        cal.set(Calendar.MONTH, getMonth());
                        cal.set(Calendar.DAY_OF_MONTH, getDay());
                        cal.set(Calendar.HOUR_OF_DAY, getHours());
                        cal.set(Calendar.MINUTE, getMinutes());
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        Date date = cal.getTime();
                        handler.onDateTimePicked(date);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    public void setMinDate(Date minDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(minDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        datePicker.setMinDate(cal.getTime().getTime());
        this.minDate = Calendar.getInstance();
        this.minDate.setTime(minDate);
    }

    public void setMaxDate(Date maxDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(maxDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        datePicker.setMaxDate(cal.getTime().getTime());
        this.maxDate = Calendar.getInstance();
        this.maxDate.setTime(maxDate);
    }

    private DatePicker.OnDateChangedListener mDateChangedListener = new OnDateChangedListener() {
        @Override
        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            setYear(year);
            setMonth(monthOfYear);
            setDay(dayOfMonth);
        }
    };

    private TimePicker.OnTimeChangedListener mTimePickerListener = new TimePicker.OnTimeChangedListener() {
        public void onTimeChanged(TimePicker timePicker, int hourOfDay, int minute) {
            //If a min or max time have been set then check if the time is within these bounds
            boolean validTime = true;
            if(TimePickerDialog.this.maxDate != null) {
                int maxHour = TimePickerDialog.this.maxDate.get(Calendar.HOUR_OF_DAY);
                int maxMinutes = TimePickerDialog.this.maxDate.get(Calendar.MINUTE);
                if(hourOfDay > maxHour) {
                    validTime = false;
                } else if(hourOfDay == maxHour) {
                    validTime = minute < maxMinutes;
                }
            }
            if(TimePickerDialog.this.minDate != null && validTime) {
                int minHour = TimePickerDialog.this.minDate.get(Calendar.HOUR_OF_DAY);
                int minMinutes = TimePickerDialog.this.minDate.get(Calendar.MINUTE);
                if(hourOfDay < minHour) {
                    validTime = false;
                } else if(hourOfDay == minHour) {
                    validTime = minute > minMinutes;
                }
            }

            if(validTime) {
                setHours(hourOfDay);
                setMinutes(minute * 10);
            } else {
                //Update the display back to the previous time
                updateDisplay();
            }
        }
    };

    public interface TimePickerDialogHandler {
        void onDateTimePicked(Date date);
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

}
