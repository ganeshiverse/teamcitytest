package com.babylon.enums;

public enum CheckGender {
    MALE(1), FEMALE(2), ALL(0);
    public final int checkValue;

    CheckGender(int checkValue) {
        this.checkValue = checkValue;
    }
}
