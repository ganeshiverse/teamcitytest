package com.babylon.database.schema.check;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;

import static com.babylon.database.check.CheckTables.BODY_PART_COLOR;

public interface BodyPartColorSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("check_entity").appendPath(BODY_PART_COLOR.getName()).build();
    Uri BODY_PART_BY_RGB_CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("body_part_by_color").build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        RGB("rgb", DBType.TEXT),
        BODY_PART_ID("bodyPartId", DBType.TEXT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public interface Query {

        String[] PROJECTION = {
                BODY_PART_COLOR + "." + Columns._ID.getName(),
                BODY_PART_COLOR + "." + Columns.ID.getName(),
                BODY_PART_COLOR + "." + Columns.SYNC_WITH_SERVER.getName(),
                BODY_PART_COLOR + "." + Columns.RGB.getName(),
                BODY_PART_COLOR + "." + Columns.BODY_PART_ID.getName()
        };


        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int RGB = 3;
        int BODY_PART_ID = 4;

    }
}
