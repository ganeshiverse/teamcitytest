package com.babylon.model;

import com.babylon.sync.SyncUtils;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Food extends ServerModel {

    @SkipFieldInContentValues
    private static final long serialVersionUID = 42L;

    private Integer id;

    private String title;

    private Integer calories;

    @SerializedName("weight")
    private Float portionWeight;

    private transient Float addPortionWeight;

    private transient String portionDetails;

    private Date date;

    private Integer count;

    private Integer nutrientId;

    private Integer category;

    public Food getServerModel() {
        Food food = new Food();

        food.title = title;
        food.calories = calories;
        food.portionWeight = portionWeight;
        food.addPortionWeight = addPortionWeight;
        food.portionDetails = portionDetails;
        food.date = date;
        food.count = count;
        food.category = category;

        //server reject extra fields
        food.id = null;
        food.portionWeight = null;
        food.portionDetails = null;
        food.nutrientId = null;

        return food;
    }

    public Food() {
        id = getRandomUUIDInt();
        syncWithServer = SyncUtils.SYNC_TYPE_ADD;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Float getPortionWeight() {
        return portionWeight;
    }

    public void setPortionWeight(Float portionWeight) {
        this.portionWeight = portionWeight;
    }

    public Float getAddPortionWeight() {
        return addPortionWeight;
    }

    public void setAddPortionWeight(Float addPortionWeight) {
        this.addPortionWeight = addPortionWeight;
    }

    public String getPortionDetails() {
        return portionDetails;
    }

    public void setPortionDetails(String portionDetails) {
        this.portionDetails = portionDetails;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getNutrientId() {
        return nutrientId;
    }

    public void setNutrientId(Integer nutrientId) {
        this.nutrientId = nutrientId;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public static class FoodList {
        private List<Food> nutrients;

        public List<Food> getNutrients() {
            return nutrients;
        }

        public void setNutrients(List<Food> nutrients) {
            this.nutrients = nutrients;
        }
    }
}
