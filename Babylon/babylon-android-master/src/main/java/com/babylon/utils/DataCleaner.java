package com.babylon.utils;

import android.content.Context;

import com.babylon.database.DatabaseHelper;
import com.babylon.database.check.CheckDatabaseHelper;
import com.babylon.sync.SyncUtils;

import java.io.File;

public class DataCleaner {

    public static final String TAG = DataCleaner.class.getSimpleName();

    public void clearAll(Context context) {
        context.getContentResolver().cancelSync(null, null);
        SyncUtils.removeAccounts(context);

        PreferencesManager.getInstance().clear();
        DatabaseHelper.deleteDataBase(context);
        CheckDatabaseHelper.deleteDataBase(context);
        deleteCache(context);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            L.e(TAG, e.getMessage(), e);
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
