package com.babylon.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.appsflyer.AppsFlyerLib;
import com.babylon.App;
import com.babylon.Config;
import com.babylon.Keys;
import com.babylon.PushBroadcastReceiver;
import com.babylon.R;
import com.babylon.activity.payments.PromotionTransactionActivity;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.FacebookPost;
import com.babylon.api.request.FacebookSessionsPost;
import com.babylon.api.request.RegionsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.database.DatabaseHelper;
import com.babylon.model.FacebookToken;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Redemption;
import com.babylon.model.payments.Subscription;
import com.babylon.social.FacebookManager;
import com.babylon.sync.SyncUtils;
import com.babylon.sync.auth.BabylonAuthenticator;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylonpartners.babylon.HomePageActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;


public class RegistrationActivity extends FragmentActivity implements View.OnClickListener,FacebookManager.OnFbLoginStatusListener
{
    public enum SignInType {
        EMAIL,
        FACEBOOK
    }

    private LinearLayout mParentLayout;
    private boolean mTransitionComplete;
    private Intent authIntent;
    private AccountManager mAccountManager;
    private Bundle mResultBundle = null;
    static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mAccountManager = AccountManager.get(this);

        //Checking for first launch of the app
        if (PreferencesManager.getInstance().isFirstLaunch())
        {
            //Send Event to MixPanel
            try
            {
                JSONObject props = new JSONObject();
                props.put("Install Date",PreferencesManager.getInstance().getDateString());
                MixPanelHelper helper = new MixPanelHelper(RegistrationActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.APP_INSTALLED);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            PreferencesManager.getInstance().setIsFirstLogin(false);
            PreferencesManager.getInstance().setIsFirstLaunch(false);
            Intent intent = new Intent(this, WelcomeTourActivity.class);
            startActivity(intent);
            this.finish();
        }

        //Not first launch but the user is not logged in
        else if (SyncUtils.isLoggedIn())
        {
            Intent intent=new Intent(this, HomePageActivity.class);
            startActivity(intent);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //startActivityForResult(intent, CREATE_ACCOUNT_REQUEST_CODE);
            this.finish();
        }

        initViews();
        changeBackground();

        getActionBar().hide();

        AppsFlyerLib.setAppsFlyerKey(Config.APPSFLYER_DEV_KEY);
        AppsFlyerLib.sendTracking(getApplicationContext());

        checkPlayServices();

        /*if(Build.VERSION.SDK_INT == 21)
        {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
        }*/

       /* // create our manager instance after the content view is set
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        // enable status bar tint
        tintManager.setStatusBarTintEnabled(true);
        // enable navigation bar tint
        tintManager.setNavigationBarTintEnabled(true);

        // set a custom tint color for all system bars
        tintManager.setTintColor(getResources().getColor(R.color.blue_top_bar));*/

    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported.",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this,
                REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
    }

    private void initViews()
    {
        //Connect with facebook button
        Button facebookButton = (Button) findViewById(R.id.activity_registration_login_facebook);
        //facebookButton.setBackgroundResource(R.drawable.fb_button_blank);
        //facebookButton.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        facebookButton.setOnClickListener(this);

        //login with email button
        Button emailButton = (Button) findViewById(R.id.activity_registration_login_email);
        emailButton.setOnClickListener(this);

        //Parent layout
        mParentLayout = (LinearLayout) findViewById(R.id.parent_layout);

        //Sign up button
        Button SignupButton = (Button) findViewById(R.id.activity_registration_sign_up);
        SignupButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.activity_registration_sign_up:
                /*//Mix Panel Event -> Email Registration
                //Send Event to MixPanel
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(RegistrationActivity.this,props);
                helper.sendEvent(MixPanelEventConstants.SIGN_UP_WITH_EMAIL);*/

                startCreateAccountActivity(null);
                break;
            case R.id.activity_registration_login_email:

                /*//Send Event to MixPanel
                JSONObject props1 = new JSONObject();
                MixPanelHelper helper1 = new MixPanelHelper(RegistrationActivity.this,props1);
                helper1.sendEvent(MixPanelEventConstants.SIGN_IN);*/

                Intent signInIntent = new Intent(this, SignInActivity.class);
                setArriveExtras(signInIntent);
                signInIntent.putExtra(Keys.IS_ARRIVE_FROM_REGISTRATION, true);
                startActivity(signInIntent);
                break;
            case R.id.activity_registration_login_facebook:

                //Send Event to MixPanel
                JSONObject props2 = new JSONObject();
                MixPanelHelper helper2 = new MixPanelHelper(RegistrationActivity.this,props2,null);
                helper2.sendEvent(MixPanelEventConstants.CONNECT_FACEBOOK);

                AppsFlyerLib.sendTrackingWithEvent(getApplicationContext(), Keys.APPSFLYER_REGISTRATION_EVENT_KEY, "");

                FacebookManager.getInstance().login(this, this);
                break;
            default:
                break;
        }
    }

   /* @Override
    protected void onStop() {
        super.onStop();
        if(mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }*/

    private void startCreateAccountActivity(Patient patient)
    {
        Intent intent = new Intent(this, CreateAccountActivity.class);

        setArriveExtras(intent);
        if (patient != null)
        {
            intent.putExtra(Keys.PATIENT, patient);
        }
        startActivityForResult(intent, HomePageActivity.CREATE_ACCOUNT_REQUEST_CODE);
    }

    private void setArriveExtras(Intent intent)
    {
        Intent arriveIntent = getIntent();
        if (arriveIntent != null)
        {
            Bundle extras = arriveIntent.getExtras();
            if (extras != null)
            {
                intent.putExtras(extras);
            }
        }
    }

    /*@Override
    protected void onResume()
    {
        super.onResume();
        if(mProgressDialog != null)
        {
            if (mProgressDialog.isShowing()) mProgressDialog.hide();
        }
    }*/

    public void onFbLoginSuccess(final String facebookAccessToken, String action)
    {
        if (!TextUtils.isEmpty(facebookAccessToken)) {
             final FacebookPost createFacebookPost = new FacebookPost();
            FacebookToken token = new FacebookToken();
            token.setToken(facebookAccessToken);
            createFacebookPost.withEntry(token);
            createFacebookPost.execute(this, new RequestListener<Patient>() {
                @Override
                public void onRequestComplete(Response<Patient> response) {
                    if (response.isSuccess()) {
                        /*AppsFlyerLib.sendTrackingWithEvent(getApplicationContext(), Keys
                                .APPSFLYER_REGISTRATION_EVENT_KEY, "");*/
                        PreferencesManager.getInstance().setShouldShowTermsPopup(true);
                        Patient patient = response.getData();
                        setupNewUser(patient);
                        setResult(RESULT_OK);
                        finish();
                        startNextActivity();
                    } else {

                        String errorMessage = !TextUtils.isEmpty(response.getUserResponseError()) ? response
                                .getUserResponseError() : getString(R.string.login_failure);
                        if (response.getStatus() == 422)
                        {
                            if (!TextUtils.isEmpty(facebookAccessToken)) {
                                FacebookToken facebookToken = new FacebookToken();
                                facebookToken.setToken(facebookAccessToken);

                                FacebookSessionsPost facebookPost = new FacebookSessionsPost();
                                facebookPost.withEntry(facebookToken).execute(RegistrationActivity.this, new
                                        RequestListener<Patient>() {
                                            @Override
                                            public void onRequestComplete(Response<Patient> response) {
                                                processResponse(response, SignInType.FACEBOOK);
                                            }
                                        });
                            }
                        } else {
                            AlertDialogHelper.getInstance().showMessage(RegistrationActivity.this, errorMessage);
                        }
                    }
                    getSupportLoaderManager().destroyLoader(createFacebookPost.hashCode());
                }
            });
        }
    }


    void processResponse(Response<Patient> response, @NonNull SignInType signInType) {
        final Patient data = response.getData();
        // submitBtn.setProgressState(false);
        if (response.isSuccess() && response.getData() != null) {

            String currentEmail = response.getData().getEmail();
            Patient previousPatient = App.getInstance().getPatient();

            if (previousPatient == null || (!TextUtils.isEmpty(previousPatient.getEmail())
                    && !previousPatient.getEmail().equals(currentEmail))) {
                /** Clear notification center from previous user */
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancelAll();
            }

            PreferencesManager.getInstance().setUserActive(data.isActive());
            data.setInstallationId(App.getInstance().getParseInstallationObjectId());
            data.setDeviceToken(App.getInstance().getDeviceId());
            data.setIsUpdated(true);
            App.getInstance().setPatient(data);
            //PreferencesManager.getInstance().setShouldShowTermsPopup(true);
            Patient patient = getRequestEntity(currentEmail);

            initAuthIntent(data, patient);


            DatabaseHelper.recreateDatabase(this);
            onLoggedInFinish();
        } else {
            String errorMessage = getString(R.string.login_failure);
            if (signInType.equals(SignInType.FACEBOOK)
                    && !TextUtils.isEmpty(response.getUserResponseError())) {
                errorMessage = response.getUserResponseError();
            }
            AlertDialogHelper.getInstance().showMessage(RegistrationActivity.this,
                    errorMessage);
            // submitBtn.setEnabled(true);
        }
    }

    private Patient getRequestEntity() {
        return getRequestEntity(null);
    }

    private Patient getRequestEntity(String email) {
        final String userEmail =email;
        final String userPass=null ;
        Patient patient = new Patient();
        patient.setEmail(userEmail);
        patient.setPassword(userPass);
        return patient;
    }

    private void onLoggedInFinish() {

        if(PreferencesManager.getInstance().getRegions() == null) {
            loadRegions();
        } else {
            onAuthFinish();
        }
    }

    private void onAuthFinish()
    {
        if(FacebookManager.mProgressDialog.isShowing()) FacebookManager.mProgressDialog.dismiss();

        String accountName = authIntent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String accountPassword = authIntent.getStringExtra(AccountManager.KEY_PASSWORD);

        final Account account = SyncUtils.createSyncAccount(this, accountName, accountPassword);

        if (getIntent().getBooleanExtra(AccountManager.LOGIN_ACCOUNTS_CHANGED_ACTION, false))
        {
            mAccountManager.setPassword(account, accountPassword);
        }
        else
        {
            String authToken = authIntent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword, null);
            mAccountManager.setAuthToken(account, BabylonAuthenticator.AUTH_TOKEN_TYPE,
                    App.getInstance().getPatient().getToken());
            SyncUtils.setWsse(new Wsse(App.getInstance().getPatient().getId(), authToken));
        }
        setAccountAuthenticatorResult(authIntent.getExtras());
        // submitBtn.setEnabled(true);

        Bundle arguments = authIntent.getExtras();
        boolean isLaunchFromNotification = isLaunchFromNotification(arguments);

        if (isLaunchFromNotification)
        {
            String parseData = arguments.getString(PushBroadcastReceiver.KEY_PARSE_DATA);
            Intent i = new Intent(this, HomePageActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, parseData);
            i.putExtra(Keys.IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, true);
            i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
            startActivity(i);
        }
        else if (arguments != null && arguments.getBoolean(Keys.SHOULD_START_HOME_ACTIVITY)) {
            arguments.putString(PushBroadcastReceiver.KEY_PARSE_DATA, null);
            //   UIUtils.hideSoftKeyboard(this, emailEdt);
            Intent baseIntent = new Intent(this, HomePageActivity.class);
            baseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TOP);
            baseIntent.putExtras(arguments);
            startActivity(baseIntent);
        }
        else {
            Intent i = new Intent(this, HomePageActivity.class);
            i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                    .FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(Keys.IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, true);
            startActivity(i);
        }

        finish();
    }

    private boolean isLaunchFromNotification(Bundle arguments) {
        boolean isLaunchFromNotification = false;
        if (arguments != null) {
            PushBroadcastReceiver.PushAction pushAction = (PushBroadcastReceiver.PushAction) arguments
                    .getSerializable(Keys.NOTIFICATION_DATA);
            isLaunchFromNotification = pushAction != null;
        }
        return isLaunchFromNotification;
    }


    public final void setAccountAuthenticatorResult(Bundle result) {
        mResultBundle = result;
    }

    private void loadRegions() {
        RegionsGet regionsGet = new RegionsGet();
        regionsGet.execute(this, new RequestListener<Region[]>() {
            @Override
            public void onRequestComplete(Response<Region[]> response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    if (response.getData() != null) {
                        onLoginFinish(response);
                    } else {
                        Toast.makeText(RegistrationActivity.this, R.string.some_error, Toast.LENGTH_LONG).show();
                    }
                }
            }

        });
    }

    private void onLoginFinish(Response<Region[]> response) {
        Region[] regions = response.getData();
        PreferencesManager.getInstance().setRegions(regions);
        onAuthFinish();
    }

    private void initAuthIntent(Patient data, Patient patient) {
        authIntent = getIntent();
        authIntent.putExtra(AccountManager.KEY_AUTHTOKEN, data.getToken());

        authIntent.putExtra(AccountManager.KEY_ACCOUNT_NAME, patient.getEmail());
        authIntent.putExtra(AccountManager.KEY_PASSWORD, patient.getPassword());
    }


    @Override
    public void onFbLoginFail(String result)
    {
        if(FacebookManager.mProgressDialog.isShowing()) FacebookManager.mProgressDialog.hide();
        Log.d("onFbLoginFail", result);
    }

    private void setupNewUser(Patient patient)
    {
        PreferencesManager.getInstance().setUserActive(patient.isActive());

        patient.setInstallationId(App.getInstance().getParseInstallationObjectId());
        patient.setDeviceToken(App.getInstance().getDeviceId());
        patient.setIsUpdated(true);
        App.getInstance().setPatient(patient);
        if(patient.getPromotion()!=null) {
            PreferencesManager.getInstance().setIsFirstPromo(true);
        }
        SyncUtils.createSyncAccount(this, patient.getEmail(), patient.getPassword());
        SyncUtils.setWsse(new Wsse(patient.getId(), patient.getToken()));

        String facebookUserId = patient.getFacebookUserId();
        AnalyticsWrapper.sendEvent(!TextUtils.isEmpty(facebookUserId) ? AnalyticsWrapper.Events.ON_FACEBOOK_REGISTERED : AnalyticsWrapper.Events.ON_BABYLON_REGISTERED);

        DatabaseHelper.recreateDatabase(this);

        /** Clear notification center from previous user */
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
    }

    private void startNextActivity()
    {
        Patient patient = App.getInstance().getPatient();
        Bundle arguments = getIntent().getExtras();
        if (patient.getRegionId() != null && patient.getPracticeId() == null)
        {
            Intent intent = new Intent(RegistrationActivity.this, SelectPracticeActivity.class);
            intent.putExtra(Keys.KEY_ALLOW_SKIP_MAP, true);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(RegistrationActivity.this, HomePageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (arguments == null)
            {
                arguments = new Bundle();
            }
            arguments.putBoolean(Keys.SHOULD_START_HOME_ACTIVITY, true);
            intent.putExtras(arguments);
            startActivity(intent);
        }
    }

    private void finishRegistration()
    {
        setResult(RESULT_OK);
        startNextActivity();
        finish();
    }

    /* public void finish() {
         if (mAccountAuthenticatorResponse != null) {
             // send the result bundle back if set, otherwise send an error.
             if (mResultBundle != null) {
                 mAccountAuthenticatorResponse.onResult(mResultBundle);
             } else {
                 mAccountAuthenticatorResponse.onError(AccountManager.ERROR_CODE_CANCELED,
                         "canceled");
             }
             mAccountAuthenticatorResponse = null;
         }
         super.finish();
     }
 */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        switch (requestCode)
        {
            case FacebookManager.FACEBOOK_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK)
                {
                    FacebookManager.getInstance().onActivityResult(requestCode, resultCode, intent);
                }
                break;
            case HomePageActivity.CREATE_ACCOUNT_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK)
                {
                    AppsFlyerLib.sendTrackingWithEvent(getApplicationContext(), Keys.APPSFLYER_REGISTRATION_EVENT_KEY, "");
                    Patient patient = (Patient) intent.getSerializableExtra(Keys.PATIENT);
                    setupNewUser(patient);
                    if (patient.getSubscription() != null && patient.getPromotion() != null)
                    {
                        Redemption redemption = new Redemption();
                        redemption.setPromotion(patient.getPromotion());
                        List<Subscription> subscriptions = new ArrayList<>();
                        subscriptions.add(patient.getSubscription());
                        redemption.setSubscriptions(subscriptions);
                        PromotionTransactionActivity.startPromotionTransactionActivity(this, redemption);
                    }
                    else
                    {
                        finishRegistration();
                    }

                    // Mix panel event for registration complete
                    JSONObject props = new JSONObject();
                    try
                    {
                        props.put("Date",PreferencesManager.getInstance().getDateString());
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                    Bundle fb_parameters = new Bundle();
                    fb_parameters.putString("Date",PreferencesManager.getInstance().getDateString());

                    MixPanelHelper helper = new MixPanelHelper(getApplicationContext(),props,fb_parameters);
                    helper.sendEvent(MixPanelEventConstants.REGISTRATION_COMPLETE);
                }
                break;
            case PromotionTransactionActivity.PROMOTION_REQUEST_CODE:
                finishRegistration();
                break;
            default:
                break;
        }
    }

    public void changeBackground()
    {
        //Create the timer object which will run the desired operation on a schedule or at a given time
        Timer timer = new Timer();

        //Create a task which the timer will execute.  This should be an implementation of the TimerTask interface.
        //I have created an inner class below which fits the bill.
        MyTimer mt = new MyTimer();

        //We schedule the timer task to run after 1000 ms and continue to run every 1000 ms.
        timer.schedule(mt, 15000, 10000);
    }

    //An inner class which is an implementation of the TImerTask interface to be used by the Timer.
    class MyTimer extends TimerTask
    {
        public void run()
        {
            //This runs in a background thread.
            //We cannot call the UI from this thread, so we must call the main UI thread and pass a runnable
            runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Random rand = new Random();
                    int nextInt = rand.nextInt(1 - 0 + 1) + 0;

                    if(nextInt == 0)
                    {
                        mParentLayout.setBackgroundResource(R.drawable.transition_1);
                    }
                    else
                    {
                        mParentLayout.setBackgroundResource(R.drawable.transition);
                    }
                    TransitionDrawable drawable = (TransitionDrawable) mParentLayout.getBackground();

                    if(!mTransitionComplete) {
                        drawable.startTransition(1000);
                        mTransitionComplete = true;
                    }
                    else {
                        drawable.reverseTransition(1500);
                        mTransitionComplete = false;
                    }
                }
            });
        }
    }
}
