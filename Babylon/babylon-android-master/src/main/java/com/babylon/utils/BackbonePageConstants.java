package com.babylon.utils;


import com.babylon.R;

/**
 * Created by Rahul.
 */
public enum BackbonePageConstants
{
    BOOK_APPOINTMENT (new String("Babylon.routers.appointments.new()"),true), //'type':'bloodtest'
    BLOOD_ANALYSIS_BOOK_APPOINTMENT (new String("Babylon.routers.appointments.new({'type': 'bloodtest'})"),true),
    SHOW_APPOINTMENT (new String("Babylon.routers.appointments.show(%d)"),true),
    PATHOLOGY_SHOW (new String("Babylon.routers.pathology.show(%d)"),true),
    CLINICAL_RECORDS (new String("file:///android_asset/dist/index.html#my-records"),false),
    LOADING_SCREEN (new String("file:///android_asset/dist/index.html#loading"),false),
    SHOW_PRESCRIPTIONS(new String("Babylon.routers.prescriptions.show(%d)"),true);

    private String mPageUrl;
    private boolean mIsJsCommand;

    private BackbonePageConstants(String pageUrl,boolean isJsCommand)
    {
        mPageUrl = pageUrl;
        mIsJsCommand = isJsCommand;
    }

    public String getPageUrl()
    {
        return mPageUrl;
    }

    public boolean getIsJsCommand()
    {
        return mIsJsCommand;
    }
}