package com.babylon.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.babylon.utils.L;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class BaseSQLiteOpenHelper extends SQLiteOpenHelper {
    public BaseSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * * WARNING! Reflection is used here!
     * Every Table must specify name and class of schema
     *
     * Every schema must have: - CUSTOM_SQL constant for specific logic while creating tables - Columns enumeration with
     * getName() and getType() methods getName() must return String getType() must return DBType
     *
     * @param sql
     * @param table
     * @return
     */
    protected boolean appendCreateTableSQL(StringBuilder sql, ITable table) {
        SchemeInterpreter schemeInterpreter = new SchemeInterpreter(table.getSchema());
        sql.append("CREATE TABLE IF NOT EXISTS ").append(table.getName()).append(" (");
        StringBuilder columnsSql = new StringBuilder(1024);

        HashMap<String, String> columns = schemeInterpreter.getColumnsMap();
        Iterator<Map.Entry<String, String>> it = columns.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = it.next();
            if (columnsSql.length() > 0) {
                columnsSql.append(",");
            }
            columnsSql.append(pairs.getKey());
            if (pairs.getKey().equalsIgnoreCase(BaseColumns._ID)) {
                columnsSql.append(" INTEGER PRIMARY KEY AUTOINCREMENT");
            } else {
                columnsSql.append(" ").append(pairs.getValue());
            }
        }

        if (columnsSql.length() > 0) {
            sql.append(columnsSql.toString());
        } else {
            return true;
        }

        String customSql = schemeInterpreter.getCustomSql();
        if (customSql != null && customSql.length() > 0) {
            sql.append(customSql);
        }
        sql.append(")");

        L.d(getClass().getSimpleName(), sql.toString());
        return false;
    }

    protected String getCreateTableString(Table table) {
        SchemeInterpreter schemeInterpreter = new SchemeInterpreter(table.getSchema());
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE IF NOT EXISTS ").append(table.getName()).append(" (");
        StringBuilder columnsSql = new StringBuilder(1024);

        HashMap<String, String> columns = schemeInterpreter.getColumnsMap();
        Iterator<Map.Entry<String, String>> it = columns.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = it.next();
            if (columnsSql.length() > 0) {
                columnsSql.append(",");
            }
            columnsSql.append(pairs.getKey());
            if (pairs.getKey().equalsIgnoreCase(BaseColumns._ID)) {
                columnsSql.append(" INTEGER PRIMARY KEY AUTOINCREMENT");
            } else {
                columnsSql.append(" ").append(pairs.getValue());
            }
        }

        if (columnsSql.length() > 0) {
            sql.append(columnsSql.toString());
            String customSql = schemeInterpreter.getCustomSql();
            if (customSql != null && customSql.length() > 0) {
                sql.append(customSql);
            }
            sql.append(")");
        }

        return sql.toString();
    }


    public static String preparePlaceHolders(int count) {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += i > 0 ? ",?" : "?";
        }
        return result;
    }
}
