package com.iversecomics.client.refresh;

import android.text.format.Time;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

public class RecentHashes {
    private Map<Integer, Long> hashes;
    private long expiresAfter = Time.SECOND;

    public RecentHashes() {
        hashes = Collections.synchronizedMap(new WeakHashMap<Integer, Long>());
    }

    public boolean hasSeen(int hashCode) {
        long now = System.currentTimeMillis();
        Long age = hashes.get(hashCode);
        hashes.put(hashCode, now); // we've seen it.
        if (age == null) {
            return false;
        }
        long expiresOn = now + expiresAfter;
        return (age.longValue() < expiresOn);
    }
}
