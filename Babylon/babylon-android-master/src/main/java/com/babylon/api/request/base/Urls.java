package com.babylon.api.request.base;

import com.babylon.Config;

public class Urls {


    public static final String SESSION = "/api/v1/sessions";
    public static final String PATIENTS = "/api/v1/patients";
    public static final String REGIONS = "/api/v1/regions";
    public static final String FACEBOOK_FIELDS = "/api/v1/registrations/facebook";
    public static final String PASSWORDS = "/api/v1/passwords";
    public static final String APPOINTMENTS = "/api/v1/appointments";
    public static final String PATCH = "/api/v1/appointments/%d";
    public static final String VIDEO_SESSIONS = "/api/v1/video_sessions";
    public static final String APP_LOG = "/api/v1/mobile_app_logs";
    public static final String PRACTICES = "/api/v1/regions/%s/practices";
    public static final String JOIN_PRACTICE = "/api/v1/regions/%s/practices/%s/join";
    public static final String PLANS_GET = "/api/v1/plans";

    public static final String QUESTION_POST = "/mobile/patient/question/add";
    public static final String QUESTION_LIST_GET = "/mobile/patient/question/list";
    public static final String QUESTION_GET = "/mobile/question/view/%s";
    public static final String PASSWORD_CONFIRM = "/api/v1/patients/";

    //Rate your appointment
    public static final String RATE_APPOINTMENT = "/api/v1/appointments/%s/reviews";

    public static final String SCANNED_FOOD_GET = "/mobile/patient/food/scan-barcode";

    public static final String UPLOAD_FILE_POST = "/mobile/question/file/upload/%s";
    public static final String DOWNLOAD_FILE_GET = "/mobile/question/file/download/%s";

    public static final String MONITOR_ME_PARAMS_GET = "/mobile/patient/monitorme/features";
    public static final String MONITOR_ME_PARAMS_LIGHT_GET = "/mobile/patient/monitorme/features-light";
    public static final String MONITOR_ME_REPORT_GET = "/mobile/patient/monitorme/reports";
    public static final String MONITOR_ME_PARAMS_POST = "/mobile/patient/monitorme/measures/new";

    public static final String PATIENT_DATE_POST = "/mobile/patient/monitorme/data";

    public static final String SEARCH_FOOD_BY_NAME_POST = "/mobile/patient/nutrients/search-nutratech";

    public static final String ACTIVITY_EDIT = "/mobile/patient/activity/edit/%s";
    public static final String ACTIVITY_LIST = "/mobile/patient/activity/";
    public static final String NUTRIENT_LIST = "/mobile/patient/nutrients/";
    public static final String NUTRIENT_RECENT = "/mobile/patient/nutrients/consumed/";

    public static final String FITBIT_SUBSCRIBE_POST = "/mobile/fitbit/subscribe";

    public static final String VALIDIC_SUBSCRIBE_POST = "/mobile/patient/validic/subscribe";
    public static final String VALIDIC_SUBSCRIBE_URL_GET = "/mobile/patient/validic/subscribe-url";

    public static final String STRESS_QUIZ_GET = "/mobile/patient/monitorme/measure/questions?measure[]=stress";
    public static final String MONITOR_CATEGORY_LIST_GET = "/mobile/patient/monitorme/measure/categories";
    public static final String CHART_GET = "/mobile/patient/monitorme/chart";

    //Payments
    public static final String ADD_CREDIT_CARD_POST = "/api/v1/credit_cards";
    public static final String CREDIT_CARD_LIST_GET = "/api/v1/credit_cards";
    public static final String CREDIT_CARD_LIST_DELETE = "/api/v1/credit_cards/%s";
    public static final String SUBSCRIBE_POST = "/api/v1/subscription";
    public static final String BUY_APPOINTMENT_POST = "/api/v1/appointments/%s/transaction";
    public static final String REDEMPTION_POST = "/api/v1/redemptions";
    public static final String DOCTOR_BIO = "/api/v1/consultants";
    public static final String SUBSCRIPTION_GET_CANCEL_TEXT = "/api/v1/subscription/cancel_text";
    public static final String SUBSCRIPTION_DELETE = "/api/v1/subscription";
    public static final String TRANSACTIONS_GET = "/api/v1/transactions";

    //Family
    public static final String ACCOUNTS = "/api/v1/accounts";
    public static final String ACCOUNTS_AUTHENTICATE = ACCOUNTS + "/%s/authenticate";


    //Kit
    public static final String CATEGORIES = "/api/v1/shop/categories";
    public static final String ORDERS = "/api/v1/shop/orders";
    public static final String ORDERS_PAY = "/api/v1/shop/orders/%s/transaction";
    public static final String POSTCODE_URL = "http://ws1.postcodesoftware.co.uk/lookup" +
            ".asmx/getAddress?account=%s&password=%s&postcode=%s";
    public static final String ADDRESS="/api/v1/patients/%s/addresses";
    public static final String GET_USER_ADDRESS = "/api/v1/patients/%s/addresses";

    //Notifications
    public static final String GET_NOTIFICATIONS = "/api/v2/notifications#index";
    public static final String PATCH_NOTIFICATIONS = "/api/v1/notifications/";
    public static final String CLEAR_NOTIFICATIONS = "/api/v1/notifications/clear/";



    //Insurance
    public static final String GET_INSURERS = "/api/v1/insurance_companies";
    public static final String TRIAGE_RESULT_SYMPTOMS_POST = "/mobile/triage/result/symptom/create";
    public static final String TRIAGE_BODY_PARTS_TREE_GET = "/mobile/triage/bodyparts/tree";
    public static final String TRIAGE_SYMPTOMS_TREE_GET = "/mobile/triage/symptoms/tree";
    public static final String TRIAGE_SYMPTOMS_GENERAL_GET = "/mobile/triage/symptoms/general";
    public static final String TRIAGE_EXPORT = "/mobile/triage/data/export";
    public static final String TRIAGE_UPDATE = "/mobile/triage/data/updates";

    public static String get(String s) {
        return Config.PHP_SERVER_URL.concat(s);
    }

    public static String getRuby(String s) {
        return Config.RUBY_SERVER_URL.concat(s);
    }
}
