package com.iversecomics.client.my.db;

import java.util.List;

import android.net.Uri;

public class MyComic {
    private long   dbId = -1;

    private String comicId;
    private String name;

    private String assetFilename;
    private Long   assetFilesize;
    private String description;
    private String synopsis;
    private String seriesId;
    private int    pricingTier;
    private String type;
    private String publisherId;
    private String publisherName;
    private String genres[];
    private Double publicRating;
    private Double myRating;

    private Long   timestampPurchased;
    private Long   timestampAsset;
    private Long   timestampReleased;

    private int    status;
    private Uri    uriCover;

    private String bookMark;
    private Integer onDemand;
     
    public String getBookMark() {
		return bookMark;
	}

	public void setBookMark(String bookMark) {
		this.bookMark = bookMark;
	}

	public String getAssetFilename() {
        return assetFilename;
    }

    public Long getAssetFilesize() {
        return assetFilesize;
    }

    public String getComicId() {
        return comicId;
    }

    public long getDbId() {
        return dbId;
    }

    public String getDescription() {
        return description;
    }

    public String[] getGenres() {
        return genres;
    }

    public String getName() {
        return name;
    }

    public int getPricingTier() {
        return pricingTier;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public Double getMyRating(){
    	return myRating == null ? 0 : myRating;
    }
    
    public Double getRating() {
        return publicRating;
    }

    public String getSeriesId() {
        return seriesId;
    }

    public int getStatus() {
        return status;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public Long getTimestampAsset() {
        return timestampAsset;
    }

    public Long getTimestampPurchased() {
        return timestampPurchased;
    }

    public Long getTimestampReleased() {
        return timestampReleased;
    }

    public String getType() {
        return type;
    }

    public Uri getUriCover() {
        return uriCover;
    }

    public void setAssetFilename(String assetFilename) {
        this.assetFilename = assetFilename;
    }

    public void setAssetFilesize(Long assetFilesize) {
        this.assetFilesize = assetFilesize;
    }

    public void setComicId(String comicId) {
        this.comicId = comicId;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGenres(List<String> categories) {
        String cats[] = new String[categories.size()];
        categories.toArray(cats);
        setGenres(cats);
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPricingTier(int pricingTier) {
        this.pricingTier = pricingTier;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public void setMyRating(Double rating){
    	this.myRating = rating;
    }
    public void setRating(Double rating) {
        this.publicRating = rating;
    }

    public void setSeriesId(String seriesId) {
        this.seriesId = seriesId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public void setTimestampAsset(Long timestampAsset) {
        this.timestampAsset = timestampAsset;
    }

    public void setTimestampPurchased(Long timestampPurchased) {
        this.timestampPurchased = timestampPurchased;
    }

    public void setTimestampReleased(Long timestampReleased) {
        this.timestampReleased = timestampReleased;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUriCover(Uri uriCover) {
        this.uriCover = uriCover;
    }

    public void setOnDemand(boolean onDemand) {
        this.onDemand = onDemand ? 1 : 0;
    }

    public boolean getOnDemand() {
        if (this.onDemand == null)
            return false;
        else
            return this.onDemand == 1;
    }
}
