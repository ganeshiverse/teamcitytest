package com.iversecomics.client.comic.viewer.curl;

import android.graphics.Bitmap;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.bitmap.IBitmapLoader;
import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.client.comic.ComicMode;
import com.iversecomics.client.comic.IComicSourceListener;
import com.iversecomics.client.comic.IMyComicSourceAdapter;
import com.iversecomics.client.comic.LogComicSourceListener;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

public class ComicBundleOnDemandAdapter implements IMyComicSourceAdapter, IBitmapLoader, AsyncReadQueue.AsyncQueueListener {

    public interface PageAsyncStatusListener {
        public void OnPageLoadingStart();

        public void OnPageLoadingFinish();
    }

    private static final Logger LOG = LoggerFactory.getLogger(ComicBundleOnDemandAdapter.class);
    private IComicSourceListener comicSourceListener;
    private WebBitmapLoader bitmapLoader;
    private BitmapManager bitmapManager;

    ArrayList<String> mPages;
    private PageAsyncStatusListener mAsyncStatusListener;
    private AsyncReadQueue mAsyncReadQueue;

    private boolean mFirstBitmapLoaded = false;

    public ComicBundleOnDemandAdapter(ArrayList<String> pages) {
        this.mPages = pages;
        final IverseApplication iverse = IverseApplication.getApplication();
        bitmapManager = iverse.createBitmapManager();
        bitmapLoader = new WebBitmapLoader(iverse.getHttpClient());

        mAsyncReadQueue = new AsyncReadQueue(this);
    }

    public void setAsyncStatusListener(PageAsyncStatusListener listener) {
        this.mAsyncStatusListener = listener;
    }

    @Override
    public int getImageCount() {
        return mPages != null ? mPages.size() : 0;
    }

    @Override
    public URI getImageURI(int pageIndex) {
        if (mPages == null) {
            LOG.error("getImageURI no pages found");
            return null;
        }

        try {
            return URI.create(mPages.get(pageIndex));
        } catch (IndexOutOfBoundsException e) {
            LOG.error(e, "ComicBundleOnDemandAdapter.getImageURI error");
        }
        return null;
    }

    @Override
    public String[] getUriSchemes() {
        return bitmapLoader.getUriSchemes();
    }

    @Override
    public Bitmap loadBitmap(URI bitmapUri) throws IOException {
        // Load image from cache
        return bitmapManager.getCacheDir().getBitmap(bitmapUri);
    }


    public void queuePageForDownload(CurlPage curlPage, int pageIndex) {

        // queue the next page as well (if necessary)
        // do this first since our queue is Last In First Out
        int nextPageIndex = pageIndex + 1;
        if (nextPageIndex < getImageCount()) {
            URI nextPageBitmapUri = getImageURI(nextPageIndex);
            if (!bitmapManager.getCacheDir().hasCachedBitmap(nextPageBitmapUri))
                mAsyncReadQueue.add(new QueableOnDemandPage(bitmapLoader, null, nextPageBitmapUri));
        }

        // queue the requested page
        if (pageIndex < getImageCount()) {
            URI bitmapUri = getImageURI(pageIndex);
            if (!bitmapManager.getCacheDir().hasCachedBitmap(bitmapUri))
                mAsyncReadQueue.add(new QueableOnDemandPage(bitmapLoader, curlPage, bitmapUri));
        }

        // only show loading indicator if we are loading more than one page at a time
        // otherwise it gets annoying and the page being loaded is not even needed yet (it's the next page)
        if (mAsyncStatusListener != null && mAsyncReadQueue.isBusy())
            mAsyncStatusListener.OnPageLoadingStart();
    }

    @Override
    public void setComicSourceListener(IComicSourceListener listener) {
        this.comicSourceListener = listener;
        if (comicSourceListener == null)
            comicSourceListener = new LogComicSourceListener();
    }

    @Override
    public ComicMode getComicMode() {
        return ComicMode.STREAMING;
    }

    @Override
    public void setComicMode(ComicMode mode) {
        // NA, comic mode is always ComicMode.STREAMING
    }

    @Override
    public Bitmap loadComicPage(int index) {
        return null;
    }

    @Override
    public void close() throws IOException {
        mAsyncReadQueue.stop();
    }

    @Override
    public void open() throws IOException {
        // NA
    }

    @Override
    public void onOpenFinished(boolean bSuccess) {
        if (bSuccess && comicSourceListener != null)
            comicSourceListener.onComicSourceUpdated(this);
    }


    @Override
    public void onAsyncReadComplete(AsyncQueueableObject queueableObject) {

        QueableOnDemandPage onDemandPage = (QueableOnDemandPage) queueableObject;

        // Add bitmap to cache so that it will be loaded from cache when mPageLoadedListener.OnPageLoaded
        // is called, otherwise another AsyncTask would be kicked off.
        bitmapManager.getCacheDir().addBitmap(onDemandPage.getPageUri(), onDemandPage.getBitmap());

        if (mAsyncStatusListener != null)
            mAsyncStatusListener.OnPageLoadingFinish();

        if (!mFirstBitmapLoaded) {
            mFirstBitmapLoaded = true;
            if (comicSourceListener != null)
                comicSourceListener.onComicSourceUpdated(this);
        }
    }

}
