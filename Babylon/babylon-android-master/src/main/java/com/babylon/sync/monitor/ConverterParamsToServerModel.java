package com.babylon.sync.monitor;

import android.text.TextUtils;

import com.babylon.model.MeasurePostModel;

import java.util.Date;

public class ConverterParamsToServerModel {
    private String paramIds;
    private MeasurePostModel measurePostModel;
    private Date firstDateToSync;

    public MeasurePostModel getMeasurePostModel() {
        return measurePostModel;
    }

    public String getParamIds() {
        return paramIds;
    }

    public Date getFirstDateToSync() {
        return firstDateToSync;
    }

    public void concatParamIds(String curParamId) {
        if (TextUtils.isEmpty(paramIds)) {
            paramIds = curParamId;
        } else {
            paramIds += "," + curParamId;
        }
    }

    public void setMeasurePostModel(MeasurePostModel measurePostModel) {
        this.measurePostModel = measurePostModel;
    }

    public void setFirstDateToSync(Date firstDateToSync) {
        this.firstDateToSync = firstDateToSync;
    }
}
