package com.axsystech.excelicare.loaders;

import android.content.Context;
import android.text.TextUtils;

import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import org.w3c.dom.Text;

import java.sql.SQLException;
import java.util.concurrent.Callable;

/**
 * Created by someswarreddy on 06/10/15.
 */
public class UpdateMsgPriorityInDbLoader extends DataAsyncTaskLibLoader<Boolean> {

    private String priorityUpdatedMsgId;
    private String updatedPriority;

    public UpdateMsgPriorityInDbLoader(Context context, String priorityUpdatedMsgId, String updatedPriority) {
        super(context);
        this.priorityUpdatedMsgId = priorityUpdatedMsgId;
        this.updatedPriority = updatedPriority;
    }

    @Override
    protected Boolean performLoad() throws Exception {
        if (TextUtils.isEmpty(priorityUpdatedMsgId))
            throw new SQLException("Unable to update message priority in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (!TextUtils.isEmpty(priorityUpdatedMsgId) && !TextUtils.isEmpty(updatedPriority)) {
                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        final Dao<MessageListItemDBModel, Long> messageListItemDBModelDao = helper.getDao(MessageListItemDBModel.class);

                        messageListItemDBModelDao.updateBuilder().updateColumnValue(MessageListItemDBModel.MSG_PRIORITY, updatedPriority)
                                .where().eq(MessageListItemDBModel.MESSAGE_ID, priorityUpdatedMsgId);

                        return true;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return true;
    }
}
