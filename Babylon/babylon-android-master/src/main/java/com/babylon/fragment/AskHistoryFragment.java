package com.babylon.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.QuestionActivity;
import com.babylon.adapter.MessageHistoryAdapter;
import com.babylon.database.schema.MessageSchema;
import com.babylon.view.TopBarInterface;

public class AskHistoryFragment extends BaseFragment
        implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    private ListView listView;
    private MessageHistoryAdapter messageHistoryAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ask_history, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(MessageSchema.Query.CURSOR_LOAD_MESSAGES,
                null, this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) view.findViewById(R.id.fragment_ask_history_list_view);
        listView.setOnItemClickListener(this);
        setTitle(getString(R.string.ask_history_title));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = messageHistoryAdapter.getCursor();
        if (cursor != null && !cursor.isClosed()
                && cursor.getCount() != 0) {
            String questionId = cursor.getString(MessageSchema.Query.ID);
            if (!TextUtils.isEmpty(questionId)) {
                Intent intent = new Intent(getActivity(), QuestionActivity.class);
                intent.putExtra(Keys.QUESTION_ID, questionId);
                startActivity(intent);
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id) {
            case MessageSchema.Query.CURSOR_LOAD_MESSAGES:
                return new CursorLoader(getActivity(),
                        MessageSchema.CONTENT_URI, null,
                        MessageSchema.Query.SELECT_SENDED_MESSAGES_ONLY,
                        null,
                        MessageSchema.Query.DATE_SORT);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        int id = cursorLoader.getId();
        switch (id) {
            case MessageSchema.Query.CURSOR_LOAD_MESSAGES:
                if (messageHistoryAdapter == null) {
                    messageHistoryAdapter = new MessageHistoryAdapter(getActivity(), cursor);
                    listView.setAdapter(messageHistoryAdapter);
                } else {
                    messageHistoryAdapter.changeCursor(cursor);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        /* data is not available anymore*/
        messageHistoryAdapter.changeCursor(null);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.GREEN_THEME;
    }
}