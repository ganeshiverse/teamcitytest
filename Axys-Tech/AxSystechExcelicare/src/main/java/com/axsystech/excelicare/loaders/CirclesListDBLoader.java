package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/9/2015.
 */
public class CirclesListDBLoader extends DataAsyncTaskLibLoader<ArrayList<CirclesListDbModel>> {

    private ArrayList<CirclesListDbModel> circlesListDbModels;
    private String TAG = CirclesListDBLoader.class.getSimpleName();

    public CirclesListDBLoader(final Context context, final ArrayList<CirclesListDbModel> mediaItems) {
        super(context, true);
        this.circlesListDbModels = mediaItems;
    }

    @Override
    protected ArrayList<CirclesListDbModel> performLoad() throws Exception {
        if (circlesListDbModels == null)
            throw new SQLException("Unable to cache media item details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (circlesListDbModels != null && circlesListDbModels.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<CirclesListDbModel>>() {
                    @Override
                    public ArrayList<CirclesListDbModel> call() throws Exception {
                        for (final CirclesListDbModel dbModel : circlesListDbModels) {
                            final Dao<CirclesListDbModel, Long> mediaItemDao = helper.getDao(CirclesListDbModel.class);

                            mediaItemDao.createOrUpdate(dbModel);
                        }

                        return circlesListDbModels;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return circlesListDbModels;
    }

}
