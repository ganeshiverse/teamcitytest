package com.iversecomics.client.net;

import android.content.Context;

import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.zip.GZIPInputStream;

public class HTTPClient {
    private static final Logger LOG = LoggerFactory.getLogger(HTTPClient.class);
    private DefaultHttpClient httpclient;
    private String userAgent;

    public HTTPClient(Context context) {
        HttpParams parameters = new BasicHttpParams();
        HttpProtocolParams.setVersion(parameters, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(parameters, HTTP.UTF_8);
        // some webservers have problems if this is set to true
        HttpProtocolParams.setUseExpectContinue(parameters, false);
        ConnManagerParams.setMaxTotalConnections(parameters, 9);
        HttpConnectionParams.setConnectionTimeout(parameters, 60000); //5000
        HttpConnectionParams.setSoTimeout(parameters, 120000); //15000

        SchemeRegistry schReg = new SchemeRegistry();
        schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schReg.register(new Scheme("https", TrustAllSSLSocketFactory.getDefault(), 443));

        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(parameters, schReg);

        httpclient = new DefaultHttpClient(conMgr, parameters);
        if (context == null) {
            userAgent = "Android Iverse Client [Unknown packageName]";
        } else {
            userAgent = AndroidInfo.getUserAgent(context);
        }
    }

    public void close(HttpEntity entity) {
        if (entity == null) {
            return;
        }

        try {
            entity.consumeContent();
        } catch (IOException e) {
            LOG.error(e, "Entity failure");
        }
    }

    public void enableGzip(HttpRequestBase httpbase) {
        httpbase.setHeader("Accept-Encoding", "gzip");
    }

    public HttpResponse execute(HttpRequestBase request) throws ClientProtocolException, IOException {
        return httpclient.execute(request);
    }

    public InputStream getUngzippedContent(HttpEntity entity) throws IOException {
        InputStream responseStream = entity.getContent();
        if (responseStream == null) {
            return responseStream;
        }
        if (isGzipEncoded(entity)) {
            return new GZIPInputStream(responseStream);
        }
        return responseStream;
    }

    public boolean isGzipEncoded(HttpEntity entity) {
        Header header = entity.getContentEncoding();
        if (header == null) {
            return false;
        }
        String encoding = header.getValue();
        if (encoding == null) {
            return false;
        }
        return encoding.contains("gzip");
    }

    public void logContentLength(URI uri, HttpResponse response) {
        LOG.debug("%s | Downloaded %,d bytes", uri.toASCIIString(), response.getEntity().getContentLength());
    }

    public void logResponseHeaders(HttpResponse response) {
        for (Header header : response.getAllHeaders()) {
            LOG.debug("response header| %s: %s", header.getName(), header.getValue());
        }
    }

    public void setUserAgent(HttpRequestBase httpbase) {
        httpbase.setHeader("User-Agent", userAgent);
    }
}
