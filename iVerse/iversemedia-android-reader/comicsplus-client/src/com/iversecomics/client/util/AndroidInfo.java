package com.iversecomics.client.util;

import java.util.Locale;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;

import com.iversecomics.client.R;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public final class AndroidInfo {
    private static final Logger LOG = LoggerFactory.getLogger(AndroidInfo.class);

    public static CharSequence getApplicationLabel(Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo(context.getPackageName(), 0);
            return pm.getApplicationLabel(ai);
        } catch (NameNotFoundException e) {
            return "AnonDroid";
        }
    }

    public static String getApplicationVersion(Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            return pm.getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            return "";
        }
    }

    public static String getArtifactForDevice(final Context context)
    {
    	return context.getResources()
		.getBoolean(R.bool.artifact_hd) == true ? "HD" : "SD";
    }
    
    public static String getDeviceModel() {
        StringBuilder ret = new StringBuilder();

        if ("sdk".equals(Build.MODEL) && "sdk".equals(Build.PRODUCT)) {
            return "SDK Emulator";
        }

        ret.append(Build.MODEL).append(" [");
        ret.append(Build.MANUFACTURER).append(" ");
        ret.append(Build.PRODUCT).append("]");

        return ret.toString();
    }

    public static String getLocale(Context context) {
        return Locale.getDefault().toString();
    }

    public static String getOSVersion() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.CUR_DEVELOPMENT) {
            return "DEV";
        }
        return Build.VERSION.RELEASE;
    }

    public static String getUniqueDeviceID(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String id = android.provider.Settings.Secure.getString(contentResolver,
                android.provider.Settings.Secure.ANDROID_ID);
        if (id == null) {
            LOG.warn("Running on emulator.");
            id = "deadbeef00111100"; // running on emulator.
        }
        return id;
    }

    /**
     * Build an HTTP User-Agent suitable enough to identify this application + version + handset
     */
    public static String getUserAgent(Context context) {
        StringBuilder ua = new StringBuilder();
        ua.append(AndroidInfo.getApplicationLabel(context)).append("/");
        ua.append(AndroidInfo.getApplicationVersion(context));
        ua.append(" (Android ").append(AndroidInfo.getOSVersion());
        ua.append("/").append(AndroidInfo.getDeviceModel()).append(")");
        return ua.toString();
    }

    private AndroidInfo() {
        /* prevent instantiation */
    }
}
