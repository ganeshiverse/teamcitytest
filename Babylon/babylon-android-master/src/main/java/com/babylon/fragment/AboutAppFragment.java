package com.babylon.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.utils.ActionBarType;
import com.babylon.view.TopBarInterface;

public class AboutAppFragment extends BaseFragment {

    public static final String TAG = AboutAppFragment.class.getSimpleName();
    private TextView mTvVersion;

    public static AboutAppFragment newInstance() {
        AboutAppFragment fragment = new AboutAppFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTvVersion =(TextView)view.findViewById(R.id.edtApp_version);

        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        mTvVersion.setText(version);

        getActivity().getActionBar().show();
        setupActionBar(ActionBarType.AboutApp);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }


}






