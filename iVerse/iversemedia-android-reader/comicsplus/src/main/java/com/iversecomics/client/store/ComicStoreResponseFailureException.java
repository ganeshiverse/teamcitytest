package com.iversecomics.client.store;

public class ComicStoreResponseFailureException extends ComicStoreException {
    private static final long serialVersionUID = -2419992477116696086L;

    public ComicStoreResponseFailureException(String detailMessage) {
        super(detailMessage);
    }

    public ComicStoreResponseFailureException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ComicStoreResponseFailureException(Throwable throwable) {
        super(throwable);
    }
}
