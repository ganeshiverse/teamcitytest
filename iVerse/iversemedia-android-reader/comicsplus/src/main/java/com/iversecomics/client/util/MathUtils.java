package com.iversecomics.client.util;

public class MathUtils {
    static public int decToMin(int val, int min) {
        return --val < min ? min : val;
    }

    static public int incToMax(int val, int max) {
        return ++val > max ? max : val;
    }

    static public int incWrap(int val, int max) {
        return ++val > max ? 0 : val;
    }

    public static int incWrapEx(int val, int max) {
        return ++val >= max ? max - 1 : 0;
    }
}
 