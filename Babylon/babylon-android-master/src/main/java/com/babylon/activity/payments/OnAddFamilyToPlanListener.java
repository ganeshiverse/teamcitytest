package com.babylon.activity.payments;

public interface OnAddFamilyToPlanListener {
    void onAddFamilyToPlanClick();
}
