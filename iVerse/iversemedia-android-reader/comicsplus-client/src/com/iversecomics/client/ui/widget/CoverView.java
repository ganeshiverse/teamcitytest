package com.iversecomics.client.ui.widget;

import android.content.Context;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.iversecomics.client.R;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class CoverView extends ComicStoreImageView {
    private static final Logger LOG = LoggerFactory.getLogger(CoverView.class);
    private CoverSize coverSize;
    private Context context;

    public CoverView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CoverView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public CoverView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init() {
        setImageResource(R.drawable.cover_placeholder);

        NinePatchDrawable npd = (NinePatchDrawable) context.getResources().getDrawable(R.drawable.comicborder);
        this.setBackgroundDrawable(npd);
        int padding = ComicStoreImageView.dpToPx(PADDING_DP, getContext());
        this.setPadding(padding, padding, padding, padding);
    }

    //added this function in case the cover view is nested inside of ViewPager
    //which is the case sub-CoverView doesn't have valid layout param.
    public void setCoverWithHeight(Comic comic, int height, BitmapManager bitmapManager) {
        LOG.debug("setCover(%s, bitmapManager)", comic.getComicId());

        if (coverSize == null) {
            coverSize = CoverSize.bestFit(height);
        }

        Uri serverUri = coverSize.getServerUri(comic.getImageFileName());
        setImageViaUri(serverUri, bitmapManager);
    }

    public void setCover(Comic comic, BitmapManager bitmapManager) {
        LOG.debug("setCover(%s, bitmapManager)", comic.getComicId());

        if (coverSize == null) {
            ViewGroup.LayoutParams lp = getLayoutParams();
            if (lp != null) {
                coverSize = CoverSize.bestFit(lp.height);
            } else {
                coverSize = CoverSize.bestFit(this);
            }
        }

        Uri serverUri = coverSize.getServerUri(comic.getImageFileName());
        setImageViaUri(serverUri, bitmapManager);
    }
}
