package com.babylon.activity.family;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.family.FamilyAccountListGet;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Account;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.view.TopBarInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FamilyAccountsListFragment extends BaseFragment implements AbsListView.OnItemClickListener {

    public static final String TAG = FamilyAccountsListFragment.class.getSimpleName();

    public interface FamilyListFragmentListener {
        public void onFamilyAccountListItemClicked(Account account);
        public void onAddFamilyAccountPressed();
    }

    private ListView listView;
    private FamilyAccountsListAdapter adapter;
    private List<Account> accounts = new ArrayList<Account>();
    private FamilyListFragmentListener listener;
    private TextView mTvEmptyAcct;

    private View.OnClickListener onAddClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            listener.onAddFamilyAccountPressed();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getString(R.string.family_list_fragment_title));
        setRightButton(onAddClickedListener, R.string.family_list_add);

        listView = (ListView)view.findViewById(android.R.id.list);
        mTvEmptyAcct=(TextView)view.findViewById(R.id.tvEmptyFamilyAccount);

        adapter = new FamilyAccountsListAdapter(getActivity(), accounts);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);

        new FamilyAccountListGet().execute(getActivity(), familyListRequestListener);
        setProgressBarVisible(true);
    }

    private final RequestListener<Account[]> familyListRequestListener = new
            RequestListenerErrorProcessed<Account[]>() {
                @Override
                public void onRequestComplete(Response<Account[]> response, String errorMessage) {
                    setProgressBarVisible(false);
                    if (!getActivity().isFinishing())
                    {
                        if(response.isSuccess())
                        {
                            final Account[] accounts = response.getData();
                            setAccounts(accounts);
                        }
                        else
                        {
                            final String userResponseError = response.getUserResponseError();
                            if (!TextUtils.isEmpty(userResponseError)) {
                                AlertDialogHelper.getInstance().showMessage(getActivity(), userResponseError);
                            }
                        }
                    }
                }
            };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (FamilyListFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (listener != null) {
            listener.onFamilyAccountListItemClicked(adapter.getItem(position));
        }
    }

    private void setAccounts(Account[] accounts) {
        this.accounts.clear();
        this.accounts.addAll(Arrays.asList(accounts));
        if(accounts.length>0){
            listView.setVisibility(View.VISIBLE);
            mTvEmptyAcct.setVisibility(View.GONE);
        }
        else{
            listView.setVisibility(View.GONE);
            mTvEmptyAcct.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

}