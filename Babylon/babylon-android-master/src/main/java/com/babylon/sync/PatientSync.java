package com.babylon.sync;

import android.content.ContentResolver;
import android.os.Bundle;
import android.util.Log;

import com.babylon.App;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.PatientPatch;
import com.babylon.api.request.base.Response;
import com.babylon.model.Patient;

public class PatientSync extends AbsSyncStrategy {

    private static final String TAG = PatientSync.class.getSimpleName();

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        Patient patient = App.getInstance().getPatient();

        if (patient.isUpdated()) {
            new PatientPatch().withEntry(patient).execute();
            Log.d(TAG, "-push- " + patient.toString());
        }
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        Response<Patient> response = new PatientGet().execute();
        Patient newPatient = response.getData();

        if (newPatient != null) {
            Log.d(TAG, "-pull- " + newPatient.toString());
            newPatient.setIsUpdated(false);
            App.getInstance().setPatient(newPatient);
        }
    }
}
