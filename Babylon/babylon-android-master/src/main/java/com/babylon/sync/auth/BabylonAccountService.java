package com.babylon.sync.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.babylon.App;
import com.babylon.utils.L;

public class BabylonAccountService extends Service {
    private static final String TAG = BabylonAccountService.class.getSimpleName();
    private BabylonAuthenticator mAuthenticator;

    public static Account getAccount() {
        Account account = null;
        AccountManager accountManager = AccountManager.get(App.getInstance());
        final Account[] accountsByType = accountManager.getAccountsByType(BabylonAuthenticator.ACCOUNT_TYPE);
        if (accountsByType.length > 0) {
            account = accountsByType[0];
        }

        return account;
    }

    @Override
    public void onCreate() {
        L.i(TAG, "Service created");
        mAuthenticator = new BabylonAuthenticator(this);
    }

    @Override
    public void onDestroy() {
        L.i(TAG, "Service destroyed");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }

}

