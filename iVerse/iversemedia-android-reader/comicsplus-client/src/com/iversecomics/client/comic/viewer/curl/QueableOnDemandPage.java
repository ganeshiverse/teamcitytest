package com.iversecomics.client.comic.viewer.curl;

import android.graphics.Bitmap;

import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URI;

public class QueableOnDemandPage implements AsyncQueueableObject {

    private WebBitmapLoader mLoader;
    private URI mPageUri;
    private Bitmap mBitmap;
    private WeakReference<CurlPage> mWeakCurlPage;
    private boolean mWasGarbageCollected = false;
    private boolean mWasOutOfMemory = false;

    private static final Logger LOG = LoggerFactory.getLogger(QueableOnDemandPage.class);

    public QueableOnDemandPage(WebBitmapLoader loader, CurlPage curlPage, URI pageUri) {
        this.mLoader = loader;
        this.mPageUri = pageUri;
        this.mWeakCurlPage = new WeakReference<CurlPage>(curlPage);
    }

    @Override
    public void performOperation() {

        CurlPage curlPage = mWeakCurlPage.get();
        if (curlPage == null)
            mWasGarbageCollected = true;

        try {
            mBitmap = mLoader.loadBitmap(mPageUri);
        } catch (OutOfMemoryError e) {
            LOG.error("out of memory loading bitmap in QueableOnDemandPage %s", e.getLocalizedMessage());
            mWasOutOfMemory = true;
        } catch (IOException e) {
            LOG.error("error loading bitmap in QueableOnDemandPage %s", e.getLocalizedMessage());
        }
    }

    @Override
    public void handleOperationResult() {

        if (mWasOutOfMemory || mWasGarbageCollected)
            return;

        CurlPage curlPage = mWeakCurlPage.get();
        if (mBitmap != null && curlPage != null)
            curlPage.setTexture(mBitmap, CurlPage.SIDE_BOTH);
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public URI getPageUri() {
        return mPageUri;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof QueableOnDemandPage) {
            QueableOnDemandPage qodp = (QueableOnDemandPage)obj;
            return  mPageUri != null &&
                    qodp.getPageUri() != null &&
                    mPageUri.equals(qodp.getPageUri());
        } else {
            return false;
        }
    }
}
