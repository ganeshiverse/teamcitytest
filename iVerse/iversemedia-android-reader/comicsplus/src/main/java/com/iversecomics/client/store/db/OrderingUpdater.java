package com.iversecomics.client.store.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.iversecomics.client.store.model.Comic;

public class OrderingUpdater implements ComicDBSubUpdater {
    private String listId;
    private int initialOffset;

    public OrderingUpdater(String listId) {
        this.listId = listId;
        this.initialOffset = 0;
    }

    public int getInitialOffset() {
        return initialOffset;
    }

    @Override
    public void onUpdateEnd(BatchInserts batcher) {
        /* do nothing */
    }

    public void setInitialOffset(int initialOffset) {
        this.initialOffset = initialOffset;
    }

    @Override
    public void update(SQLiteDatabase db, BatchInserts batcher, Comic comic) {
        ContentValues values = new ContentValues();
        values.put(OrderingTable.COMICID, comic.getComicId());
        values.put(OrderingTable.LISTID, listId);
        values.put(OrderingTable.INDEX, initialOffset++);
        values.put(OrderingTable.TIMESTAMP, System.currentTimeMillis());

        batcher.insert(OrderingTable.TABLE, values);
    }
}
