package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.base.Urls;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Kit.Address;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.CommonHelper;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.PreferencesManager;
import com.babylon.view.TopBarInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ConfirmAddressFragment extends BaseFragment {

    public static final String TAG = ConfirmAddressFragment.class.getSimpleName();
    public static final String ADDRESS_BUNDLE_KEY="address";
    private Button btnConfirm;
    private DataProtocol dataProtocol;
    private Address address;
    private TextView tvAddress1,tvTown,tvCounty,tvPostcode;
    private ConfirmAddressFragmentListener listener;
    public interface ConfirmAddressFragmentListener {
        public void onConfirmAdressClicked(Address address);

    }

    public static ConfirmAddressFragment newInstance() {
        ConfirmAddressFragment fragment = new ConfirmAddressFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_address_confirmation, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.fragment_confirm_address_title));

        btnConfirm=(Button)view.findViewById(R.id.btnConfirm);
        tvAddress1=(TextView)view.findViewById(R.id.tvAddress1);
        tvTown=(TextView)view.findViewById(R.id.tvTown);
        tvCounty=(TextView)view.findViewById(R.id.tvCounty);
        tvPostcode=(TextView)view.findViewById(R.id.tvPostcode);
        address=dataProtocol.getAddressSelected();

        fillAddressDetails(address);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updatePatientsAddress(address);
            }
        });

    }

    private void fillAddressDetails(Address address) {

                 tvAddress1.setText(address.getFirstLine());
                 tvTown.setText(address.getSecondLine());
                 tvCounty.setText(address.getThirdLine());
                 tvPostcode.setText(address.getPostCode());

    }


    private void updatePatientsAddress(final Address address) {

        JSONObject json_params=getParams(address);

        String mUri=  String.format(CommonHelper.getUrl(Urls.ADDRESS), SyncUtils.getWsse().getId());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, mUri, json_params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                              if (listener != null) {
                                  Gson gson = new GsonBuilder()
                                          .excludeFieldsWithoutExposeAnnotation()
                                          .create();
                                  String jsonString = response.toString();

                                Address address_response = gson.fromJson(jsonString, Address.class);
                                  PreferencesManager.getInstance().setPatientAddress(address_response.getFirstLine()+"," +
                                          ""+address_response.getSecondLine()+"\n"+address_response.getCity()+", " +
                                          ""+address_response.getPostCode());
                                  listener.onConfirmAdressClicked(address);
                                 PreferencesManager.getInstance().setPatientAddressId(address_response.getId());
                              }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }



    }


    private JSONObject getParams(Address address) {
        String RegObject = null;

        Map<String, Object> objMap = new HashMap<String, Object>();
        objMap.put("address", GsonWrapper.getGson().toJsonTree(address).toString());


        StringBuffer buf = new StringBuffer().append('{');
        Set<String> keys = objMap.keySet();
        Iterator<String> iterator = keys.iterator();
        Object key;
        Object value;

        boolean first = true;
        while (iterator.hasNext())
        {
            if (first)
                first = false;
            else
                buf.append(',');
            key = iterator.next();
            value = objMap.get(key);
            buf.append( key + ":" + value );
        }
        buf.append('}');


        RegObject=buf.toString();


        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(RegObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (ConfirmAddressFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }

        try {
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }
    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    public interface DataProtocol {

        public Address getAddressSelected();
    }

}






