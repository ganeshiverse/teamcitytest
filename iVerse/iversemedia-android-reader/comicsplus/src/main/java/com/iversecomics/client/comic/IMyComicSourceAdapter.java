package com.iversecomics.client.comic;

import java.io.IOException;

public interface IMyComicSourceAdapter extends IComicSourceAdapter {
    public void close() throws IOException;

    public void open() throws IOException;

    public void onOpenFinished(boolean bSuccess); //in order to split notificaion handler from heavy open thread
}
