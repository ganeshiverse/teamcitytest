package com.iversecomics.client.store.tasks;

import android.content.Context;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ServerConfig;

public class ServerConfigTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testGetConfig() throws Exception {
        startStubActivity();
        Context context = getActivity();
        freshDB(context);
        Freshness freshness = new Freshness(context);

        ServerConfig.getDefault().clear();

        assertFalse("ServerConfig.isFresh", ServerConfig.getDefault().isFresh(freshness));

        ComicStore comicStore = getComicStore();
        ServerConfigTask task = new ServerConfigTask(context, comicStore);
        runTaskWaitForCompletion(task);

        ServerConfig config = ServerConfig.getDefault();

        assertNotNull("ServerConfig should not be null", config);
        assertEquals("ServerConfig.serverApiVersion", 2, config.getServerApiVersion());
        assertTrue("ServerConfig.isFresh", config.isFresh(freshness));
    }
}
