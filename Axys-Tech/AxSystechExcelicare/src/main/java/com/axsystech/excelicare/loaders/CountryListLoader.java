package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CountryListResponse;
import com.axsystech.excelicare.data.responses.StateListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 15/09/15.
 */
public class CountryListLoader extends DataAsyncTaskLibLoader<CountryListResponse> {

    private String systemLookups;
    private String userLookups;
    private String token;


    public CountryListLoader(Context context, String token, String systemLookups, String userLookups) {
        super(context);
        this.systemLookups = systemLookups;
        this.userLookups = userLookups;
        this.token = token;
    }

    @Override
    protected CountryListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCountryList(token, systemLookups, userLookups);
    }
}
