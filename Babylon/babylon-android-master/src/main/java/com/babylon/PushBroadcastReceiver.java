package com.babylon;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.model.push.NotifAskConsultant;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.*;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

public class PushBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = PushBroadcastReceiver.class.getSimpleName();

    public static final String KEY_PARSE_DATA = "com.parse.Data";

    public static enum PushAction {
        ADD_COMMENT,
        SUGGEST_CONSULTATION,
        SUGGEST_CONSULTATION_AFTER_ARCHIVING,
        REJECT_QUESTION,
        BLOCK_PATIENT_TEMPORARILY,
        BLOCK_PATIENT,
        MARK_QUESTION_AS_PENDING,
        UNMARK_QUESTION_AS_PENDING,
        APPROVE_QUESTION,
        APPOINTMENT_REMINDER,
        APPOINTMENT_START,
        BLOOD_ANALYSIS_HEALTHY("BLOOD_ANALYSIS.HEALTHY"),
        BLOOD_ANALYSIS_APPOINTMENT("BLOOD_ANALYSIS.APPOINTMENT"),
        SUBSCRIPTION,
        NEW_PRESCRIPTION,
        REFERRAL,
        PATHOLOGY_TEST,
        ACCEPT_UNVERIFIED_PATIENT,
        REDEMPTION_USED,
        REDEMPTION_EXPIRY,
        REJECT_UNVERIFIED_PATIENT,
        UNKNOWN;


        private String value;

        private PushAction() {
            // default constructor
        }

        private PushAction(String value) {
            this.value = value;
        }

        public static PushAction getValue(String stringValue) {
            for (PushAction pushAction : values()) {
                if (pushAction.value != null && pushAction.value.equals(stringValue)) {
                    return pushAction;
                }
            }
            PushAction pushAction = UNKNOWN;
            try {
                pushAction = PushAction.valueOf(stringValue);
            } catch (IllegalArgumentException e) {
                L.e(TAG, e.getMessage(), e);
            }

            return pushAction;
        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle extras = intent.getExtras();
        if (extras != null && !TextUtils.isEmpty(extras.getString(KEY_PARSE_DATA)))
        {
            final String data = extras.getString(KEY_PARSE_DATA);

            L.d(TAG, data);

            final String action = getAction(data);
            final PushAction pushAction = PushAction.getValue(action.replace("com.babylon.", ""));

            if (pushAction != null)
            {
                //Push notification received Mixpanel Event
                int questionCount = PreferencesManager.getInstance().getNotificationsCount();
                String lastQuestionAsked = PreferencesManager.getInstance().getLastNotificationDate();

                //Send Event to MixPanel
                try
                {
                    JSONObject props = new JSONObject();
                    //props.put("Number of Notifications Received", questionCount);
                    props.put("Last Notification Received", lastQuestionAsked);


                    Bundle fb_parameters = new Bundle();
                    fb_parameters.putInt("Number of Notifications Received", questionCount);
                    fb_parameters.putString("Last Notification Received", lastQuestionAsked);


                    MixPanelHelper helper = new MixPanelHelper(context,props,fb_parameters);
                    helper.sendEvent(MixPanelEventConstants.NOTIFICATION_RECEIVED);
                    helper.incrementProperty("Number of Notifications Received");

                    PreferencesManager.getInstance().setLastNotificationDate();
                    PreferencesManager.getInstance().incrementNotificationsCount();
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

                switch (pushAction) {
                    case APPOINTMENT_START:
                        App.getInstance().getMediaPlayer().play();
                        break;
                    case ADD_COMMENT:
                    case SUGGEST_CONSULTATION:
                    case REJECT_QUESTION:
                        final NotifAskConsultant parseData = new Gson().fromJson(data, NotifAskConsultant.class);
                        String questionId = parseData.getQuestionId();
                        if (TextUtils.isEmpty(questionId)) {
                            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MESSAGES, SyncAdapter.SYNC_PULL);
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putString(Keys.QUESTION_ID, questionId);
                            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_SINGLE_MESSAGE, bundle, SyncAdapter.SYNC_PULL);
                        }
                        break;
                    case REFERRAL:
                        AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.REFERRAL_RECEIVED);
                        break;
                    default:
                        L.i(TAG, "Unknown push");
                        break;
                }
            }

        }
    }

    public static String getAction(String data) {
        String action = "";

        try {
            JSONObject obj = new JSONObject(data);
            action = obj.getString("action");

        } catch (JSONException e) {
            L.e(TAG, e.getMessage(), e);
        }

        return action;
    }
}
