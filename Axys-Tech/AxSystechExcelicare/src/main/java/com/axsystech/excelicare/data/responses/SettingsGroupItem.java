package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by alanka on 9/28/2015.
 */
public class SettingsGroupItem {

    private String GroupName;

    @SerializedName("SettingsChildItem")
    private ArrayList<SettingsChildItem> settingsChildItem;

    public ArrayList<SettingsChildItem> getSettingsChildItem() {
        return settingsChildItem;
    }

    public void setSettingsChildItem(ArrayList<SettingsChildItem> settingsChildItem) {
        this.settingsChildItem = settingsChildItem;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }
}
