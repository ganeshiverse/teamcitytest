package com.iversecomics.client.store;

public class ComicStoreUnavailableException extends ComicStoreException {
    private static final long serialVersionUID = -3788342971760374581L;

    public ComicStoreUnavailableException(String detailMessage) {
        super(detailMessage);
    }

    public ComicStoreUnavailableException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ComicStoreUnavailableException(Throwable throwable) {
        super(throwable);
    }
}
