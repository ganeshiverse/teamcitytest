package com.iversecomics.client.util;

public enum Dir {
    NONE, NORTH_WEST, NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST;

    private static final Dir ZONES[]       = new Dir[] { Dir.EAST, Dir.NORTH_EAST, Dir.NORTH, Dir.NORTH_WEST, Dir.WEST,
            Dir.SOUTH_WEST, Dir.SOUTH, Dir.SOUTH_EAST };

    private static final int FULL_CIRCLE   = 360;
    private static final int HALF_CIRCLE   = FULL_CIRCLE / 2;
    private static final int QUATER_CIRCLE = HALF_CIRCLE / 2;
    private static final int EIGHT_CIRCLE  = QUATER_CIRCLE / 2;

    public static Dir asDir(double deltaX, double deltaY) {
        double degrees = calcDegrees(deltaX, deltaY);
        int n = (int) (degrees + (EIGHT_CIRCLE / 2));
        int z = (n / 45);
        if (z == 8) {
            z = 0;
        }
        // Log.i("Dir", String.format("asDir(%.1f, %.1f) = %.1f degrees = %s", deltaX, deltaY, degrees, zones[z]));
        return ZONES[z];
    }

    public static double calcDegrees(double x, double y) {
        double radians = Math.atan2(y, x);
        double degrees = (radians * (HALF_CIRCLE / Math.PI));
        if (degrees < 0.0) {
            degrees = FULL_CIRCLE - ((-1) * degrees);
        }
        return degrees;
    }
}
