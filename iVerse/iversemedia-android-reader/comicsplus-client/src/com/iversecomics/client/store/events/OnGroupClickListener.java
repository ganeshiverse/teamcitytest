package com.iversecomics.client.store.events;

import android.support.v4.app.Fragment;

import com.iversecomics.client.store.model.Group;

public interface OnGroupClickListener {
	void onGroupClick(Fragment fragment, Group genre);
}
