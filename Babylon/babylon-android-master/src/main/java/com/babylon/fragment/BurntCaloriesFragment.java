package com.babylon.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.CaloriesAdapter;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.broadcast.SyncFinishedBroadcast;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.Table;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.dialog.OkDialogFragment;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.AbsSyncStrategy;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.timroes.android.listview.EnhancedListView;

public class
        BurntCaloriesFragment extends BaseMonitorMeParamsFragment
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private CaloriesAdapter caloriesAdapter;
    private EnhancedListView caloriesListView;
    private List<PhysicalActivity> autoTrackingActivities = new LinkedList<PhysicalActivity>();

    private List<PhysicalActivity> undoableActivities = new ArrayList<PhysicalActivity>();

    private boolean isCaloriesLoading = false;
    private View footerOtherCaloriesView;

    private int wearableCalories;
    private int otherCalories;

    public static BurntCaloriesFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        BurntCaloriesFragment fragment = new BurntCaloriesFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_burnt_calories, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isCaloriesLoading = true;

        if (App.getInstance().isConnection()) {
            final long maxDate = DateUtils.getDayStartMills(dateInMillis, 1);

            loadPhysicalActivities(maxDate);
            loadMonitorMeFeatures(maxDate);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.babylon.ACTION_SYNC_FINISHED");
        getActivity().registerReceiver(syncFinishedBroadcastReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(syncFinishedBroadcastReceiver);
    }

    private void loadMonitorMeFeatures(long maxDate) {
        final List<String> featureList = new LinkedList<String>();

        featureList.add(MonitorMeParam.BMR.getId());
        featureList.add(MonitorMeParam.WEARABLE_CALORIES.getId());

        MonitorMeParamsGet.newMonitorMeParamsGet(dateInMillis, maxDate,
                featureList).execute(getActivity(), monitorMeFeaturesRequestListener);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.calories_burnt_title);

        setRightImageButton(onDataSourceClickListener, R.drawable.ic_menu_sync);
        setSecondRightImageButton(onAddActivityClickListener, R.drawable.ic_menu_add);

        footerOtherCaloriesView = ((LayoutInflater) getActivity().getSystemService(Context
                .LAYOUT_INFLATER_SERVICE)).inflate
                (R.layout.list_item_calories, null, false);
        footerOtherCaloriesView.setVisibility(View.GONE);

        initListView();
        setupCirclesOnClickListeners(view);
    }

    private void setupCirclesOnClickListeners(View view) {
        view.findViewById(R.id.cycle_text_view).setOnClickListener(onAddActivityClickListener);
        view.findViewById(R.id.others_text_view).setOnClickListener(onAddActivityClickListener);
        view.findViewById(R.id.walk_and_run_text_view).setOnClickListener(onAddActivityClickListener);

        view.findViewById(R.id.metabolism_text_view).setOnClickListener(onMetabolismClickListener);
    }

    private void initListView() {
        caloriesListView = (EnhancedListView) getView().findViewById(R.id.fragment_burnt_calories_list_view);
        caloriesListView.addFooterView(footerOtherCaloriesView);

        caloriesAdapter = new CaloriesAdapter(getActivity(), null,
                CaloriesAdapter.CaloriesAdapterType.CALORIES_BURNT);
        caloriesListView.setAdapter(caloriesAdapter);

        caloriesListView.setUndoStyle(EnhancedListView.UndoStyle.SINGLE_POPUP);
        caloriesListView.setShouldSwipeCallback(shouldSwipeCallback);
        caloriesListView.setDismissCallback(dismissCallback);
        caloriesListView.setUndoHideDelay(getResources().getInteger(R.integer.undo_delay_in_millis));
        caloriesListView.setRequireTouchBeforeDismiss(false);
        caloriesListView.enableSwipeToDismiss();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setProgressBarVisible(isCaloriesLoading);

        startLoaderManager();
    }

    private void loadPhysicalActivities(long maxDate) {
        Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, dateInMillis);
        b.putLong(Constants.EXTRA_MAX_DATE, maxDate);

        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY, b,
                SyncAdapter.SYNC_PULL);
    }

    private void startLoaderManager() {
        getLoaderManager().initLoader(PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_PHYSICAL_ACTIVITIES,
                null, this);
        getLoaderManager().initLoader(PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_CYCLE,
                null, this);
        getLoaderManager().initLoader(PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_WALK_AND_RUN,
                null, this);
        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_WEARABLE_CALORIES_TODAY,
                null, this);
        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_BMR_TODAY,
                null, this);
        getLoaderManager().initLoader(PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_OTHERS,
                null, this);
    }

    @Override
    protected void onTopBarButtonPressed() {
        Cursor notSyncActivityCursor = AbsSyncStrategy.getUpdateCursor(getActivity().getContentResolver(),
                Table.PHYSICAL_ACTIVITY,
                PhysicalActivitySchema.CONTENT_URI, null);

        if (notSyncActivityCursor.moveToFirst()) {
            syncActivities();
        }
        super.onTopBarButtonPressed();
    }

    private void syncActivities() {
        Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, dateInMillis);
        b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(dateInMillis, 1));
        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY, b,
                SyncAdapter.SYNC_PUSH);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id) {
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_PHYSICAL_ACTIVITIES:
                return new CursorLoader(getActivity(),
                        PhysicalActivitySchema.CONTENT_URI, null,
                        PhysicalActivitySchema.Query.DAY_SELECTION_WITHOUT_SLEEPING,
                        PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                        PhysicalActivitySchema.Query.SORT_DATE_ASC);
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_CYCLE:
                return new CursorLoader(getActivity(),
                        PhysicalActivitySchema.CONTENT_URI,
                        new String[]{PhysicalActivitySchema.Query.SUM_KILOCALORIES_COLUMN},
                        PhysicalActivitySchema.Query.DAY_SELECTION_CYCLE,
                        PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                        PhysicalActivitySchema.Query.SORT_DATE_ASC);
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_WALK_AND_RUN:
                return new CursorLoader(getActivity(),
                        PhysicalActivitySchema.CONTENT_URI,
                        new String[]{PhysicalActivitySchema.Query.SUM_KILOCALORIES_COLUMN},
                        PhysicalActivitySchema.Query.DAY_SELECTION_WALK_AND_RUN,
                        PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                        PhysicalActivitySchema.Query.SORT_DATE_ASC);
            case MonitorMeParamSchema.Query.CURSOR_LOAD_WEARABLE_CALORIES_TODAY:
                return new CursorLoader(getActivity(),
                        MonitorMeParamSchema.CONTENT_URI,
                        null,
                        MonitorMeParamSchema.Query.OTHER_CALORIES_TODAY_SELECTION,
                        PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                        null);
            case MonitorMeParamSchema.Query.CURSOR_LOAD_BMR_TODAY:
                return new CursorLoader(getActivity(),
                        MonitorMeParamSchema.CONTENT_URI,
                        null,
                        MonitorMeParamSchema.Query.BMR_TODAY_SELECTION,
                        PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                        null);
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_OTHERS:
                return new CursorLoader(getActivity(),
                        PhysicalActivitySchema.CONTENT_URI,
                        new String[]{PhysicalActivitySchema.Query.SUM_KILOCALORIES_COLUMN},
                        PhysicalActivitySchema.Query.DAY_SELECTION_OTHERS,
                        PhysicalActivitySchema.Query.getDateSelectionArguments(dateInMillis),
                        PhysicalActivitySchema.Query.SORT_DATE_ASC);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        int id = cursorLoader.getId();
        switch (id) {
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_PHYSICAL_ACTIVITIES:
                caloriesAdapter.swapCursor(cursor);
                caloriesListView.resetDeletedViews();
                break;
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_CYCLE:
                showCaloriesSum(R.id.cycle_text_view, cursor);
                break;
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_WALK_AND_RUN:
                showCaloriesSum(R.id.walk_and_run_text_view, cursor);
                break;
            case MonitorMeParamSchema.Query.CURSOR_LOAD_WEARABLE_CALORIES_TODAY:
                MonitorMeFeatures.Feature feature = getFeatureValue(cursor);
                wearableCalories = feature.getFloatValue().intValue();
                showWearableCaloriesListItem(cursor);
                showOthers();
                break;
            case MonitorMeParamSchema.Query.CURSOR_LOAD_BMR_TODAY:
                showFeatureValue(R.id.metabolism_text_view, cursor);
                break;
            case PhysicalActivitySchema.Query.CURSOR_LOAD_TODAY_SUM_OTHERS:
                otherCalories = getCaloriesSumValue(cursor);
                showOthers();
                break;
            default:
                break;
        }
    }

    private void showWearableCaloriesListItem(Cursor cursor) {
        if (cursor.moveToFirst()) {
            final MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature();
            feature.createFromCursor(cursor);

            final int valueInt = feature.getFloatValue().intValue();

            if (valueInt != 0) {
                if (footerOtherCaloriesView.getVisibility() != View.VISIBLE) {
                    footerOtherCaloriesView.setVisibility(View.VISIBLE);
                }

                fillInOtherCaloriesListItem(feature, valueInt);
            }
        }
    }

    private void fillInOtherCaloriesListItem(MonitorMeFeatures.Feature feature, int valueInt) {

        final TextView titleTextView = (TextView) footerOtherCaloriesView.findViewById(R.id
                .list_item_calories_title_text_view);
        final TextView amountTextView = (TextView) footerOtherCaloriesView.findViewById(R.id
                .list_item_calories_amount_text_view);
        final TextView quantityTextView = (TextView) footerOtherCaloriesView.findViewById(R.id
                .list_item_calories_quantity_text_view);

        final ImageView imageView = (ImageView) footerOtherCaloriesView.findViewById(R.id.image_view);

        titleTextView.setText(feature.getName());
        amountTextView.setText(String.valueOf(valueInt));
        imageView.setImageResource(R.drawable.ic_validic_list_item);
        quantityTextView.setText(R.string.location_tracking_daily_activity);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        /* data is not available anymore*/
        caloriesAdapter.changeCursor(null);
    }

    /* methods */
    private void showCaloriesSum(int textViewId, Cursor cursor) {
        TextView caloriesSumTextView = (TextView) getView().findViewById(textViewId);

        if (caloriesSumTextView != null) {
            int caloriesAmount = getCaloriesSumValue(cursor);
            caloriesSumTextView.setText(String.valueOf(caloriesAmount));
        }
    }

    private void showOthers() {
        int other = wearableCalories + otherCalories;
        TextView caloriesSumTextView = (TextView) getView().findViewById(R.id.others_text_view);
        caloriesSumTextView.setText(String.valueOf(other));
    }

    private int getCaloriesSumValue(Cursor cursor) {
        int value = 0;

        if (cursor.moveToFirst()) {
            value = cursor.getInt(cursor.getColumnIndex(
                    PhysicalActivitySchema.Query.SUM_KILOCALORIES_COLUMN));
        }

        return value;
    }

    private void showFeatureValue(@IdRes int textViewId, Cursor cursor) {
        TextView metabolismTextView = (TextView) getView().findViewById(textViewId);

        MonitorMeFeatures.Feature featureValue = getFeatureValue(cursor);

        metabolismTextView.setText(String.valueOf(featureValue.getFloatValue().intValue()));
    }


    private MonitorMeFeatures.Feature getFeatureValue(Cursor cursor) {
        MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature();
        if (cursor.moveToFirst()) {
            feature.createFromCursor(cursor);
        }
        return feature;
    }


    private void deleteActivity(Cursor cursor) {
        PhysicalActivity physicalActivity = new PhysicalActivity();
        physicalActivity.createFromCursor(cursor);
        if (physicalActivity.getSyncWithServer().equals(SyncUtils.SYNC_TYPE_ADDED)) {
            updateSyncedActivity(physicalActivity);
        } else {
            deleteNotSyncedActivity(physicalActivity);
        }
    }

    private void deleteNotSyncedActivity(PhysicalActivity physicalActivity) {
        if (physicalActivity.getSteps() != 0 && !autoTrackingActivities.isEmpty()) {
            for (PhysicalActivity autoTrackingActivity : autoTrackingActivities) {
                deleteActivityFromDatabase(autoTrackingActivity);
            }
        } else {
            deleteActivityFromDatabase(physicalActivity);
        }
    }

    private void updateSyncedActivity(PhysicalActivity physicalActivity) {
        if (physicalActivity.getSteps() != 0 && !autoTrackingActivities.isEmpty()) {
            for (PhysicalActivity autoTrackingActivity : autoTrackingActivities) {
                physicalActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_DELETE);
                updateActivity(autoTrackingActivity);
            }
        } else {
            physicalActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_DELETE);
            updateActivity(physicalActivity);
        }
    }

    private void updateActivity(PhysicalActivity physicalActivity) {
        DatabaseOperationUtils.getInstance().insertPhysicalActivity(physicalActivity,
                getActivity().getContentResolver());
    }

    private void deleteActivityFromDatabase(PhysicalActivity physicalActivity) {
        DatabaseOperationUtils.getInstance().deletePhysicalActivity(physicalActivity,
                getActivity().getContentResolver());
    }

    private final EnhancedListView.OnDismissCallback dismissCallback = new EnhancedListView.OnDismissCallback() {
        private String title;

        @Override
        public EnhancedListView.Undoable onDismiss(EnhancedListView enhancedListView, int position) {
            final Cursor c = caloriesAdapter.getCursor();

            if (c.moveToPosition(position)) {
                title = c.getString(PhysicalActivitySchema.Query.TITLE);


                dismissActivity(c);
                return undoable;
            } else {
                return null;
            }
        }

        private void dismissActivity(Cursor c) {
            final PhysicalActivity physicalActivity = new PhysicalActivity();
            physicalActivity.createFromCursor(c);

            undoableActivities.add(physicalActivity);

            deleteActivity(c);
        }

        private final EnhancedListView.Undoable undoable = new EnhancedListView.Undoable() {

            @Override
            public void undo() {
                int lastIndex = undoableActivities.size() - 1;
                updateActivity(undoableActivities.get(lastIndex));
                undoableActivities.remove(lastIndex);
            }

            @Override
            public void discard() {
                super.discard();
                int lastIndex = undoableActivities.size() - 1;
                undoableActivities.remove(lastIndex);
            }

            @Override
            public String getTitle() {
                return getString(R.string.undo_title, title);
            }
        };
    };

    private final EnhancedListView.OnShouldSwipeCallback shouldSwipeCallback = new EnhancedListView
            .OnShouldSwipeCallback() {
        @Override
        public boolean onShouldSwipe(EnhancedListView enhancedListView, int position) {
            boolean result = false;

            final Cursor c = caloriesAdapter.getCursor();

            if (c.moveToPosition(position)) {
                final PhysicalActivity activity = new PhysicalActivity();
                activity.createFromCursor(c);
                result = activity.isEditable();
            }

            return result;
        }
    };

    private final RequestListener<ResponseList<MonitorMeFeatures>> monitorMeFeaturesRequestListener = new
            RequestListener<ResponseList<MonitorMeFeatures>>() {
                @Override
                public void onRequestComplete(Response<ResponseList<MonitorMeFeatures>> response) {
                    if (response.isSuccess() && response.getData() != null
                            && response.getData().getList() != null) {
                        if (getActivity() != null && isVisible()) {
                            final long maxDate = DateUtils.getDayStartMills(dateInMillis, 1);
                            MonitorMeParamSync.saveMonitorMeParams(getActivity().getContentResolver(),
                                    response, dateInMillis, maxDate);
                        }
                    }
                }
            };

    private final BroadcastReceiver syncFinishedBroadcastReceiver = new SyncFinishedBroadcast() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            final int action = intent.getIntExtra(SyncAdapter.KEY_SYNC_ACTION, -1);
            final String type = intent.getStringExtra(SyncAdapter.KEY_SYNC_TYPE);

            boolean isSyncBoth = type.equals(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY) && action == SyncAdapter
                    .SYNC_BOTH;
            boolean isSyncPull = type.equals(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY) && action == SyncAdapter
                    .SYNC_PULL;

            if (isSyncBoth || isSyncPull) {
                onSyncFinished();
            }
        }

        private void onSyncFinished() {
            if (getActivity() != null && isVisible()) {
                isCaloriesLoading = false;

                setProgressBarVisible(false);
            }
        }
    };

    private final View.OnClickListener onAddActivityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            long beginOfTheDay = getArguments().getLong(Constants.DATE_IN_MILLIS, 0);
            startFragment(AddActivityFragment.getInstance(beginOfTheDay));
        }
    };

    private final View.OnClickListener onMetabolismClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String resultMessage = getString(R.string.calories_metabolism_dialog);
            final OkDialogFragment okDialogFragment = OkDialogFragment.getInstance(resultMessage);
            okDialogFragment.show(getFragmentManager(), OkDialogFragment.TAG);
        }
    };

    private final View.OnClickListener onDataSourceClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startFragment(new DataSourcesFragment());
        }
    };
}
