package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppointmentDetailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 01/09/15.
 */
public class AppointmentDetailsLoader extends DataAsyncTaskLibLoader<AppointmentDetailsResponse> {

    private String token;
    private String appointmentID;
    private String isDictionaryJSONResponse;

    public AppointmentDetailsLoader(Context context, String token, String appointmentID, String isDictionaryJSONResponse) {
        super(context);
        this.token = token;
        this.appointmentID = appointmentID;
        this.isDictionaryJSONResponse = isDictionaryJSONResponse;
    }

    @Override
    protected AppointmentDetailsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getAppointmentDetails(token, appointmentID, isDictionaryJSONResponse);
    }
}
