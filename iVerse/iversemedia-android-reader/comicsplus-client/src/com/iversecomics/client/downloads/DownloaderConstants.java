package com.iversecomics.client.downloads;

public final class DownloaderConstants {
    public static final String ACTION_CLEANUP           = "com.iversecomics.downloader.actions.CLEANUP";
    public static final String ACTION_DOWNLOAD_ADDED    = "com.iversecomics.downloader.actions.DOWNLOAD_ADDED";
    public static final String ACTION_DOWNLOAD_WAKEUP   = "com.iversecomics.downloader.actions.DOWNLOAD_WAKEUP";
    public static final String ACTION_DOWNLOAD_COMPLETE = "com.iversecomics.downloader.actions.DOWNLOAD_COMPLETE";
    public static final String ACTION_DOWNLOAD_PROGRESS = "com.iversecomics.downloader.actions.DOWNLOAD_PROGRESS";
}
