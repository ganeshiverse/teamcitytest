package com.iversecomics.otto.event;

/**
 */
public class DownloadProgressChangeEvent {
    private String uri;
    private long currentByte;
    private long totalBytes;

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setCurrentByte(long currentByte) {
        this.currentByte = currentByte;
    }

    public long getCurrentByte() {
        return currentByte;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

}
