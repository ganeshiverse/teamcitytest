package com.iversecomics.client.store.tasks;

import android.content.Context;

import com.iversecomics.app.ComicApp;
import com.iversecomics.client.store.ComicStore;

public class FetchFeaturedComicsOnDemand extends FetchFeaturedComicsByCategoryString {
    public FetchFeaturedComicsOnDemand(ComicStore comicStore, Context context) {
        super(comicStore, context, "ondemand");
    }

    @Override
    protected void postDataUpdate() {
        ComicApp.getInstance().getDataProducer().postFeaturedComicsOnDemand();
    }
}