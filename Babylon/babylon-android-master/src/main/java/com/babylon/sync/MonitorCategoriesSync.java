package com.babylon.sync;

import android.content.ContentResolver;
import android.os.Bundle;

import com.babylon.App;
import com.babylon.api.request.MonitorCategoriesGet;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.enums.MonitorCategory;
import com.babylon.model.Icon;
import com.babylon.model.MonitorMeCategory;
import com.babylon.utils.PreferencesManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MonitorCategoriesSync extends AbsSyncStrategy {

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        //empty method
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        pullMonitorMeCategories(contentResolver);
    }

    private void pullMonitorMeCategories(ContentResolver contentResolver) {
        Response<ResponseList<MonitorMeCategory>> monitorMeCategories = MonitorCategoriesGet
                .newCategoryListGet(MonitorCategory.getAllNames()).execute();

        if (monitorMeCategories != null && monitorMeCategories.isSuccess()) {
            if (monitorMeCategories.getData() != null && monitorMeCategories.getData().getList() != null
                    && !monitorMeCategories.getData().getList().isEmpty()) {

                /* Save  categories to database for show description of category */
                List<MonitorMeCategory> categories = monitorMeCategories.getData().getList();
                DatabaseOperationUtils.getInstance().saveMonitorMeCategories(contentResolver, categories);

                /* Cache icons  */
                for (MonitorMeCategory category : categories) {
                    cacheIcons(category.getIconsDescription());
                    cacheIcons(category.getIconsReport());
                }

                /* Cache all categories in one string for show monitor me report */
                PreferencesManager.getInstance().setMonitorCategories(categories);
            }
        }
    }

    private void cacheIcons(List<Icon> icons) {
        for (Icon icon : icons) {
            if (icon.getSrc() != null) {
                Picasso.with(App.getInstance()).load(icon.getSrc());
            }
        }
    }
}
