package com.iversecomics.client.bitmap;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;

import com.iversecomics.client.util.Dim;

public class ImageRect extends RectF {
    private static final float MIN_VISIBLE_SIZE = 0.001F;

    public ImageRect() {
        super();
    }

    public ImageRect(float l, float t, float r, float b) {
        super(l, t, r, b);
    }

    public ImageRect(Rect bounds) {
        super();
        left = bounds.left;
        top = bounds.top;
        right = bounds.right;
        bottom = bounds.bottom;
    }

    public ImageRect(RectF rect) {
        super(rect);
    }

    public Rect getBounds() {
        return new Rect((int) left, (int) top, (int) right, (int) bottom);
    }

    public ImageRect insetRectF(float inset) {
        return new ImageRect(this.left + inset, this.top + inset, this.right - inset, this.bottom - inset);
    }

    /**
     * True if the rectangle represents a visible size.
     * 
     * @return true if visible
     */
    public boolean isVisible() {
        return (width() > MIN_VISIBLE_SIZE) && (height() > MIN_VISIBLE_SIZE);
    }

    public float midX() {
        return left + (width() / 2f);
    }

    public float midY() {
        return top + (height() / 2f);
    }

    public void reset() {
        left = 0f;
        top = 0f;
        right = 0f;
        bottom = 0f;
    }

    public void setDim(Dim dim) {
        left = 0F;
        top = 0F;
        right = dim.getWidth();
        bottom = dim.getHeight();
    }

    public void setFrom(Bitmap bitmap) {
        left = 0F;
        top = 0F;
        right = bitmap.getWidth();
        bottom = bitmap.getHeight();
    }
}
