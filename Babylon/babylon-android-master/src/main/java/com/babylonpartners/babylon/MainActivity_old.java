/*
package com.babylonpartners.babylon;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.*;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.appsflyer.AppsFlyerLib;
import com.babylon.*;
import com.babylon.activity.*;
import com.babylon.activity.check.CheckActivity;
import com.babylon.activity.family.FamilyAccountsActivity;
import com.babylon.activity.payments.AppointmentTransactionActivity;
import com.babylon.activity.payments.PaymentCardsListActivity;
import com.babylon.activity.payments.RecordTransactionActivity;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.*;
import com.babylon.api.request.base.AbsRequest;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.controllers.AskController;
import com.babylon.enums.Screen;
import com.babylon.model.*;
import com.babylon.model.VideoSession;
import com.babylon.model.payments.Appointment;
import com.babylon.model.payments.OrderDetail;
import com.babylon.model.push.AppointmentPush;
import com.babylon.model.push.ItemIdPush;
import com.babylon.model.push.NotifAskConsultant;
import com.babylon.model.push.SpecialismPush;
import com.babylon.service.TriageSyncService;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.*;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.gson.Gson;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends FragmentActivity implements
        DialogInterface.OnDismissListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,PasswordConfirmActivity.IWebViewControl {

    @Override
    public void webViewGoBack() {
        webView.goBack();
    }

    public interface OnSpecialistChoiceListener {
        void onSpecialistChoiceClick(int specialistId);
    }

    public interface OnConsultantChoiceListener {
        void onConsultantChoiceClick(int consultantId);
    }

    public static final int DAY_FOR_SAVE_UNREAD_COMMENTS = -1;

    private static final int CODE_HOST_FROM_WEB = 202;

    private static final int LOGIN_REQUEST_CODE = 100;
    public static final int CREATE_ACCOUNT_REQUEST_CODE = 101;
    private static final int RESET_PASSWORD_REQUEST_CODE = 102;
    private static final int SELECT_PRACTICE_REQUEST_CODE = 103;
    private static final int PHONE_NUMBER_REQUEST_CODE = 104;
    private static final int ASK_PHONE_NUMBER_REQUEST_CODE = 105;
    private static final int FAMILY_ACCOUNTS_REQUEST_CODE = 106;
    private static final int KIT_CATEGORIES_REQUEST_CODE = 107;
    private final static int REQUEST_CODE_IMAGE_PICKER = 1;
    private final static int REQUEST_CODE_VIDEO_SESSION = 2;
    private final static int LODER_ID_PENDING_MESSGAE = 1;

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String APPTAG = "Babylon";
    private LocationClient locationClient;

    //A placeholder is used to prevent the WebView being destroyed when the activity is restarted.
    private FrameLayout webViewPlaceholder;
    //webView must be static to persist between Activity launches.
    private WebView webView;
    private com.babylon.model.VideoSession startedVideoSession;
    private boolean waitingForPlayServices = true;
    private Intent pendingIntent;
    private Integer pendingRequestCode = null;
    private WebViewJsManager webViewJsManager;
    private Appointment appointment;
    private boolean isLogin = false;

    */
/*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     *//*

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog dialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            dialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            this.dialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return dialog;
        }
    }

    static enum HtmlPage {
        index, appointments_new, terms;

        public String getUri() {
            return getUri(null);
        }

        public String getUri(String params) {
            StringBuilder uri = new StringBuilder("file:///android_asset/dist/");
            uri.append(name())
                    .append(".html");
            if (!TextUtils.isEmpty(params)) {
                uri.append(params);
            }
            return uri.toString();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Crashlytics.start(this);

        AppsFlyerLib.setAppsFlyerKey(Config.APPSFLYER_DEV_KEY);
        AppsFlyerLib.sendTracking(getApplicationContext());

        initWebView();

        waitingForPlayServices = true;

        final Bundle extras = getIntent().getExtras();

        if (PreferencesManager.getInstance().isFirstLaunch()) {
            PreferencesManager.getInstance().setIsFirstLogin(false);
            PreferencesManager.getInstance().setIsFirstLaunch(false);
            Intent intent = new Intent(this, WelcomeTourActivity.class);
            startIntent(intent);
        } else if (!SyncUtils.isLoggedIn()) {
            startIntent(new Intent(this, RegistrationActivity.class),
                    CREATE_ACCOUNT_REQUEST_CODE);
        } else if (PreferencesManager.getInstance().shouldShowTermsPopup()) {
            PreferencesManager.getInstance().setShouldShowTermsPopup(false);
            Intent termsPopupIntent = new Intent(this, TermsPopupActivity.class);
            startActivity(termsPopupIntent);
        }

        setContentView(R.layout.activity_main);

        if (extras != null) {
            processParsePushNotificationData(extras);
        }

        syncPatientData();

        locationClient = new LocationClient(this, this, this);
        setVisibleTestButtons(SyncUtils.isLoggedIn());
        TriageSyncService.startFirstSync();
    }

    private void initWebView() {
        if (webView == null) {
            webView = new WebView(this);
            webView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
           */
/* getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);*//*

            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webView.addJavascriptInterface(new WebAppInterface(), "Android");
            webView.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    fetchHostFromWeb();
                    int TIME_OUT=500;
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            refreshWebView();
                        }
                    }, TIME_OUT);
                    //   refreshWebView();
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.contains("babylon://")) {
                        String command = url.split("babylon://")[1];
                        Log.w("Babylon-JS-Bridge", "calling from JS: " + command);

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(command);
                        } catch (JSONException e) {
                            L.e(TAG, e.toString(), e);
                        }

                        if (jsonObject != null) {
                            Iterator<?> keys = jsonObject.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                try {
                                    if (jsonObject.isNull(key)) {
                                        nativeCall(key, null);
                                    } else {
                                        Object value = jsonObject.get(key);
                                        nativeCall(key, value);
                                    }
                                } catch (JSONException e) {
                                    L.e(TAG, e.toString(), e);
                                }
                            }
                        }

                        return true;
                    } else if (url.startsWith("tel:")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    } else if (url.startsWith("mailto:")) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse(url));
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivity(intent);
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    Log.d("MyApplication", cm.message() + " -- From line "
                            + cm.lineNumber() + " of "
                            + cm.sourceId());
                    return true;
                }
            });

            String databasePath = webView.getContext().getDir("databases",
                    Context.MODE_PRIVATE).getPath();
            webView.getSettings().setDatabasePath(databasePath);
            webView.getSettings().setDomStorageEnabled(true);

            enableDebugging();
            allowUniversalAccess();
            loadEnvironment(HtmlPage.index);
        }
        webViewJsManager = new WebViewJsManager(webView);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the state of the WebView
        webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore the state of the WebView
        webView.restoreState(savedInstanceState);
    }

    */
/*
     * Called when the Activity becomes visible.
     *//*

    @Override
    protected void onStart() {
        super.onStart();

        // Attach the WebView to its placeholder for save web view hierarchy
        webViewPlaceholder = ((FrameLayout)findViewById(R.id.webViewPlaceholder));
        webViewPlaceholder.removeView(webView);
        webViewPlaceholder.addView(webView);

        // Connect the client.
        locationClient.connect();

        AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.APP_OPEN_EVENT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        webViewJsManager.resumeJsCalls();

        refreshNotificationsCount();
        refreshRegions();
        com.facebook.AppEventsLogger.activateApp(this, getString(R.string.facebook_app_id));

        if (PreferencesManager.getInstance().shouldShowTermsPopup()) {
            PreferencesManager.getInstance().setShouldShowTermsPopup(false);
            Intent termsPopupIntent = new Intent(this, TermsPopupActivity.class);
            startActivity(termsPopupIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        webViewJsManager.pauseJsCalls();
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        locationClient.disconnect();

        AnalyticsWrapper.flush();

        if (webView != null) {
            // Remove the WebView from the old placeholder
            webViewPlaceholder.removeView(webView);
        }
        super.onStop();
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        refreshNotificationsCount();
    }

    private void refreshNotificationsCount() {
        webViewJsManager.callJs("Babylon.Vent.trigger('home:refresh_notifications')");
    }

    private void refreshRegions()
    {
        RegionsGet regionsGet = new RegionsGet();
        regionsGet.execute(this, new RequestListener<Region[]>() {
            @Override
            public void onRequestComplete(Response<Region[]> response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    if (response.getData() != null) {
                        Region[] regions = response.getData();
                        if (regions.length != 0) {
                            PreferencesManager.getInstance().setRegions(regions);

                            refreshCurrentRegionInWebView();
                        }
                    }
                }
            }
        });
    }

    public void mobileAppLog(String message) {
        final AppLogPost appLogPost = new AppLogPost();
        final String patientId = App.getInstance().getPatient().getId();
        final AppLog appLog = new AppLog(message, patientId);

        appLogPost.withEntry(appLog);
        appLogPost.execute(this, appLogRequestListener);
    }

    private void startIntent(Intent intent) {
        startIntent(intent, null);
    }

    private void startIntent(Intent intent, Integer requestCode) {
        if (waitingForPlayServices) {
            pendingIntent = intent;
            pendingRequestCode = requestCode;
        } else {
            if (requestCode != null) {
                startActivityForResult(intent, requestCode);
            } else {
                startActivity(intent);
                finish();
            }
        }
    }

    private void syncPatientData() {
        if (SyncUtils.isLoggedIn()) {
            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MESSAGES, SyncAdapter.SYNC_PULL);
            final boolean isMonitorMeEnabled = new RegionSettings().isMonitorMeEnabled();
            if (isMonitorMeEnabled) {
                SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_CATEGORIES,
                        SyncAdapter.SYNC_PULL);
            }
        }
    }

    private void nativeCall(String methodName, Object object) {
        try {
            if (object != null) {
                Method m = MainActivity.class.getDeclaredMethod(methodName, object.getClass());
                Object[] params = {object};
                m.invoke(MainActivity.this, params);
            } else {
                Method m = MainActivity.class.getDeclaredMethod(methodName);
                m.invoke(MainActivity.this);
            }
        } catch (IllegalArgumentException e) {
            Log.e("Babylon-JS-Bridge", "IllegalArgumentException " + e);
        } catch (IllegalAccessException e) {
            Log.e("Babylon-JS-Bridge", "IllegalAccessException " + e);
        } catch (InvocationTargetException e) {
            Log.e("Babylon-JS-Bridge", "InvocationTargetException ", e);
        } catch (NoSuchMethodException e) {
            Log.e("Babylon-JS-Bridge", "invalid method ", e);
        }
    }

    private void nativeRequestPhoneNumberDialog() {
        Intent intent = new Intent(this, PhoneNumberDialogActivity.class);
        startActivityForResult(intent, PHONE_NUMBER_REQUEST_CODE);
    }

    private boolean isLaunchAfterLoginWithNotification() {
        Bundle extras = getIntent().getExtras();
        boolean isLaunchAfterLoginAfterNotification = false;
        if (extras != null) {
            isLaunchAfterLoginAfterNotification = extras.getBoolean(com.babylon.Keys
                            .IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION,
                    false
            );
        }
        return isLaunchAfterLoginAfterNotification;
    }

    @SuppressLint("NewApi")
    private void enableDebugging() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    public class WebAppInterface {

        @android.webkit.JavascriptInterface
        public void get(int code, String value) {
            switch (code) {
                case CODE_HOST_FROM_WEB:
                    L.d(TAG, "Host from web: " + value);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        final Bundle extras = intent.getExtras();
        if (extras != null) {
            processParsePushNotificationData(extras);
        }
    }

    */
/**
     * Check if user sign in(execute server request for get 401 status
     * of authorized or not)
     *//*

    private void processParsePushNotificationData(Bundle extras) {
        final String data = extras.getString(PushBroadcastReceiver.KEY_PARSE_DATA);
        if (!TextUtils.isEmpty(data)) {
            String action = PushBroadcastReceiver.getAction(data);
            if (!TextUtils.isEmpty(action)) {
                final PushBroadcastReceiver.PushAction pushAction =
                        PushBroadcastReceiver.PushAction.getValue(action.replace("com.babylon.", ""));
                if (pushAction != null) {
                    if (App.getInstance().isConnection()) {
                        PreferencesManager.getInstance().setManualHandlingAutoSignOut(true);
                        new PatientGet().execute(this, new RequestListener<Patient>() {
                            @Override
                            public void onRequestComplete(Response<Patient> response) {
                                boolean isUnauthorized = response.getStatus() == HttpStatus.SC_UNAUTHORIZED;
                                if (isUnauthorized) {
                                    SyncUtils.signOut();
                                }
                                PreferencesManager.getInstance().setManualHandlingAutoSignOut(false);
                                handlePushNotification(pushAction, data, isUnauthorized);
                            }
                        });
                    } else {
                        handlePushNotification(pushAction, data, false);
                    }
                }
            }
        }
    }

    private void handlePushNotification(PushBroadcastReceiver.PushAction pushAction,
                                        String parseData, boolean isUnauthorized) {
        switch (pushAction) {
            case APPOINTMENT_START:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    App.getInstance().stopMediaPlayer();
                    videoAppointmentView(parseData);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case APPOINTMENT_REMINDER:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    bookAnAppointmentView(parseData);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case SUGGEST_CONSULTATION_AFTER_ARCHIVING:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    bookAnAppointmentView(null);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case SUGGEST_CONSULTATION:
            case ADD_COMMENT:
            case REJECT_QUESTION:
            case APPROVE_QUESTION:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    final NotifAskConsultant data = new Gson().fromJson(parseData, NotifAskConsultant.class);
                    String questionId = data.getQuestionId();
                    startQuestionActivityWithQuestion(questionId);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case BLOOD_ANALYSIS_HEALTHY:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    Intent intent = new Intent(this, BaseActivity.class);
                    intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.MONITOR_ME_FRAGMENT_TAG);
                    startActivity(intent);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case BLOOD_ANALYSIS_APPOINTMENT:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    bookAnAppointmentBloodResultView();
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case SUBSCRIPTION:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    showSubscriptionPage();
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case NEW_PRESCRIPTION:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    startNewPrescriptionScreen(parseData);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case REFERRAL:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    startReferralScreen(parseData);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case PATHOLOGY_TEST:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    startPathologyTestScreen(parseData);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            case ACCEPT_UNVERIFIED_PATIENT:
                if (SyncUtils.isLoggedIn() && !isUnauthorized) {
                    bookAnAppointmentView(null);
                } else {
                    startSignInActivity(pushAction, parseData);
                }
                break;
            default:
                loadEnvironment(HtmlPage.index);
                break;
        }
    }

    private void hideWebAppProgressBar() {
        webViewJsManager.callJs("Babylon.Vent.trigger('page:loaded')");
    }

    private void startSignInActivity(PushBroadcastReceiver.PushAction pushAction, String parseData) {
        hideWebAppProgressBar();

        Intent intent = new Intent(this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Keys.NOTIFICATION_DATA, pushAction);
        intent.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, parseData);
        startActivity(intent);
    }

    @SuppressLint("NewApi")
    private void allowUniversalAccess() {
        if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN)
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
    }

    private void loadEnvironment(HtmlPage html) {
        loadEnvironment(html, null);
    }

    private void loadEnvironment(HtmlPage html, String params) {
        webView.loadUrl(html.getUri(params));
        Log.d(TAG,html.getUri(params).toString());
    }

    // Method calling via reflection
    public void mixpanelEvent(String screenStr) {
        Screen screen = Screen.valueOf(screenStr);
        switch (screen) {
            case consultant:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_CONSULTANT_CLICK);
                }
                break;
            case record:
                if(TextUtils.isEmpty(App.getInstance().getPatient().getFacebookUserId())) {
                    PasswordConfirmActivity.mainActivityInstance = MainActivity.this;
                    Intent temp = new Intent(MainActivity.this, PasswordConfirmActivity.class);
                    startActivity(temp);

                }
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.OPENED_RECORD);
                }


                break;
            case settings:

                if (PreferencesManager.getInstance().isFirstLogin()) {
                    requestScreen(Screen.createAccount.toString());
                } else if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.OPENED_SETTINGS);
                }
                break;
            case notifications:
                if (PreferencesManager.getInstance().isFirstLogin()) {
                    requestScreen(Screen.createAccount.toString());
                } else if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_NOTIFICATIONS_CLICK);
                }
                break;
            case monitorMe:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_MONITOR_CLICK);
                }
                break;
            case ask:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_ASK_CLICK);
                }
                break;
            case shop:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_SHOP_CLICK);
                }
                break;
            case check:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_CHECK_CLICK);
                }
                break;
            case appointmentTried:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.APPOINTMENT_TRIED);
                }
                break;
            case appointmentBooked:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.APPOINTMENT_BOOKED);
                }
                break;
            case prescriptionReceived:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.PRESCRIPTION_RECEIVED);
                }
                break;
            default:
                break;
        }

    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void startVideo(JSONObject arguments) {
        String sessionId = null;
        String token = null;
        String identifier = null;
        String consultantName = null;
        String consultantAvatarUrl = null;
        String appointmentId = null;
        boolean enableVoiceCalls = true;
        try {
            token = arguments.getString("token");
            sessionId = arguments.getString("session");
            identifier = arguments.getString("identifier");
            consultantName = arguments.getString("consultantName");
            consultantAvatarUrl = arguments.getString("consultantAvatarUrl");
            appointmentId = arguments.getString("appointmentId");
            enableVoiceCalls = arguments.getBoolean("enableVoiceCalls");
        } catch (JSONException e) {
            if (token == null || sessionId == null) {
                Log.e("Babylon-video", "startVideo failed", e);
                return;
            }
        }

        Intent videoIntent = new Intent(this, VideoSessionActivity.class);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_SESSION_ID, sessionId);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_TOKEN, token);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_IDENTIFIER, identifier);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_NAME, consultantName);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_AVATAR_URL, consultantAvatarUrl);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_ENABLE_VOICE_CALLS, enableVoiceCalls);

        startedVideoSession = new VideoSession();
        startedVideoSession.setAppointmentId(appointmentId);

        startActivityForResult(videoIntent, REQUEST_CODE_VIDEO_SESSION);
    }

    public void startAskControllerActivity(String questionId) {
        if (SyncUtils.isLoggedIn()) {
            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ASK_OPEN_EVENT);
            Bundle bundle = null;
            if (questionId != null) {
                bundle = new Bundle();
                bundle.putString(Keys.QUESTION_ID, questionId);
            }
            new AskController(this).start(bundle);
        } else {
            notLoggedInAction();
        }
    }

    */
/**
     * @param data if null open view for booking appointment
     *             else open already created appointment with <b>id</b>
     *//*

    public void bookAnAppointmentView(String data) {
        AppointmentPush parseData = null;
        if (data != null) {
            parseData = GsonWrapper.getGson().fromJson(data,
                    AppointmentPush.class);
        }

        StringBuilder params = new StringBuilder("#appointments/");
        if (parseData == null || TextUtils.isEmpty(parseData.getAppointmentId())) {
            params.append("new");
        } else {
            params.append(parseData.getAppointmentId());
        }
        loadEnvironment(HtmlPage.index, params.toString());
    }


    public void videoAppointmentView(String data) {
        AppointmentPush parseData = null;
        if (data != null) {
            parseData = GsonWrapper.getGson().fromJson(data, AppointmentPush.class);
        }

        boolean dataEmpty = (parseData == null || TextUtils.isEmpty(parseData.getAppointmentId()));
        if (dataEmpty) return;

        String appointmentId = parseData.getAppointmentId();

        final AppointmentGet appointmentGet = new AppointmentGet(appointmentId);
        appointmentGet.execute(this, new RequestListener<Appointment>() {
            @Override
            public void onRequestComplete(Response<Appointment> response) {
                if (response.isSuccess()) {
                    Appointment appointment = response.getData();
                    fetchAndStartVideoSession(appointment);
                }
                getSupportLoaderManager().destroyLoader(appointmentGet.hashCode());
            }
        });
    }

    private void fetchAndStartVideoSession(Appointment appointment) {
        final VideoSessionGet videoSessionGet = new VideoSessionGet(appointment.videoSessionId());

        videoSessionGet.execute(this, new RequestListener<com.babylon.model.VideoSession>() {
            @Override
            public void onRequestComplete(Response<com.babylon.model.VideoSession> videoResponse) {
                if (videoResponse.isSuccess()) {
                    com.babylon.model.VideoSession videoSession = videoResponse.getData();
                    startVideoSession(videoSession);
                }
                getSupportLoaderManager().destroyLoader(videoSessionGet.hashCode());
            }
        });
    }

    private void startVideoSession(com.babylon.model.VideoSession videoSession) {
        Intent videoIntent = new Intent(this, VideoSessionActivity.class);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_SESSION_ID, videoSession.session());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_TOKEN, videoSession.uniqueToken());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_IDENTIFIER, videoSession.id());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_NAME, videoSession.consultantName());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_AVATAR_URL, videoSession.consultantAvatarUrl());
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_ENABLE_VOICE_CALLS, videoSession.enabledVoiceCalls());

        startedVideoSession = videoSession;
        startActivityForResult(videoIntent, REQUEST_CODE_VIDEO_SESSION);
    }

    public void startNewPrescriptionScreen(String notificationJsonData) {
        if (notificationJsonData != null) {
            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.PRESCRIPTION_OPENED);
            ItemIdPush pushNotification = GsonWrapper.getGson().fromJson(notificationJsonData,
                    ItemIdPush.class);
            if (pushNotification != null && !TextUtils.isEmpty(pushNotification.getItemId())) {
                loadEnvironment(HtmlPage.index, "#prescriptions/" + pushNotification.getItemId());
            }
        }
    }

    public void startReferralScreen(String notificationJsonData) {
        if (notificationJsonData != null) {
            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.REFERRAL_OPENED);
            SpecialismPush pushNotification = GsonWrapper.getGson().fromJson(notificationJsonData,
                    SpecialismPush.class);
            if (pushNotification != null && !TextUtils.isEmpty(pushNotification.getSpecialism())) {
                loadEnvironment(HtmlPage.index, "#referral-notifications/"
                        + Uri.encode(pushNotification.getSpecialism()));
            }
        }
    }

    public void startPathologyTestScreen(String notificationJsonData) {
        if (notificationJsonData != null) {
            ItemIdPush pushNotification = GsonWrapper.getGson().fromJson(notificationJsonData,
                    ItemIdPush.class);
            if (pushNotification != null && !TextUtils.isEmpty(pushNotification.getItemId())) {
                loadEnvironment(HtmlPage.index, "#pathology-tests/" + pushNotification.getItemId());
            }
        }
    }

    public void bookAnAppointmentBloodResultView() {
        final String params = "#appointments/new?type=bloodtest";
        loadEnvironment(HtmlPage.index, params);
    }

    public void showSubscriptionPage() {
        loadEnvironment(HtmlPage.index, "#subscriptions");
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    // Checks if we have an internet connection or not
    public void checkOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            webViewJsManager.callJs("Babylon.Vent.trigger('internetConnection', true)");
            return;
        }
        webViewJsManager.callJs("Babylon.Vent.trigger('internetConnection', false)");
    }

    public void getPosition() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        if (lastKnownLocation == null) {
            webViewJsManager.callJs("Babylon.Vent.trigger('position:not_found'");
        } else {
            double latitude = lastKnownLocation.getLatitude();
            double longitude = lastKnownLocation.getLongitude();
            webViewJsManager.callJs("Babylon.Vent.trigger('position:found', " + latitude + ", " + longitude + ")");
        }
    }

    private void noInternetAlert() {
        AlertDialogHelper.getInstance().showMessage(this, R.string.offline_error, this);
    }

    private void requestAskScreen() {
        webViewJsManager.startWebSpinner();
        new PatientGet().execute(this, new RequestListener<Patient>() {
            @Override
            public void onRequestComplete(Response<Patient> response) {
                webViewJsManager.stopWebSpinner();
                Patient patient = response.getData();
                if (patient == null) {
                    noInternetAlert();
                } else {
                    App.getInstance().setPatient(patient);
                    if (patient.hasPhoneNumber()) {
                        startAskControllerActivity(null);
                    } else {
                        startActivityForResult(new Intent(MainActivity.this,
                                        PhoneNumberDialogActivity.class),
                                ASK_PHONE_NUMBER_REQUEST_CODE);
                    }
                }
            }
        });
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void requestScreen(String screen) {
        Screen page = Screen.valueOf(screen);
        if (!page.isOfflineModeSupported() && !App.getInstance().isConnection()) {
            noInternetAlert();
            return;
        }

        Intent intent = null;
        int requestCode = -1;
        switch (page) {
            case login:
                intent = new Intent(this, SignInActivity.class);
                requestCode = LOGIN_REQUEST_CODE;

                hideWebAppProgressBar();
                break;
            case createAccount:
                if (isLaunchAfterLoginWithNotification()) {
                    refreshWebView();
                } else {
                    intent = new Intent(this, RegistrationActivity.class);
                    requestCode = CREATE_ACCOUNT_REQUEST_CODE;
                }

                hideWebAppProgressBar();
                break;
            case ask:
                if (SyncUtils.isLoggedIn()) {
                    requestAskScreen();
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_ASK_CLICK);
                } else {
                    notLoggedInAction();
                }
                break;
            case ask_history:
                if (SyncUtils.isLoggedIn()) {
                    intent = new Intent(this, BaseActivity.class);
                    intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, page.name());
                } else {
                    notLoggedInAction();
                }
                break;
            case monitorMe:
                if (SyncUtils.isLoggedIn()) {
                    intent = new Intent(this, BaseActivity.class);
                    intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE,
                            Constants.MONITOR_ME_FRAGMENT_TAG);
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_MONITOR_CLICK);
                } else {

                    notLoggedInAction();
                }
                break;
            case resetPassword:
                intent = new Intent(this, ResetPasswordActivity.class);
                requestCode = RESET_PASSWORD_REQUEST_CODE;
                break;
            case loggedOut:
                loggedOut();
                break;
            case terms:
                intent = new Intent(this, TermsAndConditionsActivity.class);
                break;
            case check:
                if (SyncUtils.isLoggedIn()) {
                    TriageSyncService.startSync();
                    intent = new Intent(this, CheckActivity.class);
                } else {
                    notLoggedInAction();
                }
                break;
            case replayPhoneConsultation:
                //TODO: Display audio playback dialog
                Log.d("MainActivity", "replayPhoneConsultation");
                String link = "http://staging.babylon.alliants.co" +
                        ".uk/public_audio/4c7ff6d9-dddd-4dcf-a50e-0609df4b4194/open-uri20140909-23086-xnfsxx";
                intent = new Intent(this, AudioPlaybackDialogActivity.class);
                intent.putExtra("audio_url_extra", link);
                break;
            case selectPractice:
                Log.d("MainActivity", "selectPractice");
                intent = new Intent(this, SelectPracticeActivity.class);
                requestCode = SELECT_PRACTICE_REQUEST_CODE;
                break;
            case subscriptions:
                intent = new Intent(this, RecordTransactionActivity.class);
                intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.SUBSCRIPTIONS_FRAGMENT_TAG);
                break;
            case billings:
                intent = new Intent(this, RecordTransactionActivity.class);
                intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.BILLINGS_FRAGMENT_TAG);
                break;
            case paymentDetails:
                intent = new Intent(this, PaymentCardsListActivity.class);
                break;
            case familyAccounts:
                if(App.getInstance().isConnection())
                {
                    intent = new Intent(this, FamilyAccountsActivity.class);
                    requestCode = FAMILY_ACCOUNTS_REQUEST_CODE;
                }
                break;
            case measureMe:
                intent = new Intent(this,ShopCategoriesActivity.class);
                requestCode = KIT_CATEGORIES_REQUEST_CODE;
                break;
            default:
                L.d(TAG, "Unknown screen");
        }
        if (intent != null) {

            startActivityForResult(intent, requestCode);

        }
    }



    // Method called via reflection from shouldOverrideUrlLoading()
    public void requestAudioReplay(String url) {
        AudioRecordingGet getRequest = new AudioRecordingGet(url);
        getRequest.execute(this, new RequestListener<AudioRecordingInfo>() {
            @Override
            public void onRequestComplete(Response<AudioRecordingInfo> response) {
                final AudioRecordingInfo info = response.getData();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Date now = new Date();
                        if (now.compareTo(info.getExpiry()) < 0) {
                            Intent intent = new Intent(MainActivity.this, AudioPlaybackDialogActivity.class);
                            intent.putExtra("audio_url_extra", info.getUrl());
                            startActivityForResult(intent, -1);
                        } else {
                            String errorText = getString(R.string.audio_recording_expired_message);
                            Toast.makeText(MainActivity.this, errorText, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void nativeAlert(String message) {
        */
/* handle incorrect using of \n symbol *//*

        message = message.replaceAll("/n", "\n");
        AlertDialogHelper.getInstance().showMessage(this, message, this);
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void nativeConfirm(String message) {
        message = message.replaceAll("/n", "\n");
        AlertDialogHelper.getInstance().showDefaultSimpleConfirmationDialog(this, message, new AlertDialogHelper
                .ConfirmationDialogListener() {
            @Override
            public void confirm() {
                webViewJsManager.callJs("Babylon.Vent.trigger('nativeConfirmCallback', true);");
            }

            @Override
            public void cancel() {
                webViewJsManager.callJs("Babylon.Vent.trigger('nativeConfirmCallback', false);");
            }
        });
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void nativeLog(String message) {
        L.d(TAG, message);
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void specialistSelect(JSONArray jsonArraySpecialist) {
        if (jsonArraySpecialist != null && jsonArraySpecialist.length() != 0) {
            OnSpecialistChoiceListener onSpecialistChoiceListener = new OnSpecialistChoiceListener() {
                @Override
                public void onSpecialistChoiceClick(int specialistId) {
                    webViewJsManager.callJs("Babylon.Vent.trigger('specialistPicked',"
                            + String.valueOf(specialistId) + ")");
                }
            };
            AlertDialogHelper.getInstance().showSpecialistAlert(
                    this, jsonArraySpecialist, onSpecialistChoiceListener);
        }
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void consultantSelect(JSONArray jsonArrayConsultant) {
        if (jsonArrayConsultant != null && jsonArrayConsultant.length() != 0) {
            OnConsultantChoiceListener onConsultantChoiceListener = new OnConsultantChoiceListener() {
                @Override
                public void onConsultantChoiceClick(int specialistId) {
                    webViewJsManager.callJs("Babylon.Vent.trigger('consultantPicked',"
                            + String.valueOf(specialistId) + ")");
                }
            };
            AlertDialogHelper.getInstance().showConsultantAlert(
                    this, jsonArrayConsultant, onConsultantChoiceListener);
        }
    }

    // Method called via reflection from shouldOverrideUrlLoading()
    public void confirmAppointment(JSONObject data) {
        appointment = new Gson().fromJson(data.toString(), Appointment.class);
        startAppointmentPaymentProcess();
    }

    // Method called via reflection from shouldOverrideUrlLoading()
    public void payForAppointment(JSONObject data) {
        appointment = new Gson().fromJson(data.toString(), Appointment.class);
        if (appointment.getGpConsent()) {
            startAppointmentPaymentProcess();
        } else {
            AlertDialogHelper.getInstance().showGpConsentDialog(this, gpConsentConfirmationListener);
        }
    }

    // Method called via reflection from shouldOverrideUrlLoading()
    public void familySwitchedToOwner() {
        Patient patient = App.getInstance().getPatient();
        patient.setSwitchedToFamilyAccountId(-1);
    }

    private AlertDialogHelper.ConfirmationDialogListener gpConsentConfirmationListener = new AlertDialogHelper.ConfirmationDialogListener() {
        @Override
        public void confirm() {
            appointment.setGpConsent(true);
            new AppointmentPatch().withEntry(appointment, new String[] {"gp_consent"}).execute(MainActivity.this, appointmentPatchRequestListener);
        }
        @Override
        public void cancel() {
            startAppointmentPaymentProcess();
        }
    };

    private RequestListener<Appointment> appointmentPatchRequestListener = new RequestListenerErrorProcessed<Appointment>() {
        @Override
        public void onRequestComplete(Response<Appointment> response, String errorMessage) {
            if (!response.isSuccess()) {
                AlertDialogHelper.getInstance().showMessage(MainActivity.this, response.getUserResponseError());
            }
            startAppointmentPaymentProcess();
        }
    };

    private void startAppointmentPaymentProcess() {
        final OrderDetail orderDetail = new OrderDetail(getString(R.string.appointment_order_details),
                appointment.getPrice());
        AppointmentTransactionActivity.startActivity(this, orderDetail, appointment);
    }

    private void notLoggedInAction() {
        requestScreen(Screen.createAccount.toString());
    }

    public void loggedOut() {
        getIntent().putExtra(com.babylon.Keys
                .IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, false);

        PreferencesManager.getInstance().setPatientAddress(null);

        SyncUtils.signOut();

        setVisibleTestButtons(false);

        startIntent(new Intent(this, RegistrationActivity.class), CREATE_ACCOUNT_REQUEST_CODE);
    }

    private void refreshWebView()
    {
        Wsse wsse = SyncUtils.getWsse();

        if (wsse != null && !isLogin && SyncUtils.isLoggedIn()) {
            Bundle extras = getIntent().getExtras();
            if (extras != null && extras.getBoolean(Keys.SHOULD_START_HOME_ACTIVITY)) {
                String apiKey = wsse.getToken();
                Patient patient = App.getInstance().getPatient();
                String patientJson = GsonWrapper.getGson().toJson(patient);

                final StringBuilder js = new StringBuilder();
                js.append(String.format("Babylon.Vent.trigger('patient:logged_in', %s);", patientJson))
                        .append(String.format("Babylon.Vent.trigger('set:request:header', '%s');", apiKey))
                        .append("Babylon.Vent.trigger('homePage:reload');");
                webViewJsManager.callJs(js.toString());
                isLogin = true;
            }

            refreshCurrentRegionInWebView();
            refreshVersionHeaderInWebView();

        }
        // TODO: still needed?
        setVisibleTestButtons(SyncUtils.isLoggedIn());

    }

    private void refreshCurrentRegionInWebView()
    {
        Region region = App.getInstance().getCurrentRegion();
        String regionJson = GsonWrapper.getGson().toJson(region);

        String setVersionHeader = String.format("Babylon.Vent.trigger('region:loaded', %s);", regionJson);
        webViewJsManager.callJs(setVersionHeader);
    }

    private void refreshVersionHeaderInWebView()
    {
        String setVersionHeader = String.format("Babylon.Vent.trigger('set:request:version_header', %s);", getRequestHeaderForVersion());
        webViewJsManager.callJs(setVersionHeader);
    }

    public void fetchHostFromWeb() {
        webViewJsManager.callJs(String.format("Android.get(%d,Babylon.Config.baseUrl);", CODE_HOST_FROM_WEB));
    }

    //Image upload methods
    public void pickImage(String source) {
        Intent imagePicker = new Intent(MainActivity.this, Base64ImagePicker.class);
        imagePicker.putExtra(Base64ImagePicker.TYPE_IDENTIFIER, source);
        startActivityForResult(imagePicker, REQUEST_CODE_IMAGE_PICKER);
    }

    private void imagePickerPicked(String base64) {
        webViewJsManager.callJs("Babylon.Vent.trigger('imagePicked', '" + base64.replace("\n", "") + "')");
    }

    private void imagePickerCancelled() {
        webViewJsManager.callJs("Babylon.Vent.trigger('imagePickerCancelled')");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_VIDEO_SESSION:
                VideoSession session = startedVideoSession;
                webViewJsManager.callJs("Babylon.Vent.trigger('rating:display', "
                        + session.appointmentId() + ")");
                startedVideoSession = null;
                break;
            case REQUEST_CODE_IMAGE_PICKER:
                if (resultCode == RESULT_OK) {
                    imagePickerPicked(data.getStringExtra(Base64ImagePicker.IMAGE_DATA));
                } else {
                    imagePickerCancelled();
                }
                break;
            case CREATE_ACCOUNT_REQUEST_CODE: //Intentional fall-through
            case LOGIN_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    refreshWebView();
                } else {
                    finish();
                }
                break;
            case PHONE_NUMBER_REQUEST_CODE:
                if(resultCode == Activity.RESULT_OK) {
                    webViewJsManager.callJs("Babylon.Vent.trigger('patient:phoneNumberSaved')");
                }
                break;
            case ASK_PHONE_NUMBER_REQUEST_CODE:
                if(resultCode == Activity.RESULT_OK) {
                    startAskControllerActivity(null);
                }
                break;
            case FAMILY_ACCOUNTS_REQUEST_CODE:
                if (resultCode == FamilyAccountsActivity.SWITCH_ACCOUNT_RESULT_CODE) {
                    String accountJson = data.getStringExtra(FamilyAccountsActivity.ACCOUNT_EXTRA_KEY);
                    switchAccount(accountJson);
                }
                break;
        }
        */
/*
         * Handle results returned to the FragmentActivity
         * by Google Play services
         *//*

        if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    */
/*
                    Try the request again
                     *//*

                    break;
            }
        }
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates", "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason.
            // resultCode holds the error code.
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getSupportFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }

    public void showDateTimePicker(Integer timeInterval, boolean allowPast, boolean allowFuture) {
        Date date = new Date(timeInterval * 1000L);
        Date now = new Date();

        TimePickerDialog tpd = new TimePickerDialog(this, date, new TimePickerDialog.TimePickerDialogHandler() {
            @Override
            public void onDateTimePicked(Date date) {
                if(date.before(new Date()))
                {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Attention")
                            .setMessage("You are trying to book an appointment in the past. Please select a valid date and time.")
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which)
                                        {
                                            dialog.dismiss();
                                        }
                                    })
                            .setCancelable(false)
                            .show();
                }
                else {
                    Long unixTime = date.getTime() / 1000L;
                    webViewJsManager.callJs("Babylon.Vent.trigger('dateTimePicked', " + unixTime + ")");
                }
            }
        });
        if (!allowPast) {
            tpd.setMinDate(now);
        }
        if (!allowFuture) {
            tpd.setMaxDate(now);
        }
    }

    public void showFutureDateTimePicker(Integer timeInterval) {
        showDateTimePicker(timeInterval, true, true);
    }

    public void showFutureDateTimePicker(String timeIntervalString) {
        try {
            Integer timeInterval = Integer.parseInt(timeIntervalString);
            showFutureDateTimePicker(timeInterval);
        } catch(NumberFormatException e) {
            //Ignoring exception
        }
    }

    public void showPastDateTimePicker(Integer timeInterval) {
        showDateTimePicker(timeInterval, true, false);
    }

    public void showPastDateTimePicker(String timeIntervalString) {
        try {
            Integer timeInterval = Integer.parseInt(timeIntervalString);
            showPastDateTimePicker(timeInterval);
        } catch(NumberFormatException e) {
            //Ignoring exception
        }
    }

    public void showDateTimePicker(String timeInterval) {
        try {
            Integer time = Integer.valueOf(timeInterval);
            showDateTimePicker(time, true, true);
        } catch (NumberFormatException e) {
            L.e(TAG, e.toString(), e);
        }
    }

    @Override
    public void onBackPressed() {
        String end = "if (Babylon.history.length == 1) window.location = 'babylon://{ \"endApplication\": null }';";
        String back = "if (Babylon.history.length > 1) Babylon.Vent.trigger('request:page:back');";
        webViewJsManager.callJs(end);
        webViewJsManager.callJs(back);
    }

    public void endApplication() {
        finish();
    }

    private void startQuestionActivity(boolean shouldCheckPhoneNumber) {
        if (!shouldCheckPhoneNumber) {
            Intent intent = new Intent(this, com.babylon.activity.AskQuestionActivity.class);
            startActivity(intent);
            return;
        }

        webViewJsManager.startWebSpinner();
        new PatientGet().execute(this, new RequestListener<Patient>() {
            @Override
            public void onRequestComplete(Response<Patient> response) {
                webViewJsManager.stopWebSpinner();
                Patient patient = response.getData();
                if (patient == null) {
                    noInternetAlert();
                } else {
                    App.getInstance().setPatient(patient);
                    if (patient.hasPhoneNumber()) {
                        startQuestionActivity(false);
                    } else {
                        startActivityForResult(new Intent(MainActivity.this, PhoneNumberDialogActivity.class),
                                ASK_PHONE_NUMBER_REQUEST_CODE);
                    }
                }
            }
        });
    }


    public void startQuestionActivityWithQuestion(String questionId) {
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(Keys.QUESTION_ID, questionId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private final RequestListener<String> appLogRequestListener = new RequestListener<String>() {
        @Override
        public void onRequestComplete(Response<String> response) {
            //process response here
        }
    };

    */
/*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     *//*

    @Override
    public void onConnected(Bundle dataBundle) {
        App.getInstance().setLocation(locationClient.getLastLocation());
        if (waitingForPlayServices) {
            waitingForPlayServices = false;
            if (pendingIntent != null) {
                startIntent(pendingIntent, pendingRequestCode);
                pendingIntent = null;
                pendingRequestCode = null;
            }
        }
    }

    */
/*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     *//*

    @Override
    public void onDisconnected() {
        waitingForPlayServices = true;
    }

    */
/*
     * Called by Location Services if the attempt to
     * Location Services fails.
     *//*

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        */
/*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         *//*

        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                */
/*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 *//*

            } catch (IntentSender.SendIntentException e) {
                // Log the error
                L.e(TAG, e.toString(), e);
            }
        } else {
            */
/*
             * If no resolution is available, display a dialog to the
             * user with the error.
             *//*

            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    */
/**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     *//*

    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                this,
                CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getSupportFragmentManager(), APPTAG);
        }
    }
    */
/*
    Invisible buttons with id and action for auto tests in UI Automation
     *//*


    public void onTestClick(View view) {
        switch (view.getId()) {
            case R.id.ask_button:
                requestScreen(Screen.ask.name());
                break;
            case R.id.monitor_button:
                requestScreen(Screen.monitorMe.name());
                break;
            case R.id.ask_history_button:
                requestScreen(Screen.ask_history.name());
                break;
            case R.id.check_button:
                requestScreen(Screen.check.name());
                break;
            case R.id.sign_in_button:
                requestScreen(Screen.login.name());
                break;
            case R.id.sign_up_button:
                requestScreen(Screen.createAccount.name());
                break;
            case R.id.reset_password_button:
                requestScreen(Screen.resetPassword.name());
                break;
            case R.id.logout_button:
                requestScreen(Screen.loggedOut.name());
                break;
            case R.id.terms_button:
                requestScreen(Screen.terms.name());
                break;
            default:
                break;
        }
    }

    private void setVisibleTestButtons(boolean isSignIn) {
        Button askButton = (Button) findViewById(R.id.ask_button);
        Button monitorButton = (Button) findViewById(R.id.monitor_button);
        Button askHistoryButton = (Button) findViewById(R.id.ask_history_button);
        Button checkButton = (Button) findViewById(R.id.check_button);
        Button signInButton = (Button) findViewById(R.id.sign_in_button);
        Button signUpButton = (Button) findViewById(R.id.sign_up_button);
        Button resetPasswordButton = (Button) findViewById(R.id.reset_password_button);
        Button logoutButton = (Button) findViewById(R.id.logout_button);
        Button termsButton = (Button) findViewById(R.id.terms_button);

        boolean isProduction = BuildConfig.BUILD_TYPE.equals("release");

        int signInVisibility = isProduction || !isSignIn ?
                View.GONE : View.VISIBLE;
        int signOutVisibility = !isProduction && isSignIn ?
                View.VISIBLE : View.GONE;

        askButton.setVisibility(signInVisibility);
        monitorButton.setVisibility(signInVisibility);
        askHistoryButton.setVisibility(signInVisibility);
        checkButton.setVisibility(signInVisibility);
        logoutButton.setVisibility(signInVisibility);

        signInButton.setVisibility(signOutVisibility);
        signUpButton.setVisibility(signOutVisibility);
        resetPasswordButton.setVisibility(signOutVisibility);

        termsButton.setVisibility(isProduction ? View.GONE : View.VISIBLE);
    }

    private String getRequestHeaderForVersion()
    {
        HashMap headers = new HashMap();

        for (Map.Entry<String, String> entry : AbsRequest.getVersionHeader().entrySet())
            headers.put(entry.getKey(), entry.getValue());

        return GsonWrapper.getGson().toJson(headers);
    }


    private void switchAccount(String accountJson) {
        Gson gson = new Gson();
        Account account = gson.fromJson(accountJson, Account.class);
        Patient currentPatient = App.getInstance().getPatient();
        currentPatient.setSwitchedToFamilyAccountId(Integer.parseInt(account.getId()));
        Region currentRegion = App.getInstance().getCurrentRegion();
        String regionJson = GsonWrapper.getGson().toJson(currentRegion);
        webViewJsManager.callJs(String.format("Babylon.Vent.trigger('set:request:header', '%s');", account.getSwitchAccountAccessToken()));
        webViewJsManager.callJs(String.format("Babylon.Vent.trigger('set:request:version_header', %s);", getRequestHeaderForVersion()));
        webViewJsManager.callJs(String.format("Babylon.Vent.trigger('patient:logged_in', %s);", accountJson));
        webViewJsManager.callJs(String.format("Babylon.Vent.trigger('region:loaded', %s);", regionJson));
        webViewJsManager.callJs("Babylon.routers.app.index()");
    }

}*/
