package com.babylon.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.Keys;
import com.babylon.PushBroadcastReceiver;
import com.babylon.R;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.MessageSchema;
import com.babylon.model.Comment;
import com.babylon.model.Message;
import com.babylon.model.push.NotifAskConsultant;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.BackbonePageConstants;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.view.MessageView;
import com.babylon.view.TopBarInterface;
import com.babylonpartners.babylon.HomePageBackboneActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class QuestionActivity extends BaseActivity {
    private static final int LOAD_QUESTION = 2135;

    private LinearLayout answerView;
    private Comment comment;
    /**
     * this array could send from AskQuestionActivity, because while record
     * will be uploading to server, audio file path will be unavailable
     */
    private byte[] audioArray;

    private View.OnClickListener bookConsultationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            updateComment();
            sendSuggestConsultationBroadcast();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ANSWER_READ_EVENT);

        answerView = (LinearLayout) findViewById(R.id.answer_linear_layout);

        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                getSupportLoaderManager().initLoader(LOAD_QUESTION, extras, questionLoader);
                audioArray = extras.getByteArray(Keys.AUDIO_BYTE_ARRAY);
            }
        }

        setTitle(getString(R.string.ask_question_title));
        initializeTopBar();
    }

    @Override
    public void onBackPressed() {
        updateComment();
        super.onBackPressed();
    }

    private void updateComment() {
        if (comment != null
                && !comment.isRead()) {
            DatabaseOperationUtils.getInstance().updateCommentsAsRead(getContentResolver());
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.GREEN_THEME;
    }

    private void showAnswer(Message message)
    {
        answerView.removeAllViews();


        int  messageStateId = message.getStateId();
        switch (messageStateId) {
            case Message.OPEN_STATE_ID:
            case Message.IN_PROGRESS_STATE_ID:
                if (message.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADDED) {
                    showAnswerView(R.layout.view_answer_sended, messageStateId);
                }
                break;
            case Message.DONE_STATE_ID:
            case Message.SUGGEST_APPOINTMENT_STATE_ID:
                showDoneAnswerView(message.getComment(), messageStateId);
                comment = message.getComment();
                break;
            case Message.REJECT_STATE_ID:
                showAnswerView(R.layout.view_answer_reject, messageStateId);
                comment = message.getComment();
                break;
            default:
                break;
        }
        setAnswerViewMargin(message);
    }

    private void setAnswerViewMargin(Message message) {
        boolean isMessageHasAttach = message.hasAudio() || message.hasPhoto();
        if (!isMessageHasAttach) {
            switch (message.getStateId()) {
                case Message.DONE_STATE_ID:
                case Message.SUGGEST_APPOINTMENT_STATE_ID:
                case Message.REJECT_STATE_ID:
                    answerView.setPadding(0, 0, 0, 0);
                    break;
                default:
                    break;
            }
        }
    }

    private void showAnswerView(int layoutId, int questionState) {
        View view = View.inflate(this, layoutId, null);
        if (questionState == Message.REJECT_STATE_ID) {
            setConsultationView(view);
        }
        answerView.addView(view);
    }

    private void showDoneAnswerView(Comment comment, int stateId) {

        int questionCount = PreferencesManager.getInstance().getAnswersReadCount();
        String lastAnswerRead = PreferencesManager.getInstance().getLastAnswerReadDate();

        //Send Event to MixPanel
        try
        {
            JSONObject props = new JSONObject();
            //props.put("Number of Answers Read", questionCount);
            props.put("Answer Read Date", lastAnswerRead);

            Bundle fb_parameters = new Bundle();
            fb_parameters.putInt("Number of Answers Read", questionCount);
            fb_parameters.putString("Answer Read Date", lastAnswerRead);


            MixPanelHelper helper = new MixPanelHelper(QuestionActivity.this,props,fb_parameters);
            helper.sendEvent(MixPanelEventConstants.USER_RECEIVES_REPLY);
            helper.incrementProperty("Number of Answers Read");

            PreferencesManager.getInstance().setLastAnswerReadDate();
            PreferencesManager.getInstance().incrementAnswersReadCount();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        if (comment != null) {
            View view = View.inflate(this, R.layout.view_answer_succesfull, null);
            TextView answerTextView = (TextView) view.findViewById(R.id.answer_text_view);
            TextView dateTextView = (TextView) view.findViewById(R.id.date_text_view);

            if (comment.getDate() != 0) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(MessageView.ASK_DATE_PATTERN,
                        Locale.getDefault());
                dateTextView.setText(dateFormat.format(new Date(comment.getDate())));
            }

            String answer = comment.getText();
            if (!TextUtils.isEmpty(answer)) {
                if (stateId == Message.SUGGEST_APPOINTMENT_STATE_ID) {
                    showSuggestAppointmentView(answer, answerTextView);
                } else {
                    answerTextView.setText(answer);
                }
            }

            setConsultationView(view);
            answerView.addView(view);
        }
    }

    private void setConsultationView(View consultationView) {
        TextView consultationTextView = (TextView) consultationView.findViewById(R.id.consult_text_view);
        ImageButton doneImageButton = (ImageButton) consultationView.findViewById(R.id.done_button);

        consultationTextView.setOnClickListener(bookConsultationListener);
        doneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showSuggestAppointmentView(String answer, TextView answerTextView) {
        answer = answer.replaceAll("<a>", "<u>");
        answer = answer.replaceAll("</a>", "</u>");

        answerTextView.setText(Html.fromHtml(answer));
        answerTextView.setOnClickListener(bookConsultationListener);
    }

    private void sendSuggestConsultationBroadcast() {
        NotifAskConsultant parseData = new NotifAskConsultant();
        parseData.setAction(PushBroadcastReceiver.PushAction.SUGGEST_CONSULTATION_AFTER_ARCHIVING.toString());
        Intent homePageIntent = new Intent(QuestionActivity.this,HomePageBackboneActivity.class);
        homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
        homePageIntent.putExtra("PAGE2", BackbonePageConstants.BOOK_APPOINTMENT);
        startActivity(homePageIntent);

     /*   Intent intent = new Intent(this, HomePageActivity.class);
        intent.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, GsonWrapper.getGson().toJson(parseData));
        startActivity(intent);*/
    }

    private final LoaderManager.LoaderCallbacks<Cursor> questionLoader =
            new LoaderManager.LoaderCallbacks<Cursor>() {
                @Override
                public Loader<Cursor> onCreateLoader(int id, Bundle arguments) {
                    final String questionId = arguments.getString(Keys.QUESTION_ID);

                    if (!TextUtils.isEmpty(questionId)) {
                        final String selectionArgs[] = new String[] {
                                questionId
                        };
                        return new CursorLoader(QuestionActivity.this,
                                MessageSchema.CONTENT_URI_QUESTIONS_WITH_FILES_URI,
                                MessageSchema.QueryWithFiles.PROJECTION,
                                MessageSchema.Query.QUESTION_SELECTION,
                                selectionArgs, null);
                    }

                    return null;
                }

                @Override
                public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                    if (cursor.moveToFirst()) {
                        Message message = Message.fromCursorWithFiles(cursor);
                        MessageView messageView = (MessageView) findViewById(R.id.message_view);
                        messageView.setMessage(message, audioArray);
                        showAnswer(message);
                    } else {
                        finish();
                    }
                }

                @Override
                public void onLoaderReset(Loader<Cursor> loader) {

                }
            };
}
