//
//  CKBundleDefs.h
//  Comics
//
//  Created by Derek Stutsman on 7/8/14.
//  Copyright (c) 2014 iVerse Media. All rights reserved.
//

//Identifiers for the components of the CKBundle format

#define MAXIMUM_BUNDLE_VERSION 5

#define HEADER @"IVERSE"
#define ICON 100							// PNG Data The Icon Data (PNG)
#define THUMB 101							// PNG Data The Thumb Data (PNG)
#define HORIZONTAL_IMAGE_UNENCRYPTED 110	// PNG Data The Horizontal Panel Data (PNG) In Unencrypted Form
#define HORIZONTAL_IMAGE_ENCRYPTED 111		// PNG Data The Horizontal Image Data (PNG) Encrypted ("Panel")
#define VERTICAL_IMAGE_UNENCRYPTED 112		// PNG Data The Verticaal Panel Data (PNG) In Unencrypted Form
#define VERTICAL_IMAGE_ENCRYPTED 113		// PNG Data The Vertical Image Data (PNG) Encrypted ("Panel")
#define COMIC_IMAGE_SMALL 114				// PNG Data Small Image for App (PNG) ("icon")
#define COMIC_IMAGE_MEDIUM 115				// PNG Data Medium Image for App (PNG) ("thumbnail")
#define COMIC_IMAGE_LARGE 116				// PNG Data Large Image for App (PNG) ("cover")
#define COVER 117							// PNG Data Large Image for App (PNG) ("cover")
#define SIDE_PANEL_IMAGE 118				// PNG Data Image for sidebar on iPad (can be specified multiple times, expected case 2)
#define COMIC_AD_HORIZONTAL 120				// String horizontal comic ad URL
#define COMIC_AD_VERTICAL 121				// String vertical comic ad URL
#define PANEL_KEY_BAD1 200					// (Obsolete) Panel Key No Longer Used (Dead ID)
#define GENERATED_DATE 201					// String The Date that the Comic Bundle was Generated (in format "yyyy-MM-dd")
#define GENERATED_BY 202					// String The User / Tool that Generated This Comic Bundle
#define OWNER_EMAIL_ENCRYPTED 203			// Iverse Encrypted String The Email Address of the Owner of this Comic Bundle, encrypted with AES using the Iverse Key
#define OWNER_EMAIL 204						// String The Email Address of the Owner of this Comic Bundle (in free form text form)
#define PANEL_KEY_BAD2 205					// (Obsolete) Panel Key No Longer Used (Dead ID)
#define PANEL_KEY 206						// Panel Key The AES Key used to encrypt the panels (itself encrypted with a user cipher)
#define COMIC_ID 1							// ID String (UTF-8) The ID of this Comic
#define NAME 3								// String (UTF-8)	 The Name of the Comic
#define WRITER 5							// String (UTF-8)	 A Writer of the Comic (can occur more than once)
#define	ARTIST 6							// String (UTF-8)	 An Artist for the Comic (can occur more than once)
#define PUBLISHER_NAME 7					// String (UTF-8)	 The Name of the Publisher
#define VIDEO_URL 8                         // String (UTF-8)	 The URL to the Publisher
#define PUBLISHER_DATE 9					// String (UTF-8)	 The Date of Publishing for this Comic
#define PANEL_COUNT_BAD 10					// (Obsolete) Short (Signed/16-bit) The count of panels in the comic (Dead ID)
#define COPYRIGHT 11						// String (UTF-8)	 The Copyright for the Comic
#define HORIZONTAL_COUNT 12					// HORIZONTAL_COUNT Short (Signed/16-bit) The count of panels in the comic
#define VERTICAL_COUNT 13					// VERTICAL_COUNT Short (Signed/16-bit) The count of panels in the comic
#define CONTENT_COUNT 14					// PANEL_COUNT Short (Signed/16-bit) The count of panels in the comic
#define SYNOPSIS 15							// String (UTF-8)	 The short 1 line synopsis of the comic
#define DESCRIPTION 16						// String (UTF-8)	 The longer, possible multi-paragraph description of the comic
#define CATEGORY 17							// String (UTF-8)	 The category for this comic (can occur more than once)
#define PUBLISHER_ID 18						// String (UTF-8)	 The canonical publisher ID for this comic (eg red.five.comics, antarctic.press)
#define PRODUCT_TYPE 19						// String (UTF-8)	 The type of product this is (eg One Shot, Mini Series, etc.)
#define SERIES_COMIC_ID 20					// String (UTF-8)	 The parent product ID for this comic
#define BUNDLE_CHECKSUM 299					// BUNDLE_CHECKSUM SHA1 Checksum The SHA1 Checksum for the Comic
#define MANGA_ORIENTATION 207               // Short (Signed/16-bit) Flag for manga
#define RESERVED_STRING_1 208               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_2 209               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_3 210               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_4 211               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_5 212               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_6 213               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_7 214               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_8 215               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_9 216               // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define RESERVED_STRING_10 217              // String (UTF-8)    Reserved for future expansion without requiring a format rev
#define HORIZONTAL_FADES 218                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_2 219                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_3 220                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_4 221                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_5 222                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_6 223                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_7 224                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_8 225                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_9 226                // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev
#define RESERVED_SHORT_10 227               // Short (Signed/16-bit) Reserved for future expansion without requiring a format rev