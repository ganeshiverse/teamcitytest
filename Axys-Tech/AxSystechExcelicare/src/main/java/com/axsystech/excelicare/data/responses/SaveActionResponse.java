package com.axsystech.excelicare.data.responses;

/**
 * Created by someswarreddy on 06/08/15.
 */
public class SaveActionResponse extends BaseResponse {

    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
