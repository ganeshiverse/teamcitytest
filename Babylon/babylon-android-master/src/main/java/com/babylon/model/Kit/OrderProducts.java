package com.babylon.model.Kit;


public class OrderProducts {



    private int id;


    public OrderProducts(int id){
        this.id = id;
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof OrderProducts){
            OrderProducts ptr = (OrderProducts) v;
            retVal = ptr.id == this.id;
        }


        return retVal;
    }



    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }






}