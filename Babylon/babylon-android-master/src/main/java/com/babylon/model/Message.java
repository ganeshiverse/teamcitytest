package com.babylon.model;

import android.database.Cursor;
import android.text.TextUtils;
import com.babylon.database.schema.MessageSchema;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Message extends ServerModel {

    @SkipFieldInContentValues
    public static final String AUDIO_MIMETYPE = "audio";
    @SkipFieldInContentValues
    public static final String PHOTO_MIMETYPE = "image";
    @SkipFieldInContentValues
    private static final transient String TAG = Message.class.getSimpleName();
    @SkipFieldInContentValues
    private static final long serialVersionUID = 979345683647343L;

    // 4 Awaiting - deprecated
    // 5 Close - deprecated
    @SkipFieldInContentValues
    public static final int OPEN_STATE_ID = 1;
    @SkipFieldInContentValues
    public static final int IN_PROGRESS_STATE_ID = 2;
    @SkipFieldInContentValues
    public static final int DONE_STATE_ID = 3;
    @SkipFieldInContentValues
    public static final int REJECT_STATE_ID = 6;
    @SkipFieldInContentValues
    public static final int SUGGEST_APPOINTMENT_STATE_ID = 7;

    private String id;
    private String text;
    private Integer stateId = OPEN_STATE_ID;
    private Integer consultantId;
    private Integer patientId;
    private Boolean isPendingDoctor;
    private Long date;
    private Long dateSent;
    @SkipFieldInContentValues
    private List<FileEntry> files = new ArrayList<FileEntry>();
    @SkipFieldInContentValues
    private List<Comment> comments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Message createNewMessage() {
        final Message message = new Message();

        message.setRandomUUID();
        message.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
        message.setDate(System.currentTimeMillis());

        final long dateSent = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        message.setDateSent(dateSent);

        return message;
    }

    @Override
    public String toString() {
        return "Message{"
                + "text='" + text + '\''
                + ", stateId=" + stateId
                + ", consultantId=" + consultantId
                + ", patientId=" + patientId
                + ", isPendingDoctor=" + isPendingDoctor
                + ", date='" + date + '\''
                + '}';
    }

    public boolean hasAudio() {
        return hasFile(true);
    }

    public boolean hasPhoto() {
        return hasFile(false);
    }

    private boolean hasFile(boolean isAudio) {
        boolean result = false;

        for (int i = 0; i < files.size(); i++) {
            FileEntry file = files.get(i);
            if (isAudio ? file.isAudio() : file.isPhoto()) {
                result = true;
                break;
            }
        }

        return result;
    }

    public static List<Message> fromCursorWithFilesList(Cursor cursor) {
        List<Message> messages = new ArrayList<Message>();
        while (cursor.moveToNext()) {
            final Message message = fromCursorWithFiles(cursor);
            messages.add(0, message);
        }
        return messages;
    }

    public static Message fromCursorWithFiles(Cursor cursor) {
        final Message message = new Message();
        message.createFromCursor(cursor);

        List<FileEntry> files = new ArrayList<FileEntry>();
        List<Comment> comments = new ArrayList<Comment>();
        do {
            Comment commentFromCursor = getCommentFromCursor(cursor);
            if (commentFromCursor != null && !isCommentEqual(commentFromCursor, comments)) {
                comments.add(commentFromCursor);
            }

            FileEntry fileFromCursor = getFileFromCursor(cursor);
            if (fileFromCursor != null) {
                files.add(fileFromCursor);
            }
        } while (moveToNextSameQuestion(message, cursor));

        message.setFiles(files);
        message.setComments(comments);
        return message;
    }

    private static boolean isCommentEqual(Comment newComment, List<Comment> comments) {
        return !comments.isEmpty()
                && comments.get(comments.size() - 1).getDate().equals(newComment.getDate())
                && comments.get(comments.size() - 1).getDate().equals(newComment.getDate());
    }

    private static boolean moveToNextSameQuestion(Message message, Cursor cursor) {
        boolean result = false;
        if (cursor.moveToNext()) {
            Message messageNext = new Message();
            messageNext.createFromCursor(cursor);
            if (message.getId().equals(messageNext.getId())) {
                result = true;
            } else {
                cursor.moveToPrevious();
            }
        }
        return result;
    }

    private static FileEntry getFileFromCursor(Cursor cursor) {
        FileEntry fileEntry = null;
        String id = cursor.getString(MessageSchema.QueryWithFiles.FILE_ID);
        String name = cursor.getString(MessageSchema.QueryWithFiles.FILE_NAME);
        Long date = cursor.getLong(MessageSchema.QueryWithFiles.FILE_DATE) * DateUtils.SECOND_IN_MILLS;
        String mimeType = cursor.getString(MessageSchema.QueryWithFiles.MIMETYPE);
        if (!TextUtils.isEmpty(id)) {
            fileEntry = new FileEntry();
            fileEntry.setId(id);
            fileEntry.setMimetype(mimeType);
            fileEntry.setName(name);
            fileEntry.setDate(date);
        }
        return fileEntry;
    }

    private static Comment getCommentFromCursor(Cursor cursor) {
        Comment comment = null;
        String text = cursor.getString(MessageSchema.QueryWithFiles.COMMENT_TEXT);
        Long date = cursor.getLong(MessageSchema.QueryWithFiles.COMMENT_DATE) * DateUtils.SECOND_IN_MILLS;
        if (text != null) {
            comment = new Comment();
            comment.setText(text);
            comment.setDate(date);
        }
        return comment;
    }

    public void addFile(FileEntry fileEntry, FileEntry.FileType fileType) {
        removeExistingFileType(fileType);
        files.add(fileEntry);
    }

    public void addFile(byte[] byteArray, FileEntry.FileType fileType) {
        removeExistingFileType(fileType);

        final FileEntry file = getFileEntry(byteArray, fileType);

        files.add(file);

        saveFileLocally(byteArray, file);
    }

    private FileEntry getFileEntry(byte[] fileByteArray, FileEntry.FileType fileType) {
        final FileEntry file = new FileEntry();

        file.setRandomUUID();
        file.setData(fileByteArray);
        file.setQuestionId(id);
        file.setDate(date);
        switch (fileType) {
            case AUDIO:
                file.setName("audio_" + file.getId() + ".m4a");
                file.setMimetype(AUDIO_MIMETYPE);
                break;
            case PHOTO:
                file.setName("photo_" + file.getId() + ".jpeg");
                file.setMimetype(PHOTO_MIMETYPE);
                break;
            default:
                break;
        }

        return file;
    }

    private void saveFileLocally(byte[] data, FileEntry fileEntry) {
        try {
            final File file = new File(fileEntry.getFilePath());
            if (!file.exists()) {
                file.createNewFile();
                final FileOutputStream out = new FileOutputStream(file);
                out.write(data);
                out.close();
            }
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }

    public void removeExistingFileType(FileEntry.FileType fileType) {
        for (int i = 0; i < files.size(); i++) {
            FileEntry file = files.get(i);
            String fileMimetype;
            switch (fileType) {
                case AUDIO:
                    fileMimetype = AUDIO_MIMETYPE;
                    break;
                case PHOTO:
                    fileMimetype = PHOTO_MIMETYPE;
                    break;
                default:
                    fileMimetype = null;
                    break;
            }
            if (!TextUtils.isEmpty(fileMimetype)
                    && file.getMimetype().equals(fileMimetype)) {

                final String filePath = file.getFilePath();
                deleteFileFromStorage(filePath);

                files.remove(i);
                break;
            }
        }
    }

    private void deleteFileFromStorage(String filePath) {
        final File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }

    public Comment getComment() {
        if (comments != null && !comments.isEmpty()) {
            return comments.get(0);
        }
        return null;
    }

    public void setRandomUUID() {
        id = getRandomUUIDStr();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Integer consultantId) {
        this.consultantId = consultantId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Boolean getIsPendingDoctor() {
        return isPendingDoctor;
    }

    public void setIsPendingDoctor(Boolean isPendingDoctor) {
        this.isPendingDoctor = isPendingDoctor;
    }

    public Long getDate() {
        return getLongMills(date);
    }

    public void setDate(Long date) {
        this.date = getLongSec(date);
    }

    public void setDateSent(Long date) {
        this.dateSent = date;
    }

    public List<FileEntry> getFiles() {
        return files;
    }

    public void setFiles(List<FileEntry> files) {
        this.files = files;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void deleteAllFiles() {
        for (final FileEntry entry : files) {
            final String filePath = entry.getFilePath();

            deleteFileFromStorage(filePath);
        }


    }

    public Long getSendDate() {
        return dateSent;
    }

    public static class Question {
        private Message question;

        public Message getQuestion() {
            return question;
        }

    }
}


