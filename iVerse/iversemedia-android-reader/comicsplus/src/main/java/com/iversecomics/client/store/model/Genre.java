package com.iversecomics.client.store.model;

public class Genre {
    private long dbId = -1;

    private String genreId;
    private String legacyKey;
    private String name;
    private String bannerFilename;

    public String getBannerFilename() {
        return bannerFilename;
    }

    public long getDbId() {
        return dbId;
    }

    public String getGenreId() {
        return genreId;
    }

    public String getLegacyKey() {
        return legacyKey;
    }

    public String getName() {
        return name;
    }

    public void setBannerFilename(String bannerFilename) {
        this.bannerFilename = bannerFilename;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public void setLegacyKey(String legacyKey) {
        this.legacyKey = legacyKey;
    }

    public void setName(String name) {
        this.name = name;
    }
}
