package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.client.store.model.Promotion;

public class PromotionTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "promotions");

    public static final Uri CONTENT_URI_BY_PROMOTIONID = Uri.withAppendedPath(CONTENT_URI, "byid");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of genres.
     */
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/vnd.iversecomics.store.promotions";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single genre.
     */
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/vnd.iversecomics.store.promotions";

    public static final String DEFAULT_SORT_ORDER = "name ASC";
    public static final String TABLE = "promotions";

    public static final String PROMOTIONID = "promotionId";
    public static final String NAME = "name";
    public static final String IMAGEURL = "imageurl";
    public static final String IMAGEURLPHONE = "imageurlphone";
    public static final String TARGETURL = "targeturl";
    public static final String ACTIVE = "active";

    public static final String[] FULL_PROJECTION = new String[]{
            _ID,
            PROMOTIONID,
            NAME,
            IMAGEURL,
            IMAGEURLPHONE,
            TARGETURL,
            ACTIVE
    };

    public static ContentValues asValues(Promotion promotion) {

        ContentValues values = new ContentValues();
        if (promotion.getPromotionID() >= 0) {
            values.put(_ID, promotion.getPromotionID());
        }
        values.put(PROMOTIONID, promotion.getPromotionID());
        values.put(NAME, promotion.getName());
        values.put(IMAGEURL, promotion.getImageURLString());
        values.put(IMAGEURLPHONE, promotion.getImageURLPhoneString());
        values.put(TARGETURL, promotion.getTargetURLString());
        values.put(ACTIVE, promotion.isActive() ? 1 : 0);

        return values;
    }

    public static Promotion fromCursor(Cursor c) {

        Promotion promotion = new Promotion();

        promotion.setPromotionID(getInteger(c, PROMOTIONID));
        promotion.setName(getString(c, NAME));
        promotion.setImageURLString(getString(c, IMAGEURL));
        promotion.setImageURLPhoneString(getString(c, IMAGEURLPHONE));
        promotion.setTargetURLString(getString(c, TARGETURL));
        promotion.setActive(getInteger(c, ACTIVE) == 1);

        return promotion;
    }
}
