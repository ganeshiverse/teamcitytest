package com.axsystech.excelicare.loaders;

import android.os.AsyncTask;

import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.network.ServerMethods;

/**
 * Created by someswarreddy on 14/08/15.
 */
public class UrlCardAsyncTask extends AsyncTask<Void, Void, GetDashBoardCardsResponse> {

    private String selectedMenuDashboardId;
    private String includeDesign;
    private String outputType;

    private LoadUrlCardListener loadUrlCardListener;

    public UrlCardAsyncTask(String selectedMenuDashboardId, String includeDesign, String outputType, LoadUrlCardListener loadUrlCardListener) {
        this.selectedMenuDashboardId = selectedMenuDashboardId;
        this.includeDesign = includeDesign;
        this.outputType = outputType;
        this.loadUrlCardListener = loadUrlCardListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected GetDashBoardCardsResponse doInBackground(Void... params) {
        try {
            return ServerMethods.getInstance().getDashboardCards(GlobalDataModel.getInstance().getActiveUser() != null ? GlobalDataModel.getInstance().getActiveUser().getToken() : "",
                    selectedMenuDashboardId, includeDesign);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(GetDashBoardCardsResponse dashBoardCardsResponse) {
        super.onPostExecute(dashBoardCardsResponse);

        if (dashBoardCardsResponse != null) {
            if (dashBoardCardsResponse.getListObject() != null && dashBoardCardsResponse.getListObject().size() > 0) {
                if (loadUrlCardListener != null) {
                    loadUrlCardListener.onUrlCardLoadSuccess(dashBoardCardsResponse, outputType);
                }
            } else {
                if (loadUrlCardListener != null) {
                    loadUrlCardListener.onUrlCardLoadFailure("Unable to load url card, please try again");
                }
            }
        } else {
            if (loadUrlCardListener != null) {
                loadUrlCardListener.onUrlCardLoadFailure("Unable to load url card, please try again");
            }
        }
    }

    public interface LoadUrlCardListener {
        public void onUrlCardLoadSuccess(GetDashBoardCardsResponse dashBoardCardsResponse, String outputType);

        public void onUrlCardLoadFailure(String errorMessage);
    }

}
