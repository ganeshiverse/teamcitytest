package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/3/2015.
 */
public class ChangeMemberRoleLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String circleManagerRole;
    private String userCircleId;
    private String status;
    private String userCircleMemberId;


    public ChangeMemberRoleLoader(Context context,String circleManagerRole, String token,  String userCircleId, String userCircleMemberId, String status) {
        super(context);
        this.token = token;
        this.circleManagerRole = circleManagerRole;
        this.userCircleId = userCircleId;
        this.userCircleMemberId = userCircleMemberId;
        this.status = status;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMemberChangeRole(circleManagerRole, token, userCircleId, userCircleMemberId, status);
    }
}
