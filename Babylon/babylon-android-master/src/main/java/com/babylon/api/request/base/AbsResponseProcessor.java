package com.babylon.api.request.base;

import com.babylon.utils.L;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.lang.reflect.Type;

public abstract class AbsResponseProcessor<O> {
    private final Type type;
    private Gson gson;

    public abstract Response<O> getResponse(HttpResponse httpResponse);

    public AbsResponseProcessor(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public String getStringResponse(HttpResponse response) {
        String result = null;
        try {
            HttpEntity entity = response.getEntity();
            if(entity!=null) {
                result = EntityUtils.toString(entity);
            }
        } catch (IOException e) {
            L.e(AbsRequest.TAG, e.toString(), e);
        }
        L.d(AbsRequest.TAG, result);
        return result;
    }

    public Gson getGson() {
        AbsRequest.failIfTrue(gson == null, "Set gson wrapper for responseProcessor"
                        + " in proccesResponse(), if you override this method",
                getClass().getSimpleName()
        );
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
