package com.babylon.test;

import android.os.Environment;

import com.babylon.model.Comment;
import com.babylon.utils.L;
import com.babylon.utils.SchemeCreator;

import junit.framework.TestCase;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SchemeCreatorRun extends TestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testMakeScheme() {
        final Class<Comment> clazz = Comment.class;
        final String schema = SchemeCreator.create(clazz);
        System.out.println(schema);
        logToFile(schema, clazz.getSimpleName());
    }

    protected void logToFile(String string, String fileName) {
        File log = new File(Environment.getExternalStorageDirectory() + "/" + fileName + ".txt");
        try {
            FileWriter fileWriter = new FileWriter(log, true);
            fileWriter.append(string);
            fileWriter.close();
        } catch (IOException e) {
            L.e("SCEME CREATOR RUN", e.toString());
        }
    }
}
