package com.babylon.api.request.family;

import android.text.TextUtils;

import com.babylon.api.request.RubyResponseProcessor;
import com.babylon.api.request.base.AbsRequestPatch;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Account;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.http.message.BasicHeader;

public class AccountPatch extends AbsRequestPatch<Account, Account> {

    private String entryId;

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    public AbsRequestPatch<Account, Account> withEntry(Account entry) {
        entryId = entry.getId();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("account", getGson().toJsonTree(entry));
        jsonObject.getAsJsonObject("account").remove("minor");
        jsonBody = jsonObject.toString();
        return this;
    }

    public AbsRequestPatch<Account, Account> withEntry(Account entry, String[] fieldsToPatch) {
        entryId = entry.getId();
        JsonObject jsonObject = new JsonObject();
        JsonElement accountElement = getGson().toJsonTree(entry);
        JsonObject accountObject = accountElement.getAsJsonObject();
        JsonObject filteredObject = new JsonObject();
        //Copy properties from the 'full' patient object that match the field arguments into the 'filtered' patient object
        for(String fieldName : fieldsToPatch) {
            filteredObject.add(fieldName, accountObject.get(fieldName));
        }
        jsonObject.add("account", filteredObject);
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        String url = Urls.getRuby(Urls.ACCOUNTS);
        return TextUtils.concat(url, "/", entryId).toString();
    }

    @Override
    protected AbsResponseProcessor<Account> getResponseProcessor() {
        return new RubyResponseProcessor<Account>(getOutputType());
    }
}
