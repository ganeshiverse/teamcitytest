package com.babylon.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.base.Urls;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.SecureJsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RatingScreenActivity extends BaseActivity
{
    private Button mSaveButton;
    private String mUri;
    private RequestQueue mQueue;
    private JsonObjectRequest mJsonReq;
    private EditText mUserComment;
    private Bundle mBundle;
    private RatingBar mRatingBar;
    private int mRating;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_screen);

        getActionBar().show();
        setupActionBar(ActionBarType.RATING_SCREEN);

        //get all views
        getViews();

        //Setup controllers
        setupViewControllers();

        //get Intent from caller
        mBundle = getIntent().getExtras();
    }

    public void getViews()
    {
        mSaveButton = (Button) findViewById(R.id.btnSendRating);
        mUserComment = (EditText) findViewById(R.id.edit_text_user_comment);
        mRatingBar = (RatingBar) findViewById(R.id.rating_bar);
    }

    public void setupViewControllers()
    {
        mSaveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                sendUserFeedback();
            }
        });

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()
        {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
            {
                mRating = (int) rating;
            }
        });
    }

    public void sendUserFeedback()
    {
        //Send Event to MixPanel
        try
        {
            JSONObject props = new JSONObject();
            props.put("App rating",mRating);

            Bundle fb_parameters = new Bundle();
            fb_parameters.putInt("App Rating", mRating);



            MixPanelHelper helper = new MixPanelHelper(RatingScreenActivity.this,props,fb_parameters);
            helper.sendEvent(MixPanelEventConstants.APP_RATING);




            PreferencesManager.getInstance().setLastAnswerReadDate();
            PreferencesManager.getInstance().incrementAnswersReadCount();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        mUri = getUrl();

        mQueue = App.getInstance().getRequestQueue();

        mJsonReq = new SecureJsonObjectRequest(Request.Method.POST, mUri,RatingScreenActivity.this,getJSONObject(), new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                Toast.makeText(RatingScreenActivity.this, "Thank you for your feedback", Toast.LENGTH_SHORT).show();
                RatingScreenActivity.this.finish();
            }
        },
        new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                error.printStackTrace();
            }
        });
        mQueue.add(mJsonReq);
    }

    protected String getUrl()
    {
        String url = Urls.getRuby(Urls.RATE_APPOINTMENT);
        if(mBundle.containsKey("ITEM_ID")) {
            url = String.format(url, mBundle.getString("ITEM_ID"));
        }
        return url;
    }

    public JSONObject getJSONObject()
    {
        JSONObject apiJsonObject = new JSONObject();
        try
        {
            apiJsonObject.put("comment", mUserComment.getText().toString().trim());
            apiJsonObject.put("rating",mRating);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return apiJsonObject;
    }
}
