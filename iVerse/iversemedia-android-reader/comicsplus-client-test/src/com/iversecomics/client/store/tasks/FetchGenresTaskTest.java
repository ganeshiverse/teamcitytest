package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ContentChangeTracker;
import com.iversecomics.client.store.db.GenresTable;

public class FetchGenresTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testFetchGenres() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(GenresTable.CONTENT_URI, this);

        ComicStore comicStore = getComicStore();
        FetchGenresTask task = new FetchGenresTask(comicStore);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, GenresTable.CONTENT_URI);
        OurAsserts.assertGreaterThan("Genres table rows", 5, actualCount);
    }
}
