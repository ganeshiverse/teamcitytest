package com.iversecomics.client;

public final class Preferences {
    public static final String NAME            = "IverseClient";
    public static final String KEY_OWNER_EMAIL = "iverse.owner.email";
    //public static final String DEFAULT_EMAIL   = "bogus@iversecomics.com";
    public static final String DEFAULT_EMAIL   = "";

    public static final String KEY_OWNER_PASSWORD = "iverse.owner.password";
    public static final String DEFAULT_PASSWORD = "";
    
    private Preferences() {
        /* prevent instantiation */
    }
}
