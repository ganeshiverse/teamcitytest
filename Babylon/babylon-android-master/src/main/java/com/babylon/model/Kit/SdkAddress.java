package com.babylon.model.Kit;

public class SdkAddress {
    public final String Address1;
    public final String Town;
    public final String County;
    public final String Postcode;
    public final String PremiseData;
    public final int ErrorNumber;
    public final String ErrorMessage;


    public SdkAddress(String Address1, String Town, String County, String Postcode, String PremiseData, int ErrorNumber,
                      String ErrorMessage) {
        this.Address1 = Address1;
        this.Town = Town;
        this.County = County;
        this.Postcode=Postcode;
        this.PremiseData=PremiseData;
        this.ErrorNumber=ErrorNumber;
        this.ErrorMessage=ErrorMessage;
    }
}
