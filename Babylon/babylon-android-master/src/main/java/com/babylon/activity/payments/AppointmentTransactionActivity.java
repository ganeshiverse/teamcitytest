package com.babylon.activity.payments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.api.request.AppointmentGet;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.PlansGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.family.FamilyAccountListGet;
import com.babylon.model.Account;
import com.babylon.model.Patient;
import com.babylon.model.payments.Appointment;
import com.babylon.model.payments.OrderDetail;
import com.babylon.model.payments.Plan;
import com.babylon.model.payments.Subscription;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class AppointmentTransactionActivity extends BaseActivity {

    private Plan[] plans;
    private Appointment appointment;
    private Patient patient;

    public static void startActivity(Activity activity, OrderDetail orderDetail, Appointment appointment) {
        Intent intent = new Intent(activity, AppointmentTransactionActivity.class);
        intent.putExtra(Keys.KEY_ORDER_DETAILS, orderDetail);
        intent.putExtra(Keys.APPOINTMENT_KEY, appointment);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appointment = (Appointment) getIntent().getSerializableExtra(Keys.APPOINTMENT_KEY);

        findViewById(R.id.base_layout).setBackgroundResource(R.drawable.monitor_me_background);

        if (appointment.getIsPaid()) {
            OrderDetail orderDetail = getIntent().getParcelableExtra(Keys.KEY_ORDER_DETAILS);
                boolean isSpecialist=false;
            if(appointment.getIsSpecialist())
            {


                isSpecialist=true;
            }
            Patient tempPatient = new Patient();
            tempPatient.setId(appointment.getPatientId());
            ConsultationTransactionActivity.startActivity(AppointmentTransactionActivity.this, orderDetail,
                    appointment, tempPatient,isSpecialist);

        }

        else {

            new PatientGet().execute(this, onPatientRequestListener);

          patient = App.getInstance().getPatient();
            /*if (appointment.getPatientId() != null && (!patient.getId().equals(appointment.getPatientId()))) {
                showProgressBar();
                new FamilyAccountListGet().execute(this, familyAccountsRequestListener);
            } else {
                startPaymentProcess();
            }*/
        }
    }

    private final RequestListener<Patient> onPatientRequestListener = new RequestListener<Patient>() {
        @Override
        public void onRequestComplete(Response<Patient> response) {
            if(response.isSuccess()) {
                Patient patient = response.getData();
                if (patient != null) {
                    App.getInstance().setPatient(patient);
                    if (appointment.getPatientId() != null && (!patient.getId().equals(appointment.getPatientId()))) {
                        showProgressBar();
                        new FamilyAccountListGet().execute(AppointmentTransactionActivity.this,
                                familyAccountsRequestListener);
                    } else {
                        if(patient.getPromotion()==null) {
                            startPaymentProcess();
                        }
                        else
                        {
                            OrderDetail orderDetails = getIntent().getParcelableExtra(Keys.KEY_ORDER_DETAILS);
                            ConsultationTransactionActivity.startActivity(AppointmentTransactionActivity.this,
                                    orderDetails,
                                    appointment,
                                    patient,false);
                            finish();
                        }
                    }
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SelectSubscriptionTransactionActivity.NEW_SUBSCRIPTION_SELECTED_RESULT) {
        subscriptionConfirmed();
            // finish();
           /* startFragment(new AppointmentConfirmationFragment(), false,
                    AppointmentConfirmationFragment.TAG);*/
        } else if(resultCode == RESULT_CANCELED && requestCode == SelectSubscriptionTransactionActivity.NEW_SUBSCRIPTION_SELECTED_RESULT) {
            finish();
        }
    }

    private void startPaymentProcess() {
        if (appointment.getIsSpecialist()) {
            OrderDetail orderDetail = getIntent().getParcelableExtra(Keys.KEY_ORDER_DETAILS);
            ConsultationTransactionActivity.startActivity(AppointmentTransactionActivity.this,
                    orderDetail, appointment, patient,true);
            finish();
        } else {
            //Fetch plans
            new PlansGet().execute(this, plansRequestListener);
        }
    }

    private final RequestListener<Plan[]> plansRequestListener = new RequestListener<Plan[]>() {
        @Override
        public void onRequestComplete(Response<Plan[]> response) {
            plans = response.getData();
            if (response.isSuccess()) {
                if (plans.length == 1) { // If there is 1 plan, assume £0 per month
                    Subscription subscription = patient.getSubscription();
                    if (!subscription.isPending()) {
                        subscriptionConfirmed();
                    } else {
                        OrderDetail orderDetails = getIntent().getParcelableExtra(Keys.KEY_ORDER_DETAILS);
                        ConsultationTransactionActivity.startActivity(AppointmentTransactionActivity.this,
                                orderDetails,
                                appointment,
                                patient,false);
                        finish();
                    }
                }
                else if(patient.getPromotion()!=null || (appointment!=null && appointment.getIsPromotion()))
                {

                    OrderDetail orderDetails = getIntent().getParcelableExtra(Keys.KEY_ORDER_DETAILS);
                    ConsultationTransactionActivity.startActivity(AppointmentTransactionActivity.this,
                            orderDetails,
                            appointment,
                            patient,false);
                    finish();

                }
                else { // Pay monthly
                    SelectSubscriptionTransactionActivity.startActivityForResult(
                            AppointmentTransactionActivity.this,
                            appointment,
                            patient,
                            SelectSubscriptionTransactionActivity.NEW_SUBSCRIPTION_SELECTED_RESULT);
                }
            } else {
                AlertDialogHelper.getInstance().showMessage(AppointmentTransactionActivity.this, response.getUserResponseError());
            }
        }
    };

    private void subscriptionConfirmed() {
        showProgressBar();
        new AppointmentGet(String.valueOf(appointment.getId())).execute(this, onAppointmentRequestListener);
    }

    private final RequestListenerErrorProcessed<Appointment> onAppointmentRequestListener = new
            RequestListenerErrorProcessed<Appointment>() {
                @Override
                public void onRequestComplete(Response<Appointment> response, String errorMessage) {
                    if (!AppointmentTransactionActivity.this.isFinishing()) {
                        if (response.isSuccess()) {
                            final Appointment appointment = response.getData();
                            final OrderDetail orderDetail = new OrderDetail(getString(R.string
                                    .appointment_order_details),
                                    appointment.getPrice());
                            ConsultationTransactionActivity.startActivity(AppointmentTransactionActivity.this,
                                    orderDetail, appointment, patient,false);
                            finish();
                        } else {
                            Toast.makeText(AppointmentTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

    private final RequestListenerErrorProcessed<Account[]> familyAccountsRequestListener = new
            RequestListenerErrorProcessed<Account[]>() {
                @Override
                public void onRequestComplete(Response<Account[]> response, String errorMessage) {
                    if (response.isSuccess()) {
                        Account[] familyAccounts = response.getData();
                        for (Account account : familyAccounts) {
                            if (account.getId().equals(appointment.getPatientId())) {
                                new PatientGet(account.getId()).execute(AppointmentTransactionActivity.this, familyPatientGetListener);
                                break;
                            }
                        }
                    } else {
                        AlertDialogHelper.getInstance().showMessage(AppointmentTransactionActivity.this, response.getUserResponseError());
                    }
                }
            };

    private final RequestListenerErrorProcessed<Patient> familyPatientGetListener = new
            RequestListenerErrorProcessed<Patient>() {
                @Override
                public void onRequestComplete(Response<Patient> response, String errorMessage) {
                    if (response.isSuccess()) {
                        patient = response.getData();
                        startPaymentProcess();
                    } else {
                        AlertDialogHelper.getInstance().showMessage(AppointmentTransactionActivity.this, response.getUserResponseError());
                    }
                }
            };
}
