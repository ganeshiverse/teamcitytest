package com.iversecomics.client.iab;

import android.app.Activity;
import android.content.Intent;

import com.iversecomics.client.store.model.Comic;

/**
 * Created by Gabriel Dogaru.
 */
public abstract class AbstractAppStoreAdapter {


    public static final int DIALOG_BILLING_CANNOT_CONNECT_ID = 1;
    public static final int DIALOG_BILLING_NOT_SUPPORTED_ID = 2;

    /**
     * Creates the Adapter and checks if the app billing is supported.
     *
     * @param savedInstanceState
     * @param activity
     */
    public abstract void onCreate(android.os.Bundle savedInstanceState, Activity activity);

    public abstract void onResume();

    public abstract void onPause();

    public abstract void onStart();

    public abstract void onStop();

    public abstract void onDestroy();

    /**
     * Initiates the purchase in the store.
     *
     * @param comic
     */
    public abstract void purchaseComic(Comic comic);

    public abstract void purchaseSubscription(final String subscriptionSku);

    public abstract boolean canHandle(int requestCode);

    public abstract void handleActivityResult(int requestCode, int resultCode, Intent intent);

    public abstract void restoreTransactions();
}
