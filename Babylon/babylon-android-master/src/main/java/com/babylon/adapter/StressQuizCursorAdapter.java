package com.babylon.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.database.schema.StressQuizSchema;
import com.babylon.model.StressQuestion;

public class StressQuizCursorAdapter extends CursorAdapter {
    private final StressQuestionState stressQuestionState;

    public StressQuizCursorAdapter(Context context, StressQuestionState stressQuestionState, Cursor c) {
        super(context, c, true);
        this.stressQuestionState = stressQuestionState;
    }

    @Override
    public long getItemId(int position) {
        long id = -1;
        final Cursor c = getCursor();

        if(c!=null && c.moveToPosition(position)) {
           id = c.getLong(StressQuizSchema.Query.ID);
        }

        return id;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return View.inflate(context, R.layout.list_item_stress_quiz, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        StressQuestion question = new StressQuestion();
        question.createFromCursor(cursor);


        final String questionText = question.getQuestion();
        final TextView questionTextView = (TextView) view.findViewById(R.id.text_view_question);
        questionTextView.setText(questionText);

        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.check_box);
        checkBox.setChecked(stressQuestionState.isChecked(question.getId()));
    }

    public interface StressQuestionState {
        boolean isChecked(int id);
    }
}


