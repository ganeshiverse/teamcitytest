package com.babylon.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.babylon.R;
import com.babylon.chart.OnLoadChartProgressListener;
import com.babylon.enums.MonitorMeParam;
import com.babylon.utils.Constants;
import com.babylon.view.ChartView;

public class BloodChartFragment extends BaseMonitorMeParamsFragment
        implements OnLoadChartProgressListener {
    private static final String ADD_PARAMETERS_FRAGMENT_TAG = "ADD_PARAMETERS_FRAGMENT_TAG";


    public static BloodChartFragment getInstance(long beginOfTheDayInMillis, String description,
                                                 boolean isMedicalKitExist) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        b.putString(Constants.DESCRIPTION, description);
        b.putBoolean(Constants.IS_KIT_BUTTON_EXIST, isMedicalKitExist);

        BloodChartFragment fragment = new BloodChartFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blood_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String description = getArguments().getString(Constants.DESCRIPTION);
        boolean isMedicalKitExist = getArguments().getBoolean(Constants.IS_KIT_BUTTON_EXIST);

        ChartView chartView = (ChartView) view.findViewById(R.id.chartView);
        chartView.setOnLoadChartProgressListener(this);
        chartView.setDateInMillis(dateInMillis);
        chartView.setFeatureId(MonitorMeParam.BLOOD_PRESSURE_ID.getId());
        chartView.setDescription(description);

        if (!isChartLoaded) {
            chartView.showChart();
        }
        isChartLoaded = true;

        setTitle(getString(R.string.blood_pressure_fragment_title));

        setRightImageButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content,
                        BloodPressureParametersFragment.getInstance(dateInMillis), ADD_PARAMETERS_FRAGMENT_TAG).addToBackStack(null)
                        .commit();
            }
        }, R.drawable.ic_menu_add);
        if (isMedicalKitExist && onMedicalKitClickListener != null) {
            setSecondRightImageButton(onMedicalKitClickListener.getOnKitClickListener(),
                    R.drawable.ic_menu_kit);
        }
    }

    @Override
    public void onStartLoadChartFromServer() {
        setProgressBarVisible(true);
    }

    @Override
    public void onFinishLoadChartFromServer() {
        setProgressBarVisible(false);
    }

    /* Listeners */
    @Override
    protected void onTopBarButtonPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

}