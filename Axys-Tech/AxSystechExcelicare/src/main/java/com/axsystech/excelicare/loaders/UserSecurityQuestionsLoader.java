package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.UserSecurityQuestionsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 04/09/15.
 */
public class UserSecurityQuestionsLoader extends DataAsyncTaskLibLoader<UserSecurityQuestionsResponse> {

    private String loginName;

    public UserSecurityQuestionsLoader(Context context, String loginName) {
        super(context);
        this.loginName = loginName;
    }

    @Override
    protected UserSecurityQuestionsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getUserSecurityQuestions(loginName);
    }
}
