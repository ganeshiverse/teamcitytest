package com.iversecomics.client;

import java.io.IOException;
import java.net.URI;

import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthListener;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.client.comic.AsyncMyComicSourceOpenTask;
import com.iversecomics.client.comic.IMyComicSourceAdapter;
import com.iversecomics.client.comic.AsyncMyComicSourceOpenTask.OnAsyncMyComicSourceOpenTaskListener;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.util.ConfigProj;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ShareManager {

//	public static ShareManager instance = null;

	public static final int METHOD_NONE = -1;
	public static final int METHOD_FACEBOOK = 0;
	public static final int METHOD_TWITTER = 1;
	
	SocialAuthAdapter socialAdapter;
	OnSocialPostListener socialListener;

	Activity context;
	Button sharebutton;
	Comic comic;

	public ShareManager() {
		initSocialAuth();
	}

//	public static ShareManager getInstance() {
//		if (instance == null) {
//			instance = new ShareManager();
//		}
//		return instance;
//	}

	public void initSocialAuth() {
		socialAdapter = new SocialAuthAdapter(new ResponseListener());

		// Add providers
		socialAdapter.addProvider(Provider.FACEBOOK, R.drawable.facebook);
		socialAdapter.addProvider(Provider.TWITTER, R.drawable.twitter);
	}

	public void configurePostComic(Activity context, Button sharebutton,
			Comic comic, OnSocialPostListener listener) {
		socialListener = listener;
		socialAdapter.enable(sharebutton);
		this.sharebutton = sharebutton;
		this.comic = comic;
		this.context = context;
	}
	
	public void shareComicVia(Activity ctx, int method, Comic comic, OnSocialPostListener listener)
	{
		this.comic = comic;
		this.context = ctx;
		socialListener = listener;
		
		if(socialAdapter != null)
		{
			if(method == METHOD_FACEBOOK)
			{
				socialAdapter.authorize(this.context, Provider.FACEBOOK);
			}
			else if(method == METHOD_TWITTER)
			{
				socialAdapter.authorize(this.context, Provider.TWITTER);
			}
		}
	}
	

	String message;
	Bitmap bitmap;
	URI producturi;

	AsyncPrepareResourceTask resourcePrepareTask = null;
	
	private boolean prepareResources() throws IOException {
		producturi = ServerConfig.getDefault().getWebProductUri(
				comic.getComicId());
		Uri imageuri = ServerConfig.getDefault().getLargeImageUri(
				comic.getImageFileName());
		String shareMessage = context.getResources().getString(
				R.string.share_message);
		this.message = shareMessage.replace("_1_", comic.getName()).replace("_2_",
				ConfigProj.SOCIAL_NETWORKING_APP_NAME);
		
		// load image
		WebBitmapLoader webLoader = new WebBitmapLoader(IverseApplication
				.getApplication().getHttpClient());
		bitmap = webLoader.loadBitmap(URI.create(imageuri.toString()));

		return true;
	}

	/**
	 * Listens Response from Library
	 * 
	 */
	private final class ResponseListener implements DialogListener {
		@Override
		public void onComplete(Bundle values) {
			Log.d("SocialAuthAdapter", "Authentication Successful");

			// Get name of provider after authentication
			final String providerName = values
					.getString(SocialAuthAdapter.PROVIDER);
			Log.d("SocialAuthAdapter", "Provider Name = " + providerName);

			
			if(providerName.equals(SocialAuthAdapter.Provider.FACEBOOK.toString()) || 
					providerName.equals(SocialAuthAdapter.Provider.TWITTER.toString()))
			{
				//launch
				if(resourcePrepareTask != null)
				{
					return;
				}
				resourcePrepareTask = new AsyncPrepareResourceTask(providerName);
				resourcePrepareTask.execute();
			}
		}

		@Override
		public void onError(SocialAuthError error) {
			Log.d("ShareButton", "Authentication Error: " + error.getMessage());
		}

		@Override
		public void onCancel() {
			Log.d("ShareButton", "Authentication Cancelled");
		}

		@Override
		public void onBack() {
			Log.d("Share-Button", "Dialog Closed by pressing Back Key");
		}

	}

	public void releaseResources() {
		this.sharebutton = null;
		this.context = null;
		this.comic = null;
		if (this.bitmap != null && this.bitmap.isMutable())
			this.bitmap.recycle();
		this.bitmap = null;
		this.message = null;
	}

	// To get status of message after authentication
	private final class MessageListener implements SocialAuthListener<Integer> {
		@Override
		public void onExecute(String arg0, Integer arg1) {
			Integer status = arg1;
			if (socialListener != null) {
				if (status.intValue() == 200 || status.intValue() == 201
						|| status.intValue() == 204) {
					socialListener.onPosted(true, null);
				} else {
					socialListener.onPosted(false, null);
				}
			}
			//releaseResources();
		}

		@Override
		public void onError(SocialAuthError e) {
			if (socialListener != null) {
				socialListener.onPosted(false, null);
			}
			//releaseResources();
		}
	}

	public interface OnSocialPostListener {
		abstract void onPosted(boolean success, String provider);
	}
	
	
	public class AsyncPrepareResourceTask extends AsyncTask<Void, Void, Boolean> {
		
	    public AsyncPrepareResourceTask(String method) {
	    	
	    }

	    @Override
	    protected Boolean doInBackground(Void... params) {
	        try {
	        	prepareResources();
	        } catch (IOException e) {
	        	//releaseResources();
	            return Boolean.FALSE;
	        }
	        return Boolean.TRUE;
	    }
	    
	    
	    protected void onPostExecute(Boolean result) {
	        if(result.booleanValue())
	        {
	        	//post
	        	try {
					socialAdapter.uploadImageAsync(message,
							ConfigProj.SOCIAL_NETWORKING_APP_NAME + ".jpg",
							bitmap, 80, new MessageListener());
				} catch (Exception e) {
					//releaseResources();
				}
	        	
	        	resourcePrepareTask = null;
	        }
	    }
	}
}
