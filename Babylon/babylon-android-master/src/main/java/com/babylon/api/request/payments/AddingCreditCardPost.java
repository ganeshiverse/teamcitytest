package com.babylon.api.request.payments;

import com.babylon.Config;
import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.database.PaymentCardOperationUtils;
import com.babylon.model.Wsse;
import com.babylon.model.payments.NewPaymentCard;
import com.babylon.model.payments.PaymentCard;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.braintreegateway.encryption.Braintree;
import com.braintreegateway.encryption.BraintreeEncryptionException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;

import java.util.HashMap;

public class AddingCreditCardPost extends AbsRequestPost<NewPaymentCard, PaymentCard> {

    @Override
    public AbsRequestPost<NewPaymentCard, PaymentCard> withEntry(NewPaymentCard entry) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("credit_card", getJSONForCreditCard(entry));
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.ADD_CREDIT_CARD_POST);
    }

    protected JsonElement getJSONForCreditCard(NewPaymentCard card) {
        Braintree braintree = new Braintree(Config.BRAINTREE_KEY);

        HashMap<String, String> encryptedValues = new HashMap<>();
        try {
            encryptedValues.put("number", braintree.encrypt(card.getCardNumber()));
            encryptedValues.put("cvv", braintree.encrypt(card.getCvv()));
            encryptedValues.put("expiration_date", braintree.encrypt(card.getExpiryDate()));

        } catch (BraintreeEncryptionException e) {
            L.e(TAG, e.getMessage(), e);
        }

        return getGson().toJsonTree(encryptedValues);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<PaymentCard> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }

    @Override
    protected Response<PaymentCard> processResponse(HttpResponse httpResponse) {
        Response<PaymentCard> result = super.processResponse(httpResponse);

        if(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            PaymentCardOperationUtils.getInstance().save(result.getData());
        }

        return result;
    }
}
