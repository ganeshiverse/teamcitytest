package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.GetRoleDataResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.data.responses.SendInviteToCircleMemberResponse;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.messages.CircleMemberSelectionListener;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.framework.ui.messages.MsgCircleMembersListFragment;
import com.axsystech.excelicare.framework.ui.messages.ProvidersSelectionListener;
import com.axsystech.excelicare.framework.ui.messages.SearchDoctorsFragment;
import com.axsystech.excelicare.loaders.CircleMemberDeleteLoader;
import com.axsystech.excelicare.loaders.GetRoleLoader;
import com.axsystech.excelicare.loaders.RemoveCircleLoader;
import com.axsystech.excelicare.loaders.SaveMyCircleLoader;
import com.axsystech.excelicare.loaders.SendInviteToCircleMemberLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class AddCircleMemberFragment extends BaseFragment implements View.OnClickListener, ProvidersSelectionListener, CircleMemberSelectionListener {

    private String TAG = AddCircleMemberFragment.class.getSimpleName();
    private AddCircleMemberFragment mFragment;
    private MainContentActivity mActivity;

    private static final String SEND_INVITE_TO_CIRCLE_MEMBER_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.AddCircleMemberFragment.SEND_INVITE_TO_CIRCLE_MEMBER_LOADER";
    private static final String GET_ROLE = "com.axsystech.excelicare.framework.ui.fragments.circles.AddCircleMemberFragment.GET_ROLE";

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.roleEditTextContainer)
    private FloatLabeledEditText roleEditTextContainer;

    @InjectView(R.id.roleEditText)
    private EditText roleEditText;

    @InjectView(R.id.sendInvitationButton)
    private Button mSendInvitationButton;

    @InjectView(R.id.firstNameEditTextContainer)
    private FloatLabeledEditText firstNameEditTextContainer;

    @InjectView(R.id.lastNameEditTextContainer)
    private FloatLabeledEditText lastNameEditTextContainer;

    @InjectView(R.id.emailIdEditTextContainer)
    private FloatLabeledEditText emailIdEditTextContainer;

    @InjectView(R.id.messageEditTextContainer)
    private FloatLabeledEditText messageEditTextContainer;

    @InjectView(R.id.inviteMembersImageView)
    private ImageView inviteMembersImageView;

    @InjectView(R.id.messageEditText)
    private EditText messageEditText;

    @InjectSavedState
    private String mCircleId;

    private ListMyCirclesResponse.CircleDetails mSelectedCircleObject;
    private String mInviteTarget = AxSysConstants.INVITE_CIRCLE_MEMBER;

    private String mRoleName;

    @Override
    protected void initActionBar() {
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, getString(R.string.title_invitation)));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_newcircle_member, container, false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        readIntentArgs();
        preloadData();
        setEventListeners();
    }

    private void setEventListeners() {

        // roleEditTextContainer.setEnabled(false);
        roleEditText.setEnabled(false);
        roleEditText.setFocusable(false);
        roleEditTextContainer.setFocusable(false);
        messageEditText.setText("Sending Invitation, Please Accept my invitation");
        roleEditText.setOnClickListener(this);
        roleEditTextContainer.setOnClickListener(this);
        firstNameEditTextContainer.setOnClickListener(this);
        lastNameEditTextContainer.setOnClickListener(this);
        emailIdEditTextContainer.setOnClickListener(this);
        messageEditTextContainer.setOnClickListener(this);
        mSendInvitationButton.setOnClickListener(this);

        inviteMembersImageView.setOnClickListener(this);

    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_CIRCLES_INVITE_TARGET)) {
                mInviteTarget = bundle.getString(AxSysConstants.EXTRA_CIRCLES_INVITE_TARGET);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CIRCLEID)) {
                mCircleId = bundle.getString(AxSysConstants.EXTRA_SELECTED_CIRCLEID);
            }
        }
    }

    private void preloadData() {
        //displayRolesDialog();
        if (mInviteTarget != null) {
            if (mInviteTarget.equalsIgnoreCase(AxSysConstants.INVITE_CIRCLE_DOCTOR)) {
                inviteMembersImageView.setImageResource(R.drawable.carecircles_doctor);
            } else if (mInviteTarget.equalsIgnoreCase(AxSysConstants.INVITE_CIRCLE_MEMBER)) {
                inviteMembersImageView.setImageResource(R.drawable.carecircles_user);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty_addcirclemember, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                for (int i = 0; i < activityFragmentManager.getBackStackEntryCount(); i++) {
                    if (activityFragmentManager.getBackStackEntryAt(i).getName().equals("AddCircleMemberFragment")) {
                        // add your code here
                        System.out.println("Name" + activityFragmentManager.getBackStackEntryAt(i).getName());
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getString(R.string.confirmation_to_cancel_invitation));
                        alert.setPositiveButton(getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mActivity.onBackPressed();
                            }
                        });
                        alert.show();
                    }
                }
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.roleEditTextContainer:
                displayRolesDialog();
//
//                if (CommonUtils.hasInternet()) {
//                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
//
//                    displayProgressLoader(false);
//                    GetRoleLoader getRoleLoader = new GetRoleLoader(mActivity, mActiveUser.getToken());
//                    if (mFragment.isAdded() && mFragment.getView() != null) {
//                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_ROLE), getRoleLoader);
//                    }
//                } else {
//                    showToast(R.string.error_no_network);
//                }
//
//                break;
            /*case R.id.roleEditText:
                displayRolesDialog();
                break;*/
            case R.id.firstNameEditTextContainer:
            case R.id.lastNameEditTextContainer:
            case R.id.emailIdEditTextContainer:
            case R.id.messageEditTextContainer:
                if (view instanceof FloatLabeledEditText) {
                    ((FloatLabeledEditText) view).getEditText().requestFocus();
                    CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                }
                break;
            case R.id.sendInvitationButton:

                if (validateUserInputs()) {
                    // Create SaveCircle request body
                    JSONObject dataObject = new JSONObject();

                    try {
                        dataObject.put("UserCircleMember_ID", "");
                        dataObject.put("Invite_Id", "");
                        dataObject.put("RecipientEmailAddress", emailIdEditTextContainer.getEditText().getText().toString().trim());
                        dataObject.put("RecipientForename", firstNameEditTextContainer.getEditText().getText().toString().trim());
                        dataObject.put("RecipientSurname", lastNameEditTextContainer.getEditText().getText().toString().trim());
                        dataObject.put("MessageText", messageEditTextContainer.getEditText().getText().toString().trim());
                        dataObject.put("Role_SLU", roleEditTextContainer.getEditText().getTag(R.id.selected_role_id) != null ?
                                (String) roleEditTextContainer.getEditText().getTag(R.id.selected_role_id) : "");
                        dataObject.put("UserCircle_ID", mCircleId);
                        dataObject.put("SenderUser_ID", "");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // POST saveCircle request
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);

                        SendInviteToCircleMemberLoader sendInviteToCircleMemberLoader = null;
                        try {
                            sendInviteToCircleMemberLoader = new SendInviteToCircleMemberLoader(mActivity,
                                    mActiveUser != null ? mActiveUser.getToken() : "",
                                    URLEncoder.encode(dataObject.toString(), "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SEND_INVITE_TO_CIRCLE_MEMBER_LOADER), sendInviteToCircleMemberLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                break;

            case R.id.inviteMembersImageView:
                if (mInviteTarget != null) {
                    if (mInviteTarget.equalsIgnoreCase(AxSysConstants.INVITE_CIRCLE_DOCTOR)) {
                        selectProvider();

                    } else if (mInviteTarget.equalsIgnoreCase(AxSysConstants.INVITE_CIRCLE_MEMBER)) {
                        loadListMembers();

                    }
                }
                break;
            default:
                break;
        }
    }

    private void loadListMembers() {
        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

        MsgCircleMembersListFragment circleMembersListFragment = new MsgCircleMembersListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_select_circle_member));
        bundle.putString(AxSysConstants.EXTRA_CIRCLE_MEMBER_TARGET, AddCircleMemberFragment.class.getSimpleName());
        circleMembersListFragment.setArguments(bundle);
        ft.add(R.id.container_layout, circleMembersListFragment, MsgCircleMembersListFragment.class.getSimpleName());
        ft.addToBackStack(MsgCircleMembersListFragment.class.getSimpleName());
        ft.commit();
    }

    private void selectProvider() {
        FragmentManager activityFragmentManager = mActivity.getSupportFragmentMngr();
        FragmentTransaction ft = activityFragmentManager.beginTransaction();

        SearchDoctorsFragment searchDoctorsFragment = new SearchDoctorsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_find_doctors));
        bundle.putString(AxSysConstants.EXTRA_PROVIDER_SOURCE, AxSysConstants.PROVIDER_SOURCE_COMPOSE);
        searchDoctorsFragment.setArguments(bundle);
        ft.add(R.id.container_layout, searchDoctorsFragment, SearchDoctorsFragment.class.getSimpleName());
        ft.addToBackStack(SearchDoctorsFragment.class.getSimpleName());
        ft.commit();
    }

    private void displayRolesDialog() {
        final CharSequence[] items = {
                getString(R.string.dialog_text_memner),
                getString(R.string.dialog_text_proxy)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.picker_title_select_role));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                roleEditTextContainer.getEditText().setText(items[item]);

                if (items[item].equals(getString(R.string.dialog_text_memner))) {
                    roleEditTextContainer.getEditText().setTag(R.id.selected_role_id, AxSysConstants.LOOKUP_CLINICAL_CARE_MANAGER);
                } else if (items[item].equals(getString(R.string.dialog_text_proxy))) {
                    roleEditTextContainer.getEditText().setTag(R.id.selected_role_id, AxSysConstants.LOOKUP_PROXY);
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(roleEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_role);
            roleEditTextContainer.performClick();
            return false;
        }
        if (TextUtils.isEmpty(firstNameEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_first_name);
            firstNameEditTextContainer.getEditText().requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(lastNameEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_last_name);
            lastNameEditTextContainer.getEditText().requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(emailIdEditTextContainer.getEditText().getText().toString().trim()) ||
                !CommonUtils.isValidEmail(emailIdEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_valid_email);
            emailIdEditTextContainer.getEditText().requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SEND_INVITE_TO_CIRCLE_MEMBER_LOADER)) {

        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SEND_INVITE_TO_CIRCLE_MEMBER_LOADER)) {
            if (result != null && result instanceof String) {
                String resultsString = (String) result;

                Gson gson = new GsonBuilder().create();
                final SendInviteToCircleMemberResponse sendInviteToCircleMemberResponse = gson.fromJson(resultsString, SendInviteToCircleMemberResponse.class);

                if (!TextUtils.isEmpty(sendInviteToCircleMemberResponse.getData())) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            if (!TextUtils.isEmpty(sendInviteToCircleMemberResponse.getMessage())) {
                                showToast(sendInviteToCircleMemberResponse.getMessage());
                            }
                            mActivity.onBackPressed();
                        }
                    });
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(sendInviteToCircleMemberResponse.getMessage());
                    builder.setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mActivity.onBackPressed();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_ROLE)) {
            if (result != null && result instanceof GetRoleDataResponse) {
                final GetRoleDataResponse getRoleDataResponse = (GetRoleDataResponse) result;

                if (getRoleDataResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(getRoleDataResponse.getMessage());
                            mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onProviderSelected(SearchDoctorsResponse.DoctorDetails selectedClinicianObject, ClinicianDetailsResponse.ClinicianDetailsData clinicianDetailsData) {
        Trace.d(TAG, "selectedClinicianObject:" + selectedClinicianObject);

        if (clinicianDetailsData != null) {
            emailIdEditTextContainer.getEditText().setText(!TextUtils.isEmpty(clinicianDetailsData.getEmail()) ? clinicianDetailsData.getEmail() : "");
        }
    }

    @Override
    public void onCircleMemberSelected(ListMembersInCircleResponse.CircleMemberDetails selectedCircleMemberObject) {
        Trace.d(TAG, "selectedCircleMemberObject:" + selectedCircleMemberObject);

        if (selectedCircleMemberObject != null) {
            emailIdEditTextContainer.getEditText().setText(!TextUtils.isEmpty(selectedCircleMemberObject.getUserCircleMemberEmail_ID()) ?
                    selectedCircleMemberObject.getUserCircleMemberEmail_ID() : "");
        }
    }
}
