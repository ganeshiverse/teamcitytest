package com.babylon.utils;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class DateUtils {
    private static DateUtils dateUtils;

    private static final String TAG = DateUtils.class.getSimpleName();
    public static final int SECOND_IN_MILLS = 1000;
    public static final int DAYS_IN_YEAR = 365;
    private static List<DateFormat> dateFormatArrayList = new ArrayList<DateFormat>();

    public static final String DATE_FORMAT = "dd MMM yyy";
    public static String HOUR_MINUTE1_FORMAT = "HH:mm";
    public static String HOUR_MINUTE2_FORMAT = "HH mm";
    public static String MONTH_YEAR1_FORMAT = "MM yyyy";
    public static String MONTH_YEAR2_FORMAT = "MMMM yyyy";
    public static final String DAY_MONTH_YEAR1_FORMAT = "dd-MM-yyyy";
    public static final String DAY_MONTH_YEAR2_FORMAT = "dd MM yyyy";
    public static final String DAY_MONTH_YEAR3_FORMAT = "ddMMyyyy";
    public static final String DAY_MONTH_YEAR4_FORMAT = "dd MMMM yyyy";
    public static final String DAY_MONTH_YEAR5_FORMAT = "dd.MM.yy";
    public static final String DAY_MONTH_YEAR6_FORMAT = "dd MMMM, yyyy";
    public static final String DAY_MONTH_YEAR_HOUR_MINUTE1_FORMAT = "dd-MM-yyyy HH:mm";
    public static final String DAY_MONTH_YEAR_HOUR_MINUTE2_FORMAT = "dd MM yyyy HH:mm";
    public static final String DAY_MONTH_YEAR_HOUR_MINUTE3_FORMAT = "ddMMyyyy HH:mm";
    public static final String DAY_MONTH_YEAR_HOUR_SECOND4_FORMAT = "dd MMMM, yyyy, HH:mm:ss";
    public static final String TWITTER_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss Z";
    public static final String YT_DATE_FORMAT_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String YT_DATE_FORMAT_ZZZ = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
    public static final String FULL_SIMPLE_DATE_FORMAT = "dd MMM yyy HH:mm";

    static {
        dateFormatArrayList.add(new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT, Locale.getDefault()));
        dateFormatArrayList.add(new SimpleDateFormat(Constants.SHORT_DATE_PATTERN, Locale.getDefault()));
        dateFormatArrayList.add(new SimpleDateFormat(Constants.FACEBOOK_DATE_PATTERN, Locale.getDefault()));
    }

    public static DateUtils getInstance() {
        if (dateUtils == null) {
            dateUtils = new DateUtils();
        }
        return dateUtils;
    }

    private DateUtils() {
        // singleton
    }

    /**
     * Method finds out saving hours, minute, second for feature date. The day, month, year is not changed.
     * Method helps to resolve issues for user who crosses different timezone. He might have a problems with
     * identifying activities between dates for changing timezones.
     *
     * @param newDate supposed date for saving feature
     * @return date for saving feature
     */
    public static Date findOutSavingDate(@Nullable Date newDate) {
        if (newDate == null) {
            newDate = new Date();
        }

        Calendar newDateCalendar = Calendar.getInstance();
        newDateCalendar.setTime(newDate);

        //system takes current time(only hour, minute, second) and sets the same time for the record
        // which user is adding
        Calendar currentDateCalendar = Calendar.getInstance();
        currentDateCalendar.setTime(new Date());
        newDateCalendar.set(Calendar.HOUR_OF_DAY, currentDateCalendar.get(Calendar.HOUR_OF_DAY));
        newDateCalendar.set(Calendar.MINUTE, currentDateCalendar.get(Calendar.MINUTE));
        newDateCalendar.set(Calendar.SECOND, currentDateCalendar.get(Calendar.SECOND));

        return newDateCalendar.getTime();
    }

    public static String getIso8601DateFormat(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        return df.format(new Date());
    }

    public static Date findOutSavingDate(long newDateInMillis) {
        return findOutSavingDate(new Date(newDateInMillis));
    }

    public SimpleDateFormat getDateFormat(String format, Locale locale) {
        return new SimpleDateFormat(format, locale);
    }

    public SimpleDateFormat getDateFormat(String format) {
        return new SimpleDateFormat(format, Locale.getDefault());
    }

    public static long getDateNowInSeconds() {
        long currentTimeInMillis = new Date().getTime();
        long currentTimeInSeconds = currentTimeInMillis / android.text.format
                .DateUtils.SECOND_IN_MILLIS;
        return currentTimeInSeconds;
    }

    public static Date millsToDate(String stringDate) {
        Date result = null;
        try {
            result = new Date(Long.parseLong(stringDate));
        } catch (NumberFormatException e) {
            L.e(TAG, e.toString(), e);
        }
        return result;
    }

    public static int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / (android.text.format.DateUtils.DAY_IN_MILLIS));
    }

    public static long millsToSeconds(Long mills) {
        return mills / SECOND_IN_MILLS;
    }

    public static Date millsToDate(String stringDate, String format) {
        Date result = null;
        try {
            result = new Date(Long.parseLong(stringDate));
        } catch (NumberFormatException e) {
            L.e(TAG, e.toString(), e);
        }
        return result;
    }

    public static Date stringToDate(String stringDate) {
        Date result = null;

        for (int i = 0; i < dateFormatArrayList.size(); i++) {
            DateFormat dateFormat = dateFormatArrayList.get(i);
            try {
                result = dateFormat.parse(stringDate);
                break;
            } catch (ParseException e) {
                L.d(TAG, e.getMessage());
            }
        }
        return result;
    }

    public static long getDayStartMills(int dayOffset) {
        return getDayStartMills(null, dayOffset);
    }

    public static long getDayStartSeconds(int dayOffset) {
        return getDayStartMills(null, 0) / android.text.format.DateUtils.SECOND_IN_MILLIS;
    }

    public static long getDayStartMills(Date date) {
        return getDayStartMills(date, 0);
    }

    public static long getDayStartMills(long date) {
        return getDayStartMills(date, 0);
    }

    public static long getDayStartMills(Date date, int dayOffset) {
        if (date == null) {
            date = new Date();
        }

        long startDate = getDayStartMills(date.getTime(), dayOffset);

        return startDate;
    }

    public static long getDayStartMills(long dateInMillis, int dayOffset) {
        Date date = new Date(dateInMillis);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.DAY_OF_YEAR, dayOffset);

        Date startDate = getStartDate(calendar.getTime());

        return startDate.getTime();
    }

    public static Date getCurrentDayDate() {
        Date result = getStartDate(null);
        return result;
    }

    public static Date getStartDate(long date) {
        Date startDate = getStartDate(new Date(date));
        return startDate;
    }

    public static Date getStartDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date == null ? new Date() : date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static String dateToString(Date date, SimpleDateFormat simpleDateFormat) {
        String dateString = "";
        if (date != null && simpleDateFormat != null) {
            dateString = simpleDateFormat.format(date);
        }
        return dateString;
    }

    public static long stringToLong(String dateString, SimpleDateFormat simpleDateFormat) {
        long time = 0;
        if (!TextUtils.isEmpty(dateString) && simpleDateFormat != null) {
            try {
                time = simpleDateFormat.parse(dateString).getTime();
            } catch (ParseException e) {
                L.e(TAG, "StringToLong: Parse error", e);
            }
        }
        return time;
    }

    public static String longToString(long dateLong) {
        return longToString(dateLong, DAY_MONTH_YEAR_HOUR_SECOND4_FORMAT);
    }

    public static String longToString(long dateLong, String format) {
        DateUtils dateUtils = DateUtils.getInstance();

        return dateUtils.getDateFormat(format, Locale.ENGLISH)
                .format(new Date(dateLong));
    }

    public static long twitterTimeToLong(String dateString) {
        try {
            return getInstance().getDateFormat(TWITTER_DATE_FORMAT, Locale.ENGLISH)
                    .parse(dateString).getTime();
        } catch (ParseException e) {
            L.e("twitterTimeToLong", "Parse error", e);
        }
        return 0;
    }

    public static long timeToLong(String dateString, String format) {
        try {
            return getInstance().getDateFormat(format).parse(dateString).getTime();
        } catch (ParseException e) {
            L.e("twitterTimeToLong", "Parse error", e);
        }
        return 0;
    }

    public static long dateToLong(int mYear, int mMonth, int mDay) {
        Date date = new Date(mYear - 1900, mMonth, mDay);
        return date.getTime();
    }

    public static long dateToLong(int mYear, int mMonth, int mDay, int mHours, int mMinute) {
        Date date = new Date(mYear - 1900, mMonth, mDay, mHours, mMinute);
        return date.getTime();
    }

    public static long dateToLong(long selectedLongDate, int mHours, int mMinute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(selectedLongDate);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), mHours, mMinute);
        return calendar.getTimeInMillis();
    }

    public static long dateToLong(long selectedLongDate, int mMinute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(selectedLongDate);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), mMinute);

        // Log.i(TAG, "Insert data:" + calendar.getTime().toString());
        return calendar.getTimeInMillis();
    }

    public static DatePicker setDatePickerDate(DatePicker datePicker, long longDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(longDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePicker.updateDate(year, month, day);
        return datePicker;
    }

    public static long dateToDay(long date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH), 0, 0);
        return cal.getTimeInMillis();
    }

    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + String.valueOf(c);
        }
    }

    public static long getYesterdayDay(long date) {
        return date - android.text.format.DateUtils.DAY_IN_MILLIS;
    }

    /**
     * @return seconds
     */
    public static long getTimestamp(Date date) {
        return date != null ? date.getTime() / android.text.format.DateUtils.SECOND_IN_MILLIS : 0;
    }

    /**
     * @param timestamp seconds
     * @return date
     */
    public static Date getDateFromTimestamp(long timestamp) {
        return new Date(timestamp * android.text.format.DateUtils.SECOND_IN_MILLIS);
    }

    public static long getEndOfTheDay(long dateInMillis) {
        return getEndOfTheDay(new Date(dateInMillis));
    }

    public static long getEndOfTheDay(Date date) {
        return getStartDate(date).getTime() + android.text.format.DateUtils.DAY_IN_MILLIS - 1;
    }

    public static long getLongFromHoursAndMinutes(int hours, int minutes) {
        return hours * android.text.format.DateUtils.HOUR_IN_MILLIS
                + minutes * android.text.format.DateUtils.MINUTE_IN_MILLIS;
    }

    public static int getHours(long timeInMillisecond) {
        return (int) (timeInMillisecond / android.text.format.DateUtils.HOUR_IN_MILLIS);
    }

    public static double getHoursInDouble(long timeInMillisecond) {
        return Math.floor(timeInMillisecond / (double) android.text.format.DateUtils.HOUR_IN_MILLIS);
    }

    public static int getMinutes(long timeInMillisecond) {
        int hours = (int) Math.floor((double) timeInMillisecond / (double) android.text.format.DateUtils
                .HOUR_IN_MILLIS);
        return (int) Math.floor((timeInMillisecond - android.text.format.DateUtils.HOUR_IN_MILLIS * hours)
                / (double) android.text.format.DateUtils.MINUTE_IN_MILLIS);
    }

    public static int getAllInMinutes(long timeInMillisecond) {
        return (int) Math.floor(timeInMillisecond / (double) android.text.format.DateUtils.MINUTE_IN_MILLIS);
    }

    public static int getFieldValue(Date date, int calendarField) {
        Calendar c = Calendar.getInstance();

        if (date == null) {
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
        } else {
            c.setTimeInMillis(date.getTime());
        }

        int value = c.get(calendarField);

        return value;
    }

    public static String getDefaultTimezoneId() {
        TimeZone tz = TimeZone.getDefault();
        return tz.getID();
    }
}
