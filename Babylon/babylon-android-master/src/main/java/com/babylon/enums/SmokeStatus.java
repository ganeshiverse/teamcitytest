package com.babylon.enums;

import com.babylon.App;
import com.babylon.R;
import com.babylon.model.MonitorMeFeatures;

public enum SmokeStatus {
    NON_SMOKER(0),
    EX_SMOKER(1),
    SMOKER(2);

    private int smokerValue;

    public static SmokeStatus getSmokerStatus(MonitorMeFeatures.Feature smokerFeature) {
        final int value = smokerFeature.getFloatValue().intValue();

        SmokeStatus smokerStatus;

        switch (value) {
            case 1:
                smokerStatus = EX_SMOKER;
                break;
            case 2:
                smokerStatus = SMOKER;
                break;
            default:
                smokerStatus = NON_SMOKER;
        }

        return smokerStatus;
    }

    SmokeStatus(int value) {
        smokerValue = value;
    }

    public String getSmokerText() {
        String[] smokerStatuses = App.getInstance().getResources().getStringArray(R.array.smoker_statuses);
        String result = smokerStatuses[smokerValue];

        return result;
    }
}
