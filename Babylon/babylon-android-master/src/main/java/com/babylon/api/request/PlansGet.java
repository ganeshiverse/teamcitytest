package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.payments.Plan;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import org.apache.http.message.BasicHeader;

public class PlansGet extends AbsRequestGet<Plan[]> {

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.PLANS_GET);
    }

    @Override
    protected AbsResponseProcessor<Plan[]> getResponseProcessor() {
        return new RubyResponseProcessor<Plan[]>(getOutputType());
    }
}
