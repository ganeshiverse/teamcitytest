package com.babylon.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.*;
import com.babylon.R;
import com.babylon.api.request.base.Response;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.L;
import com.babylon.view.TopBarInterface;


/**
 * BaseFragment collect base methods for all fragments
 */
public class BaseFragment extends Fragment implements TopBarInterface {
    private NetworkReceiver networkErrorReceiver;
    int mSectionActionBarBackgroundColor;
    private View mActionBarCustomView;
    private TextView mTitle;
    private ImageButton mActionBarBackArrow;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeTopBar();

        if(getActivity() != null && getActivity().getActionBar() != null)
        {
            getActivity().getActionBar().hide();
            initActionBar();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        networkErrorReceiver = new NetworkReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SyncAdapter.ACTION_NETWORK_ERROR);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(networkErrorReceiver, intentFilter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(networkErrorReceiver);
    }

    protected void onTopBarButtonPressed() {
        //please be attentive when you remove this IF
        if (isBackAllowed()) {
            getActivity().onBackPressed();
        }
    }

    public void initActionBar()
    {
        final ActionBar actionBar = getActivity().getActionBar();
        if (actionBar == null) return;

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.activity_action_bar);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        mActionBarCustomView = actionBar.getCustomView();
        mTitle = (TextView) mActionBarCustomView.findViewById(R.id.action_bar_title);
        mActionBarBackArrow = (ImageButton) getActivity().findViewById(R.id.action_bar_back_arrow);

        //Handling back pressed on arrow press
        mActionBarBackArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getActivity().onBackPressed();
            }
        });
    }


    public void setupActionBar(final ActionBarType actionBarType)
    {
        if (actionBarType == null) return;
        mSectionActionBarBackgroundColor =
                getResources().getColor(actionBarType.getBackgroundColourResId());
        mActionBarCustomView.setBackgroundColor(mSectionActionBarBackgroundColor);

        /*if (mActionBarLogo != null)
        {
            mActionBarLogo.setVisibility(
                    actionBarType == ActionBarType.HOME ? View.VISIBLE : View.GONE);
        }*/
        /*if (mActionBarBackArrow != null)
        {
            mActionBarBackArrow.setVisibility(
                    actionBarType == ActionBarType.HOME ? View.GONE : View.VISIBLE);
        }*/
        if (mTitle != null)
        {
           // mTitle.setCompoundDrawablesWithIntrinsicBounds(actionBarType.getIconResId(), 0, 0, 0);
            if (actionBarType.getTitleResId() != 0)
            {
                mTitle.setText(actionBarType.getTitleResId());
                mTitle.setTextColor(getResources().getColor(actionBarType.getMenuBackgroundColourResId()));
                mActionBarBackArrow.setVisibility(View.VISIBLE);
                mActionBarBackArrow.setImageDrawable(getResources().getDrawable(actionBarType.getIconResId()));


            }
            else
            {
                mTitle.setText("");
            }
        }
    }

    public void initializeTopBar() {
        ImageButton backButton = (ImageButton) getView().findViewById(R.id.topbar_left_image_button);
        TextView titleTextView = (TextView) getView().findViewById(R.id.topbar_title_text_view);
        Button rightButton = (Button) getView().findViewById(R.id.topbar_right_text_button);
        if (backButton != null && titleTextView != null) {
            int colorTheme = getColorTheme();
            titleTextView.setTextColor(getResources().getColor(colorTheme));

            switch (colorTheme) {
                case TopBarInterface.BLUE_THEME:
                    backButton.setImageResource(R.drawable.back_button_blue);
                    break;
                case TopBarInterface.GREEN_THEME:
                    backButton.setImageResource(R.drawable.back_arrow_green);
                    break;
                case TopBarInterface.ORANGE_THEME:
                    backButton.setImageResource(R.drawable.selector_orange_back_button);
                    break;
                case TopBarInterface.PURPLE_THEME:
                    backButton.setImageResource(R.drawable.ic_back_arrow_purple);
                    break;
                default:
                    L.e(this.getClass().getSimpleName(),
                            "Unexpected constant for top bar color theme");
                    break;
            }

            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTopBarButtonPressed();
                }
            });
        }
    }

    /**
     * @return allow back with hard back button in activity
     */
    public boolean isBackAllowed() {
        return true;
    }

    /* Top bar methods*/

    @Override
    public void setTitle(String title) {
        View parentView = getView();
        if (parentView != null) {
            TextView titleTextView = (TextView) getView().findViewById(R.id.topbar_title_text_view);
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(title);
        }
    }

    public void setTitle(int stringId, int dimenId) {
        String title = getString(stringId);
        setTitle(title);
        TextView titleTextView = (TextView) getView().findViewById(R.id.topbar_title_text_view);
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(dimenId));
    }

    public void setTitle(int stringId) {
        String title = getString(stringId);
        setTitle(title);
    }

    @Override
    public void setBackButtonVisible(boolean isVisible) {
        View parentView = getView();
        if (parentView != null) {
            ImageButton backButton = (ImageButton) parentView.findViewById(R.id.topbar_left_image_button);
            backButton.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    public void setRightImageButtonVisible(boolean isVisible) {
        View parentView = getView();
        if (parentView != null) {
            ImageButton rightButton = (ImageButton) parentView.findViewById(R.id.topbar_right_button);
            rightButton.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    public void setSecondRightImageButtonVisible(boolean isVisible) {
        View parentView = getView();
        if (parentView != null) {
            ImageButton secondRightButton = (ImageButton) parentView.findViewById(R.id.topbar_second_right_button);
            secondRightButton.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void setProgressBarVisible(boolean isVisible) {
        View parentView = getView();
        if (parentView != null) {
            ProgressBar progressBar = (ProgressBar) parentView.findViewById(R.id.topbar_network_progress_bar);
            progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    public void setLeftButton(final View.OnClickListener onClickListener, int stringResourceId) {
        View parentView = getView();
        if (parentView != null) {
            ImageButton leftImageButton = (ImageButton) parentView.findViewById(R.id.topbar_left_image_button);
            Button leftTextButton = (Button) parentView.findViewById(R.id.topbar_left_text_button);
            leftImageButton.setVisibility(View.GONE);
            leftTextButton.setVisibility(View.VISIBLE);
            leftTextButton.setTextColor(getResources().getColor(getColorTheme()));
            if (stringResourceId != 0) {
                leftTextButton.setText(stringResourceId);
            }
            leftTextButton.setOnClickListener(onClickListener);
        }
    }

    public void setLeftButtonText(String text) {
        View parentView = getView();
        if (parentView != null) {
            Button leftButton = (Button) parentView.findViewById(R.id.topbar_left_text_button);
            leftButton.setText(text);
        }
    }

    @Override
    public void setRightImageButton(final View.OnClickListener onClickListener, int iconResourceId) {
        View parentView = getView();
        if (parentView != null) {
            ImageButton rightButton = (ImageButton) parentView.findViewById(R.id.topbar_right_button);
            rightButton.setVisibility(View.VISIBLE);
            if (iconResourceId != 0) {
                rightButton.setImageResource(iconResourceId);
            }
            rightButton.setOnClickListener(onClickListener);
        }
    }

    @Override
    public void setRightButton(final View.OnClickListener onClickListener, @StringRes int stringResourceId) {
        View parentView = getView();
        if (parentView != null) {
            Button rightButton = (Button) parentView.findViewById(R.id.topbar_right_text_button);
            rightButton.setVisibility(View.VISIBLE);
            rightButton.setTextColor(getResources().getColor(getColorTheme()));

            if (stringResourceId != 0) {
                rightButton.setText(stringResourceId);
            }
            rightButton.setOnClickListener(onClickListener);
        }
    }

    public void setRightButtonEnabled(boolean isEnable, @ColorRes int textColorId) {
        View parentView = getView();
        if (parentView != null) {
            Button rightButton = (Button) parentView.findViewById(R.id.topbar_right_text_button);
            rightButton.setEnabled(isEnable);
            rightButton.setTextColor(getResources().getColor(textColorId));
        }
    }

    public void setRightButtonText(String text) {
        View parentView = getView();
        if (parentView != null) {
            Button rightButton = (Button) parentView.findViewById(R.id.topbar_right_text_button);
            rightButton.setText(text);
        }
    }

    public void setRightButtonVisibility(int visibility) {
        View parentView = getView();
        if (parentView != null) {

            final Button rightButton = (Button) parentView.findViewById(R.id.topbar_right_text_button);
            if (rightButton.getVisibility() != visibility) {
                rightButton.setVisibility(visibility);
            }
        }

    }

    @Override
    public void setSecondRightImageButton(final View.OnClickListener onClickListener, int iconResourceId) {
        View parentView = getView();
        if (parentView != null) {
            ImageButton rightButton = (ImageButton) parentView.findViewById(R.id.topbar_second_right_button);
            rightButton.setVisibility(View.VISIBLE);
            if (iconResourceId != 0) {
                rightButton.setImageResource(iconResourceId);
            }
            rightButton.setOnClickListener(onClickListener);
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }

    protected void startFragment(Fragment fragment) {
        startFragment(fragment, true);
    }

    protected void startFragment(Fragment fragment, boolean isAddToBackStack) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    protected boolean getBooleanValue(String text) {
        if (!TextUtils.isEmpty(text)) {
            try {
                return Float.valueOf(text).intValue() == 1;
            } catch (NumberFormatException e) {
                return Boolean.valueOf(text);
            }
        }
        return false;
    }

    class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(SyncAdapter.KEY_RESPONSE_ENTITY)) {
                Response response = (Response) intent.getSerializableExtra(SyncAdapter.KEY_RESPONSE_ENTITY);
                if (response.isSuccess()) {

                } else if (!TextUtils.isEmpty(response.getUserResponseError())) {
                    AlertDialogHelper.getInstance().showMessage(getActivity(), response.getUserResponseError());
                }
            }
        }
    }
}
