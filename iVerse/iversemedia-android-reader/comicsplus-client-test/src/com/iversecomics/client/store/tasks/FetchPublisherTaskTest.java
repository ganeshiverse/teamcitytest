package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ContentChangeTracker;
import com.iversecomics.client.store.db.PublishersTable;

public class FetchPublisherTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testFetchPublisher() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(PublishersTable.CONTENT_URI, this);

        String publisherId = "moonstone";
        ComicStore comicStore = getComicStore();
        FetchPublisherTask task = new FetchPublisherTask(comicStore, publisherId);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, PublishersTable.CONTENT_URI);
        assertEquals("Publishers table rows", 1, actualCount);
    }
}
