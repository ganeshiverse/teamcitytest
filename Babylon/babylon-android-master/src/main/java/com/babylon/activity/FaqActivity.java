package com.babylon.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.babylon.R;
import com.babylon.utils.UrlCache;
//import com.babylon.utils.UrlCache;

public class FaqActivity extends BaseActivity
{

    private WebView mWebView;
    //private ProgressDialog mProgressDialog;
    //private UrlCache mUrlCache = null;
    private PasswordConfirmActivity.IWebViewControl mWebViewControl;
    public static Activity mainActivityInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_fragment);

        initializeTopBar();

        setTitle(R.string.faqs);

        mWebView = (WebView) findViewById(R.id.faq_activity_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("http://www.babylonhealth.com/faqs");

        mWebViewControl = (PasswordConfirmActivity.IWebViewControl) mainActivityInstance;

        /*mProgressDialog = getLoadingDialog(this);

        this.mUrlCache = new UrlCache(this);

        this.mUrlCache.register("http://www.babylonhealth.com/faqs", "babylonhealth.html",
                "text/html", "UTF-8", 1000 * UrlCache.ONE_MINUTE);
*/
        mWebView.setWebViewClient(new WebViewClient()
        {

           /* @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                mProgressDialog.show();
            }

            public void onPageFinished(WebView view, String url)
            {
                if(mProgressDialog.isShowing())
                {
                    mProgressDialog.dismiss();
                }
            }*/

           /* @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                return mUrlCache.load(url);
            }*/

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                /*if (url.indexOf("babylonhealth.com") > -1)
                {
                    return false;
                }*/
                if (url.startsWith("mailto:"))
                {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse(url));
                    if (intent.resolveActivity(getPackageManager()) != null)
                    {
                        startActivity(intent);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }

        });
        mWebView.loadUrl("http://www.babylonhealth.com/faqs");

    }

    @Override
    public void onBackPressed()
    {
        if(mWebView.canGoBack())
        {
            mWebView.goBack();
        }
        else
        {
            super.onBackPressed();
        }
    }

    /*public static ProgressDialog getLoadingDialog(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        return progressDialog;
    }*/

}

