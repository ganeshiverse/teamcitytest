package com.babylon.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.controllers.BirthdayViewController;
import com.babylon.controllers.OnDateSetListener;
import com.babylon.controllers.PatientViewController;
import com.babylon.enums.Gender;
import com.babylon.model.Patient;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.L;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SetProfileActivity extends FragmentActivity {

    private static final String TAG = SetProfileActivity.class.getSimpleName();

    private PatientViewController patientViewController;
    private Patient patient;
    private Date currentBirthDate;
    private EditText weightEditText;
    private EditText heightEditText;
    private CheckBox maleCheckBox;
    private CheckBox femaleCheckBox;
    private BirthdayViewController birthdayViewController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_profile);

        patient = App.getInstance().getPatient();
        patientViewController = new PatientViewController(patient);
        currentBirthDate = patient.getBirthday();
        initViews();
    }

    public void onSendProfileClick(View view) {
        boolean wasProfileUpdated = wasProfileUpdated();

        if (wasProfileUpdated) {
            updatePatientProfile();

            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PATIENT, SyncAdapter.SYNC_PUSH);
        }

        setResult(Activity.RESULT_OK);
        finish();
    }

    private void updatePatientProfile() {
        if(birthdayViewController.isValid(currentBirthDate)) {
            patient.setBirthday(currentBirthDate);
        }

        final float curWeight = getFloat(weightEditText);
        patient.setWeight(curWeight);


        final float curHeight = getFloat(heightEditText);
        patient.setHeight(curHeight);

        final String gender = getSelectedGender();
        patient.setGender(gender);

        patient.setIsUpdated(true);

        App.getInstance().setPatient(patient);
    }

    private boolean wasProfileUpdated() {
        final float curWeight = getFloat(weightEditText);
        final float curHeight = getFloat(heightEditText);

        final boolean isBirthDateTheSame = patient.getBirthdayTime() == currentBirthDate.getTime();

        final String selectedGender = getSelectedGender();
        final boolean isGenderTheSame = selectedGender.equals(patient.getGender());

        final boolean result = !isBirthDateTheSame || curHeight != patient.getHeight() || curWeight != patient
                .getWeight() || !isGenderTheSame;

        return result;
    }

    private String getSelectedGender() {
        if (femaleCheckBox.isChecked()) {
            return Gender.getGender(Gender.FEMALE);
        } else if (maleCheckBox.isChecked()) {
            return Gender.getGender(Gender.MALE);
        } else {
            return null;
        }
    }

    private float getFloat(EditText editText) {
        float result = 0;
        final String weightText = editText.getText().toString();

        if (!TextUtils.isEmpty(weightText)) {
            result = Float.valueOf(weightText);
        }

        return result;
    }

    private void initViews() {
        maleCheckBox = (CheckBox) findViewById(R.id.chButtonMale);
        maleCheckBox.setOnCheckedChangeListener(onMaleCheckedChangeListener);

        femaleCheckBox = (CheckBox) findViewById(R.id.chButtonFemale);
        femaleCheckBox.setOnCheckedChangeListener(onFemaleCheckedChangeListener);

        showGender();

        final TextView birthDateTextView = (TextView) findViewById(R.id.dateofBirthEdt);
        showDateOfBirth(birthDateTextView);

        heightEditText = (EditText) findViewById(R.id.edit_text_height);
        patientViewController.initHeightEditText(heightEditText);

        weightEditText = (EditText) findViewById(R.id.edit_text_weight);
        patientViewController.initWeightEditText(weightEditText);

        final EditText birthDateErrorEditText = (EditText) findViewById(R.id.edit_text_birthday_error);
        birthDateErrorEditText.setInputType(InputType.TYPE_NULL);

        birthdayViewController = new BirthdayViewController(this, birthDateTextView, new OnDateSetListener() {

            @Override
            public void onDateSet(Date date) {
                birthdayViewController.validateBirthday(date, birthDateErrorEditText);
                currentBirthDate = date;
            }

        }, new Date());
    }

    private void showDateOfBirth(TextView birthDateTextView) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BirthdayViewController.BIRTH_DATE_FORMAT,
                Locale.getDefault());
        birthDateTextView.setText(simpleDateFormat.format(patient.getBirthday()));
    }

    private void showGender() {
        final Gender gender = Gender.get(patient.getGender());
        switch (gender) {
            case FEMALE:
                femaleCheckBox.setChecked(true);
                break;
            case MALE:
                maleCheckBox.setChecked(true);
                break;
            default:
                L.e(TAG, "Unknown Gender value");
        }
    }

    private final CompoundButton.OnCheckedChangeListener onMaleCheckedChangeListener = new CompoundButton
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (femaleCheckBox.isChecked() && b) {
                femaleCheckBox.setChecked(false);
            }

        }
    };

    private final CompoundButton.OnCheckedChangeListener onFemaleCheckedChangeListener = new CompoundButton
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (maleCheckBox.isChecked() && b) {
                maleCheckBox.setChecked(false);
            }
        }
    };
}
