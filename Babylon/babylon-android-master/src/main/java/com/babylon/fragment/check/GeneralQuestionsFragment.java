package com.babylon.fragment.check;

import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.Toast;

import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.enums.CheckGender;
import com.babylon.model.check.SymptomAnswer;
import com.babylon.model.check.ViewState;

import java.util.ArrayList;

public class GeneralQuestionsFragment extends QuestionsFragment {

    private static final int GENERAL_ROOT_SYMPTOMS_PARENT_ID = 0;

    public static GeneralQuestionsFragment newInstance(CheckGender gender, int bodyPartId, String title) {
        GeneralQuestionsFragment fragment = new GeneralQuestionsFragment();
        fragment.putArgs(gender, bodyPartId, title);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((BaseActivity) getActivity()).setRightImageButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                symptomsStack.clear();
                init();
            }
        }, R.drawable.btn_x);
    }

    private SparseArray<ArrayList<String>> groupAnswersByParentId(ArrayList<SymptomAnswer> symptomAnswers) {
        SparseArray<ArrayList<String>> groupedAnswers = new SparseArray<ArrayList<String>>();
        groupedAnswers.clear();
        for (int i = 0; i < symptomAnswers.size(); i++) {
            SymptomAnswer symptomAnswer = symptomAnswers.get(i);
            int parentId = symptomAnswer.getParentId();
            ArrayList<String> childAnswers = groupedAnswers.get(parentId);
            if (childAnswers == null) {
                childAnswers = new ArrayList<String>();
                groupedAnswers.append(parentId, childAnswers);
            }

            childAnswers.add(symptomAnswer.toString());
        }
        return groupedAnswers;
    }

    @Override
    protected ArrayList<String> getHashesList(ArrayList<SymptomAnswer> symptomAnswers) {
        SparseArray<ArrayList<String>> groupedAnswers = groupAnswersByParentId(symptomAnswers);
        ArrayList<String> symptomsHashes = new ArrayList<String>();
        for (int i = 0; i < groupedAnswers.size(); i++) {
            int parentId = groupedAnswers.keyAt(i);
            ArrayList<String> answers = groupedAnswers.get(parentId);
            if (parentId == GENERAL_ROOT_SYMPTOMS_PARENT_ID) {
                symptomsHashes.addAll(answers);
            } else {
                symptomsHashes.add(TextUtils.join("-", answers));
            }
        }
        return symptomsHashes;
    }

    @Override
    protected boolean hasUnselectedAnswers(ArrayList<SymptomAnswer> symptomAnswers) {
        int unselectedCount = 0;
        for (SymptomAnswer symptomAnswer : symptomAnswers) {
            if (ViewState.UNSELECTED.equals(symptomAnswer.getState())) {
                unselectedCount++;
            }
        }
        return unselectedCount >= symptomAnswers.size();
    }

    protected void notifyAboutUnselectedAnswers() {
        Toast.makeText(getActivity(), getString(R.string.select_one_answers), Toast.LENGTH_SHORT).show();
    }
}
