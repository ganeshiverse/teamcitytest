package com.iversecomics.client.downloads.internal;

import com.iversecomics.client.downloads.internal.data.DownloadStatus;

/**
 * Unable to write to the file
 */
public class FileWriteException extends DownloadFailedException {
    private static final long serialVersionUID = -2079719365887548837L;

    public FileWriteException(String detailMessage) {
        super(DownloadStatus.FILE_ERROR, detailMessage);
    }

    public FileWriteException(String detailMessage, Throwable throwable) {
        super(DownloadStatus.FILE_ERROR, detailMessage, throwable);
    }
}
