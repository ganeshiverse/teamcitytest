package com.babylon.enums;

import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;

import java.util.HashSet;
import java.util.Set;

public enum SamsungHealthPermission {
    WEIGHT_READ(HealthConstants.Weight.HEALTH_DATA_TYPE),
    STEP_READ(HealthConstants.StepCount.HEALTH_DATA_TYPE);

    private HealthPermissionManager.PermissionKey permissionKey;
    private String permissionName;

    private SamsungHealthPermission(String permissionName) {
        this(permissionName, HealthPermissionManager.PermissionType.READ);
    }

    private SamsungHealthPermission(String permissionName,
                                    HealthPermissionManager.PermissionType permissionType ) {
        permissionKey = new HealthPermissionManager.PermissionKey(permissionName,permissionType);
        this.permissionName = permissionName;
    }

    public HealthPermissionManager.PermissionKey getKey() {
        return permissionKey;
    }

    public String getName() {
        return permissionName;
    }

    public static Set<HealthPermissionManager.PermissionKey> getPermissionKeySet() {
        Set<HealthPermissionManager.PermissionKey> keySet = new HashSet<>();
        for (SamsungHealthPermission permission : values()) {
            keySet.add(permission.getKey());
        }
        return keySet;
    }
}
