package com.babylon.model.push;

public class AppointmentPush extends Push {

    private String appointmentId;

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointment) {
        this.appointmentId = appointment;
    }
}
