package com.babylon.api.request.payments;

import android.text.TextUtils;
import com.babylon.api.request.RubyResponseProcessor;
import com.babylon.api.request.base.AbsRequestDelete;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Subscription;
import com.babylon.model.payments.SubscriptionDeleteConfirmation;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionDelete extends AbsRequestDelete<SubscriptionDeleteConfirmation> {
    private int patientId;

    public AbsRequestDelete<SubscriptionDeleteConfirmation> withEntry(Subscription subscription) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("patient_id", subscription.getPatientId().toString()));
        return (SubscriptionDelete) new SubscriptionDelete().withParams(params);
    }

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.getRuby(Urls.SUBSCRIPTION_DELETE);
        return TextUtils.concat(url, urlParams).toString();
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<SubscriptionDeleteConfirmation> getResponseProcessor() {
        return new RubyResponseProcessor<SubscriptionDeleteConfirmation>(getOutputType());
    }
}
