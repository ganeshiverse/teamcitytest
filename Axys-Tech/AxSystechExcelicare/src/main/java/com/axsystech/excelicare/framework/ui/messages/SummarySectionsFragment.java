package com.axsystech.excelicare.framework.ui.messages;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.data.responses.SummarySectionsResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.adapters.MessagesInboxAndSentAdapter;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.GetMessageListFromDbLoader;
import com.axsystech.excelicare.loaders.GetMsgSentLoader;
import com.axsystech.excelicare.loaders.MsgInboxAndSentSearchLoader;
import com.axsystech.excelicare.loaders.SaveMessageListDBLoader;
import com.axsystech.excelicare.loaders.SavePriorityLoader;
import com.axsystech.excelicare.loaders.UpdateMsgPriorityInDbLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;
import java.util.List;

public class SummarySectionsFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = SummarySectionsFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private SummarySectionsFragment mFragment;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectView(R.id.summarySectionsListView)
    private ListView summarySectionsListView;

    private TextView noteTextView;
    private Button summarySelectButton;

    private ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj> summaryPanelDetailsObjsAL;
    private SummarySectionsAdapter summarySectionsAdapter;

    private SummarySectionsSelectionListener summarySelectionListener;

    public SummarySectionsSelectionListener getSummarySelectionListener() {
        return summarySelectionListener;
    }

    public void setSummarySelectionListener(SummarySectionsSelectionListener selectionListener) {
        this.summarySelectionListener = selectionListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary_sections, container, false);
        noteTextView = (TextView) view.findViewById(R.id.noteTextView);
        summarySelectButton = (Button) view.findViewById(R.id.summarySelectButton);
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        noteTextView.setTypeface(bold);
        summarySelectButton.setTypeface(bold);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        initViews();
        readIntentArgs();
        preloadData();
        addEventListeners();
    }

    private void initViews() {

    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SUMMARY_SECTIONS_LIST)) {
                summaryPanelDetailsObjsAL = (ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj>) bundle.getSerializable(AxSysConstants.EXTRA_SUMMARY_SECTIONS_LIST);
            }
        }
    }

    private void preloadData() {
        // display summary sections in listview
        summarySectionsAdapter = new SummarySectionsAdapter(summaryPanelDetailsObjsAL);
        summarySectionsListView.setAdapter(summarySectionsAdapter);
    }

    private void addEventListeners() {
        summarySelectButton.setOnClickListener(this);
    }

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.summarySelectButton:
                if (summarySelectionListener != null) {
                    summarySelectionListener.onSummarySectionsSelected(summaryPanelDetailsObjsAL);
                }
                mActivity.onBackPressed();
                break;
            default:
                break;
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class SummarySectionsAdapter extends BaseAdapter {

        private ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj> summaryPanelDetailsObjsAL;

        public SummarySectionsAdapter(ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj> summaryPanelDetailsObjsAL) {
            this.summaryPanelDetailsObjsAL = summaryPanelDetailsObjsAL;
        }

        @Override
        public int getCount() {
            return (summaryPanelDetailsObjsAL != null && summaryPanelDetailsObjsAL.size() > 0) ? summaryPanelDetailsObjsAL.size() : 0;
        }

        @Override
        public SummarySectionsResponse.SummaryPanelDetailsObj getItem(int position) {
            return summaryPanelDetailsObjsAL.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final SummarySectionsResponse.SummaryPanelDetailsObj rowObj = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.summary_sections_row, parent, false);
            }

            TextView summarySectionNameTextView = (TextView) convertView.findViewById(R.id.summarySectionNameTextView);
            final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            final CheckBox checkBoxChecked = (CheckBox) convertView.findViewById(R.id.checkBoxChecked);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            summarySectionNameTextView.setTypeface(regular);

            summarySectionNameTextView.setText(!TextUtils.isEmpty(rowObj.getComprehensiveHealthSummaryList()) ? rowObj.getComprehensiveHealthSummaryList() : "");
            checkBox.setChecked(rowObj.isSelected());

            checkBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkBoxChecked.setVisibility(View.VISIBLE);
                    checkBox.setVisibility(View.GONE);
                    rowObj.setIsSelected(checkBox.isChecked());

                    checkBox.setChecked(rowObj.isSelected());
                    notifyDataSetChanged();
                }
            });
            checkBoxChecked.setChecked(!rowObj.isSelected());
            checkBoxChecked.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBoxChecked.setVisibility(View.GONE);
                    checkBox.setVisibility(View.VISIBLE);
                    rowObj.setIsSelected(!checkBox.isChecked());
                    notifyDataSetChanged();
                }
            });
           /* convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    rowObj.setIsSelected(!checkBox.isChecked());

                    checkBox.setChecked(rowObj.isSelected());
                    checkBoxChecked.setVisibility(View.VISIBLE);
                    checkBox.setVisibility(View.GONE);
                    notifyDataSetChanged();
                }
            });*/

            return convertView;
        }
    }

    public interface SummarySectionsSelectionListener {
        public void onSummarySectionsSelected(ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj> summaryPanelDetailsObjsAL);
    }
}
