package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CircleDetailsEditResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class CircleDetailsLoader extends DataAsyncTaskLibLoader<CircleDetailsEditResponse> {

    private String token;
    private String userCircleId;


    public CircleDetailsLoader(Context context, String token, String userCircleId) {
        super(context);
        this.token = token;
        this.userCircleId = userCircleId;
    }

    @Override
    protected CircleDetailsEditResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCircleDetailsContent(token, userCircleId);
    }
}
