package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ForgotPasswordResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class DeleteProfilePhotoLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String patientId;
    private String securityAns1;
    private String securityQ2;
    private String securityAns2;

    public DeleteProfilePhotoLoader(Context context, String token, String patientId) {
        super(context);
        this.token = token;
        this.patientId = patientId;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().deleteProfilePhoto(token, patientId);
    }
}
