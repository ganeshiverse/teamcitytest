package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public class MonitorMeReportSchema {
    public static final Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon()
            .appendPath("entity").appendPath(Table.MONITOR_ME.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        DATE("date", DBType.INT),
        FEATURES("features", DBType.TEXT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }


        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return columnName;
        }
    }

    public interface Query {
        public static String DAY_SELECTION = Columns.DATE.getName() + ">= ? AND " +
                Columns.DATE.getName() + "< ? ";

        public static final String[] PROJECTION = {
                Columns._ID.getName(),
                Columns.DATE.getName(),
                Columns.FEATURES.getName()
        };

        int _ID = 0;
        int DATE = 1;
        int FEATURES = 2;
    }
}
