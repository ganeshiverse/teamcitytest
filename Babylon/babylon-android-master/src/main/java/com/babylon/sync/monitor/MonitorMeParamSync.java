package com.babylon.sync.monitor;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.MonitorMeParamsPost;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.Table;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.MeasurePostModel;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.sync.AbsSyncStrategy;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MonitorMeParamSync extends AbsSyncStrategy {

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        Cursor notSyncMonitorMeParamsCursor = getUpdateCursor(contentResolver, Table.MONITOR_ME_PARAM,
                MonitorMeParamSchema.CONTENT_URI, MonitorMeParamSchema.Query.PROJECTION);

        if (notSyncMonitorMeParamsCursor.moveToFirst()) {


            ConverterParamsToServerModel converterParamsToServerModel =
                    new ConverterParamsToServerModel();
            MeasurePostModel measurePostModel = new MeasurePostModel();

            do {
                MonitorMeFeatures.Feature monitorMeFeature = new MonitorMeFeatures.Feature();
                monitorMeFeature.createFromCursor(notSyncMonitorMeParamsCursor);
                measurePostModel.add(monitorMeFeature);

                String curParamId = notSyncMonitorMeParamsCursor
                        .getString(MonitorMeParamSchema.Query._ID);
                converterParamsToServerModel.concatParamIds(curParamId);

                if (converterParamsToServerModel.getFirstDateToSync() == null) {
                    converterParamsToServerModel.setFirstDateToSync(monitorMeFeature.getDate());
                }

                if ((notSyncMonitorMeParamsCursor.getPosition() + 1)
                        % SyncUtils.MAX_SYNC_OBJECTS_NUMBER == 0
                        || notSyncMonitorMeParamsCursor.isLast()) {
                    converterParamsToServerModel.setMeasurePostModel(measurePostModel);
                    sync(contentResolver, converterParamsToServerModel);
                    measurePostModel = new MeasurePostModel();
                    converterParamsToServerModel = new ConverterParamsToServerModel();
                }

            } while (notSyncMonitorMeParamsCursor.moveToNext());
        }

        notSyncMonitorMeParamsCursor.close();
    }

    private void sync(ContentResolver contentResolver,
                      ConverterParamsToServerModel converterParamsToServerModel) {
        final MeasurePostModel measurePostModel =
                converterParamsToServerModel.getMeasurePostModel();

        final Response response = sendMonitorMeParamsToServer(measurePostModel);
        if (response != null && response.isSuccess()) {
            markMonitorMeParamsAsSynced(contentResolver,
                    converterParamsToServerModel.getParamIds());
        }

        long firstDateToSync = DateUtils.getDayStartMills(
                converterParamsToServerModel.getFirstDateToSync());
        MonitorReportSync.startPullMonitorMeReport(contentResolver, firstDateToSync);
    }

    private void markMonitorMeParamsAsSynced(ContentResolver contentResolver, String ids) {
        final MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature();
        feature.setSyncWithServer(SyncUtils.SYNC_TYPE_ADDED);

        final String where = MonitorMeParamSchema.Columns._ID + " IN (" + ids + ")";
        contentResolver.update(MonitorMeParamSchema.CONTENT_URI, feature.toContentValues(), where,
                null);
    }

    private Response sendMonitorMeParamsToServer(MeasurePostModel measurePostModel) {
        final MonitorMeParamsPost monitorMeParamsPost = new MonitorMeParamsPost();
        monitorMeParamsPost.withEntry(measurePostModel);
        return monitorMeParamsPost.execute();
    }

    /**
     * Save monitorMe response in cash (preferences) and parse this string
     * when display monitor me screen
     */
    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        long dateFrom = bundle.getLong(Constants.EXTRA_MIN_DATE, SyncUtils.getMinDateSyncInMillis());
        long endOfTheDay = bundle.getLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(new Date(), 1));

        Response<ResponseList<MonitorMeFeatures>> paramsResponse = MonitorMeParamsGet
                .newMonitorMeParamsGet(dateFrom, endOfTheDay,
                        MonitorMeParam.getParameterList()).execute();

        saveMonitorMeParams(contentResolver, paramsResponse, dateFrom, endOfTheDay);

        //because we do not need reports more that one day
        if ((dateFrom - endOfTheDay) <= android.text.format.DateUtils.DAY_IN_MILLIS) {
            new MonitorReportSync().pull(contentResolver, bundle);
        }
    }

    public static void saveMonitorMeParams(ContentResolver contentResolver, Response<ResponseList<MonitorMeFeatures>>
            paramsResponse, long dateFrom, long endOfTheDay) {
        if (paramsResponse != null && paramsResponse.isSuccess()
                && paramsResponse.getData() != null) {
            List<MonitorMeFeatures> featuresList = paramsResponse.getData().getList();
            if (featuresList != null && !featuresList.isEmpty()) {
                /* delete old raw and save new */
                List<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

                DatabaseOperationUtils.getInstance().deleteFeature(contentResolver, featuresList, dateFrom,
                        endOfTheDay, operations);
                saveResults(featuresList, operations);
                DatabaseOperationUtils.applyBatch(contentResolver, operations);
            }
        }
    }

    private static void saveResults(List<MonitorMeFeatures> featuresList,
                                    List<ContentProviderOperation> operations) {

        for (MonitorMeFeatures features : featuresList) {
            saveFeaturesToBatch(operations, features);
        }
    }

    private static void saveFeaturesToBatch(List<ContentProviderOperation> batch, MonitorMeFeatures features) {
        if (features != null && features.getFeatures() != null) {
            for (MonitorMeFeatures.Feature feature : features.getFeatures()) {
                feature.setDate(features.getDate());
                saveFeature(batch, feature);
            }
        }
    }

    private static void saveFeature(List<ContentProviderOperation> batch, MonitorMeFeatures.Feature features) {
        features.setSyncWithServer(SyncUtils.SYNC_TYPE_ADDED);

        final ContentProviderOperation.Builder insertBuilder = ContentProviderOperation.newInsert
                (MonitorMeParamSchema
                        .CONTENT_URI);

        insertBuilder.withValues(features.toContentValues());
        batch.add(insertBuilder.build());
    }

}