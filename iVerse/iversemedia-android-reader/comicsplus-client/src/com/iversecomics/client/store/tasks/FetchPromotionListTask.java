package com.iversecomics.client.store.tasks;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchPromotionListTask extends ComicStoreTask {
	public static final String PROMOTION_UPDATED = "promotionupdated";
    private static final Logger LOG = LoggerFactory.getLogger(FetchPromotionListTask.class);
    private final String        freshnessKey;
    private Context context;


    public FetchPromotionListTask(ComicStore comicStore, Context context) {
        super(comicStore);
        this.freshnessKey = "PromotionList";
        this.context = context;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchPromotionListTask other = (FetchPromotionListTask) obj;
        if (freshnessKey == null) {
            if (other.freshnessKey != null) {
                return false;
            }
        } else if (!freshnessKey.equals(other.freshnessKey)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();
            JSONObject json = server.getPromotionList();
            ResponseParser parser = new ResponseParser();
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            parser.parsePromotion(json, updater);
            
            final Intent intent = new Intent(PROMOTION_UPDATED);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            
            server.updateFreshness(freshnessKey, Time.HOUR * 3);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update ");
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((freshnessKey == null) ? 0 : freshnessKey.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(freshnessKey);
    }
}
