package com.babylonpartners.babylon;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.*;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.*;
import com.babylon.activity.family.FamilyAccountsActivity;
import com.babylon.activity.payments.AppointmentTransactionActivity;
import com.babylon.activity.payments.PaymentCardsListActivity;
import com.babylon.activity.payments.RecordTransactionActivity;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.AppLogPost;
import com.babylon.api.request.AppointmentPatch;
import com.babylon.api.request.AudioRecordingGet;
import com.babylon.api.request.base.AbsRequest;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.enums.Screen;
import com.babylon.model.*;
import com.babylon.model.VideoSession;
import com.babylon.model.payments.Appointment;
import com.babylon.model.payments.OrderDetail;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.*;
import com.babylon.view.TypefaceTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HomePageBackboneActivity extends FragmentActivity implements PasswordConfirmActivity.IWebViewControl,DialogInterface.OnDismissListener,GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener{
    private WebView mWebView;
    private WebViewJsManager mWebViewJSManager;
    private Bundle mBundle;

    //Constants
    private int TIME_OUT = 500;
    private static final int CODE_HOST_FROM_WEB = 202;
    private static final int LOGIN_REQUEST_CODE = 100;
    public static final int CREATE_ACCOUNT_REQUEST_CODE = 101;
    private static final int RESET_PASSWORD_REQUEST_CODE = 102;
    private static final int SELECT_PRACTICE_REQUEST_CODE = 103;
    private static final int FAMILY_ACCOUNTS_REQUEST_CODE = 106;
    private static final int KIT_CATEGORIES_REQUEST_CODE = 107;
    private final static int REQUEST_CODE_VIDEO_SESSION = 2;
    private static final int PHONE_NUMBER_REQUEST_CODE = 104;
    private final static int REQUEST_CODE_IMAGE_PICKER = 1;

    public static final String ACCOUNT_EXTRA_KEY = "ACCOUNT";
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String APPTAG = "Babylon";

    private static final String TAG = HomePageBackboneActivity.class.getSimpleName();
    private com.babylon.model.VideoSession startedVideoSession;
    private Appointment appointment;
    private boolean mIsLogin = false;
    private boolean mIsBookConsultation = false;
    private boolean mIsClinicalRecords = false;
    private boolean mIsPrescriptionScreen = false;
    private boolean mDoOnce = true;
    private LocationClient locationClient;
    private boolean waitingForPlayServices = true;
    private Intent pendingIntent;
    private Integer pendingRequestCode = null;
    private RelativeLayout mTopBarLayout;
    private boolean mShowTopBar=false;
    private String mTitle;
    private ImageButton mCloseBtn;
    private TypefaceTextView mTitleView;
    private boolean isFamilyAccountClicked= false;
    private TextView mAddButton;

    private String APPOINTMENTS_URL = "file:///android_asset/dist/index.html#appointments/new";
    private String VIEW_APPOINTMENTS = "file:///android_asset/dist/index.html#appointments";
    private String VIEW_PRESCRIPTIONS = "file:///android_asset/dist/index.html#prescription";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home_page_backbone);

        //Hide actionbar
        getActionBar().hide();

        //When Activity is launched with URL and params, we load the webview with this URL
        mBundle = getIntent().getExtras();
        if(mBundle.getString("TOPBAR")!=null){
            mShowTopBar=true;
            mTitle=mBundle.getString("TOPBAR");
        }

        /*if (mBundle != null)
        {
            //to do
            if(mBundle.getString("URL").equals("BOOK_CONSULTATION"))
            {
                mIsBookConsultation = true;
            }
            else if(mBundle.getString("URL").equals("CLINICAL_RECORDS"))
            {
                mIsClinicalRecords = true;
            }
            else if(mBundle.getString("URL").equals("PRESCRIPTIONS"))
            {
                mIsPrescriptionScreen = true;
            }
        }*/

        //Initializing WebView
        locationClient  = new LocationClient(this, this, this);
        initWebView();

        waitingForPlayServices = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        locationClient.connect();
    }

    public void navigateBackToNative()
    {
        HomePageBackboneActivity.this.finish();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mWebViewJSManager.resumeJsCalls();
        if(isFamilyAccountClicked)
        {
            mWebView.reload();
            isFamilyAccountClicked = false;
        }

        refreshNotificationsCount();
        //refreshRegions();
        com.facebook.AppEventsLogger.activateApp(this, getString(R.string.facebook_app_id));

        if (PreferencesManager.getInstance().shouldShowTermsPopup())
        {
            PreferencesManager.getInstance().setShouldShowTermsPopup(false);
            Intent termsPopupIntent = new Intent(this, TermsPopupActivity.class);
            startActivity(termsPopupIntent);
        }
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        locationClient.disconnect();

        AnalyticsWrapper.flush();

        //NOT SURE WHY THIS IS NEEDED

        /*if (mWebView != null)
        {
            // Remove the WebView from the old placeholder
            webViewPlaceholder.removeView(mWebView);
        }*/

        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the state of the WebView
        mWebView.saveState(outState);
    }

    @Override
    public void onBackPressed()
    {
        String abc = mWebView.getUrl();
        if(mWebView!= null && mWebView.canGoBack() && !mWebView.getUrl().contains(APPOINTMENTS_URL) && !mWebView.getUrl().contains(VIEW_APPOINTMENTS) && !mWebView.getUrl().contains(VIEW_PRESCRIPTIONS))
        {
            mWebView.goBack();
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore the state of the WebView
        mWebView.restoreState(savedInstanceState);
    }

    private void refreshNotificationsCount() {
        mWebViewJSManager.callJs("Babylon.Vent.trigger('home:refresh_notifications')");
    }

    //Initialize Web View
    private void initWebView()
    {
        mTopBarLayout=(RelativeLayout)findViewById(R.id.top_layout);
        mAddButton = (TextView) mTopBarLayout.findViewById(R.id.fragment_home_page_backbone_add_button);
        mTitleView=(TypefaceTextView)findViewById(R.id.fragment_home_page_backbone_title);
        mCloseBtn=(ImageButton)findViewById(R.id.fragment_home_page_backbone_close_img);

        mCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomePageBackboneActivity.this.finish();

            }
        });

        if(mShowTopBar){
            mTopBarLayout.setVisibility(View.VISIBLE);
            mTitleView.setText(mTitle);
        }
        else{
            mTopBarLayout.setVisibility(View.GONE);
        }
        if (mWebView == null)
        {
            mWebView = (WebView) findViewById(R.id.fragment_home_page_backbone_webview);

            //Setting WebView properties
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);

            String databasePath = mWebView.getContext().getDir("databases", Context.MODE_PRIVATE).getPath();
            mWebView.getSettings().setDatabasePath(databasePath);
            mWebView.getSettings().setDomStorageEnabled(true);
            mWebView.addJavascriptInterface(new WebAppInterface(), "Android");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                WebView.setWebContentsDebuggingEnabled(true);
            }

            //Setting webview client
            mWebView.setWebViewClient(new WebViewClient()
            {
                @Override
                public void onPageFinished(WebView view, String url)
                {
                    super.onPageFinished(view, url);
                    fetchHostFromWeb();

                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            refreshWebView();
                            if(mDoOnce)
                            {
                                if (mBundle.containsKey("PAGE2")) {
                                    BackbonePageConstants backBoneConstantPage2 = (BackbonePageConstants) mBundle.getSerializable("PAGE2");

                                    //If the command string is a JS command
                                    if (backBoneConstantPage2.getIsJsCommand())
                                    {
                                       if(mBundle.containsKey("URL_FORMAT_NEEDED"))
                                        {
                                              String urlToLoad = String.format(backBoneConstantPage2.getPageUrl(),
                                                Integer.parseInt(mBundle.getString("URL_FORMAT_NEEDED")));
                                            mWebViewJSManager.callJs(new String(urlToLoad));
                                        }
                                        else {
                                           String urlToLoad ="Babylon.routers.appointments.new()";
                                           mWebViewJSManager.callJs(backBoneConstantPage2.getPageUrl());
                                     }
                                    }
                                    //If the command string is a url to be loaded on the webview
                                    else {
                                        mWebView.loadUrl(backBoneConstantPage2.getPageUrl());
                                    }
                                }
                                mDoOnce = false;
                            }
                        }
                    }, TIME_OUT);

                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url)
                {
                    if (url.contains("babylon://"))
                    {
                        String command = url.split("babylon://")[1];
                        Log.w("Babylon-JS-Bridge", "calling from JS: " + command);

                        JSONObject jsonObject = null;
                        try
                        {
                            jsonObject = new JSONObject(command);
                        }
                        catch (JSONException e)
                        {
                            L.e(TAG, e.toString(), e);
                        }

                        if (jsonObject != null)
                        {
                            Iterator<?> keys = jsonObject.keys();
                            while (keys.hasNext())
                            {
                                String key = (String) keys.next();
                                try
                                {
                                    if (jsonObject.isNull(key))
                                    {
                                        nativeCall(key, null);
                                    }
                                    else
                                    {
                                        Object value = jsonObject.get(key);
                                        nativeCall(key, value);
                                    }
                                }
                                catch (JSONException e)
                                {
                                    L.e(TAG, e.toString(), e);
                                }
                            }
                        }

                        return true;
                    }
                    else if (url.startsWith("tel:"))
                    {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    }
                    else if (url.startsWith("mailto:"))
                    {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse(url));
                        if (intent.resolveActivity(getPackageManager()) != null)
                        {
                            startActivity(intent);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });

            mWebView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    Log.d("MyApplication", cm.message() + " -- From line " + cm.lineNumber() + " of " + cm.sourceId());
                    return true;
                }
            });

            //enableDebugging();
            // allowUniversalAccess();
            //loadEnvironment(HtmlPage.index);
            //mWebView.loadUrl(HtmlPage.index.getUri("index"));

        }

        mWebViewJSManager = new WebViewJsManager(mWebView);
        if(mBundle.containsKey("PAGE1"))
        {
            BackbonePageConstants backBoneConstantPage1 = (BackbonePageConstants) mBundle.getSerializable("PAGE1");

            //If the command string is a JS command
            if(backBoneConstantPage1.getIsJsCommand())
            {
                mWebViewJSManager.callJs(backBoneConstantPage1.getPageUrl());
            }
            //If the command string is a url to be loaded on the webview
            else
            {
                mWebView.loadUrl(backBoneConstantPage1.getPageUrl());
            }
        }
    }

    public void mobileAppLog(String message) {
        final AppLogPost appLogPost = new AppLogPost();
        final String patientId = App.getInstance().getPatient().getId();
        final AppLog appLog = new AppLog(message, patientId);

        appLogPost.withEntry(appLog);
        appLogPost.execute(this, appLogRequestListener);
    }

    private final RequestListener<String> appLogRequestListener = new RequestListener<String>() {
        @Override
        public void onRequestComplete(Response<String> response) {
            //process response here
        }
    };

    private void refreshWebView()
    {
        Wsse wsse = SyncUtils.getWsse();

        if (mBundle != null &&  mBundle.containsKey("PAGE1") && !TextUtils.isEmpty(mBundle.getString("OPEN")))
        {

            if (mBundle.getString("OPEN").equals("OPEN_CLINICAL")) {


                String accountJson = mBundle.getString(ACCOUNT_EXTRA_KEY);
                Gson gson = new Gson();
                Account account = gson.fromJson(accountJson, Account.class);

                String apiKey =account.getSwitchAccountAccessToken();


                final StringBuilder js = new StringBuilder();
                js.append(String.format("Babylon.Vent.trigger('patient:logged_in', %s);", accountJson))
                        .append(String.format("Babylon.Vent.trigger('set:request:header', '%s');", apiKey))
                        .append("Babylon.Vent.trigger('homePage:reload');");
                mWebViewJSManager.callJs(js.toString());

                mIsLogin = true;

                refreshCurrentRegionInWebView();
                refreshVersionHeaderInWebView();
            }

            /*if (mBundle.getString("OPEN").equals("OPEN_PRESCRIPTION")) {


                String apiKey = wsse.getToken();
                Patient patient = App.getInstance().getPatient();
                String patientJson = GsonWrapper.getGson().toJson(patient);

               *//* final StringBuilder js = new StringBuilder();
                js.append(String.format("Babylon.Vent.trigger('patient:logged_in', %s);", patientJson))
                        .append(String.format("Babylon.Vent.trigger('set:request:header', '%s');", apiKey))
                        .append("Babylon.Vent.trigger('homePage:reload');");
                mWebViewJSManager.callJs(js.toString());

                mIsLogin = true;*//*

                BackbonePageConstants backBoneConstantPage2 = (BackbonePageConstants) mBundle.getSerializable("PAGE2");

                if(mBundle.containsKey("URL_FORMAT_NEEDED"))
                {
                    String urlToLoad = String.format(backBoneConstantPage2.getPageUrl(),Integer.parseInt(mBundle.getString("URL_FORMAT_NEEDED")));
                    mWebViewJSManager.callJs(new String(urlToLoad));
                }
                refreshCurrentRegionInWebView();
                refreshVersionHeaderInWebView();
            }*/
        }
        else if (wsse != null && !mIsLogin && SyncUtils.isLoggedIn()) {

            String apiKey = wsse.getToken();
            Patient patient = App.getInstance().getPatient();
            String patientJson = GsonWrapper.getGson().toJson(patient);

            final StringBuilder js = new StringBuilder();
            js.append(String.format("Babylon.Vent.trigger('patient:logged_in', %s);", patientJson))
                    .append(String.format("Babylon.Vent.trigger('set:request:header', '%s');", apiKey))
                    .append("Babylon.Vent.trigger('homePage:reload');");
            mWebViewJSManager.callJs(js.toString());

            mIsLogin = true;
            refreshCurrentRegionInWebView();
            refreshVersionHeaderInWebView();

        }
        // TODO: still needed?
        //setVisibleTestButtons(SyncUtils.isLoggedIn());
    }

    private void refreshCurrentRegionInWebView()
    {
        Region region = App.getInstance().getCurrentRegion();
        if(region != null)
        {
            String regionJson = GsonWrapper.getGson().toJson(region);

            String setVersionHeader = String.format("Babylon.Vent.trigger('region:loaded', %s);", regionJson);
            mWebViewJSManager.callJs(setVersionHeader);
        }
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void nativeAlert(String message)
    {

        message = message.replaceAll("/n", "\n");
        AlertDialogHelper.getInstance().showMessage(HomePageBackboneActivity.this, message, HomePageBackboneActivity.this);
    }

    public void nativeInsertAddButton()
    {
        if(mTopBarLayout != null && mAddButton != null)
        {
            mAddButton.setVisibility(View.VISIBLE);
        }
    }

    public void nativeHideAddButton()
    {
        if(mTopBarLayout != null && mAddButton != null)
        {
            mAddButton.setVisibility(View.GONE);
        }
    }

    private void refreshVersionHeaderInWebView()
    {
        String setVersionHeader = String.format("Babylon.Vent.trigger('set:request:version_header', %s);", getRequestHeaderForVersion());
        mWebViewJSManager.callJs(setVersionHeader);
    }

    private String getRequestHeaderForVersion()
    {
        HashMap headers = new HashMap();

        for (Map.Entry<String, String> entry : AbsRequest.getVersionHeader().entrySet())
            headers.put(entry.getKey(), entry.getValue());

        return GsonWrapper.getGson().toJson(headers);
    }

    private void nativeRequestPhoneNumberDialog() {
        Intent intent = new Intent(this, PhoneNumberDialogActivity.class);
        startActivityForResult(intent, PHONE_NUMBER_REQUEST_CODE);
    }

    private void nativeCall(String methodName, Object object) {
        try {
            if (object != null) {
                Method m = HomePageBackboneActivity.class.getDeclaredMethod(methodName, object.getClass());
                Object[] params = {object};
                m.invoke(HomePageBackboneActivity.this, params);
            } else {
                Method m = HomePageBackboneActivity.class.getDeclaredMethod(methodName);
                m.invoke(HomePageBackboneActivity.this);
            }
        } catch (IllegalArgumentException e) {
            Log.e("Babylon-JS-Bridge", "IllegalArgumentException " + e);
        } catch (IllegalAccessException e) {
            Log.e("Babylon-JS-Bridge", "IllegalAccessException " + e);
        } catch (InvocationTargetException e) {
            Log.e("Babylon-JS-Bridge", "InvocationTargetException ", e);
        } catch (NoSuchMethodException e) {
            Log.e("Babylon-JS-Bridge", "invalid method ", e);
        }
    }

    @Override
    public void webViewGoBack() {
        if (mWebView != null) {
            mWebView.goBack();
        }
    }

    //Image upload methods
    public void pickImage(String source) {
        Intent imagePicker = new Intent(HomePageBackboneActivity.this, Base64ImagePicker.class);
        imagePicker.putExtra(Base64ImagePicker.TYPE_IDENTIFIER, source);
        startActivityForResult(imagePicker, REQUEST_CODE_IMAGE_PICKER);
    }

    private void imagePickerPicked(String base64) {
        mWebViewJSManager.callJs("Babylon.Vent.trigger('imagePicked', '" + base64.replace("\n", "") + "')");
    }

    private void imagePickerCancelled() {
        mWebViewJSManager.callJs("Babylon.Vent.trigger('imagePickerCancelled')");
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        refreshNotificationsCount();
    }

    private void startIntent(Intent intent) {
        startIntent(intent, null);
    }

    private void startIntent(Intent intent, Integer requestCode) {
        if (waitingForPlayServices) {
            pendingIntent = intent;
            pendingRequestCode = requestCode;
        } else {
            if (requestCode != null) {
                startActivityForResult(intent, requestCode);
            } else {
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        App.getInstance().setLocation(locationClient.getLastLocation());
        if (waitingForPlayServices) {
            waitingForPlayServices = false;
            if (pendingIntent != null) {
                startIntent(pendingIntent, pendingRequestCode);
                pendingIntent = null;
                pendingRequestCode = null;
            }
        }
    }

    @Override
    public void onDisconnected()
    {
        waitingForPlayServices = true;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */

        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);


            } catch (IntentSender.SendIntentException e) {
                // Log the error
                L.e(TAG, e.toString(), e);
            }
        } else {
/*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */

            showErrorDialog(connectionResult.getErrorCode());
        }

    }

    public void getPosition() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        if (lastKnownLocation == null) {
            mWebViewJSManager.callJs("Babylon.Vent.trigger('position:not_found'");
        } else {
            double latitude = lastKnownLocation.getLatitude();
            double longitude = lastKnownLocation.getLongitude();
            mWebViewJSManager.callJs("Babylon.Vent.trigger('position:found', " + latitude + ", " + longitude + ")");
        }
    }

    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                this,
                CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getFragmentManager(), APPTAG);
        }
    }

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog dialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            dialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            this.dialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            return dialog;
        }
    }

    public static enum HtmlPage {
        index, appointments_new, terms;

        public String getUri() {
            return getUri(null);
        }

        public String getUri(String params) {
            StringBuilder uri = new StringBuilder("file:///android_asset/dist/");
            uri.append(name()).append(".html");
            if (!TextUtils.isEmpty(params)) {
                uri.append(params);
            }
            return uri.toString();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mWebViewJSManager.pauseJsCalls();
    }

    public void fetchHostFromWeb() {
        mWebViewJSManager.callJs(String.format("Android.get(%d,Babylon.Config.baseUrl);", CODE_HOST_FROM_WEB));
    }

    public class WebAppInterface {
        @android.webkit.JavascriptInterface
        public void get(int code, String value) {
            switch (code) {
                case CODE_HOST_FROM_WEB:
                    L.d(TAG, "Host from web: " + value);
                    break;
                default:
                    break;
            }
        }
    }

    // Method calling via reflection
    public void mixpanelEvent(String screenStr) {
        Screen screen = Screen.valueOf(screenStr);
        switch (screen) {
            case consultant:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_CONSULTANT_CLICK);
                }
                break;
            case record:
                PasswordConfirmActivity.mainActivityInstance = HomePageBackboneActivity.this;
                Intent temp = new Intent(HomePageBackboneActivity.this, PasswordConfirmActivity.class);
                startActivity(temp);
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.OPENED_RECORD);
                }
                break;
            case settings:

                if (PreferencesManager.getInstance().isFirstLogin()) {
                    requestScreen(Screen.createAccount.toString());
                } else if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.OPENED_SETTINGS);
                }
                break;
            case notifications:
                if (PreferencesManager.getInstance().isFirstLogin()) {
                    requestScreen(Screen.createAccount.toString());
                } else if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_NOTIFICATIONS_CLICK);
                }
                break;
            case monitorMe:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_MONITOR_CLICK);
                }
                break;
            case ask:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_ASK_CLICK);
                }
                break;
            case shop:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_SHOP_CLICK);
                }
                break;
            case check:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_CHECK_CLICK);
                }
                break;
            case appointmentTried:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.APPOINTMENT_TRIED);
                }
                break;
            case appointmentBooked:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.APPOINTMENT_BOOKED);
                }
                break;
            case prescriptionReceived:
                if (SyncUtils.isLoggedIn()) {
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.PRESCRIPTION_RECEIVED);
                }
                break;
            default:
                break;
        }

    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void startVideo(JSONObject arguments) {
        String sessionId = null;
        String token = null;
        String identifier = null;
        String consultantName = null;
        String consultantAvatarUrl = null;
        String appointmentId = null;
        boolean enableVoiceCalls = true;
        try {
            token = arguments.getString("token");
            sessionId = arguments.getString("session");
            identifier = arguments.getString("identifier");
            consultantName = arguments.getString("consultantName");
            consultantAvatarUrl = arguments.getString("consultantAvatarUrl");
            appointmentId = arguments.getString("appointmentId");
            enableVoiceCalls = arguments.getBoolean("enableVoiceCalls");
        } catch (JSONException e) {
            if (token == null || sessionId == null) {
                Log.e("Babylon-video", "startVideo failed", e);
                return;
            }
        }

        Intent videoIntent = new Intent(this, VideoSessionActivity.class);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_SESSION_ID, sessionId);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_TOKEN, token);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_IDENTIFIER, identifier);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_NAME, consultantName);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_CONSULTANT_AVATAR_URL, consultantAvatarUrl);
        videoIntent.putExtra(VideoSessionActivity.INTENT_EXTRA_ENABLE_VOICE_CALLS, enableVoiceCalls);

        startedVideoSession = new VideoSession();
        startedVideoSession.setAppointmentId(appointmentId);

        startActivityForResult(videoIntent, REQUEST_CODE_VIDEO_SESSION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_VIDEO_SESSION:
                VideoSession session = startedVideoSession;
                //mWebViewJSManager.callJs("Babylon.Vent.trigger('rating:display', " + session.appointmentId() + ")");
                startedVideoSession = null;
                break;
            case REQUEST_CODE_IMAGE_PICKER:
                if (resultCode == RESULT_OK) {
                    imagePickerPicked(data.getStringExtra(Base64ImagePicker.IMAGE_DATA));
                } else {
                    imagePickerCancelled();
                }
                break;
            case CREATE_ACCOUNT_REQUEST_CODE: //Intentional fall-through
            case LOGIN_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    refreshWebView();
                } else {
                    finish();
                }
                break;
            case PHONE_NUMBER_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    mWebViewJSManager.callJs("Babylon.Vent.trigger('patient:phoneNumberSaved')");
                }
                break;
/*            case ASK_PHONE_NUMBER_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    startQuestionActivity(false);
                }
                break;*/
        }
    }


    // Method calling via reflection from shouldOverrideUrlLoading()
    public void requestScreen(String screen) {
        Screen page = Screen.valueOf(screen);
        if (!page.isOfflineModeSupported() && !App.getInstance().isConnection()) {
            noInternetAlert();
            return;
        }

        Intent intent = null;
        int requestCode = -1;
        switch (page) {
            case login:
                intent = new Intent(this, SignInActivity.class);
                requestCode = LOGIN_REQUEST_CODE;

                //hideWebAppProgressBar();
                break;
            case createAccount:
                if (isLaunchAfterLoginWithNotification()) {
                    //refreshWebView();
                } else {
                    intent = new Intent(this, RegistrationActivity.class);
                    requestCode = CREATE_ACCOUNT_REQUEST_CODE;
                }

                //hideWebAppProgressBar();
                break;
            case ask:
                if (SyncUtils.isLoggedIn()) {
                    //openAskQuestionScreen();
                    AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_ASK_CLICK);
                } else {
                    notLoggedInAction();
                }
                break;
            case ask_history:
                if (SyncUtils.isLoggedIn()) {
                    intent = new Intent(this, BaseActivity.class);
                    intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, page.name());
                } else {
                    notLoggedInAction();
                }
                break;
            case monitorMe:
                if (SyncUtils.isLoggedIn()) {
                    if (App.getInstance().isConnection()) {
                        intent = new Intent(this, BaseActivity.class);
                        intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.MONITOR_ME_FRAGMENT_TAG);
                        AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ON_MONITOR_CLICK);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.offline_error), Toast.LENGTH_LONG).show();
                    }
                } else {
                    notLoggedInAction();
                }
                break;
            case resetPassword:
                intent = new Intent(this, ResetPasswordActivity.class);
                requestCode = RESET_PASSWORD_REQUEST_CODE;
                break;
            case loggedOut:
                loggedOut();
                break;
            case terms:
                intent = new Intent(this, TermsAndConditionsActivity.class);
                break;
            case replayPhoneConsultation:
                //TODO: Display audio playback dialog
                Log.d("MainActivity", "replayPhoneConsultation");
                String link = "http://staging.babylon.alliants.co" + ".uk/public_audio/4c7ff6d9-dddd-4dcf-a50e-0609df4b4194/open-uri20140909-23086-xnfsxx";
                intent = new Intent(this, AudioPlaybackDialogActivity.class);
                intent.putExtra("audio_url_extra", link);
                break;
            case selectPractice:
                Log.d("MainActivity", "selectPractice");
                intent = new Intent(this, SelectPracticeActivity.class);
                requestCode = SELECT_PRACTICE_REQUEST_CODE;
                break;
            case subscriptions:
                intent = new Intent(this, RecordTransactionActivity.class);
                intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.SUBSCRIPTIONS_FRAGMENT_TAG);
                break;
            case billings:
                intent = new Intent(this, RecordTransactionActivity.class);
                intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.BILLINGS_FRAGMENT_TAG);
                break;
            case paymentDetails:
                intent = new Intent(this, PaymentCardsListActivity.class);
                break;
            case familyAccounts:
                if (App.getInstance().isConnection()) {
                    intent = new Intent(this, FamilyAccountsActivity.class);
                    requestCode = FAMILY_ACCOUNTS_REQUEST_CODE;
                }
                break;
            case measureMe:
                intent = new Intent(this, ShopCategoriesActivity.class);
                requestCode = KIT_CATEGORIES_REQUEST_CODE;
                break;

            case showAddFamilyAccountScreen:
                showAddFamilyAccountScreen();
            default:
                L.d(TAG, "Unknown screen");
        }
        if (intent != null) {

            startActivityForResult(intent, requestCode);

        }
    }


    //CRASH ON AUDIO PLAYBACK -> Need to check where expiry comes from

    // Method called via reflection from shouldOverrideUrlLoading()
    public void requestAudioReplay(String url)
    {
        AudioRecordingGet getRequest = new AudioRecordingGet(url);
        getRequest.execute(HomePageBackboneActivity.this, new RequestListener<AudioRecordingInfo>() {
            @Override
            public void onRequestComplete(Response<AudioRecordingInfo> response)
            {
                final AudioRecordingInfo info = response.getData();
                HomePageBackboneActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Date now = new Date();
                        if (now.compareTo(info.getExpiry()) < 0) {
                            Intent intent = new Intent(HomePageBackboneActivity.this, AudioPlaybackDialogActivity.class);
                            intent.putExtra("audio_url_extra", info.getUrl());
                            startActivityForResult(intent, -1);
                        } else {
                            String errorText = getString(R.string.audio_recording_expired_message);
                            Toast.makeText(HomePageBackboneActivity.this, errorText, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void nativeConfirm(String message) {
        message = message.replaceAll("/n", "\n");
        AlertDialogHelper.getInstance().showDefaultSimpleConfirmationDialog(this, message, new AlertDialogHelper
                .ConfirmationDialogListener() {
            @Override
            public void confirm() {
                mWebViewJSManager.callJs("Babylon.Vent.trigger('nativeConfirmCallback', true);");
            }

            @Override
            public void cancel() {
                mWebViewJSManager.callJs("Babylon.Vent.trigger('nativeConfirmCallback', false);");
            }
        });
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void nativeLog(String message) {
        L.d(TAG, message);
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void specialistSelect(JSONArray jsonArraySpecialist)
    {
        /*//Mix Panel event for booking specialist appointment
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(HomePageBackboneActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.SPECIALIST_BOOKED);*/

        if (jsonArraySpecialist != null && jsonArraySpecialist.length() != 0) {
            HomePageBackboneActivity.OnSpecialistChoiceListener onSpecialistChoiceListener = new HomePageBackboneActivity.OnSpecialistChoiceListener() {
                @Override
                public void onSpecialistChoiceClick(int specialistId) {
                    mWebViewJSManager.callJs("Babylon.Vent.trigger('specialistPicked',"
                            + String.valueOf(specialistId) + ")");
                }
            };
            AlertDialogHelper.getInstance().showSpecialistAlert(
                    this, jsonArraySpecialist, onSpecialistChoiceListener);
        }
    }

    // Method calling via reflection from shouldOverrideUrlLoading()
    public void consultantSelect(JSONArray jsonArrayConsultant) {
        if (jsonArrayConsultant != null && jsonArrayConsultant.length() != 0) {
            HomePageBackboneActivity.OnConsultantChoiceListener onConsultantChoiceListener = new HomePageBackboneActivity.OnConsultantChoiceListener() {
                @Override
                public void onConsultantChoiceClick(int specialistId) {
                    mWebViewJSManager.callJs("Babylon.Vent.trigger('consultantPicked',"
                            + String.valueOf(specialistId) + ")");
                }
            };
            AlertDialogHelper.getInstance().showConsultantAlert(
                    this, jsonArrayConsultant, onConsultantChoiceListener);
        }
    }

    // Method called via reflection from shouldOverrideUrlLoading()
    public void confirmAppointment(JSONObject data) {
        appointment = new Gson().fromJson(data.toString(), Appointment.class);
        startAppointmentPaymentProcess();
    }

    // Method called via reflection from shouldOverrideUrlLoading()
    public void payForAppointment(JSONObject data) {
        appointment = new Gson().fromJson(data.toString(), Appointment.class);
        if (appointment.getGpConsent()) {
            startAppointmentPaymentProcess();
        } else {
            AlertDialogHelper.getInstance().showGpConsentDialog(this, gpConsentConfirmationListener);
        }
    }

    private AlertDialogHelper.ConfirmationDialogListener gpConsentConfirmationListener = new AlertDialogHelper.ConfirmationDialogListener() {
        @Override
        public void confirm() {
            appointment.setGpConsent(true);
            new AppointmentPatch().withEntry(appointment, new String[] {"gp_consent"}).execute(HomePageBackboneActivity.this, appointmentPatchRequestListener);
        }
        @Override
        public void cancel() {
            startAppointmentPaymentProcess();
        }
    };

    private RequestListener<Appointment> appointmentPatchRequestListener = new RequestListenerErrorProcessed<Appointment>() {
        @Override
        public void onRequestComplete(Response<Appointment> response, String errorMessage) {
            if (!response.isSuccess()) {
                AlertDialogHelper.getInstance().showMessage(HomePageBackboneActivity.this, response.getUserResponseError());
            }
            startAppointmentPaymentProcess();
        }
    };

    // Method called via reflection from shouldOverrideUrlLoading()
    public void familySwitchedToOwner() {
        Patient patient = App.getInstance().getPatient();
        patient.setSwitchedToFamilyAccountId(-1);
    }

    private void noInternetAlert() {
        //AlertDialogHelper.getInstance().showMessage(HomePageBackboneActivity.this, R.string.offline_error, this);
    }

    private boolean isLaunchAfterLoginWithNotification()
    {
        Bundle extras = getIntent().getExtras();
        boolean isLaunchAfterLoginAfterNotification = false;
        if (extras != null) {
            isLaunchAfterLoginAfterNotification = extras.getBoolean(com.babylon.Keys
                            .IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION,
                    false
            );
        }
        return isLaunchAfterLoginAfterNotification;
    }

    private void notLoggedInAction() {
        requestScreen(Screen.createAccount.toString());
    }

    public void loggedOut() {
        getIntent().putExtra(com.babylon.Keys
                .IS_LAUNCH_FROM_LOGIN_AFTER_NOTIFICATION, false);

        PreferencesManager.getInstance().setPatientAddress(null);

        SyncUtils.signOut();

        //setVisibleTestButtons(false);

        //startIntent(new Intent(this, RegistrationActivity.class), CREATE_ACCOUNT_REQUEST_CODE);
    }

    private void startAppointmentPaymentProcess() {
        final OrderDetail orderDetail = new OrderDetail(getString(R.string.appointment_order_details),
                appointment.getPrice());
        AppointmentTransactionActivity.startActivity(this, orderDetail, appointment);
    }

    public void showAddFamilyAccountScreen()
    {
        isFamilyAccountClicked = true;
        startActivity(new Intent(HomePageBackboneActivity.this,FamilyAccountsActivity.class));
    }

    //Show date time pickers
    public void showDateTimePicker(Integer timeInterval, boolean allowPast, boolean allowFuture) {
        Date date = new Date(timeInterval * 1000L);
        Date now = new Date();

        TimePickerDialog tpd = new TimePickerDialog(HomePageBackboneActivity.this, date, new TimePickerDialog.TimePickerDialogHandler()
        {
            @Override
            public void onDateTimePicked(Date date)
            {
                if(date.before(new Date()))
                {
                    new AlertDialog.Builder(HomePageBackboneActivity.this)
                            .setTitle("Attention")
                            .setMessage("You are trying to book an appointment in the past. Please select a valid date and time.")
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which)
                                        {
                                            dialog.dismiss();
                                        }
                                    })
                            .setCancelable(false)
                            .show();
                }
                else {
                    Long unixTime = date.getTime() / 1000L;
                    mWebViewJSManager.callJs("Babylon.Vent.trigger('dateTimePicked', " + unixTime + ")");
                }
            }
        });
        if (!allowPast) {
            tpd.setMinDate(now);
        }
        if (!allowFuture) {
            tpd.setMaxDate(now);
        }
    }

    public void showFutureDateTimePicker(Integer timeInterval) {
        showDateTimePicker(timeInterval, true, true);
    }

    public void showFutureDateTimePicker(String timeIntervalString) {
        try {
            Integer timeInterval = Integer.parseInt(timeIntervalString);
            showFutureDateTimePicker(timeInterval);
            //showDateTimePicker(timeInterval, true, true);
        } catch(NumberFormatException e) {
            //Ignoring exception
        }
    }

    public void showPastDateTimePicker(Integer timeInterval) {
        showDateTimePicker(timeInterval, true, false);
    }

    public void showPastDateTimePicker(String timeIntervalString) {
        try {
            Integer timeInterval = Integer.parseInt(timeIntervalString);
            showPastDateTimePicker(timeInterval);
        } catch(NumberFormatException e) {
            //Ignoring exception
        }
    }

    public void showDateTimePicker(String timeInterval) {
        try {
            Integer time = Integer.valueOf(timeInterval);
            showDateTimePicker(time, true, true);
        } catch (NumberFormatException e) {
            L.e(TAG, e.toString(), e);
        }

    }

    public interface OnSpecialistChoiceListener {
        void onSpecialistChoiceClick(int specialistId);
    }

    public interface OnConsultantChoiceListener {
        void onConsultantChoiceClick(int consultantId);

    }
}
