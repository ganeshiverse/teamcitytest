package com.babylon.model.payments;

import com.babylon.model.BaseModel;
import com.babylon.model.Patient;
import com.google.gson.annotations.SerializedName;

public class Plan extends BaseModel implements Comparable<Plan> {
    @SkipFieldInContentValues
    private static final long serialVersionUID = 917L;

    private Integer id;
    private String title;
    private String subtitle;
    private Double price;
    @SerializedName("minor_price")
    private Double minorPrice;
    @SerializedName("gp_appointment_price")
    private Double gpAppointmentPrice;
    @SerializedName("specialist_appointment_price")
    private Double specialistAppointmentPrice;
    @SerializedName("is_default")
    private Boolean isDefault;
    @SerializedName("order_index")
    private Integer orderIndex;


    public boolean isFree() {
        return Double.compare(price, 0) == 0;
    }

    public boolean isFreeForPatient(Patient patient) {
        Double priceToCheck = patient.getIsMinor() ? minorPrice : price;
        return Double.compare(priceToCheck, 0) == 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Double getPrice() {
        return price;
    }

    public Double getPriceForPatient(Patient patient) { return (patient != null && patient.getIsMinor()) ?  minorPrice : price; }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMinorPrice() {
        return minorPrice;
    }

    public void setMinorPrice(Double minorPrice) {
        this.minorPrice = minorPrice;
    }

    public Double getGpAppointmentPrice() {
        return gpAppointmentPrice;
    }

    public void setGpAppointmentPrice(Double gpAppointmentPrice) {
        this.gpAppointmentPrice = gpAppointmentPrice;
    }

    public Double getSpecialistAppointmentPrice() {
        return specialistAppointmentPrice;
    }

    public void setSpecialistAppointmentPrice(Double specialistAppointmentPrice) {
        this.specialistAppointmentPrice = specialistAppointmentPrice;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public int compareTo( Plan p ) {
        return p.getOrderIndex().compareTo(this.getOrderIndex());
    }
}
