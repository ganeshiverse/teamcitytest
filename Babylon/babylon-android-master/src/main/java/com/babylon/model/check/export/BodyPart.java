package com.babylon.model.check.export;

import java.util.List;

public class BodyPart extends BaseExportModel {
    private int parentId;
    private String name;
    private int gender;
    private int sort;
    private boolean useParentSymptoms;
    private List<BodyPart> childrenList;

    public int getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public int getGender() {
        return gender;
    }

    public int getSort() {
        return sort;
    }

    public List<BodyPart> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<BodyPart> childrenList) {
        this.childrenList = childrenList;
    }

    public boolean isUseParentSymptoms() {
        return useParentSymptoms;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BodyPart{" +
                "name='" + name + '\'' +
                '}';
    }
}


