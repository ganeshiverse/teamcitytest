package com.iversecomics.client.store;

import android.text.TextUtils;

import com.iversecomics.client.store.model.Comic;

public class Price {
    static public String getComicPrice(final Comic comic) {
        String amount = comic.getAmountUSD();
        if (TextUtils.isEmpty(amount)) {
            switch (comic.getPricingTier()) {
                case 1:
                    amount = "0.99";
                    break;
                case 2:
                    amount = "1.99";
                    break;
                case 3:
                    amount = "2.99";
                    break;
                case 4:
                    amount = "3.99";
                    break;
                case 5:
                    amount = "4.99";
                    break;
                case 6:
                    amount = "5.99";
                    break;
                case 7:
                    amount = "6.99";
                    break;
                case 8:
                    amount = "7.99";
                    break;
                case 9:
                    amount = "8.99";
                    break;
                default:
                    amount = "ERROR";
                    break;
            }

        }
        return amount;
    }
}
