package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Appointment;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.JsonObject;
import org.apache.http.message.BasicHeader;

;

public class PaymentAppointmentPost extends AbsRequestPost<Long, Appointment>  {

    private static final String KEY_CREDIT_CARD_ID = "credit_card_id";
    private static final String KEY_PATIENT_ID = "patient_id";
    private static final String NO_CARD = "no_card";
    private long appointmentId;
    private long patientId;
    private boolean noCard;

    public static PaymentAppointmentPost getPaymentAppointmentPost(long creditCardId, long appointmentId, long patientId) {

        PaymentAppointmentPost paymentAppointmentPost =  new PaymentAppointmentPost();
        paymentAppointmentPost.appointmentId = appointmentId;
        paymentAppointmentPost.patientId = patientId;
        paymentAppointmentPost.noCard = creditCardId == -1;
        paymentAppointmentPost.withEntry(creditCardId);

        return paymentAppointmentPost;
    }

    @Override
    public AbsRequestPost<Long, Appointment> withEntry(Long creditCardId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KEY_CREDIT_CARD_ID, creditCardId);
        jsonObject.addProperty(KEY_PATIENT_ID, patientId);
        jsonObject.addProperty(NO_CARD, noCard);
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(String.format(Urls.BUY_APPOINTMENT_POST, appointmentId));
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<Appointment> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }
}
