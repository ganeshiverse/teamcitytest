package com.babylon.dialog;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class TimePickerDialogFragment extends DialogFragment {
    public static final String TAG = TimePickerDialogFragment.class.getSimpleName();
    private static final String HOURS = "HOURS";
    private static final String MINUTES = "MINUTES";

    private TimePickerDialog.OnTimeSetListener onTimeSetListener;

    public static TimePickerDialogFragment getInstance(int hours, int minutes) {
        final Bundle args = new Bundle();
        args.putInt(HOURS, hours);
        args.putInt(MINUTES, minutes);

        final TimePickerDialogFragment dialogFragment = new TimePickerDialogFragment();
        dialogFragment.setArguments(args);

        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int hours = getArguments().getInt(HOURS, 0);
        int minutes = getArguments().getInt(MINUTES, 0);

        return new com.babylon.view.TimePickerDialog(getActivity(), onTimeSetListener, hours, minutes);
    }

    public void setOnTimeSetListener(TimePickerDialog.OnTimeSetListener listener) {
        onTimeSetListener = listener;
    }

}
