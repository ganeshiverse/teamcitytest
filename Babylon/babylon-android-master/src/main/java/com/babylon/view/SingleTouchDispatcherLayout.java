package com.babylon.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class SingleTouchDispatcherLayout extends FrameLayout {
    public SingleTouchDispatcherLayout(Context context) {
        super(context);
    }

    public SingleTouchDispatcherLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SingleTouchDispatcherLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        return ev.getActionIndex() == 0 && super.dispatchTouchEvent(ev);
    }

}
