package com.iversecomics.client.store;

public class ComicStoreException extends Exception {
    private static final long serialVersionUID = -4972546169642145510L;

    public ComicStoreException(String detailMessage) {
        super(detailMessage);
    }

    public ComicStoreException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ComicStoreException(Throwable throwable) {
        super(throwable);
    }
}
