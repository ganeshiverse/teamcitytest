#!/bin/bash

function emuCmd()
{
    EMUCMD="$@"
    echo "[adb shell]\$ $EMUCMD"
    adb shell $EMUCMD
    echo ""
}

emuCmd rm /data/data/com.iversecomics.client/databases/downloads.db

