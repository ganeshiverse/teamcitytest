package com.babylon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification
{
    @SerializedName("id")
    @Expose
    private String id;

    public String getId()
    {
        return id;
    }

    @SerializedName("item_id")
    @Expose
    private String itemId;
    public String getItemId()
    {
        return itemId;
    }


    @SerializedName("item_type")
    @Expose
    private String item_type;

    public String getItemType()
    {
        return item_type;
    }

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage()
    {
        return message;
    }

    @SerializedName("action")
    @Expose
    private String action;

    public String getAction()
    {
        return action;
    }

    @SerializedName("patient_id")
    @Expose
    private String patient_id;

    public String getPatientId()
    {
        return patient_id;
    }

    @SerializedName("image_path")
    @Expose
    private String image_path;

    public String getImagePath()
    {
        return image_path;
    }

    @SerializedName("button_title")
    @Expose
    private String button_title;

    public String getButtonTitle()
    {
        return button_title;
    }

    @SerializedName("item_details")
    @Expose
    private NotificationItemDetails item_details;

    public NotificationItemDetails getItemDetails()
    {
        return item_details;
    }
}