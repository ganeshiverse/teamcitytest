package com.iversecomics.ui;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.iversecomics.app.AppConstants;
import com.iversecomics.app.ComicApp;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.refresh.TaskCallback;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.DatabaseApi;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.store.tasks.FetchComicsByGroupTask;
import com.iversecomics.client.store.tasks.FetchComicsByPromotionTask;
import com.iversecomics.client.store.tasks.FetchComicsByPublisherTask;
import com.iversecomics.client.store.tasks.FetchComicsBySeriesTask;
import com.iversecomics.client.store.tasks.FetchComicsNewReleasesTask;
import com.iversecomics.client.store.tasks.FetchComicsTopFreeTask;
import com.iversecomics.client.store.tasks.FetchComicsTopPaidTask;
import com.iversecomics.client.store.tasks.FetchPublisherTask;
import com.iversecomics.ui.slidelist.SlideableComicListAdapter;
import com.iversecomics.ui.slidelist.SlideableListFragment;
import com.iversecomics.ui.widget.ComicReaderSeekBar;
import com.iversecomics.ui.widget.NavigationBar;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.iversecomics.ui.slidelist.SlideableListConstants.NUM_PROGRESS_STEPS;

public class ComicListFragment extends SlideableListFragment {
    public static final String ARG_PROMO_ID = "com.iverse.PROMO_ID";
    public static final String ARG_GROUP_ID = "com.iverse.GROUP_ID";
    public static final String ARG_PUBLISHER_ID = "com.iverse.PUBLISHER_ID";
    public static final String ARG_SERIES_ID = "com.iverse.SERIES_ID";
    public static final String ARG_SUPPLIER_ID = "com.iverse.SUPPLIER_ID";
    public static final String ARG_TOP_FREE = "com.iverse.TOP_FREE";
    public static final String ARG_TOP_PAID = "com.iverse.TOP_PAID";
    public static final String ARG_NEW_RELEASES = "com.iverse.NEW_RELEASES";
    public static final String ARG_TITLE = "com.iverse.TITLE";

    private String mPromotionId;
    private String mGroupId;
    private String mPublisherId;
    private String mSeriesId;
    // joliver: supplierId is the same thing as publisherId (I think)
    private String mSupplierId;
    private boolean mTopFree;
    private boolean mTopPaid;
    private boolean mNewReleases;
    private ComicListListener mListener;
    private TaskCallback mRefreshCallback;
    private View mSpinner;

    public static ComicListFragment newInstance(Promotion promo) {
        Bundle args = new Bundle();
        args.putString(ARG_PROMO_ID, String.valueOf(promo.getPromotionID()));
        args.putString(ARG_TITLE, String.valueOf(promo.getName()));
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstanceForPromotionId(int promotionId, String title) {
        Bundle args = new Bundle();
        args.putString(ARG_PROMO_ID, String.valueOf(promotionId));
        args.putString(ARG_TITLE, title);
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstance(Group group) {
        Bundle args = new Bundle();
        args.putString(ARG_GROUP_ID, group.getGroupId());
        args.putString(ARG_TITLE, group.getName());
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstanceForGroupId(int groupId, String title) {
        Bundle args = new Bundle();
        args.putString(ARG_GROUP_ID, String.valueOf(groupId));
        args.putString(ARG_TITLE, title);
        return createComicListFragment(args);
    }

    private static ComicListFragment createComicListFragment(Bundle args) {
        ComicListFragment frag = new ComicListFragment();
        frag.setArguments(args);
        return frag;
    }

    public static ComicListFragment newInstanceForPublisher(Publisher publisher) {
        Bundle args = new Bundle();
        args.putString(ARG_SUPPLIER_ID, publisher.getPublisherId());
        args.putString(ARG_TITLE, publisher.getName());
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstanceForSupplierId(int supplierId, String title) {
        Bundle args = new Bundle();
        Publisher publisher = ComicApp.getInstance().getComicStore().getDatabaseApi().getPublisher(String.valueOf(supplierId));
        if (publisher != null) {
            return newInstanceForPublisher(publisher);
        }
        args.putString(ARG_SUPPLIER_ID, String.valueOf(supplierId));
        args.putString(ARG_TITLE, title);

        return createComicListFragment(args);

    }

    public static ComicListFragment newInstanceTopFree() {
        Bundle args = new Bundle();
        args.putBoolean(ARG_TOP_FREE, true);
        args.putString(ARG_TITLE, ComicApp.getApplication().getString(com.iversecomics.archie.android.R.string.top_free));
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstanceTopPaid() {
        Bundle args = new Bundle();
        args.putBoolean(ARG_TOP_PAID, true);
        args.putString(ARG_TITLE, ComicApp.getApplication().getString(com.iversecomics.archie.android.R.string.top_paid));
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstanceNewReleases() {
        Bundle args = new Bundle();
        args.putBoolean(ARG_NEW_RELEASES, true);
        args.putString(ARG_TITLE, ComicApp.getApplication().getString(com.iversecomics.archie.android.R.string.new_releases));
        return createComicListFragment(args);
    }

    public static ComicListFragment newInstanceForSeriesId(String seriesId) {
        Bundle args = new Bundle();
        args.putString(ARG_SERIES_ID, seriesId);
        return createComicListFragment(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRefreshCallback = new TaskCallback() {
            @Override
            public void onTaskCompleted(Throwable error) {
                refreshList();
            }
        };

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)throws InflateException { // Fixed crashlytics #34 issue
        View content = inflater.inflate(com.iversecomics.archie.android.R.layout.page_comiclist, container, false);

        NavigationBar navbar = (NavigationBar) content.findViewById(com.iversecomics.archie.android.R.id.navbar);
        navbar.setNavigationListener((NavigationBar.NavigationListener) getActivity());

        mSpinner = content.findViewById(com.iversecomics.archie.android.R.id.spinner);
        mGrid = (TwoWayGridView) content.findViewById(com.iversecomics.archie.android.R.id.comic_list);
        mGrid.setVisibility(View.INVISIBLE);
        mGrid.setNumRows(mNumRows);
        mGrid.setRowHeight(getResources().getDisplayMetrics().widthPixels);
        mGrid.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                Comic comic = (Comic) view.getTag();
                mListener.onComicClicked(comic);
            }
        });

        ComicReaderSeekBar mSeekBar = (ComicReaderSeekBar) content.findViewById(com.iversecomics.archie.android.R.id.seekbar);
        mSeekBar.setMax(NUM_PROGRESS_STEPS);
        mSeekBar.setProgress(NUM_PROGRESS_STEPS);
        mSeekBar.setOnSeekBarChangeListener(this);

//        if (getArguments().containsKey(ARG_TITLE) && !StringUtils.isNullOrEmpty(getArguments().getString(ARG_TITLE))) {
//            navbar.setTitle(getArguments().getString(ARG_TITLE));
//        }

        startUpdateTask();
        return content;
    }

    private void startUpdateTask() {
        Bundle args = getArguments();
        if (args != null) {
            ComicStore store = ComicApp.getInstance().getComicStore();
            ComicApp app = ComicApp.getInstance();
            ComicStore comicStore = app.getComicStore();

            Task task = null;
            mPromotionId = args.getString(ARG_PROMO_ID);
            if (mPromotionId != null) {
                task = new FetchComicsByPromotionTask(comicStore, mPromotionId);
            }
            mGroupId = args.getString(ARG_GROUP_ID);
            if (mGroupId != null) {
                task = new FetchComicsByGroupTask(comicStore, mGroupId);
            }
            mPublisherId = args.getString(ARG_PUBLISHER_ID);
            if (mPublisherId != null) {
                task = new FetchPublisherTask(comicStore, mPublisherId);
            }
            mSeriesId = args.getString(ARG_SERIES_ID);
            if (mSeriesId != null) {
                task = new FetchComicsBySeriesTask(comicStore, mSeriesId);
            }
            mSupplierId = args.getString(ARG_SUPPLIER_ID);
            if (mSupplierId != null) {
                task = new FetchComicsByPublisherTask(comicStore, mSupplierId);
            }
            mTopFree = args.getBoolean(ARG_TOP_FREE, false);
            if (mTopFree) {
                task = new FetchComicsTopFreeTask(comicStore);
            }
            mTopPaid = args.getBoolean(ARG_TOP_PAID, false);
            if (mTopPaid) {
                task = new FetchComicsTopPaidTask(comicStore);
            }
            mNewReleases = args.getBoolean(ARG_NEW_RELEASES, false);
            if (mNewReleases) {
                task = new FetchComicsNewReleasesTask(comicStore);
            }

            if (task != null) {
                app.getTaskPool().submitIfExpired(app.getFreshness(), task, mRefreshCallback);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (ComicListListener) activity;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGrid = null;
    }

    /**
     * See <a href="http://developer.android.com/training/animation/crossfade.html"> Developer Doc</a>.*
     */
    private void crossfade() {
        mGrid.startAnimation(AnimationUtils.loadAnimation(getActivity(), com.iversecomics.archie.android.R.anim.fade_in));
        mGrid.setVisibility(View.VISIBLE);

        Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), com.iversecomics.archie.android.R.anim.fade_out);
        fadeOut.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //Do nothing
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mSpinner.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //Do nothing
            }
        });
        mSpinner.startAnimation(fadeOut);
    }

    private void refreshList() {
        if (mGrid != null) {
            crossfade();
            //mGrid.removeAllViews();
            DatabaseApi databaseApi = ComicApp.getInstance().getComicStore().getDatabaseApi();

            Cursor cursor = null;
            if (mPromotionId != null) {
                cursor = databaseApi.getCursorComicsByPromotion(mPromotionId);
            } else if (mGroupId != null) {
                cursor = databaseApi.getCursorComicsByGroupId(mGroupId);
            } else if (mSeriesId != null) {
                cursor = databaseApi.getCursorComicsBySeries(mSeriesId);
            } else if (mSupplierId != null) {
                cursor = databaseApi.getCursorComicsByPublisher(mSupplierId);
            } else if (mTopFree) {
                cursor = databaseApi.getCursorComicsTopFree();
            } else if (mTopPaid) {
                cursor = databaseApi.getCursorComicsTopPaid();
            } else if (mNewReleases) {
                cursor = databaseApi.getCursorComicsNew();
            }

            adapter = new ComicListAdapter(getActivity(), cursor);
            mGrid.setAdapter(adapter);
        }
    }

    public static interface ComicListListener {
        void onComicClicked(Comic comic);
    }

    private class ComicListAdapter extends SlideableComicListAdapter {
        Uri coverUri=null;
        boolean isTablet = false;
        public ComicListAdapter(Context context, Cursor c) {
            super(context, c, com.iversecomics.archie.android.R.layout.page_comiclist_item);
            isTablet = getResources().getBoolean(com.iversecomics.archie.android.R.bool.isTablet);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Comic comic = ComicsTable.fromCursor(cursor);
            view.setTag(comic);

            // joliver 12/23/2013 Anything bigger than LARGE covers take too long to load,
            // and besides the iOS version uses smaller images.
            if(isTablet){
                coverUri = CoverSize.TABLET_LARGE.getServerUri(comic.getImageFileName());
            }else{
                coverUri = CoverSize.LARGE.getServerUri(comic.getImageFileName());
            }
            ImageView cover = (ImageView) view.findViewById(com.iversecomics.archie.android.R.id.cover);
            cover.setImageDrawable(null);
            ImageLoader.getInstance().displayImage(coverUri.toString(), cover);

            if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                int onDemandVisibility = comic.getSubscription() ? View.VISIBLE : View.INVISIBLE;
                view.findViewById(com.iversecomics.archie.android.R.id.badgeUnlimited).setVisibility(onDemandVisibility);
            }

            // TODO: IV-81 (Backlogged), joliver: if this is an item in the recently added list, show badgeDateAdded view

            // Check comic possessionState so that we can
            // show a badge in lower right corner if comic has already been purchased or downloaded
            ImageView badgeDownloadedOrPurchased = (ImageView) view.findViewById(com.iversecomics.archie.android.R.id.badgeDownloadedOrPurchased);
            MyComicsModel.PossessionState possessionState = mMyComicsModel.isOwned(comic);

            // Show purchased-state badge in lower right if user hasn't disabled them via the Settings menu.
            if (ComicPreferences.getInstance().isShowPurchaseOverlayShown()) {
                // comic is on device and ready to read
                if (possessionState == MyComicsModel.PossessionState.AVAILABLE) {
                    badgeDownloadedOrPurchased.setImageResource(com.iversecomics.archie.android.R.drawable.downloaded);
                    badgeDownloadedOrPurchased.setVisibility(View.VISIBLE);
                }
                // comic has been purchased but the comicBundle has not been downloaded
                else if (possessionState == MyComicsModel.PossessionState.PURCHASED) {
                    badgeDownloadedOrPurchased.setImageResource(com.iversecomics.archie.android.R.drawable.purchased);
                    badgeDownloadedOrPurchased.setVisibility(View.VISIBLE);
                } else
                    badgeDownloadedOrPurchased.setVisibility(View.INVISIBLE);
            } else
                badgeDownloadedOrPurchased.setVisibility(View.INVISIBLE);

        }
    }
}
