package com.babylon.enums;

/**
 * Screens, whose calls by web app
 */

public enum Screen {
    login,
    createAccount,
    ask(true),
    ask_history(true),
    monitorMe,
    resetPassword,
    loggedOut,
    terms,
    consultant,
    record,
    replayPhoneConsultation,
    selectPractice,
    settings,
    notifications,
    payForAppointment,
    paymentDetails,
    subscriptions,
    billings,
    shop,
    appointmentTried,
    appointmentBooked,
    prescriptionReceived,
    familyAccounts,
    showAddFamilyAccountScreen,
    measureMe,
    check(true);

    private boolean isOfflineModeSupported;

    private Screen() {
        this(false);
    }

    private Screen(boolean isSupportOfflineMode) {
        this.isOfflineModeSupported = isSupportOfflineMode;
    }

    public boolean isOfflineModeSupported() {
        return isOfflineModeSupported;
    }
}
