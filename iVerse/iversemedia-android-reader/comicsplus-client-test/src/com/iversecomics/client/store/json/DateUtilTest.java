package com.iversecomics.client.store.json;

import java.util.Calendar;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.MediumTest;

public class DateUtilTest extends AndroidTestCase {
    @MediumTest
    public void testTimestampAug09() {
        long actualTimestamp = DateUtil.parseServerTimestamp("Thu, 06 Aug 2009 08:54:00 GMT");

        Calendar cal = Calendar.getInstance(DateUtil.UTC);
        cal.set(Calendar.DAY_OF_MONTH, 6);
        cal.set(Calendar.MONTH, Calendar.AUGUST);
        cal.set(Calendar.YEAR, 2009);
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 54);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long expectedTimestamp = cal.getTimeInMillis();

        assertEquals("Parse of server timestamp", expectedTimestamp, actualTimestamp);
    }
}
