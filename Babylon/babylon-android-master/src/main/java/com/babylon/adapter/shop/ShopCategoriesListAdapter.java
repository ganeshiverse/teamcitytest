package com.babylon.adapter.shop;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.model.Kit.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ShopCategoriesListAdapter extends ArrayAdapter<Category> {

    public interface OnPropertyItemClickListener {
        View.OnClickListener getOnPropertyClickListener();
    }


    private static final String TAG = ShopCategoriesListAdapter.class.getSimpleName();
    private OnPropertyItemClickListener onPropertyItemClickListener;
    public ShopCategoriesListAdapter(Context context, List<Category> categories,OnPropertyItemClickListener onPropertyItemClickListener) {
        super(context, 0, categories);
        this.onPropertyItemClickListener = onPropertyItemClickListener;
    }

    private class ViewHolder
    {
        ImageView avatarView;
        TextView catNameView;
        TextView descriptionView;
        Button listCatHelpBtn;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = View.inflate(getContext(), R.layout.list_item_categories, null);
            viewHolder.avatarView = (ImageView) convertView.findViewById(R.id.category_image_view);
            viewHolder.catNameView = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.descriptionView = (TextView) convertView.findViewById(R.id.tvDescription);
            viewHolder.listCatHelpBtn = (Button) convertView.findViewById(R.id.btnHelpCategories);


            if (onPropertyItemClickListener != null) {
                viewHolder.listCatHelpBtn.setOnClickListener(onPropertyItemClickListener
                        .getOnPropertyClickListener());
            }
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Category category = getItem(position);
         if (category.getImageUrl() != null) {
             Picasso.with(getContext()).load(category.getImageUrl()).into(viewHolder.avatarView);
         }

        viewHolder.catNameView.setText(category.getName());
        viewHolder.descriptionView.setText(category.getShortDescription());
        //category.getName().contains("kit".toLowerCase());
        boolean is_kit = category.getName().toLowerCase().contains("kit".toLowerCase());

        viewHolder.listCatHelpBtn.setVisibility(is_kit ? View.VISIBLE : View.GONE);

        return convertView;
    }




}
