package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppointmentListResponse;
import com.axsystech.excelicare.data.responses.CircleUserPermissionsDataResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/12/2015.
 */
public class CircleUserPermissionsLoader extends DataAsyncTaskLibLoader<CircleUserPermissionsDataResponse> {

    private String token;
    private String userCircleMember_ID;

    public CircleUserPermissionsLoader(Context context, String token, String userCircleMember_ID) {
        super(context);
        this.token = token;
        this.userCircleMember_ID = userCircleMember_ID;

    }

    @Override
    protected CircleUserPermissionsDataResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getUserPermissions(token, userCircleMember_ID);
    }
}
