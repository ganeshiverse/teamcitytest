package com.axsystech.excelicare.network.exceptions;

/**
 * Date: 07.05.14
 * Time: 13:47
 *
 * @author SomeswarReddy
 */
public class NoMessageShowInBaseActivityException extends Exception {

    private static final long serialVersionUID = -8505710039021273347L;

    public NoMessageShowInBaseActivityException() {
    }

    public NoMessageShowInBaseActivityException(final String detailMessage) {
        super(detailMessage);
    }

    public NoMessageShowInBaseActivityException(final String detailMessage, final Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NoMessageShowInBaseActivityException(final Throwable throwable) {
        super(throwable);
    }
}