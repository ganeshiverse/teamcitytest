package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Transaction;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import org.apache.http.message.BasicHeader;

public class TransactionListGet extends AbsRequestGet<Transaction[]> {

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.TRANSACTIONS_GET);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<Transaction[]> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }
}
