package com.iversecomics.ui.slidelist;

/**
 */
public class SlideableListConstants {
    public static final int NUM_PROGRESS_STEPS = 100;
    public static final int MAX_NUM_ROWS = 3;
    public static final int MIN_NUM_ROWS = 1;

    public static final int START_NUM_ROWS = MIN_NUM_ROWS;
}
