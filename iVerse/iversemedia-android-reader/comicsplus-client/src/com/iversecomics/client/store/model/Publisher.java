package com.iversecomics.client.store.model;

public class Publisher {
    private long    dbId     = -1;
    private String  publisherId;
    private String  name;
    private String  imageUrl;
    private String  bannerImageFileName;
    private String  logoImageFileName;
    private boolean featured = false;   // default is not featured

    public String getBannerImageFileName() {
        return bannerImageFileName;
    }

    public long getDbId() {
        return dbId;
    }

    public String getLogoImageFileName() {
        return logoImageFileName;
    }

    public String getName() {
        return name;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public boolean isFeatured() {
        return featured;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setBannerImageFileName(String bannerImageFileName) {
        this.bannerImageFileName = bannerImageFileName;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public void setLogoImageFileName(String logoImageFileName) {
        this.logoImageFileName = logoImageFileName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }
}
