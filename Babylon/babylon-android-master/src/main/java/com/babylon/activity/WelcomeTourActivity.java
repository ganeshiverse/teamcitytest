package com.babylon.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.viewpagerindicator.CirclePageIndicator;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WelcomeTourActivity extends FragmentActivity
        implements WelcomeTourFragment.WelcomeTourListener {

    private static final String[] tourImages = {
            "tour_1", "tour_2", "tour_3", "tour_4", "tour_5", "tour_6" };
    private ViewPager viewPager;
    private Map<String, Fragment> fragments = new HashMap<>();
    private int mCurrentTourPage;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_tour);

        //hide actionbar
        if(getActionBar()!=null) {
            getActionBar().hide();
        }

        viewPager = (ViewPager)findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new WelcomeTourPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.tour_page_indicator);
        indicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                mCurrentTourPage = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Tour Started MixPanel Event
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(WelcomeTourActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.TOUR_STARTED);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    private class WelcomeTourPagerAdapter extends FragmentStatePagerAdapter {
        public WelcomeTourPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            String tourImageName = tourImages[position];
            int resId = getResources().getIdentifier(tourImageName, "drawable", getPackageName());
            Fragment fragment = fragments.get(tourImageName);
            if (fragment == null) {
                fragment = WelcomeTourFragment.newInstance(resId,
                        (position == tourImages.length - 1));
                fragments.put(tourImageName, fragment);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return tourImages.length;
        }
    }

    public void onContinueClick(View view)
    {
        //Send Event to MixPanel
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(WelcomeTourActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.TOUR_COMPLETED);

        closeTour();
    }
    public void onSkipClick(View view)
    {
        //Send Event to MixPanel
        JSONObject props = new JSONObject();
        try
        {
            props.put("PAGE NUMBER",mCurrentTourPage);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Bundle fb_parameters = new Bundle();
        fb_parameters.putInt("PAGE NUMBER", mCurrentTourPage);



        MixPanelHelper helper = new MixPanelHelper(WelcomeTourActivity.this,props,fb_parameters);
        helper.sendEvent(MixPanelEventConstants.TOUR_SKIPPED);

        closeTour();
    }

    public void closeTour() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                .FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Keys.IS_RETURNING_FROM_TOUR, true);
        intent.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
        startActivity(intent);
        finish();
    }
}
