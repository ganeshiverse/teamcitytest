package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class ProxieDetails implements Serializable {

    @SerializedName("EndReasonComment")
    private String endReasonComment;

    @SerializedName("EndReason_LU")
    private String endReason_LU;

    @SerializedName("ForeName")
    private String foreName;

    @SerializedName("IsActive")
    private String isActive;

    @SerializedName("LastModifiedDate")
    private String lastModifiedDate;

    @SerializedName("LastModifiedUserID")
    private String lastModifiedUserID;

    @SerializedName("RoleType_SLU")
    private String roleType_SLU;

    @SerializedName("StartDate")
    private String startDate;

    @SerializedName("StopDate")
    private String stopDate;

    @SerializedName("SurName")
    private String surName;

    @SerializedName("URL")
    private String imageURL;

    @SerializedName("UserAuthorityType_SLU")
    private String userAuthorityType_SLU;

    @SerializedName("UserCircleMemberEmail_ID")
    private String userCircleMemberEmail_ID;

    @SerializedName("UserCircleMember_ID")
    private String userCircleMember_ID;

    @SerializedName("UserCircleName")
    private String userCircleName;

    @SerializedName("UserCircle_ID")
    private String userCircle_ID;

    @SerializedName("UserID")
    private String userID;

    private boolean isLoginUser;

    public boolean isLoginUser() {
        return isLoginUser;
    }

    public void setIsLoginUser(boolean isLoginUser) {
        this.isLoginUser = isLoginUser;
    }

    public String getEndReasonComment() {
        return endReasonComment;
    }

    public String getEndReason_LU() {
        return endReason_LU;
    }

    public String getForeName() {
        return foreName;
    }

    public String getIsActive() {
        return isActive;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getLastModifiedUserID() {
        return lastModifiedUserID;
    }

    public String getRoleType_SLU() {
        return roleType_SLU;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getStopDate() {
        return stopDate;
    }

    public String getSurName() {
        return surName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getUserAuthorityType_SLU() {
        return userAuthorityType_SLU;
    }

    public String getUserCircleMemberEmail_ID() {
        return userCircleMemberEmail_ID;
    }

    public String getUserCircleMember_ID() {
        return userCircleMember_ID;
    }

    public String getUserCircleName() {
        return userCircleName;
    }

    public String getUserCircle_ID() {
        return userCircle_ID;
    }

    public String getUserID() {
        return userID;
    }

    public void setEndReasonComment(String endReasonComment) {
        this.endReasonComment = endReasonComment;
    }

    public void setEndReason_LU(String endReason_LU) {
        this.endReason_LU = endReason_LU;
    }

    public void setForeName(String foreName) {
        this.foreName = foreName;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public void setLastModifiedUserID(String lastModifiedUserID) {
        this.lastModifiedUserID = lastModifiedUserID;
    }

    public void setRoleType_SLU(String roleType_SLU) {
        this.roleType_SLU = roleType_SLU;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setUserAuthorityType_SLU(String userAuthorityType_SLU) {
        this.userAuthorityType_SLU = userAuthorityType_SLU;
    }

    public void setUserCircleMemberEmail_ID(String userCircleMemberEmail_ID) {
        this.userCircleMemberEmail_ID = userCircleMemberEmail_ID;
    }

    public void setUserCircleMember_ID(String userCircleMember_ID) {
        this.userCircleMember_ID = userCircleMember_ID;
    }

    public void setUserCircleName(String userCircleName) {
        this.userCircleName = userCircleName;
    }

    public void setUserCircle_ID(String userCircle_ID) {
        this.userCircle_ID = userCircle_ID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return userCircleMemberEmail_ID;
    }
}