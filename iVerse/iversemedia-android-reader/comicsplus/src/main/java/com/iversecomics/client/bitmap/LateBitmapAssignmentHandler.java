package com.iversecomics.client.bitmap;

import android.os.Handler;
import android.os.Message;

public class LateBitmapAssignmentHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
        FetchRequest req = (FetchRequest) msg.obj;
        if (req.hasBitmap()) {
            req.deliverBitmap();
        }
    }
}
