package com.babylon.activity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.utils.L;
import com.babylon.view.TopBarInterface;

public class RequestSentSplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_sent_splash);

        initializeTopBar();

        setTitle(getString(R.string.request_sent_title));

        TextView view = (TextView)findViewById(R.id.request_sent_text);
        view.setMovementMethod(new ScrollingMovementMethod());
    }
}
