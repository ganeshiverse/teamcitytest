package com.babylon.utils;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;

import com.babylon.App;
import com.babylon.R;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Audio operations manager
 */
public class AudioManager {

    private static final int MAX_AUDIO_DURATION = 30 * 1000;
    private static final String TAG = AudioManager.class.getSimpleName();

    private boolean isAudioPlaying;

    public static interface OnRecordCompleteListener {
        void onRecordComplete(byte[] audioRecord);
        void onRecordFail();
    }

    private static AudioManager instance;

    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;
    public static final String AUDIO_FORMAT = ".3gp";
    public static final String APPLICATION_DIR_PATH = File.separator + App.getInstance().getString(R.string.app_name);
    public static final String AUDIO_FILE_NAME = "/record";
    public static final int AUDIO_INIT_BYTE_ARRAY_SIZE = 1024;
    private String fileName = Environment.getExternalStorageDirectory() + APPLICATION_DIR_PATH + AUDIO_FILE_NAME + AUDIO_FORMAT;

    private MediaPlayer.OnCompletionListener onCompletionListener;


    private AudioManager() {
        /* singleton */
        File applicationDir = new File(Environment.getExternalStorageDirectory() + APPLICATION_DIR_PATH);
        if (!applicationDir.exists() || !applicationDir.isDirectory()) {
            applicationDir.mkdir();
        }
    }

    public static AudioManager getInstance() {
        if (instance == null) {
            instance = new AudioManager();
        }
        return instance;
    }

    public void startRecord(final OnRecordCompleteListener onRecordCompleteListener) {
        try {
            releaseRecorder();
            File outFile = new File(fileName);
            if (outFile.exists()) {
                boolean isDelete = outFile.delete();
                if (!isDelete) {
                    return;
                }
            }
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setMaxDuration(MAX_AUDIO_DURATION);
            mediaRecorder.setOutputFile(fileName);
            mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                @Override
                public void onInfo(MediaRecorder mr, int what, int extra) {
                    if (onRecordCompleteListener != null && what == MediaRecorder
                            .MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                        stopRecord(onRecordCompleteListener);
                    }
                }
            });
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }

    }

    public void stopRecord(OnRecordCompleteListener onRecordCompleteListener) {
        if (mediaRecorder != null) {
            try {
                mediaRecorder.stop();
                if (onRecordCompleteListener != null) {
                    FileInputStream inputStream = new FileInputStream(new File(fileName));
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    byte[] initialByteArray = new byte[AUDIO_INIT_BYTE_ARRAY_SIZE];
                    while (true) {
                        int byteCount = inputStream.read(initialByteArray);
                        if (byteCount != -1) {
                            outputStream.write(initialByteArray, 0, byteCount);
                        } else {
                            break;
                        }
                    }
                    inputStream.close();
                    byte[] audioByteArray = outputStream.toByteArray();
                    onRecordCompleteListener.onRecordComplete(audioByteArray);
                }
            } catch (FileNotFoundException e) {
                L.e(TAG, e.toString(), e);
                onRecordCompleteListener.onRecordFail();
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
                onRecordCompleteListener.onRecordFail();
            } catch (IllegalStateException e) {
                L.e(TAG, e.toString(), e);
                onRecordCompleteListener.onRecordFail();
            } catch (RuntimeException e) {
                L.e(TAG, e.toString(), e);
                onRecordCompleteListener.onRecordFail();
            }
        }
    }

    public void playRecord(MediaPlayer.OnCompletionListener onCompletionListener) {
        playRecord(fileName, onCompletionListener, false);
    }

    public void playRecord(String fileName, MediaPlayer.OnCompletionListener onCompletionListener) {
        File audioFile = new File(fileName);
        if (audioFile.exists()) {
            playRecord(audioFile.getAbsolutePath(), onCompletionListener, false);
        }
    }

    public void playRecord(byte[] audioByteArray, MediaPlayer.OnCompletionListener onCompletionListener) {
        File audioFile = new File(Environment.getExternalStorageDirectory() + APPLICATION_DIR_PATH
                + AUDIO_FILE_NAME + String.valueOf(System.currentTimeMillis()) + AUDIO_FORMAT);
        try {
            boolean isCreated = audioFile.createNewFile();
            if (isCreated) {
                BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(audioFile));
                outputStream.write(audioByteArray);
                outputStream.flush();
                outputStream.close();
                playRecord(audioFile.getAbsolutePath(), onCompletionListener, true);
            }
        } catch (IOException e) {
            L.e(TAG, e.toString(), e);
        }
    }

    /**
     * @param isDeleteFile flag for delete audio file after playing
     */
    private void playRecord(final String fileName, final MediaPlayer.OnCompletionListener onCompletionListener,
                            final boolean isDeleteFile) {
        if (isAudioPlaying) {
            stopPlaying();
        }
        try {
            releasePlayer();
            mediaPlayer = new MediaPlayer();
            if (onCompletionListener != null) {
                this.onCompletionListener = onCompletionListener;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (isDeleteFile) {
                            File file = new File(fileName);
                            if (file.exists()) {
                                file.delete();
                            }
                        }
                        onCompletionListener.onCompletion(mp);
                    }
                });
            }
            mediaPlayer.setDataSource(fileName);
            mediaPlayer.prepare();
            mediaPlayer.start();
            isAudioPlaying = true;
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }
    }

    public void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            if (isAudioPlaying && onCompletionListener != null) {
                onCompletionListener.onCompletion(mediaPlayer);
            }
            isAudioPlaying = false;
        }
    }

    private void releaseRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    private void releasePlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void onDestroy() {
        releasePlayer();
        releaseRecorder();
    }
}
