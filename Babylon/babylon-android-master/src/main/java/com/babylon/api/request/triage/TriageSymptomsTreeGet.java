package com.babylon.api.request.triage;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseTree;
import com.babylon.api.request.base.Urls;
import com.babylon.enums.Gender;
import com.babylon.model.check.export.Symptom;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class TriageSymptomsTreeGet extends AbsRequestGet<ResponseTree<Symptom>> {
    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.TRIAGE_SYMPTOMS_TREE_GET);
        String endURL = TextUtils.concat(url, urlParams).toString();
        return endURL;
    }

    public static TriageSymptomsTreeGet newSymptomsTreeGet(Gender gender, int bodyPart) {
        return newSymptomsTreeGet(gender, bodyPart, -1);
    }

    public static TriageSymptomsTreeGet newSymptomsTreeGet(Gender gender, int bodyPartId, int parentId) {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (gender != null) {
            params.add(new BasicNameValuePair(PARAM_GENDER, String.valueOf(gender.ordinal())));
        }
        if (bodyPartId != -1) {
            params.add(new BasicNameValuePair(PARAM_BODY_PART_ID, String.valueOf(bodyPartId)));
        }
        if (parentId != -1) {
            params.add(new BasicNameValuePair(PARAM_PARENT_ID, String.valueOf(parentId)));
        }
        return (TriageSymptomsTreeGet) new TriageSymptomsTreeGet().withParams(params);
    }

    public static String PARAM_BODY_PART_ID = "bodyPartId";
    public static String PARAM_GENDER = "gender";
    public static String PARAM_PARENT_ID = "parentId";

}