package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 11/07/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_MESSAGES_LIST)
public class MessageListItemDBModel extends DBUtils implements Serializable {

    @DatabaseField(id = true, columnName = MESSAGE_ID)
    private String messageId;

    @DatabaseField(columnName = PANEL_ID)
    private String panelId;

    @DatabaseField(columnName = SITE_ID)
    private String siteId;

    @DatabaseField(columnName = EC_USER_ID)
    private String ecUserId;

    @DatabaseField(columnName = MSG_RECEIVED_DATE)
    private String receivedDate;

    @DatabaseField(columnName = MSG_SENT_DATE)
    private String sentDate;

    @DatabaseField(columnName = MESSAGE_BODY)
    private String messageBody;

    @DatabaseField(columnName = USER_TYPE)
    private String userType;

    @DatabaseField(columnName = PHOTO_URL)
    private String photoUrl;

    @DatabaseField(columnName = MSG_PRIORITY)
    private String priority;

    @DatabaseField(columnName = READ_STATUS)
    private String readStatus;

    @DatabaseField(columnName = MSG_SENDER)
    private String sender;

    @DatabaseField(columnName = MSG_SENT_TO)
    private String sentTo;

    @DatabaseField(columnName = MSG_SUBJECT)
    private String subject;

    @DatabaseField(columnName = MSG_ATTACHMENTS)
    private String attachments;

    @DatabaseField(columnName = MSG_HEADER)
    private String header;

    @DatabaseField(columnName = SPECIALITY)
    private String speciality;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getPanelId() {
        return panelId;
    }

    public void setPanelId(String panelId) {
        this.panelId = panelId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getEcUserId() {
        return ecUserId;
    }

    public void setEcUserId(String ecUserId) {
        this.ecUserId = ecUserId;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSentTo() {
        return sentTo;
    }

    public void setSentTo(String sentTo) {
        this.sentTo = sentTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

}
