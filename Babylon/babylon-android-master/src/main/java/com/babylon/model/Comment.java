package com.babylon.model;

public class Comment extends ServerModel {
    @SkipFieldInContentValues
    private static final long serialVersionUID = 979345683647343L;

    private String id;
    private String text;
    private Integer consultantId;
    private Integer patientId;
    private Long date;
    private String questionId;
    private Boolean isRead = false;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Integer consultantId) {
        this.consultantId = consultantId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Long getDate() {
        return getLongMills(date);
    }

    public void setDate(Long date) {
        this.date = getLongSec(date);
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean isRead() {
        return isRead;
    }

    public void setRead(Boolean isRead) {
        this.isRead = isRead;
    }
}
