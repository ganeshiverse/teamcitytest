package com.babylon.validator;

public interface FormFilledChangedListenerInterface {
    public void onFormFilledChange(boolean formIsFilled);
}
