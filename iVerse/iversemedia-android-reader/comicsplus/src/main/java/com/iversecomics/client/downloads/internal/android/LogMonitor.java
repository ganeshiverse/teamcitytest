package com.iversecomics.client.downloads.internal.android;

import android.net.Uri;

import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class LogMonitor implements DownloadMonitor {
    private static final Logger LOG = LoggerFactory.getLogger(LogMonitor.class);

    @Override
    public void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        LOG.info("downloadConnected(%s, %s, %d, %s)", uri, quote(title), totalBytes, quote(etag));
    }

    @Override
    public void downloadFailure(Uri uri, DownloadException failure) {
        LOG.warn("downloadFailure(%s, %s)", uri, failure);
    }

    @Override
    public void downloadProgress(Uri uri, long currentByte) {
        LOG.debug("downloadProgress(%s, %d)", uri, currentByte);
    }

    @Override
    public void downloadState(Uri uri, DownloadStatus state, String message) {
        LOG.info("downloadState(%s, %s, %s)", uri, state, quote(message));
    }

    private String quote(String str) {
        if (str == null) {
            return "<null>";
        }
        return "\"" + str + "\"";
    }
}
