package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by someswarreddy on 23/09/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CLIENT_URL_DETAILS)
public class ClientUrlDbModel extends DBUtils {

    @DatabaseField(id = true, columnName = COMPOSITE_PK)
    private String siteIdAndUserId;

    @DatabaseField(columnName = SITE_ID)
    private String siteId;

    @DatabaseField(columnName = SITE_NAME)
    private String siteName;

    @DatabaseField(columnName = EC_USER_ID)
    private String ecUserId;

    @DatabaseField(columnName = USER_EMAIL_ID)
    private String userEmailId;

    @DatabaseField(columnName = CLIENT_URL)
    private String clientURL;

    public String getSiteIdAndUserId() {
        return siteIdAndUserId;
    }

    public void setSiteIdAndUserId(String siteIdAndUserId) {
        this.siteIdAndUserId = siteIdAndUserId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getEcUserId() {
        return ecUserId;
    }

    public void setEcUserId(String ecUserId) {
        this.ecUserId = ecUserId;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getClientURL() {
        return clientURL;
    }

    public void setClientURL(String clientURL) {
        this.clientURL = clientURL;
    }

    @Override
    public String toString() {
        return this.siteName;
    }
}
