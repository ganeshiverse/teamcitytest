package com.babylon.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AudioRecordingInfo {
    private String url;
    private String expiry;

    public String getUrl() {
        return url;
    }

    public Date getExpiry() {
        //2014-09-10T17:02:31.015+01:00
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        try {
            Date result = fmt.parse(expiry);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
