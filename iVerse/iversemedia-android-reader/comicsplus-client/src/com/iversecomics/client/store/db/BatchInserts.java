package com.iversecomics.client.store.db;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class BatchInserts {
    private static class NotifyStat {
        public String tableName;
        public Uri    contentUris[];
        public int    insertCount;
        public int    updateCount;

        public boolean hasChanged() {
            return (insertCount > 0) || (updateCount > 0);
        }
    }

    private static final Logger     LOG        = LoggerFactory.getLogger(BatchInserts.class);
    protected static final int      BATCH_SIZE = 300;
    private Map<String, NotifyStat> notifyMap;
    private SQLiteDatabase          db;
    private int                     count;

    public BatchInserts(SQLiteDatabase db) {
        this.db = db;
        this.notifyMap = new HashMap<String, BatchInserts.NotifyStat>();
        this.count = 0;
    }

    public void end() {
        setXactionEnd();
    }

    /** Insert or update. **/
    public void insert(String tableName, ContentValues values) {
        preBatch();
        //db.insert(tableName, "", values);
        db.insertWithOnConflict(tableName, "", values, SQLiteDatabase.CONFLICT_REPLACE);
        NotifyStat stat = notifyMap.get(tableName);
        if (stat != null) {
            stat.insertCount++;
        }
        postBatch();
    }

    public void notifyContentResolvers(ContentResolver contentResolver) {
        for (NotifyStat stat : notifyMap.values()) {
            if (!stat.hasChanged()) {
                continue; // skip unchanged
            }
            for (Uri contentUri : stat.contentUris) {
                LOG.debug("Notifying of change: %s - %s", stat.tableName, contentUri);
                contentResolver.notifyChange(contentUri, null);
            }
        }
    }

    private void postBatch() {
        count++;
        if (count > BATCH_SIZE) {
            setXactionEnd();
            count = 0;
        }
    }

    private void preBatch() {
        if (!db.inTransaction()) {
            LOG.info("Starting transaction on DB");
            db.beginTransaction();
        }
    }

    public void registerContentNotifier(String tableName, Uri... contentUris) {
        NotifyStat stat = new NotifyStat();
        stat.tableName = tableName;
        stat.contentUris = contentUris;
        notifyMap.put(tableName, stat);
    }

    private void setXactionEnd() {
        if (db.inTransaction()) {
            if (count > 0) {
                LOG.info("Success transaction on DB [count=%d]", count);
                db.setTransactionSuccessful();
            }
            LOG.info("Ending transaction on DB");
            db.endTransaction();
        }
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("BatchInserts[");
        b.append("count=").append(count);
        b.append(",db.inTransaction=").append(db.inTransaction());
        b.append("]");
        return b.toString();
    }
}