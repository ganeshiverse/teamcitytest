package com.iversecomics.client.downloads.internal.net;

import android.net.Uri;
import android.text.TextUtils;

import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.FileWriteException;
import com.iversecomics.client.downloads.internal.android.LogMonitor;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * The basic Download Client.
 * <p/>
 * The role of this class is to just initiate and do the transfer of the content.
 * <p/>
 * Making a determination on retry or failure is not the responsibility of this class.<br/>
 * Updating the database is not the responsibility of this class.<br/>
 * <p/>
 * See {@link com.iversecomics.client.downloads.internal.PendingDownload} for details on how retry is determined.
 */
public class DownloadClient {
    private static final Logger LOG = LoggerFactory.getLogger(DownloadClient.class);
    private List<Header> extraHeaders = new ArrayList<Header>();
    private String userAgent;
    private DownloadMonitor monitor;
    private long currentByte;
    private long totalBytes;

    public DownloadClient() {
        this.userAgent = "DownloadClient-Impl"; // Default UA
        String configuredUserAgent = DownloaderManager.getInstance().getUserAgent();
        if (!TextUtils.isEmpty(configuredUserAgent)) {
            this.userAgent = configuredUserAgent;
        }
        this.monitor = new LogMonitor();
        this.currentByte = 0;
        this.totalBytes = -1;
    }

    public void close() {
        /* IGNORE */
    }

    public void download(Uri uri, FileOutputStream out, String title) throws DownloadException {
        InputStream in = null;
        try {
            //Create the download input stream
            URL u = new URL(uri.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.connect();

            in = c.getInputStream();

            //Determine the length of the download
            totalBytes = c.getContentLength();
            String etag = c.getHeaderField("ETag");

            //Wire up the download monitor	
            monitor.downloadConnected(uri, title, totalBytes, etag);

            //Do the download
            transfer(uri, in, out);

            //Done!
            monitor.downloadState(uri, DownloadStatus.SUCCESS, "Complete");
            LOG.debug("Download complete: %s", u.toString());
        } catch (DownloadException e) {
            LOG.error("Download exception: %s", e.toString());
            monitor.downloadFailure(uri, e);
            throw e;
        } catch (MalformedURLException e) {
            LOG.error("Download URL exception: %s", e.toString());
            DownloadException ex = new DownloadException(DownloadStatus.BAD_REQUEST, e.getMessage());
            monitor.downloadFailure(uri, ex);
            throw ex;
        } catch (IOException e) {
            LOG.error("Download I/O exception: %s", e.toString());
            DownloadException ex = new DownloadException(DownloadStatus.BAD_REQUEST, e.getMessage());
            monitor.downloadFailure(uri, ex);
            throw ex;
        }
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setEtag(String etag) {
        if (!TextUtils.isDigitsOnly(etag)) {
            extraHeaders.add(new BasicHeader("If-Match", etag));
        }
    }

    public void setMonitor(DownloadMonitor monitor) {
        this.monitor = monitor;
    }

    public void setStartRange(long resumeFrom) {
        currentByte = resumeFrom;
        extraHeaders.add(new BasicHeader("Range", "bytes=" + resumeFrom + "-"));
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    private void transfer(Uri uri, InputStream in, OutputStream out) throws DownloadException {
        int bufSize = DownloaderManager.getInstance().getBufferSize();
        byte data[] = new byte[bufSize];

        int bytesRead = -1;
        do {
            // Read a Buffer
            try {
                bytesRead = in.read(data);
            } catch (IOException e) {
                currentByte += bytesRead;
                monitor.downloadProgress(uri, currentByte);
                throw new DownloadException(DownloadStatus.HTTP_DATA_ERROR,
                        "Failed transfer: Read error on " + uri, e);
            }

            // Write Buffer
            if (bytesRead > 0) {
                try {
                    out.write(data, 0, bytesRead);
                    out.flush();
                    currentByte += bytesRead;
                    monitor.downloadProgress(uri, currentByte);
                } catch (IOException e) {
                    throw new FileWriteException("Unable to write data to file", e);
                }
            }
        } while (bytesRead != -1);
    }
}
