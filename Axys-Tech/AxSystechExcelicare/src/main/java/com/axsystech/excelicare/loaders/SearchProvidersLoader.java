package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 08.11.2015
 *
 * @author G Ajaykumar
 */
public class SearchProvidersLoader extends DataAsyncTaskLibLoader<SearchDoctorsResponse> {

    private String token;
    private String searchCriteria;
    private String pageNumber;
    private String pageSize;
    private String isFavorite;

    public SearchProvidersLoader(final Context context, String token, String searchCriteria, String pageNumber, String pageSize, String isFavorite) {
        super(context);
        this.token = token;
        this.searchCriteria = searchCriteria;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.isFavorite = isFavorite;
    }

    @Override
    protected SearchDoctorsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getDoctorsList(token, searchCriteria, pageNumber, pageSize,isFavorite);
    }
}