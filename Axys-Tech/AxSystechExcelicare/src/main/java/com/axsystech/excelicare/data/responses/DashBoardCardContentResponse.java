package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 12/08/15.
 */
public class DashBoardCardContentResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private CardContentDataObject contentDataObject;

    public CardContentDataObject getContentDataObject() {
        return contentDataObject;
    }

    public class CardContentDataObject implements Serializable {

        @SerializedName("Body")
        private ArrayList<ContentBody> contentBodyArrayList;

        public ArrayList<ContentBody> getContentBodyArrayList() {
            return contentBodyArrayList;
        }
    }

    public class ContentBody implements Serializable {

        @SerializedName("Form")
        private ArrayList<FormObject> formObjectArrayList;

        public ArrayList<FormObject> getFormObjectArrayList() {
            return formObjectArrayList;
        }
    }

    public class FormObject implements Serializable {

        @SerializedName("RecordCount")
        private int recordCount;

        @SerializedName("RecordPosition")
        private int recordPosition;

        public int getRecordCount() {
            return recordCount;
        }

        public int getRecordPosition() {
            return recordPosition;
        }
    }
}
