package com.babylon.activity;

import android.app.ActionBar;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.babylon.R;
import com.babylon.enums.Screen;
import com.babylon.fragment.*;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.babylon.view.TopBarInterface;
import com.parse.ParseAnalytics;

public class BaseActivity extends FragmentActivity implements TopBarInterface
{
    int mSectionActionBarBackgroundColor;
    private TextView mTitle;
    private View mActionBarCustomView;
    private ImageButton mActionBarBackArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        if(getActionBar() != null)
        {
            getActionBar().hide();
            initActionBar();
        }


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String fragmentTag = extras.getString(Constants.EXTRA_FRAGMENT_TYPE);
            if (fragmentTag != null) {
                if (fragmentTag.equals(Constants.MONITOR_ME_FRAGMENT_TAG)) {
                    MonitorLauncherActivity.startActivity(this);
                    finish();
                } else {
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
                    if (fragment == null) {
                        if (fragmentTag.equals(Constants.ASK_FRAGMENT_TAG)) {
                            fragment = new AskFragment();
                        } else if (fragmentTag.equals(Constants.LOCATION_TRACKING_FRAGMENT_TAG)) {
                            fragment = new DataSourcesFragment();
                        } else if (fragmentTag.equals(Screen.ask_history.name())) {
                            fragment = new AskHistoryFragment();
                        }
                        if (fragment != null) {
                            fragment.setArguments(extras);
                        }
                    }

                    if (fragment != null)
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_layout,
                                fragment, fragmentTag).commit();
                }
            }
        }
        /* Parse analytics */
        ParseAnalytics.trackAppOpened(getIntent());
    }

    protected void initializeTopBar() {
        ImageButton backButton = (ImageButton) findViewById(R.id.topbar_left_image_button);
        TextView titleTextView = (TextView) findViewById(R.id.topbar_title_text_view);
        Button rightButton = (Button) findViewById(R.id.topbar_right_text_button);
        if (backButton != null && titleTextView != null) {
            int colorTheme = getColorTheme();
            titleTextView.setTextColor(getResources().getColor(colorTheme));
            rightButton.setTextColor(getResources().getColor(colorTheme));
            switch (colorTheme) {
                case TopBarInterface.BLUE_THEME:
                    backButton.setImageResource(R.drawable.back_button_blue);
                    break;
                case TopBarInterface.GREEN_THEME:
                    backButton.setImageResource(R.drawable.back_arrow_green);
                    break;
                case TopBarInterface.ORANGE_THEME:
                    backButton.setImageResource(R.drawable.selector_orange_back_button);
                    break;
                case TopBarInterface.PURPLE_THEME:
                    backButton.setImageResource(R.drawable.ic_back_arrow_purple);
                    break;
                default:
                    L.e(BaseActivity.class.getSimpleName(),
                            "Unexpected constant for top bar color theme");
                    break;
            }

            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTopBarButtonPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
        if (fragment != null) {
            if (fragment.isBackAllowed()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setTitle(String title) {
        TextView titleTextView = (TextView) findViewById(R.id.topbar_title_text_view);
        if(titleTextView != null)
        {
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(title);
        }
    }

    public void setRightButtonEnabled(boolean isEnable, @ColorRes int textColorId) {
        Button rightButton = (Button) findViewById(R.id.topbar_right_text_button);
        rightButton.setEnabled(isEnable);
        rightButton.setTextColor(getResources().getColor(textColorId));
    }

    public void setTitle(int stringId) {
        String title = getString(stringId);
        setTitle(title);
    }

    @Override
    public void setBackButtonVisible(boolean isVisible) {
        ImageButton backButton = (ImageButton) findViewById(R.id.topbar_left_image_button);
        backButton.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    public void setRightImageButtonVisible(boolean isVisible) {
        ImageButton rightButton = (ImageButton) findViewById(R.id.topbar_right_button);
        rightButton.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    public void setSecondRightImageButtonVisible(boolean isVisible) {
        ImageButton secondRightButton = (ImageButton) findViewById(R.id.topbar_second_right_button);
        secondRightButton.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setProgressBarVisible(boolean isVisible) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.topbar_network_progress_bar);
        progressBar.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setRightImageButton(final View.OnClickListener onClickListener, int iconResourceId) {
        ImageButton rightButton = (ImageButton) findViewById(R.id.topbar_right_button);
        rightButton.setVisibility(View.VISIBLE);
        if (iconResourceId != 0) {
            rightButton.setImageResource(iconResourceId);
        }
        rightButton.setOnClickListener(onClickListener);
    }

    @Override
    public void setRightButton(final View.OnClickListener onClickListener, int stringResourceId) {
        ImageButton rightImageButton = (ImageButton) findViewById(R.id.topbar_right_button);
        Button rightTextButton = (Button) findViewById(R.id.topbar_right_text_button);
        rightImageButton.setVisibility(View.GONE);
        rightTextButton.setVisibility(View.VISIBLE);
        if (stringResourceId != 0) {
            rightTextButton.setText(stringResourceId);
        }
        rightTextButton.setOnClickListener(onClickListener);
    }

    @Override
    public void setSecondRightImageButton(final View.OnClickListener onClickListener, int iconResourceId) {
        ImageButton rightButton = (ImageButton) findViewById(R.id.topbar_second_right_button);
        rightButton.setVisibility(View.VISIBLE);
        if (iconResourceId != 0) {
            rightButton.setImageResource(iconResourceId);
        }
        rightButton.setOnClickListener(onClickListener);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }

    protected void onTopBarButtonPressed() {
        onBackPressed();
    }

    public void activeBackButton() {
        ImageButton backButton = (ImageButton) findViewById(R.id.topbar_left_image_button);
        if (backButton != null) {
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTopBarButtonPressed();
                }
            });
        }
    }

    public void startFragment(Fragment fragment) {
        startFragment(fragment, true, null, android.R.id.content);
    }

    public void startFragment(Fragment fragment, String tag) {
        startFragment(fragment, true, tag, android.R.id.content);
    }

    public void startFragment(Fragment fragment, boolean isAddToBackStack, String tag) {
        startFragment(fragment, isAddToBackStack, tag, android.R.id.content);
    }


    public void startFragment(Fragment fragment, boolean isAddToBackStack, String tag,
                              int containerResourceId) {
        FragmentTransaction fragmentTransaction;

        if (TextUtils.isEmpty(tag)) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction()
                    .add(containerResourceId, fragment);
        } else {
            fragmentTransaction = getSupportFragmentManager().beginTransaction()
                    .replace(containerResourceId, fragment, tag);
        }
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void popBackToFirstFragment() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    protected void showProgressBar() {
        final ProgressBar progressBar = new ProgressBar(this);
        final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        progressBar.setLayoutParams(lp);

        ((RelativeLayout) findViewById(R.id.base_layout)).addView(progressBar, lp);
    }

    public void initActionBar()
    {
        final ActionBar actionBar = getActionBar();
        if (actionBar == null) return;

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.activity_action_bar);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        mActionBarCustomView = actionBar.getCustomView();
        mTitle = (TextView) mActionBarCustomView.findViewById(R.id.action_bar_title);
        mActionBarBackArrow = (ImageButton) findViewById(R.id.action_bar_back_arrow);

        //Handling back pressed on arrow press
        mActionBarBackArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
    }

    public void setupActionBar(final ActionBarType actionBarType)
    {
        if (actionBarType == null) return;
        mSectionActionBarBackgroundColor =
                getResources().getColor(actionBarType.getBackgroundColourResId());
        mActionBarCustomView.setBackgroundColor(mSectionActionBarBackgroundColor);

        /*if (mActionBarLogo != null)
        {
            mActionBarLogo.setVisibility(
                    actionBarType == ActionBarType.HOME ? View.VISIBLE : View.GONE);
        }*/
        /*if (mActionBarBackArrow != null)
        {
            mActionBarBackArrow.setVisibility(
                    actionBarType == ActionBarType.HOME ? View.GONE : View.VISIBLE);
        }*/
        if (mTitle != null)
        {
            // mTitle.setCompoundDrawablesWithIntrinsicBounds(actionBarType.getIconResId(), 0, 0, 0);
            if (actionBarType.getTitleResId() != 0)
            {
                mTitle.setText(actionBarType.getTitleResId());
                mTitle.setTextColor(getResources().getColor(actionBarType.getMenuBackgroundColourResId()));
                mActionBarBackArrow.setVisibility(View.VISIBLE);
                mActionBarBackArrow.setImageDrawable(getResources().getDrawable(actionBarType.getIconResId()));


            }
            else
            {
                mTitle.setText("");
            }
        }
    }
}
