package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.data.responses.ValidateSiteResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 29/09/15.
 */
public class ValidateSiteCodeLoader extends DataAsyncTaskLibLoader<ValidateSiteResponse> {

    private String emailId;
    private String deviceId;
    private String siteId;

    public ValidateSiteCodeLoader(Context context, String emailId, String deviceId, String siteId) {
        super(context);
        this.emailId = emailId;
        this.deviceId = deviceId;
        this.siteId = siteId;
    }

    @Override
    protected ValidateSiteResponse performLoad() throws Exception {
        return ServerMethods.getInstance().validateSiteCode(emailId, deviceId, siteId);
    }
}
