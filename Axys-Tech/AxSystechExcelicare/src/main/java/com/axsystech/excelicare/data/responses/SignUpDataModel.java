package com.axsystech.excelicare.data.responses;

import java.io.Serializable;

/**
 * Created by someswar on 7/1/2015.
 */
public class SignUpDataModel implements Serializable {

    private String loginName = "";
    private String firstName = "";
    private String lastName = "";
    private int gender;
    private String dob = "";
    private String mobileNo = "";
    private String siteId = "";
    private String aadharCardNumber = "";
    private String drivingLicenseNumber = "";
    private String pincode = "";
    private String state = "";
    private String country = "";
    private String city = "";
    private String address1 = "";
    private String address2 = "";
    private String address3 = "";
    private String postCode = "";
    private String securityQues1 = "";
    private String securityQues1Answer = "";
    private String securityQues2 = "";
    private String securityQues2Answer = "";

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getAadharCardNumber() {
        return aadharCardNumber;
    }

    public void setAadharCardNumber(String aadharCardNumber) {
        this.aadharCardNumber = aadharCardNumber;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getSecurityQues1() {
        return securityQues1;
    }

    public void setSecurityQues1(String securityQues1) {
        this.securityQues1 = securityQues1;
    }

    public String getSecurityQues1Answer() {
        return securityQues1Answer;
    }

    public void setSecurityQues1Answer(String securityQues1Answer) {
        this.securityQues1Answer = securityQues1Answer;
    }

    public String getSecurityQues2() {
        return securityQues2;
    }

    public void setSecurityQues2(String securityQues2) {
        this.securityQues2 = securityQues2;
    }

    public String getSecurityQues2Answer() {
        return securityQues2Answer;
    }

    public void setSecurityQues2Answer(String securityQues2Answer) {
        this.securityQues2Answer = securityQues2Answer;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

}
