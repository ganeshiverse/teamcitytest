package com.babylon.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.babylon.App;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.L;

public class SyncFinishedBroadcast extends BroadcastReceiver {

    public static void sendBroadcast(String syncType, int syncAction) {
        final Intent i = new Intent();
        i.setAction("com.babylon.ACTION_SYNC_FINISHED");
        i.putExtra(SyncAdapter.KEY_SYNC_TYPE, syncType);
        i.putExtra(SyncAdapter.KEY_SYNC_ACTION, syncAction);
        App.getInstance().sendBroadcast(i);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String syncType = intent.getStringExtra(SyncAdapter.KEY_SYNC_TYPE);
        final int syncAction = intent.getIntExtra(SyncAdapter.KEY_SYNC_ACTION, 0);
        L.d("SyncFinishedBroadcast", syncType + " " + String.valueOf(syncAction)  );
    }
}
