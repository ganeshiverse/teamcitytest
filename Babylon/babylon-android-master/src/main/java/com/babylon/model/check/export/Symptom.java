package com.babylon.model.check.export;


public class Symptom extends BaseExportModel {

    private String name;
    private int gender;
    private boolean selectBox;
    private int sort;
    private int parentId;

    //childrenCount - additional field which set from db query when calling Symptom.fromCursor()
    private int childrenCount = 0;

    public int getChildrenCount() {
        return childrenCount;
    }

    private Integer bodyPartId;

    private Symptom[] childrenList;

    public String getName() {
        return name;
    }

    public int getGender() {
        return gender;
    }

    public boolean isSelectBox() {
        return selectBox;
    }

    public int getSort() {
        return sort;
    }

    public int getParentId() {
        return parentId;
    }

    public int getBodyPartId() {
        return getInt(bodyPartId);
    }

    public Symptom[] getChildrenList() {
        return childrenList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setSelectBox(boolean selectBox) {
        this.selectBox = selectBox;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setBodyPartId(int bodyPartId) {
        this.bodyPartId = bodyPartId;
    }

    public void setChildrenList(Symptom[] childrenList) {
        this.childrenList = childrenList;
    }
}
