package com.babylon.api.request.base;

import java.util.ArrayList;
import java.util.List;

public class ResponseList<T> {
    private Integer count;
    private List<T> list;

    public Integer getCount() {
        return count;
    }

    public List<T> getList() {
        return list == null ? list = new ArrayList<T>() : list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}