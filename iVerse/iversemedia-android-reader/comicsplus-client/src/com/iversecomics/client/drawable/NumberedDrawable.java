package com.iversecomics.client.drawable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;

import com.iversecomics.client.bitmap.ImageRect;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class NumberedDrawable extends Drawable {
    @SuppressWarnings("unused")
    private static final Logger LOG            = LoggerFactory.getLogger(NumberedDrawable.class);
    private static final int    BGCOLOR_TOP    = Color.parseColor("#dcdcdc");
    private static final int    BGCOLOR_BOTTOM = Color.parseColor("#7c7cac");
    private int                 number;
    private Paint               background;
    private Paint               border;
    private LinearGradient      bgGradient;
    private TextPaint           paint;
    private Path                clip;
    private ImageRect           imageBounds;
    private float               cornerRadius;
    private boolean             enableCorners  = false;
    private boolean             enableBorder   = true;

    public NumberedDrawable(int num) {
        this(num, false);
    }

    public NumberedDrawable(int num, boolean inEditMode) {
        this.number = num;

        enableCorners = !inEditMode;
        enableBorder = !inEditMode;

        this.background = new Paint();
        this.background.setStyle(Paint.Style.FILL);
        bgGradient = new LinearGradient(0F, 0F, 100F, 100F, BGCOLOR_TOP, BGCOLOR_BOTTOM, TileMode.MIRROR);
        this.background.setShader(bgGradient);

        this.border = new Paint();
        this.border.setStyle(Paint.Style.STROKE);
        this.border.setColor(Color.parseColor("#88bbbbbb"));
        this.border.setStrokeWidth(5F);

        this.paint = new TextPaint();
        this.paint.setAntiAlias(true);
        this.paint.setColor(Color.WHITE);
        this.paint.setTextAlign(Paint.Align.CENTER);
        this.paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
    }

    @Override
    public void draw(Canvas canvas) {
        // Inset
        ImageRect inset = imageBounds.insetRectF(-1F);

        // Clip Rect
        if (enableCorners) {
            canvas.clipPath(clip);
        }

        // Background
        canvas.drawRect(imageBounds, background);

        // Border
        if (enableBorder) {
            if (enableCorners) {
                canvas.drawRoundRect(inset, cornerRadius, cornerRadius, border);
            } else {
                canvas.drawRect(inset, border);
            }
        }

        // The Font
        float fontSize = imageBounds.height() * .90F;
        this.paint.setTextSize(fontSize);
        canvas.drawText(Integer.toString(number), imageBounds.midX(), imageBounds.midY() + (fontSize * .35F), paint);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

    public boolean isEnableBorder() {
        return enableBorder;
    }

    public boolean isEnableCorners() {
        return enableCorners;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bgGradient = new LinearGradient(0F, 0F, 0F, bounds.height(), BGCOLOR_TOP, BGCOLOR_BOTTOM, TileMode.MIRROR);
        background.setShader(bgGradient);

        imageBounds = new ImageRect(bounds);
        cornerRadius = imageBounds.width() / 10F;

        if (enableCorners) {
            clip = new Path();
            ImageRect inset = imageBounds.insetRectF(-1F);
            clip.addRoundRect(inset, cornerRadius, cornerRadius, Direction.CW);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        /* ignore */
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        /* ignore */
    }

    public void setEnableBorder(boolean enableBorder) {
        this.enableBorder = enableBorder;
    }

    public void setEnableCorners(boolean enableCorners) {
        this.enableCorners = enableCorners;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NumberedDrawable [number=");
        builder.append(number);
        builder.append(", imageBounds=");
        builder.append(imageBounds);
        builder.append("]");
        return builder.toString();
    }
}
