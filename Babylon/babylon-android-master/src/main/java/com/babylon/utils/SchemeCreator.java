package com.babylon.utils;

import android.text.TextUtils;

import com.babylon.model.ServerModel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class SchemeCreator {

    private static final String TAG = "SchemeCreator";

    protected SchemeCreator() {
        //not called
    }

    static class Sorter implements Comparator<Field> {
        private List<String> sortRule = new LinkedList<String>();

        Sorter() {
            sortRule.add("id");
            sortRule.add("syncWithServer");

            Collections.reverse(sortRule);
        }

        @Override
        public int compare(Field one, Field two) {
            int onePos = sortRule.indexOf(one.getName());
            int twoPos = sortRule.indexOf(two.getName());

            if (onePos > twoPos) {
                return -1;
            } else if (onePos == twoPos) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    static List<Field> readAllFields(Class cls, List<Field> fields) {
        fields.addAll(Arrays.asList(cls.getDeclaredFields()));
        Class superCls = cls.getSuperclass();
        if (superCls != null) {
            readAllFields(superCls, fields);
        }
        return fields;
    }

    public static String create(Class<?> modelCls) {

        String template = "public interface @ENTITY_NAME@Schema {\n    String CUSTOM_SQL = \", UNIQUE (\" + Columns.ID.getName() + \") ON CONFLICT REPLACE\";\n    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath(\"entity\").appendPath(Table.@TABLE_NAME@.getName()).build();\n\n    public enum Columns {\n        @COLUMNS_ENUM@\n\n        private String columnName;\n        private DBType type;\n        \n        Columns(String columnName, DBType type) {\n            this.columnName = columnName;\n            this.type = type;\n        }\n\n        public String getName() {\n            return columnName;\n        }\n        \n        public DBType getType() {\n            return type;\n        }\n        \n        public String toString() {\n            return this.getName();\n        }\n    }\n    \n    public interface Query {\n        int TOKEN_QUERY = 80;\n\n        String[] PROJECTION = { \n                Table.@TABLE_NAME@.getName() + \".\"+\n                @COLUMNS_PROJECTION@\n                };\n\n        @COLUMNS_NUMBER@\n    }\n}";

        StringBuilder enumStringBuilder = new StringBuilder();
        StringBuilder projectionStringBuilder = new StringBuilder();
        StringBuilder columnsNumbersStringBuilder = new StringBuilder();

        List<Field> fields = new ArrayList<Field>();
        readAllFields(modelCls, fields);

        Collections.sort(fields, new Sorter());

        String entityName = modelCls.getSimpleName();
        String tableName = modelCls.getSimpleName().toUpperCase(Locale.getDefault());


        int count = addRow(enumStringBuilder, projectionStringBuilder, columnsNumbersStringBuilder, 0, "DBType.PRIMARY", "_id", "_ID");

        for (Field field : fields) {
            String dbType = null;
            try {
                if (field.isAnnotationPresent(ServerModel.SkipFieldInContentValues.class)) {
                    continue;
                }
                String simpleName = field.getType().getSimpleName();
//                Class<?> cls = Class.forName(canonicalName);
//                FieldType key = FieldType.fromClass(cls);
                FieldType key = FieldType.fromClassName(simpleName, field.getType());
                if (key != null) {
                    switch (key) {
                        case INTEGER:
                            dbType = "DBType.INT";
                            break;
                        case STRING:
                            dbType = "DBType.TEXT";
                            break;
                        case LONG:
                            dbType = "DBType.NUMERIC";
                            break;
                        case FLOAT:
                            dbType = "DBType.FLOAT";
                            break;
                        case DOUBLE:
                            dbType = "DBType.FLOAT";
                            break;
                        case BOOLEAN:
                            dbType = "DBType.INT";
                            break;
                        case BLOB:
                            dbType = "DBType.BLOB";
                            break;
                        case SERILAIZABLE:
                            dbType = "DBType.TEXT";
                            break;
                        default:
                            continue;
                    }
                }
                field.setAccessible(true);
                String name = field.getName();
                String nameUpperCase = splitByUpperCase(name).toUpperCase(Locale.getDefault());

                if (key != null) {
                    count = addRow(enumStringBuilder, projectionStringBuilder, columnsNumbersStringBuilder, count, dbType, name, nameUpperCase);
                } else {
                    L.e(TAG, field.getType().getCanonicalName());
                }
            } catch (IllegalArgumentException e) {
                L.e(TAG, e.getMessage(), e);
            }

        }
        projectionStringBuilder.setLength(projectionStringBuilder.length() - 2);
        enumStringBuilder.setLength(enumStringBuilder.length() - 2);
        enumStringBuilder.append(";\n");

        template = template.replaceAll("@TABLE_NAME@", tableName);
        template = template.replaceAll("@ENTITY_NAME@", entityName);
        template = template.replaceAll("@COLUMNS_PROJECTION@", projectionStringBuilder.toString());
        template = template.replaceAll("@COLUMNS_ENUM@", enumStringBuilder.toString());
        template = template.replaceAll("@COLUMNS_NUMBER@", columnsNumbersStringBuilder.toString());

        return template;
    }

    private static int addRow(StringBuilder enumStringBuilder, StringBuilder projectionStringBuilder, StringBuilder columnsNumbersStringBuilder, int count, String dbType, String name, String nameUpperCase) {
        enumStringBuilder.append(nameUpperCase + "(\"" + name + "\", " + dbType + "),\n");
        projectionStringBuilder.append("Columns." + nameUpperCase + ".getName(),\n");
        columnsNumbersStringBuilder.append("int " + nameUpperCase + " = " + count++ + ";\n");
        return count;
    }

    public static String splitByUpperCase(String string) {
        String[] stringArray = string.split("(?=\\p{Lu})");
        String newStringName = TextUtils.join("_", stringArray);
        return newStringName;
    }


}
