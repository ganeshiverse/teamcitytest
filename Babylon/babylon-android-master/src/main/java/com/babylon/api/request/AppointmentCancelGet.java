package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Appointment;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class AppointmentCancelGet extends AbsRequestGet<Appointment> {

    private String appointmentId;

    public AppointmentCancelGet(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        String url = Urls.getRuby(Urls.APPOINTMENTS);
        return TextUtils.concat(url, "/", appointmentId, "/cancel").toString();
    }

    @Override
    protected AbsResponseProcessor<Appointment> getResponseProcessor() {
        return new RubyResponseProcessor<Appointment>(getOutputType());
    }


}
