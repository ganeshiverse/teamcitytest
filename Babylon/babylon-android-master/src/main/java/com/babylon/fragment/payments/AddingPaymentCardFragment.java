package com.babylon.fragment.payments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.controllers.OnDateSetListener;
import com.babylon.controllers.payments.ExpiryDateViewController;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.payments.NewPaymentCard;
import com.babylon.utils.*;
import com.babylon.validator.ValidationController;
import com.babylon.view.CustomEditText;
import com.babylon.view.TopBarInterface;
import com.babylon.view.TypefaceEditText;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddingPaymentCardFragment extends BaseFragment {
    public static final String TAG = AddingPaymentCardFragment.class.getSimpleName();
    public static final int SCAN_CARD_REQUEST_CODE = 42001;

    private OnCreatingPaymentCardClickListener onAddingPaymentCardListener;

    private ExpiryDateViewController expiryDateViewController;
    private CustomEditText cardNumberInputView;

    private ValidationController validationController = new ValidationController();
    private CustomEditText cvvInputView;
    private TypefaceEditText expiryDateEditText;
    private Button btnScanCard;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onAddingPaymentCardListener = (OnCreatingPaymentCardClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAddingPaymentCardClickListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_adding_payment_card, container, false);
    }

    @Override
    public void onStop()
    {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(cvvInputView.getWindowToken(), 0);
        super.onStop();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(R.string.add_payment_card_title);
        final TextView birthDateTextView = (TextView) view.findViewById(R.id
                .payment_data_text_view);

        expiryDateViewController = new ExpiryDateViewController(getActivity(),
                birthDateTextView, onExpiryDateSet, null);

        cardNumberInputView = (CustomEditText) getView().findViewById(R.id
                .number_data_edit_text);
        cardNumberInputView.setHintTextColor(R.color.text_color_hint);

        cvvInputView = (CustomEditText) getView().findViewById(R.id
                .due_data_text_view);
        cvvInputView.setHintTextColor(R.color.text_color_hint);

        validationController.addValidation(cardNumberInputView, null);
        validationController.addValidation(cvvInputView, null);

        cardNumberInputView.getEditText().addTextChangedListener(onCardNumberTextWatcher);
        setRightButton(onAddingCreditCardClickListener, R.string.adding_payment_card_save);

        expiryDateEditText = (TypefaceEditText)getView().findViewById(R.id.edit_text_date_error);
        expiryDateEditText.setInputType(InputType.TYPE_NULL);

        btnScanCard = (Button)getView().findViewById(R.id.scan_card_button);
        btnScanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false

                startActivityForResult(scanIntent, SCAN_CARD_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onTopBarButtonPressed()
    {
        UIUtils.hideSoftKeyboard(getActivity(), (EditText) getView().findViewById(R.id
                .amount_data_text_view));
        super.onTopBarButtonPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_CARD_REQUEST_CODE)
        {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Card number
                cardNumberInputView.setText(scanResult.getFormattedCardNumber());

                // Expiry date
                if (scanResult.isExpiryValid()) {

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, scanResult.expiryYear);
                    cal.set(Calendar.MONTH, scanResult.expiryMonth-1);
                    Date expiryDate = cal.getTime();

                    expiryDateViewController.setCurrentDate(expiryDate);
                    expiryDateViewController.setCurrentDateText(expiryDate);
                }

                // CVV
                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    cvvInputView.setText(scanResult.cvv);
                }
            }
            else {
                //Toast.makeText(App.getInstance(), "Scan was canceled.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //onAddingPaymentCardListener = null;
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    private final View.OnClickListener onAddingCreditCardClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            final String creditCardNumberText = cardNumberInputView.getText().toString().trim();
            final String cardNumber = new PaymentCardViewController().removeWhitespacesFromCreditCard(creditCardNumberText);

            if(cardNumber == null || cardNumber.length() < 16)
            {
                AlertDialogHelper.getInstance().showMessage(getActivity(), R.string.card_not_valid);
                return;
            }
            else
            {
                //Mix Panel Event for Adding card
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(getActivity(),props,null);
                helper.sendEvent(MixPanelEventConstants.CARD_ADDED);

                final String cvv = cvvInputView.getEditText().getText().toString();

                if (validationController.performValidation() && expiryDateViewController.validateDate(expiryDateEditText)) {

                    final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy", Locale.getDefault());
                    final String formattedExpiredDate = dateFormat.format(expiryDateViewController.getCurrentDate());
                    final NewPaymentCard paymentCard = new NewPaymentCard(cardNumber, cvv, formattedExpiredDate);

                    UIUtils.hideSoftKeyboard(getActivity(), cvvInputView.getEditText());
                    onAddingPaymentCardListener.onCreatingPaymentCardClick(paymentCard);
                }
            }
        }
    };

    private final TextWatcher onCardNumberTextWatcher = new TextWatcher() {

        private final int firstSpacePosition = 4;
        private final int secondSpacePosition = 9;
        private final int thirdSpacePosition = 14;

        private int before;
        private int count;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            this.before = before;
            this.count = count;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (before < count) {
                switch (s.length()) {
                    case firstSpacePosition:
                        s.insert(firstSpacePosition, " ");
                        break;
                    case secondSpacePosition:
                        s.insert(secondSpacePosition, " ");
                        break;
                    case thirdSpacePosition:
                        s.insert(thirdSpacePosition, " ");
                        break;
                    default:
                        L.d(TAG, "No auto space");
                        break;
                }
            }
        }

    };

    private final OnDateSetListener onExpiryDateSet = new OnDateSetListener() {
        @Override
        public void onDateSet(Date date) {
            expiryDateViewController.validateDate(expiryDateEditText);
        }
    };

    public interface OnCreatingPaymentCardClickListener {
        public void onCreatingPaymentCardClick(NewPaymentCard paymentCard);
    }

}
