package com.iversecomics.logging;

import java.util.Map;

public interface IAndroidLoggingInit {
    Map<String, Integer> getLogLevels();

    int getRootLevel();
}
