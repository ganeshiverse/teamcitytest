package com.iversecomics.client.downloads.internal;

import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DownloadPool {
    private static final Logger LOG = LoggerFactory.getLogger(DownloadPool.class);
    private static final String TAG = DownloadPool.class.getSimpleName();
    private LinkedBlockingQueue<Runnable> queue;
    private ThreadPoolExecutor pool;

    public DownloadPool(DownloaderManager mgr) {
        this.queue = new LinkedBlockingQueue<Runnable>();
        int maxThreads = mgr.getMaxSimultaneous();
        this.pool = new ThreadPoolExecutor(maxThreads, maxThreads, 0L, TimeUnit.MILLISECONDS, this.queue);
    }

    public void execute(PendingDownload pending) {
        LOG.debug("Executing pending download %s", pending.toString());
        synchronized (queue) {
            if (queue.contains(pending)) {
                // Already have this pending download.
                LOG.debug("Queue already contains download for %s, skipping.", pending.toString());
                return;
            }
            LOG.debug("Queue did NOT contains download for %s, executing.", pending.toString());
            pool.execute(pending);
        }
    }

    public void shutdown() {
        pool.shutdownNow();
    }
}
