package com.axsystech.excelicare.data.responses;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by SomeswarReddy on 7/8/2015.
 */
public class GetMediaItemResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private MediaItemData mediaItemData;

    public MediaItemData getMediaItemData() {
        return mediaItemData;
    }

    public class MediaItemData implements Serializable {

        @SerializedName("id")
        private long mediaId;

        @SerializedName("FileDuration")
        private String fileDuration;

        @SerializedName("LMD")
        private String lmd;

        @SerializedName("annotationMedia")
        private String annotationMedia;

        @SerializedName("comments")
        private String comments;

        @SerializedName("createdDate")
        private String createdDate;

        @SerializedName("fileName")
        private String fileName;

        @SerializedName("folderId")
        private int folderId;

        @SerializedName("subfodlerId")
        private int subfolderId;

        @SerializedName("name")
        private String name;

        @SerializedName("originalURL")
        private String originalURL;

        public void setPreviewURL(String previewURL) {
            this.previewURL = previewURL;
        }

        @SerializedName("previewURL")
        private String previewURL;

        public long getMediaId() {
            return mediaId;
        }

        public String getFileDuration() {
            return fileDuration;
        }

        public String getLmd() {
            return lmd;
        }

        public String getAnnotationMedia() {
            return annotationMedia;
        }

        public String getComments() {
            return comments;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public String getFileName() {
            return fileName;
        }

        public int getFolderId() {
            return folderId;
        }

        public int getSubfolderId() {
            return subfolderId;
        }

        public String getName() {
            return name;
        }

        public String getOriginalURL() {
            return originalURL;
        }

        public String getPreviewURL() {
            return previewURL;
        }
    }
}
