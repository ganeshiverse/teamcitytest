package com.iversecomics.client.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;


public final class NetStatus {
    private static final Logger LOG = LoggerFactory.getLogger(NetStatus.class);

    public static ConnectivityManager getConnectivityManager(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            LOG.warn("couldn't get connectivity manager");
        }
        return connectivity;
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public static boolean isAvailable(Context context) {
        ConnectivityManager conn = getConnectivityManager(context);
        if (conn == null) {
            LOG.verbose("network connectivity manager is unavailable");
            return false;
        }

        NetworkInfo[] infos = conn.getAllNetworkInfo();
        if (infos == null) {
            LOG.verbose("network information is unavailable");
            return false;
        }

        for (NetworkInfo info : infos) {
            if (info.getState() == NetworkInfo.State.CONNECTED) {
                LOG.verbose("network is available");
            }
            return true;
        }

        LOG.verbose("network is not available");
        return false;
    }

    public static boolean isRoaming(Context context) {
        ConnectivityManager conn = getConnectivityManager(context);
        if (conn == null) {
            LOG.verbose("network connectivity manager is unavailable");
            return false;
        }

        NetworkInfo info = conn.getActiveNetworkInfo();
        if (info == null) {
            LOG.verbose("network information is unavailable");
            return false;
        }

        if (info.getType() != ConnectivityManager.TYPE_MOBILE) {
            LOG.verbose("not using mobile network");
            return false;
        }

        TelephonyManager telephony = getTelephonyManager(context);
        if (telephony == null) {
            LOG.verbose("network telephone is unavailable");
            return false;
        }

        if (telephony.isNetworkRoaming()) {
            LOG.verbose("network is roaming");
            return true;
        }

        return false;
    }

    private NetStatus() {
        /* prevent instantiation */
    }
}
