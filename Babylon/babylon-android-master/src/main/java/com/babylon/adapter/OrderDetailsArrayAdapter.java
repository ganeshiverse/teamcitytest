package com.babylon.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.model.payments.OrderDetail;

import java.util.List;

public class OrderDetailsArrayAdapter extends ArrayAdapter<OrderDetail> {

    public OrderDetailsArrayAdapter(Context context, List<OrderDetail> objects) {
        super(context, R.layout.list_item_order_details, objects);
    }

    static class ViewHolder {
        protected TextView nameTextView;
        protected TextView priceTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if(convertView == null) {
            LayoutInflater inflator = ((Activity)getContext()).getLayoutInflater();
            convertView = inflator.inflate(R.layout.list_item_order_details, null);
            viewHolder = new ViewHolder();
            viewHolder.nameTextView = (TextView) convertView.findViewById(R.id.order_details_name);
            viewHolder.priceTextView = (TextView) convertView.findViewById(R.id.order_details_price);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        final OrderDetail orderDetail = getItem(position);

        viewHolder.nameTextView.setText(orderDetail.getName());

        viewHolder.priceTextView.setText(App.getInstance().getCurrentRegion().getFormattedPriceString(orderDetail.getPrice()));

        return convertView;
    }
}
