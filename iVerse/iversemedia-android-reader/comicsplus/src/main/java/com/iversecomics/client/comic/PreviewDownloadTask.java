package com.iversecomics.client.comic;

import com.iversecomics.client.refresh.Task;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;


public class PreviewDownloadTask extends Task {

    IMyComicSourceAdapter adapter;
    private static final Logger LOG = LoggerFactory.getLogger(PreviewDownloadTask.class);

    public PreviewDownloadTask(IMyComicSourceAdapter comicadapter) {
        this.adapter = comicadapter;
    }

    @Override
    public void execTask() {
        try {
            adapter.open();
        } catch (IOException e) {

            //TODO from the docs
            //getSigners() - Returns null. (On Android, a ClassLoader can load classes
            //from multiple dex files. All classes from any given dex file will have the
            //same signers, but different dex files may have different signers. This does
            //not fit well with the original ClassLoader-based model of getSigners.)
            LOG.warn(e, "Unable to open comicSource: %s", this.adapter.getClass().getSigners());
        }
    }

}
