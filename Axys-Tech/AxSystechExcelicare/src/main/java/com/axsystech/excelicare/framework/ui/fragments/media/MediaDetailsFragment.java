package com.axsystech.excelicare.framework.ui.fragments.media;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.DeleteMediaItemResponse;
import com.axsystech.excelicare.data.responses.UploadMediaItemResponse;
import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MediaDetailsActivity;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.DateTimePickerFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.models.MediaDetailsViewType;
import com.axsystech.excelicare.loaders.DeleteMediaFromServerLoader;
import com.axsystech.excelicare.loaders.DeleteMediaItemDbLoader;
import com.axsystech.excelicare.loaders.SaveMediaItemsDBLoader;
import com.axsystech.excelicare.loaders.UpdateMediaItemDBLoader;
import com.axsystech.excelicare.loaders.UploadMediaItemLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.SharedPreferenceUtil;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.axsystech.excelicare.util.views.FloatLabeledTextView;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by SomeswarReddy on 7/12/2015.
 */
public class MediaDetailsFragment extends BaseFragment implements View.OnClickListener,
        DateTimePickerFragment.DateTimeDialogListener, ConfirmationDialogFragment.OnConfirmListener {

    private String TAG = MediaDetailsFragment.class.getSimpleName();
    private MediaDetailsActivity mActivity;

    private static final int DELETE_MEDIA_DIALOG_ID = 106;

    private static final String SAVE_MEDIA_ITEM_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.SAVE_MEDIA_ITEM_LOADER";
    private static final String SAVE_AND_UPLOAD_MEDIA_ITEM_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.SAVE_AND_UPLOAD_MEDIA_ITEM_LOADER";
    private static final String SINGLE_UPLOAD_MEDIA_ITEM_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.UPLOAD_MEDIA_ITEM_LOADER";
    private static final String DELETE_MEDIA_ITEM_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.DELETE_MEDIA_ITEM_LOADER";
    private static final String DELETE_MEDIA_ITEM_FROM_SERVER_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.DELETE_MEDIA_ITEM_FROM_SERVER_LOADER";
    private static final String UPDATE_MEDIA_ITEM_IN_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.UPDATE_MEDIA_ITEM_IN_DB_LOADER";
    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.currentDateTextView)
    private TextView currentDateTextView;

    @InjectView(R.id.mediaFrameLayout)
    private FrameLayout mediaFrameLayout;

    @InjectView(R.id.mediaImageView)
    private ImageView mediaImageView;

    @InjectView(R.id.playIconImageView)
    private ImageView playIconImageView;

    @InjectView(R.id.mediaDurationtextView)
    private TextView mediaDurationtextView;


    /**/@InjectView(R.id.m_descriptionEditText)
    private EditText m_descriptionEditText;

    @InjectView(R.id.m_nameEditText)
    private  EditText m_nameEditText;

    @InjectView(R.id.dateTimeTextView)
    private TextView dateTimeTextView;
    /************/

    @InjectView(R.id.dateandtime)
    private TextView dateandtime;

    @InjectView(R.id.name)
    private TextView name;

    @InjectView(R.id.description)
    private TextView description;

    @InjectView(R.id.dateTimeTextViewnew)
    private TextView dateTimeTextViewnew;

    @InjectView(R.id.nametext)
    private TextView nametext;
    @InjectView(R.id.descriptiontext)
    private TextView descriptiontext;
    @InjectView(R.id.dateandtimetext)
    private TextView dateandtimetext;

    /***************/

    @InjectView(R.id.syncStatusImageView)
    private ImageView syncStatusImageView;

    @InjectView(R.id.dateAndTimeTextViewTopContainer)
    private LinearLayout dateAndTimeTextViewTopContainer;

    @InjectView(R.id.dateTimeTextViewContainer)
    private FloatLabeledTextView dateTimeTextViewContainer;

    @InjectView(R.id.dateTimeTextViewContainernew)
    private FloatLabeledTextView dateTimeTextViewContainernew;



    @InjectView(R.id.nameEditTextContainer)
    private FloatLabeledEditText nameEditTextContainer;

    @InjectView(R.id.descriptionEditTextContainer)
    private FloatLabeledEditText descriptionEditTextContainer;

    @InjectView(R.id.cancelBtn)
    private Button cancelBtn;

    @InjectView(R.id.doneBtn)
    private Button doneBtn;

    @InjectView(R.id.saveAndUploadBtn)
    private Button saveAndUploadBtn;

    @InjectView(R.id.bottomMenuLinearLayout)
    private LinearLayout bottomMenuLinearLayout;

    /********/
    @InjectView(R.id.backgrounglayoutindetails)
    private LinearLayout backgrounglayoutindetails;

    @InjectView(R.id.addeddevision)
    private LinearLayout addeddevision;



    @InjectView(R.id.dateAndTimeTextViewTopContainernew)
    private LinearLayout dateAndTimeTextViewTopContainernew;

    private  View viewindetails;
    private  View viewsecond;

    @InjectView(R.id.datecalender)
    private ImageView datecalender;


    /***********/
    @InjectView(R.id.uploadImageView)
    private ImageView uploadImageView;

    @InjectView(R.id.shareImageView)
    private ImageView shareImageView;

    @InjectView(R.id.editImageView)
    private ImageView editImageView;

    @InjectView(R.id.deleteImageView)
    private ImageView deleteImageView;

    @InjectSavedState
    private Uri mSelectedMediaUri;

    @InjectSavedState
    private FilterType mSelectedMediaType = FilterType.FILTER_ALL;

    @InjectSavedState
    private MediaItemDbModel mMediaItemDbModel = new MediaItemDbModel();

    @InjectSavedState
    private MediaDetailsViewType mViewType = MediaDetailsViewType.ADD;



    @InjectSavedState
    private boolean mShouldEnableDateTimePicker = false;

    @InjectSavedState
    private Calendar mCalendar = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_adddetails, container, false);
        viewindetails=(View)view.findViewById(R.id.viewindetails);
        viewsecond=(View)view.findViewById(R.id.viewsecond);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MediaDetailsActivity) getActivity();

        initViews();
        readIntentArgs();

        addEventListeners();
        preLoadScreenData(mViewType);
    }

    private void initViews() {
        InputFilter alphaNumericFilter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence arg0, int arg1, int arg2, Spanned arg3, int arg4, int arg5)
            {
                for (int k = arg1; k < arg2; k++) {
                    if (!Character.isLetterOrDigit(arg0.charAt(k))) {
                        return "";
                    }   //the first editor deleted this bracket when it is definitely necessary...
                }
                return null;
            }
        };
        nameEditTextContainer.getEditText().setFilters(new InputFilter[]{ alphaNumericFilter});
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_MEDIA_FILE_PATH)) {
                mSelectedMediaUri = (Uri) bundle.getParcelable(AxSysConstants.EXTRA_MEDIA_FILE_PATH);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE)) {
                mSelectedMediaType = (FilterType) bundle.getSerializable(AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_MEDIA_DETAILS_VIEWTYPE)) {
                mViewType = (MediaDetailsViewType) bundle.getSerializable(AxSysConstants.EXTRA_MEDIA_DETAILS_VIEWTYPE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEDIA_OBJ)) {
                mMediaItemDbModel = (MediaItemDbModel) bundle.getSerializable(AxSysConstants.EXTRA_SELECTED_MEDIA_OBJ);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER)) {
                mShouldEnableDateTimePicker = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER, false);
            }
        }
    }

    private void addEventListeners() {
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

        m_descriptionEditText.setTypeface(regular);
        dateTimeTextView.setTypeface(regular);
        m_nameEditText.setTypeface(regular);
        dateTimeTextViewnew.setTypeface(regular);
        nametext.setTypeface(regular);
        descriptiontext.setTypeface(regular);
        dateandtimetext.setTypeface(regular);

        dateAndTimeTextViewTopContainer.setOnClickListener(this);
        nameEditTextContainer.setOnClickListener(this);
        descriptionEditTextContainer.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        doneBtn.setOnClickListener(this);
        saveAndUploadBtn.setOnClickListener(this);

        uploadImageView.setOnClickListener(this);
        shareImageView.setOnClickListener(this);
        editImageView.setOnClickListener(this);
        deleteImageView.setOnClickListener(this);
    }

    public void preLoadScreenData(MediaDetailsViewType viewType) {
        mViewType = viewType;

        // toggle views based on view type
        if (viewType == MediaDetailsViewType.ADD) {
            // ADD
            // set current date to textView
            currentDateTextView.setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_HEADER_DATE_FORMAT).format(Calendar.getInstance().getTime()));

            loadMediaThumbnailView(mSelectedMediaType, mSelectedMediaUri.getPath());
            syncStatusImageView.setImageResource(mMediaItemDbModel.isUploaded() ? R.drawable.photouploaded : R.drawable.photonotselected);

            dateAndTimeTextViewTopContainer.setEnabled(mShouldEnableDateTimePicker);
            //dateAndTimeTextViewTopContainer.setBackgroundResource(R.drawable.white_form);
            dateTimeTextViewContainer.setEnabled(mShouldEnableDateTimePicker);
            dateTimeTextViewContainer.getTextView().setEnabled(mShouldEnableDateTimePicker);

            //nameEditTextContainer.setBackgroundResource(R.drawable.white_form);
            nameEditTextContainer.getEditText().setEnabled(true);
            nameEditTextContainer.setEnabled(true);

            //descriptionEditTextContainer.setBackgroundResource(R.drawable.white_form);
            descriptionEditTextContainer.getEditText().setEnabled(true);
            descriptionEditTextContainer.setEnabled(true);

            dateTimeTextViewContainer.getTextView().setText(CommonUtils.getSelectedMediaDateString(mCalendar.getTime()));
            dateTimeTextViewContainernew.getTextView().setText(CommonUtils.getSelectedMediaTimeString(mCalendar.getTime()));


            cancelBtn.setVisibility(View.VISIBLE);
            doneBtn.setVisibility(View.VISIBLE);
            saveAndUploadBtn.setVisibility(View.VISIBLE);
            bottomMenuLinearLayout.setVisibility(View.GONE);

        } else if (viewType == MediaDetailsViewType.VIEW) {
            // VIEW
            backgrounglayoutindetails.setBackgroundResource(R.color.white);
            viewindetails.setVisibility(View.GONE);
            viewsecond.setVisibility(View.GONE);
            datecalender.setVisibility(View.GONE);
            dateAndTimeTextViewTopContainernew.setVisibility(View.GONE);
            dateandtime.setVisibility(View.VISIBLE);
            name.setVisibility(View.VISIBLE);
            description.setVisibility(View.VISIBLE);
            descriptiontext.setVisibility(View.VISIBLE);
            //dateTimeTextViewContainer.setHint("");
            //nameEditTextContainer.setHint("");
            //descriptionEditTextContainer.setHint("");
            descriptionEditTextContainer.setVisibility(View.GONE);
            dateTimeTextViewContainer.setVisibility(View.GONE);
            nameEditTextContainer.setVisibility(View.GONE);
            addeddevision.setVisibility(View.GONE);
            if (mMediaItemDbModel != null) {
                currentDateTextView.setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_HEADER_DATE_FORMAT).
                        format(new Date(mMediaItemDbModel.getCreatedDate())));

                mCalendar.setTime(new Date(mMediaItemDbModel.getCreatedDate()));
                mSelectedMediaType = mMediaItemDbModel.getMediaType();

                loadMediaThumbnailView(mMediaItemDbModel.getMediaType(), mMediaItemDbModel.getPreviewMedia());
                syncStatusImageView.setImageResource(mMediaItemDbModel.isUploaded() ? R.drawable.photouploaded : R.drawable.photonotselected);

                dateAndTimeTextViewTopContainer.setBackgroundResource(R.drawable.white_form_disabled);
                dateAndTimeTextViewTopContainer.setEnabled(false);
                dateTimeTextViewContainer.setEnabled(false);
                dateTimeTextViewContainer.getTextView().setEnabled(false);

                nameEditTextContainer.setBackgroundResource(R.drawable.white_form_disabled);
                nameEditTextContainer.getEditText().setEnabled(false);
                nameEditTextContainer.getEditText().setText(mMediaItemDbModel.getMediaName());
                nameEditTextContainer.setEnabled(false);
                nametext.setVisibility(View.VISIBLE);
                nametext.setText(mMediaItemDbModel.getMediaName());

                descriptionEditTextContainer.setBackgroundResource(R.drawable.white_form_disabled);
                descriptionEditTextContainer.getEditText().setEnabled(false);
                descriptionEditTextContainer.getEditText().setText(mMediaItemDbModel.getComments());
                descriptiontext.setText(mMediaItemDbModel.getComments());
                descriptionEditTextContainer.setEnabled(false);
                dateandtimetext.setVisibility(View.VISIBLE);
                dateandtimetext.setText("");
                dateandtimetext.setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_DATE_TIME).format(new Date(mMediaItemDbModel.getCreatedDate())));

                dateTimeTextViewContainer.getTextView().setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_DATE_TIME).
                        format(new Date(mMediaItemDbModel.getCreatedDate())));
            }

            cancelBtn.setVisibility(View.GONE);
            doneBtn.setVisibility(View.GONE);
            saveAndUploadBtn.setVisibility(View.GONE);
            bottomMenuLinearLayout.setVisibility(View.VISIBLE);

            // Add click listener to media thumbnail to preview only in VIEW mode
            mediaFrameLayout.setOnClickListener(this);

        } else if (viewType == MediaDetailsViewType.EDIT) {

            backgrounglayoutindetails.setBackgroundResource(R.color.backgroungcolor);
            viewindetails.setVisibility(View.VISIBLE);
            viewsecond.setVisibility(View.VISIBLE);
            datecalender.setVisibility(View.VISIBLE);
            dateAndTimeTextViewTopContainernew.setVisibility(View.VISIBLE);
            dateandtime.setVisibility(View.GONE);
            name.setVisibility(View.GONE);
            description.setVisibility(View.GONE);

            //dateTimeTextViewContainer.setHint("Date");
            //nameEditTextContainer.setHint("Name");
            //descriptionEditTextContainer.setHint("description");
            dateandtimetext.setVisibility(View.GONE);
            nametext.setVisibility(View.GONE);
            addeddevision.setVisibility(View.VISIBLE);
            descriptionEditTextContainer.setVisibility(View.VISIBLE);
            descriptiontext.setVisibility(View.GONE);
            // EDIT
            if (mMediaItemDbModel != null) {
                currentDateTextView.setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_HEADER_DATE_FORMAT).
                        format(new Date(mMediaItemDbModel.getCreatedDate())));

                mCalendar.setTime(new Date(mMediaItemDbModel.getCreatedDate()));
                mSelectedMediaType = mMediaItemDbModel.getMediaType();

                loadMediaThumbnailView(mMediaItemDbModel.getMediaType(), mMediaItemDbModel.getPreviewMedia());
                syncStatusImageView.setImageResource(mMediaItemDbModel.isUploaded() ? R.drawable.photouploaded : R.drawable.photonotselected);
                dateTimeTextViewContainer.setVisibility(View.VISIBLE);
                //dateAndTimeTextViewTopContainer.setBackgroundResource(R.drawable.white_form);
                dateAndTimeTextViewTopContainer.setEnabled(true);
                dateTimeTextViewContainer.setEnabled(true);
                dateTimeTextViewContainer.getTextView().setEnabled(true);
                nameEditTextContainer.setVisibility(View.VISIBLE);
                // nameEditTextContainer.setBackgroundResource(R.drawable.white_form);
                nameEditTextContainer.getEditText().setEnabled(true);
                nameEditTextContainer.getEditText().setText(mMediaItemDbModel.getMediaName());
                nameEditTextContainer.setEnabled(true);

                // descriptionEditTextContainer.setBackgroundResource(R.drawable.white_form);
                descriptionEditTextContainer.getEditText().setEnabled(true);
                descriptionEditTextContainer.getEditText().setText(mMediaItemDbModel.getComments());
                descriptionEditTextContainer.setEnabled(true);

                dateTimeTextViewContainer.getTextView().setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_DATE_TIME).
                        format(new Date(mMediaItemDbModel.getCreatedDate())));
                dateTimeTextViewContainernew.getTextView().setText(new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_ONLY_TIME).
                        format(new Date(mMediaItemDbModel.getCreatedDate())));

            }

            cancelBtn.setVisibility(View.VISIBLE);
            doneBtn.setVisibility(View.VISIBLE);
            saveAndUploadBtn.setVisibility(View.VISIBLE);
            bottomMenuLinearLayout.setVisibility(View.GONE);
        }
    }

    private void loadMediaThumbnailView(FilterType selectedMediaType, String mediaUri) {
        // preload selected media
        if (selectedMediaType == FilterType.FILTER_PHOTOS) {
            // PHOTOS
            playIconImageView.setVisibility(View.GONE);
            mediaDurationtextView.setVisibility(View.GONE);

            if (mediaUri != null && !TextUtils.isEmpty(mediaUri)) {
                String imagePath = mediaUri;
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                Picasso picasso = new Picasso.Builder(getActivity()).listener(
                        new Picasso.Listener() {
                            @Override
                            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                                exception.printStackTrace();
                            }

                        }).build();
                // "http://crackberry.com/sites/crackberry.com/files/styles/large/public/topic_images/2013/ANDROID.png?itok=xhm7jaxS"
                picasso.load(imagePath)
                        .fit()
                        .placeholder(R.drawable.default_media)
                        .error(R.drawable.default_media)
                        .into(mediaImageView);
            }
        } else if (selectedMediaType == FilterType.FILTER_VIDEOS) {
            if (mediaUri != null && !TextUtils.isEmpty(mediaUri)) {
                String videoPath = mediaUri;
                if (!videoPath.startsWith("file://") && !videoPath.startsWith("http")) {
                    videoPath = "file://" + videoPath;
                }
//                    String imagePath = mediaItemDbModel.getPreviewMedia();
//                    if(!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
//                        Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(mSelectedMediaUri.getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
//                        mediaImageView.setImageBitmap(bmThumbnail);
//                    } else {
                Picasso picasso = new Picasso.Builder(getActivity()).listener(
                        new Picasso.Listener() {
                            @Override
                            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                                exception.printStackTrace();
                            }

                        }).build();
                // "http://crackberry.com/sites/crackberry.com/files/styles/large/public/topic_images/2013/ANDROID.png?itok=xhm7jaxS"
                picasso.load(videoPath)
                        .fit()
                        .placeholder(R.drawable.default_media)
                        .error(R.drawable.default_media)
                        .into(mediaImageView);
//                    }
            }
        } else if (selectedMediaType == FilterType.FILTER_AUDIOS) {
           playIconImageView.setImageResource(R.drawable.play_hdpi);
            mediaImageView.setImageResource(R.drawable.musicbg);
        }
    }

    @Override
    protected void initActionBar() {

        if(mViewType==MediaDetailsViewType.ADD)
        {
            mActivity.getSupportActionBar().setTitle(R.string.text_add_details);

        }

        else if(  mViewType==MediaDetailsViewType.VIEW)
        {
            mActivity.getSupportActionBar().setTitle(R.string.text_details);

        }
        else if(  mViewType==MediaDetailsViewType.EDIT)
        {
            mActivity.getSupportActionBar().setTitle(R.string.text_details);

        }
        else {
            mActivity.getSupportActionBar().setTitle(R.string.text_add_details);

        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.mediaFrameLayout:
                if (mViewType == MediaDetailsViewType.VIEW) {
                    if (mSelectedMediaType == FilterType.FILTER_PHOTOS) {
                        String imagePath = mMediaItemDbModel.getPreviewMedia();
                        if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                            imagePath = "file://" + imagePath;
                        }

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse(imagePath), "image/*");
                        startActivity(intent);
                    } else if (mSelectedMediaType == FilterType.FILTER_VIDEOS) {
                        String videoPath = mMediaItemDbModel.getPreviewMedia();
//                        if(!videoPath.startsWith("file://") && !videoPath.startsWith("http")) {
//                            videoPath = "file://" + videoPath;
//                        }

                        Intent intent = new Intent();
                        intent.setAction(android.content.Intent.ACTION_VIEW);
                        File file = new File(videoPath);
                        intent.setDataAndType(Uri.fromFile(file), "video/*");
                        startActivity(intent);
                    } else if (mSelectedMediaType == FilterType.FILTER_AUDIOS) {
                        String audioPath = mMediaItemDbModel.getPreviewMedia();
//                        if(!audioPath.startsWith("file://") && !audioPath.startsWith("http")) {
//                            audioPath = "file://" + audioPath;
//                        }

                        Intent intent = new Intent();
                        intent.setAction(android.content.Intent.ACTION_VIEW);
                        File file = new File(audioPath);
                        intent.setDataAndType(Uri.fromFile(file), "audio/*");
                        startActivity(intent);
                    }
                }
                break;

            case R.id.dateAndTimeTextViewTopContainer:
                showDatePicker();
                break;

            case R.id.nameEditTextContainer:
                nameEditTextContainer.getEditText().requestFocus();
                CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                nameEditTextContainer.getEditText().setSelection(nameEditTextContainer.getEditText().getText().length());
                break;

            case R.id.descriptionEditTextContainer:
                descriptionEditTextContainer.getEditText().requestFocus();
                CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                descriptionEditTextContainer.getEditText().setSelection(descriptionEditTextContainer.getEditText().getText().length());
                break;

            case R.id.cancelBtn:
                CommonUtils.hideSoftKeyboard(mActivity, true, new View[]{nameEditTextContainer.getEditText(),
                        descriptionEditTextContainer.getEditText()});

                if (mViewType == MediaDetailsViewType.ADD) {
                    Intent resultIntent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, resultIntent);
                    mActivity.finish();
                } else {
                    preLoadScreenData(MediaDetailsViewType.VIEW);
                }
                break;

            case R.id.doneBtn:
                CommonUtils.hideSoftKeyboard(mActivity, true, new View[]{nameEditTextContainer.getEditText(),
                        descriptionEditTextContainer.getEditText()});

                if (validateInputs()) {
                    // Do not upload media to server, just save the media in DB and navigate to back
                    if (mViewType == MediaDetailsViewType.ADD) {
                        mMediaItemDbModel.setIsEdited(false);
                        mMediaItemDbModel.setIsUploading(false);
                        mMediaItemDbModel.setIsUploaded(false);
                    } else {
                        mMediaItemDbModel.setIsEdited(true);
                        mMediaItemDbModel.setIsUploading(false);
                        mMediaItemDbModel.setIsUploaded(false);
                    }

                    ArrayList<MediaItemDbModel> mediaItemsAL = new ArrayList<MediaItemDbModel>();
                    mediaItemsAL.add(mMediaItemDbModel);

                    displayProgressLoader(false);
                    final SaveMediaItemsDBLoader saveMediaItemsDBLoader = new SaveMediaItemsDBLoader(mActivity, mediaItemsAL);
                    if(isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_MEDIA_ITEM_LOADER), saveMediaItemsDBLoader);
                    }
                }

                break;

            case R.id.saveAndUploadBtn:
                CommonUtils.hideSoftKeyboard(mActivity, true, new View[]{nameEditTextContainer.getEditText(),
                        descriptionEditTextContainer.getEditText()});

                if (validateInputs()) {
                    // Save media details in DB
                    ArrayList<MediaItemDbModel> mediaItemsAL = new ArrayList<MediaItemDbModel>();
                    mMediaItemDbModel.setIsEdited(true);
                    mMediaItemDbModel.setIsUploaded(false);
                    mMediaItemDbModel.setIsUploading(true);
                    mediaItemsAL.add(mMediaItemDbModel);
                    displayProgressLoader(false);
                    final SaveMediaItemsDBLoader saveMediaItemsDBLoader = new SaveMediaItemsDBLoader(mActivity, mediaItemsAL);
                    if(isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_AND_UPLOAD_MEDIA_ITEM_LOADER), saveMediaItemsDBLoader);
                    }
                }
                break;

            case R.id.uploadImageView:
                displayProgressLoader(false);
                mMediaItemDbModel.setIsEdited(mMediaItemDbModel.isEdited());
                mMediaItemDbModel.setIsUploaded(mMediaItemDbModel.isUploaded());
                mMediaItemDbModel.setIsUploading(true);
                final UploadMediaItemLoader uploadMediaItemLoader = new UploadMediaItemLoader(mActivity, mMediaItemDbModel,
                        mActiveUser.getToken(), mActiveUser.getPatientID());
                if(isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SINGLE_UPLOAD_MEDIA_ITEM_LOADER), uploadMediaItemLoader);
                }
                break;

            case R.id.shareImageView:
                break;

            case R.id.editImageView:
                preLoadScreenData(MediaDetailsViewType.EDIT);
                break;

            case R.id.deleteImageView:
                final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.delete_media_confirmation));
                bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, DELETE_MEDIA_DIALOG_ID);
                bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                confirmationDialogFragment.setArguments(bundle);
                showDialog(confirmationDialogFragment);
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SAVE_MEDIA_ITEM_LOADER)) {
            showToast(R.string.error_caching_media_details);
        } else if (id == getLoaderHelper().getLoaderId(SAVE_AND_UPLOAD_MEDIA_ITEM_LOADER)) {
            showToast(R.string.error_uploading_media);
        } else if (id == getLoaderHelper().getLoaderId(SINGLE_UPLOAD_MEDIA_ITEM_LOADER)) {
            if (mViewType == MediaDetailsViewType.ADD) {
                mMediaItemDbModel.setIsEdited(mMediaItemDbModel.isEdited());
                mMediaItemDbModel.setIsUploaded(mMediaItemDbModel.isUploaded());
                mMediaItemDbModel.setIsUploading(true);

                Intent resultIntent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, resultIntent);
                mActivity.finish();
            } else {
                mMediaItemDbModel.setIsEdited(mMediaItemDbModel.isEdited());
                mMediaItemDbModel.setIsUploaded(mMediaItemDbModel.isUploaded());
                mMediaItemDbModel.setIsUploading(true);

                preLoadScreenData(MediaDetailsViewType.VIEW);
            }
        } else if (id == getLoaderHelper().getLoaderId(DELETE_MEDIA_ITEM_LOADER)) {
            showToast(R.string.error_deleting_media_from_cache);
        } else if (id == getLoaderHelper().getLoaderId(DELETE_MEDIA_ITEM_FROM_SERVER_LOADER)) {
            showToast(R.string.error_deleting_media_from_server);
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SAVE_MEDIA_ITEM_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                ArrayList<MediaItemDbModel> mediaItemDbModelAL = (ArrayList<MediaItemDbModel>) result;

                if (mViewType == MediaDetailsViewType.ADD) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(AxSysConstants.EXTRA_SAVED_MEDIA_OBJECT, mediaItemDbModelAL.get(0));
                    mActivity.setResult(Activity.RESULT_OK, resultIntent);
                    mActivity.finish();
                } else {
                    preLoadScreenData(MediaDetailsViewType.VIEW);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_AND_UPLOAD_MEDIA_ITEM_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                ArrayList<MediaItemDbModel> mediaItemDbModelAL = (ArrayList<MediaItemDbModel>) result;

                if (mViewType == MediaDetailsViewType.ADD) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(AxSysConstants.EXTRA_UPLOAD_MEDIA_OBJECT, mediaItemDbModelAL.get(0));
                    mActivity.setResult(Activity.RESULT_OK, resultIntent);
                    mActivity.finish();
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SINGLE_UPLOAD_MEDIA_ITEM_LOADER)) {
            if (result != null && result instanceof UploadMediaItemResponse) {
                UploadMediaItemResponse uploadMediaItemResponse = (UploadMediaItemResponse) result;

                mMediaItemDbModel.setIsEdited(false);
                mMediaItemDbModel.setIsUploaded(true);
                mMediaItemDbModel.setIsUploading(false);

                // update DB once upload is done with new media id
                final UpdateMediaItemDBLoader updateMediaItemDBLoader = new UpdateMediaItemDBLoader(mActivity, mMediaItemDbModel, uploadMediaItemResponse.getData());
                if(isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(UPDATE_MEDIA_ITEM_IN_DB_LOADER), updateMediaItemDBLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(UPDATE_MEDIA_ITEM_IN_DB_LOADER)) {
            if (result != null && result instanceof MediaItemDbModel) {
                MediaItemDbModel updatedMedia = (MediaItemDbModel) result;

                if (mViewType == MediaDetailsViewType.ADD) {
                    Intent resultIntent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, resultIntent);
                    mActivity.finish();
                } else {
                    mMediaItemDbModel = updatedMedia;
                    preLoadScreenData(MediaDetailsViewType.VIEW);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(DELETE_MEDIA_ITEM_LOADER)) {
            if (result != null && result instanceof MediaItemDbModel) {
                MediaItemDbModel deletedMedia = (MediaItemDbModel) result;

                mMediaItemDbModel.setIsEdited(false);
                mMediaItemDbModel.setIsDeleted(true);

                if (deletedMedia.getMediaId() > 0) {
                    // Delete media item from server and then send user back to media category screen
                    final DeleteMediaFromServerLoader deleteMediaFromServerLoader = new DeleteMediaFromServerLoader(mActivity, mMediaItemDbModel);
                    if(isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DELETE_MEDIA_ITEM_FROM_SERVER_LOADER), deleteMediaFromServerLoader);
                    }
                } else {
//                    Intent resultIntent = new Intent(AxSysConstants.BCR_MEDIA_DELETED);
//                    resultIntent.putExtra(AxSysConstants.EXTRA_DELETED_MEDIA_OBJECT, deletedMedia);
//                    mActivity.sendBroadcast(resultIntent);

                    mActivity.finish();
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(DELETE_MEDIA_ITEM_FROM_SERVER_LOADER)) {
            if (result != null && result instanceof DeleteMediaItemResponse) {
                DeleteMediaItemResponse deleteMediaItemResponse = (DeleteMediaItemResponse) result;

//                Intent resultIntent = new Intent(AxSysConstants.BCR_MEDIA_DELETED);
//                resultIntent.putExtra(AxSysConstants.EXTRA_DELETED_MEDIA_OBJECT, mMediaItemDbModel);
//                mActivity.sendBroadcast(resultIntent);

                mActivity.finish();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CommonUtils.hideSoftKeyboard(mActivity);

        if (mViewType == MediaDetailsViewType.VIEW || mViewType == MediaDetailsViewType.EDIT) {
            Intent editIntent = new Intent(AxSysConstants.BCR_UPDATE_MEDIA_SCREEN);
            mActivity.sendBroadcast(editIntent);
        }
    }

    private boolean validateInputs() {
        // check media size before uploading or validating other inputs
        String mediaPath = (mSelectedMediaUri != null ? mSelectedMediaUri.getPath() : null);

        if(!TextUtils.isEmpty(mediaPath)) {
            File file = new File(mediaPath);
            // Get length of file in bytes
            long fileSizeInBytes = file.length();
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            long fileSizeInKB = fileSizeInBytes / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            long fileSizeInMB = fileSizeInKB / 1024;

            if (fileSizeInMB > CommonUtils.getUploadFileSizeInMB(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                showToast(R.string.media_size_exceeded);
                return false;
            }
        }

        if (TextUtils.isEmpty(dateTimeTextViewContainer.getTextView().getText().toString().trim())) {
            showToast(R.string.input_media_date);
            dateTimeTextViewContainer.requestFocus();
            return false;
        } else {
            mMediaItemDbModel.setCreatedDate(CommonUtils.getMediaDateInMills(AxSysConstants.DF_MEDIA_DETAILS_DATE_TIME, dateTimeTextViewContainer.getTextView().getText().toString().trim()));
            mMediaItemDbModel.setLmd(CommonUtils.getMediaDateInMills(AxSysConstants.DF_MEDIA_DETAILS_DATE_TIME, dateTimeTextViewContainer.getTextView().getText().toString().trim()));
        }

        if (TextUtils.isEmpty(nameEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_media_name);
            nameEditTextContainer.getEditText().requestFocus();
            nameEditTextContainer.getEditText().setSelection(nameEditTextContainer.getEditText().getText().length());
            return false;
        } else {
            mMediaItemDbModel.setMediaName(nameEditTextContainer.getEditText().getText().toString().trim());
        }

        // Clear focus
        dateTimeTextViewContainer.getTextView().clearFocus();
        nameEditTextContainer.getEditText().clearFocus();
        descriptionEditTextContainer.getEditText().clearFocus();
        currentDateTextView.requestFocus();

        // Fill Media Details to DB object
        List<MediaFoldersModel> queriedFolder = CommonUtils.getFolderIdFromDB(mSelectedMediaType.getFilterName());

        mMediaItemDbModel.setFolderId((queriedFolder != null && queriedFolder.size() > 0) ? queriedFolder.get(0).getMediaFolderId() : -1);
        mMediaItemDbModel.setSubFolderId((queriedFolder != null && queriedFolder.size() > 0) ? queriedFolder.get(0).getMediaSubFolderId() : -1);

        if (mViewType == MediaDetailsViewType.ADD) {
            // While adding new media generate new ID with negative sign
            int mediaId = SharedPreferenceUtil.getIntFromSP(mActivity, SharedPreferenceUtil.SP_MEDIA_LOCAL_ID_KEY, 0);
            mediaId = mediaId - 1;
            mMediaItemDbModel.setMediaId(mediaId);
            SharedPreferenceUtil.saveIntInSP(mActivity, SharedPreferenceUtil.SP_MEDIA_LOCAL_ID_KEY, mediaId);
        } else {
            // While editing make sure we set the old media id before committing to DB
            mMediaItemDbModel.setMediaId(mMediaItemDbModel.getMediaId());
        }

        mMediaItemDbModel.setIsUploaded(mMediaItemDbModel.isUploaded());
        mMediaItemDbModel.setAnnotationMedia("");
        mMediaItemDbModel.setOriginalMedia("");
        mMediaItemDbModel.setSiteId(mActiveUser.getSiteID());
        mMediaItemDbModel.setEcUserID(mActiveUser.getEcUserID());

        mMediaItemDbModel.setMediaType(mSelectedMediaType);
        mMediaItemDbModel.setPreviewMedia((mViewType == MediaDetailsViewType.ADD) ? mSelectedMediaUri.getPath() : mMediaItemDbModel.getPreviewMedia());
        mMediaItemDbModel.setComments(descriptionEditTextContainer.getEditText().getText().toString().trim());

        return true;
    }

    private void showDatePicker() {
        DateTimePickerFragment dateAndTimePicker = new DateTimePickerFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(AxSysConstants.DF_MEDIA_CALENDAR_DATE, mCalendar);
        dateAndTimePicker.setArguments(bundle);

        dateAndTimePicker.setDateTimeDialogListener(this);
        dateAndTimePicker.show(getActivity().getFragmentManager(), DateTimePickerFragment.class.getSimpleName());
    }

    @Override
    public void onDialogPositiveButtonClick(Calendar calendar) {
        if (calendar != null) {
            mCalendar = calendar;

            dateTimeTextViewContainer.getTextView().setText(CommonUtils.getSelectedMediaDateString(calendar.getTime()));
            dateTimeTextViewContainernew.getTextView().setText(CommonUtils.getSelectedMediaTimeString(calendar.getTime()));
        }
    }

    @Override
    public void onDialogNegativeButtonClick() {
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == DELETE_MEDIA_DIALOG_ID) {
            final DeleteMediaItemDbLoader deleteMediaItemDbLoader = new DeleteMediaItemDbLoader(mActivity, mMediaItemDbModel);
            if(isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DELETE_MEDIA_ITEM_LOADER), deleteMediaItemDbLoader);
            }
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == DELETE_MEDIA_DIALOG_ID) {

        }
    }

    public interface OnMediaSavedListener {
        public void onMediaSaved(MediaItemDbModel mediaItemDbModel);
        public void onMediaUpload(MediaItemDbModel mediaItemDbModel);
    }

}
