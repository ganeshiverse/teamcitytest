package com.axsystech.excelicare.framework.ui.messages;

/**
 * Created by someswarreddy on 02/09/15.
 */
public class RecipientDetails {

    private String recipientType_LU;
    private String recipientUserID;

    public RecipientDetails() {}

    public RecipientDetails(String recipientType_LU, String recipientUserID) {
        this.recipientType_LU = recipientType_LU;
        this.recipientUserID = recipientUserID;
    }

    public String getRecipientType_LU() {
        return recipientType_LU;
    }

    public void setRecipientType_LU(String recipientType_LU) {
        this.recipientType_LU = recipientType_LU;
    }

    public String getRecipientUserID() {
        return recipientUserID;
    }

    public void setRecipientUserID(String recipientUserID) {
        this.recipientUserID = recipientUserID;
    }
}
