package com.iversecomics.client.drawable;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PixelFormat;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IDebuggable;
import com.iversecomics.client.bitmap.ImageRect;
import com.iversecomics.client.util.MatrixUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicPlusSplashDrawable extends Drawable implements IDebuggable {
    private static final Logger LOG = LoggerFactory.getLogger(ComicPlusSplashDrawable.class);
    private static final int DEFAULT_HEIGHT = 50;
    private static final int DEFAULT_WIDTH = 50;

    private static final float FULL_CIRCLE = 360F;

    private static final int WHITE_OPAQUE = Color.parseColor("#ffffffff");
    private static final int DEFAULT_SEGMENTS = 20;

    private int segments = DEFAULT_SEGMENTS;
    private boolean debug = false;

    private Paint backgroundPaint;
    private Paint wedgePaint;
    private Paint borderPaint;
    private Paint hilitePaint;

    private Path wedgePath;
    private Path clipPath;

    private int hiliteColor = WHITE_OPAQUE;
    private boolean drawBorder = true;

    static public ComicPlusSplashDrawable makeWithResource(final Resources r, final boolean border, final boolean highlite) {
        final int lightToneColor = r.getColor(R.color.background_wedge_mid_tone);
        final int midToneColor = r.getColor(R.color.background_wedge_lite_tone);

        final ComicPlusSplashDrawable cpd = new ComicPlusSplashDrawable(lightToneColor, midToneColor);
        cpd.setDrawBorder(border);

        if (highlite) {
            final int color = r.getColor(R.color.comicsplus_blue);
            cpd.setHiliteColor(color);
        }

        return cpd;
    }

    public ComicPlusSplashDrawable(final int lightTone, final int midTone) {
        super();

        backgroundPaint = new Paint();
        backgroundPaint.setAntiAlias(true);
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setColor(lightTone);

        wedgePaint = new Paint();
        wedgePaint.setAntiAlias(true);
        wedgePaint.setStyle(Paint.Style.FILL);
        wedgePaint.setColor(midTone);

        borderPaint = new Paint();
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(5F);
        borderPaint.setColor(Color.parseColor("#88000000"));

        hilitePaint = new Paint();
        hilitePaint.setAntiAlias(true);
        hilitePaint.setStyle(Paint.Style.FILL);

        clipPath = new Path();
        wedgePath = new Path();

        ImageRect r = new ImageRect(0, 0, 100, 100);
        recalcClip(r);
        recalcWedge(r);
    }

    private void debug(String format, Object... args) {
        if (debug) {
            LOG.debug(format, args);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        ImageRect rect = new ImageRect(getBounds());
        if (!rect.isVisible()) {
            return;
        }

        int saveCount = canvas.save();
        try {
            debug("rect = %s", rect);

            // calculate centerpoint
            float cx = rect.midX();
            float cy = rect.midY();

            debug("center = %.3f x %.3f", cx, cy);

            // Clip Rect
            canvas.clipPath(clipPath);

            // background
            canvas.drawRect(rect, backgroundPaint);

            // draw box
            if (debug) {
                DrawableDebug.box(canvas, rect);
            }

            canvas.save();
            // draw all wedges
            Matrix m = new Matrix();
            m.set(canvas.getMatrix());
            if (debug) {
                debug("canvas.matrix = %s", MatrixUtil.dump(canvas.getMatrix()));
            }
            m.preTranslate(cx, cy);

            float degrees = FULL_CIRCLE / segments;

            Matrix sm = new Matrix();
            Matrix nm = new Matrix();
            for (int i = 0; i < segments; i++) {
                float rotate = degrees * i;
                sm.setRotate(rotate);
                nm.set(sm);
                nm.postConcat(m);
                canvas.setMatrix(nm);
                canvas.drawPath(wedgePath, wedgePaint);
            }

            // restore original matrix.
            canvas.restore();

            // hilite
            float hiliteRadius = Math.max(cx, cy);
            canvas.drawCircle(cx, cy, hiliteRadius, hilitePaint);

            if (drawBorder) {
                // border
                canvas.drawRect(rect.insetRectF(1F), borderPaint);
            }
        } finally {
            canvas.restoreToCount(saveCount);
        }
    }

    public int getBackgroundColor() {
        return backgroundPaint.getColor();
    }

    public int getHiliteColor() {
        return hiliteColor;
    }

    @Override
    public int getIntrinsicHeight() {
        return DEFAULT_HEIGHT;
    }

    @Override
    public int getIntrinsicWidth() {
        return DEFAULT_WIDTH;
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public int getSegments() {
        return segments;
    }

    public int getWedgeColor() {
        return wedgePaint.getColor();
    }

    @Override
    public boolean isDebug() {
        return debug;
    }

    public boolean isDrawBorder() {
        return drawBorder;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);

        ImageRect r = new ImageRect(bounds);
        float cx = r.midX();
        float cy = r.midY();

        // Highlight
        float hiliteRadius = Math.max(cx, cy);
        int colorCenter = hiliteColor;
        float hsv[] = new float[3];
        Color.colorToHSV(colorCenter, hsv);
        int colorOuter = Color.HSVToColor(0, hsv);
        RadialGradient highlight = new RadialGradient(cx, cy, hiliteRadius, colorCenter, colorOuter, TileMode.CLAMP);
        hilitePaint.setShader(highlight);

        recalcClip(r);
        recalcWedge(r);
    }

    private void recalcClip(ImageRect r) {
        clipPath.reset();
        clipPath.addRect(r, Direction.CW);
    }

    private void recalcWedge(ImageRect r) {
        float cx = r.midX();
        float cy = r.midY();
        float wedgeLength = (float) Math.sqrt((cx * cx) + (cy * cy));
        float degrees = FULL_CIRCLE / segments;
        float wedgeRadians = (float) Math.toRadians(degrees / 2F);
        float rise = (float) Math.abs(wedgeLength * Math.tan(wedgeRadians));

        wedgePath.reset();
        wedgePath.moveTo(0, 0);
        wedgePath.lineTo(wedgeLength, rise / 2);
        wedgePath.lineTo(wedgeLength, -(rise / 2));
        wedgePath.close();
    }

    @Override
    public void setAlpha(int alpha) {
        /* ignore */
    }

    public void setBackgroundColor(int backgroundColor) {
        backgroundPaint.setColor(backgroundColor);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        /* ignore */
    }

    @Override
    public void setDebug(boolean flag) {
        this.debug = flag;
    }

    public void setDrawBorder(boolean drawBorder) {
        this.drawBorder = drawBorder;
    }

    public void setHiliteColor(int hiliteColor) {
        this.hiliteColor = hiliteColor;
    }

    public void setSegments(int segments) {
        this.segments = segments;
        ImageRect r = new ImageRect(getBounds());
        recalcWedge(r);
    }

    public void setWedgeColor(int wedgeColor) {
        this.wedgePaint.setColor(wedgeColor);
    }
}
