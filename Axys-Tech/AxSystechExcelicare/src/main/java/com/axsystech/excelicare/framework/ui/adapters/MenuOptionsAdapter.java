package com.axsystech.excelicare.framework.ui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.util.CommonUtils;
import com.squareup.picasso.Picasso;

/**
 * Date: 06.05.14
 * Time: 13:37
 *
 * @author SomeswarReddy
 */
public class MenuOptionsAdapter extends BaseExpandableListAdapter {

    private Context context;
    private final LayoutInflater mLayoutInflater;
    private AppMenuListResponse appMenuListResponse;

    public MenuOptionsAdapter(final Context context, AppMenuListResponse appMenuListResponse) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.appMenuListResponse = appMenuListResponse;
    }

    @Override
    public int getGroupCount() {
        return ((appMenuListResponse != null && appMenuListResponse.getMenuListDataAL() != null) ? appMenuListResponse.getMenuListDataAL().size() : 0);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return ((appMenuListResponse != null && appMenuListResponse.getMenuListDataAL() != null) ?
                (appMenuListResponse.getMenuListDataAL().get(groupPosition).getItemsAL() != null ?
                        appMenuListResponse.getMenuListDataAL().get(groupPosition).getItemsAL().size() : 0) : 0);
    }

    @Override
    public Object getGroup(int groupPosition) {
        return appMenuListResponse.getMenuListDataAL().get(groupPosition);
    }

    @Override
    public AppMenuListResponse.Items getChild(int groupPosition, int childPosition) {
        return appMenuListResponse.getMenuListDataAL().get(groupPosition).getItemsAL().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final AppMenuListResponse.MenuListData groupMenuItem = (AppMenuListResponse.MenuListData) getGroup(groupPosition);
        final GroupViewHolder holder;

        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.leftmenu_header_view, parent, false);
            holder = new GroupViewHolder((ImageView) convertView.findViewById(R.id.leftmenuImageView),
                    (TextView) convertView.findViewById(R.id.menuItemGroupNameTextView),
                    (ImageView) convertView.findViewById(R.id.collapsableImageView),
                    (TextView) convertView.findViewById(R.id.proxyEmail));

            Typeface regular = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_bold.ttf");

            holder.nameTextView.setTypeface(regular);
            holder.proxyEmail.setTypeface(regular);

            convertView.setTag(R.id.groupholder, holder);
        } else {
            holder = (GroupViewHolder) convertView.getTag(R.id.groupholder);
        }

        if (holder != null && groupMenuItem != null) {
            if (holder.collapsableImageView != null)
                holder.collapsableImageView.setVisibility(View.VISIBLE);
            if (holder.proxyEmail != null) holder.proxyEmail.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(groupMenuItem.getIcon()) && groupMenuItem.getIcon().trim().startsWith("http")) {
                if (holder.leftmenuImageView != null)
                    holder.leftmenuImageView.setVisibility(View.VISIBLE);
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(CommonUtils.dpToPx(50), CommonUtils.dpToPx(50));
                if (holder.leftmenuImageView != null)
                    holder.leftmenuImageView.setLayoutParams(params);

                Picasso.with(context)
                        .load(groupMenuItem.getIcon())
                        .placeholder(R.drawable.default_menu_header_icon)
                        .error(R.drawable.default_menu_header_icon)
                        .into(holder.leftmenuImageView);
            } else {
                if (holder.leftmenuImageView != null)
                    holder.leftmenuImageView.setVisibility(View.GONE);
            }

            if (holder.nameTextView != null)

                holder.nameTextView.setText(groupMenuItem.getText().trim());
            if (groupMenuItem.getItemsAL() != null && groupMenuItem.getItemsAL().size() > 0) {
                if (holder.collapsableImageView != null)
                    holder.collapsableImageView.setVisibility(View.VISIBLE);
                if (isExpanded) {
                    if (holder.collapsableImageView != null)
                        holder.collapsableImageView.setImageResource(R.drawable.ic_minus);
                } else {
                    if (holder.collapsableImageView != null)
                        holder.collapsableImageView.setImageResource(R.drawable.ic_plus);
                }
            } else {
                if (holder.collapsableImageView != null)
                    holder.collapsableImageView.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final AppMenuListResponse.Items childMenuItem = getChild(groupPosition, childPosition);
        final ChildViewHolder holder;

        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.leftmenu_child_view, parent, false);
            holder = new ChildViewHolder((TextView) convertView.findViewById(R.id.menuItemChildNameTextView),
                    (ImageView) convertView.findViewById(R.id.leftmenuChildImageView));
            Typeface regular = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_reg.ttf");
             holder.nameTextView.setTypeface(regular);
            convertView.setTag(R.id.childholder, holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag(R.id.childholder);
        }

        if (holder.nameTextView != null) holder.nameTextView.setText(childMenuItem.getText());

        if (!TextUtils.isEmpty(childMenuItem.getIcon()) && childMenuItem.getIcon().trim().startsWith("http")) {
            if (holder.leftmenuChildImageView != null)
                holder.leftmenuChildImageView.setVisibility(View.VISIBLE);
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(CommonUtils.dpToPx(50), CommonUtils.dpToPx(50));
            if (holder.leftmenuChildImageView != null)
                holder.leftmenuChildImageView.setLayoutParams(params);

            Picasso.with(context)
                    .load(childMenuItem.getIcon())
                    .placeholder(R.drawable.default_menu_child_icon)
                    .error(R.drawable.default_menu_child_icon)
                    .into(holder.leftmenuChildImageView);
        } else {
            if (holder.leftmenuChildImageView != null)
                holder.leftmenuChildImageView.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    public static class GroupViewHolder {
        final ImageView leftmenuImageView;
        final TextView nameTextView;
        final ImageView collapsableImageView;
        final TextView proxyEmail;

        public GroupViewHolder(final ImageView leftmenuImageView, final TextView nameTextView, final ImageView collapsableImageView,
                               TextView proxyEmail) {
            this.leftmenuImageView = leftmenuImageView;
            this.collapsableImageView = collapsableImageView;
            this.nameTextView = nameTextView;
            this.proxyEmail = proxyEmail;
        }
    }

    public static class ChildViewHolder {
        final TextView nameTextView;
        final ImageView leftmenuChildImageView;

        public ChildViewHolder(final TextView nameTextView, final ImageView leftmenuChildImageView) {
            this.nameTextView = nameTextView;
            this.leftmenuChildImageView = leftmenuChildImageView;
        }
    }

}
