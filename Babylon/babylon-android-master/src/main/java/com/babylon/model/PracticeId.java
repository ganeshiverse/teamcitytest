package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class PracticeId {
    @SerializedName("practice_id")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
