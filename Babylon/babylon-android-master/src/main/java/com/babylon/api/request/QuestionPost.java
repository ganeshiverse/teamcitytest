package com.babylon.api.request;

import android.text.TextUtils;
import com.babylon.BaseConfig;
import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;
import com.babylon.model.FileEntry;
import com.babylon.model.Message;
import com.babylon.utils.L;
import com.parse.entity.mime.MultipartEntity;
import com.parse.entity.mime.content.FileBody;
import com.parse.entity.mime.content.StringBody;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class QuestionPost extends AbsRequestPost<Message, Message.Question> {

    private static final String TEXT = "text";
    private static final String FILES = "files";
    private static final String DATE_SENT = "dateSent";

    private MultipartEntity multipartEntity;

    @Override
    protected void setHeaders(HttpPost httpRequest) {
        //Without content-type header
    }

    @Override
    protected String getUrl() {
        return Urls.get(Urls.QUESTION_POST);
    }

    @Override
    public HttpEntity getHttpEntity() {
        return multipartEntity;
    }

    @Override
    public AbsRequestPost<Message, Message.Question> withEntry(Message entry) {
        multipartEntity = new MultipartEntity();

        try {

            if(!TextUtils.isEmpty(entry.getText())){
                multipartEntity.addPart(TEXT, new StringBody(entry.getText()));
            }

            multipartEntity.addPart(DATE_SENT, new StringBody(String.valueOf(entry.getSendDate())));
        } catch (UnsupportedEncodingException e) {
            L.e(TAG, e.toString(), e);
        }

        final List<FileEntry> files = entry.getFiles();
        for (int i = 0; i < files.size(); i++) {
            FileEntry fileEntry = files.get(i);
            new File(BaseConfig.CACHE_DIR).mkdirs();
            final File file = new File(fileEntry.getFilePath());
            try {
                file.createNewFile();
                final FileOutputStream fileOutputStream = new FileOutputStream(file);

                if (fileEntry.getData() != null) {
                    fileOutputStream.write(fileEntry.getData());
                }
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    L.e(TAG, e.toString(), e);
                }
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }

            FileBody fileBody = new FileBody(file);
            multipartEntity.addPart(FILES + "[" + i + "]", fileBody);
        }
        return this;
    }
}


