package com.iversecomics.client.net;

import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;

public class HttpUtil {
    public static void close(HttpEntity entity) {
        if (entity == null) {
            return;
        }

        try {
            InputStream in = entity.getContent();
            IOUtil.close(in);
        } catch (IllegalStateException e) {
            /* ignore */
        } catch (IOException e) {
            /* ignore */
        }
    }

    public static String getHeader(HttpResponse response, String name) {
        return getHeader(response, name, null);
    }

    public static String getHeader(HttpResponse response, String name, String defaultValue) {
        Header header = response.getFirstHeader(name);
        if (header == null) {
            return defaultValue;
        }
        return header.getValue();
    }

    public static long getRetryAfter(Header header) {
        int minRetry = DownloaderManager.getInstance().getRetriesMinAfter();
        int maxRetry = DownloaderManager.getInstance().getRetriesMaxAfter();

        if (header == null) {
            return maxRetry;
        }

        try {
            long retryAfter = Long.parseLong(header.getValue());

            if (retryAfter < minRetry) {
                return minRetry;
            }

            if (retryAfter > maxRetry) {
                return maxRetry;
            }

            return retryAfter;
        } catch (NumberFormatException e) {
            return minRetry;
        }
    }

    public static boolean isRedirect(int httpStatusCode) {
        return (httpStatusCode == HttpStatus.SC_MOVED_PERMANENTLY)
                || (httpStatusCode == HttpStatus.SC_MOVED_TEMPORARILY) || (httpStatusCode == HttpStatus.SC_SEE_OTHER)
                || (httpStatusCode == HttpStatus.SC_TEMPORARY_REDIRECT);
    }

    public static boolean isServiceUnavailable(int httpStatusCode) {
        return (httpStatusCode == HttpStatus.SC_SERVICE_UNAVAILABLE);
    }

    public static void logResponse(Logger logger, HttpResponse response) {
        int httpStatusCode = response.getStatusLine().getStatusCode();
        logger.debug("Got HTTP response %d : %s", httpStatusCode, response.getStatusLine().getReasonPhrase());
        for (Header header : response.getAllHeaders()) {
            logger.debug("%s: %s", header.getName(), header.getValue());
        }
    }

    public static int toInt(String str, int defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}
