package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.ContentChangeTracker;

public class FetchComicTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testFetchComic() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(ComicsTable.CONTENT_URI, this);

        String comicId = "com.iversecomics.idw.star.trek.year.four.enterprise.experiment.one";
        ComicStore comicStore = getComicStore();
        FetchComicTask task = new FetchComicTask(comicStore, comicId);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, ComicsTable.CONTENT_URI);
        assertEquals("Comics table rows", 1, actualCount);
    }
}
