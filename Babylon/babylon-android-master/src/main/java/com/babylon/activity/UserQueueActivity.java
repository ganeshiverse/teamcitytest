package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.sync.SyncUtils;


public class UserQueueActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_queue);

        Button logOutButton = (Button) findViewById(R.id.user_queue_log_out_button);
        logOutButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_queue_log_out_button:
                finish();
                SyncUtils.signOut();
                Intent intent = new Intent(this, SignInActivity.class);
                intent.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

}
