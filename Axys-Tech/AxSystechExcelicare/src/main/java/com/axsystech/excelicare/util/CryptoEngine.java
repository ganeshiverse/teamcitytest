package com.axsystech.excelicare.util;

import android.provider.SyncStateContract;
import android.util.Base64;

import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by alanka on 9/25/2015.
 */
public class CryptoEngine {
    // Key length should be 32 characters for AES encryption
    private static final String sKey = "bsdiuvbwyerw8vfgwbv87vb7gb847eri";

    private static final String AES = "AES";
    private static Cipher sCipher;
    private static SecretKeySpec sSecretKeySpec;
    private static byte[] sEncrypted;
    private static byte[] sDecrypted;
    private static byte[] sClear;

    static {
        try {
            sCipher = Cipher.getInstance("AES/ECB/PKCS7Padding");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method is for Encryption of data
     *
     * @param data to be used for Encryption
     * @return Encrypted data
     * @throws Exception
     */
    public static String encrypt(String data) throws Exception {

        sClear = data.getBytes("UTF-8");
        sSecretKeySpec = new SecretKeySpec(sKey.getBytes(), AES);
        sCipher.init(Cipher.ENCRYPT_MODE, sSecretKeySpec);
        sEncrypted = sCipher.doFinal(sClear);
        String sResultString = Base64
                .encodeToString(sEncrypted, Base64.DEFAULT);
        clearTempData();
        return sResultString;
    }

    /**
     * The method is for Decryption of data
     *
     * @param encryptedData to be used for Decryption
     * @return Decrypted data
     * @throws Exception
     */
    public static String decrypt(String encryptedData) throws Exception {

        sSecretKeySpec = new SecretKeySpec(sKey.getBytes(), AES);
        sCipher.init(Cipher.DECRYPT_MODE, sSecretKeySpec);
        sDecrypted = sCipher.doFinal(Base64.decode(encryptedData, Base64.DEFAULT));
        //String sResultString = new String(sDecrypted);
        String sResultString = new String(sDecrypted, "UTF-8");
        clearTempData();
        return sResultString;
    }


    private static void clearTempData() {
        sClear = null;
        sSecretKeySpec = null;
        sEncrypted = null;
        sDecrypted = null;
    }

}
