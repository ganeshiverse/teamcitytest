package com.babylon.sync;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import com.babylon.App;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.database.DatabaseHelper;
import com.babylon.model.Patient;
import com.babylon.model.Wsse;
import com.babylon.sync.auth.BabylonAccountService;
import com.babylon.sync.auth.BabylonAuthenticator;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;

import java.util.Calendar;

/**
 * Static helper methods for working with the sync framework.
 */
public class SyncUtils {
    private static String TAG = SyncUtils.class.getSimpleName();

    /**
     * Max auto sync period
     */
    public static final int MAX_SYNC_DAYS = 1;
    /**
     * Already synced data and sync already not need
     */
    public static final int SYNC_TYPE_ADDED = 0;
    /**
     * Need to sync
     */
    public static final int SYNC_TYPE_ADD = 1;
    /**
     * Need to sync with delete request, after that delete from database
     */
    public static final int SYNC_TYPE_DELETE = 2;
    /**
     * Need to sync with delete request, after that delete from database
     */
    public static final int SYNC_TYPE_PATCH = 3;

    /**
     * Max number of the objects that can be sent per one request in the body
     */
    public static final int MAX_SYNC_OBJECTS_NUMBER = 400;

    private static final long SYNC_FREQUENCY = 24 * 60 * 60;  // 24 hour (in seconds)
    private static final String CONTENT_AUTHORITY = DatabaseHelper.CONTENT_AUTHORITY; //Authority from provider

    public static long getMinDateSyncInMillis() {
        Calendar calendarFrom = getSyncCalendar();

        long date = DateUtils.getStartDate(calendarFrom.getTime()).getTime();
        return date;
    }

    public static Calendar getSyncCalendar() {
        Calendar calendarFrom = Calendar.getInstance();
        calendarFrom.add(Calendar.DAY_OF_MONTH, -SyncUtils.MAX_SYNC_DAYS);
        return calendarFrom;
    }

    /**
     * Create an entry for this application in the system account list, if it isn't already there.
     *
     * @param context Context
     */
    @TargetApi(Build.VERSION_CODES.FROYO)
    public static Account createSyncAccount(Context context, String name, String pass) {
        boolean newAccount = false;
        boolean setupComplete = PreferenceManager
                .getDefaultSharedPreferences(context).getBoolean(Constants.PREF_SETUP_COMPLETE, false);

        // Create account, if it's missing. (Either first run, or user has deleted account.)
        Account account = new Account(name, BabylonAuthenticator.ACCOUNT_TYPE);
        AccountManager accountManager = AccountManager.get(context);

        if (accountManager.addAccountExplicitly(account, pass, null)) {
            // Inform the system that this account supports sync
            ContentResolver.setIsSyncable(account, CONTENT_AUTHORITY, 1);
            // Inform the system that this account is eligible for auto sync when the network is up
            ContentResolver.setSyncAutomatically(account, CONTENT_AUTHORITY, true);
            // Recommend a schedule for automatic synchronization. The system may modify this based
            // on other scheduled syncs and network utilization.
            ContentResolver.addPeriodicSync(
                    account, CONTENT_AUTHORITY, new Bundle(), SYNC_FREQUENCY);
            newAccount = true;
        }

        // Schedule an initial sync if we detect problems with either our account or our local
        // data has been deleted. (Note that it's possible to clear app data WITHOUT affecting
        // the account list, so wee need to check both.)
        if (newAccount || !setupComplete) {
            PreferenceManager.getDefaultSharedPreferences(context).edit()
                    .putBoolean(Constants.PREF_SETUP_COMPLETE, true).commit();
        }
        return account;
    }

    public static void removeAccounts(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType(BabylonAuthenticator.ACCOUNT_TYPE);
        for (int i = 0; i < accounts.length; i++) {
            Account account = accounts[i];
            accountManager.removeAccount(account, null, null);
        }
    }

    /**
     * Helper method to trigger an immediate sync ("refresh").
     * <p/>
     * <p>This should only be used when we need to preempt the normal sync schedule. Typically, this means the user has
     * pressed the "refresh" button.
     * <p/>
     * Note that SYNC_EXTRAS_MANUAL will cause an immediate sync, without any optimization to preserve battery life. If
     * you know new data is available (perhaps via a GCM notification), but the user is not actively waiting for that
     * data, you should omit this flag; this will give the OS additional freedom in scheduling your sync request.
     */
    public static void triggerRefresh(Bundle bundle) {
        // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        final Account account = BabylonAccountService.getAccount();
        ContentResolver.requestSync(account, CONTENT_AUTHORITY, bundle);
    }

    public static boolean isSyncActive() {
        final Account account = BabylonAccountService.getAccount();
        boolean result = ContentResolver.isSyncActive(account, CONTENT_AUTHORITY);
        return result;
    }

    public static void invalidateAuthToken(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        accountManager.invalidateAuthToken(BabylonAuthenticator.ACCOUNT_TYPE, null);
    }

//    public static String getAuthToken() {
//        return getAuthToken(null, null);
//    }
//
//    public static String getAuthToken(Handler handler, AccountManagerCallback<Bundle> callBack) {
//        final AccountManager accountManager = AccountManager.get(App.getInstance());
//        Account account = BabylonAccountService.getAccount();
//
//        String authToken = null;
//        if (account != null) {
//            try {
//                AccountManagerFuture<Bundle> bundle = accountManager.getAuthToken(account,
//                        BabylonAuthenticator.AUTH_TOKEN_TYPE, null, null, callBack, handler);
//                authToken = bundle.getResult().getString(AccountManager.KEY_AUTHTOKEN);
//            } catch (OperationCanceledException e) {
//                L.e(TAG, e.toString());
//            } catch (IOException e) {
//                L.e(TAG, e.toString());
//            } catch (AuthenticatorException e) {
//                L.e(TAG, e.toString());
//            }
//        }
//        return authToken;
//    }

    public static Wsse getWsse() {
        Wsse wsse = null;
        Account account = BabylonAccountService.getAccount();
        AccountManager accountManager = AccountManager.get(App.getInstance());
        if (account != null) {
            String authToken = accountManager.getUserData(account,
                    Constants.Header_RequestKeys.HEADER_WSSE_KEY);
            wsse = GsonWrapper.getGson().fromJson(authToken, Wsse.class);
            if (wsse != null) {
                L.d(TAG, wsse.toString());
            }
        }

        return wsse;
    }

    public static void setWsse(Wsse wsse) {
        Account account = BabylonAccountService.getAccount();
        if (account != null) {
            AccountManager accountManager = AccountManager.get(App.getInstance());
            String token = null;
            if (wsse != null) {
                token = wsse.toString();
            }
            accountManager.setUserData(account, Constants.Header_RequestKeys.HEADER_WSSE_KEY, token);

            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.LOGIN_EVENT);
        }
    }

    public static Patient getPatientWithCreds() {
        Patient patient = null;
        Account account = BabylonAccountService.getAccount();
        AccountManager accountManager = AccountManager.get(App.getInstance());
        if (account != null) {
            patient = new Patient();
            patient.setEmail(account.name);
            patient.setPassword(accountManager.getPassword(account));
        }

        return patient;
    }

    public static boolean isLoggedIn() {
        return getWsse() != null;
    }

    public static void signOut() {
        Handler handler = null;
        if (Looper.myLooper() != null) {
            handler = new Handler();
        }
        Account account = BabylonAccountService.getAccount();
        if (account != null) {
            AccountManager accountManager = AccountManager.get(App.getInstance());
            accountManager.removeAccount(account, null, handler);
        }
        setWsse(null);
    }

}

