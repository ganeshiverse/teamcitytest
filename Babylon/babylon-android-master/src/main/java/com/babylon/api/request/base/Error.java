package com.babylon.api.request.base;

import java.io.Serializable;

public class Error implements Serializable {
    String message;
    String userMessage;
    public Error(String userMessage) {
        this(null, userMessage);
    }

    public Error(String message, String userMessage) {
        this.message = message;
        this.userMessage = userMessage;
    }


    public String getMessage() {
        return message;
    }

    public String getUserMessage() {
        return userMessage;
    }
}
