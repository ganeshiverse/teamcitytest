package com.babylon.sync;

import android.content.ContentResolver;
import android.os.Bundle;

import com.babylon.api.request.RegionsGet;
import com.babylon.api.request.base.Response;
import com.babylon.model.Region;
import com.babylon.utils.PreferencesManager;

import org.apache.http.HttpStatus;

public class RegionSync extends AbsSyncStrategy {

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        /* no push method for this strategy */
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        Response<Region[]> response = new RegionsGet().execute();
        if (response.getStatus() == HttpStatus.SC_OK) {
            Region[] regions = response.getData();
            if (regions != null && regions.length != 0) {
                PreferencesManager.getInstance().setRegions(regions);
            }
        }
    }
}
