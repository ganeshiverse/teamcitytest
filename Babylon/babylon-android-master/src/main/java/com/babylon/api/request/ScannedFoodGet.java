package com.babylon.api.request;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.ScannedFood;
import com.babylon.utils.L;

import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ScannedFoodGet extends AbsRequestGet<ResponseList<ScannedFood>> {

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.SCANNED_FOOD_GET);
        return TextUtils.concat(url, urlParams).toString();
    }

    @Override
    public void execute(@NonNull FragmentActivity activity, @NonNull RequestListener<ResponseList<ScannedFood>> requestListener) {
        super.execute(activity, requestListener);
    }

    public static ScannedFoodGet init(String barcode) {

        try {
            barcode = URLEncoder.encode(barcode, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            L.e(TAG, e.getMessage(), e);
        }

        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("barcode", barcode));
        return (ScannedFoodGet)new ScannedFoodGet().withParams(params);
    }
}
