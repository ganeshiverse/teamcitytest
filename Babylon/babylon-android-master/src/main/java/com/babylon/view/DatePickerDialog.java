package com.babylon.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.babylon.R;
import com.babylon.utils.L;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatePickerDialog extends Dialog implements View.OnClickListener {

    private static final String TAG = DatePickerDialog.class.getSimpleName();
    public static final String DAY = "android:id/day";
    public static final String MONTH = "android:id/month";
    public static final String YEAR = "android:id/year";

    private DatePicker datePicker;
    private Date selectedDate;
    private android.app.DatePickerDialog.OnDateSetListener onDateSetListener;
    private String[] pickers;
    private List<String> defaultPickers;

    /**
     * @param pickers use constant pickers from @see DatePickerDialog
     * You can choose what you show in picker : day, month, year
     * If you are not using this param or send null, by default show all pickers
     */
    public DatePickerDialog(Context context, Date selectedDate, android.app.DatePickerDialog.OnDateSetListener
            onDateSetListener, String... pickers) {
        super(context);
        this.selectedDate = selectedDate;
        this.onDateSetListener = onDateSetListener;
        this.pickers = pickers;

        defaultPickers = new ArrayList<>();
        defaultPickers.add(DAY);
        defaultPickers.add(MONTH);
        defaultPickers.add(YEAR);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_date_picker);
        getWindow().setGravity(Gravity.CENTER);
        getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        initViews();

        try {
            show();
        } catch (Exception e) {
            L.e(TAG, "Show dialog error" + e.getMessage(), e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (pickers != null && pickers.length != 0) {
            for (String pickerIdentifier : pickers) {
                defaultPickers.remove(pickerIdentifier);
            }
            for (String pickerIdentifier : defaultPickers) {
                setPickerVisibility(pickerIdentifier, false);
            }
        }
    }

    private void setPickerVisibility(String identifier, boolean isVisible) {
        int pickerIdentifier = getContext().getResources().getIdentifier(identifier, null, null);
        if (pickerIdentifier != 0) {
            View dayPicker = findViewById(pickerIdentifier);
            if (dayPicker != null) {
                dayPicker.setVisibility(isVisible ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void initViews() {
        datePicker = (DatePicker) findViewById(R.id.datePicker);
        Calendar calendar = Calendar.getInstance();

        if (selectedDate != null) {
            calendar.setTime(selectedDate);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            datePicker.updateDate(year, month, day);
        }

        findViewById(R.id.okButton).setOnClickListener(this);
        findViewById(R.id.cancelButton).setOnClickListener(this);
    }

    public void setMaxDate(long millis) {
        datePicker.setMaxDate(millis);
    }

    public void setMinDate(long millis) {
        datePicker.setMinDate(millis);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.okButton:
                onDateSetListener.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(),
                        datePicker.getDayOfMonth());
                dismiss();
                break;

            case R.id.cancelButton:
                dismiss();
                break;

            default:
                L.e(TAG, "Unknown View id");
        }
    }
}
