package com.babylon.activity;

import com.babylon.adapter.MonitorMePagerAdapter;
import com.babylon.utils.DateUtils;

import java.util.Date;

public class LoadingDateRange {

    public final static int PAGES_NUMBER = 5;

    private long minDate;
    private long maxDate;

    public boolean multipleNumber(int pageNumb) {
        boolean result = false;

        int yesterday = 1;
        if (pageNumb == MonitorMePagerAdapter
                .MIN_PAGE_NUMBER) {
            result = false;
        } else if (pageNumb % PAGES_NUMBER == 0 || pageNumb == yesterday) {
            result = true;
        }
        return result;
    }

    public void findDateLimits(int prevPage, int curPage) {
        int selectedDayNumber = getDayNumber(curPage);

        if (curPage <= prevPage) {
            int loadedDayNumber = selectedDayNumber + PAGES_NUMBER;
            minDate = DateUtils.getDayStartMills(-loadedDayNumber + 1);
            maxDate = DateUtils.getDayStartMills(-selectedDayNumber + 1);
        } else {
            int loadedDayNumber = Math.abs(selectedDayNumber - PAGES_NUMBER);
            minDate = DateUtils.getDayStartMills(-selectedDayNumber);
            maxDate = DateUtils.getDayStartMills(-loadedDayNumber);
        }
    }

    public int getDayNumber(int curPage) {
        int selectedDayNumber = MonitorMePagerAdapter.MAX_PAGE_NUMBER - curPage;
        return selectedDayNumber;
    }

    public int getDayNumber(Date date) {
        int diff = DateUtils.daysBetween(date, new Date());
        int dayNumber = MonitorMePagerAdapter.MAX_PAGE_NUMBER - diff;
        return dayNumber;
    }

    public void findDateLimits(int dayNumber) {
        int floor = dayNumber % LoadingDateRange.PAGES_NUMBER;

        int minDayNumber;
        int maxDayNumber;

        minDayNumber = dayNumber - floor;
        if (dayNumber == MonitorMePagerAdapter.MIN_PAGE_NUMBER) {
            maxDayNumber = PAGES_NUMBER;
        } else {
            maxDayNumber = dayNumber + (LoadingDateRange.PAGES_NUMBER - floor);
        }

        if (maxDayNumber > MonitorMePagerAdapter.MAX_PAGE_NUMBER) {
            maxDayNumber = MonitorMePagerAdapter.MAX_PAGE_NUMBER;
        }

        if (minDayNumber <= MonitorMePagerAdapter.MIN_PAGE_NUMBER) {
            minDayNumber = MonitorMePagerAdapter.MIN_PAGE_NUMBER;
        }

        minDate = DateUtils.getDayStartMills(minDayNumber - MonitorMePagerAdapter.MAX_PAGE_NUMBER);
        maxDate = DateUtils.getDayStartMills(maxDayNumber - MonitorMePagerAdapter.MAX_PAGE_NUMBER);
    }

    public long getMinDate() {
        return minDate;
    }

    public long getMaxDate() {
        return maxDate;
    }

}
