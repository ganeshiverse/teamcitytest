package com.iversecomics.client.downloads.internal.android;

import android.content.Context;
import android.os.PowerManager;

public class NetLock {
    private PowerManager          powerMgr = null;
    private PowerManager.WakeLock wakeLock = null;

    public NetLock(Context context) {
        this.powerMgr = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    }

    public void acquire(String tag) {
        wakeLock = powerMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, tag);
        wakeLock.acquire();
    }

    public void release() {
        wakeLock.release();
    }
}
