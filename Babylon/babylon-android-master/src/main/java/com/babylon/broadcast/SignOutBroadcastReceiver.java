package com.babylon.broadcast;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import com.babylon.App;
import com.babylon.Keys;
import com.babylon.activity.SignInActivity;
import com.babylon.sync.SyncUtils;

public class SignOutBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = SignOutBroadcastReceiver.class.getSimpleName();

    public static void sendBroadcast() {
        Intent i = new Intent();
        i.setAction("com.babylon.ACTION_LOGOUT");
        App.getInstance().sendBroadcast(i);
    }

    public static void setEnabled(boolean enabled) {
        ComponentName component = new ComponentName(App.getInstance(), SignOutBroadcastReceiver.class);

        int flag = (enabled
                ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                : PackageManager.COMPONENT_ENABLED_STATE_DISABLED
        );

        App.getInstance().getPackageManager()
                .setComponentEnabledSetting(component, flag, PackageManager.DONT_KILL_APP);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        SyncUtils.signOut();
        setEnabled(false);
      Log.d("Sigoutbroadcast", "sign shown");
        startSignInActivity(context);
    }

    private void startSignInActivity(Context context) {
        if(App.getInstance().isAppOnForeground()) {
            Intent i = new Intent(context, SignInActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
            context.startActivity(i);
        }
    }
}
