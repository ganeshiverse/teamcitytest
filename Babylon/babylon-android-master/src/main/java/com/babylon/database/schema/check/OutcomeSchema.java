package com.babylon.database.schema.check;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;

import static com.babylon.database.check.CheckTables.OUTCOME;

public interface OutcomeSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("check_entity").appendPath(OUTCOME.getName()).build();
    Uri HASH_BASED_OUTCOME_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("symptom_hash_outcome").build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        NAME("name", DBType.TEXT),
        DESCRIPTION("description", DBType.TEXT),
        BODY_PART_ID("bodyPartId", DBType.INT_DEF),
        SORT("sort", DBType.TEXT),
        BOOK_APPOINTMENT("bookAppointment", DBType.INT),
        GENDER("gender", DBType.INT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public interface Query {

        String[] PROJECTION = {
                OUTCOME + "." + Columns._ID.getName(),
                OUTCOME + "." + Columns.ID.getName(),
                OUTCOME + "." + Columns.SYNC_WITH_SERVER.getName(),
                OUTCOME + "." + Columns.NAME.getName(),
                OUTCOME + "." + Columns.DESCRIPTION.getName(),
                OUTCOME + "." + Columns.BODY_PART_ID.getName(),
                OUTCOME + "." + Columns.SORT.getName(),
                OUTCOME + "." + Columns.GENDER.getName(),
                OUTCOME + "." + Columns.BOOK_APPOINTMENT.getName()
        };

        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int NAME = 3;
        int DESCRIPTION = 4;
        int BODY_PART_ID = 5;
        int SORT = 6;
        int BOOK_APPOINTMENT = 7;
        int GENDER = 8;

        String SORT_ID_ASC = OUTCOME.getName() + "." + Columns.ID + " ASC";
    }
}
