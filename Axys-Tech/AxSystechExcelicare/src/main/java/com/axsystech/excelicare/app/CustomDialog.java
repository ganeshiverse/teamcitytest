package com.axsystech.excelicare.app;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;

import com.axsystech.excelicare.R;
import com.mig35.injectorlib.utils.inject.InjectView;

/**
 * This will be used to display the progress loader while performing any background operations.
 */
public class CustomDialog extends Dialog {

    @InjectView(R.id.progressBar1)
    private ProgressBar mProgressBar;

    /**
     * Constructor for instantiating the {@link Dialog}.
     *
     * @param context {@link Context} reference in which the {@link Dialog} is used.
     */
    public CustomDialog(Context context) {
        super(context);
    }

    private CustomDialog mCustomDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_progress_bar);
        mCustomDialog = this;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
