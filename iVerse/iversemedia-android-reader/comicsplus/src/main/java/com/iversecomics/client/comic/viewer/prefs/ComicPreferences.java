package com.iversecomics.client.comic.viewer.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.iversecomics.client.store.ServerConfig;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class ComicPreferences {
//    public static final String HINT_SHOW_FINGER_DRAG = "hint_show_finger_drag";
//    public static final String PREF_EMAIL            = "email";
//    public static final String BYPASS_24_HOUR        = "admin.bypass.24.hour";

    // A unique identifier generated the first time the app is run
    private static final String KEY_INSTALL_ID = "iverse.installid";
    private static final String PREFID = "ComicPrefs";
    public static final String PREF_TRANSITION = "transition";
    public static final String PANEL_NUMS = "panel_nums";

    public enum ComicPreferenceKeys {
        PREF_NOTIFY_FOR_NEW_RELEASES,
        PREF_NOTIFY_FOR_SERIES_UPDATES,
        // Determines whether the badge in the lower right-hand corner indicating that this comic
        // has been purchased is shown or not.  See ComicListFragment
        PREF_SHOW_OVERLAY_ON_PURCHASED_BOOKS,
    }

    private static ComicPreferences sInstance;
    private Context mAppContext;

    public static void initialize(Context context) {
        if (sInstance == null) {
            sInstance = new ComicPreferences(context);
            sInstance.initializeInstallId();
        }
    }

    /**
     * Generates and saves a UUID in SharedPreferences if one has not already been set.
     * The installId is used to identify this installation of the app on the Iverse server.
     */
    private void initializeInstallId() {
        if (!getPrivatePrefs(mAppContext).contains(KEY_INSTALL_ID))
            getPrivatePrefs(mAppContext).edit().putString(KEY_INSTALL_ID, UUID.randomUUID().toString()).apply();
    }

    public static ComicPreferences getInstance() {
        if (sInstance == null)
            throw new IllegalStateException("You must call ComicPreferences.initialize(context) first!");
        else
            return sInstance;
    }

    private ComicPreferences(Context context) {
        mAppContext = context.getApplicationContext();
        // enable current locale's language as default preferred language if no language pref has been set by user
        String defaultLanguageKey = Locale.getDefault().getLanguage(); // returns something like "en" for English
        SharedPreferences prefs = getPrivatePrefs(mAppContext);
        if (!prefs.contains(Locale.getDefault().getLanguage())) {
            prefs.edit().putBoolean(defaultLanguageKey, true).apply();
        }
    }

    public static SharedPreferences getPrivatePrefs(Context context) {
        return context.getSharedPreferences(PREFID, Context.MODE_PRIVATE);
    }

    /**
     * Abstracted Editor save, due to bugs in Android.
     *
     * @param editor the editor to save.
     * @return true if saved successfully, false if otherwise.
     */
    public static boolean save(Editor editor) {
        // TODO: check for editor.commit() when new SDK (post Android 1.0)
        // return editor.commit();

        editor.commit();
        return true; // Always true until we use a later SDK.
    }


    public void setBoolean(ComicPreferenceKeys key, boolean b) {
        getPrivatePrefs(mAppContext).edit().putBoolean(key.name(), b).apply();
    }

    public boolean isSet(ComicPreferenceKeys key, boolean defaultValue) {
        return getPrivatePrefs(mAppContext).getBoolean(key.name(), defaultValue);
    }


    /**
     * @param key A Locale.langauge value, e.g. "en", "fr", etc.  The list of available values is given by ServerConfig.availableLanguages.
     * @param b   true if comics written in this language should be returned when requesting comics from the server.
     */
    public void setPreferredLanguage(String key, boolean b) {
        getPrivatePrefs(mAppContext).edit().putBoolean(key, b).apply();
    }

    public boolean isSet(String key, boolean defaultValue) {
        return getPrivatePrefs(mAppContext).getBoolean(key, defaultValue);
    }

    public boolean isShowPurchaseOverlayShown() {
        return isSet(ComicPreferenceKeys.PREF_SHOW_OVERLAY_ON_PURCHASED_BOOKS, true);
    }

    /**
     * Generates a string of user's preferred language values suitable for use by ServerConfigTask.java
     *
     * @return string of language values with each language separated by a pipe character
     * @see com.iversecomics.client.store.tasks.ServerConfigTask
     */
    public String getPreferredLanguagesServerConfigString() {
        StringBuilder sb = new StringBuilder();
        List<String> availableLanguages = ServerConfig.getDefault().getAvailableLanguages();

        if (availableLanguages == null) {
            return null;
        }

        for (String key : availableLanguages) {
            if (isSet(key, false)) {
                sb.append(key);
                sb.append("|");
            }
        }
        if (sb.length() > 0)
            return sb.toString().substring(0, sb.toString().length() - 1); // remove trailing pipe
        else
            return null;
    }

    public String getVersionString() {
        String version = "?";
        try {
            PackageInfo packageInfo = mAppContext.getPackageManager().getPackageInfo(mAppContext.getPackageName(), 0);
            // Need to localize
            version = String.format("Version %s (Build %s)", packageInfo.versionName, packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(ComicPreferences.class.getSimpleName(), "getVersionString error", e);
        }

        return version;
    }


    public String getInstallId() {
        return getPrivatePrefs(mAppContext).getString(KEY_INSTALL_ID, "unknown");
    }

}
