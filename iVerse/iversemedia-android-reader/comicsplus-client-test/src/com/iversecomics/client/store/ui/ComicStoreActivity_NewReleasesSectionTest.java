package com.iversecomics.client.store.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.ListView;

import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.R;
import com.iversecomics.client.store.ui.adapters.ComicListAdapter;
import com.iversecomics.client.store.ui.adapters.FeaturedListAdapter;

public class ComicStoreActivity_NewReleasesSectionTest extends AbstractComicStoreActivityTestCase {
    public ComicStoreActivity_NewReleasesSectionTest() {
        super();

        Intent intent = new Intent();
        intent.putExtra("testing", true);
        intent.putExtra(ComicStoreActivity.BUNDLE_ACTIVE_PAGE, NewReleasesComicsFragment.TAG);
        setActivityIntent(intent);
    }

    private void assertHasAdapterCountAtLeast(String id, AbsListView adapterView, int minCount) {
        Adapter adapter = adapterView.getAdapter();
        assertNotNull("View[" + id + "].getAdapter() should not be null", adapter);
        OurAsserts.assertAtLeast("View[" + id + "].adapter.count", minCount, adapter.getCount());
    }

    private void assertIsVisible(String id, View view) {
        assertNotNull("View [" + id + "] should exist", view);
        assertEquals("View [" + id + "] should be visible", view.getVisibility(), View.VISIBLE);
    }

    @LargeTest
    public void testOnResumeLoadListFromServer() throws Throwable {
        final ComicStoreActivity activity = getActivity();
        assertNotNull("Activity should have launched", activity);

        waitForRefreshTasks(activity);

        Fragment fragment = activity.getActiveFragment();
        MoreAsserts.assertAssignableFrom(NewReleasesComicsFragment.class, fragment);

        NewReleasesComicsFragment nrelFrag = (NewReleasesComicsFragment) fragment;
        ComicListAdapter listadapter = nrelFrag.getListAdapter();
        FeaturedListAdapter gridadapter = nrelFrag.getGridAdapter();
        assertNotNull("ComicListAdapter should not be null", listadapter);
        assertNotNull("FeaturedListAdapter should not be null", gridadapter);

        refresh(listadapter);
        refresh(gridadapter);

        OurAsserts.assertAtLeast("ComicListAdapter.count", 20, listadapter.getCount());
        OurAsserts.assertAtLeast("FeaturedListAdapter.count", 4, gridadapter.getCount());

        ListView listView = (ListView) nrelFrag.getView().findViewById(R.id.listing);
        GridView featView = (GridView) nrelFrag.getView().findViewById(R.id.featured_grid);

        assertIsVisible("R.id.listing", listView);
        assertIsVisible("R.id.featured_grid", featView);

        assertHasAdapterCountAtLeast("R.id.listing", listView, 20);
        assertHasAdapterCountAtLeast("R.id.featured_grid", featView, 4);
    }
}
