package com.iversecomics.client.store.ui.sidemenu;

public class SideMenuItemInfo
{
	public enum ItemType
	{
		Account,
		Promotions,
		Navigation,
		ItemTitles,
		ItemPublishers
	};

	public ItemType itemType;
	public String 	title;
	public String	imageURL;
    /** data is typically a Promotion, Group, or String constant. */
	public Object	data;
    public boolean imageLoaded = false;
    public boolean hasSubMenu = false;

	public SideMenuItemInfo()
	{

	}

    public SideMenuItemInfo(ItemType type, String title, String imageURL, Object data)
    {
        this.itemType = type;
        this.title = title;
        this.imageURL = imageURL;
        this.data = data;
    }

	public SideMenuItemInfo(ItemType type, String title, String imageURL, Object data, boolean hasSubMenu)
	{
        this(type, title, imageURL, data);
        this.hasSubMenu = hasSubMenu;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

    public boolean hasSubMenu() {
        return hasSubMenu;
    }

    public void setHasSubMenu(boolean hasSubMenu) {
        this.hasSubMenu = hasSubMenu;
    }

}