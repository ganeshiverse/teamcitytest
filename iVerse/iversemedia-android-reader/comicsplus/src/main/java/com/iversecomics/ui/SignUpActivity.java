package com.iversecomics.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.EditText;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Preferences;
import com.iversecomics.client.account.PasswordRequestActivity;
import com.iversecomics.client.account.SignUpTask;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;

import java.net.URI;

public class SignUpActivity extends Activity implements View.OnClickListener {
    private static final String TAG = SignUpActivity.class.getSimpleName();

    EditText userEmail;
    EditText userPassword;
    ProgressDialog progressDialog;
    SignupResponseReceiver signupResponseReceiver = new SignupResponseReceiver();


    private class SignupResponseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            String action = intent.getAction();
            if (SignUpTask.SIGN_UP_SERVER_RESPONSE.equals(action)) {
                final Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    final String jsonResponse = bundle.getString("response");
                    JSONObject json = null;
                    try {
                        json = new JSONObject(jsonResponse);

                    } catch (JSONException e) {
                        Log.e(TAG, "json exception", e);
                    }

                    if (json != null) {
                        final String status = json.optString("status");

                        // success!
                        if (status.equals("ok")) {
                            commitLoginToPrefs(bundle.getString("userName"), bundle.getString("userPassword"));
                            // login activity will pull default username & password from prefs
                            startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                            finish();
                            return;
                        } else {
                            JSONObject errorObj = null;
                            int errorCode = 0;
                            try {
                                errorObj = (JSONObject) json.get("error");
                                errorCode = errorObj.getInt("errorCode");
                            } catch (JSONException e) {
                                Log.e(TAG, "json exception", e);
                            }

                            // 102 = duplicate user error
                            if (errorCode == 102) {
                                final Resources res = getResources();
                                final String message = String.format(res.getString(R.string.sign_up_existing_account_message), bundle.getString("userName"));

                                final AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                                builder.setIcon(R.drawable.icon)
                                        .setTitle(R.string.sign_up_existing_account_title)
                                        .setMessage(message)
                                        .setPositiveButton(android.R.string.ok, null)
                                        .setNegativeButton(R.string.login_incorrect_remind_button, new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                startActivity(new Intent(getApplicationContext(), PasswordRequestActivity.class));
                                            }
                                        }).create().show();
                                return;
                            }
                        }
                    }

                    // Default, unhandled error
                    final AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                    builder.setIcon(R.drawable.icon)
                            .setTitle(R.string.account_creation_fail_title)
                            .setMessage(R.string.account_creation_fail_message)
                            .setPositiveButton(android.R.string.ok, null);
                    builder.create().show();
                }
            }
        }
    }

    void commitLoginToPrefs(final String userEmail, final String userPassword) {
        final SharedPreferences preferences = getSharedPreferences(Preferences.NAME, 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString(Preferences.KEY_OWNER_EMAIL, userEmail);
        editor.putString(Preferences.KEY_OWNER_PASSWORD, userPassword);

        final IverseApplication app = IverseApplication.getApplication();
        app.getOwnership().setEmailAddress(userEmail);
        app.getOwnership().setPassword(userPassword);

        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_privacy);

        final WebView webView = (WebView) findViewById(R.id.privacy_policy_body);
        ServerConfig.getDefault().getLoginMovieUri();
        URI privacyUri = ServerConfig.getDefault().getPrivacyPolicyUri();
        Log.i(TAG, "webview url: " + privacyUri);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // force redirect to load in this webview
                // for example: <meta HTTP-EQUIV="REFRESH" content="0; url=http://iversemedia.com/privacy/archie/en_privacy.php">
                view.loadUrl(url);
                return false;
            }
        });

        if (privacyUri != null)
            webView.loadUrl(privacyUri.toString());
    }

    @Override
    public void onResume() {
        super.onResume();

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SignUpTask.SIGN_UP_SERVER_RESPONSE);
        LocalBroadcastManager.getInstance(this).registerReceiver(signupResponseReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(signupResponseReceiver);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_button:
                createAccount();
                break;
            case R.id.back_button:
                finish();
                break;
        }
    }

    public void onBackClick(View v) {
        finish();
    }


    public void onAgreeClick(View v) {

        // Show the signup view
        setContentView(R.layout.activity_signup_user_info);

        userEmail = (EditText) findViewById(R.id.user_email);
        userPassword = (EditText) findViewById(R.id.user_password);

        findViewById(R.id.signup_button).setOnClickListener(this);
        findViewById(R.id.back_button).setOnClickListener(this);

        return;
    }


    private void createAccount() {

        final String password = userPassword.getText().toString();
        final String email = userEmail.getText().toString();

        // User must be over 13 years of age
        CheckBox over13 = (CheckBox) findViewById(R.id.over_13);
        if (!over13.isChecked()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
            builder.setIcon(R.drawable.icon)
                    .setTitle(R.string.sign_up_over_13_error_title)
                    .setMessage(R.string.sign_up_over_13_error_msg)
                    .setPositiveButton(android.R.string.ok, null);
            builder.create().show();

            return;
        }

        // validate email address
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            makeInvalidEmailDialog();
            return;
        }
        //password req. capital, lowercase and numeric - at least 5 characers long.
        final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{5,})";

        if (!password.matches(PASSWORD_PATTERN)) {
            userPassword.setText("");
            makeInvalidPasswordDialog();
            return;
        }

        final IverseApplication app = (IverseApplication) getApplication();

        //not all skus have an opt in option in the sign up, so account for that when resolving the
        //dialog's user responses
        int resID = getResources().getIdentifier("marketing_opt_in", "id", app.getPackageName());

        final boolean marketingOptIn = resID > 0 ?
                ((CheckBox) findViewById(resID)).isChecked() :
                false;

        final SignUpTask task = new SignUpTask(app.getComicStore());
        task.setUserName(email);
        task.setUserPassword(password);
        task.setMarketingOptIn(marketingOptIn);

        app.getTaskPool().submit(task);
        progressDialog = ProgressDialog.show(SignUpActivity.this, "", "Creating account...", true);
    }

    void makeInvalidEmailDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.icon)
                .setTitle(R.string.invalid_email_title)
                .setMessage(R.string.invalid_email_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // NA
                    }
                }).create().show();
    }

    void makeInvalidPasswordDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.icon)
                .setTitle(R.string.invalid_password_title)
                .setMessage(R.string.invalid_password_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // NA
                    }
                }).create().show();
    }
}
