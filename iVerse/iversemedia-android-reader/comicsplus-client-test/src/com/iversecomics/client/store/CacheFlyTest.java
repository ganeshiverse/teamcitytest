package com.iversecomics.client.store;

import java.io.InputStream;
import java.net.URI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.util.Dim;
import com.iversecomics.client.util.ResourceUtil;
import com.iversecomics.io.IOUtil;

public class CacheFlyTest extends AbstractStubActivityTestCase {
    private void assertValidImageUri(Uri uri, Dim expectedDim) throws Exception {
        // Attempt to download the image into a Bitmap.
        URI imageUri = URI.create(uri.toString());
        InputStream in = null;
        try {
            in = imageUri.toURL().openStream();
            Bitmap bitmap = BitmapFactory.decodeStream(in);
            assertEquals("Image.width [" + uri + "]", expectedDim.getWidth(), bitmap.getWidth());
            assertEquals("Image.height [" + uri + "]", expectedDim.getHeight(), bitmap.getHeight());
            ResourceUtil.free(bitmap);
        } finally {
            IOUtil.close(in);
        }
    }

    @MediumTest
    public void testGetFeatureImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getFeatureImageUri("0000001501");
        assertValidImageUri(uri, Dim.FEATURED);
    }

    @MediumTest
    public void testGetiPadMediumImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getiPadMediumImageUri("0000000612");
        assertValidImageUri(uri, Dim.COVER_TABLET_MEDIUM);
    }

    @MediumTest
    public void testGetLargeFeatureImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getLargeFeatureImageUri("0000001525");
        assertValidImageUri(uri, Dim.FEATURED_LARGE);
    }

    @MediumTest
    public void testGetLargeImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getLargeImageUri("0000001290");
        assertValidImageUri(uri, Dim.COVER_LARGE);
    }

    @MediumTest
    public void testGetMediumImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getMediumImageUri("0000000609");
        assertValidImageUri(uri, Dim.COVER_MEDIUM);
    }

    @MediumTest
    public void testGetPublisherBannerImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getPublisherBannerImageUri("publisher_1821");
        assertValidImageUri(uri, Dim.PUBLISHER_BANNER);
    }

    @MediumTest
    public void testGetPublisherLargeBannerImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getPublisherLargeBannerImageUri("publisher_boom");
        assertValidImageUri(uri, Dim.PUBLISHER_BANNER_LARGE);
    }

    @MediumTest
    public void testGetPublisherLogoImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getPublisherLogoImageUri("image_comics");
        assertValidImageUri(uri, Dim.PUBLISHER_ICON);
    }

    @MediumTest
    public void testGetSmallImageUri() throws Exception {
        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getSmallImageUri("0000000600");
        assertValidImageUri(uri, Dim.COVER_SMALL);
    }
}
