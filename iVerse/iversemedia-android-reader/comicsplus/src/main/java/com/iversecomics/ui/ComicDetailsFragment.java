package com.iversecomics.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.ShareManager;
import com.iversecomics.client.ShareManager.OnSocialPostListener;
import com.iversecomics.client.Storage;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.MyComicsModel.PossessionState;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.OnDemandOfflineQueue;
import com.iversecomics.client.store.Price;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.tasks.FetchComicTask;
import com.iversecomics.client.store.tasks.SubmitProductRatingTask;
import com.iversecomics.client.store.tasks.VerifyPurchaseTask;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.client.util.UIUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.ComicReconciledEvent;
import com.iversecomics.otto.event.DownloadProgressChangeEvent;
import com.iversecomics.otto.event.SubscriptionSaveOffLineErrorEvent;
import com.iversecomics.store.AppStoreAdapterLocator;
import com.iversecomics.store.ComicBuyer;
import com.iversecomics.ui.main.MainActivity;
import com.iversecomics.ui.widget.NavigationBar;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComicDetailsFragment extends Fragment implements View.OnClickListener {

    public static final String ARG_BUNDLE_NAME = "com.iverse.BUNDLE_NAME";
    public static final String ARG_COMIC_ID = "com.iverse.COMIC_ID";
    private static final Logger LOG = LoggerFactory.getLogger(ComicDetailsFragment.class);
    View comicDetailView;
    Button btnRemove;
    Button btnPreview;
    Button btnPublisher;
    Button btnUseXPoints;
    Button btnViewSeries;
    Button btnShare;
    Button btnReadOnDemand;
    Button btnPurchase;
    String mDownloadProgressString;

    IverseApplication mApp;

    RatingBar.OnRatingBarChangeListener ratingChangeListener = new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating,
                                    boolean fromUser) {
            if (fromUser) {
                mRatingBar.setRating(rating);

                MyComic mycomic = mMyComicsModel.getComicByBundleName(mComic.getComicBundleName());
                mycomic.setMyRating((double) rating);
                mMyComicsModel.update(mycomic);
                final int ratingValue = (int) rating;

                final IverseApplication app = IverseApplication.getApplication();
                final String deviceId = app.getOwnership().getUniqueId();
                //Create and submit the product rating task
                SubmitProductRatingTask task = new SubmitProductRatingTask(app.getComicStore());
                task.setRating(ratingValue);
                task.setDeviceId(deviceId);
                task.setComicBundleName(mycomic.getComicId());
                app.getTaskPool().submit(task);
            }
        }
    };
    private View pleaseWaitView;
    private String mComicBundleName;
    private String mComicId;
    private Comic mComic;
    private OnComicRefreshedListener onComicRefreshListener;
    private ComicObserver comicObserver;
    private ContentResolver contentResolver;
    private MyDownloadProgressReceiver progressReceiver;
    private MyComicsModel mMyComicsModel;
    private ComicBuyer mComicBuyer;
    private AbstractAppStoreAdapter appStoreAdapter;

    private RatingBar mRatingBar;
    private Bus ottoBus;
    Future<?> serverConfigFuture;

    Activity mActivity;

    //Added for checking"Coming soon" functionality.
    private boolean isOrientationChanged=true;

    public static ComicDetailsFragment newInstanceForComicBundleName(String comicBundleName) {
        ComicDetailsFragment frag = new ComicDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_BUNDLE_NAME, comicBundleName);
        frag.setArguments(args);
        return frag;
    }

    public static ComicDetailsFragment newInstanceForComicId(String comicId) {
        ComicDetailsFragment frag = new ComicDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_COMIC_ID, comicId);
        frag.setArguments(args);
        return frag;
    }

    public void setAppStoreAdapter(AbstractAppStoreAdapter appStoreAdapter) {
        this.appStoreAdapter = appStoreAdapter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApp = IverseApplication.getApplication();
        mActivity = this.getActivity();
        //Util.exportDB();
        if (getActivity() instanceof AppStoreAdapterLocator) {
            appStoreAdapter = ((AppStoreAdapterLocator) getActivity()).getAppStoreAdapter();
        } else {
            throw new IllegalArgumentException("Invalid activity. Must implement AppStoreAdapterLocator.");
        }
        mComicBuyer = new ComicBuyer(getActivity(), appStoreAdapter);
        progressReceiver = new MyDownloadProgressReceiver();
        ottoBus = OttoBusProvider.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_comic_details, container, false);

        NavigationBar navBar = (NavigationBar) view.findViewById(R.id.navbar);
        navBar.setNavigationListener((NavigationBar.NavigationListener) getActivity());

        this.pleaseWaitView = view.findViewById(R.id.please_wait_overlay);
        this.comicDetailView = view.findViewById(R.id.comic_details_panel);

        UIUtil.setTextView(view, R.id.details, "");

        if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
            if (AppConstants.ONDEMAND_BUTTON_TITLE != null)
                UIUtil.setTextView(view, R.id.btnReadOnDemand, getResources().getString(R.string.btnreadondemand));
        }
        mRatingBar = (RatingBar) view.findViewById(R.id.rating);

        btnPreview = (Button) view.findViewById(R.id.btnViewPreview);
        btnPublisher = (Button) view.findViewById(R.id.btnPublisher);
        btnUseXPoints = (Button) view.findViewById(R.id.btnUsePoints);
        if(AppConstants.ENABLE_TAPJOY){
            btnUseXPoints.setVisibility(View.VISIBLE);
        }else{
            btnUseXPoints.setVisibility(View.GONE);
        }
        btnShare = (Button) view.findViewById(R.id.btnShare);
        btnReadOnDemand = (Button) view.findViewById(R.id.btnReadOnDemand);
        btnViewSeries = (Button) view.findViewById(R.id.btnViewSeries);
        btnRemove = (Button) view.findViewById(R.id.btnRemove);
        btnPurchase = (Button) view.findViewById(R.id.btnPurchase);

        mDownloadProgressString = getResources().getString(R.string.downloading_progress);

        btnPublisher.setOnClickListener(this);
        btnUseXPoints.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnReadOnDemand.setOnClickListener(this);
        btnViewSeries.setOnClickListener(this);

        if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE){
            btnReadOnDemand.setVisibility(View.VISIBLE);
        }else {
            btnReadOnDemand.setVisibility(View.INVISIBLE);
        }

        Bundle args = getArguments();
        if (args != null) {
            mComicBundleName = args.getString(ARG_BUNDLE_NAME);
            mComicId = args.getString(ARG_COMIC_ID);
        }

        if (AppConstants.SKU_SPECIFIC_TITLES) {
            view.findViewById(R.id.btnPublisher).setVisibility(View.GONE);
            view.findViewById(R.id.btnShare).setVisibility(View.GONE);
        }

        return view;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        comicObserver = new ComicObserver(new Handler());

        contentResolver = getActivity().getContentResolver();
        contentResolver.registerContentObserver(ComicsTable.CONTENT_URI, false, comicObserver);

        if (mComicBundleName != null)
            mComic = mApp.getComicStore().getDatabaseApi().getComicByBundleName(mComicBundleName);
        else if (mComicId != null)
            mComic = mApp.getComicStore().getDatabaseApi().getComicById(mComicId);

        // joliver: previously this was called by the parent activity when the ServerConfig became available
//        enableFragment(true);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnUsePoints:

                int costPoints = (int) (Float.parseFloat(mComic.getAmountUSD()) * 100);
                int userPoints = mApp.getOwnership().getUserPoints();

                // buy it with tapjoy points
                if (userPoints >= costPoints) {
                    VerifyPurchaseTask task = new VerifyPurchaseTask(getActivity(), mApp.getComicStore(), mComic, false);
                    task.setPurchaseWithTapjoy(true);
                    mApp.getTaskPool().submit(task);
                }
                // not enough tapjoy points for this purchase
                else { // show tap joy connect

                }
                break;

            case R.id.btnViewSeries:
                ((MainActivity) getActivity()).showTopFragment(ComicListFragment.newInstanceForSeriesId(mComic.getSeriesBundleName()));
                break;

            case R.id.btnReadOnDemand:

                ServerConfig serverConfig = ServerConfig.getDefault();

                if (serverConfig.isSubscribed()) {
                    // validate that the subscription end date has not passed
                    if (serverConfig.getSubscriptionEndDate().after(new Date())) {
                        // fetch the OnDemand/Unlimited data (basically just a list of page ids that will be supplied to
                        // another web service to fetch the individual page images as needed.
                        showSubScriptionOfflineDialog(btnReadOnDemand);

                    } else {
                        // subscription may have expired or been canceled, we need to check the
                        // subscription status against the app store and then update the iverse server
                        // with the new subscription information.
                        showsubScriptionDetailsDialog(getString(R.string.subscription_expired_title),getString(R.string.subscription_expired),true);

                    }

                } else {

                    mComicBuyer.purchaseSubscription();
                }
                break;
        }
    }

    public void prepareReadAndRateState() {
        btnPurchase.setText(R.string.read_now);
        btnPurchase.setClickable(true);
        btnPurchase.setBackgroundResource(R.drawable.button_green);
        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readComic(false);
            }
        });

        // TODO: joliver This was commented out before I migrated it into the comicsplus project, is it needed?
//		final Button rate = (Button) getView().findViewById(R.id.btnRateIt);
//		rate.setVisibility(View.VISIBLE);
//		rate.setBackgroundResource(android.R.drawable.btn_default);
//		rate.setOnClickListener(new StoreRateItClickListener(this, mComic));
    }

    public void enableFragment(boolean enabled) {
        if (!enabled) {
            return;
        }

        // Fetch mComic if necessary
        if (mComic == null) {
            if (mComicBundleName != null) {
                mApp.getTaskPool().submit(new FetchComicTask(mApp.getComicStore(), mComicBundleName));
            } else if (mComicId != null) {
                // TODO: joliver, how do we fetch a mComic by id?
                // getTaskPool().submit(new FetchComicTask
            }
        }

        pleaseWaitView.setVisibility(View.GONE);

        // Set the values
        final View view = getView();

        UIUtil.setTextView(view, R.id.comic_title, mComic.getName());
        // UIUtil.setTextView(view, R.id.publisher, mComic.getPublisherName());
        // UIUtil.setTextView(view, R.id.comicType, mComic.getProductType());

        mRatingBar.setRating((float) mComic.getAvgRating());

        UIUtil.setTextView(view, R.id.details, mComic.getDescription());

        ((RatingBar) view.findViewById(R.id.rating)).setIsIndicator(true);

        String points = getResources().getString(R.string.usexpoints,
                String.valueOf((int) (Float.parseFloat(mComic.getAmountUSD()) * 100)));
        if(AppConstants.ENABLE_TAPJOY){
            UIUtil.setTextView(view, R.id.btnUsePoints, points);
        }



        btnRemove.setVisibility(View.GONE);
        btnPreview.setVisibility(View.VISIBLE);

        // Figure out color for purchase button. (hook it up)
        mMyComicsModel = new MyComicsModel(getActivity());
        PossessionState possessionState = mMyComicsModel.isOwned(mComic);

        if (possessionState == PossessionState.AVAILABLE) {
            // mComic is on device and ready to read
            prepareReadAndRateState();
            btnPreview.setVisibility(View.GONE);
            btnUseXPoints.setVisibility(View.GONE);

            //show remove button
            btnRemove.setVisibility(View.VISIBLE);
            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyComic myComic = mMyComicsModel.getComicByBundleName(mComicBundleName);
                    showDeleteComicDialog(myComic);
                }
            });

            ((RatingBar) view.findViewById(R.id.rating)).setIsIndicator(false);
            ((RatingBar) view.findViewById(R.id.rating)).setOnRatingBarChangeListener(ratingChangeListener);

        } else if (possessionState == PossessionState.PURCHASED) {
            // mComic has been purchased but the cb is not downloaded
            btnUseXPoints.setVisibility(View.GONE);

            btnPurchase.setText(R.string.download_comic);
            btnPurchase.setBackgroundResource(android.R.drawable.btn_default);
            btnPurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mComicBuyer.purchaseComic(view, mComic);
                }
            });

        } else if (mComic.getPricingTier() == 0) {
            // Free
            //btnPurchase.setBackgroundResource(R.drawable.green_button);

            btnPurchase.setText(R.string.free);
            btnPurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mComicBuyer.purchaseComic(view, mComic);
                }
            });

            btnUseXPoints.setVisibility(View.GONE);
        } else {
            // Paid
            //btnPurchase.setBackgroundResource(R.drawable.blue_button);
            // Need to handle "Coming soon" functionality.
            if(appStoreAdapter !=null){
                appStoreAdapter.checkSkuIdinGooglePlay(mComic.getSku());
            }

            final String amount = Price.getComicPrice(mComic);
            btnPurchase.setText("$" + amount);

            btnPurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mComicBuyer.purchaseComic(view, mComic);
                }
            });
            if(AppConstants.ENABLE_TAPJOY) {
                btnUseXPoints.setVisibility(View.VISIBLE);
            }
        }

        final ImageView coverImageView = (ImageView) view.findViewById(R.id.cover);
        Uri coverImageUri = ServerConfig.getDefault().getLargeImageUri(mComic.getImageFileName());
        ImageLoader.getInstance().displayImage(coverImageUri.toString(), coverImageView);

        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Offfline support change
                if(ConnectionHelper.isConnectedToInternet(getActivity())) {
                    readComic(true);
                }else {
                    ConnectionHelper.showNoInternetMessage();
                }
            }
        });

        if (!mComic.isTapjoy()) {
            this.btnUseXPoints.setVisibility(View.GONE);
        }

        if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
            int onDemandVisibility = mComic.getSubscription() ? View.VISIBLE : View.GONE;
            view.findViewById(R.id.btnReadOnDemand).setVisibility(onDemandVisibility);
            // joliver: On Demand is the same as Unlimited?
            view.findViewById(R.id.ondemand).setVisibility(onDemandVisibility);

            boolean onDemandBtnEnabled = possessionState != PossessionState.AVAILABLE && possessionState != PossessionState.PURCHASED;
            view.findViewById(R.id.btnReadOnDemand).setEnabled(onDemandBtnEnabled);
        }
        ShareManager sManager = IverseApplication.getApplication()
                .getShareManager();
        sManager.configurePostComic(this.getActivity(), this.btnShare, mComic,
                new OnSocialPostListener() {
                    @Override
                    public void onPosted(boolean success, String provider) {
                        Toast.makeText(mApp
                                ,
                                success ? R.string.shared
                                        : R.string.share_failed,
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public OnComicRefreshedListener getOnComicRefreshListener() {
        return onComicRefreshListener;
    }

    public void setOnComicRefreshListener(OnComicRefreshedListener onComicRefreshListener) {
        this.onComicRefreshListener = onComicRefreshListener;
    }


    @Override
    public void onResume() {
        super.onResume();

        ottoBus.register(this);
        ottoBus.register(progressReceiver);
        enableFragment(true);
        // TODO: joliver I think we need to refresh ServerConfig so that the value for our tapjoy
        // points will be accurate after returning from tapjoy activity.
    }

    private void readComic(boolean previewMode) {
        final Intent intent = new Intent(getActivity(), ComicReaderPhotoViewActivity.class);
        intent.putExtra(ComicReaderPhotoViewActivity.EXTRA_COMIC_BUNDLE_NAME, mComic.getComicBundleName());
        intent.putExtra(ComicReaderPhotoViewActivity.EXTRA_PREVIEW, previewMode);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        ottoBus.unregister(this);
        ottoBus.unregister(progressReceiver);
    }

    @Override
    public void onDestroy() {
        if (contentResolver != null && comicObserver != null)
            contentResolver.unregisterContentObserver(comicObserver);
        IverseApplication.getApplication().getShareManager().releaseResources();

        super.onDestroy();
    }

    public void refreshFragment(boolean force) {
        if (mComicBundleName != null) {
            Task task = new FetchComicTask(mApp.getComicStore(), mComicBundleName);
            if (force)
                mApp.getTaskPool().submit(task);
            else
                mApp.getTaskPool().submitIfExpired(mApp.getFreshness(), task);
        }
    }

    private void fetchOnDemandComicBundle(final String productId) {

        final ComicStore.ServerApi server = mApp.getComicStore().getServerApi();
        final IverseApplication app = (IverseApplication)getActivity().getApplication();
        final String deviceId = app.getOwnership().getUniqueId();

        new AsyncTask<String, Void, OnDemandResponse>() {
            @Override
            protected OnDemandResponse doInBackground(String... params) {

                JSONObject payload = new JSONObject();
                try {
                    payload.put("productId", productId);
                    payload.put("streamPages", true);
                    payload.put("deviceId", deviceId);
                    payload.put("installId", ComicPreferences.getInstance().getInstallId());
                } catch (JSONException e) {
                    LOG.error(e, "FetchOnDemandComicTask, error building payload json");
                }
                OnDemandResponse resp = null;
                try {
                    JSONObject respJson = server.submitOnDemandComicRequest(payload);
                    resp = new Gson().fromJson(respJson.toString(), OnDemandResponse.class);

                } catch (ComicStoreException e) {
                    LOG.error(e, "Unable to fetch on demand comic, productId:%s", productId);
                }
                return resp;
            }

            @Override
            protected void onPostExecute(OnDemandResponse resp) {
                if (resp != null) {
                    LOG.info("FetchOnDemandComicTask, response json:%s", resp.toString());
                    if (resp.isError()) {
                        // TODO: show error dialog.
                        String msg = resp.error != null ? resp.error.errorMessage : resp.status;
                        Toast.makeText(getActivity(), getResources().getString(R.string.error) + msg, Toast.LENGTH_SHORT).show();
                    } else {
                        // Success!
//                        final Intent intent = new Intent(getActivity(), ComicReaderActivity.class);
                        final Intent intent = new Intent(mActivity, ComicReaderPhotoViewActivity.class);
                        intent.putExtra(ComicReaderPhotoViewActivity.EXTRA_COMIC_BUNDLE_NAME, mComic.getComicBundleName());
                        intent.putStringArrayListExtra(ComicReaderPhotoViewActivity.EXTRA_ONDEMAND_PAGES, resp.pages);
                        startActivity(intent);
                    }
                } else {
                    // TODO: show error dialog.
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_in_processing_request), Toast.LENGTH_SHORT).show();
                }
            }

        }.execute();
    }


    private void removeOfflineProduct(final String productId, final MyComic comic) {

        final ComicStore.ServerApi server = mApp.getComicStore().getServerApi();
        final IverseApplication app = IverseApplication.getApplication();
        final String deviceId = app.getOwnership().getUniqueId();

        new AsyncTask<String, Void, OnDemandResponse>() {
            @Override
            protected OnDemandResponse doInBackground(String... params) {

                JSONObject payload = new JSONObject();
                try {
                    payload.put("productId", productId);
                    payload.put("streamPages", false);
                    payload.put("deviceId", deviceId);
                    payload.put("installId", ComicPreferences.getInstance().getInstallId());
                    payload.put("removeFromQueue", true);
                    } catch (JSONException e) {
                    LOG.error(e, "FetchOnDemandComicTask, error building payload json");
                }
                OnDemandResponse resp = null;
                try {
                    JSONObject respJson = server.submitOnDemandComicRequest(payload);
                    resp = new Gson().fromJson(respJson.toString(), OnDemandResponse.class);

                } catch (ComicStoreException e) {
                    LOG.error(e, "Unable to fetch on demand comic, productId:%s", productId);
                }
                return resp;
            }

            @Override
            protected void onPostExecute(OnDemandResponse resp) {
                if (resp != null) {
                    LOG.info("FetchOnDemandComicTask, response json:%s", resp.toString());
                    if (resp.isError()) {
                        // TODO: show error dialog.
                        String msg = resp.error != null ? resp.error.errorMessage : resp.status;
                        if(getActivity()!=null && isAdded())
                            Toast.makeText(getActivity(), getResources().getString(R.string.error) + msg, Toast.LENGTH_SHORT).show();
                    } else {
                        // Success!
                        final MyComicsModel model = new MyComicsModel(((MainActivity) getActivity()));
                        model.delete(comic);
                        try {
                            Storage.assertAvailable();
                            final File comicAsset = new File(Storage.getExternalComicsStorageDir(), comic.getAssetFilename());
                            comicAsset.delete();
                            // After deleting comic we are reducing the array count.
                            removeOnDemandOfflineQueueItem(productId);
//                            final Task task = new ServerConfigTask(getActivity(), app.getComicStore());
//                            serverConfigFuture = app.getTaskPool().submit(task);
                            getFragmentManager().popBackStack();
                        } catch (Exception e) {
                            LOG.error("error deleting comic: " + e.getLocalizedMessage());
                            showStorageOptions();
                        }
                    }
                } else {
                    // TODO: show error dialog.
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_in_processing_request), Toast.LENGTH_SHORT).show();
                }
            }

        }.execute();
    }

    @Subscribe
    public void onSubscriptionQueFullError(SubscriptionSaveOffLineErrorEvent event) {
        final String errorCode = event.getErrorCode();
        final String errorMessage = event.getErrorMessage();
        if (errorCode.equals("10981")) {
            //TODO - Show alert for offline Queue >6 case
        }
    }
    /**
     * This method id used to delete the OnDemandOfflineQueue item from the parsed OnDemandOfflineQueue list. this item will be deleted locally.
     * @param productID - for the deleted OnDemandOfflineQueue item
     */
    private void removeOnDemandOfflineQueueItem(String productID){
        for(int i=0; i<ServerConfig.getDefault().getOnDemandOfflineQueue().size();i++){
            OnDemandOfflineQueue offlineQue = ServerConfig.getDefault().getOnDemandOfflineQueue().get(i);
            if(productID.equalsIgnoreCase(offlineQue.getComic_bundle_name())){
                LOG.debug("OnDemandOfflineQueue Comic Removed",offlineQue.getName());
                ServerConfig.getDefault().getOnDemandOfflineQueue().remove(i);
                break;
            }

        }
    }

    private void showDeleteComicDialog(final MyComic comic) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(com.iversecomics.archie.android.R.string.delete_comic_title)
                .setIcon(android.R.drawable.stat_sys_warning)
                .setMessage(com.iversecomics.archie.android.R.string.delete_comic_message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //If the product is "Unlimited product", -  we need to update the server when we delete the product.
                        // because the offline que is limited to 6 only.
                        if (comic.getOnDemand() == true) {
                            removeOfflineProduct(comic.getComicId(), comic);
                            return;
                        }
                        // deletion of a normal product.
                        final MyComicsModel model = new MyComicsModel(getActivity());
                        model.delete(comic);
                        try {
                            Storage.assertAvailable();
                            final File comicAsset = new File(Storage.getExternalComicsStorageDir(), comic.getAssetFilename());
                            comicAsset.delete();
                            getFragmentManager().popBackStack();
                        } catch (Exception e) {
                            LOG.error("error deleting comic: " + e.getLocalizedMessage());
                            showStorageOptions();
                        }
                    }
                })
                .setNegativeButton(com.iversecomics.archie.android.R.string.cancel, null);
        builder.create().show();

    }

    private void showStorageOptions() {
        // presumably something went wrong accessing external storage
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(com.iversecomics.archie.android.R.string.storage_unavailable_dialog_title)
                .setIcon(com.iversecomics.archie.android.R.drawable.microsd_warn)
                .setMessage(getString(com.iversecomics.archie.android.R.string.storage_unavailable_dialog_message))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(com.iversecomics.archie.android.R.string.settings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // show storage options if that's what we want
                        final Intent intent = new Intent(android.provider.Settings.ACTION_INTERNAL_STORAGE_SETTINGS);
                        startActivity(intent);
                    }
                });
        builder.create().show();
    }



    private void updateDownloadProgress(int progress) {
        if (btnPurchase != null) {
            btnPurchase.setText(String.format(Locale.getDefault(), "%s %d%%", mDownloadProgressString, progress));
            btnPurchase.setBackgroundResource(android.R.drawable.btn_default);
            btnPurchase.setClickable(false);
            btnPurchase.setOnClickListener(null);
        }
    }

    public static interface OnComicRefreshedListener {
        public void onComicRefreshedListener(Comic comic);
    }

    private class ComicObserver extends ContentObserver {
        public ComicObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            LOG.debug("onChange()");

            if (mComicBundleName != null)
                mComic = mApp.getComicStore().getDatabaseApi().getComicByBundleName(mComicBundleName);
            else if (mComicId != null)
                mComic = mApp.getComicStore().getDatabaseApi().getComicById(mComicId);

            // Notify those interested the Comic details were refreshed.
            if (onComicRefreshListener != null)
                onComicRefreshListener.onComicRefreshedListener(mComic);

            // Show the updated details
            // TODO: joliver, this
//            enableView(true);
        }
    }

    public class MyDownloadProgressReceiver {

        @Subscribe
        public void onDownloadProgressChange(DownloadProgressChangeEvent event) {
            final String uri = event.getUri();

            // Extract mComic bundle name from the download uri so that we
            // can display the download progress if this is a progress broadcast
            // for the mComic this fragment is displaying.
            final Pattern p = Pattern.compile("(com\\..+)\\.cbsd");
            final Matcher m = p.matcher(uri);
            final String name = m.find() ? m.group(1) : "";

            // download is in progress
            if (name.equals(mComic.getComicBundleName())) {
                final long currentBytes = event.getCurrentByte();
                final long totalBytes = event.getTotalBytes();
                final int progress = (int) ((currentBytes * 100) / totalBytes);

                updateDownloadProgress(progress);
            }
        }

        @Subscribe
        public void onDownloadProgressChange(ComicReconciledEvent event) {
            final String name = event.getName();

            if (name.equals(mComic.getComicBundleName())) {
                prepareReadAndRateState();
            }
        }

    }

    class OnDemandResponse implements Serializable {
        public MetaData metadata;
        public ArrayList<String> pages;
        /*
            example error response: {"error":{"errorCode":1005,"errorMessage":"Subscription expired or invalid"},"status":"error"}
        */
        public String status;
        public ErrorData error;

        public boolean isHorizontalOnly() {
            return metadata != null && metadata.horizontal_fades != 0;
        }

        public boolean isRemotePages() {
            return true;
        }

        public boolean isError() {
            return error != null || (status != null && status.equals("error"));
        }

        class MetaData {
            public String name;
            public String product_id;
            public String series_product_id;
            public String synopsis;
            public String product_type;
            public String description;
            public String publisher_id;
            public String publisher_name;
            public int manga_orientation;
            public int horizontal_fades;
        }

        class ErrorData {
            public int errorCode;
            public String errorMessage;
        }

    }
    /**
     * This method is used to update the price button status. if the item available in Google play store to buy, We can set the price of the item to the button.
     * If the item not available in Google play store, We update the item as "Coming soon".
     * @param isProductavailavleInPlayStore
     */
    public void updateComingSoon(boolean isProductavailavleInPlayStore) {
        if (!isProductavailavleInPlayStore) {
            btnPurchase.setText(getResources().getString(R.string.coming_soon));
            btnPurchase.setClickable(false);
        } else {
            final String amount = Price.getComicPrice(mComic);
            btnPurchase.setText("$" + amount);
        }
    }

    /**
     * Displays a subscription expired dialog and gives a chance to user to buy a new subscription.
     * @param title - title of the dialog
     * @param message - message to be displayed
     * @param buyNewSubscription - yes/no
     */
    private void showsubScriptionDetailsDialog(String title, String message, final boolean buyNewSubscription) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(buyNewSubscription){
                            mComicBuyer.purchaseSubscription();
                            }
                    }
                });
        if(buyNewSubscription) {
            builder.setNegativeButton(com.iversecomics.archie.android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                }
            });
        }
        builder.create().show();

    }

    /**
     * Displays a subscription store in offline dialog.
     */
    private void showSubScriptionOfflineDialog(final View btnview) {
        final ArrayList<OnDemandOfflineQueue> onDemandOfflineQueue = ServerConfig.getDefault().getOnDemandOfflineQueue();
        int offlineQueCount= 0;
        if(onDemandOfflineQueue !=null && onDemandOfflineQueue.size()>0)
            offlineQueCount = 6-onDemandOfflineQueue.size();
        String message  = getString(R.string.subscription_offline_alert, offlineQueCount+"");

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.ondemand))
                    .setMessage(message)
                    .setPositiveButton(getResources().getString(R.string.read_now), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            fetchOnDemandComicBundle(mComicBundleName);
                        }
                    });
            // if offlineQueCount < 6 means user inly stores up to six books in his device, if offlineQueCount>6 he can only read the comics.

            builder.setNegativeButton(getResources().getString(R.string.save_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                    if(onDemandOfflineQueue !=null && onDemandOfflineQueue.size() >=6){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getResources().getString(R.string.error_title))
                                .setMessage(getActivity().getString(R.string.offline_max_cpunt_reached))
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
//                                        fetchOnDemandComicBundle(mComicBundleName);
                                    }
                                });
                        builder.create().show();

                    }else {
                        if (mComicBundleName != null) {
                            mComic = mApp.getComicStore().getDatabaseApi().getComicByBundleName(mComicBundleName);
                            mComicBuyer.downloadSubscriptioBook(mComic);
                        }
                    }
                }
            });
            builder.create().show();

        }

}
