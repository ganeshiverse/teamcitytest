package com.iversecomics.client.my;

import android.database.Cursor;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.my.db.AbstractMyComicsProviderTest;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.my.db.MyComicsTable;
import com.iversecomics.client.util.DBUtil;

public class MyComicsModelTest extends AbstractMyComicsProviderTest {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testGetCursorMyComics_Empty() {
        MyComicsModel model = getMyComicsModel();
        Cursor c = null;
        try {
            c = model.getCursorMyComics();
            assertNotNull("Should have a cursor", c);
            assertFalse("Should have no first", c.moveToFirst());
            assertTrue("Should be after lase", c.isAfterLast());
            assertEquals("Row count", 0, c.getCount());
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testGetCursorMyComics_Multi() {
        MyComicsModel model = getMyComicsModel();

        String ids[] = { "Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet" };
        for (String id : ids) {
            MyComic comic = new MyComic();
            comic.setComicId("test." + id.toLowerCase());
            comic.setName(id);
            Uri uri = model.insert(comic);
            assertDatabaseUriHasDBId("MyComic", uri);
        }

        Cursor c = null;
        try {
            c = model.getCursorMyComics();
            assertNotNull("Should have a cursor", c);
            assertTrue("Should have a first", c.moveToFirst());
            assertEquals("Row count", ids.length, c.getCount());
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testGetCursorMyComics_Single() {
        MyComicsModel model = getMyComicsModel();

        MyComic comic = new MyComic();
        comic.setComicId("test.comic");
        comic.setName("Test Comic");
        Uri uri = model.insert(comic);
        assertDatabaseUriHasDBId("MyComic", uri);

        Cursor c = null;
        try {
            c = model.getCursorMyComics();
            assertNotNull("Should have a cursor", c);
            assertTrue("Should have a first", c.moveToFirst());
            assertEquals("Row count", 1, c.getCount());
            MyComic actual = MyComicsTable.fromCursor(c);
            assertEquals("comic.id", comic.getComicId(), actual.getComicId());
            assertEquals("comic.name", comic.getName(), actual.getName());
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testInsertUpdateByAssetName() {
        MyComicsModel model = getMyComicsModel();

        // Create initial comics
        String ids[] = { "Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet" };
        for (String id : ids) {
            MyComic comic = new MyComic();
            comic.setComicId("test." + id.toLowerCase());
            comic.setName(id);
            comic.setAssetFilename("/test/colors/" + id + ".txt");
            comic.setAssetFilesize(555L);
            Uri uri = model.insert(comic);
            assertDatabaseUriHasDBId("MyComic", uri);
        }

        // Update entries with 'o' in name.
        for (String id : ids) {
            if (!id.toLowerCase().contains("o")) {
                continue;
            }
            String assetFilename = "/test/colors/" + id + ".txt";
            MyComic comic = new MyComic();
            comic.setComicId("test." + id.toLowerCase());
            comic.setName(id);
            comic.setAssetFilename(assetFilename);
            comic.setAssetFilesize(777L);
            model.updateByAssetName(assetFilename, comic);
        }

        Cursor c = null;
        try {
            c = model.getCursorMyComics();
            assertNotNull("Should have a cursor", c);
            assertTrue("Should have a first", c.moveToFirst());
            assertEquals("Row count", ids.length, c.getCount());
            while (!c.isAfterLast()) {
                MyComic actual = MyComicsTable.fromCursor(c);
                long actualFilesize = actual.getAssetFilesize();
                if (actual.getComicId().contains("o")) {
                    assertEquals(actual.getComicId() + ".assetFilesize (updated)", 777L, actualFilesize);
                } else {
                    assertEquals(actual.getComicId() + ".assetFilesize", 555L, actualFilesize);
                }
                c.moveToNext();
            }
        } finally {
            DBUtil.close(c);
        }
    }
}
