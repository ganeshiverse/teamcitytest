package com.iversecomics.client.store.tasks;

import java.io.IOException;

import org.acra.ErrorReporter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.io.Digest;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.ServerConfigErrorEvent;

public class ServerConfigTask extends ComicStoreTask {
    private static final Logger LOG           = LoggerFactory.getLogger(ServerConfigTask.class);

    private Context             context;
    protected JSONObject          payload;
    private Freshness           freshness;
    private String              taskId;
    private static int 			SERVER_CONFIG_RETRY_COUNT = 3;

    public ServerConfigTask(Context context, ComicStore comicStore) {
        super(comicStore);
        // hack, to let ServerConfigTask be detected when duplicated in TaskPool
        this.taskId = this.getClass().getName();
        this.context = context;
        this.payload = new JSONObject();
        this.freshness = new Freshness(context);

        ServerConfig config = ServerConfig.getDefault();

        final StringBuilder verifyCode = new StringBuilder();

        final String appId = context.getPackageName();
        final String deviceId = AndroidInfo.getUniqueDeviceID(context);

        final IverseApplication app =  IverseApplication.getApplication();
        final String userAccount = app.getOwnership().getEmailAddress();
        final String userPassword = app.getOwnership().getPassword();

        verifyCode.append(appId);
        verifyCode.append(deviceId);
        verifyCode.append(SHARED_SECRET);

        try {
            payload.put("appID", appId);
            payload.put("appVersion", AndroidInfo.getApplicationVersion(context));
            payload.put("deviceLocale", AndroidInfo.getLocale(context));
            payload.put("deviceModel", AndroidInfo.getDeviceModel());
            payload.put("deviceUDID", deviceId);
            payload.put("deviceID", deviceId); // extra, not expected by server
            payload.put("deviceOS", "Android"); // extra, not expected by server
            payload.put("deviceVersion", AndroidInfo.getOSVersion());
            payload.put("preferredLanguages", ComicPreferences.getInstance().getPreferredLanguagesServerConfigString());

            //Only send login and password if we have both
            if (userAccount.length() > 0 && userPassword.length() > 0) {
                payload.put("userAccount", userAccount);
                payload.put("userPassword", userPassword);
            }

            payload.put("verifyCode", Digest.sha1(verifyCode.toString()));
        } catch (JSONException e) {
            LOG.error(e, "Unable to create JSON payload!?");
            ErrorReporter.getInstance().handleSilentException(e);
        } catch (IOException e) {
            LOG.error(e, "Unable to create Verify Code!?");
            ErrorReporter.getInstance().handleSilentException(e);
        }
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ServerConfigTask other = (ServerConfigTask) obj;
        if (taskId == null) {
            if (other.taskId != null) {
                return false;
            }
        } else if (!taskId.equals(other.taskId)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
    	boolean registered = false;

        final ServerApi server = comicStore.getServerApi();

        LOG.debug("payload = %s", payload.toString(2));

        while (!registered) {
            try {
                final JSONObject resp = server.getServerConfig(payload);
                final ServerConfig config = ServerConfig.getDefault();
                config.setFromServerResponse(resp);
                config.updateFreshness(freshness);

                if (config.isConfigured()) {
                	registered = true;

                	//set user points
                    IverseApplication.getApplication().getOwnership().setUserPoints(config.getUserPoints());

                    // Notify event subscribers that config has changed
                    OttoBusProvider.getInstance().post(ServerConfig.getDefault());
                }
            } catch (Exception e) {

                LOG.warn(e, "Unable to register");
                OttoBusProvider.getInstance().post(new ServerConfigErrorEvent(e.getLocalizedMessage()));
            }
        }
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        final ServerConfig config = ServerConfig.getDefault();
        return (config.isConfigured() && config.isFresh(freshness));
    }

    private void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ignore) {
            // Ignore
        }
    }
}
