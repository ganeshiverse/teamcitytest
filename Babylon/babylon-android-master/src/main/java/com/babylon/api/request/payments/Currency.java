package com.babylon.api.request.payments;

import com.google.gson.annotations.SerializedName;

public class Currency {
    private String id;
    @SerializedName("iso_code")
    private String isoCode;
    @SerializedName("iso_numeric")
    private String isoNumeric;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoNumeric() {
        return isoNumeric;
    }

    public void setIsoNumeric(String isoNumeric) {
        this.isoNumeric = isoNumeric;
    }
}
