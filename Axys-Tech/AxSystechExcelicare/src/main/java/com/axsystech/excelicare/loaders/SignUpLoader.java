package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.SignUpDataModel;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SignUpLoader extends DataAsyncTaskLibLoader<Object> {

    private boolean isSignUpTypeFull;
    private SignUpDataModel signUpDataModel;
    private String deviceId;
    private String isPrivacyAgreed;
    private boolean shouldAddNewProxie;
    private long ecUserId;

    public SignUpLoader(final Context context, boolean signUpType, SignUpDataModel signUpDataModel,
                        String deviceId, String isPrivacyAgreed, boolean shouldAddNewProxie, long ecUserId) {
        super(context);
        this.isSignUpTypeFull = signUpType;
        this.signUpDataModel = signUpDataModel;
        this.deviceId = deviceId;
        this.isPrivacyAgreed = isPrivacyAgreed;
        this.shouldAddNewProxie = shouldAddNewProxie;
        this.ecUserId = ecUserId;
    }

    @Override
    protected Object performLoad() throws Exception {
        return ServerMethods.getInstance().registerNewUser(isSignUpTypeFull, signUpDataModel, deviceId, isPrivacyAgreed, shouldAddNewProxie, ecUserId);
    }
}