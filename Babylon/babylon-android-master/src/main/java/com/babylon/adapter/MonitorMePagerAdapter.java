package com.babylon.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.babylon.fragment.MonitorMeFragment;

public class MonitorMePagerAdapter extends FragmentStatePagerAdapter {
    public static int MAX_PAGE_NUMBER = 365;
    public static int MIN_PAGE_NUMBER = 0;

    public MonitorMePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return MonitorMeFragment.getInstance(MAX_PAGE_NUMBER - position);
    }

    @Override
    public int getCount() {
        return MAX_PAGE_NUMBER + 1;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}
