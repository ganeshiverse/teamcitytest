package com.babylon.fragment;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.model.Patient;
import com.babylon.service.TrackingService;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.AutoTrackedActivity;


public class LocationTrackingFragment extends BaseFragment {

    private static final String TAG = LocationTrackingFragment.class.getSimpleName();

    /**
     * True, when service is running.
     */
    private boolean isRunning;

    private TextView distanceTextView;
    private TextView caloriesTextView;
    private TextView stepsTextView;
    private CheckBox switchTrackingCheckBox;

    private TrackingService service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_motion_tracking, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        distanceTextView = (TextView) view.findViewById(R.id.fragment_motion_tracking_distance_value_text_view);
        caloriesTextView = (TextView) view.findViewById(R.id.fragment_motion_tracking_calories_value_text_view);
        stepsTextView = (TextView) view.findViewById(R.id.fragment_motion_tracking_steps_value_text_view);
        switchTrackingCheckBox = (CheckBox) view.findViewById(R.id.fragment_motion_tracking_check_box);

        setTitle(R.string.location_tracking_fragment_title);

        switchTrackingCheckBox.setChecked(PreferencesManager.getInstance().isTrackingActivity());
        switchTrackingCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switchTrackingActivity(isChecked);
            }
        });

        Patient patient = App.getInstance().getPatient();
        if (patient == null || patient.getWeight() == null
                || patient.getWeight().equals(0f)) {
            AlertDialogHelper.getInstance().showMessage(getActivity(), R.string.location_tracking_weight_alert);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isRunning = isMyServiceRunning();
        if (isRunning) {
            bindTrackingStepService();
        } else {
            startTrackingStepService();
            bindTrackingStepService();
            PreferencesManager.getInstance().setTrackingActivity(true);
            switchTrackingCheckBox.setChecked(true);
        }
    }

    @Override
    public void onPause() {
        if (isRunning) {
            unbindTrackingStepService();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        AutoTrackedActivity.save(getActivity().getContentResolver());
        super.onDestroy();
    }

    private void startTrackingStepService() {
        if (!isRunning) {
            L.i(TAG, "Tracking Service start");
            isRunning = true;
            getActivity().startService(new Intent(App.getInstance(),
                    TrackingService.class));
        }
    }

    private void bindTrackingStepService() {
        L.i(TAG, "Tracking Service bind");
        getActivity().bindService(new Intent(App.getInstance(),
                TrackingService.class), mConnection, Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
    }

    private void unbindTrackingStepService() {
        L.i(TAG, "Tracking Service unbind");
        getActivity().unbindService(mConnection);
    }

    private void stopTrackingTrackingStepService() {
        if (service != null) {
            L.i(TAG, "Tracking Service stopService");
            getActivity().stopService(new Intent(App.getInstance(),
                    TrackingService.class));
        }
        isRunning = false;
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null
                && manager.getRunningServices(Integer.MAX_VALUE) != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (TrackingService.class.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            service = ((TrackingService.TrackingStepBinder) binder).getService();
            service.registerCallback(mCallback);
            service.reloadSettings();
        }

        public void onServiceDisconnected(ComponentName className) {
            service = null;
        }
    };

    private TrackingService.ICallback mCallback = new TrackingService.ICallback() {
        public void stepsChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
        }

        public void paceChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(PACE_MSG, value, 0));
        }

        public void distanceChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG, (int) (value * 1000), 0));
        }

        public void speedChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(SPEED_MSG, (int) (value * 1000), 0));
        }

        public void caloriesChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(CALORIES_MSG, (int) (value), 0));
        }
    };

    private static final int STEPS_MSG = 1;
    private static final int PACE_MSG = 2;
    private static final int DISTANCE_MSG = 3;
    private static final int SPEED_MSG = 4;
    private static final int CALORIES_MSG = 5;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isVisible()) {
                switch (msg.what) {
                    case STEPS_MSG:
                        int mStepValue = msg.arg1;
                        stepsTextView.setText("" + mStepValue);
                        break;
                    case PACE_MSG:
                        break;
                    case DISTANCE_MSG:
                        float mDistanceValue = msg.arg1 / 1000f;
                        String distance = "0";
                        if (mDistanceValue > 0) {
                            try {
                                distance = ("" + (mDistanceValue + 0.000001f)).substring(0, 5);
                            } catch (StringIndexOutOfBoundsException e) {
                                L.e(TAG, e.getMessage(), e);
                            }
                        }
                        distanceTextView.setText(getString(R.string.location_tracking_km_pattern,
                                distance));
                        break;
                    case SPEED_MSG:
                        break;
                    case CALORIES_MSG:
                        int mCaloriesValue = msg.arg1;
                        caloriesTextView.setText(mCaloriesValue <= 0
                                ? "0" : getString(R.string.location_tracking_calories_pattern,
                                mCaloriesValue));
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        }

    };

    private void switchTrackingActivity(boolean isOn) {
        if (isOn) {
            startTrackingStepService();
            bindTrackingStepService();
        } else {
            unbindTrackingStepService();
            stopTrackingTrackingStepService();
        }
    }
}
