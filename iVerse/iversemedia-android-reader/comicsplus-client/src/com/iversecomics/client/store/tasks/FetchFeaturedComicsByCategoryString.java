package com.iversecomics.client.store.tasks;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.db.OrderingUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchFeaturedComicsByCategoryString extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(FetchFeaturedComicsByCategoryString.class);
    private String              categoryID;
    private String              listId;
    private Context				context;

    public FetchFeaturedComicsByCategoryString(ComicStore comicStore, Context context, String categoryID) {
        super(comicStore);
        this.categoryID = categoryID;
        this.listId = ListID.getFeaturedComicsByCategoryId(categoryID);
        this.context = context;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchFeaturedComicsByCategoryString other = (FetchFeaturedComicsByCategoryString) obj;
        if (listId == null) {
            if (other.listId != null) {
                return false;
            }
        } else if (!listId.equals(other.listId)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getFeaturedComicsListByCategoryString(categoryID);
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            updater.addComicDBSubUpdater(new OrderingUpdater(listId));

            ResponseParser parser = new ResponseParser();
            parser.parseComics(json, updater);
            server.updateFreshness(listId, Time.HOUR * 3);
            
            final Intent intent = new Intent(FEATURED_ITEMS_UPDATED);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update featured comics list by-category id: " + categoryID);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listId == null) ? 0 : listId.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(listId);
    }
}