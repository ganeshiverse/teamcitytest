package com.iversecomics.client.store.events;

public interface IPublisherChooser {
    OnPublisherClickListener getOnPublisherClickListener();

    void setOnPublisherClickListener(OnPublisherClickListener listener);
}
