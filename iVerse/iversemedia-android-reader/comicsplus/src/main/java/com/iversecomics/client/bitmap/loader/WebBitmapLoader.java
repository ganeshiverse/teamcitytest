package com.iversecomics.client.bitmap.loader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iversecomics.client.bitmap.IBitmapLoader;
import com.iversecomics.client.net.HTTPClient;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

public class WebBitmapLoader implements IBitmapLoader {
    private static final Logger LOG = LoggerFactory
            .getLogger(WebBitmapLoader.class);
    private HTTPClient http;

    public WebBitmapLoader(HTTPClient http) {
        this.http = http;
    }

    @Override
    public String[] getUriSchemes() {
        return new String[]{"http", "https"};
    }

    private Bitmap loadBitmap(HttpEntity entity) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = entity.getContent();
            ByteArrayOutputStream datastream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(datastream, IOUtil.BUFFER_SIZE);
            IOUtil.copy(in, out);
            out.flush();

            final byte[] data = datastream.toByteArray();
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            return BitmapFactory.decodeByteArray(data, 0, data.length, opts);
        } catch (IOException e) {
            LOG.error(e, "Unable to load image");
            return null;
        } finally {
            IOUtil.close(out);
            IOUtil.close(in);
        }
    }

    @Override
    public Bitmap loadBitmap(URI bitmapUri) throws IOException {
        HttpEntity entity = null;
        try {
            HttpGet get = new HttpGet(bitmapUri);
            HttpResponse response = http.execute(get);
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != HttpStatus.SC_OK) {
                String err = "Request not succesful: " + status.getStatusCode() + " " + status.getReasonPhrase();
                err += "\n" + bitmapUri.toASCIIString();
                LOG.warn(err);
                return null;
            }

            entity = response.getEntity();
            return loadBitmap(entity);
        } finally {
            try {
                http.close(entity);
            } catch (Exception e) {

            }

        }
    }
}
