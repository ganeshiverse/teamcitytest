package com.axsystech.excelicare.framework.ui.messages;

import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.adapters.MessagesInboxAndSentAdapter;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.GetMessageListFromDbLoader;
import com.axsystech.excelicare.loaders.GetMsgInboxLoader;
import com.axsystech.excelicare.loaders.MsgInboxAndSentSearchLoader;
import com.axsystech.excelicare.loaders.SaveMessageListDBLoader;
import com.axsystech.excelicare.loaders.SavePriorityLoader;
import com.axsystech.excelicare.loaders.UpdateMsgPriorityInDbLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MessageInboxFragment extends BaseFragment implements View.OnClickListener, TextWatcher {

    private static final String MSG_INBOX_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment.MSG_INBOX_LOADER";
    private static final String MSG_SAVE_INBOX_IN_DB_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment.MSG_SAVE_INBOX_IN_DB_LOADER";
    private static final String GET_MSG_INBOX_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment.GET_MSG_INBOX_FROM_DB_LOADER";

    private static final String MSG_INBOX_SEARCH = "com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment.MSG_INBOX_SEARCH";

    private static final String MSG_SAVE_PRIORITY = "com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment.MSG_SAVE_PRIORITY";
    private static final String UPDATE_MSG_PRIORITY_IN_DB = "com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment.UPDATE_MSG_PRIORITY_IN_DB";

    private String TAG = MessageInboxFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;
    private MessageInboxFragment mFragment;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.listView)
    private ListView listView;

    //@InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.hintTextView)
    private TextView mHintTextView;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;
    private MessagesInboxAndSentAdapter messageInboxAdapter;

    private ArrayList<MessageListItemDBModel> messageListItemDBModelsAL;

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));
        mActivity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.changepasswordactionbar_color)));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message_inbox, container, false);
        searchEditText = (EditText) view.findViewById(R.id.searchEditText);
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        searchEditText.setTypeface(regular);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        preloadData();
        addEventListeners();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void preloadData() {
        searchEditText.setHint("Search by Name ,Specialty");
        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            GetMsgInboxLoader msgInboxLoader = new GetMsgInboxLoader(mActivity,
                    (mActiveUser != null ? mActiveUser.getToken() : ""), AxSysConstants.MSG_INBOX_PANEL_ID,
                    (mActiveUser != null ? mActiveUser.getPatientID() : ""), 1, 100, 1);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_INBOX_LOADER), msgInboxLoader);
            }
        } else {
            displayProgressLoader(false);
            // fetch list of inbox messages from DB and display them in ListView
            GetMessageListFromDbLoader getMessageListFromDbLoader = new GetMessageListFromDbLoader(mActivity, AxSysConstants.MSG_INBOX_PANEL_ID,
                    mActiveUser.getEcUserID());
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MSG_INBOX_FROM_DB_LOADER), getMessageListFromDbLoader);
        }
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String searchEdit = null;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        try {
                            searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        //                    String filterCriteria = "Sender like \'%" + searchEditText.getText().toString() + "%\' or Message like \'%" + searchEditText.getText().toString() + "%\'";
                        MsgInboxAndSentSearchLoader msgSentLoader = null;
                        //                    try {
                        msgSentLoader = new MsgInboxAndSentSearchLoader(mActivity, mActiveUser.getToken(), AxSysConstants.MSG_INBOX_PANEL_ID,
                                mActiveUser.getPatientID(), 1, 100, 1, /*URLEncoder.encode(filterCriteria, "UTF-8")*/ searchEdit);
                        //                    } catch (UnsupportedEncodingException e) {
                        //                        e.printStackTrace();
                        //                    }
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_INBOX_SEARCH), msgSentLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_message, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            case R.id.action_msg_delete:
                break;

            case R.id.action_msg_uncheckbox:
                messageInboxAdapter = new MessagesInboxAndSentAdapter(mActivity, mFragment, messageListItemDBModelsAL, true, true);
                listView.setAdapter(messageInboxAdapter);
                break;
            case R.id.action_msg_compose:
                final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                final FragmentTransaction ft = activityFragmentManager.beginTransaction();
                MessageComposeFragment messageComposeFragment = new MessageComposeFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_compose_message));
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_ALL);
                messageComposeFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                ft.commit();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(MSG_INBOX_LOADER)) {
            showToast(R.string.invalid_server_response);
        } else if (id == getLoaderHelper().getLoaderId(MSG_INBOX_SEARCH)) {
            showToast(R.string.no_search_results_found);
        } else if (id == getLoaderHelper().getLoaderId(MSG_SAVE_PRIORITY)) {
            showToast(R.string.error_updating_priority);
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(MSG_INBOX_LOADER)) {
            if (result != null && result instanceof MessagesInboxAndSentResponse) {
                MessagesInboxAndSentResponse msgInboxResponse = (MessagesInboxAndSentResponse) result;

                if (msgInboxResponse.getPanel() != null) {
                    ArrayList<MessagesInboxAndSentResponse.DataObject> msgDataObjectsAL = msgInboxResponse.getPanel().getDataObjectsAL();

                    if (msgDataObjectsAL != null && msgDataObjectsAL.size() > 0) {
                        // Cache message list in DB table and then display them in ListView
                        displayProgressLoader(false);

                        SaveMessageListDBLoader saveMessageListDBLoader = new SaveMessageListDBLoader(mActivity, msgDataObjectsAL, AxSysConstants.MSG_INBOX_PANEL_ID,
                                mActiveUser.getSiteID(), mActiveUser.getEcUserID());
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_SAVE_INBOX_IN_DB_LOADER), saveMessageListDBLoader);
                    } else {
                        showToast(R.string.error_msg_inbox_empty);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(MSG_SAVE_INBOX_IN_DB_LOADER)) {
            if (result != null && result instanceof Boolean) {
                boolean statusFlag = (boolean) result;

                // fetch list of inbox messages from DB and display them in ListView
                GetMessageListFromDbLoader getMessageListFromDbLoader = new GetMessageListFromDbLoader(mActivity,
                        AxSysConstants.MSG_INBOX_PANEL_ID, mActiveUser.getEcUserID());
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MSG_INBOX_FROM_DB_LOADER), getMessageListFromDbLoader);
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_MSG_INBOX_FROM_DB_LOADER)) {
            if (result != null && result instanceof List) {
                messageListItemDBModelsAL = (ArrayList<MessageListItemDBModel>) result;

                if (messageListItemDBModelsAL != null && messageListItemDBModelsAL.size() > 0) {
                    // update to list adapter
                    messageInboxAdapter = new MessagesInboxAndSentAdapter(mActivity, mFragment, messageListItemDBModelsAL, true, false);
                    listView.setAdapter(messageInboxAdapter);
                } else {
                    listView.setVisibility(View.GONE);
                    mHintTextView.setVisibility(View.VISIBLE);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(MSG_INBOX_SEARCH)) {
            if (result != null && result instanceof MessagesInboxAndSentResponse) {
                MessagesInboxAndSentResponse mMsgSentResponse = (MessagesInboxAndSentResponse) result;

                if (mMsgSentResponse.getPanel() != null) {
                    final ArrayList<MessagesInboxAndSentResponse.DataObject> searchResultsDataObjectsAL = mMsgSentResponse.getPanel().getDataObjectsAL();

                    if (searchResultsDataObjectsAL != null && searchResultsDataObjectsAL.size() > 0) {
                        ArrayList<MessageListItemDBModel> dbModelsAL = ConverterResponseToDbModel.getMessageListDbModelsAL(searchResultsDataObjectsAL,
                                AxSysConstants.MSG_INBOX_PANEL_ID,
                                mActiveUser.getSiteID(), mActiveUser.getEcUserID());

                        // update to list adapter with search items
                        if (messageInboxAdapter != null) {
                            messageInboxAdapter.updateData(dbModelsAL);
                        }
                    } else {
                        showToast(R.string.no_search_results_found);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(MSG_SAVE_PRIORITY)) {
            if (result != null && result instanceof BaseResponse) {
                BaseResponse baseResponse = (BaseResponse) result;

                if (baseResponse.isSuccess()) {
                    if (!TextUtils.isEmpty(baseResponse.getMessage())) {
                        showToast(baseResponse.getMessage());

                        // Update the priority of particular message in DB
                        UpdateMsgPriorityInDbLoader updateMsgPriorityInDbLoader = new UpdateMsgPriorityInDbLoader(mActivity, priorityUpdatedMsgId, updatedPriority);
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(UPDATE_MSG_PRIORITY_IN_DB), updateMsgPriorityInDbLoader);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(UPDATE_MSG_PRIORITY_IN_DB)) {
            if (result != null && result instanceof Boolean) {
                boolean statusFlag = (boolean) result;


            }
        }
    }

    private String priorityUpdatedMsgId = "";
    private String updatedPriority = "Medium"; // "High" : "Medium"


    public void onMessagePriorityChanged(String messageId, boolean isChecked) {
        Trace.d(TAG, "messageId:" + messageId + "|| isChecked:" + isChecked);

        priorityUpdatedMsgId = messageId;

        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);

            String priorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
            updatedPriority = "Medium";
            if (isChecked) {
                priorityLU = AxSysConstants.HIGH_PRIORITY_ID;
                updatedPriority = "High";
            }

            SavePriorityLoader savePriorityLoader = new SavePriorityLoader(mActivity, messageId, priorityLU, mActiveUser.getToken());
            if (getView() != null && isAdded()) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_SAVE_PRIORITY), savePriorityLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                // update to list adapter with original items
                if (messageInboxAdapter != null) {
                    messageInboxAdapter.updateData(messageListItemDBModelsAL);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }
}