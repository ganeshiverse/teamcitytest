package com.babylon.api.request;

import android.text.TextUtils;
import android.text.format.DateUtils;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Food;
import com.babylon.model.NutrientCategory;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class NutrientRecentGet extends AbsRequestGet<ResponseList<Food>> {
    private static final String DATE = "date";
    private static final String CATEGORY = "category";

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.NUTRIENT_RECENT);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param date date for which the recent food is needed , timestamp mills
     */
    public static NutrientRecentGet newNutrientRecentGet(long date, NutrientCategory category) {
        final List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair(DATE, String.valueOf(date / DateUtils.SECOND_IN_MILLIS)));
        params.add(new BasicNameValuePair(CATEGORY, String.valueOf(category.getValue())));

        return (NutrientRecentGet) new NutrientRecentGet().withParams(params);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }
}
