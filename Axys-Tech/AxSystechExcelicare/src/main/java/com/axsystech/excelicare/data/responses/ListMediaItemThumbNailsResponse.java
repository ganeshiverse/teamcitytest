package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hkyerra on 7/8/2015.
 */
public class ListMediaItemThumbNailsResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<MediaItemServerModel> mediaItemServerModelArrayList;

    public ArrayList<MediaItemServerModel> getMediaItemServerModelArrayList() {
        return mediaItemServerModelArrayList;
    }

    public class MediaItemServerModel {
        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        @SerializedName("createdDate")
        private String createdDate;

        @SerializedName("previewMedia")
        private String previewMedia;

        @SerializedName("LMD")
        private String lmd;

        @SerializedName("comments")
        private String comments;

        @SerializedName("subfolderId")
        private int subfolderId;

        @SerializedName("folderId")
        private int folderId;

        @SerializedName("fileName")
        private String fileName;

        @SerializedName("FileDuration")
        private String fileDuration;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public String getPreviewMedia() {
            return previewMedia;
        }

        public String getLmd() {
            return lmd;
        }

        public String getComments() {
            return comments;
        }

        public int getSubfolderId() {
            return subfolderId;
        }

        public int getFolderId() {
            return folderId;
        }

        public String getFileName() {
            return fileName;
        }

        public String getFileDuration() {
            return fileDuration;
        }
    }
}
