package com.iversecomics.client.my.db;

import android.net.Uri;

import com.iversecomics.client.util.ConfigProj;

public final class MyComicsDB {
    public static final String AUTHORITY   = ConfigProj.getSkuPackageName() +".my";
    /**
     * The content:// style URL for this table
     */
    protected static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String DB_NAME     = "my_comics.db";
    public static final int    DB_VERSION  = 2;
}
