package com.babylon.api.request.triage;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;
import com.babylon.model.ServerModel;
import com.babylon.model.check.CheckOutcome;
import com.babylon.model.check.CheckResult;
import com.babylon.model.check.SymptomAnswer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TriageSymptomsPost extends AbsRequestPost<TriageSymptomsPost.ModelForPost, CheckOutcome> {
    @Override
    protected String getUrl() {
        return Urls.get(Urls.TRIAGE_RESULT_SYMPTOMS_POST);
    }

    public static TriageSymptomsPost newTriageSymptomsPost(CheckResult checkResult) {
        TriageSymptomsPost post = new TriageSymptomsPost();
        post.withEntry(new ModelForPost(checkResult.getBodyPartId(),
                checkResult.getOutcome(),
                checkResult.getAnswersList()));
        return post;
    }

    public static class ModelForPost extends ServerModel {
        int bodyPartId;
        int outcome;
        Map<String, String> symptoms = new HashMap<String, String>();

        public ModelForPost(int bodyPartId, int outcome, ArrayList<SymptomAnswer> answersList) {
            this.bodyPartId = bodyPartId;
            this.outcome = outcome;
            for (SymptomAnswer symptomAnswer : answersList) {
                symptoms.put(String.valueOf(symptomAnswer.getId()),
                        String.valueOf(symptomAnswer.getState().getValue()));
            }
        }
    }
}
