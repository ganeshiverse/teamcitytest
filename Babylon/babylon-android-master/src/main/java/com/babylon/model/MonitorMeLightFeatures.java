package com.babylon.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class MonitorMeLightFeatures extends BaseModel {
    @SerializedName("date_time")
    private Date date;
    private List<LightFeature> features;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<LightFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<LightFeature> features) {
        this.features = features;
    }

    public static class LightFeature extends ServerModel {
        private String id;
        private String viewValue;
        private String targetValue;
        private Integer state;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getViewValue() {
            return viewValue;
        }

        public void setViewValue(String viewValue) {
            this.viewValue = viewValue;
        }

        public String getTargetValue() {
            return targetValue;
        }

        public void setTargetValue(String targetValue) {
            this.targetValue = targetValue;
        }

        public Integer getState() {
            return state;
        }

        public void setState(Integer state) {
            this.state = state;
        }
    }
}
