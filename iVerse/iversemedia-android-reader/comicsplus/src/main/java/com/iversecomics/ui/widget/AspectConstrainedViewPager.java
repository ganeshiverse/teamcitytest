package com.iversecomics.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;


/**
 * A view pager with a fixed aspect ratio.
 */
public class AspectConstrainedViewPager extends ViewPager {

    static private final int MIN_WIDTH = 40;

    // default comic book height-to-width ratio
    float aspectRatio = 1.5013f;

    public AspectConstrainedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
            int desiredHeight = getHeightFromRatio(width);
            if (heightMode == MeasureSpec.EXACTLY) {
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                height = Math.min(desiredHeight, heightSize);
            } else {
                height = desiredHeight;
            }
        } else if (widthMode == MeasureSpec.AT_MOST) {
            if (heightMode == MeasureSpec.EXACTLY) {
                int desiredWidth = getWidthFromRatio(heightSize);
                width = Math.min(desiredWidth, widthSize);
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                int desiredWidth = MIN_WIDTH;
                int desiredHeight = getHeightFromRatio(MIN_WIDTH);
                width = Math.min(desiredWidth, widthSize);
                height = Math.min(desiredHeight, heightSize);
            } else {
                int desiredWidth = MIN_WIDTH;
                width = Math.min(desiredWidth, widthSize);
                height = getHeightFromRatio(width);
            }
        } else {
            if (heightMode == MeasureSpec.EXACTLY) {
                width = getWidthFromRatio(heightSize);
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                width = MIN_WIDTH;
                int desiredHeight = getHeightFromRatio(MIN_WIDTH);
                height = Math.min(desiredHeight, heightSize);
            } else {
                width = MIN_WIDTH;
                height = getHeightFromRatio(MIN_WIDTH);
            }
        }
        width = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        height = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(width, height);
    }

    private int getWidthFromRatio(int height) {
        int ret = (int) (height / aspectRatio);
        if (ret < 0) ret = 0;
        return ret;
    }

    private int getHeightFromRatio(int width) {
        int ret = (int) (width * aspectRatio);
        if (ret < 0) ret = 0;
        return ret;
    }


}
