package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 02/09/15.
 */
public class SavePriorityLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String messageId;
    private String priority_LU;
    private String token;

    public SavePriorityLoader(Context context, String messageId, String priority_LU, String token) {
        super(context);
        this.messageId = messageId;
        this.priority_LU = priority_LU;
        this.token = token;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().saveMessagePriority(messageId, priority_LU, token);
    }

}
