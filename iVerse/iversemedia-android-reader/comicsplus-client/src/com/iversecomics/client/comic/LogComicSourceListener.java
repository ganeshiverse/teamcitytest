package com.iversecomics.client.comic;

import android.graphics.Bitmap;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class LogComicSourceListener implements IComicSourceListener {
    private static final Logger LOG = LoggerFactory.getLogger(LogComicSourceListener.class);

    @Override
    public void onComicSourceUpdated(IComicSourceAdapter adapter) {
        LOG.debug("onComicSourceUpdated() - " + adapter);
    }

    @Override
    public void onBitmapLoaded(int index, Bitmap bitmap) {
        LOG.debug("onBitmapLoaded() - index: %d", index );
    }
}
