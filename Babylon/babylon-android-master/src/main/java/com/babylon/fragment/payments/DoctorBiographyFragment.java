package com.babylon.fragment.payments;


import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Doctor;
import com.babylonpartners.babylon.CircleTransform;
import com.squareup.picasso.Picasso;

public class DoctorBiographyFragment extends BaseFragment {

    public static final String TAG = DoctorBiographyFragment.class.getSimpleName();
    private DataProtocol dataProtocol;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            dataProtocol = (DataProtocol)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        dataProtocol = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_doctor_biography, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView doctorImageView = (ImageView) view.findViewById(R.id.gp_bio_avatar_image_view);
        TextView doctorNameTextView = (TextView) view.findViewById(R.id.gp_name_text_view);
        TextView doctorNumberTextView = (TextView) view.findViewById(R.id.gp_number_text_view);
        TextView doctorCodeTextView = (TextView) view.findViewById(R.id.gp_code_text_view);
        TextView doctorBioTextView = (TextView) view.findViewById(R.id.gp_bio_text_view);

        Doctor doctor = dataProtocol.getDoctor();
        doctorNameTextView.setText(doctor.getName());
        doctorBioTextView.setText(Html.fromHtml(doctor.getBio()));
        doctorNumberTextView.setText(String.format(getString(R.string.doctor_bio_gmc_number),
                doctor.getMedicalIdentifierLabel(), doctor.getMedicalIdentifier()));

        String avatar = doctor.getAvatar();
        if(!TextUtils.isEmpty(avatar)) {
            Picasso.with(getActivity()).load(com.babylon.api.request.base.Urls.getRuby(avatar))
                    .transform(new CircleTransform()).into(doctorImageView);
        }
    }

    public interface DataProtocol {
        public Doctor getDoctor();
    }
}
