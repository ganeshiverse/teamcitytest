package com.iversecomics.ui.menu;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.iversecomics.app.AppConstants;
import com.iversecomics.app.ComicApp;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.store.tasks.FetchGroupsTask;
import com.iversecomics.client.store.tasks.FetchPromotionListTask;
import com.iversecomics.client.store.tasks.FetchPublishersTask;
import com.iversecomics.client.store.ui.sidemenu.SideMenuDataProvider;
import com.iversecomics.client.store.ui.sidemenu.SideMenuItemInfo;
import com.iversecomics.client.store.ui.sidemenu.SideMenuSection;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.LogoutEvent;
import com.iversecomics.ui.widget.ComicBackButton;
import com.iversecomics.util.ui.UiUtil;
import com.squareup.otto.Subscribe;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * MenuFragment operates with three distinct data providers depending upon how it's constructed:
 * 1) Default Mode: new MenuFragment(), creates a standard top level menu
 * 2) Digests Mode: MenuFragment.newInstanceForGroup(groupId, groupName), creates a comic group sub-menu
 * 3) Publishers Mode: MenuFragment.newInstanceForPublisherGroup(publisherId, publisherName), creates a publisher group sub-menu
 */
public class MenuFragment extends Fragment {
    private final static Logger LOG = LoggerFactory.getLogger(MenuFragment.class);

    public interface MenuClickListener {
        public void onStoreSearch(String searchTerm);
        //Account Section

        /**
         * Login button in side menu clicked.
         */
        public void onLoginClick();

        /**
         * Sign up button in side menu clicked.
         */
        public void onSignUpClick();

        /**
         * SendFeedback button in side menu clicked.
         */
        public void onSendFeedbackClick();

        /**
         * Email address in side menu clicked.
         */
        public void onLogoutClick();

        public void onMyPurchasesClick();

        public void onDownloadsClick();

        void onMyComicsClick();
        //Promotion Section

        /**
         * An item in the "Promotion" section of the side menu clicked.
         */
        public void onPromotionClick(Promotion promo);

        //Navigation Section
        public void onFeaturedClick();

        public void onDemandClick();

        public void onNewReleasesClick();

        public void onTopPaidClick();

        public void onTopFreeClick();

        public void onSettingsClick();
        //Titles Section

        /**
         * An item in the "Titles" section of the side menu clicked.
         */
        public void onGroupClick(Group group);

        public void onPublisherClick(Publisher publisher);
    }

    public static final String ARG_PARENT_GROUP_NAME = "com.iverse.ARG_PARENT_GROUP_NAME";
    public static final String ARG_PARENT_GROUP_ID = "com.iverse.ARG_PARENT_GROUP_ID";

    public static final String ARG_PUBLISHER_GROUP_ID = "com.iverse.ARG_PUBLSIHER_GROUP_ID";
    public static final String ARG_PUBLISHER_NAME = "com.iverse.ARG_PUBLSIHER_NAME";

    private ViewGroup mRootView;
    private ComicBackButton mBackButton;
    private MenuClickListener mListener;
    private MenuAdapter mMenuAdapter;
    private EditText mSearchEditText;

    private BroadcastReceiver mRefreshContentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LOG.info("Received refresh broadcast " + intent.getAction());
            refreshContent();
        }
    };


    /**
     * Creates a "Digests" instance of MenuFragment that acts as a sub-menu to a
     * normal top level MenuFragment.  GroupTable data is filtered by the given parentGroupId.
     *
     * @param parentGroupId
     * @return
     */
    public static final MenuFragment newInstanceForGroup(String parentGroupId, String groupName) {
        Bundle args = new Bundle();
        args.putString(ARG_PARENT_GROUP_ID, parentGroupId);
        args.putString(ARG_PARENT_GROUP_NAME, groupName);
        MenuFragment frag = new MenuFragment();
        frag.setArguments(args);
        return frag;
    }

    public static final MenuFragment newInstanceForPublisherId(String publisherId, String publisherName) {
        Bundle args = new Bundle();
        args.putString(ARG_PUBLISHER_GROUP_ID, publisherId);
        args.putString(ARG_PUBLISHER_NAME, publisherName);
        MenuFragment frag = new MenuFragment();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (MenuClickListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMenuAdapter = new MenuAdapter(getActivity());

        ComicApp app = ComicApp.getInstance();
        ComicStore comicStore = app.getComicStore();

        // Configure SideMenuDataProvider to show either titles or publishers depending on the build variant.
        // Archie lists titles, ComicsPlus lists publishers.
        SideMenuDataProvider.initialize(app, AppConstants.PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU);

        FetchPromotionListTask promotionsTask = new FetchPromotionListTask(comicStore, app);
        app.getTaskPool().submitIfExpired(app.getFreshness(), promotionsTask);

        FetchGroupsTask groupsTask = new FetchGroupsTask(comicStore);
        app.getTaskPool().submitIfExpired(app.getFreshness(), groupsTask);

        // Fetch data needed for listing publisher names in the side menu
        if (AppConstants.PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU) {
            FetchPublishersTask publishersTask = new FetchPublishersTask(comicStore, app);
            app.getTaskPool().submitIfExpired(app.getFreshness(), publishersTask);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        OttoBusProvider.getInstance().register(this);

        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getActivity());
        lbm.registerReceiver(mRefreshContentReceiver,
                new IntentFilter(FetchPromotionListTask.PROMOTION_UPDATED));
        lbm.registerReceiver(mRefreshContentReceiver,
                new IntentFilter(FetchGroupsTask.GROUPS_UPDATED));

        refreshContent();
    }

    @Override
    public void onPause() {
        super.onPause();

        OttoBusProvider.getInstance().unregister(this);

        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getActivity());
        lbm.unregisterReceiver(mRefreshContentReceiver);
    }

    @Subscribe
    public void serverConfigAvailable(ServerConfig config) {
        refreshContent();
    }

    @Subscribe
    public void onLogout(LogoutEvent event) {
        refreshContent();
    }

    public void refreshContent() {
        Bundle args = getArguments();

        List<SideMenuSection> sections = null;

        // get data for a comic group's sub-menu, e.g., "Digests"
        if (args != null && args.containsKey(ARG_PARENT_GROUP_ID))
            sections = SideMenuDataProvider.getInstance(getActivity()).getDataForParentGroup(args.getString(ARG_PARENT_GROUP_ID));
            // get data for a publisher group's sub-menu
        else if (args != null && args.containsKey(ARG_PUBLISHER_GROUP_ID))
            sections = SideMenuDataProvider.getInstance(getActivity()).getDataForPublisherGroup(args.getString(ARG_PUBLISHER_GROUP_ID));
            // get data for full top level menu
        else
            sections = SideMenuDataProvider.getInstance(getActivity()).getAllData();
        mMenuAdapter.setData(sections);
        mMenuAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_menu, container, false);

        // back navigation button
        try {
            mBackButton = (ComicBackButton) mRootView.findViewById(R.id.action_back);
        }catch (InflateException ex){

        }

        Bundle args = getArguments();

        if (args != null && (args.containsKey(ARG_PARENT_GROUP_ID) || args.containsKey(ARG_PUBLISHER_GROUP_ID))) {

            String subMenuTitle = null;
            if (args.containsKey(ARG_PARENT_GROUP_NAME))
                subMenuTitle = args.getString(ARG_PARENT_GROUP_NAME);
            else if (args.containsKey(ARG_PUBLISHER_NAME))
                subMenuTitle = args.getString(ARG_PUBLISHER_NAME);

            if (subMenuTitle != null)
                ((TextView) mRootView.findViewById(R.id.title)).setText(subMenuTitle);

            mBackButton.setVisibility(View.VISIBLE);
            mBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStack();
                }
            });
        } else {
            mBackButton.setVisibility(View.GONE);
        }


        StickyListHeadersListView listView = (StickyListHeadersListView) mRootView.findViewById(android.R.id.list);
        listView.setAdapter(mMenuAdapter);

        //Remove line separator in list. I tried to do this in XML, but was getting errors setting dividerHeight to 0dp.
        listView.setDivider(null);
        listView.setDividerHeight(0);

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                handleItemClick(mMenuAdapter.getItem(position));
            }
        });

        // text search
        mSearchEditText = (EditText) (mRootView.findViewById(R.id.txtSearch));
        // handle soft keyboard's search button
        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchForProducts(mSearchEditText.getEditableText().toString().trim());
                    return true;
                }
                return false;
            }
        });

        // search button
        View btnSearch = mRootView.findViewById(R.id.btnSearch);
        UiUtil.applyButtonEffect(btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchForProducts(mSearchEditText.getEditableText().toString().trim());
            }
        });

        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void handleItemClick(SideMenuItemInfo info) {
        Object data = info.data;
        switch (info.getItemType()) {
            case Account:
                if (SideMenuDataProvider.OP_LOGIN.equals(data)) {
                    mListener.onLoginClick();
                } else if (SideMenuDataProvider.OP_SIGNUP.equals(data)) {
                    mListener.onSignUpClick();
                } else if (SideMenuDataProvider.OP_FEEDBACK.equals(data)) {
                    mListener.onSendFeedbackClick();
                } else if (SideMenuDataProvider.OP_LOGOUT.equals(data)) {
                    mListener.onLogoutClick();
                } else if (SideMenuDataProvider.OP_MYPURCHASES.equals(data)) {
                    mListener.onMyPurchasesClick();
                } else if (SideMenuDataProvider.OP_MYCOMICS.equals(data)) {
                    mListener.onMyComicsClick();
                } else if (SideMenuDataProvider.OP_MYDOWNLOADS.equals(data)) {
                    mListener.onDownloadsClick();
                } else if (SideMenuDataProvider.OP_XPOINTS.equals(data)) {

                }
                break;
            case Promotions:
                Promotion promo = (Promotion) data;
                mListener.onPromotionClick(promo);
                break;
            case Navigation:
                if (SideMenuDataProvider.OP_FEATURED.equals(data)) {
                    mListener.onFeaturedClick();
                } else if (SideMenuDataProvider.OP_DEMAND.equals(data)) {
                    mListener.onDemandClick();
                } else if (SideMenuDataProvider.OP_TOPPAID.equals(data)) {
                    mListener.onTopPaidClick();
                } else if (SideMenuDataProvider.OP_TOPFREE.equals(data)) {
                    mListener.onTopFreeClick();
                } else if (SideMenuDataProvider.OP_NEWRELEASES.equals(data)) {
                    mListener.onNewReleasesClick();
                } else if (SideMenuDataProvider.OP_SETTINGS.equals(data)) {
                    mListener.onSettingsClick();
                }
                break;
            // Archie lists titles in the side menu
            case ItemTitles:
                if (data instanceof Group) {
                    Group group = (Group) data;
                    mListener.onGroupClick(group);

                } else if (data instanceof Publisher) {
                    Publisher publisher = (Publisher) data;
                    mListener.onPublisherClick(publisher);
                } else {
                    LOG.warn("Unrecognized data type in ItemTitles");
                }
                break;
            // ComicsPlus lists publishers in the side menu
            case ItemPublishers:
                Publisher publisher = (Publisher) data;
                mListener.onPublisherClick(publisher);
                break;
        }
    }


    private void searchForProducts(String searchTerm) {

        // hide keyboard
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);

        mListener.onStoreSearch(searchTerm);
        mSearchEditText.setText("");
    }
}
