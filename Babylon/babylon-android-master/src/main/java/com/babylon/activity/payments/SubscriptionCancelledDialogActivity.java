package com.babylon.activity.payments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.babylon.Keys;
import com.babylon.R;

import java.text.DateFormat;
import java.util.Date;

public class SubscriptionCancelledDialogActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_cancelled_dialog);

        long expirationDate = getIntent().getLongExtra(Keys.KEY_SUBSCRIPTION_EXPIRATION_DATE, 0l);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String expirationDateString = dateFormat.format(new Date(expirationDate));

        TextView bodyText = (TextView)findViewById(R.id.subscription_cancelled_body_text);
        bodyText.setText(String.format(getString(R.string.subscription_cancelled_text_body), expirationDateString));
    }

    public void onOkPressed(View view) {
        onBackPressed();
    }
}
