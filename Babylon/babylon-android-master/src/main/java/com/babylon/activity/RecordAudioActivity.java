package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.babylon.Keys;
import com.babylon.R;
import com.babylon.utils.AudioManager;

import java.util.Arrays;

public class RecordAudioActivity extends Activity {

    public static final int RECORD_SEC_LENGTH = 30;
    public static final int DELAY_RECORD_SECONDS = 2;
    public static final int SECOND_IN_MILLS = 1000;

    private TextView countdownTextView;
    private ImageButton acceptButton;
    private ImageButton declineButton;
    private Button audioButton;

    private boolean isAudioRecording;
    private boolean isAudioPlaying;
    private byte[] audioByteArray;
    private AudioManager audioManager;
    private int recordCountdown = RECORD_SEC_LENGTH;
    private int currentAudioDrawable = R.drawable.btn_microphone;

    /** Use for play audio only. In this case this activity not record, only play */
    private String audioPath;
    private boolean isPlayOnly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_record);

        countdownTextView = (TextView) findViewById(R.id.tvCountdown);
        acceptButton = (ImageButton) findViewById(R.id.btnSaveAudio);
        declineButton = (ImageButton) findViewById(R.id.btnCancelAudio);
        audioButton = (Button) findViewById(R.id.btnRecordAudio);

        audioManager = AudioManager.getInstance();

        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String audioPath = extras.getString(Keys.KEY_AUDIO_PATH);
                byte[] audioByteArray = extras.getByteArray(Keys.AUDIO_BYTE_ARRAY);
                if (!TextUtils.isEmpty(audioPath) || audioByteArray != null) {
                    stopRecord();
                    declineButton.setVisibility(View.GONE);
                    isPlayOnly = true;
                    this.audioPath = audioPath;
                    this.audioByteArray = audioByteArray;
                }
            }
        }
    }

    public void onRecordAudioClick(View view) {
        if (isAudioPlaying) {
            stopAudioPlaying();
        } else if (audioByteArray != null || !TextUtils.isEmpty(audioPath)) {
            startAudioPlaying();
        } else if (isAudioRecording) {
            stopRecord();
        } else {
            startRecording();
        }
    }

    public void onAcceptClick(View view) {
        Intent data = new Intent();
        data.putExtra(Keys.AUDIO_RESULT, true);
        if (audioByteArray != null && !isPlayOnly) {
            data.putExtra(Keys.AUDIO_BYTE_ARRAY, audioByteArray);
        }
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    public void onDeclineClick(View view) {
        Intent data = new Intent();
        data.putExtra(Keys.AUDIO_RESULT, true);
        data.putExtra(Keys.AUDIO_DELETE_RESULT, true);
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    private AudioManager.OnRecordCompleteListener onRecordCompleteListener = new AudioManager
            .OnRecordCompleteListener() {
        @Override
        public void onRecordComplete(byte[] audioRecord) {
            if (audioRecord == null) {
                audioByteArray = new byte[0];
            } else {
                audioByteArray = Arrays.copyOf(audioRecord, audioRecord.length);
                audioButton.setText(R.string.play);
                isAudioRecording = false;
            }
        }

        @Override
        public void onRecordFail() {
            isAudioRecording = false;
            audioButton.setText(R.string.ask_fragment_audio);
        }
    };

    private CountDownTimer recordTimer = new CountDownTimer(RECORD_SEC_LENGTH * SECOND_IN_MILLS
            + DELAY_RECORD_SECONDS * SECOND_IN_MILLS, SECOND_IN_MILLS) {
        @Override
        public void onTick(long millisUntilFinished) {
            countdownTextView.setText(String.valueOf(recordCountdown--));
            currentAudioDrawable = currentAudioDrawable == R.drawable.btn_microphone ?
                    R.drawable.microphone_recording : R.drawable.btn_microphone;
            audioButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(currentAudioDrawable),
                    null, null);
        }

        @Override
        public void onFinish() {
            stopRecord();
        }
    };

    private void stopRecord() {
        acceptButton.setVisibility(View.VISIBLE);
        declineButton.setVisibility(View.VISIBLE);
        countdownTextView.setVisibility(View.INVISIBLE);
        recordCountdown = RECORD_SEC_LENGTH;
        recordTimer.cancel();
        audioManager.stopRecord(onRecordCompleteListener);
        audioButton.setText(R.string.play);
        audioButton.setCompoundDrawablesWithIntrinsicBounds(null,
                getResources().getDrawable(R.drawable.btn_microphone), null, null);
    }

    private void stopAudioPlaying() {
        acceptButton.setVisibility(View.VISIBLE);
        if (!isPlayOnly) {
            declineButton.setVisibility(View.VISIBLE);
        }
        audioManager.stopPlaying();
        isAudioPlaying = false;
        audioButton.setText(R.string.play);
    }

    private MediaPlayer.OnCompletionListener onCompletionAudioListenerListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            isAudioPlaying = false;
            audioButton.setText(R.string.play);
            acceptButton.setVisibility(View.VISIBLE);
            if (!isPlayOnly) {
                declineButton.setVisibility(View.VISIBLE);
            }
        }
    };

    private void startAudioPlaying() {
        if (!TextUtils.isEmpty(audioPath)) {
            audioManager.playRecord(audioPath, onCompletionAudioListenerListener);
        } else if (audioByteArray != null) {
            audioManager.playRecord(audioByteArray, onCompletionAudioListenerListener);
        }
        audioButton.setText(R.string.stop);
        isAudioPlaying = true;
        acceptButton.setVisibility(View.GONE);
        declineButton.setVisibility(View.GONE);
    }

    private void startRecording() {
        audioManager.startRecord(onRecordCompleteListener);
        isAudioRecording = true;
        audioButton.setText(R.string.stop);
        acceptButton.setVisibility(View.INVISIBLE);
        declineButton.setVisibility(View.INVISIBLE);
        countdownTextView.setVisibility(View.VISIBLE);
        recordTimer.start();
    }
}
