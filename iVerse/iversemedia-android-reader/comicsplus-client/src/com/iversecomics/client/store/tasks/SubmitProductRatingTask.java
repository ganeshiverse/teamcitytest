package com.iversecomics.client.store.tasks;

import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class SubmitProductRatingTask extends ComicStoreTask 
{
	private static final Logger LOG = LoggerFactory.getLogger(SubmitProductRatingTask.class);
	private String deviceId;
	private String comicBundleName;
	private int rating;

	public SubmitProductRatingTask(ComicStore comicStore) {
		super(comicStore);
	}

	@Override
	public void execTask() 
	{
		try
		{
			//Get the API
			ServerApi api = this.comicStore.getServerApi();
			
			//Submit the rating request
			final JSONObject json = api.submitRating(
					new JSONObject().put("rating", rating)
									.put("deviceId", this.deviceId)
									.put("productId", this.comicBundleName));
									
			final String status = json.optString("status");
			
			if (status.equals("ok")) 
			{
				LOG.info("Comic %s rating submitted", this.comicBundleName);
			}
			else
			{
				LOG.info("Comic %s rating submit failed", this.comicBundleName);
			}
		} 
		catch (ComicStoreException e) 
		{
			LOG.error("Comic %s rating submit failed", this.comicBundleName);
		} 
		catch (JSONException e) 
		{
			LOG.error("Could not create rating JSON object");
		}
	}

	public String getComicBundleName() {
		return comicBundleName;
	}
	public String getDeviceId() {
		return deviceId;
	}

	public int getRating() {
		return rating;
	}

	public void setComicBundleName(String comicBundleName) {
		this.comicBundleName = comicBundleName;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
}
