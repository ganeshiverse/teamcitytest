package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.data.responses.GetFoldersListResponse;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SaveMediaFoldersDBLoader extends DataAsyncTaskLibLoader<Boolean> {

    private String TAG = SaveMediaFoldersDBLoader.class.getSimpleName();
    private final GetFoldersListResponse mFoldersResponse;

    public SaveMediaFoldersDBLoader(final Context context, final GetFoldersListResponse foldersResponse) {
        super(context, true);
        this.mFoldersResponse = foldersResponse;
    }

    @Override
    protected Boolean performLoad() throws Exception {
        if (mFoldersResponse == null)
            throw new SQLException("Unable to cache media folder details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final ArrayList<MediaFoldersModel> mediaFoldersDBModelsAL = ConverterResponseToDbModel.getMediaFoldersDBModel(mFoldersResponse);

            if (mediaFoldersDBModelsAL != null && mediaFoldersDBModelsAL.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        // Initially clear all etried before inserting new, by doing this we can ensure that duplicate folder entries will not be insterted
                        final Dao<MediaFoldersModel, Long> siteDao1 = helper.getDao(MediaFoldersModel.class);
                        siteDao1.delete(siteDao1.queryForAll());

                        // here insert updated entries
                        for (final MediaFoldersModel dbModel : mediaFoldersDBModelsAL) {
                            final Dao<MediaFoldersModel, Long> siteDao = helper.getDao(MediaFoldersModel.class);

                            siteDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved media folder Details in DB...");
                        }

                        return true;
                    }
                });
            }
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return true;
    }
}
