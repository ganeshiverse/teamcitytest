package com.iversecomics.client.store.ui.sidemenu;


import android.util.Pair;

import java.util.Collections;
import java.util.List;

public class SideMenuSection extends Pair<String, List<SideMenuItemInfo>> {
    public final String title;
    public final List<SideMenuItemInfo> menuItems;

    /**
     * Constructor for a SideMenuSection.
     *
     * @param sectionTitle the title of the section
     * @param menuItemList list of SideMenuItemInfo for this section
     */
    public SideMenuSection(String sectionTitle, List<SideMenuItemInfo> menuItemList) {
        super(sectionTitle, menuItemList);
        title = sectionTitle;
        if (menuItemList == null) {
            menuItems = Collections.emptyList();
        } else {
            menuItems = menuItemList;
        }
    }

    public int size() {
        return menuItems.size();
    }
}
