package com.babylonpartners.babylon;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.List;

public class TenMinuteDateTimePicker extends TimePicker {

    private List<NumberPicker> numberPickers;
    private NumberPicker minutesNumberPicker;

    public TenMinuteDateTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpSpinner();
    }

    public TenMinuteDateTimePicker(Context context) {
        super(context);
        setUpSpinner();
    }

    public TenMinuteDateTimePicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setUpSpinner();
    }

    @TargetApi(21)
    public TenMinuteDateTimePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setUpSpinner();
    }

    private void findNumberPickers() {
        numberPickers = new ArrayList<NumberPicker>();
        recursiveView(this);

        for (NumberPicker picker : numberPickers) {
            if (picker.getMaxValue() >= 59) {
                minutesNumberPicker = picker;
            }
        }
    }

    private void recursiveView(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View subview = viewGroup.getChildAt(i);

            if (subview instanceof NumberPicker) {
                numberPickers.add((NumberPicker) subview);
            } else if (subview instanceof ViewGroup) {
                recursiveView((ViewGroup) subview);
            }
        }
    }

    private void setUpSpinner() {
        findNumberPickers();
        minutesNumberPicker.setMinValue(0);
        minutesNumberPicker.setMaxValue(5);
        minutesNumberPicker.setDisplayedValues(new String[]{"00", "10", "20", "30", "40", "50"});
        minutesNumberPicker.setOnLongPressUpdateInterval(100);
    }
}
