package com.iversecomics.client.util;

import android.content.Context;

public class ConfigProj 
{
	//public static final String packageName = "com.iversecomics.comicsplus.android";
	//public static final String packageName = "com.iversecomics.archie.android";
	//public static final String packageName = "com.iversecomics.digitalreader.android";
	
	private static String TAPJOY_APPID = "859c0ee2-557a-4f4e-8ad8-e312959ea0be"; //comics plus
	private static String TAPJOY_SECRET = "2iaZQwEHpc1yBQIsyeLy";
	private static String TAPJOY_CURRENCY_ID = "";
	
	public static boolean SKU_SPECIFIC_TITLES = false;
	public static String ONDEMAND_BUTTON_TITLE = null;
	public static boolean SOCIAL_NETWORKING = true;
	public static String SOCIAL_NETWORKING_APP_NAME = "";
    public static boolean PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU = false;
	
	//this is set by each SKU at run time
	private static String packageName = null; 
	public static String getSkuPackageName()
	{
		return packageName;
	}
	
	public static void setSkuPackageName(String pkgName)
	{
		packageName = pkgName;
	}
	
	public static final void assertPackageValid(final Context context)
	{
	    //confirm that the config package name was set correctly in the build process
	    if (!ConfigProj.packageName.equals(context.getPackageName())) 
	    {
	        throw new IllegalStateException("ConfigProj.packageName does match " + context.getPackageName() +
	        		" as set in this project's manifest");
	    }
	}
    //toggle to switch between dev and production 
    public static final boolean DEV = false;
    
    //Set true before shipping!
    public static final boolean DISABLE_LOGGING = false;
    
    
    
	public static String getTapjoyAppid() {
		return TAPJOY_APPID;
	}

	public static String getTapjoySecret() {
		return TAPJOY_SECRET;
	}

	public static String getTapjoyCurrencyId() {
		return TAPJOY_CURRENCY_ID;
	}
	
	public static void setTapjoyAppid(String newStr) {
		TAPJOY_APPID = newStr;
	}

	public static void setTapjoySecret(String newStr) {
		TAPJOY_SECRET = newStr;
	}

	public static void setTapjoyCurrencyId(String newStr) {
		TAPJOY_CURRENCY_ID = newStr;
	}

    public static void setPublishersInsteadOfTitlesInSideMenu(boolean b) {
        PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU = b;
    }

    public static boolean isPublishersInsteadOfTitlesInSideMenu() {
        return PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU;
    }
}
