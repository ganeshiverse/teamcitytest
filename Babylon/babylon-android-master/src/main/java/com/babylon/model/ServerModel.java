package com.babylon.model;

import com.babylon.sync.SyncUtils;

import java.util.Random;
import java.util.UUID;

public class ServerModel extends BaseModel {

    protected transient Integer syncWithServer = SyncUtils.SYNC_TYPE_ADDED;


    public ServerModel() {
    }

    public Integer getSyncWithServer() {
        return syncWithServer;
    }

    public void setSyncWithServer(Integer syncWithServer) {
        this.syncWithServer = syncWithServer;
    }

    public Integer getRandomUUIDInt() {
        Random random = new Random();
        long l = System.currentTimeMillis() / android.text.format.DateUtils.SECOND_IN_MILLIS;
        return random.nextInt((int)l);
    }

    public String getRandomUUIDStr() {
        return UUID.randomUUID().toString();
    }

}
