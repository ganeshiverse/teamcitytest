package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.ContentChangeTracker;

public class FetchComicsTopFreeTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testFetchTopFree() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(ComicsTable.CONTENT_URI, this);

        ComicStore comicStore = getComicStore();
        FetchComicsTopFreeTask task = new FetchComicsTopFreeTask(comicStore);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, ComicsTable.CONTENT_URI);
        OurAsserts.assertGreaterThan("Comics table rows", 10, actualCount);
    }
}
