package com.iversecomics.client.comic.viewer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.iversecomics.archie.android.R;

public class ComicNav {
    @SuppressWarnings("unused")
    private static final String TAG = "ComicNav";
    private int panelPosition = 0;
    //    private PanelProvider       panelProvider;
    private EditText panelNumber;
    private boolean canceled = false;
    private String message;
    private Resources resources;
    private int panelCount;
    private int currentPanel;

    public Dialog createDialog(Context context) {
        this.resources = context.getResources();

        // Create View
        LayoutInflater factory = LayoutInflater.from(context);
        View navView = factory.inflate(R.layout.nav_dialog, null);
        panelNumber = (EditText) navView.findViewById(R.id.panel_number);
        panelNumber.setEnabled(false);
        SeekBar seekBar = (SeekBar) navView.findViewById(R.id.seek);
        TextView txtMax = (TextView) navView.findViewById(R.id.panel_of_count);

        // Populate View
        panelNumber.setKeyListener(DigitsKeyListener.getInstance());
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
                panelNumber.setText(String.valueOf(progress + 1));
                panelPosition = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                /* ignore */
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                /* ignore */
            }
        });

//        seekBar.setMax(panelProvider.getCount()-1);
//        seekBar.setProgress(panelProvider.getPosition());
//        panelNumber.setText(String.valueOf(panelProvider.getPosition() + 1));
//        txtMax.setText(String.valueOf(panelProvider.getCount()));
        seekBar.setMax(panelCount - 1);
        seekBar.setProgress(currentPanel);
        panelNumber.setText(String.valueOf(currentPanel + 1));
        txtMax.setText(String.valueOf(panelCount));

        // Create Dialog
        AlertDialog.Builder dlg = new AlertDialog.Builder(context);
        dlg.setTitle(R.string.navigation_title);
        dlg.setIcon(R.drawable.icon);
        dlg.setView(navView);
        dlg.setCancelable(true);
        dlg.setPositiveButton(R.string.go, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                onGo();
            }
        });
        dlg.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                onClose();
            }
        });

        return dlg.create();
    }

    public int getPanelPosition() {
        return panelPosition;
    }

//    public PanelProvider getPanelProvider() {
//        return panelProvider;
//    }

    public boolean isCanceled() {
        return canceled;
    }

    private void onClose() {
        this.message = null;
        this.canceled = true;
    }

    private void onGo() {
        this.canceled = true;
        final String positionText = panelNumber.getText().toString();
        try {
            panelPosition = Integer.parseInt(positionText) - 1;
//            if (panelProvider.isValid(panelPosition)) {
            message = resources.getString(R.string.loading_panel, (panelPosition + 1));
            this.canceled = false;
//            } else {
//                message = resources.getString(R.string.invalid_panel_position, (panelPosition + 1), (panelProvider
//                        .getCount() + 1));
//                this.canceled = true;
//            }
        } catch (final NumberFormatException e) {
            message = resources.getString(R.string.invalid_panel_number, positionText);
            this.canceled = true;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setPanelPosition(int panelPosition) {
        this.panelPosition = panelPosition;
    }

    public void setMaxAndCurrent(int max, int current) {
        panelCount = max;
        currentPanel = current;
    }

//    public void setPanelProvider(PanelProvider panelProvider) {
//        this.panelProvider = panelProvider;
//    }
}
