package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.axsystech.excelicare.util.CryptoEngine;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by vvydesai on 9/9/2015.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CIRCLES_LIST)
public class CirclesListDbModel extends DBUtils implements Serializable {

    private static final long serialVersionUID = 2L;

    @DatabaseField(id = true, columnName = CIRCLES_USER_CIRCLE_ID)
    private String userCircleId;

    @DatabaseField(columnName = CIRCLES_IS_SYSTEM_CIRCLE)
    private String isSystemCircle;

    @DatabaseField(columnName = CIRCLES_MEMBER_COUNT)
    private int memberCount;

    @DatabaseField(columnName = CIRCLES_NAME)
    private String circleName;

    @DatabaseField(columnName = CIRCLES_IS_PROTECTED)
    private boolean isProtected;

    @DatabaseField(columnName = CIRCLES_SITEID)
    private String siteId;

    @DatabaseField(columnName = CIRCLES_STARTDATE)
    private String startDate;

    @DatabaseField(columnName = CIRCLES_STOPDATE)
    private String stopDate;

    @DatabaseField(columnName = CIRCLES_STOPREASONDESC)
    private String stopReasonDesc;

    @DatabaseField(columnName = CIRCLES_STOPREASONDSLU)
    private String stopReasonSlu;

    @DatabaseField(columnName = CIRCLES_SYSTEM_CIRCLE_ID)
    private String systemCircleId;

    @DatabaseField(columnName = CIRCLES_USER_ID)
    private String userId;

    @DatabaseField(columnName = CIRCLES_USER_TYPE)
    private String userType;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIsSystemCircle() throws Exception {
        return CryptoEngine.decrypt(isSystemCircle);
    }

    public void setIsSystemCircle(String isSystemCircle) throws Exception {
        this.isSystemCircle = CryptoEngine.encrypt(isSystemCircle);
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public boolean getIsProtected() {
        return isProtected;
    }

    public void setIsProtected(boolean isProtected) {
        this.isProtected = isProtected;
    }

    public String getSiteId() throws Exception {
        return CryptoEngine.decrypt(siteId);
    }

    public void setSiteId(String siteId) throws Exception {
        this.siteId = CryptoEngine.encrypt(siteId);
    }

    public String getStartDate() throws Exception {
        return CryptoEngine.decrypt(startDate);
    }

    public void setStartDate(String startDate) throws Exception {
        this.startDate = CryptoEngine.encrypt(startDate);
    }

    public String getStopDate() throws Exception {
        return CryptoEngine.decrypt(stopDate);
    }

    public void setStopDate(String stopDate) throws Exception {
        this.stopDate = CryptoEngine.encrypt(stopDate);
    }

    public String getStopReasonDesc() throws Exception {
        return CryptoEngine.decrypt(stopReasonDesc);
    }

    public void setStopReasonDesc(String stopReasonDesc) throws Exception {
        this.stopReasonDesc = CryptoEngine.encrypt(stopReasonDesc);
    }

    public String getStopReasonSlu() throws Exception {
        return CryptoEngine.decrypt(stopReasonSlu);
    }

    public void setStopReasonSlu(String stopReasonSlu) throws Exception {
        this.stopReasonSlu = CryptoEngine.encrypt(stopReasonSlu);
    }

    public String getSystemCircleId() throws Exception {
        return CryptoEngine.decrypt(systemCircleId);
    }

    public void setSystemCircleId(String systemCircleId) throws Exception {
        this.systemCircleId = CryptoEngine.encrypt(systemCircleId);
    }

    public String getUserCircleId() throws Exception {
        return CryptoEngine.decrypt(userCircleId);
    }

    public void setUserCircleId(String userCircleId) throws Exception {
        this.userCircleId = CryptoEngine.encrypt(userCircleId);
    }

    public String getUserId() throws Exception {
        return CryptoEngine.decrypt(userId);
    }

    public void setUserId(String userId) throws Exception {
        this.userId = CryptoEngine.encrypt(userId);
    }

    public String getUserType() throws Exception {
        return CryptoEngine.decrypt(userType);
    }

    public void setUserType(String userType) throws Exception {
        this.userType = CryptoEngine.encrypt(userType);
    }
}
