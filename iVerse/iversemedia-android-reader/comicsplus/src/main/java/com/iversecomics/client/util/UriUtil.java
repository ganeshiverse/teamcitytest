package com.iversecomics.client.util;

import android.net.Uri;

import java.net.URI;
import java.net.URISyntaxException;

public final class UriUtil {
    public static Uri toSafeAndroidUri(String str) throws UnsupportedOperationException {
        try {
            return Uri.parse(toSafeJavaURI(str).toASCIIString());
        } catch (URISyntaxException e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }

    public static URI toSafeJavaURI(String str) throws URISyntaxException {
        String scheme = null;
        String ssp = null;
        String fragment = null;
        int schemeidx = str.indexOf(':');
        if (schemeidx > 0) {
            scheme = str.substring(0, schemeidx);
            // TODO: limit to known schemes. (http, https, file, etc...)
            int fragidx = str.indexOf('#'); // TODO: don't use fragments on file schemes?
            if (fragidx > 0) {
                ssp = str.substring(schemeidx + 1, fragidx);
                fragment = str.substring(fragidx + 1);
            } else {
                ssp = str.substring(schemeidx + 1);
            }
            return new URI(scheme, ssp, fragment);
        } else {
            throw new URISyntaxException(str, "Schemeless URIs not supported by this application");
        }
    }
}
