package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.data.responses.CityListResponse;
import com.axsystech.excelicare.loaders.CityListLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.NavigationUtils;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * Created by someswar on 6/30/2015.
 */
public class CityListActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private static final String GET_CITY_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.SignUpActivity.GET_CITY_LIST_LOADER";
    private static final String SEARCH_CITY_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.SignUpActivity.SEARCH_CITY_LIST_LOADER";

    private CityListActivity mActivity;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.listView)
    private ListView listView;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    private ArrayList<CityListResponse.CityDetails> cityDetailsAL;
    private CityListAdapter cityListAdapter;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity,getString(R.string.signup_text)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_message_inbox);
        mActivity = this;

        preloadData();
        loadCityList();
        readIntentData();
        addEventListeners();

    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_city_name));
    }

    private void loadCityList() {
        // get City list from Server
        if(CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            CityListLoader cityListLoader = new CityListLoader(mActivity, AxSysConstants.CITY_LIST_ID, "", AxSysConstants.CITY_LIST_TOKEN);
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CITY_LIST_LOADER), cityListLoader);
        } else {
            showToast(R.string.error_loading_cities);
        }
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if(CommonUtils.hasInternet()) {
                        CityListLoader cityListLoader = new CityListLoader(mActivity, AxSysConstants.CITY_LIST_ID, searchEditText.getText().toString().trim(),
                                AxSysConstants.CITY_LIST_TOKEN);
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SEARCH_CITY_LIST_LOADER), cityListLoader);
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if(id == getLoaderHelper().getLoaderId(GET_CITY_LIST_LOADER)) {
            if(result != null && result instanceof CityListResponse) {
                CityListResponse cityListResponse = (CityListResponse) result;

                if(cityListResponse.getCityDetailsAL() != null && cityListResponse.getCityDetailsAL().size() > 0) {
                    cityDetailsAL = cityListResponse.getCityDetailsAL();
                    // Bind data to listview
                    if(cityListAdapter == null) {
                        cityListAdapter = new CityListAdapter(cityDetailsAL);
                        listView.setAdapter(cityListAdapter);
                    } else {
                        cityListAdapter.updateData(cityDetailsAL);
                    }
                }
            }
        } else if(id == getLoaderHelper().getLoaderId(SEARCH_CITY_LIST_LOADER)) {
            if(result != null && result instanceof CityListResponse) {
                CityListResponse cityListResponse = (CityListResponse) result;

                if(cityListResponse.getCityDetailsAL() != null && cityListResponse.getCityDetailsAL().size() > 0) {
                    ArrayList<CityListResponse.CityDetails> cityDetailsAL = cityListResponse.getCityDetailsAL();
                    // Bind search data to listview
                    if(cityListAdapter == null) {
                        cityListAdapter = new CityListAdapter(cityDetailsAL);
                        listView.setAdapter(cityListAdapter);
                    } else {
                        cityListAdapter.updateData(cityDetailsAL);
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if(id == getLoaderHelper().getLoaderId(GET_CITY_LIST_LOADER)) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                // update to list adapter with original items
                if(cityListAdapter != null) {
                    cityListAdapter.updateData(cityDetailsAL);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if(text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    private class CityListAdapter extends BaseAdapter {

        private ArrayList<CityListResponse.CityDetails> cityDetailsArrayList;

        public CityListAdapter(ArrayList<CityListResponse.CityDetails> cityDetailsArrayList) {
            this.cityDetailsArrayList = cityDetailsArrayList;
        }

        public void updateData(ArrayList<CityListResponse.CityDetails> cityDetailsArrayList) {
            this.cityDetailsArrayList = cityDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (cityDetailsArrayList != null && cityDetailsArrayList.size() > 0) ? cityDetailsArrayList.size() : 0 ;
        }

        @Override
        public CityListResponse.CityDetails getItem(int position) {
            return cityDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final CityListResponse.CityDetails rowObj = getItem(position);

            if(convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.circles_list_row, parent, false);
            }

            TextView cityNameTextView = (TextView) convertView.findViewById(R.id.circleNameTextView);
            cityNameTextView.setText(!TextUtils.isEmpty(rowObj.getCity()) ? rowObj.getCity() : "");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(AxSysConstants.EXTRA_SELECTED_CITY, rowObj);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });

            return convertView;
        }
    }

}
