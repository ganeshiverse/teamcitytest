package com.axsystech.excelicare.framework.ui.messages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AttachmentDetails;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.MessageDetailsResponse;
import com.axsystech.excelicare.data.responses.MessageSendingResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.data.responses.SummarySectionsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.DashboardFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.MediaFragment;
import com.axsystech.excelicare.loaders.GetSummarySectionsLoader;
import com.axsystech.excelicare.loaders.MessageSendingLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MessageComposeFragment extends BaseFragment implements View.OnClickListener, ProvidersSelectionListener,
        CircleMemberSelectionListener, SummarySectionsFragment.SummarySectionsSelectionListener, MediaFragment.MediaAttachmentsListener {

    private static final String SEND_MESSAGE_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment.SEND_MESSAGE_LOADER ";
    private static final String GET_SUMMARY_SECTIONS_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment.GET_SUMMARY_SECTIONS_LOADER ";

    private String TAG = MessageComposeFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mMemberMailId;

    @InjectSavedState
    private String mCircleId;

    @InjectSavedState
    private String mComposeMessageTo = AxSysConstants.COMPOSE_MSG_TO_ALL;

    @InjectSavedState
    private String mComposeMessageType = AxSysConstants.COMPOSE_MSG_TYPE_REPLY_ALL;

    private MessageDetailsResponse msgDetailsResponse;

    @InjectView(R.id.toDetailsLayout)
    private LinearLayout toDetailsLayout;

    @InjectView(R.id.listMembersToImageView)
    private ImageView listMembersToImageView;

    @InjectView(R.id.ccDetailsLayout)
    private LinearLayout ccDetailsLayout;

    @InjectView(R.id.listMembersCcImageView)
    private ImageView listMembersCcImageView;

    // @InjectView(R.id.subjectEditText)
    private EditText subjectEditText;

    @InjectView(R.id.priorityCheckBox)
    private CheckBox priorityCheckBox;

    @InjectView(R.id.providersListToImageView)
    private ImageView providersListToImageView;

    @InjectView(R.id.providersListCcImageView)
    private ImageView providersListCcImageView;

    //@InjectView(R.id.msgBodyEditText)
    private EditText msgBodyEditText;

    @InjectView(R.id.attachmentsListView)
    private ListView attachmentsListView;

    private TextView toTextView;
    private TextView ccTextView;
    private TextView subjectTextView;

    private AttachmentsListAdapter attachmentsListAdapter;

    private enum SelectedSourceForProvider {
        TO,
        CC
    }

    private enum SelectedSourceForListMember {
        TO,
        CC
    }

    private String mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;

    private ArrayList<RecipientDetails> toRecipientsList = new ArrayList<RecipientDetails>();
    private ArrayList<RecipientDetails> ccRecipientsList = new ArrayList<RecipientDetails>();

    private SelectedSourceForProvider selectedSourceForProvider = SelectedSourceForProvider.TO;
    private SelectedSourceForListMember selectedSourceForListMember = SelectedSourceForListMember.TO;

    private ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj> summaryPanelDetailsObjsAL;

    private ArrayList<AttachmentDetails> attachmentDetailsAL = new ArrayList<AttachmentDetails>();

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }

        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message_compose, container, false);
        view.setFocusable(true);
        //view.setFocusableInTouchMode(true);
        view.setClickable(true);
        subjectEditText = (EditText) view.findViewById(R.id.subjectEditText);
        msgBodyEditText = (EditText) view.findViewById(R.id.msgBodyEditText);
        toTextView = (TextView) view.findViewById(R.id.toTextView);
        ccTextView = (TextView) view.findViewById(R.id.ccTextView);
        subjectTextView = (TextView) view.findViewById(R.id.subjectTextView);

        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

        subjectTextView.setTypeface(bold);
        toTextView.setTypeface(bold);
        ccTextView.setTypeface(bold);
        subjectEditText.setTypeface(bold);
        msgBodyEditText.setTypeface(regular);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        initViews();
        addEventListeners();
        preloadData();
    }

    private void initViews() {
        if (!TextUtils.isEmpty(mMemberMailId)) {
            final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

            if (recipientLayout != null) {
                final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
                final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);
                Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
                receipientNameTextView.setTypeface(regular);
                receipientNameTextView.setText(mMemberMailId);

                // add selected recipient to AL
                final RecipientDetails recipientDetails = new RecipientDetails(AxSysConstants.TO_RECIPIENT_TYPE_LU, mCircleId);
                if (toRecipientsList.size() > 0) {
                    toRecipientsList.set(0, recipientDetails);
                } else {
                    toRecipientsList.add(0, recipientDetails);
                }
                removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setCancelable(true);
                        builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                        builder.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                recipientLayout.setVisibility(View.GONE);
                                toRecipientsList.remove(0);

                                toDetailsLayout.removeAllViews();
                                if (toDetailsLayout.getChildCount() == 0) {
                                    toDetailsLayout.setVisibility(View.INVISIBLE);
                                } else {
                                    toDetailsLayout.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                        builder.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });

                // Add recipient view to main linear layout
                toDetailsLayout.removeAllViews();
                toDetailsLayout.addView(recipientLayout, 0);
                if (toDetailsLayout.getChildCount() == 0) {
                    toDetailsLayout.setVisibility(View.INVISIBLE);
                } else {
                    toDetailsLayout.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_COMPOSE_MSG_TO)) {
                mComposeMessageTo = bundle.getString(AxSysConstants.EXTRA_COMPOSE_MSG_TO);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE)) {
                mComposeMessageType = bundle.getString(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS)) {
                msgDetailsResponse = (MessageDetailsResponse) bundle.getSerializable(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID)) {
                mMemberMailId = (bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID));
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_ID)) {
                mCircleId = (bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_ID));
            }
        }
    }

    private void addEventListeners() {
        listMembersToImageView.setOnClickListener(this);
        listMembersCcImageView.setOnClickListener(this);

        providersListToImageView.setOnClickListener(this);
        providersListCcImageView.setOnClickListener(this);

        priorityCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mPriorityLU = AxSysConstants.HIGH_PRIORITY_ID;
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_high);
                } else {
                    mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                }
            }
        });
    }

    private void preloadData() {
        attachmentsListAdapter = new AttachmentsListAdapter(attachmentDetailsAL);
        attachmentsListView.setAdapter(attachmentsListAdapter);
        setListViewHeightBasedOnChildren(attachmentsListView);

        // Hide or Display Search Circle Members & Providers options based on Compose Message type
        if (!TextUtils.isEmpty(mComposeMessageTo)) {
            if (mComposeMessageTo.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TO_ALL)) {
                listMembersToImageView.setVisibility(View.VISIBLE);
                providersListToImageView.setVisibility(View.VISIBLE);

                listMembersCcImageView.setVisibility(View.VISIBLE);
                providersListCcImageView.setVisibility(View.VISIBLE);
            } else if (mComposeMessageTo.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TO_CIRCLE)) {
                listMembersToImageView.setVisibility(View.VISIBLE);
                providersListToImageView.setVisibility(View.GONE);

                listMembersCcImageView.setVisibility(View.VISIBLE);
                providersListCcImageView.setVisibility(View.VISIBLE);
            } else if (mComposeMessageTo.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TO_PROVIDERS)) {
                listMembersToImageView.setVisibility(View.GONE);
                providersListToImageView.setVisibility(View.VISIBLE);

                listMembersCcImageView.setVisibility(View.VISIBLE);
                providersListCcImageView.setVisibility(View.VISIBLE);
            }
        }

        // load actual message details in the case of REPTY, REPLY ALL, FORWARD scenarios
        if (msgDetailsResponse != null && msgDetailsResponse.getDataObject() != null) {
            MessageDetailsResponse.DataObject msgDetailsDataObj = msgDetailsResponse.getDataObject();

            if (!TextUtils.isEmpty(msgDetailsDataObj.getPriority_Lu())) {
                if (msgDetailsDataObj.getPriority_Lu().equalsIgnoreCase(AxSysConstants.HIGH_PRIORITY_ID)) {
                    priorityCheckBox.setChecked(true);
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_high);
                    mPriorityLU = AxSysConstants.HIGH_PRIORITY_ID;
                } else {
                    priorityCheckBox.setChecked(false);
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                    mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
                }
            } else {
                priorityCheckBox.setChecked(false);
                priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
            }

            // Get TO & CC recipients list
            final ArrayList<MessageDetailsResponse.RecipientsObject> recipentslist = msgDetailsDataObj.getRecipentsArraylist();
            final ArrayList<MessageDetailsResponse.RecipientsObject> toRecipentslist = new ArrayList<MessageDetailsResponse.RecipientsObject>();
            final ArrayList<MessageDetailsResponse.RecipientsObject> ccRecipentslist = new ArrayList<MessageDetailsResponse.RecipientsObject>();

            if (recipentslist != null && recipentslist.size() > 0) {

                for (int idx = 0; idx < recipentslist.size(); idx++) {
                    MessageDetailsResponse.RecipientsObject rowObj = recipentslist.get(idx);

                    if (rowObj != null && !TextUtils.isEmpty(rowObj.getRecipientTypeLU())) {
                        if (rowObj.getRecipientTypeLU().equalsIgnoreCase(AxSysConstants.TO_RECIPIENT_TYPE_LU)) {
                            toRecipentslist.add(rowObj);
                        } else if (rowObj.getRecipientTypeLU().equalsIgnoreCase(AxSysConstants.CC_RECIPIENT_TYPE_LU)) {
                            ccRecipentslist.add(rowObj);
                        }
                    } else {
                        toRecipentslist.add(rowObj);
                    }
                }
            }

            if (mComposeMessageType.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TYPE_REPLY)) {
                // add TO recipients details as TO recipients
                if (toRecipentslist != null && toRecipentslist.size() > 0) {
                    for (int idx = 0; idx < toRecipentslist.size(); idx++) {
                        MessageDetailsResponse.RecipientsObject recipientObj = toRecipentslist.get(idx);

                        addToRecipientView(recipientObj);
                    }
                }

                if (!TextUtils.isEmpty(msgDetailsDataObj.getSubject())) {
                    if (!msgDetailsDataObj.getSubject().contains("Re:")) {
                        subjectEditText.setText("Re:" + msgDetailsDataObj.getSubject());
                    } else {
                        subjectEditText.setText(msgDetailsDataObj.getSubject());
                    }
                } else {
                    subjectEditText.setText("Re:");
                }

                msgBodyEditText.setText(!TextUtils.isEmpty(msgDetailsDataObj.getMessage()) ? msgDetailsDataObj.getMessage() : "");

            } else if (mComposeMessageType.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TYPE_REPLY_ALL)) {
                // add TO recipients details as TO recipients
                if (toRecipentslist != null && toRecipentslist.size() > 0) {
                    for (int idx = 0; idx < toRecipentslist.size(); idx++) {
                        MessageDetailsResponse.RecipientsObject recipientObj = toRecipentslist.get(idx);

                        addToRecipientView(recipientObj);
                    }
                }

                // add CC recipients details as CC recipients
                if (ccRecipentslist != null && ccRecipentslist.size() > 0) {
                    for (int idx = 0; idx < ccRecipentslist.size(); idx++) {
                        MessageDetailsResponse.RecipientsObject recipientObj = ccRecipentslist.get(idx);

                        addCcRecipientView(recipientObj);
                    }
                }

                if (!TextUtils.isEmpty(msgDetailsDataObj.getSubject())) {
                    if (!msgDetailsDataObj.getSubject().contains("Re:")) {
                        subjectEditText.setText("Re:" + msgDetailsDataObj.getSubject());
                    } else {
                        subjectEditText.setText(msgDetailsDataObj.getSubject());
                    }
                } else {
                    subjectEditText.setText("Re:");
                }

                msgBodyEditText.setText(!TextUtils.isEmpty(msgDetailsDataObj.getMessage()) ? msgDetailsDataObj.getMessage() : "");

            } else if (mComposeMessageType.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TYPE_FORWARD)) {
                if (!TextUtils.isEmpty(msgDetailsDataObj.getSubject())) {
                    if (!msgDetailsDataObj.getSubject().contains("Fw:")) {
                        subjectEditText.setText("FWD:" + msgDetailsDataObj.getSubject());
                    } else {
                        subjectEditText.setText(msgDetailsDataObj.getSubject());
                    }
                } else {
                    subjectEditText.setText("FWD:");
                }

                msgBodyEditText.setText(!TextUtils.isEmpty(msgDetailsDataObj.getMessage()) ? msgDetailsDataObj.getMessage() : "");
            }
        }
    }

    private void addToRecipientView(MessageDetailsResponse.RecipientsObject recipientObj) {
        final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

        if (recipientLayout != null) {
            final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
            final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            receipientNameTextView.setTypeface(regular);

            receipientNameTextView.setText(!TextUtils.isEmpty(recipientObj.getRecipientName()) ? (recipientObj.getRecipientName() + (
                    !TextUtils.isEmpty(recipientObj.getRecipientSpeciality()) ? ("(" + recipientObj.getRecipientSpeciality() + ")") : "")) : "");

            // add selected recipient to AL
            final RecipientDetails recipientDetails = new RecipientDetails(recipientObj.getRecipientTypeLU(), recipientObj.getRecipientUserID());
            if (toRecipientsList.size() > 0) {
                toRecipientsList.set(0, recipientDetails);
            } else {
                toRecipientsList.add(0, recipientDetails);
            }
            removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setCancelable(true);
                    builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                    builder.setPositiveButton(getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recipientLayout.setVisibility(View.GONE);
                            toRecipientsList.remove(0);
                            toDetailsLayout.removeAllViews();
                            if (toDetailsLayout.getChildCount() == 0) {
                                toDetailsLayout.setVisibility(View.INVISIBLE);
                            } else {
                                toDetailsLayout.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    builder.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });

            // Add recipient view to main linear layout
            toDetailsLayout.removeAllViews();
            toDetailsLayout.addView(recipientLayout, 0);
            if (toDetailsLayout.getChildCount() == 0) {
                toDetailsLayout.setVisibility(View.INVISIBLE);
            } else {
                toDetailsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void addCcRecipientView(MessageDetailsResponse.RecipientsObject recipientObj) {
        final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

        if (recipientLayout != null) {
            final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
            final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            receipientNameTextView.setTypeface(regular);

            receipientNameTextView.setText(!TextUtils.isEmpty(recipientObj.getRecipientName()) ? (recipientObj.getRecipientName() + (
                    !TextUtils.isEmpty(recipientObj.getRecipientSpeciality()) ? ("(" + recipientObj.getRecipientSpeciality() + ")") : "")) : "");

            // add selected recipient to AL
            final RecipientDetails recipientDetails = new RecipientDetails(recipientObj.getRecipientTypeLU(), recipientObj.getRecipientUserID());
            ccRecipientsList.add(recipientDetails);

            removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setCancelable(true);
                    builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                    builder.setPositiveButton(getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ccRecipientsList.remove(recipientDetails);

                            recipientLayout.setVisibility(View.GONE);
                            ccDetailsLayout.removeView(recipientLayout);
                            ccDetailsLayout.invalidate();
                            if (ccDetailsLayout.getChildCount() == 0) {
                                ccDetailsLayout.setVisibility(View.INVISIBLE);
                            } else {
                                ccDetailsLayout.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    builder.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });

            // Add recipient view to main linear layout
            ccDetailsLayout.addView(recipientLayout);
            if (ccDetailsLayout.getChildCount() == 0) {
                ccDetailsLayout.setVisibility(View.INVISIBLE);
            } else {
                ccDetailsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SEND_MESSAGE_LOADER)) {
            showToast(R.string.error_sending_message);
        } else if (id == getLoaderHelper().getLoaderId(GET_SUMMARY_SECTIONS_LOADER)) {
            showToast(R.string.error_loading_summary_sections);
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SEND_MESSAGE_LOADER)) {
            if (result != null && result instanceof MessageSendingResponse) {
                final MessageSendingResponse messageSendingResponse = (MessageSendingResponse) result;

                if (!TextUtils.isEmpty(messageSendingResponse.getMessage())) {
                    showToast(messageSendingResponse.getMessage());

                } else if (!TextUtils.isEmpty(messageSendingResponse.getMessageCaps())) {
                    showToast(messageSendingResponse.getMessageCaps());
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (!TextUtils.isEmpty(messageSendingResponse.getMessageResponseData())) {
                            if (mComposeMessageTo.equalsIgnoreCase(AxSysConstants.COMPOSE_MSG_TO_ALL)) {
                                mActivity.onBackPressed();
                            }
                        }
                    }
                });
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_SUMMARY_SECTIONS_LOADER)) {
            if (result != null && result instanceof SummarySectionsResponse) {
                final SummarySectionsResponse summarySectionsResponse = (SummarySectionsResponse) result;

                if (summarySectionsResponse != null && summarySectionsResponse.getPanelObject() != null) {
                    if (summarySectionsResponse.getPanelObject().getSummaryPanelDetailsObjAL() != null &&
                            summarySectionsResponse.getPanelObject().getSummaryPanelDetailsObjAL().size() > 0) {

                        summaryPanelDetailsObjsAL = summarySectionsResponse.getPanelObject().getSummaryPanelDetailsObjAL();

                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                // display summary sections list in separate screen and allow user to select for attachments
                                final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                                final FragmentTransaction ft = activityFragmentManager.beginTransaction();
                                SummarySectionsFragment summarySectionsFragment = new SummarySectionsFragment();
                                summarySectionsFragment.setSummarySelectionListener(MessageComposeFragment.this);
                                Bundle bundle = new Bundle();
                                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_select_sections));
                                bundle.putSerializable(AxSysConstants.EXTRA_SUMMARY_SECTIONS_LIST, summaryPanelDetailsObjsAL);
                                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                                summarySectionsFragment.setArguments(bundle);
                                ft.add(R.id.container_layout, summarySectionsFragment, SummarySectionsFragment.class.getSimpleName());
                                ft.addToBackStack(SummarySectionsFragment.class.getSimpleName());
                                ft.commit();
                            }
                        });
                    } else {
                        showToast(R.string.error_empty_summary_sections);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.listMembersToImageView:
                selectedSourceForListMember = SelectedSourceForListMember.TO;

                loadListMembers();
                break;

            case R.id.listMembersCcImageView:
                selectedSourceForListMember = SelectedSourceForListMember.CC;

                loadListMembers();
                break;

            case R.id.providersListToImageView:
                selectedSourceForProvider = SelectedSourceForProvider.TO;

                selectProvider();
                break;

            case R.id.providersListCcImageView:
                selectedSourceForProvider = SelectedSourceForProvider.CC;

                selectProvider();
                break;
        }
    }

    private void loadListMembers() {
        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

        MsgCircleMembersListFragment circleMembersListFragment = new MsgCircleMembersListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_select_circle_member));
        bundle.putString(AxSysConstants.EXTRA_CIRCLE_MEMBER_TARGET, MessageComposeFragment.class.getSimpleName());
        circleMembersListFragment.setArguments(bundle);
        ft.add(R.id.container_layout, circleMembersListFragment, MsgCircleMembersListFragment.class.getSimpleName());
        ft.addToBackStack(MsgCircleMembersListFragment.class.getSimpleName());
        ft.commit();
    }

    private void selectProvider() {
        FragmentManager activityFragmentManager = mActivity.getSupportFragmentMngr();
        FragmentTransaction ft = activityFragmentManager.beginTransaction();

        SearchDoctorsFragment searchDoctorsFragment = new SearchDoctorsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_find_doctors));
        bundle.putString(AxSysConstants.EXTRA_PROVIDER_SOURCE, AxSysConstants.PROVIDER_SOURCE_COMPOSE);
        searchDoctorsFragment.setArguments(bundle);
        ft.add(R.id.container_layout, searchDoctorsFragment, SearchDoctorsFragment.class.getSimpleName());
        ft.addToBackStack(SearchDoctorsFragment.class.getSimpleName());
        ft.commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.message_compose_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            case R.id.actionAttach:
                if (CommonUtils.hasInternet()) {
                    showAttachmentsPicker(getString(R.string.picker_title_attach));
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            case R.id.actionSend:
                if (validateUserInputs()) {
                    prepareMessageRequestAndSend();
                }
                break;

            default:
                break;
        }

        return false;
    }

    public void showAttachmentsPicker(String alertTitle) {
        final String[] items = {
                getString(R.string.msg_attach_from_device),
                getString(R.string.msg_attach_media),
                getString(R.string.msg_attach_summary_sections),
                getString(R.string.msg_attach_summary)
        };
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.custom_title_bar, null);
        TextView title = (TextView) deleteDialogView.findViewById(R.id.customtitlebar);
        ListView text = (ListView) deleteDialogView.findViewById(R.id.alertOptions);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(deleteDialogView);
//builder.setTitle(alertTitle);
        title.setText(alertTitle);
        builder.setCancelable(true);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
// Assign adapter to ListView
        text.setAdapter(adapter);
        final AlertDialog alert = builder.create();
        alert.show();

        text.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (items[position].equals(getString(R.string.msg_attach_media))) {
                    // navigate to media fragment to select media as attachment
                    final FragmentManager activityFragmentManager = mActivity.getSupportFragmentMngr();
                    final FragmentTransaction ft = activityFragmentManager.beginTransaction();
                    MediaFragment mediaFragment = new MediaFragment();
                    mediaFragment.setMediaAttachmentsListener(MessageComposeFragment.this);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                    bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_DISPLAY_LOCAL_MEDIA, false);
                    bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Media-All");
                    mediaFragment.setArguments(bundle);
                    ft.replace(R.id.container_layout, mediaFragment, MediaFragment.class.getSimpleName());
                    ft.addToBackStack(DashboardFragment.class.getSimpleName());
                    ft.commit();
                    alert.dismiss();
                } else if (items[position].equals(getString(R.string.msg_attach_from_device))) {
                    alert.dismiss();

                } else if (items[position].equals(getString(R.string.msg_attach_summary_sections))) {
                    // get Summary sections from server
                    getSummarySections();
                    alert.dismiss();

                } else if (items[position].equals(getString(R.string.msg_attach_summary))) {
                    alert.dismiss();

                }

            }
        });


    }

    private void getSummarySections() {
        if (summaryPanelDetailsObjsAL == null) {
            displayProgressLoader(false);
            GetSummarySectionsLoader getSummarySectionsLoader = new GetSummarySectionsLoader(mActivity, "", 100, AxSysConstants.SUMMARY_SECTIONS_PANEL_ID,
                    mActiveUser.getPatientID(), 1, mActiveUser.getToken(), "1");
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_SUMMARY_SECTIONS_LOADER), getSummarySectionsLoader);
        } else {
            // display summary sections list in separate screen and allow user to select for attachments
            final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
            final FragmentTransaction ft = activityFragmentManager.beginTransaction();
            SummarySectionsFragment summarySectionsFragment = new SummarySectionsFragment();
            summarySectionsFragment.setSummarySelectionListener(MessageComposeFragment.this);
            Bundle bundle = new Bundle();
            bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_select_sections));
            bundle.putSerializable(AxSysConstants.EXTRA_SUMMARY_SECTIONS_LIST, summaryPanelDetailsObjsAL);
            bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
            summarySectionsFragment.setArguments(bundle);
            ft.add(R.id.container_layout, summarySectionsFragment, SummarySectionsFragment.class.getSimpleName());
            ft.addToBackStack(SummarySectionsFragment.class.getSimpleName());
            ft.commit();
        }
    }

    private boolean validateUserInputs() {
        if (toRecipientsList == null) {
            showToast(R.string.select_atleast_one_receipient);
            return false;
        }
        if (toRecipientsList != null && toRecipientsList.size() == 0) {
            showToast(R.string.select_atleast_one_receipient);
            return false;
        }

        return true;
    }

    private void prepareMessageRequestAndSend() {
        JSONObject mainJson = new JSONObject();

        try {
            JSONObject recipentDetailsJson = new JSONObject();
            JSONArray recipientsJsonArray = new JSONArray();

            if (toRecipientsList != null) {
                for (int idx = 0; idx < toRecipientsList.size(); idx++) {
                    JSONObject recipientJson = new JSONObject();
                    recipientJson.put("RecipientType_LU", toRecipientsList.get(idx).getRecipientType_LU());
                    recipientJson.put("RecipientUserID", toRecipientsList.get(idx).getRecipientUserID());

                    recipientsJsonArray.put(recipientJson);
                }
            }

            if (ccRecipientsList != null) {
                for (int idx = 0; idx < ccRecipientsList.size(); idx++) {
                    JSONObject recipientJson = new JSONObject();
                    recipientJson.put("RecipientType_LU", ccRecipientsList.get(idx).getRecipientType_LU());
                    recipientJson.put("RecipientUserID", ccRecipientsList.get(idx).getRecipientUserID());

                    recipientsJsonArray.put(recipientJson);
                }
            }

            recipentDetailsJson.put("Recipients", recipientsJsonArray);
            //********************
            JSONObject messageDetailsJson = new JSONObject();
            messageDetailsJson.put("Priority_Lu", mPriorityLU);
            messageDetailsJson.put("Message", msgBodyEditText.getText().toString().trim());
            messageDetailsJson.put("Subject", subjectEditText.getText().toString().trim());
            messageDetailsJson.put("MessageFormattedText", msgBodyEditText.getText().toString().trim());
            //********************
            JSONObject attachmentDetailsJson = new JSONObject();
            JSONArray attachmentsJsonArray = new JSONArray();

            if (attachmentDetailsAL != null && attachmentDetailsAL.size() > 0) {
                for (int idx = 0; idx < attachmentDetailsAL.size(); idx++) {
                    AttachmentDetails attachmentDetails = attachmentDetailsAL.get(idx);

                    JSONObject attachmentJson = new JSONObject();

                    if (attachmentDetails.getAttachmentType_LU().equalsIgnoreCase(AxSysConstants.SUMMARY_ATTACHMENT_TYPE_LU)) {
                        attachmentJson.put("Attachment", !TextUtils.isEmpty(attachmentDetails.getDashboardId()) ? attachmentDetails.getDashboardId() : "");
                        attachmentJson.put("AttachmentType_LU", !TextUtils.isEmpty(attachmentDetails.getAttachmentType_LU()) ? attachmentDetails.getAttachmentType_LU() : "");
                        attachmentJson.put("AttachmentSize", !TextUtils.isEmpty(attachmentDetails.getAttachmentSize()) ? attachmentDetails.getAttachmentSize() : "");
                        attachmentJson.put("AttachmentName", !TextUtils.isEmpty(attachmentDetails.getAttachmentName()) ? attachmentDetails.getAttachmentName() : "");

                        attachmentsJsonArray.put(attachmentJson);
                    } else if (attachmentDetails.getAttachmentType_LU().equalsIgnoreCase(AxSysConstants.MEDIA_IMAGES_ATTACHMENT_ID) ||
                            attachmentDetails.getAttachmentType_LU().equalsIgnoreCase(AxSysConstants.MEDIA_VIDEO_ATTACHMENT_ID) ||
                            attachmentDetails.getAttachmentType_LU().equalsIgnoreCase(AxSysConstants.MEDIA_AUDIO_ATTACHMENT_ID)) {

                        attachmentJson.put("Attachment", !TextUtils.isEmpty(attachmentDetails.getAttachment()) ? attachmentDetails.getAttachment() : "");
                        attachmentJson.put("AttachmentType_LU", !TextUtils.isEmpty(attachmentDetails.getAttachmentType_LU()) ? attachmentDetails.getAttachmentType_LU() : "");
                        attachmentJson.put("AttachmentSize", !TextUtils.isEmpty(attachmentDetails.getAttachmentSize()) ? attachmentDetails.getAttachmentSize() : "");
                        attachmentJson.put("AttachmentName", !TextUtils.isEmpty(attachmentDetails.getAttachmentName()) ? attachmentDetails.getAttachmentName() : "");

                        attachmentsJsonArray.put(attachmentJson);
                    }
                }
            }

            attachmentDetailsJson.put("Attachments", attachmentsJsonArray);

            //********************
            mainJson.put("Token", mActiveUser.getToken());
            mainJson.put("RecipentDetails", recipentDetailsJson.toString());
            mainJson.put("MessageDetails", messageDetailsJson.toString());
            mainJson.put("AttachmentDetails", attachmentDetailsJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (CommonUtils.hasInternet()) {
            // Reset global data here
            displayProgressLoader(false);
            MessageSendingLoader messageSendingLoader = new MessageSendingLoader(mActivity, mainJson.toString());
            if (getView() != null && isAdded()) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SEND_MESSAGE_LOADER), messageSendingLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }

    }

    @Override
    public void onProviderSelected(SearchDoctorsResponse.DoctorDetails selectedClinicianObject, ClinicianDetailsResponse.ClinicianDetailsData clinicianDetailsData) {
        Trace.d(TAG, "selectedClinicianObject:" + selectedClinicianObject);

        if (selectedClinicianObject != null) {
            if (selectedSourceForProvider == SelectedSourceForProvider.TO) {
                final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

                if (recipientLayout != null) {
                    final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
                    final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);

                    receipientNameTextView.setText(!TextUtils.isEmpty(selectedClinicianObject.getClinicianName()) ? (selectedClinicianObject.getClinicianName() + (
                            !TextUtils.isEmpty(selectedClinicianObject.getSpeciality()) ? ("(" + selectedClinicianObject.getSpeciality() + ")") : "")) : "");

                    // add selected recipient to AL
                    final RecipientDetails recipientDetails = new RecipientDetails(AxSysConstants.TO_RECIPIENT_TYPE_LU, selectedClinicianObject.getClinicianUserID());
                    if (toRecipientsList.size() > 0) {
                        toRecipientsList.set(0, recipientDetails);
                    } else {
                        toRecipientsList.add(0, recipientDetails);
                    }
                    removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setCancelable(true);
                            builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                            builder.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    recipientLayout.setVisibility(View.GONE);
                                    toRecipientsList.remove(0);
                                    toDetailsLayout.removeAllViews();
                                    if (toDetailsLayout.getChildCount() == 0) {
                                        toDetailsLayout.setVisibility(View.INVISIBLE);
                                    } else {
                                        toDetailsLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            builder.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                    // Add recipient view to main linear layout
                    toDetailsLayout.removeAllViews();
                    toDetailsLayout.addView(recipientLayout, 0);
                    if (toDetailsLayout.getChildCount() == 0) {
                        toDetailsLayout.setVisibility(View.INVISIBLE);
                    } else {
                        toDetailsLayout.setVisibility(View.VISIBLE);
                    }
                }
            } else if (selectedSourceForProvider == SelectedSourceForProvider.CC) {
                final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

                if (recipientLayout != null) {
                    final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
                    final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);

                    receipientNameTextView.setText(!TextUtils.isEmpty(selectedClinicianObject.getClinicianName()) ? (selectedClinicianObject.getClinicianName() + (
                            !TextUtils.isEmpty(selectedClinicianObject.getSpeciality()) ? ("(" + selectedClinicianObject.getSpeciality() + ")") : "")) : "");

                    // add selected recipient to AL
                    final RecipientDetails recipientDetails = new RecipientDetails(AxSysConstants.CC_RECIPIENT_TYPE_LU, selectedClinicianObject.getClinicianUserID());
                    ccRecipientsList.add(recipientDetails);

                    removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setCancelable(true);
                            builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                            builder.setPositiveButton(getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ccRecipientsList.remove(recipientDetails);

                                    recipientLayout.setVisibility(View.GONE);
                                    ccDetailsLayout.removeView(recipientLayout);
                                    ccDetailsLayout.invalidate();
                                    ;
                                    if (ccDetailsLayout.getChildCount() == 0) {
                                        ccDetailsLayout.setVisibility(View.INVISIBLE);
                                    } else {
                                        ccDetailsLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            builder.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                    // Add recipient view to main linear layout
                    ccDetailsLayout.addView(recipientLayout);
                    if (ccDetailsLayout.getChildCount() == 0) {
                        ccDetailsLayout.setVisibility(View.INVISIBLE);
                    } else {
                        ccDetailsLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onCircleMemberSelected(ListMembersInCircleResponse.CircleMemberDetails selectedCircleMemberObject) {
        Trace.d(TAG, "selectedCircleMemberObject:" + selectedCircleMemberObject);

        if (selectedCircleMemberObject != null) {
            if (selectedSourceForListMember == SelectedSourceForListMember.TO) {
                final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

                if (recipientLayout != null) {
                    final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
                    final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);

                    receipientNameTextView.setText(!TextUtils.isEmpty(selectedCircleMemberObject.getUserCircleMemberEmail_ID()) ? (selectedCircleMemberObject.getUserCircleMemberEmail_ID() + (
                            !TextUtils.isEmpty(selectedCircleMemberObject.getSpeciality()) ? ("(" + selectedCircleMemberObject.getSpeciality() + ")") : "")) : "");

                    // add selected recipient to AL
                    final RecipientDetails recipientDetails = new RecipientDetails(AxSysConstants.TO_RECIPIENT_TYPE_LU, selectedCircleMemberObject.getUserID());
                    if (toRecipientsList.size() > 0) {
                        toRecipientsList.set(0, recipientDetails);
                    } else {
                        toRecipientsList.add(0, recipientDetails);
                    }
                    removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setCancelable(true);
                            builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                            builder.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    recipientLayout.setVisibility(View.GONE);
                                    toRecipientsList.remove(0);
                                    toDetailsLayout.removeAllViews();
                                    if (toDetailsLayout.getChildCount() == 0) {
                                        toDetailsLayout.setVisibility(View.INVISIBLE);
                                    } else {
                                        toDetailsLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            builder.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                    // Add recipient view to main linear layout
                    toDetailsLayout.removeAllViews();
                    toDetailsLayout.addView(recipientLayout, 0);
                    if (toDetailsLayout.getChildCount() == 0) {
                        toDetailsLayout.setVisibility(View.INVISIBLE);
                    } else {
                        toDetailsLayout.setVisibility(View.VISIBLE);
                    }
                }
            } else if (selectedSourceForListMember == SelectedSourceForListMember.CC) {
                final LinearLayout recipientLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.message_receipients_row_layout, null);

                if (recipientLayout != null) {
                    final TextView receipientNameTextView = (TextView) recipientLayout.findViewById(R.id.receipientNameTextView);
                    final ImageView removeReceipientImageView = (ImageView) recipientLayout.findViewById(R.id.removeReceipientImageView);

                    receipientNameTextView.setText(!TextUtils.isEmpty(selectedCircleMemberObject.getUserCircleMemberEmail_ID()) ? (selectedCircleMemberObject.getUserCircleMemberEmail_ID() + (
                            !TextUtils.isEmpty(selectedCircleMemberObject.getSpeciality()) ? ("(" + selectedCircleMemberObject.getSpeciality() + ")") : "")) : "");

                    // add selected recipient to AL
                    final RecipientDetails recipientDetails = new RecipientDetails(AxSysConstants.CC_RECIPIENT_TYPE_LU, selectedCircleMemberObject.getUserID());
                    ccRecipientsList.add(recipientDetails);

                    removeReceipientImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setCancelable(true);
                            builder.setMessage(getString(R.string.confirmation_to_delete_recipient));
                            builder.setPositiveButton(getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ccRecipientsList.remove(recipientDetails);

                                    recipientLayout.setVisibility(View.GONE);
                                    ccDetailsLayout.removeView(recipientLayout);
                                    ccDetailsLayout.invalidate();
                                    if (ccDetailsLayout.getChildCount() == 0) {
                                        ccDetailsLayout.setVisibility(View.INVISIBLE);
                                    } else {
                                        ccDetailsLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            builder.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                    // Add recipient view to main linear layout
                    ccDetailsLayout.addView(recipientLayout);
                    if (ccDetailsLayout.getChildCount() == 0) {
                        ccDetailsLayout.setVisibility(View.INVISIBLE);
                    } else {
                        ccDetailsLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onSummarySectionsSelected(ArrayList<SummarySectionsResponse.SummaryPanelDetailsObj> summaryPanelDetailsAL) {
        this.summaryPanelDetailsObjsAL = summaryPanelDetailsAL;

        if (attachmentDetailsAL != null && summaryPanelDetailsObjsAL != null) {
            for (int idx = 0; idx < summaryPanelDetailsObjsAL.size(); idx++) {
                SummarySectionsResponse.SummaryPanelDetailsObj summaryPanelDetailsObj = summaryPanelDetailsObjsAL.get(idx);

                // Add selected summary panels to attachments list datasource
                AttachmentDetails attachmentDetails = new AttachmentDetails();
                attachmentDetails.setAttachmentType(AxSysConstants.ATTACHMENT_TYPE_SUMMARY);
                attachmentDetails.setAttachment(summaryPanelDetailsObj.getPanelID());
                attachmentDetails.setAttachmentName(summaryPanelDetailsObj.getComprehensiveHealthSummaryList());
                attachmentDetails.setAttachmentSize("");
                attachmentDetails.setAttachmentType_LU(AxSysConstants.SUMMARY_ATTACHMENT_TYPE_LU);
                attachmentDetails.setDashboardId(summaryPanelDetailsObj.getDashboardId());

                if (summaryPanelDetailsObj.isSelected()) {
                    int addedIndex = isAttachmentAlreadyAdded(attachmentDetailsAL, attachmentDetails);
                    if (addedIndex == -1) {
                        attachmentDetailsAL.add(attachmentDetails);
                    } else {
                        attachmentDetailsAL.set(addedIndex, attachmentDetails);
                    }
                } else {
                    // TODO handle negative scenario while removing the un-selected summary section
                    attachmentDetailsAL.remove(attachmentDetails);
                }
            }

            // update attachments listview
            if (attachmentsListAdapter == null) {
                attachmentsListAdapter = new AttachmentsListAdapter(attachmentDetailsAL);
                attachmentsListView.setAdapter(attachmentsListAdapter);
            } else {
                attachmentsListAdapter.updateAttachmentsData(attachmentDetailsAL);
            }
            setListViewHeightBasedOnChildren(attachmentsListView);
        }
    }

    @Override
    public void onMediaSelectedForMsgAttachments(ArrayList<AttachmentDetails> mediaAttachmentsAL) {
        if (mediaAttachmentsAL != null && mediaAttachmentsAL.size() > 0) {
            attachmentDetailsAL.addAll(mediaAttachmentsAL);

            // update attachments listview
            if (attachmentsListAdapter == null) {
                attachmentsListAdapter = new AttachmentsListAdapter(attachmentDetailsAL);
                attachmentsListView.setAdapter(attachmentsListAdapter);
            } else {
                attachmentsListAdapter.updateAttachmentsData(attachmentDetailsAL);
            }
        }
    }

    private int isAttachmentAlreadyAdded(ArrayList<AttachmentDetails> attachmentsAL, AttachmentDetails attachmentDetails) {
        if (attachmentsAL != null && attachmentsAL.size() > 0) {
            for (int idx = 0; idx < attachmentsAL.size(); idx++) {
                if (attachmentsAL.get(idx).getAttachment().equalsIgnoreCase(attachmentDetails.getAttachment())) {
                    return idx;
                }
            }
        }

        return -1;
    }

    private class AttachmentsListAdapter extends BaseAdapter {

        private ArrayList<AttachmentDetails> attachmentsAL;

        public AttachmentsListAdapter(ArrayList<AttachmentDetails> attachmentsAL) {
            this.attachmentsAL = attachmentsAL;
        }

        public void updateAttachmentsData(ArrayList<AttachmentDetails> attachmentsAL) {
            this.attachmentsAL = attachmentsAL;
            notifyDataSetChanged();
        }


        @Override
        public int getCount() {
            return (attachmentsAL != null && attachmentsAL.size() > 0) ? attachmentsAL.size() : 0;
        }

        @Override
        public AttachmentDetails getItem(int position) {
            return attachmentsAL.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final AttachmentDetails rowObj = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.attachments_list_row, parent, false);
            }

            ImageView attachmentImageView = (ImageView) convertView.findViewById(R.id.attachmentImageView);
            TextView attachmentNameTextView = (TextView) convertView.findViewById(R.id.attachmentNameTextView);
            ImageView removeAttachmentImageView = (ImageView) convertView.findViewById(R.id.removeAttachmentImageView);
            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            attachmentNameTextView.setTypeface(regular);
            if(rowObj.getAttachmentType() == "ATTACHMENT_TYPE_MEDIA_IMAGE"){
                attachmentImageView.setImageResource(R.drawable.messages_gallery);
            }
            else if(rowObj.getAttachmentType() =="ATTACHMENT_TYPE_MEDIA_AUDIO"){
                attachmentImageView.setImageResource(R.drawable.messages_audio);
            }
            else if(rowObj.getAttachmentType() == "ATTACHMENT_TYPE_MEDIA_VIDEO"){
                attachmentImageView.setImageResource(R.drawable.messages_video);
            }
            else {
                attachmentImageView.setImageResource(R.drawable.message_attachment);
            }
            attachmentNameTextView.setText(!TextUtils.isEmpty(rowObj.getAttachmentName()) ? rowObj.getAttachmentName() : "");

            removeAttachmentImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attachmentsAL.remove(rowObj);
                    notifyDataSetChanged();
                    setListViewHeightBasedOnChildren(attachmentsListView);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO Navigate to summary panel details if attachment type is Summary Panel

                }
            });

            return convertView;
        }
    }

    /****
     * Method for Setting the Height of the ListView dynamically.
     * *** Hack to fix the issue of not showing all the items of the ListView
     * *** when placed inside a ScrollView
     ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

}
