package com.iversecomics.client.util;

import android.graphics.Matrix;

public final class MatrixUtil {
    public static final int MATRIX_SIZE = 9;
    public static final Matrix IDENTITY = new Matrix();

    public static void append(StringBuilder b, String prefix, Matrix matrix) {
        b.append(prefix).append("Matrix[");
        float vals[] = new float[MATRIX_SIZE];
        matrix.getValues(vals);

        if (matrix.isIdentity()) {
            b.append("IDENTITY");
        } else {
            b.append("trans=").append((int) vals[Matrix.MTRANS_X]).append("/").append((int) vals[Matrix.MTRANS_Y]);
            if ((vals[Matrix.MSCALE_X] == 1) && (vals[Matrix.MSCALE_Y] == 1)) {
                b.append(",no-scale");
            } else {
                b.append(",scale=");
                b.append(percentage(vals[Matrix.MSCALE_X]));
                if (vals[Matrix.MSCALE_X] != vals[Matrix.MSCALE_Y]) {
                    b.append("/").append(percentage(vals[Matrix.MSCALE_Y]));
                }
            }
            if ((vals[Matrix.MSKEW_X] != 0) && (vals[Matrix.MSKEW_Y] != 0)) {
                b.append(",skew=").append(vals[Matrix.MSKEW_X]).append("/").append(vals[Matrix.MSKEW_Y]);
            } else {
                b.append(",no-skew");
            }
            if ((vals[Matrix.MPERSP_0] != 0) && (vals[Matrix.MPERSP_1] != 0) && (vals[Matrix.MPERSP_2] != 1)) {
                b.append(",persp=");
                b.append(vals[Matrix.MPERSP_0]).append("/");
                b.append(vals[Matrix.MPERSP_1]).append("/");
                b.append(vals[Matrix.MPERSP_2]);
            } else {
                b.append(",no-persp");
            }
        }
        b.append("]");
    }

    public static String dump(Matrix matrix) {
        StringBuilder b = new StringBuilder();
        append(b, "", matrix);
        return b.toString();
    }

    public static String percentage(float f) {
        return ((int) (f * 100)) + "%";
    }
}
