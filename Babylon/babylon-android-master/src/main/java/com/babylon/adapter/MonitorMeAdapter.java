package com.babylon.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.MonitorDescriptionActivity;
import com.babylon.model.Icon;
import com.babylon.model.MonitorMeCategory;
import com.babylon.utils.Constants;
import com.babylon.utils.UIUtils;
import com.babylon.view.ProgressCircleView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Shows menu items
 */
public class MonitorMeAdapter extends BaseAdapter {

    public interface OnPropertyItemClickListener {
        View.OnClickListener getOnPropertyClickListener(int featureId, String paramName,
                                                        String description);
    }

    private Context context;
    private List<MonitorMeCategory> categories;
    private OnPropertyItemClickListener onPropertyItemClickListener;

    public MonitorMeAdapter(Context context, List<MonitorMeCategory> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MonitorMeCategory getItem(int position) {
        return categories.get(position);
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MonitorMeCategory category = categories.get(position);

        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.list_item_monitor_me, null);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) convertView.findViewById(R.id.list_item_monitor_me_category_text_view);
            holder.propertiesLayout = (LinearLayout) convertView.findViewById(R.id.list_item_monitor_me_circles_layout);
            holder.iconImageView = (ImageView) convertView.findViewById(R.id.list_item_monitor_me_category_image_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final String categoryName = category.getName();
        final String categoryId = category.getId();

        if (categoryName != null && !TextUtils.isEmpty(categoryName)) {
            holder.titleTextView.setText(categoryName);
        }
        List<Icon> icons = category.getIconsReport();
        if (icons != null && !icons.isEmpty()) {
            String imageUrl = UIUtils.getImageUrl(icons, context);
            if (imageUrl != null) {
                Picasso.with(context).load(imageUrl).into(holder.iconImageView);
                if (categoryId != null && !TextUtils.isEmpty(categoryId)) {
                    holder.iconImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, MonitorDescriptionActivity.class);
                            intent.putExtra(Keys.MONITOR_CATEGORY_ID, categoryId);
                            context.startActivity(intent);
                        }
                    });
                }
            }
        }

        if (category.getFeatures() != null && !category.getFeatures().isEmpty()) {
            holder.propertiesLayout.removeAllViews();

            for (MonitorMeCategory.Feature feature : category.getFeatures()) {
                View circleView = View.inflate(context, feature.getTargetValue() != null
                        ? R.layout.list_item_progress_circle : R.layout.list_item_circle, null);
                TextView circleTextView = (TextView) circleView.findViewById(R.id.list_item_circle_text_view);
                TextView nameTextView = (TextView) circleView.findViewById(R.id.list_item_name_text_view);
                ProgressCircleView progressCircleView = (ProgressCircleView)
                        circleView.findViewById(R.id.list_item_circle_progress_view);

                circleTextView.setText(feature.getViewValue());

                int circleBackgroundSelector = 0;
                int colorResource = 0;
                switch (feature.getState()) {
                    case Constants.MONITOR_ME_BAD_STATE:
                        colorResource = R.color.red;
                        circleBackgroundSelector = R.drawable.selector_red_circle_button;
                        break;
                    case Constants.MONITOR_ME_NORMAL_STATE:
                        colorResource = R.color.green_dark;
                        circleBackgroundSelector = R.drawable.selector_green_circle_button;
                        break;
                    case Constants.MONITOR_ME_WARNING_STATE:
                        colorResource = R.color.orange;
                        circleBackgroundSelector = R.drawable.selector_orange_circle_button;
                        break;
                    case Constants.MONITOR_ME_UNDEFINIED_STATE:
                        colorResource = R.color.white;
                        circleBackgroundSelector = R.drawable.selector_white_circle_button;
                        break;
                    default:
                        break;
                }
                if (circleBackgroundSelector != 0 && feature.getTargetValue() == null) {
                    circleTextView.setBackgroundResource(circleBackgroundSelector);
                }
                if (feature.getTitle() != null) {
                    nameTextView.setText(Html.fromHtml(feature.getTitle()));

                }
                if (feature.getTargetValue() != null && progressCircleView != null
                        && colorResource != 0) {
                    try {
                        Double targetValue = feature.getTargetValue();
                        Double value = Double.valueOf(feature.getViewValue());
                        int absoluteProgress = (int) (Constants.MONITOR_ME_FULL_PERCENT * value / targetValue);

                        if (absoluteProgress > 0) {
                            showProgress(progressCircleView, colorResource, absoluteProgress);
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }

                /* Set onClickListener for properties */
                if (onPropertyItemClickListener != null) {
                    circleTextView.setOnClickListener(onPropertyItemClickListener
                            .getOnPropertyClickListener(feature.getFeatureId(), feature.getTitle(), feature.getDescription()));
                    nameTextView.setOnClickListener(onPropertyItemClickListener
                            .getOnPropertyClickListener(feature.getFeatureId(), feature.getTitle(), feature.getDescription()));
                }
                holder.propertiesLayout.addView(circleView);
            }
        }

        return convertView;
    }

    private void showProgress(ProgressCircleView progressCircleView, int colorResource, int absoluteProgress) {
        progressCircleView.setProgressColor(context.getResources().getColor(colorResource));
        progressCircleView.setAbsoluteProgress(absoluteProgress);
        progressCircleView.startAnimating();
    }

    public void setOnPropertyItemClickListener(OnPropertyItemClickListener onPropertyItemClickListener) {
        this.onPropertyItemClickListener = onPropertyItemClickListener;
    }

    public void setNewData(List<MonitorMeCategory> categories) {
        this.categories = categories;
    }

    private static class ViewHolder {
        TextView titleTextView;
        ImageView iconImageView;
        LinearLayout propertiesLayout;
    }
}