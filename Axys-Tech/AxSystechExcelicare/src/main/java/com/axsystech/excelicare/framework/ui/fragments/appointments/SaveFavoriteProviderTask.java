package com.axsystech.excelicare.framework.ui.fragments.appointments;

import android.os.AsyncTask;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class SaveFavoriteProviderTask extends AsyncTask<Void, Void, BaseResponse> {

    private String clinicanID;
    private boolean isFavourite;
    private String token;

    private SaveFavoriteProviderListener mListener;

    public SaveFavoriteProviderTask(String clinicanID, boolean isFavourite, String token, SaveFavoriteProviderListener listener) {
        this.clinicanID = clinicanID;
        this.isFavourite = isFavourite;
        this.token = token;
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected BaseResponse doInBackground(Void... params) {
        try {
            return ServerMethods.getInstance().saveFavoriteProvider(clinicanID, isFavourite ? "1" : "0", token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        super.onPostExecute(baseResponse);

        if(baseResponse != null) {
            if(mListener != null) {
                mListener.onFavoriteProviderSaved();
            }
        } else {
            mListener.onFavoriteProviderFailed();
        }
    }

    public interface SaveFavoriteProviderListener {
        public void onFavoriteProviderSaved();
        public void onFavoriteProviderFailed();
    }

}
