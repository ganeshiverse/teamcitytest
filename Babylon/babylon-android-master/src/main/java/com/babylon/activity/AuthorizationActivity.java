package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.babylon.Keys;
import com.babylon.R;


public class AuthorizationActivity extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        Button createAccountButton = (Button) findViewById(R.id.create_an_account_button);
        TextView signInTextView = (TextView) findViewById(R.id.sign_in_text_view);

        createAccountButton.setOnClickListener(this);
        signInTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_an_account_button: {
                Intent intent = new Intent(this, RegistrationActivity.class);
                intent.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
                startActivity(intent);
                break;
            }
            case R.id.sign_in_text_view: {
                Intent intent = new Intent(this, SignInActivity.class);
                intent.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
                startActivity(intent);
                break;
            }
            default:
                break;
        }
    }
}
