package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;
import com.babylon.model.MeasurePostModel;

public class MonitorMeParamsPost extends AbsRequestPost<MeasurePostModel, Void> {

    @Override
    protected String getUrl() {
        return Urls.get(Urls.MONITOR_ME_PARAMS_POST);
    }
}
