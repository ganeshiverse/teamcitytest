package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/15/2015.
 */
public class GetCircleMemberDetailsFromDbLoader extends DataAsyncTaskLibLoader<List<CirclesMemberDetailsDbModel>> {

    private String TAG = GetCircleMemberDetailsFromDbLoader.class.getSimpleName();

    public GetCircleMemberDetailsFromDbLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<CirclesMemberDetailsDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<CirclesMemberDetailsDbModel, Long> circleMemberListDao = helper.getDao(CirclesMemberDetailsDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<CirclesMemberDetailsDbModel>>() {
                @Override
                public List<CirclesMemberDetailsDbModel> call() throws Exception {
                    Trace.d(TAG, "Retrived circles members details from DB...");
                    return circleMemberListDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
