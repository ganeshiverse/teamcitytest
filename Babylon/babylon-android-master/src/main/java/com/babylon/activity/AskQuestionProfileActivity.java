package com.babylon.activity;

import android.text.TextUtils;
import com.babylon.App;
import com.babylon.R;
import com.babylon.model.Patient;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.view.TopBarInterface;

public class AskQuestionProfileActivity extends ProfileActivity {

    @Override
    public ProfileField[] getFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.HEIGHT,
                ProfileField.WEIGHT,
                ProfileField.GENDER,
                ProfileField.DATE_OF_BIRTH};
    }


    @Override
    public ProfileField[] getRequiredFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.HEIGHT,
                ProfileField.WEIGHT,
                ProfileField.GENDER,
                ProfileField.DATE_OF_BIRTH};
    }

    @Override
    public void setTopBar() {
        initializeTopBar();
        setTitle(R.string.ask_question_title);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.GREEN_THEME;
    }

    @Override
    public int getLayoutResID() {
        return R.layout.activity_ask_question_profile;
    }

    @Override
    public int getBackgroundResId() {
        return R.drawable.ask_background;
    }

    @Override
    public void onProfileSent() {
        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PATIENT,
                SyncAdapter.SYNC_PUSH);
        finish();
    }

    public static boolean isProfileValid() {
        final Patient patient = App.getInstance().getPatient();

        boolean result = true;

        if (patient.getWeight() == 0 || patient.getHeight() == 0 || TextUtils.isEmpty(patient.getGender()) ||
                patient.getBirthday() == null) {
            result = false;
        }

        return result;
    }
}
