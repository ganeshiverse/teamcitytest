package com.babylon.enums;

import com.babylon.App;
import com.babylon.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum MonitorMeCategory {
    FITNESS(R.string.monitor_me_fragment_fitness, R.drawable.ic_monitor_fitness, R.array.monitor_me_fragment_fitness_fields),
    WEIGHT(R.string.monitor_me_fragment_weight, R.drawable.weight, R.array.monitor_me_fragment_weight_fields),
    HEART(R.string.monitor_me_fragment_heart, R.drawable.heart, R.array.monitor_me_fragment_heart_fields),
    LIVER(R.string.monitor_me_fragment_liver, R.drawable.liver, R.array.monitor_me_fragment_liver_fields),
    KIDNEY(R.string.monitor_me_fragment_kidney, R.drawable.kidneys, R.array.monitor_me_fragment_kidney_fields),
    VITAMINS(R.string.monitor_me_fragment_vitamins, R.drawable.vitamins, R.array.monitor_me_fragment_vitamins_fields),
    BONE(R.string.monitor_me_fragment_bone, R.drawable.bones, R.array.monitor_me_fragment_bone_fields),
    HORMONES(R.string.monitor_me_fragment_hormones, R.drawable.hormones, R.array.monitor_me_fragment_hormones_fields),
    BLOOD(R.string.monitor_me_fragment_blood, R.drawable.blood, R.array.monitor_me_fragment_blood_fields);

    private MonitorMeCategory(int titleId, int iconId, int propertyStringArrayId) {
        title = App.getInstance().getString(titleId);
        drawableIconId = iconId;
        String[] propertyStringArray = App.getInstance().getResources().getStringArray(propertyStringArrayId);
        if (propertyStringArray != null) {
            properties = new ArrayList<Property>();
            for (String property : propertyStringArray) {
                Property propertyField = new Property();
                propertyField.setName(property);
                properties.add(propertyField);
            }
        }
    }

    private String title;
    private int drawableIconId;
    private List<Property> properties;

    public String getTitle() {
        return title;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public int getDrawableIconId() {
        return drawableIconId;
    }

    public static List<MonitorMeCategory> getMonitorMeCategories() {
        return Arrays.asList(MonitorMeCategory.values());
    }

    public class Property {
        private String name;
        private String value;
        private int colorResourceId = R.color.green_dark;
        private int progress;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public int getColorResourceId() {
            return colorResourceId;
        }

        public void setColorResourceId(int colorResourceId) {
            this.colorResourceId = colorResourceId;
        }

        public int getProgress() {
            return progress;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }
    }
}
