package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.babylon.R;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.adapter.MonitorMePagerAdapter;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.controllers.OnDateSetListener;
import com.babylon.controllers.OnInvalidCache;
import com.babylon.fragment.DataSourcesFragment;
import com.babylon.fragment.OnlyChartFragment;
import com.babylon.fragment.OrderTempFragment;
import com.babylon.fragment.tour.MonitorMeGuideActivity;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.monitor.MonitorReportSync;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.samsung.health.SamsungHealthManager;
import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class MonitorMeActivity extends BaseActivity implements OnInvalidCache,
        View.OnClickListener, OnlyChartFragment.OnMedicalKitClickListener {

    private int prevPage = MonitorMePagerAdapter.MAX_PAGE_NUMBER;
    private ViewPager viewPager;
    private LoadingDateRange dateRange = new LoadingDateRange();
    private Set<Integer> loadedPages = new HashSet<>();
    private OrderTempFragment orderTempFragment;
    private SamsungHealthManager samsungHealthManager = SamsungHealthManager.getInstance();
    private boolean isFirstMonitorMeLaunch;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, MonitorMeActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final boolean isProfileValid = MonitorMeProfileActivity.isProfileValid();
        isFirstMonitorMeLaunch = PreferencesManager.getInstance().isFirstMonitorMeLaunch();
        if (PreferencesManager.getInstance().isFirstMonitorMeLaunch()) {
            MonitorMeGuideActivity.startActivityForResult(this,
                    MonitorMeGuideActivity.START_MONITOR_FOR_RESULT);
        }
        else if (!isProfileValid) {
                finish();
                MonitorMeProfileActivity.startActivity(this);

        }

        setContentView(R.layout.activity_monitor_me);

        initViews();

        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY,
                SyncAdapter.SYNC_PUSH);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isFirstMonitorMeLaunch) {
            syncSamsungHealthData();
        }
    }

    @Override
    protected void onPause() {
        if (samsungHealthManager.isActivated()) {
            samsungHealthManager.onDestroy();
        }
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isFirstMonitorMeLaunch = false;

        if (requestCode == MonitorMeGuideActivity.START_MONITOR_FOR_RESULT
                && resultCode == Activity.RESULT_CANCELED) {

            final boolean isProfileValid = MonitorMeProfileActivity.isProfileValid();

            if (!isProfileValid) {
                finish();
                MonitorMeProfileActivity.startActivity(this);
            }
        }
    }

    @Override
    public void onInvalidCache() {
        loadedPages.clear();
    }

    private void syncSamsungHealthData() {
        if (samsungHealthManager.isAvailable()) {
            startSamsungHealthSynchronization(false);
        }
    }

    private void startSamsungHealthSynchronization(boolean shouldAskPermissions) {
        samsungHealthManager.onCreate(this, new MonitorMeSamsungHealthListener(), shouldAskPermissions);
    }

    private void initPageView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        final MonitorMePagerAdapter adapter = new MonitorMePagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(MonitorMePagerAdapter.MAX_PAGE_NUMBER);

        viewPager.setOnPageChangeListener(mOnPageChangedListener);
    }

    private void initViews() {
        setTitle(R.string.monitor_me_fragment_title);
        activeBackButton();
        configNavigationButtons(MonitorMePagerAdapter.MAX_PAGE_NUMBER);
        showActiveDate(new Date());
        initPageView();
        setRightImageButton(this, R.drawable.ic_menu_sync);
        setSecondRightImageButton(getOnKitClickListener(),
                R.drawable.ic_menu_kit);

        TextView dateTextView = (TextView) findViewById(R.id.text_view_date);

        new MonitorMeDateViewController(this, dateTextView, new OnDateSetListener() {

            @Override
            public void onDateSet(Date date) {
                int dayNumber = dateRange.getDayNumber(date);
                prevPage = dayNumber;

                if (dayNumber != viewPager.getCurrentItem()) {
                    loadNewPageForDayNumber(dayNumber);
                }
            }
        }, new Date());
    }

    private void loadNewPageForDayNumber(int dayNumber) {
        if (!dateRange.multipleNumber(dayNumber)) {
            dateRange.findDateLimits(dayNumber);

            startMonitorMeReportSync(dateRange.getMinDate(), dateRange.getMaxDate());
        }

        viewPager.setCurrentItem(dayNumber, true);

        //because of bug in android. onPageSelected isn't triggered when calling setCurrentItem(0)
        if (dayNumber == 0) {
            onPageSelectedListener(dayNumber);
        }
    }


    private void configNavigationButtons(int position) {
        ImageButton nextBtn = (ImageButton) findViewById(R.id.img_btn_next);
        ImageButton prevBtn = (ImageButton) findViewById(R.id.img_btn_prev);

        if (position == MonitorMePagerAdapter.MAX_PAGE_NUMBER) {
            nextBtn.setVisibility(View.INVISIBLE);
            prevBtn.setVisibility(View.VISIBLE);
        } else if (position == MonitorMePagerAdapter.MIN_PAGE_NUMBER) {
            nextBtn.setVisibility(View.VISIBLE);
            prevBtn.setVisibility(View.INVISIBLE);
        } else {
            nextBtn.setVisibility(View.VISIBLE);
            prevBtn.setVisibility(View.VISIBLE);
        }
    }

    private void showActiveDate(Date date) {
        TextView dateTextView = (TextView) findViewById(R.id.text_view_date);
        dateTextView.setText(new MonitorMeDateViewController().getFormattedDay(date));
    }

    public void onPrevClickListener(View view) {
        int curPosition = viewPager.getCurrentItem();
        viewPager.setCurrentItem(--curPosition, true);
    }

    public void onNextClickListener(View view) {
        int curPosition = viewPager.getCurrentItem();
        viewPager.setCurrentItem(++curPosition, true);
    }

    private ViewPager.SimpleOnPageChangeListener mOnPageChangedListener = new ViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            onPageSelectedListener(position);
        }
    };

    private void onPageSelectedListener(int position) {
        tryToLoadPages(position);

        prevPage = position;

        Date date = new Date(DateUtils.getDayStartMills(-(MonitorMePagerAdapter.MAX_PAGE_NUMBER - position)));

        showActiveDate(date);
        configNavigationButtons(position);
    }

    private void tryToLoadPages(int position) {
        if (dateRange.multipleNumber(position) && !loadedPages.contains(position)) {
            dateRange.findDateLimits(prevPage, position);

            startMonitorMeReportSync(dateRange.getMinDate(), dateRange.getMaxDate());
            loadedPages.add(position);
        } else if (position == MonitorMePagerAdapter.MAX_PAGE_NUMBER - 1) {
            dateRange.findDateLimits(position);

            startMonitorMeReportSync(dateRange.getMinDate(), dateRange.getMaxDate());
            loadedPages.add(position);
        }
    }

    private void startMonitorMeReportSync(long minDate, long maxDate) {
        final Bundle b = getBundle(minDate, maxDate);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                new MonitorReportSync().pull(getContentResolver(), b);
                return null;
            }
        }.execute();
    }

    private Bundle getBundle(long minDate, long maxDate) {
        final Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, minDate);
        b.putLong(Constants.EXTRA_MAX_DATE, maxDate);
        return b;
    }


    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.topbar_right_button:
                getSupportFragmentManager().beginTransaction().replace(android.R.id.content,
                        new DataSourcesFragment())
                        .addToBackStack(null)
                        .commit();
                break;
            default:
                break;
        }
    }

    @Override
    public View.OnClickListener getOnKitClickListener() {
        return onKitClickListener;
    }

    private View.OnClickListener onKitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           /* if (orderTempFragment == null) {
                orderTempFragment = new OrderTempFragment();
            }
            startFragment(orderTempFragment);
*/
            Intent  intent = new Intent(MonitorMeActivity.this, ShopCategoriesActivity.class);
            startActivity(intent);
        }
    };

    public class MonitorMeSamsungHealthListener implements
            SamsungHealthManager.SamsungHealthListener {

        @Override
        public void onHealthStoreSuccessConnected() {
            Toast.makeText(MonitorMeActivity.this, R.string.samsung_health_success,
                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onHealthStorePermissionFailed() {
            AlertDialogHelper.getInstance().showConfirmationDialog(MonitorMeActivity.this,
                    getString(R.string.samsung_health_permission_failed),
                    getString(R.string.samsung_health_yes),
                    getString(R.string.samsung_health_no),
                    new AlertDialogHelper.ConfirmationDialogListener() {
                        @Override
                        public void confirm() {
                            samsungHealthManager.onDestroy();
                        }

                        @Override
                        public void cancel() {
                            startSamsungHealthSynchronization(true);
                        }
                    });
        }

        @Override
        public void onRequestSynchronization(boolean isPermissionApproved) {
            startSamsungHealthSynchronization(isPermissionApproved);
        }

        @Override
        public void onNoPermissionsSelected() {
            if(samsungHealthManager.isFirstConnection()) {
                AlertDialogHelper.getInstance().showConfirmationDialog(MonitorMeActivity.this,
                        getString(R.string.samsung_health_wish_question),
                        getString(android.R.string.yes),
                        getString(android.R.string.no),
                        new AlertDialogHelper.ConfirmationDialogListener() {
                            @Override
                            public void confirm() {
                                onRequestSynchronization(true);
                            }

                            @Override
                            public void cancel() {
                                Toast.makeText(MonitorMeActivity.this,
                                        R.string.samsung_health_settings, Toast.LENGTH_LONG).show();
                            }
                        });
                samsungHealthManager.setFirstConnection(false);
            }
        }

        @Override
        public void onDisconnected() {

        }

        @Override
        public void onHealthStoreFailConnected(HealthConnectionErrorResult error) {
        }
    }
}