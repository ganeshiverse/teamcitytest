package com.axsystech.excelicare.network;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;

import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.data.responses.AddProxyResponse;
import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.data.responses.AppointmentDetailsResponse;
import com.axsystech.excelicare.data.responses.AppointmentListResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.BookAppointmentResponse;
import com.axsystech.excelicare.data.responses.CardServiceDataResponse;
import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.data.responses.CircleDetailsEditResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.data.responses.CircleUserPermissionsDataResponse;
import com.axsystech.excelicare.data.responses.CityListResponse;
import com.axsystech.excelicare.data.responses.CityOrLocationDetailsResponse;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.CountryListResponse;
import com.axsystech.excelicare.data.responses.DeleteMediaItemResponse;
import com.axsystech.excelicare.data.responses.ForgotPasswordResponse;
import com.axsystech.excelicare.data.responses.GCMResponse;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.data.responses.GetFoldersListResponse;
import com.axsystech.excelicare.data.responses.GetMediaItemResponse;
import com.axsystech.excelicare.data.responses.GetRoleDataResponse;
import com.axsystech.excelicare.data.responses.HelpResponse;
import com.axsystech.excelicare.data.responses.InvitationReceivedDataResponse;
import com.axsystech.excelicare.data.responses.ListMediaItemThumbNailsResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.data.responses.MessageDetailsResponse;
import com.axsystech.excelicare.data.responses.MessageSendingResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.data.responses.PasswordRulesResponse;
import com.axsystech.excelicare.data.responses.PrivacyPolicyResponse;
import com.axsystech.excelicare.data.responses.ProxyListResponse;
import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.data.responses.RegistrationDetailsByUserResponse;
import com.axsystech.excelicare.data.responses.SaveActionResponse;
import com.axsystech.excelicare.data.responses.SaveMyCircleResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.data.responses.SignUpDataModel;
import com.axsystech.excelicare.data.responses.SignUpResponse;
import com.axsystech.excelicare.data.responses.SiteListResponse;
import com.axsystech.excelicare.data.responses.StateListResponse;
import com.axsystech.excelicare.data.responses.SummarySectionsResponse;
import com.axsystech.excelicare.data.responses.TimeSlotsResponse;
import com.axsystech.excelicare.data.responses.UploadMediaItemResponse;
import com.axsystech.excelicare.data.responses.UserSecurityQuestionsResponse;
import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.data.responses.ValidateSiteResponse;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.loaders.RemoveCircleResponse;
import com.axsystech.excelicare.network.exceptions.InternalServerException;
import com.axsystech.excelicare.network.exceptions.InvalidSiteUrlException;
import com.axsystech.excelicare.network.exceptions.TokenExpiredException;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.SharedPreferenceUtil;
import com.axsystech.excelicare.util.Trace;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

/**
 * Date: 03.10.13
 * Time: 14:48
 *
 * @author SomeswarReddy
 */
public class ServerMethods {

    private String TAG = ServerMethods.class.getSimpleName();


    public static ServerMethods mInstance = null;

    private ServerMethods() {
        // Exists only to defeat instantiation
    }

    public static ServerMethods getInstance() {
        if (mInstance == null) {
            mInstance = new ServerMethods();
        }

        return mInstance;
    }

    //*****************************URL's*********************************
    private String SITE_LIST_CLIENT_URL = "http://115.119.121.38:8989/ECService_PHRV64axsys_DEV/";

    // Which will be updated from the response we receive for 'getRegistrationDetailsByDevice'
    private String mClientUrl = "http://115.119.121.38:8989/ECService_PHRV64VL_DEV";

    private final String APP_URL = "http://115.119.121.38:8989/ECService_PHRV64Axsys_DEV/AxRSSecurity.svc/";

    private final String BASE_APP_SUPPORT_URL = "http://115.119.121.38:8989/ECService_PHRV64Axsys_DEV/AxRSAppSupport.svc/";

    private final String PROXY_URL = "http://115.119.121.38:8989/ECService_PHRV64VL_DEV/AxRSMyCircle.svc";

    private String SECURITY_QUSTIONS_BASE = "http://115.119.121.38:8989/ECService_PHRV64VL_DEV/";

    //*****************************SERVICES*********************************
    private final String SECURITY_SERVICE = "AxRSSecurity.svc/";
    private final String CIRCLES_SERVICE = "AxRSMyCircle.svc/";
    private final String MEDIA_SERVICE = "AxWSMediaService.svc/";
    private final String DASHBOARD_SERVICE = "AxRSTableView.svc/";
    private final String MSG_SERVICE = "AxRSMessenger.svc/";
    private final String APP_SUPPORT_SERVICE = "AxRSAppSupport.svc/";
    private final String APPONTMENT_MANAGER_SERVICE = "AxRSAppointmentManager.svc/";
    private final String PATIENT_DETAILS_SERVICE = "AxRSPatientDetails.svc/";

    //*****************************REQUEST URL's*********************************
    // REGISTRATION & LOGIN
    private final String GET_REGISTRATION_DETAILS_BY_DEVICE = APP_URL + "getRegistrationDetailsByDevice?DeviceID=%s&SiteID=%s&UserName=%s";

    private final String GET_REGISTRATION_DETAILS_BY_USER = APP_URL + "getRegistrationDetailsbyUser?userName=%s&DeviceId=%s&SiteId=%s&ReqSiteSelector=%s";
    private final String VALIDATE_SITE_CODE = APP_URL + "ValidateSiteCode?Email=%s&DeviceID=%s&SiteCode=%s";

    private final String GET_USER_SECURITY_QUESTIONS = SECURITY_QUSTIONS_BASE + SECURITY_SERVICE + "GetUserSecurityQuestions?LoginName=%s";

    private final String GET_SITE_LIST = SITE_LIST_CLIENT_URL + SECURITY_SERVICE + "getSiteList?UserId=%s";

    private final String GET_PRIVACY_POLICY_HTML = APP_URL + "GetPrivacyHTML";
    private final String GET_CITY_LIST = SECURITY_QUSTIONS_BASE + APP_SUPPORT_SERVICE + "GetDictionaryDetails?DictionaryId=%s&SearchString=%s&Token=%s";
    private final String GET_LOOKUP_DETAILS = BASE_APP_SUPPORT_URL + "GetLookupDetails?Token=%s&SystemLookups=%s&UserLookups=%s";
    private final String GET_STATES_LIST = BASE_APP_SUPPORT_URL + "GetLookupDetails?Token=%s&SystemLookups=%s&UserLookups=%s";
    private final String GET_COUNTRY_LIST = BASE_APP_SUPPORT_URL + "GetLookupDetails?Token=%s&SystemLookups=%s&UserLookups=%s";

    private final String REGISTER_NEW_USER_QUICK = APP_URL + "RegisterNewUser?";
    private final String REGISTER_NEW_USER_FULL = APP_URL + "RegisterNewUserFull";

    private final String GET_PASSWORD_RULES = APP_URL + "GetPasswordRules";
    private final String VERIFY_OTP_AND_REGISTER = APP_URL + "VerifyOTPandRegister?Deviceid=%s&token=%s&SiteID=%s&Password=%s";

    private final String LOGIN = SECURITY_SERVICE + "AuthenticateMobileUser?DeviceId=%s&UserName=%s&Password=%s";
    private final String GENERATE_OTP_TOKEN = SECURITY_SERVICE + "GenerateToken?userName=%s&SiteId=%s&DeviceId=%s";
    private final String FORGOT_PASSWORD = SECURITY_SERVICE + "forgotPassword?emailID=%s&SecurityQ1=%s&SecurityAns1=%s&SecurityQ2=%s&SecurityAns2=%s";

    //******************************************************************************

    // MENU LIST
    private final String GET_APP_MENU_LIST = SECURITY_SERVICE + "GetAppMenuList?TokenNo=%s&LMD=%s";
    private final String GET_DASHBOARD_CARD_MENU_LIST = SECURITY_SERVICE + "GetAppMenuList?TokenNo=%s&Menu=%s";
    private final String DELETE_PROFILE_PHOTO = PATIENT_DETAILS_SERVICE + "deletePatientPhoto?Token=%s&Pat_Id=%s";
    private final String UPLOAD_PROFILE_PHOTO = PATIENT_DETAILS_SERVICE + "SavePatientPhoto";

    //******************************************************************************

    //PROXY LIST
    private final String GET_PROXY_LIST_LOADER = CIRCLES_SERVICE + "listCaredMembers?Token=%s&&IncludeHistory=%s";

    //******************************************************************************

    // MEDIA
    private final String GET_FOLDER_LIST = MEDIA_SERVICE + "GetFolderList?Token=%s&UserId=%s";
    private final String LIST_MEDIA_ITEM_THUMBNAILS = MEDIA_SERVICE + "listMediaItemThumbnails?Criteria=%s&Token=%s&PatientId=%s&PageNo=%s&PageSize=%s";
    private final String SAVE_MEDIA_ITEM = MEDIA_SERVICE + "saveMediaItem?IsAndroid=1";
    private final String GET_MEDIA_ITEM = MEDIA_SERVICE + "getMediaItem?Id=%s&Token=%s";
    private final String DELETE_MEDIA_ITEM = MEDIA_SERVICE + "deleteMediaItem?Id=%s&token=%s";

    //******************************************************************************

    //DASHBOARD
    private final String GET_DASHBOARD_CARDS = DASHBOARD_SERVICE + "getDashboardCards?Token=%s&DashboardId=%s&IncludeDesign=%s";
    private final String CHANGE_ACCOUNT = SECURITY_SERVICE + "ChangeAccount?Token=%s&UserId=%s";

    //******************************************************************************

    // CIRCLES
    private final String LIST_MY_CIRCLES = CIRCLES_SERVICE + "ListMyCircles?Token=%s&IncludeHistory=%s&criteria=%s&PageIndex=%s&PageSize=%s";
//    private final String LIST_MY_CIRCLES = CIRCLES_SERVICE + "ListMyCircles?Token=%s&IncludeHistory=%s";
    //private final String LIST_MEMBERS_IN_CIRCLE = CIRCLES_SERVICE + "ListMembers?Token=%s&UserCircleID=%s&Criteria=%s";
    private final String LIST_MEMBERS_IN_CIRCLE = CIRCLES_SERVICE + "ListMembers?Token=%s&UserCircleID=%s&Criteria=%s&PageIndex=%s&PageSize=%s&Orderby=%s";
    private final String SAVE_MY_CIRCLE = CIRCLES_SERVICE + "saveMyCircle";
    private final String SEND_INVITE_TO_CIRCLE_MEMBER = CIRCLES_SERVICE + "sendInvite?Token=%s&Data=%s";
    //Circle Members Details
    public final String CIRCLE_MEMBERS_DETAILS = CIRCLES_SERVICE + "getMemberDetails?Token=%s&UserCircleID=%s&UserCircleMemberID=%s";
    //Circle Details
    public final String CIRCLE_DETAILS = CIRCLES_SERVICE + "getMyCircleDetails?Token=%s&CircleID=%s";
    //Circle Remove
    public final String CIRCLE_REMOVE = CIRCLES_SERVICE + "removeMyCircle?Token=%s&UserCircleID=%s&Reason=%s&Reason_SLU=%s";
    //Circle Member Remove
    public final String CIRCLE_MEMBER_REMOVE = CIRCLES_SERVICE + "removeMember?Token=%s&UserCircleMember_ID=%s&StopReason_SLU=%s&StopReasonDescription=%s";
    //Invite Remove
    public final String INVITE_REMOVE = CIRCLES_SERVICE + "RemoveInvite?Token=%s&InviteSent_ID=%s";
    //Circle Invitations Rceived
    //public final String CIRCLE_INVITATIONS_RECEIVED = CIRCLES_SERVICE + "ListInvitesReceived?Token=%s&LMD=%s&Filter=%s";
    //Circle Invitations Rceived
    public final String CIRCLE_INVITATIONS_RECEIVED = CIRCLES_SERVICE + "ListInvitesReceived?Token=%s&LMD=%s&Filter=%s&Criteria=%s&PageIndex=%s&PageSize=%s&ID=%s&orderby=%s";
    //Circle Invitations Sent
    //public final String CIRCLE_INVITATIONS_SENT = CIRCLES_SERVICE + "ListInvitationsSent?Token=%s&LMD=%s&Filter=%s";
    public final String CIRCLE_INVITATIONS_SENT = CIRCLES_SERVICE + "ListInvitationsSent?Token=%s&LMD=%s&Filter=%s&Criteria=%s&PageIndex=%s&PageSize=%s&orderby=%s";
    //Circle List Search
    public final String MYCIRCLE_SEARCH = CIRCLES_SERVICE + "ListMyCircles?Token=%s&IncludeHistory=0&criteria=%s";
    //Invitation Sent List Search
    public final String INVITATION_SENT_SEARCH = CIRCLES_SERVICE + "ListInvitationsSent?Token=%s&criteria=%s";
    //Invitation Received List Search
    public final String INVITATION_RECEIVED_SEARCH = CIRCLES_SERVICE + "ListInvitesReceived?Token=%s&criteria=%s";
    //Circle Invitations Accept
    public final String CIRCLE_INVITATIONS_ACCEPT = CIRCLES_SERVICE + "AcceptInvitation?Token=%s&InviteReceived_ID=%s";
    //Circle Invitations Accept
    public final String CIRCLE_INVITATIONS_DECLINE = CIRCLES_SERVICE + "declineInvitation?Token=%s&InviteReceived_ID=%s";
    //Get Role
    public final String GET_ROLE = CIRCLES_SERVICE + "GetLookupDetails?SysLookupNames=Role%20Type&Token=%s";
    //Change Role
    public final String MEMBER_CHANGE_ROLE = CIRCLES_SERVICE + "SaveRole?CircleManagerRole=%s&Token=%s&UserCircleId=%s&UserCircleMemberId=%s&status=%s";
    //Change Role
    public final String MEMBER_MANAGE_ROLE = CIRCLES_SERVICE + "SaveRole?Token=%s&UserCircleId=%s&UserCircleMemberId=%s&status=%s";
    //Circle User permissions
    public final String GET_CIRCLE_USER_PERMISSIONS = CIRCLES_SERVICE + "GetPermissions?Token=%s&UserCircleMember_ID=%s";
    //Save Circle User permissions
    public final String SAVE_CIRCLE_USER_PERMISSIONS = CIRCLES_SERVICE + "SavePermissions";
    //CircleListPagination
    public final String LIST_MY_CIRCLES_PAGINATION = CIRCLES_SERVICE + "ListMyCircles?Criteria=Circle&PageIndex=%s&PageSize=%s&Token=%s&IncludeHistory=%s";
    //CircleMemberListSearch
    public final String CIRCLE_MEMBER_LIST_SEARCH = CIRCLES_SERVICE + "ListMyCircles?Criteria=%s&PageIndex=%s&PageSize=%s&Token=%s&UserCircleID=%s";

    //Logout
    public final String LOGOUT = SECURITY_SERVICE + "LogOut?token=%s";
    //******************************************************************************

    // MESSAGING
    private final String MSG_LIST_INBOX_CONTENTS = DASHBOARD_SERVICE + "getListContents?Token=%s&PanelId=%s&PatientId=%s&Start=%s&Max=%s&IsDictionaryJSONResponse=%s";
    private final String MSG_LIST_SUMMARY_SECTIONS = DASHBOARD_SERVICE + "getListContents?FilterCriteria=%s&Max=%s&PanelId=%s&PatientId=%s&Start=%s&Token=%s&IsDictionaryJSONResponse=%s";
    private final String MSG_LIST_DETAILS_CONTENTS = MSG_SERVICE + "GetmessageDetails?Token=%s&MessageId=%s";
    //get Compose message details
    private final String MSG_COMPOSE_LIST_DETAILS_CONTENTS = CIRCLES_SERVICE + "ListMembers?Token=%s";
    private final String MSG_DELETE = MSG_SERVICE + "DeleteMessage?MessageId=%s&Token=%s";
    //Message Search Sent
    private final String MSG_SEARCH_SENT = DASHBOARD_SERVICE +
            "getListContents?Token=%s&PanelId=%s&PatientId=%s&Start=%s&Max=%s&IsDictionaryJSONResponse=%s&FilterCriteria=%s";
    //send messages
    private final String MSG_SENDING = MSG_SERVICE + "SendMessage";
    private final String MSG_SAVE_PRIORITY = MSG_SERVICE + "SavePriority?MessageId=%s&Priority_LU=%s&Token=%s";

    //City URL
    private final String GET_CITY_DETAILS = APP_SUPPORT_SERVICE + "GetDictionaryDetails?Token=%s&DictionaryId=%s&SearchString=%s";

    //******************************************************************************

    //APPOINTMENTS
    private final String SEARCH_DOCTORS_DETAILS = APPONTMENT_MANAGER_SERVICE + "SearchProviders?Token=%s&SearchCriteria=%s&PageNumber=%s&PageSize=%s&IsFavorite=%s";

    ///added by saketha
    private final String DOCTOR_SEARCH_RESULTS = APPONTMENT_MANAGER_SERVICE + "SearchProviders?Token=%s&SearchCriteria={%s}&PageNumber=%s&PageSize=%s";

    public final String GET_CLINICIAN_DETAILS = APPONTMENT_MANAGER_SERVICE + "GetClinicianDetails?ClinicianID=%s&Token=%s";
    public final String SAVE_FAVORITE_PROVIDER = APPONTMENT_MANAGER_SERVICE + "SaveFavouriteProvider?ClinicanID=%s&IsFavourite=%s&Token=%s";
    public final String GET_APPOINTMENTS_LIST = DASHBOARD_SERVICE + "getListContents?Token=%s&PanelId=%s&PatientId=%s&Start=%s&Max=%s&IsDictionaryJSONResponse=%s&FilterCriteria=%s";
    //public final String GET_PAST_APPOINTMENTS_LIST = DASHBOARD_SERVICE + "getListContents?Token=%s&PanelId=%s&PatientId=%s&Start=%s&Max=%s&IsDictionaryJSONResponse=%s&FilterCriteria=%s";
    public final String GET_APPOINTMENTS_LIST_PAGINATION = DASHBOARD_SERVICE + "getListContents?Token=%s&PanelId=%s&PatientId=%s&PageIndex=%s&PageSize=2&IsDictionaryJSONResponse=%s";
    public final String GET_PAST_APPOINTMENTS_LIST = DASHBOARD_SERVICE + "getListContents?Token=%s&PanelId=%s&PatientId=%s&PageIndex=%s&PageSize=%s&IsDictionaryJSONResponse=%s";

    public final String GET_TIMESLOTS_LIST = APPONTMENT_MANAGER_SERVICE + "GetTimeSlots?ClinicID=%s&ClinicanID=%s&StartDate=%s&Token=%s";
    public final String BOOK_APPOINTMENT = APPONTMENT_MANAGER_SERVICE + "BookAppointment";

    public final String APPOINTMENT_DETAILS = APPONTMENT_MANAGER_SERVICE + "GetAppointmentDetails?Token=%s&AppointmentID=%s&IsDictionaryJSONResponse=%s";
    //Cancel Booking
    public final String CANCEL_BOOKING = APPONTMENT_MANAGER_SERVICE + "CancelAppointment?Token=%s&AppointmentID=%s&TicketNumber=%s";

    //Help
    public final String HELP = SECURITY_SERVICE + "GetHelp?ModuleID=%s&Token=%s";

    //Settings
    public final String USER_PREFERENCES = SECURITY_SERVICE + "GetUserPreferences?Token=%s";
    public final String REMOVE_DEVICES = SECURITY_SERVICE + "unregisterDevice?deviceID=%s&UserDeviceId=%s&token=%s";

    //GCM
    public final String GCM_REGISTER = "http://115.119.121.38:8989/ECService_PHRV64VL_DEV/AxRSSecurity.svc/RegisterDevicePushNotification";
    //******************************************************************************


    private Gson getGson() {
        final GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.registerTypeAdapter(BaseResponse.class, new BaseResponseTypeAdapter());

        return gsonBuilder.create();
    }

    public String getGlobalClientUrl() {
        return SharedPreferenceUtil.getStringFromSP(AxSysTechApplication.getAppContext(), SharedPreferenceUtil.SP_CLIENT_URL_KEY, mClientUrl);
    }

    public void setGlobalClientUrl(String clientUrl) {
        SharedPreferenceUtil.saveStringInSP(AxSysTechApplication.getAppContext(), SharedPreferenceUtil.SP_CLIENT_URL_KEY, clientUrl);
    }

    public class BaseResponseTypeAdapter extends TypeAdapter<BaseResponse> {

        @Override
        public void write(JsonWriter out, BaseResponse baseResponseInstance) throws IOException {
            out.beginObject();
            out.name("Status").value(baseResponseInstance.getStatus());
            out.name("message").value(baseResponseInstance.getMessage());
            out.endObject();
        }

        @Override
        public BaseResponse read(JsonReader in) throws IOException {
            final BaseResponse baseResponseInstance = new BaseResponse();

            in.beginObject();
            while (in.hasNext()) {

                String jsonTag = in.nextName();

                if ("Status".equals(jsonTag) ||
                        "status".equals(jsonTag)) {
                    baseResponseInstance.setStatus(in.nextString());
                } else if ("message".equals(jsonTag)
                        || "reason".equals(jsonTag)
                        || "Reason".equals(jsonTag)) {
                    baseResponseInstance.setMessage(in.nextString());
                }
            }
            in.endObject();

            return baseResponseInstance;
        }
    }

    public RegisterResponse getRegistrationsDetailsByDevice(final String deviceId, final String siteId, final String userName) throws Exception {
        return executeGetRequest(String.format(GET_REGISTRATION_DETAILS_BY_DEVICE, deviceId, siteId, userName), RegisterResponse.class);
    }

    public RegistrationDetailsByUserResponse getRegistrationsDetailsByUser(String userName, final String deviceId, final String siteId, final String reqSiteSelector) throws Exception {
        return executeGetRequest(String.format(GET_REGISTRATION_DETAILS_BY_USER, userName, deviceId, siteId, reqSiteSelector), RegistrationDetailsByUserResponse.class);
    }

    public ValidateSiteResponse validateSiteCode(String emailId, String deviceId, String siteId) throws Exception {
        return executeGetRequest(String.format(VALIDATE_SITE_CODE, emailId, deviceId, siteId), ValidateSiteResponse.class);
    }

    public UserSecurityQuestionsResponse getUserSecurityQuestions(String loginName) throws Exception {
        return executeGetRequest(String.format(GET_USER_SECURITY_QUESTIONS, loginName), UserSecurityQuestionsResponse.class);
    }

    public SiteListResponse getSiteList(String userId) throws Exception {
        return executeGetRequest(String.format(GET_SITE_LIST, userId), SiteListResponse.class);
    }

    public PrivacyPolicyResponse getPrivacyPolicyHtml() throws Exception {
        return executeGetRequest(GET_PRIVACY_POLICY_HTML, PrivacyPolicyResponse.class);
    }

    public LookUpDetailsResponse getLookupDetails(String token, String systemLookups, String userLookups) throws Exception {
        return executeGetRequest(String.format(GET_LOOKUP_DETAILS, token, systemLookups, userLookups), LookUpDetailsResponse.class);
    }

    public CityListResponse getCityList(String dictionaryId, String searchString, String token) throws Exception {
        return executeGetRequest(String.format(GET_CITY_LIST, dictionaryId, searchString, token), CityListResponse.class);
    }

    public StateListResponse getStatesList(String token, String systemLookups, String userLookups) throws Exception {
        return executeGetRequest(String.format(GET_STATES_LIST, token, systemLookups, userLookups), StateListResponse.class);
    }

    public CountryListResponse getCountryList(String token, String systemLookups, String userLookups) throws Exception {
        return executeGetRequest(String.format(GET_COUNTRY_LIST, token, systemLookups, userLookups), CountryListResponse.class);
    }

    public SummarySectionsResponse getSummarySectionsList(String filterCriteria, int max, String panelId,
                                                          String patientId, int start, String token, String isDictionaryJSONResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_LIST_SUMMARY_SECTIONS, filterCriteria, max, panelId,
                patientId, start, token, isDictionaryJSONResponse), SummarySectionsResponse.class);
    }

    public Object registerNewUser(boolean isSignUpTypeFull, SignUpDataModel signUpDataModel,
                                  String deviceId, String isPrivacyAgreed,
                                  boolean shouldAddNewProxie, long ecUserId) throws Exception {
        if (!shouldAddNewProxie) {
            // This block will execute when user going to register new account - i.e  SignUp - FULL or QUICK
            if (isSignUpTypeFull) {
                return executePostRequest(REGISTER_NEW_USER_FULL, prepareSignUpPostData(isSignUpTypeFull, signUpDataModel, deviceId, isPrivacyAgreed, shouldAddNewProxie, ecUserId),
                        SignUpResponse.class);
            }

            return executeGetRequest((REGISTER_NEW_USER_QUICK + prepareSignUpGetUrl(isSignUpTypeFull, signUpDataModel, deviceId, isPrivacyAgreed, shouldAddNewProxie, ecUserId)),
                    SignUpResponse.class);
        } else {
            // This block will executed when user going to add new proxie
            if (isSignUpTypeFull) {
                return executePostRequest(REGISTER_NEW_USER_FULL, prepareSignUpPostData(isSignUpTypeFull, signUpDataModel, deviceId, isPrivacyAgreed, shouldAddNewProxie, ecUserId),
                        AddProxyResponse.class);
            }

            return executeGetRequest((REGISTER_NEW_USER_QUICK + prepareSignUpGetUrl(isSignUpTypeFull, signUpDataModel, deviceId, isPrivacyAgreed, shouldAddNewProxie, ecUserId)),
                    AddProxyResponse.class);
        }
    }

    public PasswordRulesResponse getPasswordRules() throws Exception {
        return executeGetRequest(GET_PASSWORD_RULES, PasswordRulesResponse.class);
    }

    public ChangePasswordResponse verifyOtpAndRegister(String deviceId, String token, String siteID, String password) throws Exception {
        return executeGetRequest(String.format(VERIFY_OTP_AND_REGISTER, deviceId, token, siteID, password), ChangePasswordResponse.class);
    }

    public LoginResponse authenticateUser(final String deviceId, final String userName, final String password) throws Exception {
        return executeGetRequestOnClientUrl(String.format(LOGIN, deviceId, userName, password), LoginResponse.class);
    }

    public BaseResponse generateNewOTP(String userName, String siteID, String deviceId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GENERATE_OTP_TOKEN, userName, siteID, deviceId), BaseResponse.class);
    }

    public ForgotPasswordResponse forgotPassword(String username, String securityQ1, String securityAns1, String securityQ2, String securityAns2) throws Exception {
        return executeGetRequestOnClientUrl(String.format(FORGOT_PASSWORD, username, securityQ1, securityAns1, securityQ2, securityAns2), ForgotPasswordResponse.class);
    }

    public AppMenuListResponse getAppMenuList(String token, String lmd) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_APP_MENU_LIST, token, lmd), AppMenuListResponse.class);
    }

    public AppMenuListResponse getCardMenuList(String token, String dashboardId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_DASHBOARD_CARD_MENU_LIST, token, dashboardId), AppMenuListResponse.class);
    }

    public BaseResponse deleteProfilePhoto(String token, String patientId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(DELETE_PROFILE_PHOTO, token, patientId), BaseResponse.class);
    }

    public ProxyListResponse getProxyList(String token, String lmd, String includeHistory) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_PROXY_LIST_LOADER, token, lmd, includeHistory), ProxyListResponse.class);
    }

    public GetFoldersListResponse getMediaFolderList(String token, String userId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_FOLDER_LIST, token, userId), GetFoldersListResponse.class);
    }

    public ListMediaItemThumbNailsResponse listMediaItemThumbnails(String criteria, String token, String patientId, int pageNo, int pageSize) throws Exception {
        return executeGetRequestOnClientUrl(String.format(LIST_MEDIA_ITEM_THUMBNAILS, criteria, token, patientId, pageNo, pageSize), ListMediaItemThumbNailsResponse.class);
    }

    public GetMediaItemResponse getMediaItem(Long id, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_MEDIA_ITEM, id, token), GetMediaItemResponse.class);
    }

    public LoginResponse changeAccount(String token, String userId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CHANGE_ACCOUNT, token, userId), LoginResponse.class);
    }

    public ListMyCirclesResponse listMyCircles(String token, String includeHistory,String criteria,String pageIndex,String pageSize) throws Exception {
        return executeGetRequestOnClientUrl(String.format(LIST_MY_CIRCLES, token, includeHistory,criteria,pageIndex,pageSize), ListMyCirclesResponse.class);
    }

    public ListMyCirclesResponse listMyCirclesPagination(int pageIndex,
                                                         String pageSize, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(LIST_MY_CIRCLES_PAGINATION, pageIndex, pageSize, token), ListMyCirclesResponse.class);
    }

//    public ListMembersInCircleResponse listMembersInCircle(String token, String userCircleId, String criteria) throws Exception {
//        return executeGetRequestOnClientUrl(String.format(LIST_MEMBERS_IN_CIRCLE, token, userCircleId, criteria), ListMembersInCircleResponse.class);
//    }

    public ListMembersInCircleResponse listMembersInCircle(String token, String userCircleId, String criteria,int pageIndex, int pageSize, String orderBy) throws Exception {
        return executeGetRequestOnClientUrl(String.format(LIST_MEMBERS_IN_CIRCLE, token, userCircleId, criteria, String.valueOf(pageIndex),String.valueOf(pageSize),orderBy), ListMembersInCircleResponse.class);
    }

    //Circle Member List Search
    public ListMembersInCircleResponse circleMembersListSearch(String criteria, int pageIndex, String pageSize, String token, String userCircleId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_MEMBER_LIST_SEARCH, criteria, pageIndex, pageSize, token, userCircleId), ListMembersInCircleResponse.class);
    }

    public SaveMyCircleResponse saveMyCircle(String requestBody) throws Exception {
        return executePostRequestOnClientUrl(SAVE_MY_CIRCLE, requestBody, SaveMyCircleResponse.class);
    }

    public String sendInviteToCircleMember(String token, String dataString) throws Exception {
        return executeGetRequestOnClientUrlAndReturnString(String.format(SEND_INVITE_TO_CIRCLE_MEMBER, token, dataString));
    }

    public UploadMediaItemResponse uploadMediaItem(MediaItemDbModel mediaItem, String token, String patientId) throws Exception {
        return executeMediaMultipartPostRequestOnClientUrl(SAVE_MEDIA_ITEM, mediaItem, token, patientId, UploadMediaItemResponse.class);

//        return executePostRequestOnClientUrl(SAVE_MEDIA_ITEM, prepareMediaItemRequestData(mediaItem, token, patientId), UploadMediaItemResponse.class);
    }

    public BaseResponse uploadProfilePic(String token, String patientId, Uri imageUri) throws Exception {
        return executePostRequestOnClientUrl(UPLOAD_PROFILE_PHOTO, prepareProfilePicRequestData(token, patientId, imageUri), BaseResponse.class);
    }

    public DeleteMediaItemResponse deleteMediaItem(long id, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(DELETE_MEDIA_ITEM, id, token), DeleteMediaItemResponse.class);
    }

    public GetDashBoardCardsResponse getDashboardCards(String token, String dashboardId, String includeDesign) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_DASHBOARD_CARDS, token, dashboardId, includeDesign), GetDashBoardCardsResponse.class);
    }

    //Messages inbox response
    public MessagesInboxAndSentResponse getMessageInboxListContent(String token, String panelId, String patientId,
                                                                   int start, int max, int isDictionaryJSONResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_LIST_INBOX_CONTENTS, token, panelId, patientId,
                start, max, isDictionaryJSONResponse), MessagesInboxAndSentResponse.class);
    }

    //Messages sent response
    public MessagesInboxAndSentResponse getMessageSentListContent(String Token, String PanelId, String PatientId, int Start, int Max, int IsDictionaryJSONResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_LIST_INBOX_CONTENTS, Token, PanelId, PatientId,
                Start, Max, IsDictionaryJSONResponse), MessagesInboxAndSentResponse.class);
    }

    //Messages sent search
    public MessagesInboxAndSentResponse getMessageSentSearchContent(String Token, String PanelId, String PatientId, int Start, int Max,
                                                                    int IsDictionaryJSONResponse, String FilterCriteria) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_SEARCH_SENT, Token, PanelId, PatientId, Start, Max, IsDictionaryJSONResponse, FilterCriteria), MessagesInboxAndSentResponse.class);
    }

    //Message details response
    public MessageDetailsResponse getMessagedetailsListContent(String Token, String MessageId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_LIST_DETAILS_CONTENTS, Token, MessageId), MessageDetailsResponse.class);
    }

    //Message delete response
    public BaseResponse getMessageDeleteResponse(String MessageId, String Token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_DELETE, MessageId, Token), BaseResponse.class);
    }

    //Message Compose response
    public ProxyListResponse getMessageComposeList(String Token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_COMPOSE_LIST_DETAILS_CONTENTS, Token), ProxyListResponse.class);
    }

    //Get Clinician Details
    public ClinicianDetailsResponse getClinicianDetails(String clinicianId, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_CLINICIAN_DETAILS, clinicianId, token), ClinicianDetailsResponse.class);
    }

    //For Sending Message
    public MessageSendingResponse sendMessage(String postData) throws Exception {
        return executePostRequestOnClientUrl(MSG_SENDING, postData, MessageSendingResponse.class);
    }

    public BaseResponse saveMessagePriority(String messageId, String priority_LU, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MSG_SAVE_PRIORITY, messageId, priority_LU, token), BaseResponse.class);
    }

    //Circle Members Details response
    public CircleMemberDetailDataResponse getCircleMembersDetailsContent(String Token, String userCircleId, String userCircleMemberId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_MEMBERS_DETAILS, Token, userCircleId, userCircleMemberId), CircleMemberDetailDataResponse.class);
    }

    //Circle Details response
    public CircleDetailsEditResponse getCircleDetailsContent(String Token, String userCircleId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_DETAILS, Token, userCircleId), CircleDetailsEditResponse.class);
    }

    //Save circle details
    public BaseResponse saveMyCircleDetails(String requestBody) throws Exception {
        return executePostRequestOnClientUrl(SAVE_MY_CIRCLE, requestBody, BaseResponse.class);
    }

    //Save circle permissions
    public BaseResponse saveCirclePermissions(String requestBody) throws Exception {
        return executePostRequestOnClientUrl(SAVE_CIRCLE_USER_PERMISSIONS, requestBody, BaseResponse.class);
    }

    //Circle Remove response
    public RemoveCircleResponse getCircleRemove(String Token, String userCircleId, String stopReasonDescription, String stopReasonSLU) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_REMOVE, Token, userCircleId, stopReasonDescription, stopReasonSLU), RemoveCircleResponse.class);
    }

    //Circle Remove member response
    public BaseResponse getCircleMemberRemove(String Token, String userCircleId, String stopReasonSLU, String stopReasonDescription) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_MEMBER_REMOVE, Token, userCircleId, stopReasonSLU, stopReasonDescription), BaseResponse.class);
    }

    //Circle Invitation Received
//    public InvitationReceivedDataResponse getCircleInvitaionReceivedContent(String Token, String LMD, String filter) throws Exception {
//        return executeGetRequestOnClientUrl(String.format(CIRCLE_INVITATIONS_RECEIVED, Token, LMD, filter), InvitationReceivedDataResponse.class);
//    }

    public InvitationReceivedDataResponse getCircleInvitaionReceivedContent(String Token, String LMD, String filter,String criteria, int pageIndex, int pageSize, String iD, String orderby) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_INVITATIONS_RECEIVED, Token, LMD, filter,criteria,String.valueOf(pageIndex),String.valueOf(pageSize),iD,orderby), InvitationReceivedDataResponse.class);
    }

    //Invite Remove response
    public BaseResponse getInviteRemove(String Token, String inviteSentId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(INVITE_REMOVE, Token, inviteSentId), BaseResponse.class);
    }

//    //Circle Invitation Sent
//    public InvitationReceivedDataResponse getCircleInvitaionSentContent(String Token, String LMD, String filter) throws Exception {
//        return executeGetRequestOnClientUrl(String.format(CIRCLE_INVITATIONS_SENT, Token, LMD, filter), InvitationReceivedDataResponse.class);
//    }

    //Circle Invitation Sent
    public InvitationReceivedDataResponse getCircleInvitaionSentContent(String Token, String LMD, String filter,String criteria, int pageIndex, int pageSize, String orderby) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_INVITATIONS_SENT, Token, LMD, filter,criteria,String.valueOf(pageIndex),String.valueOf(pageSize),orderby), InvitationReceivedDataResponse.class);
    }

    //Circle List Search response
    public ListMyCirclesResponse getCircleListSearchResponse(String Token, String criteria) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MYCIRCLE_SEARCH, Token, criteria), ListMyCirclesResponse.class);
    }

    //Invitation Sent Search response
    public InvitationReceivedDataResponse getInvitationSentSearchResponse(String Token, String criteria) throws Exception {
        return executeGetRequestOnClientUrl(String.format(INVITATION_SENT_SEARCH, Token, criteria), InvitationReceivedDataResponse.class);
    }

    //Invitation Received Search response
    public InvitationReceivedDataResponse getInvitationReceivedSearchResponse(String Token, String criteria) throws Exception {
        return executeGetRequestOnClientUrl(String.format(INVITATION_RECEIVED_SEARCH, Token, criteria), InvitationReceivedDataResponse.class);
    }

    //Invite Accept
    public BaseResponse getInviteAccept(String Token, String inviteReceivedId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_INVITATIONS_ACCEPT, Token, inviteReceivedId), BaseResponse.class);
    }

    //Invite Decline
    public BaseResponse getInviteDecline(String Token, String inviteReceivedId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CIRCLE_INVITATIONS_DECLINE, Token, inviteReceivedId), BaseResponse.class);
    }

    //Get Role
    public GetRoleDataResponse getRole(String Token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_ROLE, Token), GetRoleDataResponse.class);
    }

    //Change Member Role
    public BaseResponse getMemberChangeRole(String circleManagerRole, String Token, String userCircleId, String userCircleMemberId, String status) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MEMBER_CHANGE_ROLE, circleManagerRole, Token, userCircleId, userCircleMemberId, status), BaseResponse.class);
    }

    //Manage Member Role
    public BaseResponse getMemberManageRole(String Token, String userCircleId, String userCircleMemberId, String status) throws Exception {
        return executeGetRequestOnClientUrl(String.format(MEMBER_MANAGE_ROLE, Token, userCircleId, userCircleMemberId, status), BaseResponse.class);
    }

    //Circle User Permissions
    public CircleUserPermissionsDataResponse getUserPermissions(String Token, String userCircleMemberId) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_CIRCLE_USER_PERMISSIONS, Token, userCircleMemberId), CircleUserPermissionsDataResponse.class);
    }

    //For City Details
    public CityOrLocationDetailsResponse getCityOrLocationsList(String token, String dictionaryID, String searchString) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_CITY_DETAILS, token, dictionaryID, searchString), CityOrLocationDetailsResponse.class);
    }

    //Invite Remove response
    public BaseResponse getRemoveDevice(String deviceID, String userDeviceId, String Token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(REMOVE_DEVICES, deviceID, userDeviceId, Token), BaseResponse.class);
    }

    public SearchDoctorsResponse getDoctorsList(String token, String searchCriteria, String pageNumber, String pageSize, String isFavorite) throws Exception {
        return executeGetRequestOnClientUrl(String.format(SEARCH_DOCTORS_DETAILS, token, searchCriteria, pageNumber, pageSize , isFavorite), SearchDoctorsResponse.class);
    }

    // Added by saketha
    public SearchDoctorsResponse getDoctorResultSearch(String token, String searchCriteria, String pageNumber, String pageSize) throws Exception {
        return executeGetRequestOnClientUrl(String.format(DOCTOR_SEARCH_RESULTS, token, searchCriteria, pageNumber, pageSize ), SearchDoctorsResponse.class);
    }

    public BaseResponse saveFavoriteProvider(String clinicianId, String isFavorite, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(SAVE_FAVORITE_PROVIDER, clinicianId, isFavorite, token), BaseResponse.class);
    }

    public AppointmentListResponse getAppointmentsList(String token, String panelId, String patientId, String start, String max,
                                                       String isDictonaryResponse, String filterCriteria) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_APPOINTMENTS_LIST, token, panelId, patientId, start, max, isDictonaryResponse, filterCriteria), AppointmentListResponse.class);
    }

    //Past Appointments List
    public AppointmentListResponse getPastAppointmentsList(String token, String panelId, String patientId, int pageCount, String pageSize,
                                                           String isDictonaryResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_PAST_APPOINTMENTS_LIST, token, panelId, patientId, pageCount, pageSize, isDictonaryResponse), AppointmentListResponse.class);
    }

    //Cancelled Appointments List
    public AppointmentListResponse getCancelledAppointmentsList(String token, String panelId, String patientId, int pageCount, String pageSize,
                                                                String isDictonaryResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_PAST_APPOINTMENTS_LIST, token, panelId, patientId, pageCount, pageSize, isDictonaryResponse), AppointmentListResponse.class);
    }

    public AppointmentListResponse getAppointmentsListPagination(String token, String panelId, String patientId, int pageIndex,
                                                                 String isDictonaryResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_APPOINTMENTS_LIST_PAGINATION, token, panelId, patientId, pageIndex, isDictonaryResponse), AppointmentListResponse.class);
    }

    //Cancel Booking
    public BaseResponse cancelBooking(String token, String appointmentId, String ticketNumber) throws Exception {
        return executeGetRequestOnClientUrl(String.format(CANCEL_BOOKING, token, appointmentId, ticketNumber), BaseResponse.class);
    }

    //Help
    public HelpResponse getHelpInfo(String moduleId, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(HELP, moduleId, token), HelpResponse.class);
    }

    //Logout
    public BaseResponse getLogoutresponse(String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(LOGOUT, token), BaseResponse.class);
    }

    public String getUserPreferences(String token) throws Exception {
        return executeGetRequestOnClientUrlAndReturnString(String.format(USER_PREFERENCES, token));
    }

    public TimeSlotsResponse getTimeSlots(String clinicID, String clinicanID, String startDate, String token) throws Exception {
        return executeGetRequestOnClientUrl(String.format(GET_TIMESLOTS_LIST, clinicID, clinicanID, startDate, token), TimeSlotsResponse.class);
    }

    public BookAppointmentResponse bookAppointment(String postData) throws Exception {
        return executePostRequestOnClientUrl(BOOK_APPOINTMENT, postData, BookAppointmentResponse.class);
    }

    public AppointmentDetailsResponse getAppointmentDetails(String token, String appointmentID, String isDictionaryJSONResponse) throws Exception {
        return executeGetRequestOnClientUrl(String.format(APPOINTMENT_DETAILS, token, appointmentID, isDictionaryJSONResponse), AppointmentDetailsResponse.class);
    }

    public GCMResponse postGcmRegistration(String postData) throws Exception {
        return executePostRequestOnClientUrlGcm(GCM_REGISTER, postData, GCMResponse.class);
    }


    public CardServiceDataResponse getSingleCardServiceData(String serviceUrl) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        String clientUrl = getGlobalClientUrl();

        if (!clientUrl.endsWith("/")) {
            clientUrl = clientUrl + "/";
        }

        if (serviceUrl.startsWith("/")) {
            serviceUrl = serviceUrl.substring(1);
        }

        return executeGetRequest(clientUrl + serviceUrl, CardServiceDataResponse.class);
    }

    public String getSingleCardContentData(String serviceUrl) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        String clientUrl = getGlobalClientUrl();

        if (!clientUrl.endsWith("/")) {
            clientUrl = clientUrl + "/";
        }

        if (serviceUrl.startsWith("/")) {
            serviceUrl = serviceUrl.substring(1);
        }

        return executeGetRequestAndReturnStringResponse(clientUrl + serviceUrl);
    }

    public SaveActionResponse saveCardDetails(String saveUrl, String token, String returnFormat, String strParamData) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        String clientUrl = getGlobalClientUrl();
        if (!clientUrl.endsWith("/")) {
            clientUrl = clientUrl + "/";
        }

        if (saveUrl.startsWith("/")) {
            saveUrl = saveUrl.substring(1);
        }

        return executePostRequest(clientUrl + saveUrl, prepareSaveCardDetailsPostData(token, returnFormat, strParamData), SaveActionResponse.class);
    }

    private String prepareSaveCardDetailsPostData(String token, String returnFormat, String strParamData) {
        /**
         {
         "Token" : "e8f8c742-7a96-4d78-8fe9-f016a79b3bc5",
         "ReturnFormat" : "1",
         "strParamData" : "{\"SPFDD\":{\"Header\":{\"User\":{\"Id\":1153},\"Patient\":{\"Id\":1101},\"ctrlDispNames\":null},\"Body\":[{\"Form\":[{\"id\":7000009,\"LMD\":\"\",\"fireevents\":\"\",\"Control\":{\"cboSource\":\"527269\",\"cboADRAllergyReactionSeverity\":\"573710\",\"cboType\":\"553569\",\"txtAllergyDetails\":\"d\",\"rtbReactions\":\"Tew2\",\"txtNewAllergy\":\"New\"},\"newdataid\":-1,\"LMUID\":0,\"Sig1_userid\":\"0\",\"criteria\":\"\",\"RecordCount\":8,\"dataid\":2083,\"formlibid\":7000054,\"transaction\":5,\"versionid\":\"\",\"RecordPosition\":2,\"navigation\":\"\"}]}],\"Footer\":{\"Reason\":\"success\",\"Status\":\"success\"}}}"
         }
         */

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Token", token);
            jsonObject.put("ReturnFormat", returnFormat);
            jsonObject.put("strParamData", strParamData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public BaseResponse deleteCardDetails(String deleteUrl, String message) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        String clientUrl = getGlobalClientUrl();

        if (!clientUrl.endsWith("/")) {
            clientUrl = clientUrl + "/";
        }

        if (deleteUrl.startsWith("/")) {
            deleteUrl = deleteUrl.substring(1);
        }

        return executeGetRequest(clientUrl + deleteUrl + message, BaseResponse.class);
    }

    //****************************************

    private <T> T executeGetRequestOnClientUrl(final String url, final Class<T> classOfT) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        if (!getGlobalClientUrl().endsWith("/")) {
            setGlobalClientUrl(getGlobalClientUrl() + "/");
        }

        return executeGetRequest(getGlobalClientUrl().concat(url), classOfT);
    }

    //Gcm Service
    private <T> T executePostRequestOnClientUrlGcm(final String url, final String postData, final Class<T> classOfT) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        if (!getGlobalClientUrl().endsWith("/")) {
            setGlobalClientUrl(getGlobalClientUrl() + "/");
        }
        String serverUrl;
        if (url.equalsIgnoreCase(GCM_REGISTER)) {
            serverUrl = url;
        } else {
            serverUrl = getGlobalClientUrl().concat(url);
        }
        return executePostRequest(serverUrl, postData, classOfT);
    }

    private String executeGetRequestOnClientUrlAndReturnString(final String url) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        if (!getGlobalClientUrl().endsWith("/")) {
            setGlobalClientUrl(getGlobalClientUrl() + "/");
        }

        return executeGetRequestAndReturnStringResponse(getGlobalClientUrl().concat(url));
    }

    private <T> T executeGetRequest(final String url, final Class<T> classOfT) throws Exception {
        final HttpGet httpGet = new HttpGet();
        Trace.e("URL: ", " " + url);
        httpGet.setURI(URI.create(url));
        String result = AxSysTechHttpClient.executeMethod(httpGet);
//        result = result.replaceAll("\\\\", "");

        Trace.e("Result JSON:", " " + result);
        if (result.startsWith("\"") && result.length() > 1) {
            result = result.substring(1, result.length());
        }
        Trace.e("Result JSON: (1)", " " + result);
        if (result.endsWith("\"") && result.length() > 1) {
            result = result.substring(0, result.length() - 1);
        }
        Trace.e("Result JSON: (2)", " " + result);
        Trace.e("Result JSON LENGTH:", "=" + result.length());
        try {
            return exceptionHandler(getGson().fromJson(result, classOfT));
        } catch (JsonSyntaxException e) {
            throw new InternalServerException(AxSysConstants.INVALID_SERVER_RESPONSE);
        }
    }

    private <T> T executePostRequestOnClientUrl(final String url, final String postData, final Class<T> classOfT) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        if (!getGlobalClientUrl().endsWith("/")) {
            setGlobalClientUrl(getGlobalClientUrl() + "/");
        }

        return executePostRequest(getGlobalClientUrl().concat(url), postData, classOfT);
    }

    private <T> T executePostRequest(final String url, final String postData, final Class<T> classOfT) throws Exception {
        HttpPost httpPost = new HttpPost();
        Trace.e("URL: ", " " + url);
        Trace.e("POST DATA: ", " " + postData);

        httpPost.setURI(URI.create(url));
        httpPost.setEntity(new StringEntity(postData, "UTF-8"));
        String result = AxSysTechHttpClient.executeMethod(httpPost);

//        result = result.replaceAll("\\\\", "");

        Trace.e("Result JSON:", " " + result);
        if (result.startsWith("\"") && result.length() > 1) {
            result = result.substring(1, result.length());
        }
        Trace.e("Result JSON: (1)", " " + result);
        if (result.endsWith("\"") && result.length() > 1) {
            result = result.substring(0, result.length() - 1);
        }
        Trace.e("Result JSON: (2)", " " + result);
        Trace.e("Result JSON LENGTH:", "=" + result.length());
        try {
            return exceptionHandler(getGson().fromJson(result, classOfT));
        } catch (JsonSyntaxException e) {
            throw new InternalServerException(AxSysConstants.INVALID_SERVER_RESPONSE);
        }
    }

    private String executeGetRequestAndReturnStringResponse(final String url) throws Exception {
        HttpClient client = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet();

        Trace.e("URL: ", " " + url);

        httpGet.setURI(URI.create(url));

        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        String result = client.execute(httpGet, responseHandler);

//        result = result.replaceAll("\\\\", "");

        Trace.e("Result JSON:", " " + result);
        if (result.startsWith("\"") && result.length() > 1) {
            result = result.substring(1, result.length());
        }
        Trace.e("Result JSON: (1)", " " + result);
        if (result.endsWith("\"") && result.length() > 1) {
            result = result.substring(0, result.length() - 1);
        }
        Trace.e("Result JSON: (2)", " " + result);
        Trace.e("Result JSON LENGTH:", "=" + result.length());
        try {
            return result;
        } catch (Exception e) {
            throw new InternalServerException(AxSysConstants.INVALID_SERVER_RESPONSE);
        }
    }

    private <T> T exceptionHandler(final T classOfT) throws Exception {
        if (classOfT instanceof BaseResponse) {
            final BaseResponse baseResponse = (BaseResponse) classOfT;
            if (!baseResponse.isSuccess()) {
                if (baseResponse.getMessage().startsWith("Token expired")) {
                    throw new TokenExpiredException();
                } else {
                    if (!TextUtils.isEmpty(baseResponse.getMessage())) {
                        throw new InternalServerException(baseResponse.getMessage());
                    } else if (!TextUtils.isEmpty(baseResponse.getMessageCaps())) {
                        throw new InternalServerException(baseResponse.getMessageCaps());
                    } else if (!TextUtils.isEmpty(baseResponse.getReason())) {
                        throw new InternalServerException(baseResponse.getReason());
                    }
                }
            }
        }

        return classOfT;
    }

    private String prepareSignUpGetUrl(boolean isSignUpTypeFull, SignUpDataModel signUpData, String deviceId, String isPrivacyAgreed,
                                       boolean shouldAddNewProxie, long ecUserId) {
        StringBuffer buffer = new StringBuffer();

        if (signUpData != null) {
            // IF SignUp Type QUICK
            if (!isSignUpTypeFull) {
                if (!TextUtils.isEmpty(signUpData.getFirstName())) {
                    buffer.append("SurName=" + signUpData.getLastName());
                }
                if (!TextUtils.isEmpty(signUpData.getLastName())) {
                    buffer.append("&");
                    buffer.append("Forename=" + signUpData.getFirstName());
                }
                if (!TextUtils.isEmpty(signUpData.getGender() + "")) {
                    buffer.append("&");
                    buffer.append("Sex=" + signUpData.getGender());
                }
                if (!TextUtils.isEmpty(signUpData.getDob())) {
                    buffer.append("&");
                    buffer.append("Dob=" + signUpData.getDob());
                }
                if (!TextUtils.isEmpty(signUpData.getMobileNo())) {
                    buffer.append("&");
                    buffer.append("Mobileno=" + signUpData.getMobileNo());
                }

                buffer.append(constructCommonSigupUrl(signUpData, deviceId, isPrivacyAgreed, shouldAddNewProxie, ecUserId));

            }
        }

        return buffer.toString();
    }

    private StringBuffer constructCommonSigupUrl(SignUpDataModel signUpData, String deviceId, String isPrivacyAgreed,
                                                 boolean shouldAddNewProxie, long ecUserId) {
        StringBuffer buffer = new StringBuffer();

        if (!TextUtils.isEmpty(signUpData.getLoginName())) {
            buffer.append("&");
            buffer.append("LoginName=" + signUpData.getLoginName());
        }
        if (!TextUtils.isEmpty(deviceId)) {
            buffer.append("&");
            buffer.append("Deviceid=" + deviceId);
        }
        if (!TextUtils.isEmpty(signUpData.getSiteId())) {
            buffer.append("&");
            buffer.append("Siteid=" + signUpData.getSiteId());
        }
        if (!TextUtils.isEmpty(signUpData.getSecurityQues1())) {
            buffer.append("&");
            buffer.append("SecurityQ1=" + signUpData.getSecurityQues1());
        }
        if (!TextUtils.isEmpty(signUpData.getSecurityQues1Answer())) {
            buffer.append("&");
            buffer.append("SecurityAns1=" + signUpData.getSecurityQues1Answer());
        }
        if (!TextUtils.isEmpty(signUpData.getSecurityQues2())) {
            buffer.append("&");
            buffer.append("SecurityQ2=" + signUpData.getSecurityQues2());
        }
        if (!TextUtils.isEmpty(signUpData.getSecurityQues2Answer())) {
            buffer.append("&");
            buffer.append("SecurityAns2=" + signUpData.getSecurityQues2Answer());
        }
        if (!TextUtils.isEmpty(isPrivacyAgreed)) {
            buffer.append("&");
            buffer.append("IsprivacyAgreed=" + isPrivacyAgreed);
        }
        if (shouldAddNewProxie) {
            if (!TextUtils.isEmpty(ecUserId + "")) {
                buffer.append("&");
                buffer.append("ProxyUser_Id=" + ecUserId);
            }
        } else {
            buffer.append("&");
            buffer.append("ProxyUser_Id=" + "");
        }

        return buffer;
    }

    private String prepareSignUpPostData(boolean isSignUpTypeFull, SignUpDataModel signUpData, String deviceId, String isPrivacyAgreed,
                                         boolean shouldAddNewProxie, long ecUserId) {
        JSONObject postDataObj = new JSONObject();

        if (signUpData != null) {
            // IF SignUp Type FULL
            if (isSignUpTypeFull) {
                try {
                    postDataObj.put("Siteid", !TextUtils.isEmpty(signUpData.getSiteId()) ? signUpData.getSiteId() : "");
                    postDataObj.put("SecurityQ1", !TextUtils.isEmpty(signUpData.getSecurityQues1()) ? signUpData.getSecurityQues1() : "");
                    postDataObj.put("SecurityQ2", !TextUtils.isEmpty(signUpData.getSecurityQues2()) ? signUpData.getSecurityQues2() : "");
                    postDataObj.put("LoginName", !TextUtils.isEmpty(signUpData.getLoginName()) ? signUpData.getLoginName() : "");
                    postDataObj.put("SecurityAns1", !TextUtils.isEmpty(signUpData.getSecurityQues1Answer()) ? signUpData.getSecurityQues1Answer() : "");
                    postDataObj.put("SecurityAns2", !TextUtils.isEmpty(signUpData.getSecurityQues2Answer()) ? signUpData.getSecurityQues2Answer() : "");
                    postDataObj.put("Deviceid", !TextUtils.isEmpty(deviceId) ? deviceId : "");

                    // construct 'paramData'
                    JSONObject paramDataObj = new JSONObject();

                    JSONObject patientDetailsObj = new JSONObject();
                    patientDetailsObj.put("Surname", !TextUtils.isEmpty(signUpData.getLastName()) ? signUpData.getLastName() : "");
                    patientDetailsObj.put("Forename", !TextUtils.isEmpty(signUpData.getFirstName()) ? signUpData.getFirstName() : "");
                    patientDetailsObj.put("Sex", !TextUtils.isEmpty(signUpData.getGender() + "") ? signUpData.getGender() : "");
                    patientDetailsObj.put("DOB", !TextUtils.isEmpty(signUpData.getDob()) ? signUpData.getDob() : "");
                    patientDetailsObj.put("MobileNo", !TextUtils.isEmpty(signUpData.getMobileNo()) ? signUpData.getMobileNo() : "");
                    paramDataObj.put("PatientDetails", patientDetailsObj);

                    JSONObject addressDetailsObj = new JSONObject();
                    addressDetailsObj.put("Pincode", !TextUtils.isEmpty(signUpData.getPincode()) ? signUpData.getPincode() : "");
                    addressDetailsObj.put("State", !TextUtils.isEmpty(signUpData.getState()) ? signUpData.getState() : "");
                    addressDetailsObj.put("Address1", !TextUtils.isEmpty(signUpData.getAddress1()) ? signUpData.getAddress1() : "");
                    addressDetailsObj.put("Address2", !TextUtils.isEmpty(signUpData.getAddress2()) ? signUpData.getAddress2() : "");
                    addressDetailsObj.put("Address3", !TextUtils.isEmpty(signUpData.getAddress3()) ? signUpData.getAddress3() : "");
                    addressDetailsObj.put("Country", !TextUtils.isEmpty(signUpData.getCountry()) ? signUpData.getCountry() : "");
                    addressDetailsObj.put("City", !TextUtils.isEmpty(signUpData.getCity()) ? signUpData.getCity() : "");
                    paramDataObj.put("AddressDetails", addressDetailsObj);

                    JSONArray identifierDetailsArray = new JSONArray();
                    JSONObject identifier1Obj = new JSONObject();
                    identifier1Obj.put("ID", AxSysConstants.AADHAR_NUMBER_ID);
                    identifier1Obj.put("Value", !TextUtils.isEmpty(signUpData.getAadharCardNumber()) ? (signUpData.getAadharCardNumber()) : "");
                    JSONObject identifier2Obj = new JSONObject();
                    identifier2Obj.put("ID", AxSysConstants.DRIVING_LICENCE_NUMBER_ID);
                    identifier2Obj.put("Value", !TextUtils.isEmpty(signUpData.getDrivingLicenseNumber()) ? (signUpData.getDrivingLicenseNumber()) : "");
                    identifierDetailsArray.put(identifier1Obj);
                    identifierDetailsArray.put(identifier2Obj);
                    paramDataObj.put("IdentifierDetails", identifierDetailsArray);

                    postDataObj.put("paramData", paramDataObj.toString());

                    postDataObj.put("IsprivacyAgreed", !TextUtils.isEmpty(isPrivacyAgreed) ? isPrivacyAgreed : "");
                    if (shouldAddNewProxie) {
                        postDataObj.put("ProxyUser_Id", ecUserId);
                    } else {
                        postDataObj.put("ProxyUser_Id", "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return postDataObj.toString();
    }

    private String prepareMediaItemRequestData(MediaItemDbModel mediaItem, String token, String patientId) {
        JSONObject mainJsonObj = new JSONObject();

        if (mediaItem != null) {

            try {
                JSONObject mediaItemObject = new JSONObject();

                mediaItemObject.put("id", mediaItem.getMediaId() + "");
                mediaItemObject.put("name", mediaItem.getMediaName());
                mediaItemObject.put("createdDate", CommonUtils.getMediaServerDateFormat(mediaItem.getCreatedDate()));
                mediaItemObject.put("folderId", mediaItem.getFolderId() + "");
                mediaItemObject.put("subfolderId", mediaItem.getSubFolderId() + "");
                mediaItemObject.put("comments", mediaItem.getComments());

//                Bitmap bitmap = BitmapFactory.decodeFile(mediaItem.getPreviewMedia());
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
//                byte [] byte_arr = stream.toByteArray();
//                String image_str = Base64.encodeToString(byte_arr, Base64.DEFAULT);

                InputStream inputStream = new FileInputStream(mediaItem.getPreviewMedia());//You can get an inputStream using any IO API
                byte[] buffer = new byte[1024];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String base64Image = Base64.encodeToString(output.toByteArray(), Base64.DEFAULT);

                mediaItemObject.put("originalMedia", base64Image);
                mediaItemObject.put("previewMedia", base64Image);
                mediaItemObject.put("annotationMedia", "");

                // Prepare main json object that needs to be sent to server
                mainJsonObj.put("PatientId", patientId);
                mainJsonObj.put("Token", token);
                mainJsonObj.put("mediaItem", mediaItemObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (OutOfMemoryError ome) {
                ome.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return mainJsonObj.toString();
    }

    private String prepareProfilePicRequestData(String token, String patientId, Uri imageUri) {
        JSONObject mainJsonObj = new JSONObject();

        if (imageUri != null && !TextUtils.isEmpty(imageUri.getPath())) {
            try {
                JSONObject strPatientPhoto = new JSONObject();

                strPatientPhoto.put("PatientID", patientId);

                // get base64 image data
                String imagePath = imageUri.getPath();
                InputStream inputStream = new FileInputStream(imagePath);//You can get an inputStream using any IO API
                byte[] buffer = new byte[1024];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String base64Image = Base64.encodeToString(output.toByteArray(), Base64.DEFAULT);
                strPatientPhoto.put("Photo", base64Image);

                // get image file format
                String fileFormat = imagePath.substring(imagePath.lastIndexOf(".") + 1);
                strPatientPhoto.put("FileFormat", "image/" + fileFormat);


                // Prepare main json object that needs to be sent to server
                mainJsonObj.put("Token", token);
                mainJsonObj.put("strPatientPhoto", strPatientPhoto.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (OutOfMemoryError ome) {
                ome.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
                ;
            }
        }

        return mainJsonObj.toString();
    }

    private <T> T executeMediaMultipartPostRequestOnClientUrl(final String url, MediaItemDbModel mediaItem, String token, String patientId,
                                                              final Class<T> classOfT) throws Exception {
        if (TextUtils.isEmpty(getGlobalClientUrl())) {
            throw new InvalidSiteUrlException();
        }

        if (!getGlobalClientUrl().endsWith("/")) {
            setGlobalClientUrl(getGlobalClientUrl() + "/");
        }

        try {
            Trace.e(TAG, "URL:" + getGlobalClientUrl().concat(url));

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(getGlobalClientUrl().concat(url));

            MultipartEntity requestEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            requestEntity.addPart("PatientId", new StringBody(patientId));
            requestEntity.addPart("Token", new StringBody(token));

            JSONObject mediaItemObject = new JSONObject();
            mediaItemObject.put("id", mediaItem.getMediaId() + "");
            mediaItemObject.put("name", mediaItem.getMediaName());
            mediaItemObject.put("createdDate", CommonUtils.getMediaServerDateFormat(mediaItem.getCreatedDate()));
            mediaItemObject.put("folderId", mediaItem.getFolderId() + "");
            mediaItemObject.put("subfolderId", mediaItem.getSubFolderId() + "");
            mediaItemObject.put("comments", mediaItem.getComments());

            requestEntity.addPart("mediaItem", new StringBody(mediaItemObject.toString()));

            requestEntity.addPart("originalMedia", new FileBody(new File(mediaItem.getPreviewMedia())));
            requestEntity.addPart("previewMedia", new StringBody(""));
            requestEntity.addPart("annotationMedia", new StringBody(""));

            httpPost.setEntity(requestEntity);

            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();

            String result = "";

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();

                result = sb.toString();
                Trace.d(TAG, "MediaUpload:response:" + result);
            } catch (Exception ex) {
                Trace.e(TAG, "Error converting result " + ex.toString());
            }

            try {
                return exceptionHandler(getGson().fromJson(result, classOfT));
            } catch (JsonSyntaxException ex) {
                throw new InternalServerException(AxSysConstants.INVALID_SERVER_RESPONSE);
            }
        } catch (Exception ex) {
            throw new InternalServerException(AxSysConstants.INVALID_SERVER_RESPONSE);
        }
    }


}
