//
//  CKBundle.m
//  Comics
//
//  Created by Derek Stutsman on 7/8/14.
//  Copyright (c) 2014 iVerse Media. All rights reserved.
//

#include <CommonCrypto/CommonDigest.h>
#import "CKBundle.h"
#import "CKBundleDefs.h"
#import "CKStoreProduct.h"
#import "CKBundlePageInfo.h"
#import "NSFileManager+PathAdditions.h"
#import "UIDevice+PersistentID.h"
#import "CKImageManager.h"
#import "CKServer.h"
#import "CKLog.h"
#import "CKImageManager.h"
#import <ImageIO/ImageIO.h>

@interface CKBundle()
@property (strong, nonatomic) NSString *filePath;
@property (strong, nonatomic) NSData *expectedHash;
@property (assign, nonatomic) short bundleVersion;
@property (assign, nonatomic) short expectedHorizontalCount;
@property (assign, nonatomic) short expectedVerticalCount;
@property (assign, nonatomic) short expectedTotalCount;
@property (assign, nonatomic) NSUInteger checksumDataSize;
@end

@implementation CKBundle

#pragma mark - Init/Dealloc
- (instancetype) initWithFileAtPath:(NSString*)path
{
    self = [super init];
    if (self != nil)
    {
        //Save off the file path
        self.filePath = path;
        
        //Parse the file
        [self parseBundleData];
    }
    return self;
}

- (instancetype)initWithProductPreview:(CKStoreProduct*)aProduct
{
    self = [super init];
    if (self != nil)
    {
        //Set all the properties about it
        self.type = CKBundleTypePreview;
        self.productID = aProduct.productID;
        self.synopsis = aProduct.synopsis;
        self.name = [NSString stringWithFormat:@"%@ (%@)", aProduct.name, NSLocalizedString(@"Preview", @"Indicator on preview display, follows book title eg Book Name (Preview)")];
        self.productType = aProduct.productType;
        self.parentProductID = aProduct.seriesBundleProductID;
        self.productDescription = aProduct.productDescription;
        self.publisherID = aProduct.supplierID;
        self.publisherName = aProduct.supplierName;
        self.copyright = aProduct.copyrightText;
        self.type = CKBundleTypePreview;
        self.writers = [aProduct.writers copy];
        self.artists = [aProduct.artists copy];
        if (aProduct.mangaOrientation)
        {
            self.pagination = CKBundlePaginationManga;
        }
        
        //Store the pages
        NSMutableArray *pageList = [[NSMutableArray alloc] init];
        for (int i=0; i < aProduct.previewPages; i++)
        {
            NSURL *pageURL = [[CKServer sharedInstance] productPreviewURL:aProduct index:i];
            CKBundlePageInfo *page = [[CKBundlePageInfo alloc] initWithRemoteImageURL:pageURL orientation:CKBundlePageOrientationPortrait];
            [pageList addObject:page];
        }
        self.pages = pageList;
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self != nil)
    {
        //Set the metadata
        NSDictionary *metadata  = dictionary[@"metadata"];
        self.name = metadata[@"name"];
        self.productID = metadata[@"product_id"];
        self.parentProductID = metadata[@"series_product_id"];
        self.synopsis = metadata[@"synopsis"];
        self.productType = metadata[@"product_type"];
        self.productDescription = metadata[@"description"];
        self.publisherID = metadata[@"publisher_id"];
        self.publisherName = metadata[@"publisher_name"];
        self.type = CKBundleTypeRemote;
        if ([metadata[@"manga_orientation"] boolValue])
        {
            self.pagination = CKBundlePaginationManga;
        }
        if ([metadata[@"horizontal_fades"] boolValue])
        {
            self.transition = CKBundleTransitionFade;
            self.presentation = CKBundlePresentationHorizontal;
        }

        //Store the pages
        NSMutableArray *pageList = [[NSMutableArray alloc] init];
        for (NSString *pageURLString in dictionary[@"pages"])
        {
            NSURL *pageURL = [NSURL URLWithString:pageURLString];
            CKBundlePageInfo *page = [[CKBundlePageInfo alloc] initWithRemoteImageURL:pageURL orientation:CKBundlePageOrientationPortrait];
            [pageList addObject:page];
        }
        self.pages = pageList;
    }
    return self;
}

- (void)dealloc
{
    DebugLog(@"Dealloc bundle: %@", self.name);
}

#pragma mark - Parsing
- (NSString*)parseString:(NSFileHandle*)fileHandle
{
    //Read the size
    short stringSize = [self parseShort:fileHandle];
    
    //Read the string data
    NSData *stringData = [fileHandle readDataOfLength:stringSize];
    NSString *result = [[NSString alloc] initWithData:stringData encoding:NSUTF8StringEncoding];
    
    //Done
    return result;
}

- (short)parseShort:(NSFileHandle*)fileHandle
{
	short temp;
    NSData *data = [fileHandle readDataOfLength:sizeof(short)];
    [data getBytes:&temp length:data.length];
    short result = CFSwapInt16BigToHost(temp);
    return result;
}

- (long long)parseLongLong:(NSFileHandle*)fileHandle
{
    long long tempLong;
    NSData *data = [fileHandle readDataOfLength:sizeof(long long)];
    [data getBytes:&tempLong length:data.length];
    long long result = CFSwapInt64BigToHost(tempLong);
    return result;
}

- (NSData*)parseBinary:(NSFileHandle*)fileHandle
{
    //Read the size
    long long sectionSize = [self parseLongLong:fileHandle];
    
    //Read the data
    NSData *result = [fileHandle readDataOfLength:(NSUInteger)sectionSize];
    
    //Done
    return result;
}

- (NSRange)parseBinaryRange:(NSFileHandle*)fileHandle
{
    //Read the size
    long long sectionSize = [self parseLongLong:fileHandle];
    
    //Save off the range
    NSRange result = NSMakeRange((NSUInteger)fileHandle.offsetInFile, (NSUInteger)sectionSize);
    
    //Skip over the binary area
    [fileHandle seekToFileOffset:fileHandle.offsetInFile + sectionSize];
    
    return result;
}

- (void)parseBundleData
{
    //See if a file exists and is non-zero
    NSError *error = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:self.filePath error:&error];
    long long fileSize = [fileAttributes[NSFileSize] longLongValue];
    
	//Sanity check before the sanity check - did we even download anything?
	if (fileAttributes == nil || error != nil || fileSize == 0)
	{
		self.error = [NSError errorWithDomain:@"Download Failed" code:99 userInfo:nil];
		DebugLog(@"No file data to verify - download failed");
        return;
	}
    
    //Open the file for reading
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:self.filePath];
    
    //First sanity check the header and bail on fail
    NSData *headerData = [fileHandle readDataOfLength:[HEADER length]];
    NSString *readHeaderString = [[NSString alloc] initWithData:headerData encoding:NSASCIIStringEncoding];
    if (![readHeaderString isEqualToString:HEADER])
    {
        self.error = [NSError errorWithDomain:@"ComicBundleIncorrectHeader" code:103 userInfo:nil];
        DebugLog(@"Invalid bundle file header");
    }

    //Read the bundle checksum first
    //self.expectedHash = [data subdataWithRange:NSMakeRange([data length] - CC_SHA1_DIGEST_LENGTH, CC_SHA1_DIGEST_LENGTH)];

    //Read the version
    self.bundleVersion = [self parseShort:fileHandle];
    
    //Set up placeholders
    self.writers = [NSMutableArray array];
    self.artists = [NSMutableArray array];
    self.categories = [NSMutableArray array];
    self.pages = [NSMutableArray array];

    //Loop and parse pages
    BOOL hasLegacyHorizontals = NO;
    BOOL testedLegacyHorizontals = NO;
    while (fileHandle.offsetInFile < fileSize)
    {
        @autoreleasepool
        {
            //Read the section type
            short sectionType = [self parseShort:fileHandle];

            switch (sectionType)
            {
                case ICON:	//Sections we don't need
                case THUMB:
                case COMIC_IMAGE_SMALL:
                case COMIC_IMAGE_MEDIUM:
                case COMIC_IMAGE_LARGE:
                case OWNER_EMAIL_ENCRYPTED:
                case OWNER_EMAIL:
                case PANEL_KEY:
                case HORIZONTAL_IMAGE_ENCRYPTED:
                case VERTICAL_IMAGE_ENCRYPTED:
                {
                    //Skip over these
                    [self parseBinary:fileHandle];
                    break;
                }
                case HORIZONTAL_IMAGE_UNENCRYPTED:
                {
                    NSRange imageRange = [self parseBinaryRange:fileHandle];
                    CKBundlePageInfo *page = [[CKBundlePageInfo alloc] initWithLocalImageFile:self.filePath range:imageRange orientation:CKBundlePageOrientationLandscape];
                    
                    //Test the first horizontal image to determine if it's legacy or not.
                    //We only do this once for performance reasons.
                    if (testedLegacyHorizontals == NO)
                    {
                        testedLegacyHorizontals = YES;
                        @autoreleasepool
                        {
                            UIImage *testImage = [self bundleImageForPage:page size:0];
                            hasLegacyHorizontals = CGSizeEqualToSize(CGSizeMake(480, 320), testImage.size);
                        }
                    }
                    
                    //If we've tested legacy horizontals and we do not have them, add the page
                    if (hasLegacyHorizontals == NO)
                    {
                        [self.pages addObject:page];
                    }
                    
                    break;
                }
                case COMIC_AD_HORIZONTAL:
                case COMIC_AD_VERTICAL:
                {
                    NSString *urlString = [self parseString:fileHandle];
                    NSURL *url = [NSURL URLWithString:urlString];
                    CKBundlePageInfo *page = [[CKBundlePageInfo alloc] initWithURL:url];
                    [self.pages addObject:page];
                    break;
                }
                case COVER:
                case VERTICAL_IMAGE_UNENCRYPTED:
                {
                    NSRange imageRange = [self parseBinaryRange:fileHandle];
                    CKBundlePageInfo *page = [[CKBundlePageInfo alloc] initWithLocalImageFile:self.filePath range:imageRange orientation:CKBundlePageOrientationPortrait];
                    [self.pages addObject:page];
                    break;
                }
                case GENERATED_DATE:
                {
                    self.generatedDate = [self parseString:fileHandle];
                    break;
                }
                case GENERATED_BY:
                {
                    self.generatedBy = [self parseString:fileHandle];
                    break;
                }
                case COMIC_ID:
                {
                    self.productID = [self parseString:fileHandle];
                    break;
                }
                case SYNOPSIS:
                {
                    self.synopsis = [self parseString:fileHandle];
                    break;
                }
                case NAME:
                {
                    self.name = [self parseString:fileHandle];
                    
                    //Get the issue number
                    NSString *tempString;
                    NSScanner *scanner = [NSScanner scannerWithString:self.name];
                    if ([scanner scanUpToString:@"#" intoString:&tempString] && [scanner scanString:@"#" intoString:&tempString] && [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] intoString:&tempString])
                    {
                        self.issueNumber = tempString;
                    }
                    
                    break;
                }
                case DESCRIPTION:
                {
                    self.productDescription = [self parseString:fileHandle];
                    break;
                }
                case WRITER:
                {
                    [self.writers addObject:[self parseString:fileHandle]];
                    break;
                }
                case ARTIST:
                {
                    [self.artists addObject:[self parseString:fileHandle]];
                    break;
                }
                case PUBLISHER_NAME:
                {
                    self.publisherName = [self parseString:fileHandle];
                    break;
                }
                case VIDEO_URL:
                {
                    NSString *urlString = [self parseString:fileHandle];
                    NSURL *url = [NSURL URLWithString:urlString];
                    CKBundlePageInfo *page = [[CKBundlePageInfo alloc] initWithVideoURL:url];
                    [self.pages addObject:page];
                    break;
                }
                case PUBLISHER_DATE:
                {
                    self.publisherDate = [self parseString:fileHandle];
                    break;
                }
                case COPYRIGHT:
                {
                    self.copyright = [self parseString:fileHandle];
                    break;
                }
                case HORIZONTAL_COUNT:
                {
                    self.expectedHorizontalCount = [self parseShort:fileHandle];
                    break;
                }
                case VERTICAL_COUNT:
                {
                    self.expectedVerticalCount = [self parseShort:fileHandle];
                    break;
                }
                case CONTENT_COUNT:
                {
                    self.expectedTotalCount = [self parseShort:fileHandle];
                    break;
                }
                case CATEGORY:
                {
                    [self.categories addObject:[self parseString:fileHandle]];
                    break;
                }
                case PUBLISHER_ID:
                {
                    self.publisherID = [self parseString:fileHandle];
                    break;
                }
                case PRODUCT_TYPE:
                {
                    self.productType = [self parseString:fileHandle];
                    break;
                }
                case SERIES_COMIC_ID:
                {
                    self.parentProductID = [self parseString:fileHandle];
                    break;
                }
                case BUNDLE_CHECKSUM:
                {
                    //Save the file position
                    self.checksumDataSize = (NSUInteger)(fileHandle.offsetInFile - sizeof(short));
                    
                    //Read the size
                    short hashSize = [self parseShort:fileHandle];
                    
                    //Read the hash
                    self.expectedHash = [fileHandle readDataOfLength:hashSize];
                    break;
                }
                case MANGA_ORIENTATION:
                {
                    short manga = [self parseShort:fileHandle];
                    if (manga == 1)
                    {
                        self.pagination = CKBundlePaginationManga;
                    }
                    break;
                }
                case HORIZONTAL_FADES:
                {
                    short horizontalFades = [self parseShort:fileHandle];
                    if (horizontalFades == 1)
                    {
                        self.presentation = CKBundlePresentationHorizontal;
                        self.transition = CKBundleTransitionFade;
                    }
                    break;
                }
                case RESERVED_SHORT_2:
                case RESERVED_SHORT_3:
                case RESERVED_SHORT_4:
                case RESERVED_SHORT_5:
                case RESERVED_SHORT_6:
                case RESERVED_SHORT_7:
                case RESERVED_SHORT_8:
                case RESERVED_SHORT_9:
                case RESERVED_SHORT_10:
                {
                    //Reserved for future expansion - just ignore and keep parsing
                    [self parseShort:fileHandle];
                    break;
                }
                case RESERVED_STRING_1:
                case RESERVED_STRING_2:
                case RESERVED_STRING_3:
                case RESERVED_STRING_4:
                case RESERVED_STRING_5:
                case RESERVED_STRING_6:
                case RESERVED_STRING_7:
                case RESERVED_STRING_8:
                case RESERVED_STRING_9:
                case RESERVED_STRING_10: 
                {
                    //Reserved for future expansion - just ignore and keep parsing
                    [self parseString:fileHandle];
                    break;
                }
                default:
                {
                    //Unexpected section! Halt parsing
                    self.error = [NSError errorWithDomain:@"ComicBundleUnexpectedSectionType" code:101 userInfo:nil];
                    [fileHandle closeFile];
                    return;
                }
            }
        }
    }
    
    //Clean up
    [fileHandle closeFile];
    
    //If the first page is a video URL, move it to the end.
    CKBundlePageInfo *page = [self.pages firstObject];
    if (page.pageType == CKBundlePageTypeVideo)
    {
        [self.pages removeObject:page];
        [self.pages addObject:page];
    }
}

#pragma mark - Authentication
- (BOOL)checksumIsValid
{
    BOOL result = NO;
    @try
    {
        //Calculate a SHA1 hash of all data up to this point
        //We do this with a separate file handle to avoid a memory spike
        NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:self.filePath];
        
        //Do the SHA1 calc
        CC_SHA1_CTX sha1;
        CC_SHA1_Init(&sha1);
        int bytesRead = 0;
        while (bytesRead < self.checksumDataSize)
        {
            @autoreleasepool
            {
                NSUInteger bytesToRead = self.checksumDataSize - bytesRead;
                if (bytesToRead > 4096*4) bytesToRead = 4096*4;
                NSData *fileData = [handle readDataOfLength:bytesToRead];
                CC_SHA1_Update(&sha1, [fileData bytes], (CC_LONG)[fileData length]);
                bytesRead += [fileData length];
            }
        }
        unsigned char hash[CC_SHA1_DIGEST_LENGTH];
        CC_SHA1_Final(hash, &sha1);
        [handle closeFile];
        
        //Compare
        NSData *calculatedHash = [NSData dataWithBytes:hash length:CC_SHA1_DIGEST_LENGTH];
        if (![self.expectedHash isEqualToData:calculatedHash])
        {
            self.error = [NSError errorWithDomain:@"ComicBundleFailed" code:100 userInfo:nil];
            DebugLog(@"File is corrupt!  Expected %@ got %@", self.expectedHash, calculatedHash);
            return NO;
        }
        
        result = YES;
    }
    @catch(NSException *ex)
    {
        DebugLog(@"Unhandled exception verifying file: %@", ex);
        result = NO;
    }
    
    return result;
}

- (void)signBundle
{
    //Get the device ID and hash it
    NSString *hashString = [[UIDevice persistentInstallID] stringByAppendingString:self.productID];
    NSData *dataToChecksum = [hashString dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char hash[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([dataToChecksum bytes], (CC_LONG)[dataToChecksum length], hash);
    
    //Write a writable file handle and seek to the SHA1 position
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:self.filePath];
    [handle seekToFileOffset:self.checksumDataSize + (sizeof(short)*2)];
    
    //Write the hash over the original checksum
    [handle writeData:[NSData dataWithBytes:hash length:CC_SHA1_DIGEST_LENGTH]];
    
    //Clean up
    [handle closeFile];
}

- (BOOL)checkHashWithID:(NSString*)uniqueID
{
    //Get the device ID and hash it
    NSString *hashString = [uniqueID stringByAppendingString:self.productID];
    NSData *dataToChecksum = [hashString dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char hash[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([dataToChecksum bytes], (CC_LONG)[dataToChecksum length], hash);
    
    //Compare the device ID hash to the saved hash
    NSData *calculatedHash = [NSData dataWithBytes:hash length:CC_SHA1_DIGEST_LENGTH];
    if (![self.expectedHash isEqualToData:calculatedHash])
    {
        //Set the error
        DebugLog(@"File is tampered!  Expected %@ got %@", self.expectedHash, calculatedHash);
        self.error = [NSError errorWithDomain:@"ComicBundle" code:944 userInfo:nil];
        return NO;
    }
    
    return YES;
}

- (BOOL)isAuthentic
{
    if (self.productID == nil || self.error != nil)
    {
        return NO;
    }
    
    //First try the new ID which is how we're signing things now
    if ([self checkHashWithID:[UIDevice persistentInstallID]])
    {
        DebugLog(@"Comic passed new signature");
        return YES;
    }
    
    //Then try the legacy ID, which we could have for books downloaded before the UDID->InstallID switch
    if ([self checkHashWithID:[UIDevice persistentDeviceID]])
    {
        DebugLog(@"Comic passed legacy signature, updating to new signature");
        self.error = nil;
        [self signBundle];
        return YES;
    }
    
    //Ok, see if it's in the purchased books
    if ([[CKServer sharedInstance] hasPurchasedProduct:self.productID])
    {
        DebugLog(@"Comic found in purchase history, re-signing");
        self.error = nil;
        [self signBundle];
        return YES;
    }
    
    //No good if we got here
    DebugLog(@"Comic was invalid!");
    return NO;
}

#pragma mark - Image Fetching
- (UIImage*)coverImage
{
    //Determine the filename
    NSString *filename = [[NSFileManager coversPath] stringByAppendingPathComponent:self.productID];
    
    //Create it if needed
    if ([[NSFileManager defaultManager] fileExistsAtPath:filename] == NO)
    {
        //Get the first page for making the cover
        UIImage *coverImage;
        CGFloat coverSize = 1024;
        switch (self.pagination)
        {
            case CKBundlePaginationStandard: coverImage = [self bundleImageForPage:[self.pages firstObject] size:coverSize]; break;
            case CKBundlePaginationManga: coverImage = [self bundleImageForPage:[self.pages lastObject] size:coverSize]; break;
        }
        
        //Ensure the covers path exists
        [[[NSFileManager alloc] init] createDirectoryAtPath:[NSFileManager coversPath] withIntermediateDirectories:YES attributes:nil error:nil];
        
        //Landscape, square, something wacked - fit to width, bottom justified
        if (coverImage.size.width >= coverImage.size.height)
        {
            //Determine the size of the cover image - assuming full screen portrait
            CGFloat aspect = coverImage.size.width / coverImage.size.height;
            CGSize newImageSize = CGSizeMake(MIN([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width),MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width));
            
            CGSize fittingSize = CGSizeMake(newImageSize.width, newImageSize.width / aspect);
            CGRect drawRect = CGRectIntegral(CGRectMake(0, newImageSize.height - fittingSize.height, newImageSize.width, fittingSize.height));
        
            //Create the new image
            UIGraphicsBeginImageContextWithOptions(newImageSize, NO, 0.0);
            CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
            [coverImage drawInRect:drawRect];
            coverImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        //Save the file
        NSData *imageData = UIImagePNGRepresentation(coverImage);
        [imageData writeToFile:filename atomically:NO];
    }
    
    //Done
    return [UIImage imageWithContentsOfFile:filename];
}

- (UIImage*)bundleImageForPage:(CKBundlePageInfo*)pageInfo size:(CGFloat)size
{
    //Handle non-bundle image types
    switch (pageInfo.pageType)
    {
        case CKBundlePageTypeRemoteImage: return [UIImage imageWithData:[NSData dataWithContentsOfURL:pageInfo.url]];
        case CKBundlePageTypeURL: return [UIImage imageNamed:@"URL_icon"];
        case CKBundlePageTypeVideo: return [UIImage imageNamed:@"movie_icon"];
        case CKBundlePageTypeLocalImage:
        {
            //Open the bundle file
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:self.filePath];
            
            //Determine maximum image size, passed in value or calculated by device if 0
            CGFloat maxPageSize = size;
            if (size == 0)
            {
                switch ([[UIDevice currentDevice] userInterfaceIdiom])
                {
                    case UIUserInterfaceIdiomPad:
                    {
                        //iPad use 1400 for non-retina, 2800 for retina
                        maxPageSize = [UIScreen mainScreen].scale > 1.0 ? 2800 : 1400;
                        break;
                    }
                    case UIUserInterfaceIdiomPhone:
                    {
                        CGSize screenSize = [UIScreen mainScreen].bounds.size;
                        if (MAX(screenSize.width, screenSize.height) == 480)
                        {
                            //iPhone 4s
                            maxPageSize = 1024;
                        }
                        else if (MAX(screenSize.width, screenSize.height) == 568)
                        {
                            //iPhone 5, 5s
                            maxPageSize = 1400;
                        }
                        else
                        {
                            //iPhone 6 or bigger
                            maxPageSize = 2800;
                        }
                        break;
                    }
                    case UIUserInterfaceIdiomUnspecified:
                    {
                        maxPageSize = 1024;
                        break;
                    }
                }
            }
            
            //Pull out the image
            UIImage *image = nil;
            if (fileHandle)
            {
                @autoreleasepool
                {
                    [fileHandle seekToFileOffset:pageInfo.binaryRange.location];
                    NSData *pageData = [fileHandle readDataOfLength:pageInfo.binaryRange.length];
                    
                    //Thumbnail it from the raw data
                    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)pageData, NULL);
                    if (imageSource)
                    {
                        CFDictionaryRef options = (__bridge CFDictionaryRef) @{
                                                                               (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                                               (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                                               (id) kCGImageSourceThumbnailMaxPixelSize : @(maxPageSize)
                                                                               };
                        CGImageRef thumbnail = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options);
                        CFRelease(imageSource);
                        if (thumbnail)
                        {
                            image = [UIImage imageWithCGImage:thumbnail];
                            CFRelease(thumbnail);
                        }
                    }
                }
            }
            [fileHandle closeFile];
            return image;
        }
    }
    
    return nil;
}

- (void)fetchBundleImageForPage:(CKBundlePageInfo*)pageInfo size:(CGFloat)size response:(CKBundleImageResult)response
{
    //Sanity check
    if (response == nil)
    {
        return;
    }
    
    //Use high priority for full pages
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(queue, ^
                   {
                       UIImage *image = [weakSelf bundleImageForPage:pageInfo size:size];
                       dispatch_async(dispatch_get_main_queue(), ^
                                      {
                                          response(image);
                                      });
                   });
}

@end
