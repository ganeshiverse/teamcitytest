package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.DeleteMediaItemResponse;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class DeleteMediaFromServerLoader extends DataAsyncTaskLibLoader<DeleteMediaItemResponse> {

    private MediaItemDbModel mediaToDelete;

    public DeleteMediaFromServerLoader(Context context, MediaItemDbModel mediaToDelete) {
        super(context);
        this.mediaToDelete = mediaToDelete;
    }

    @Override
    protected DeleteMediaItemResponse performLoad() throws Exception {
        return ServerMethods.getInstance().deleteMediaItem(mediaToDelete.getMediaId(), GlobalDataModel.getInstance().getActiveUser().getToken());
    }
}
