package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.SaveMyCircleResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.SaveMyCircleLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class AddCircleFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = AddCircleFragment.class.getSimpleName();
    private AddCircleFragment mFragment;
    private MainContentActivity mActivity;

    private static final String SAVE_MY_CIRCLE_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.AddCircleFragment.SAVE_MY_CIRCLE_LOADER";

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.circleNameEditTextContainer)
    private FloatLabeledEditText circleNameEditTextContainer;

    @InjectView(R.id.commentsEditTextContainer)
    private FloatLabeledEditText commentsEditTextContainer;

    @InjectView(R.id.button_addCircles)
    private Button addCircleButton;

    @Override
    protected void initActionBar() {
        String mActionbarTitle = getString(R.string.title_create_circle);
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, getString(R.string.title_create_circle)));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_newcircle, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        setEventListeners();
    }

    private void setEventListeners() {
        circleNameEditTextContainer.setOnClickListener(this);
        commentsEditTextContainer.setOnClickListener(this);
        addCircleButton.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.circleNameEditTextContainer:
            case R.id.commentsEditTextContainer:
                if (view instanceof FloatLabeledEditText) {
                    ((FloatLabeledEditText) view).getEditText().requestFocus();
                    CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                }
                break;
            case R.id.button_addCircles:

                if (validateUserInputs()) {
                    // Create SaveCircle request body
                    JSONObject mainObject = new JSONObject();

                    try {
                        mainObject.put("Token", mActiveUser != null ? mActiveUser.getToken() : "");

                        JSONObject dataObject = new JSONObject();
                        dataObject.put("CircleName", circleNameEditTextContainer.getEditText().getText().toString().trim());
                        dataObject.put("CircleDescription", commentsEditTextContainer.getEditText().getText().toString().trim());
                        dataObject.put("IsProtected", "0");
                        dataObject.put("StartDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
                        dataObject.put("StopDate", "");
                        dataObject.put("StopReasonDesc", "");
                        dataObject.put("StopReason_SLU", "");
                        dataObject.put("UserCircle_ID", "");
                        dataObject.put("UserID", mActiveUser != null ? mActiveUser.getEcUserID() + "" : "");
                        dataObject.put("UserType", "");

                        mainObject.put("Data", dataObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // POST saveCircle request
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        SaveMyCircleLoader saveMyCircleLoader = new SaveMyCircleLoader(mActivity, mainObject.toString());
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_MY_CIRCLE_LOADER), saveMyCircleLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }

                break;

            default:
                break;
        }
    }

    private boolean validateUserInputs() {
        try {
            if (TextUtils.isEmpty(URLEncoder.encode(circleNameEditTextContainer.getEditText().getText().toString().trim(), "UTF-8"))) {
                showToast(R.string.input_circle_name);
                circleNameEditTextContainer.getEditText().requestFocus();
                return false;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SAVE_MY_CIRCLE_LOADER)) {

        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(SAVE_MY_CIRCLE_LOADER)) {
            // {"Status":"Success","data":"9F55AF92-B4D7-496E-97E7-A6BC1B8D6116","message":"rh  Group Created Successfully"}

            if (result != null && result instanceof SaveMyCircleResponse) {
                final SaveMyCircleResponse saveMyCircleResponse = (SaveMyCircleResponse) result;

                if (saveMyCircleResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(saveMyCircleResponse.getMessage());

                            mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });
                }
            }
        }
    }
}
