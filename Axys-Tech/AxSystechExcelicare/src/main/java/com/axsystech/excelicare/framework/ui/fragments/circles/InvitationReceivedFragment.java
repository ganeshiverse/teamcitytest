package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.InvitationReceivedDataResponse;
import com.axsystech.excelicare.data.responses.InvitationReceivedDetails;
import com.axsystech.excelicare.db.models.CirclesInvitationReceivedDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment;
import com.axsystech.excelicare.loaders.CircleInvitationReceivedDbLoader;
import com.axsystech.excelicare.loaders.GetCircleInvitationReceivedFromDbLoader;
import com.axsystech.excelicare.loaders.InvitationReceivedLoader;
import com.axsystech.excelicare.loaders.InvitationReceivedSearchLoader;
import com.axsystech.excelicare.loaders.RemoveInvitationLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.DateFormatUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class InvitationReceivedFragment extends BaseFragment implements View.OnClickListener, TextWatcher, InvitationDetailScreen.InvitationStatusChangeListener {

    private String TAG = InvitationReceivedFragment.class.getSimpleName();
    private InvitationReceivedFragment mFragment;
    private MainContentActivity mActivity;

    private LayoutInflater mLayoutInflater;

    @InjectView(R.id.mediaListView)
    private ListView mInvitationsReceivedListView;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    @InjectView(R.id.filter_btn)
    private ImageButton filterBtn;

    @InjectView(R.id.help_btn)
    private ImageButton helpBtn;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    private CirclesInvitationReceivedDbModel mCirclesInvitationReceivedDbModel;
    private ArrayList<InvitationReceivedDetails> mMembersInvitationReceived;


    private ArrayList<CirclesInvitationReceivedDbModel> mDbMembersInvitationReceivedPending = new ArrayList<CirclesInvitationReceivedDbModel>();
    private ArrayList<CirclesInvitationReceivedDbModel> mDbMembersInvitationReceivedAccepted = new ArrayList<CirclesInvitationReceivedDbModel>();
    private ArrayList<CirclesInvitationReceivedDbModel> mDbMembersInvitationReceivedDeclined = new ArrayList<CirclesInvitationReceivedDbModel>();

    public static ArrayList<CirclesInvitationReceivedDbModel> dbCirclesInvitationReceivedDbModel;

    private CircleInvitationReceivedAdapter circleInvitationReceivedAdapter;

    private static final String INVITATION_RECEIVED = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.INVITATION_RECEIVED";
    private static final String INVITATION_DELETE = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.INVITATION_DELETE";
    private static final String INVITATION_RECEIVED_SEARCH = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.INVITATION_RECEIVED_SEARCH";
    private static final String SAVE_INVITATION_RECEIVED_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.SAVE_INVITATION_RECEIVED_DB_LOADER";
    private static final String GET_INVITATION_RECEIVED_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.GET_INVITATION_RECEIVED_FROM_DB_LOADER";

    int pageIndex;
    int PAGE_SIZE = 10;


    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), getString(R.string.title_invitations_received_from)));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_invitation_received, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        getCirclesListFromServer(true);
        preloadData();
        addEventListener();
    }

    private void addEventListener() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);
        filterBtn.setOnClickListener(this);
        helpBtn.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String searchEdit = null;

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        try {
                            searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        InvitationReceivedSearchLoader msgSentLoader = null;
                        msgSentLoader = new InvitationReceivedSearchLoader(mActivity, mActiveUser.getToken(),
                                searchEdit);
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_RECEIVED_SEARCH), msgSentLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                return false;
            }
        });

        mInvitationsReceivedListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount)) {
                    if (dbCirclesInvitationReceivedDbModel != null && dbCirclesInvitationReceivedDbModel.size() != 0) {
                        getCirclesListFromServer(false);
                    }
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                break;

            case R.id.filter_btn:
                displayStatusDialog();
                break;

            case R.id.help_btn:
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    private void getCirclesListFromServer(boolean showProgress) {

        if (CommonUtils.hasInternet()) {
            if (showProgress)
                displayProgressLoader(false);
            pageIndex = pageIndex + 1;
            InvitationReceivedLoader invitationReceivedLoader = new InvitationReceivedLoader(mActivity, mActiveUser.getToken(), "", "0", "", pageIndex, PAGE_SIZE, "", "");
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_RECEIVED), invitationReceivedLoader);
            }
        } else {
            final GetCircleInvitationReceivedFromDbLoader getCircleInvitationReceivedFromDbLoader = new GetCircleInvitationReceivedFromDbLoader(mActivity);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_INVITATION_RECEIVED_FROM_DB_LOADER), getCircleInvitationReceivedFromDbLoader);
            }
        }
    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_members));
        circleInvitationReceivedAdapter = new CircleInvitationReceivedAdapter(null);
        mInvitationsReceivedListView.setAdapter(circleInvitationReceivedAdapter);
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
        synchronized (this) {
            if (id == getLoaderHelper().getLoaderId(INVITATION_RECEIVED)) {
                if (result != null && result instanceof InvitationReceivedDataResponse) {
                    final InvitationReceivedDataResponse invitationReceivedDataResponse = (InvitationReceivedDataResponse) result;

                    if (invitationReceivedDataResponse.getDataObject() != null) {
                        mMembersInvitationReceived = invitationReceivedDataResponse.getDataObject().getDataDetailslist();

                        // separate Pending & Accepted invitations from total invitations list
                        if (mMembersInvitationReceived != null && mMembersInvitationReceived.size() > 0) {

                            dbCirclesInvitationReceivedDbModel = new ArrayList<CirclesInvitationReceivedDbModel>();

                            for (int i = 0; i < mMembersInvitationReceived.size(); i++) {
                                InvitationReceivedDetails indexedObj = mMembersInvitationReceived.get(i);
                                CirclesInvitationReceivedDbModel circlesInvitationReceivedDbModel = new CirclesInvitationReceivedDbModel();

                                try {
                                    circlesInvitationReceivedDbModel.setInvioteId(indexedObj.getInvite_Id());
                                    circlesInvitationReceivedDbModel.setMessageReceivedStatusSlu(indexedObj.getMessageReceivedStatus_SLU());
                                    circlesInvitationReceivedDbModel.setSenderName(indexedObj.getSenderName());
                                    circlesInvitationReceivedDbModel.setSenderForeName(indexedObj.getSenderForeName());
                                    circlesInvitationReceivedDbModel.setSenderSurName(indexedObj.getSenderSurName());
                                    circlesInvitationReceivedDbModel.setSendDate(indexedObj.getSendDate());
                                    circlesInvitationReceivedDbModel.setRoleSlu(indexedObj.getRole_SLU());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                dbCirclesInvitationReceivedDbModel.add(circlesInvitationReceivedDbModel);
                            }
                            final CircleInvitationReceivedDbLoader circlesInvitationReceivedDbModel = new CircleInvitationReceivedDbLoader(mActivity, dbCirclesInvitationReceivedDbModel);
                            if (isAdded() && getView() != null) {
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_INVITATION_RECEIVED_DB_LOADER), circlesInvitationReceivedDbModel);
                            }
                        }
                    } else {
                        showToast(R.string.circles_no_invitations_received);
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(INVITATION_DELETE)) {
                if (result != null && result instanceof BaseResponse) {
                    final BaseResponse baseResponseRemove = (BaseResponse) result;

                    if (baseResponseRemove.isSuccess()) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {

//                                if (completeDbCirclesInvitationReceivedDeclined.contains(mCirclesInvitationReceivedDbModel)) {
//                                    completeDbCirclesInvitationReceivedDeclined.remove(mCirclesInvitationReceivedDbModel);
//                                    mDbMembersInvitationReceivedDeclined.remove(mCirclesInvitationReceivedDbModel);
//                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedDeclined);
//                                } else if (completeDbCirclesInvitationReceivedAccepted.contains(mCirclesInvitationReceivedDbModel)) {
//                                    completeDbCirclesInvitationReceivedAccepted.remove(mCirclesInvitationReceivedDbModel);
//                                    mDbMembersInvitationReceivedAccepted.remove(mCirclesInvitationReceivedDbModel);
//                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedAccepted);
//                                } else if (completeDbCirclesInvitationReceivedPending.contains(mCirclesInvitationReceivedDbModel)) {
//                                    completeDbCirclesInvitationReceivedPending.remove(mCirclesInvitationReceivedDbModel);
//                                    mDbMembersInvitationReceivedPending.remove(mCirclesInvitationReceivedDbModel);
//                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedPending);
//                                }

                                if (mDbMembersInvitationReceivedDeclined.contains(mCirclesInvitationReceivedDbModel)) {
                                    mDbMembersInvitationReceivedDeclined.remove(mCirclesInvitationReceivedDbModel);
                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedDeclined);
                                } else if (mDbMembersInvitationReceivedAccepted.contains(mCirclesInvitationReceivedDbModel)) {
                                    mDbMembersInvitationReceivedAccepted.remove(mCirclesInvitationReceivedDbModel);
                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedAccepted);
                                } else if (mDbMembersInvitationReceivedPending.contains(mCirclesInvitationReceivedDbModel)) {
                                    mDbMembersInvitationReceivedPending.remove(mCirclesInvitationReceivedDbModel);
                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedPending);
                                }

                                mCirclesInvitationReceivedDbModel = null;
//                                completeDbCirclesInvitationReceivedPending.clear();
//                                completeDbCirclesInvitationReceivedDeclined.clear();
//                                completeDbCirclesInvitationReceivedAccepted.clear();
                                mDbMembersInvitationReceivedPending.clear();
                                mDbMembersInvitationReceivedAccepted.clear();
                                mDbMembersInvitationReceivedDeclined.clear();
                                dbCirclesInvitationReceivedDbModel.clear();
                                pageIndex = 0;
                                //completeDbCirclesInvitationReceivedDbModel.remove(mCirclesInvitationReceivedDbModel);
                                circleInvitationReceivedAdapter.notifyDataSetChanged();
                                showToast(baseResponseRemove.getMessage());
                                getCirclesListFromServer(false);
//                            if (CommonUtils.hasInternet()) {
//                                displayProgressLoader(false);
//                                InvitationReceivedLoader invitationReceivedLoader = new InvitationReceivedLoader(mActivity, mActiveUser.getToken(), "", "0","", pageIndex, PAGE_SIZE,"", "");
//                                if (isAdded() && getView() != null) {
//                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_RECEIVED), invitationReceivedLoader);
//                                }
//                            } else {
//                                showToast(R.string.error_no_network);
//                            }
                            }
                        });
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(INVITATION_RECEIVED_SEARCH)) {
                if (result != null && result instanceof InvitationReceivedDataResponse) {
                    InvitationReceivedDataResponse mMsgSentResponse = (InvitationReceivedDataResponse) result;

                    if (mMsgSentResponse.getDataObject() != null) {
                        final ArrayList<InvitationReceivedDetails> searchResultsDataObjectsAL = mMsgSentResponse.getDataObject().getDataDetailslist();

                        if (searchResultsDataObjectsAL != null && searchResultsDataObjectsAL.size() > 0) {
                            // update to list adapter with search items
                            if (circleInvitationReceivedAdapter != null) {
                                circleInvitationReceivedAdapter.updateData(mDbMembersInvitationReceivedAccepted);
                            }
                        } else {
                            showToast(R.string.no_search_results_found);
                        }
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(SAVE_INVITATION_RECEIVED_DB_LOADER)) {
                if (result != null && result instanceof ArrayList) {
                    dbCirclesInvitationReceivedDbModel = (ArrayList<CirclesInvitationReceivedDbModel>) result;
                    //TODO: pagination global list here.
                    if (dbCirclesInvitationReceivedDbModel != null && dbCirclesInvitationReceivedDbModel.size() > 0) {

                        for (int j = 0; j < dbCirclesInvitationReceivedDbModel.size(); j++) {
                            CirclesInvitationReceivedDbModel indexedObj = dbCirclesInvitationReceivedDbModel.get(j);
                            if (mMembersInvitationReceived.size() > 0)
                                if (!TextUtils.isEmpty(mMembersInvitationReceived.get(j).getMessageReceivedStatus_SLU())) {
                                    if (mMembersInvitationReceived.get(j).getMessageReceivedStatus_SLU().
                                            trim().equalsIgnoreCase(AxSysConstants.PENDING)) {
                                        mDbMembersInvitationReceivedPending.add(indexedObj);
                                        //completeDbCirclesInvitationReceivedPending.addAll(mDbMembersInvitationReceivedPending);
                                    } else if (mMembersInvitationReceived.get(j).getMessageReceivedStatus_SLU().
                                            trim().equalsIgnoreCase(AxSysConstants.ACCEPTED)) {
                                        mDbMembersInvitationReceivedAccepted.add(indexedObj);
                                        //completeDbCirclesInvitationReceivedAccepted.addAll(mDbMembersInvitationReceivedAccepted);
                                    } else if (mMembersInvitationReceived.get(j).getMessageReceivedStatus_SLU().
                                            trim().equalsIgnoreCase(AxSysConstants.DECLINED)) {
                                        mDbMembersInvitationReceivedDeclined.add(indexedObj);
                                        //completeDbCirclesInvitationReceivedDeclined.addAll(mDbMembersInvitationReceivedDeclined);
                                    }
                                }
                        }
                        //completeDbCirclesInvitationReceivedDbModel.addAll(dbCirclesInvitationReceivedDbModel);
                        // By default display Pending invitations list
                        if (circleInvitationReceivedAdapter != null) {
                            circleInvitationReceivedAdapter.updateData(mDbMembersInvitationReceivedPending);
                        }
                        //mInvitationsReceivedListView.setAdapter(circleInvitationReceivedAdapter);
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(GET_INVITATION_RECEIVED_FROM_DB_LOADER)) {
                if (result != null && result instanceof ArrayList) {
                    dbCirclesInvitationReceivedDbModel = (ArrayList<CirclesInvitationReceivedDbModel>) result;
                    // By default display Pending invitations list
                    if (dbCirclesInvitationReceivedDbModel != null && dbCirclesInvitationReceivedDbModel.size() > 0) {
                        for (int j = 0; j < dbCirclesInvitationReceivedDbModel.size(); j++) {
                            CirclesInvitationReceivedDbModel indexedObj = dbCirclesInvitationReceivedDbModel.get(j);
                            try {
                                if (!TextUtils.isEmpty(dbCirclesInvitationReceivedDbModel.get(j).getMessageReceivedStatusSlu())) {
                                    if (dbCirclesInvitationReceivedDbModel.get(j).getMessageReceivedStatusSlu().
                                            trim().equalsIgnoreCase(AxSysConstants.PENDING)) {
                                        mDbMembersInvitationReceivedPending.add(indexedObj);
                                    } else if (dbCirclesInvitationReceivedDbModel.get(j).getMessageReceivedStatusSlu().
                                            trim().equalsIgnoreCase(AxSysConstants.ACCEPTED)) {
                                        mDbMembersInvitationReceivedAccepted.add(indexedObj);
                                    } else if (dbCirclesInvitationReceivedDbModel.get(j).getMessageReceivedStatusSlu().
                                            trim().equalsIgnoreCase(AxSysConstants.DECLINED)) {
                                        mDbMembersInvitationReceivedDeclined.add(indexedObj);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        // By default display Pending invitations list
                        if (circleInvitationReceivedAdapter != null) {
                            circleInvitationReceivedAdapter.updateData(mDbMembersInvitationReceivedPending);
                        }
                    } else {
                        showToast(R.string.no_data);
                    }
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Invitation Received Paused the fragment . . . . ");
//        if(dbCirclesInvitationReceivedDbModel!=null)
//        dbCirclesInvitationReceivedDbModel.clear();
//        mDbMembersInvitationReceivedPending.clear();
//        mDbMembersInvitationReceivedAccepted.clear();
//        mDbMembersInvitationReceivedDeclined.clear();
////        completeDbCirclesInvitationReceivedPending.clear();
////        completeDbCirclesInvitationReceivedAccepted.clear();
////        completeDbCirclesInvitationReceivedDeclined.clear();
//        pageIndex=0;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        System.out.println("Invitation Received Resumed the fragment");
//        //getCirclesListFromServer();
//    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    private void displayStatusDialog() {
        final CharSequence[] items = {
                getString(R.string.dialog_text_pending),
                getString(R.string.dialog_text_accepted),
                getString(R.string.dialog_text_declined)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.picker_title_filter_by));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.dialog_text_pending))) {
                    circleInvitationReceivedAdapter.updateData(mDbMembersInvitationReceivedPending);
                } else if (items[item].equals(getString(R.string.dialog_text_accepted))) {
                    circleInvitationReceivedAdapter.updateData(mDbMembersInvitationReceivedAccepted);
                } else if (items[item].equals(getString(R.string.dialog_text_declined))) {
                    circleInvitationReceivedAdapter.updateData(mDbMembersInvitationReceivedDeclined);
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onInvitationStateChanged() {
        //TODO:
        dbCirclesInvitationReceivedDbModel.clear();
        mDbMembersInvitationReceivedPending.clear();
        mDbMembersInvitationReceivedAccepted.clear();
        mDbMembersInvitationReceivedDeclined.clear();
        pageIndex = 0;
        getCirclesListFromServer(false);
        circleInvitationReceivedAdapter.notifyDataSetChanged();
    }


    private class CircleInvitationReceivedAdapter extends BaseAdapter {

        private ArrayList<CirclesInvitationReceivedDbModel> membersInvitationsReceived;

        public CircleInvitationReceivedAdapter(ArrayList<CirclesInvitationReceivedDbModel> membersInvitationsCircle) {
            this.membersInvitationsReceived = membersInvitationsCircle;
        }

        public void updateData(ArrayList<CirclesInvitationReceivedDbModel> membersInvitationsCircle) {
            this.membersInvitationsReceived = membersInvitationsCircle;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return membersInvitationsReceived != null && membersInvitationsReceived.size() > 0 ? membersInvitationsReceived.size() : 0;
        }

        @Override
        public CirclesInvitationReceivedDbModel getItem(int position) {
            return membersInvitationsReceived.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final CirclesInvitationReceivedDbModel rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.fragment_invitation_received_row, viewGroup, false);
            }

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImageView);
            Picasso.with(getActivity()).load(R.drawable.user_icon).transform(new CircleTransform()).into(memberImageView);
            TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberNameTextView);
            TextView memberRoleTypeTextView = (TextView) convertView.findViewById(R.id.memberTextView);
            TextView dateTypeTextView = (TextView) convertView.findViewById(R.id.dateTextView);
            final TextView statusTextView = (TextView) convertView.findViewById(R.id.statusTextView);

            memberNameTextView.setTypeface(bold);
            memberRoleTypeTextView.setTypeface(regular);
            dateTypeTextView.setTypeface(bold);
            statusTextView.setTypeface(bold);

            try {
                memberNameTextView.setText(rowObject.getSenderForeName() + " " + rowObject.getSenderSurName());
                memberRoleTypeTextView.setText(rowObject.getRoleSlu());
                String displayDate = DateFormatUtils.formatDefaultDate(rowObject.getSendDate(), "yyyy-MM-dd");
                dateTypeTextView.setText(displayDate);
                String formateddate = parseDateToddMMyyyy(rowObject.getSendDate());
                //Split Date
                String string = formateddate;
                String[] parts = string.split("-");
                String part1 = parts[0]; // month
                String part2 = parts[1]; // date
                String part3 = parts[2]; // year
                dateTypeTextView.setText(part1 + " " + part2 + "," + " " + part3);

                if (rowObject.getRoleSlu() != null) {
                    if (rowObject.getMessageReceivedStatusSlu().equals(AxSysConstants.PENDING)) {
                        statusTextView.setText("Pending");
                    } else if (rowObject.getMessageReceivedStatusSlu().equals(AxSysConstants.ACCEPTED)) {
                        statusTextView.setText("Accepted");
                    } else if (rowObject.getMessageReceivedStatusSlu().equals(AxSysConstants.DECLINED)) {
                        statusTextView.setText("Declined");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
//                        pageIndex=0;
//                        completeDbCirclesInvitationReceivedPending.clear();
//                        completeDbCirclesInvitationReceivedAccepted.clear();
//                        completeDbCirclesInvitationReceivedDeclined.clear();
//                        dbCirclesInvitationReceivedDbModel.clear();
//                        mDbMembersInvitationReceivedPending.clear();
//                        mDbMembersInvitationReceivedAccepted.clear();
//                        mDbMembersInvitationReceivedDeclined.clear();
//                        circleInvitationReceivedAdapter = new CircleInvitationReceivedAdapter(completeDbCirclesInvitationReceivedPending);
//                        mInvitationsReceivedListView.setAdapter(circleInvitationReceivedAdapter);
                        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                        final InvitationDetailScreen invitationDetailScreen = new InvitationDetailScreen();
                        Bundle bundle = new Bundle();
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_MEMBER_NAME, rowObject.getSenderForeName() + " " + rowObject.getSenderSurName());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_DATE, DateFormatUtils.formatDefaultDate(rowObject.getSendDate(), "yyyy-MM-dd"));
                        //bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_TEXT, rowObject.getMessageText());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_ID, rowObject.getInvioteId());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_MESSAGE_STATUS, statusTextView.getText().toString());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_SENT, "Invitation Received");
                        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Invitation Details");
                        invitationDetailScreen.setArguments(bundle);
                        ft.add(R.id.container_layout, invitationDetailScreen, "InvitationDetailScreen");
                        ft.addToBackStack(MessageDetailsFragment.class.getSimpleName());
                        ft.commit();
                        invitationDetailScreen.setInvitationStatusChangeListener(mFragment);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    if (CommonUtils.hasInternet()) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                getActivity());
                        alert.setTitle(getString(R.string.text_alert));
                        alert.setMessage(getString(R.string.confirmation_to_delete_invitation));
                        alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

//                                mMembersInvitationReceivedPending.clear();
//                                mMembersInvitationReceivedAccepted.clear();
//                                mMembersInvitationReceivedDeclined.clear();
//                                pageIndex=0;
//                                completeDbCirclesInvitationReceivedPending.clear();
//                                completeDbCirclesInvitationReceivedAccepted.clear();
//                                completeDbCirclesInvitationReceivedDeclined.clear();
//                                dbCirclesInvitationReceivedDbModel.clear();
                                //TODO: set adapter accordingly . ..
//                                circleInvitationReceivedAdapter.notifyDataSetChanged();
                                //circleInvitationReceivedAdapter = new CircleInvitationReceivedAdapter(completeDbCirclesInvitationReceivedPending);
                                try {
                                    if (CommonUtils.hasInternet()) {
                                        // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                                        mCirclesInvitationReceivedDbModel = rowObject;
                                        displayProgressLoader(false);
                                        RemoveInvitationLoader removeInvitationLoader = new RemoveInvitationLoader(mActivity, mActiveUser.getToken(),
                                                rowObject.getInvioteId());
                                        if (mFragment.isAdded() && mFragment.getView() != null) {
                                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_DELETE), removeInvitationLoader);
                                        }
                                    } else {
                                        showToast(R.string.error_no_network);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                    }
                    return false;
                }
            });
            return convertView;
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MMMM-dd-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
