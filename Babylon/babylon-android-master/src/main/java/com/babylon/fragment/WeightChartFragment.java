package com.babylon.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.chart.OnLoadChartProgressListener;
import com.babylon.database.MonitorMeParamsLoaderCallback;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.BaseModel;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.view.ChartView;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WeightChartFragment extends ChartFragment
        implements View.OnClickListener, OnLoadChartProgressListener {

    public static final String WEIGHT_PARAMETERS_FRAGMENT_TAG = "WEIGHT_PARAMETERS_FRAGMENT_TAG";

    private TextView currentWeightTextView;
    private TextView goalWeightTextView;
    private TextView togoWeightTextView;

    public static WeightChartFragment getInstance(long beginOfTheDayInMillis, String description) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        b.putString(Constants.DESCRIPTION, description);

        WeightChartFragment fragment = new WeightChartFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_weight_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String description = getArguments().getString(Constants.DESCRIPTION);

        setRightImageButton(this, R.drawable.ic_menu_add);

        currentWeightTextView = (TextView) getView().findViewById(R.id.list_item_first_property_text_view);
        goalWeightTextView = (TextView) getView().findViewById(R.id.list_item_second_property_text_view);
        togoWeightTextView = (TextView) getView().findViewById(R.id.list_item_third_property_text_view);

        currentWeightTextView.setOnClickListener(this);
        goalWeightTextView.setOnClickListener(this);

        ChartView chartView = (ChartView) getView().findViewById(R.id.chartView);
        chartView.setOnLoadChartProgressListener(this);
        chartView.setDateInMillis(dateInMillis);
        chartView.setFeatureId(MonitorMeParam.WEIGHT_ID.getId());
        chartView.setDescription(description);

        if (!isChartLoaded) {
            chartView.showChart();
        }
        isChartLoaded = true;

        setTitle(getString(R.string.weight_chart_current_weight));
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(final long dateFrom) {
        loadDailyWeightData();
    }

    private void loadDailyWeightData() {
        List<MonitorMeParam> featureList = new LinkedList<MonitorMeParam>();
        featureList.add(MonitorMeParam.WEIGHT_ID);
        featureList.add(MonitorMeParam.GOAL_WEIGHT_ID);

        final long dateTo = DateUtils.getDayStartMills(dateInMillis, 1);

        MonitorMeParamsGet.newMonitorMeParamsGet(dateInMillis, dateTo,
                MonitorMeParam.getParameterList(featureList)).execute(getActivity(),
                new RequestListener<ResponseList<MonitorMeFeatures>>() {
                    @Override
                    public void onRequestComplete(Response<ResponseList<MonitorMeFeatures>> response) {
                        if (getActivity() != null) {
                            MonitorMeParamSync.saveMonitorMeParams(getActivity().getContentResolver(), response,
                                    dateInMillis, dateTo);
                            getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_DAY_PARAMS,
                                    MonitorMeParamsLoaderCallback.createBundle(dateInMillis),
                                    new ParamsLoader());
                            onLoadedMonitorMeParamsFromServer();
                        }
                    }
                }
        );
    }

    @Override
    public void onStartLoadChartFromServer() {
        setProgressBarVisible(true);
    }

    @Override
    public void onFinishLoadChartFromServer() {
        setProgressBarVisible(false);
    }

    @Override
    protected void startLoaderManager() {
        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_DAY_PARAMS,
                MonitorMeParamsLoaderCallback.createBundle(dateInMillis),
                new ParamsLoader());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.topbar_right_button:
            case R.id.list_item_first_property_text_view:
            case R.id.list_item_second_property_text_view:
                getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content,
                        WeightParametersFragment.getInstance(dateInMillis), WEIGHT_PARAMETERS_FRAGMENT_TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onTopBarButtonPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    /* Listeners */

    private void showWeightParameters(DailyFeatureMap dailyFeatureMap) {
        MonitorMeFeatures.Feature weightFeature = dailyFeatureMap.getFeature(
                MonitorMeParam.WEIGHT_ID);
        float currentWeight = BaseModel.getFloat(weightFeature.getValue());
        float goalWeight = BaseModel.getFloat(dailyFeatureMap.getFeature(
                MonitorMeParam.GOAL_WEIGHT_ID).getValue());

        if (currentWeight != 0) {
            currentWeightTextView.setText(getString(R.string.monitor_me_fragment_weight_pattern, currentWeight));
        } else {
            currentWeightTextView.setText(R.string.question_symbol);
        }

        if (goalWeight != 0) {
            goalWeightTextView.setText(getString(R.string.monitor_me_fragment_weight_pattern, goalWeight));
        } else {
            goalWeightTextView.setText(R.string.question_symbol);
        }

        float toGoWeight = Math.abs(currentWeight - goalWeight);
        if (currentWeight != 0 && goalWeight != 0) {
            togoWeightTextView.setText(getString(R.string.monitor_me_fragment_weight_pattern, toGoWeight));
        } else {
            togoWeightTextView.setText(R.string.question_symbol);
        }
    }

    private class ParamsLoader extends MonitorMeParamsLoaderCallback {

        @Override
        public Context getContext() {
            return getActivity();
        }

        @Override
        public List<MonitorMeParam> getMonitorMeParamList() {
            List<MonitorMeParam> featureList =
                    new LinkedList<MonitorMeParam>();
            featureList.add(MonitorMeParam.WEIGHT_ID);
            featureList.add(MonitorMeParam.GOAL_WEIGHT_ID);
            return featureList;
        }

        @Override
        public void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> featureMap) {
            if (featureMap != null && !featureMap.isEmpty()) {
                showWeightParameters(new DailyFeatureMap(featureMap));
            }
        }
    }
}