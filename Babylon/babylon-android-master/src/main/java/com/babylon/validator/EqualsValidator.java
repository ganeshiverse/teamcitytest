package com.babylon.validator;


import com.babylon.view.CustomEditText;

public class EqualsValidator extends BaseValidator {
    private CustomEditText editTextForEquals;

    public EqualsValidator(BaseValidator nextValidator, String errorMessage, CustomEditText editTextForEquals) {
        super(nextValidator, errorMessage);
        this.editTextForEquals = editTextForEquals;
    }

    @Override
    public String validate(String string) {
        return !string.equals(editTextForEquals.getText()) ? errorMessage : null;
    }
}
