package com.iversecomics.data;

import android.database.Cursor;

import com.iversecomics.app.ComicApp;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.refresh.TaskPool;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.FeaturedSlotTable;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.client.store.tasks.FetchComicsFeaturedTask;
import com.iversecomics.client.store.tasks.FetchFeaturedComicsOnDemand;
import com.iversecomics.client.store.tasks.FetchFeaturedSlotsTask;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.FeaturedComics;
import com.iversecomics.otto.event.FeaturedComicsOnDemand;
import com.iversecomics.otto.event.FeaturedSlots;
import com.iversecomics.otto.event.FeaturedSlotsOnDemand;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class DataProducer {

    Bus bus = OttoBusProvider.getInstance();
    private ComicStore.DatabaseApi databaseApi;
    private ComicStore comicStore;
    private TaskPool taskPool;

    public DataProducer(ComicStore comicStore) {
        this.comicStore = comicStore;
        this.databaseApi = comicStore.getDatabaseApi();
        taskPool = ComicApp.getInstance().getTaskPool();
    }

    public Freshness getFreshness() {
        return ComicApp.getInstance().getFreshness();
    }

    @Produce
    public ServerConfig produceServerConfig() {
        return ServerConfig.getDefault();
    }

    @Produce
    public FeaturedComics produceFeaturedComics() {
        Task featuredTask = new FetchComicsFeaturedTask(comicStore, ComicApp.getInstance().getApplicationContext());
        if (taskPool.submitIfExpired(getFreshness(), featuredTask) == null) {
            return new FeaturedComics(extractDbFeaturedComics());
        }
        return null;
    }

    public void postFeaturedComics() {
        bus.post(new FeaturedComics(extractDbFeaturedComics()));
    }

    @Produce
    public FeaturedSlots produceFeaturedSlots() {
        Task featuredTask = new FetchFeaturedSlotsTask(comicStore, ComicApp.getInstance().getApplicationContext());
        if (taskPool.submitIfExpired(getFreshness(), featuredTask) == null) {
            return new FeaturedSlots(extractDbFeaturedSlots());
        }
        return null;
    }

    public void postFeaturedSlots() {
        bus.post(new FeaturedSlots(extractDbFeaturedSlots()));
    }

    @Produce
    public FeaturedComicsOnDemand produceFeaturedComicsOnDemand() {
        Task featuredTask = new FetchFeaturedComicsOnDemand(comicStore, ComicApp.getInstance().getApplicationContext());
        if (taskPool.submitIfExpired(getFreshness(), featuredTask) == null) {
            return new FeaturedComicsOnDemand(extractDbFeaturedComicsOnDemand());
        }
        return null;
    }

    public void postFeaturedComicsOnDemand() {
        bus.post(new FeaturedComicsOnDemand(extractDbFeaturedComicsOnDemand()));
    }

    @Produce
    public FeaturedSlotsOnDemand produceFeaturedSlotsOnDemand() {
        Task featuredTask = new FetchFeaturedSlotsTask(comicStore, ComicApp.getInstance().getApplicationContext());
        if (taskPool.submitIfExpired(getFreshness(), featuredTask) == null) {
            return new FeaturedSlotsOnDemand(extractDbFeaturedSlotsOnDemand());
        }
        return null;
    }

    public void postFeaturedSlotsOnDemand() {
        bus.post(new FeaturedSlotsOnDemand(extractDbFeaturedSlotsOnDemand()));
    }


    private List<Comic> extractDbFeaturedComics() {
        Cursor cursor = comicStore.getDatabaseApi().getCursorComicsFeatured(-1);
        return extractComicsFromCursor(cursor);
    }
    private List<Comic> extractDbFeaturedComicsOnDemand() {
        Cursor cursor =  comicStore.getDatabaseApi().getCursorComicsFeaturedOnDemand("ondemand", -1);
        return extractComicsFromCursor(cursor);
    }

    private List<FeaturedSlot> extractDbFeaturedSlots() {
        Cursor cursor = comicStore.getDatabaseApi().getCursorFeaturedSlots();
        return extractFeaturedSlotsFromCursor(cursor);
    }

    private List<FeaturedSlot> extractDbFeaturedSlotsOnDemand() {
        Cursor cursor = comicStore.getDatabaseApi().getCursorOnDemandFeaturedSlots();
        return extractFeaturedSlotsFromCursor(cursor);
    }

    private List<Comic> extractComicsFromCursor(Cursor cursor) {
        List<Comic> comics = new ArrayList<Comic>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Comic slot = ComicsTable.fromCursor(cursor);
            comics.add(slot);
            cursor.moveToNext();
        }
        cursor.close();
        return comics;
    }

    private List<FeaturedSlot> extractFeaturedSlotsFromCursor(Cursor cursor) {
        List<FeaturedSlot> result = new ArrayList<FeaturedSlot>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FeaturedSlot slot = FeaturedSlotTable.fromCursor(cursor);
            result.add(slot);
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }


}
