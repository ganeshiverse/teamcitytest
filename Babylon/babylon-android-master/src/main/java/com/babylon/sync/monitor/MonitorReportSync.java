package com.babylon.sync.monitor;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.os.Bundle;

import com.babylon.BaseConfig;
import com.babylon.api.request.MonitorMeParamsLightGet;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.MonitorMeReportSchema;
import com.babylon.model.MonitorMeLightFeatures;
import com.babylon.sync.AbsSyncStrategy;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MonitorReportSync extends AbsSyncStrategy {
    private static final String TAG = MonitorReportSync.class.getSimpleName();

    /**
     * Variables for make performance server tests
     */
    private static final int PERFORMANCE_MONITOR_REPORT_TEST_AMOUNT = 50;
    private int testCount = 1;
    private List<Long> responseTestTimes = new ArrayList<>();

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        //empty method
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        long minDate = bundle.getLong(Constants.EXTRA_MIN_DATE, DateUtils.getStartDate(new Date()).getTime());
        long maxDate = bundle.getLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(new Date(), 1));

        pullMonitorMeReport(contentResolver, minDate, maxDate);
    }

    private void pullMonitorMeReport(ContentResolver contentResolver, long minDate, long maxDate) {
        Date requestDate = null;
        if (BaseConfig.PERFORMANCE_TEST_ENABLED) {
            requestDate = new Date();
            minDate = DateUtils.getDayStartMills(maxDate, -3);
            String requestLog = "Test monitor report" + testCount + " started";
            L.i(TAG, new Date(minDate).toString() + " _ " + new Date(maxDate).toString());
            L.i(TAG, requestLog);
        }

        Response<ResponseList<MonitorMeLightFeatures>> monitorMeResponse = MonitorMeParamsLightGet
                .newMonitorMeParamsGet(minDate,
                        maxDate).execute();

        if (monitorMeResponse != null && monitorMeResponse.isSuccess()) {
            if (BaseConfig.PERFORMANCE_TEST_ENABLED) {
                Date responseDate = new Date();
                long responseTime = responseDate.getTime() - requestDate.getTime();
                String responseLog = "Test monitor report" + testCount
                        + " finished, time is " + responseTime + " ms";
                L.i(TAG, responseLog);
                responseTestTimes.add(responseTime);
            }

            if (monitorMeResponse.getData() != null && monitorMeResponse.getData().getList() != null
                    && !monitorMeResponse.getData().getList().isEmpty()) {
                List<MonitorMeLightFeatures> monitorMeList = monitorMeResponse.getData().getList();
                saveMonitorMeParamsToDatabase(contentResolver, monitorMeList);
            }

            if (BaseConfig.PERFORMANCE_TEST_ENABLED) {
                if (++testCount < PERFORMANCE_MONITOR_REPORT_TEST_AMOUNT) {
                    pullMonitorMeReport(contentResolver, minDate, maxDate);
                } else {
                    if (!responseTestTimes.isEmpty()) {
                        long responseTimeSummary = 0;
                        for (long time : responseTestTimes) {
                            responseTimeSummary += time;
                        }
                        long averageResponseTime = responseTimeSummary / responseTestTimes.size();

                        String averageLog = "Average response time of "
                                + PERFORMANCE_MONITOR_REPORT_TEST_AMOUNT
                                + " request is " + averageResponseTime;
                        L.i(TAG, averageLog);
                        responseTestTimes.clear();
                    }

                    testCount = 1;
                    System.gc();
                }
            }
        }
    }

    private void saveMonitorMeParamsToDatabase(ContentResolver contentResolver,
                                               List<MonitorMeLightFeatures> monitorMeList) {
        List<ContentProviderOperation> operations = new ArrayList<>();

        for (MonitorMeLightFeatures monitorMeFeatures : monitorMeList) {
            ContentProviderOperation.Builder deleteBuilder = ContentProviderOperation.newDelete(
                    MonitorMeReportSchema.CONTENT_URI);
            ContentProviderOperation.Builder insertBuilder = ContentProviderOperation.newInsert(
                    MonitorMeReportSchema.CONTENT_URI);

            final String selection = MonitorMeReportSchema.Columns.DATE.getName() + " >= ? AND "
                    + MonitorMeReportSchema.Columns.DATE.getName() + " < ?";
            final String[] selectionArgs = new String[] {
                    String.valueOf(DateUtils.getStartDate(monitorMeFeatures.getDate()).getTime()),
                    String.valueOf(DateUtils.getStartDate(monitorMeFeatures.getDate()).getTime()
                            + android.text.format.DateUtils.DAY_IN_MILLIS)
            };
            long curReportTime = monitorMeFeatures.getDate().getTime();

            deleteBuilder.withSelection(selection, selectionArgs);
            insertBuilder.withValue(MonitorMeReportSchema.Columns.DATE.getName(),
                    DateUtils.getStartDate(curReportTime).getTime());
            insertBuilder.withValue(MonitorMeReportSchema.Columns.FEATURES.getName(),
                    GsonWrapper.getTimestampGson().toJson
                            (monitorMeFeatures.getFeatures()));

            operations.add(deleteBuilder.build());
            operations.add(insertBuilder.build());
        }

        DatabaseOperationUtils.applyBatch(contentResolver, operations);
    }

    public static void startPullMonitorMeReport(ContentResolver contentResolver, long dateFrom, long dateTo) {
        Bundle b = new Bundle();
        b.putLong(Constants.EXTRA_MIN_DATE, DateUtils.getDayStartMills(new Date(dateFrom)));
        b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(new Date(dateTo)));
        new MonitorReportSync().pull(contentResolver, b);
    }

    public static void startPullMonitorMeReport(ContentResolver contentResolver, long dateFrom) {
        startPullMonitorMeReport(contentResolver, DateUtils.getDayStartMills(dateFrom),
                DateUtils.getDayStartMills(new Date(dateFrom), 1));
    }
}
