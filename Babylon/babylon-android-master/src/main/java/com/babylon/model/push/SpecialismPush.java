package com.babylon.model.push;

public class SpecialismPush extends Push {

    private String specialism;

    public String getSpecialism() {
        return specialism;
    }

    public void setSpecialism(String specialism) {
        this.specialism = specialism;
    }
}
