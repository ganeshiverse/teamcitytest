package com.babylon.database.schema;

import android.net.Uri;
import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public interface PaymentCardScheme {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri BASE_CONTENT_URI = Uri.parse("content://" + DatabaseHelper.CONTENT_AUTHORITY);
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.CREDIT_CARD.getName())
            .build();

    public static final String SORT_PAYMENT_CARD_TYPE = Table.CREDIT_CARD.getName() + "." + Columns.TYPE + " DESC";

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.INT),
        TYPE("cardType", DBType.TEXT),
        MASKED_NUMBER("maskedNumber", DBType.TEXT),
        EXPIRES_ON("expiresOn", DBType.TEXT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public final static class Query extends BaseQuery {
        public static final int CURSOR_LOAD_ALL_CREDIT_CARD = 1841;

        public static final int _ID = 0;
        public static final int ID = 1;
    }
}
