package com.axsystech.excelicare.framework.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.util.AxSysConstants;

import java.util.Calendar;

/**
 * Created by someswarreddy on 16/07/15.
 */
public class DateTimePickerFragment extends DialogFragment {

    private DateTimeDialogListener mListener;

    private Calendar mCalendar = Calendar.getInstance();

    public void setDateTimeDialogListener(DateTimeDialogListener dateTimeDialogListener) {
        this.mListener = dateTimeDialogListener;
    }

    public interface DateTimeDialogListener {
        public void onDialogPositiveButtonClick(Calendar calendar);

        public void onDialogNegativeButtonClick();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // read intent args
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.DF_MEDIA_CALENDAR_DATE)) {
                mCalendar = (Calendar) bundle.getSerializable(AxSysConstants.DF_MEDIA_CALENDAR_DATE);
            }
        }

        LinearLayout dateTimePickerLayout = (LinearLayout) inflater.inflate(R.layout.date_time_picker_layout, null);
        final DatePicker datePicker = (DatePicker) dateTimePickerLayout.findViewById(R.id.datePicker);
        final TimePicker timePicker = (TimePicker) dateTimePickerLayout.findViewById(R.id.timePicker);

        // set selected date & time to date & time pickers
        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DATE), null);
//        datePicker.updateDate(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DATE));

        // set maximum date for date picker
        datePicker.setMaxDate(Calendar.getInstance().getTime().getTime());

        timePicker.setCurrentHour(mCalendar.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(mCalendar.get(Calendar.MINUTE));
        timePicker.setIs24HourView(false);

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                mCalendar.set(Calendar.MINUTE, minute);
            }
        });

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(dateTimePickerLayout);

        alertDialog.setPositiveButton(R.string.set_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mCalendar == null) {
                    mCalendar = Calendar.getInstance();
                }
                mCalendar.set(Calendar.YEAR, datePicker.getYear());
                mCalendar.set(Calendar.MONTH, datePicker.getMonth());
                mCalendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                mCalendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                mCalendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                mCalendar.set(Calendar.AM_PM, (mCalendar.get(Calendar.AM_PM) == (Calendar.AM)) ? 0 : 1);

                if (mListener != null) {
                    mListener.onDialogPositiveButtonClick(mCalendar);
                }

                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DateTimePickerFragment.this.getDialog().cancel();

                if (mListener != null) {
                    mListener.onDialogNegativeButtonClick();
                }
            }
        });

        return alertDialog.create();
    }
}
