package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CardContentDataResponse;
import com.axsystech.excelicare.data.responses.CardServiceDataResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by hkyerra on 7/13/2015.
 */
public class DashBoardCardContentLoader extends DataAsyncTaskLibLoader<String> {

    private String cardContentUrl;

    public DashBoardCardContentLoader(Context context, String cardContentUrl) {
        super(context);
        this.cardContentUrl = cardContentUrl;
    }

    @Override
    protected String performLoad() throws Exception {
        return ServerMethods.getInstance().getSingleCardContentData(cardContentUrl);
    }
}
