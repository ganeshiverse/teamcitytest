package com.iversecomics.client.account;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.io.Digest;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class PasswordRequestTask extends ComicStoreTask {

	String userName;
	String userPassword;
	boolean marketingOptIn;
	private static final Logger LOG = LoggerFactory.getLogger(PasswordRequestTask.class);
	
	public final static String PASSWORD_REQUEST_SERVER_RESPONSE  = "iverse.action.server.password.request.response";
	
	public PasswordRequestTask(ComicStore comicStore) {
		super(comicStore);
	}
	
	public void setUserName(final String userName) {
		this.userName = userName;
	}
	
    @Override
    public void execTask() {

    	final Intent intent = new Intent(PASSWORD_REQUEST_SERVER_RESPONSE);
    	
        try {
            final JSONObject payload = new JSONObject();
            payload.put("userName", userName);
            payload.put("action", "forgotPassword");
            
            final StringBuilder verifyCode = new StringBuilder();
            verifyCode.append(userName);
            verifyCode.append(SHARED_SECRET);
            payload.put("verifyCode", Digest.sha1(verifyCode.toString()));
            
            final ServerApi server = comicStore.getServerApi();
            final JSONObject json = server.submitUserAccount(payload);
            final String status = json.optString("status");
            
            intent.putExtra("userName", userName);
            intent.putExtra("status", status);
        }
        catch (Exception e) 
        {
        	LOG.warn(e.toString());
        }
        finally
        {
        	LocalBroadcastManager.getInstance(IverseApplication.getApplication().getApplicationContext()).sendBroadcast(intent); 
        }

    }
}
