package com.axsystech.excelicare.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Date: 13.05.14
 * Time: 12:08
 *
 * @author SomeswarReddy
 */
public final class PreferencesUtil {

    public static final String URL = "com.axsystech.excelicare.util.PreferencesUtil.URL";
    public static final String PREF_APP = "com.axsystech.excelicare.util.PreferencesUtil.PREF_APP";
    public static final String EMAIL_KEY = "com.axsystech.excelicare.util.PreferencesUtil.EMAIL_KEY";
    public static final String USER_ID_KEY = "com.axsystech.excelicare.util.PreferencesUtil.USER_ID_KEY";
    public static final String REMEMBER_ME_KEY = "com.axsystech.excelicare.util.PreferencesUtil.REMEMBER_ME_KEY";
    public static final String NEED_LOADING_KEY = "com.axsystech.excelicare.util.PreferencesUtil.NEED_LOADING_KEY";

    public static final String TIME_KEY = "com.axsystech.excelicare.util.PreferencesUtil.TIME_KEY";
    public static final String HINT_KEY = "com.axsystech.excelicare.util.PreferencesUtil.HINT_KEY";
    public static final String PULSE_KEY = "com.axsystech.excelicare.util.PreferencesUtil.PULSE_KEY";
    public static final String SYSTOLIC_KEY = "com.axsystech.excelicare.util.PreferencesUtil.SYSTOLIC_KEY";
    public static final String DIASTOLIC_KEY = "com.axsystech.excelicare.util.PreferencesUtil.DIASTOLIC_KEY";

    private PreferencesUtil() {
    }

    public static boolean isExistPreference(final Context context, final String name) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(name, false);
    }

    public static void setPreference(final Context context, final String name, final boolean flag) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(name, flag).apply();
    }

    public static void rememberMe(final Context context, final long userId) {
        setPreference(context, REMEMBER_ME_KEY, true);
        setUserId(context, userId);
    }

    public static void forgotMe(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(REMEMBER_ME_KEY, false).putBoolean(NEED_LOADING_KEY, false).remove(USER_ID_KEY).apply();
    }

    public static boolean isRememberMe(final Context context) {
        return isExistPreference(context, REMEMBER_ME_KEY);
    }

    public static void setEmail(final Context context, final String email) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(EMAIL_KEY, email);
    }

    public static String getEmail(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getString(EMAIL_KEY, null);
    }

    public static void setSettings(final Context context, final String name, final int value) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt(name, value).apply();
    }

    public static int getPulse(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(PULSE_KEY, 0);
    }

    public static void setPulse(final Context context, final int pulse) {
        setSettings(context, PULSE_KEY, pulse);
    }

    public static int getSystolic(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(SYSTOLIC_KEY, 0);
    }

    public static void setSystolic(final Context context, final int systolic) {
        setSettings(context, SYSTOLIC_KEY, systolic);
    }

    public static int getDiastolic(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(DIASTOLIC_KEY, 0);
    }

    public static void setDiastolic(final Context context, final int diastolic) {
        setSettings(context, DIASTOLIC_KEY, diastolic);
    }

    public static long getTime(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(TIME_KEY, 0);
    }

    public static void setTime(final Context context, final long millisecond) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(TIME_KEY, millisecond).apply();
    }

    public static void setUserId(final Context context, final long userId) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(USER_ID_KEY, userId).apply();
    }

    public static long getUserId(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(USER_ID_KEY, -1);
    }

    public static String getHintBPWidget(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getString(HINT_KEY, "");
    }

    public static void setHintBPWidget(final Context context, final String hint) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(HINT_KEY, hint).apply();
    }

    public static boolean isNeedLoading(final Context context) {
        return isExistPreference(context, NEED_LOADING_KEY);
    }

    public static void setNeedLoading(final Context context, final boolean needLoading) {
        setPreference(context, NEED_LOADING_KEY, needLoading);
    }

    public static void setUrl(final Context context, final String url) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(URL, url).apply();
    }

    public static String getUrl(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getString(URL, null);
    }
}
