package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.data.responses.SiteDetailsModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SaveSiteDetailsLoader extends DataAsyncTaskLibLoader<Void> {

    private String TAG = SaveSiteDetailsLoader.class.getSimpleName();
    private final ArrayList<SiteDetailsModel> mSiteDetailsAL;

    public SaveSiteDetailsLoader(final Context context, final ArrayList<SiteDetailsModel> siteDetailsAL) {
        super(context, true);
        mSiteDetailsAL = siteDetailsAL;
    }

    @Override
    protected Void performLoad() throws Exception {
        if (mSiteDetailsAL == null) throw new SQLException("Unable to cache site details in DB");
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
            final ArrayList<SiteDetailsDBModel> siteDetailsDBModelsAL = ConverterResponseToDbModel.getSiteDetailsDBModel(mSiteDetailsAL);

            if (siteDetailsDBModelsAL != null && siteDetailsDBModelsAL.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        for (final SiteDetailsDBModel dbModel : siteDetailsDBModelsAL) {
                            final Dao<SiteDetailsDBModel, Long> siteDao = helper.getDao(SiteDetailsDBModel.class);

                            siteDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved Site Details in DB...");
                        }

                        return null;
                    }
                });
            }
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return null;
    }
}
