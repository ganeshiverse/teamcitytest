package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.SendInviteToCircleMemberResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 20/08/15.
 */
public class SendInviteToCircleMemberLoader extends DataAsyncTaskLibLoader<String> {

    private String token;
    private String dataString;

    public SendInviteToCircleMemberLoader(Context context, String token, String dataString) {
        super(context);
        this.token = token;
        this.dataString = dataString;
    }

    @Override
    protected String performLoad() throws Exception {
        return ServerMethods.getInstance().sendInviteToCircleMember(token, dataString);
    }
}
