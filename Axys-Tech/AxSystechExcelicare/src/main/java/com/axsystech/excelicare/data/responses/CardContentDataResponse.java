package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 14/07/15.
 */
public class CardContentDataResponse extends BaseResponse implements Serializable {

    @SerializedName("Panel")
    private Panel panel;

    public Panel getPanel() {
        return panel;
    }

    public class Panel implements Serializable {
        @SerializedName("Id")
        private int Id;

        @SerializedName("Name")
        private String name;

        @SerializedName("RowCount")
        private int rowCount;

        @SerializedName("Rows")
        private ArrayList<Rows> rowsArrayList;

        public int getId() {
            return Id;
        }

        public String getName() {
            return name;
        }

        public int getRowCount() {
            return rowCount;
        }

        public ArrayList<Rows> getRowsArrayList() {
            return rowsArrayList;
        }
    }

    public class Rows implements Serializable {
        @SerializedName("Cols")
        private ArrayList<Cols> colsArrayList;

        public ArrayList<Cols> getColsArrayList() {
            return colsArrayList;
        }
    }

    public class Cols implements Serializable {
        @SerializedName("SubRows")
        private ArrayList<SubRows> subRowsArrayList;

        public ArrayList<SubRows> getSubRowsArrayList() {
            return subRowsArrayList;
        }
    }

    private class SubRows implements Serializable {
        @SerializedName("Value")
        private String value;

        public String getValue() {
            return value;
        }
    }
}
