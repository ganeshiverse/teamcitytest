package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by someswarreddy on 30/06/15.
 */
public class SiteListResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<SiteDetailsModel> siteDetailsModelArrayList;

    public ArrayList<SiteDetailsModel> getSiteDetailsModelArrayList() {
        return siteDetailsModelArrayList;
    }

    public void setSiteDetailsModelArrayList(ArrayList<SiteDetailsModel> siteDetailsModelArrayList) {
        this.siteDetailsModelArrayList = siteDetailsModelArrayList;
    }

}
