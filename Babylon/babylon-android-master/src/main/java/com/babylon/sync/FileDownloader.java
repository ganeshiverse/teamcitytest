package com.babylon.sync;

import com.babylon.api.request.FileDownloadGet;
import com.babylon.api.request.base.Response;
import com.babylon.model.FileEntry;
import com.babylon.utils.L;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class FileDownloader {
    private static final String TAG = FileDownloader.class.getSimpleName();
    private static final int POOL_SIZE = 3;
    private static FileDownloader sFilesDownloader;

    private ExecutorService pool;

    private FileDownloader() {

    }

    public static synchronized FileDownloader getInstance() {
        if (sFilesDownloader == null) {
            sFilesDownloader = new FileDownloader();
        }
        return sFilesDownloader;
    }

    public void download(final FileEntry fileEntry) {
        initPool();
        execute(fileEntry);
    }

    public static FileEntry pullFileEntry(FileEntry fileEntry) {
        final List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair(FileDownloadGet.FILE_ID, fileEntry.getId()));
        Response<FileEntry> fileResponse = new FileDownloadGet().withParams(params).execute();
        if (fileResponse != null && fileResponse.getData() != null) {
            final byte[] data = fileResponse.getData().getData();
            if (fileResponse.isSuccess() && data != null) {
                try {
                    FileOutputStream outputStream = new FileOutputStream(fileEntry.getFilePath());

                    File file = new File(fileEntry.getFilePath());
                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    outputStream.write(data);
                    outputStream.close();
                } catch (IOException e) {
                    L.e(TAG, e.getMessage(), e);
                }
            }
        }
        return fileEntry;
    }

    private void execute(final FileEntry fileEntry) {
        pool.execute(new Runnable() {
            @Override
            public void run() {
                pullFileEntry(fileEntry);
            }
        });
    }

    private void initPool() {
        if (pool == null) {
            pool = Executors.newFixedThreadPool(POOL_SIZE);
        }
    }
}
