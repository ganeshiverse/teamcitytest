package com.iversecomics.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.iversecomics.archie.android.R;


public class AspectConstrainedFrame extends FrameLayout {

    static private final int MIN_WIDTH = 40;

    float aspectRatio;
    float maxHeight = -1;

    public AspectConstrainedFrame(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.getResources().obtainAttributes(attrs, R.styleable.AspectConstrainedFrame);
        try {
            if (maxHeight == -1) {
                maxHeight = ta.getDimension(R.styleable.AspectConstrainedFrame_maxHeight, 0);
            }
            aspectRatio = ta.getFloat(R.styleable.AspectConstrainedFrame_ratio, 1.5f);
        } finally {
            ta.recycle();
        }
    }

    public void setMaxHeight(float maxHeight) {
        this.maxHeight = maxHeight;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int horizontalPadding = getPaddingLeft() + getPaddingRight();
        int verticalPadding = getPaddingTop() + getPaddingBottom();

        int width;
        int height;

        if (maxHeight > 0) {
            if (heightSize > maxHeight) {
                heightSize = Math.round(maxHeight);
            }
        }
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
            int desiredHeight = getHeightFromRatio(width - horizontalPadding) + verticalPadding;
            if (heightMode == MeasureSpec.EXACTLY) {
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                height = Math.min(desiredHeight, heightSize);
            } else {
                height = desiredHeight;
            }
        } else if (widthMode == MeasureSpec.AT_MOST) {
            if (heightMode == MeasureSpec.EXACTLY) {
                int desiredWidth = getWidthFromRatio(heightSize - verticalPadding) + horizontalPadding;
                width = Math.min(desiredWidth, widthSize);
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                int desiredWidth = MIN_WIDTH;
                int desiredHeight = getHeightFromRatio(MIN_WIDTH);
                width = Math.min(desiredWidth, widthSize);
                height = Math.min(desiredHeight, heightSize);
            } else {
                int desiredWidth = MIN_WIDTH;
                width = Math.min(desiredWidth, widthSize);
                height = getHeightFromRatio(width);
            }
        } else {
            if (heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST) {
                height = heightSize;
                width = getWidthFromRatio(heightSize - verticalPadding) + horizontalPadding;
            } else {
                width = MIN_WIDTH;
                height = getHeightFromRatio(MIN_WIDTH);
            }
        }
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private int getWidthFromRatio(int height) {
        int ret = (int) (height / aspectRatio);
        if (ret < 0) {
            ret = 0;
        }
        return ret;
    }

    private int getHeightFromRatio(int width) {
        int ret = (int) (width * aspectRatio);
        if (ret < 0) {
            ret = 0;
        }
        return ret;
    }


}
