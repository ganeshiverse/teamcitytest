package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswar on 7/1/2015.
 */
public class ForgotPasswordResponse extends BaseResponse {

    @SerializedName("data")
    private ForgotPasswordData forgotPasswordData;

    public ForgotPasswordData getForgotPasswordData() {
        return forgotPasswordData;
    }

    public void setForgotPasswordData(ForgotPasswordData forgotPasswordData) {
        this.forgotPasswordData = forgotPasswordData;
    }

    public class ForgotPasswordData implements Serializable {
        @SerializedName("Forename")
        private String forename;

        @SerializedName("SiteID")
        private String siteID;

        @SerializedName("Surname")
        private String surname;

        @SerializedName("token")
        private String token;

        public String getForename() {
            return forename;
        }

        public void setForename(String forename) {
            this.forename = forename;
        }

        public String getSiteID() {
            return siteID;
        }

        public void setSiteID(String siteID) {
            this.siteID = siteID;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
