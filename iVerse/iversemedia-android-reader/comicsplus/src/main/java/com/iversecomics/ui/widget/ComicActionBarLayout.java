package com.iversecomics.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class ComicActionBarLayout extends RelativeLayout {

    public ComicActionBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        final int count = getChildCount();

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                RelativeLayout.LayoutParams st = (RelativeLayout.LayoutParams) child.getLayoutParams();
                int[] rules = st.getRules();
                if (rules[RelativeLayout.RIGHT_OF] != 0 && rules[RelativeLayout.CENTER_HORIZONTAL] != 0) {
                    View viewRight = this.findViewById(rules[RelativeLayout.RIGHT_OF]);
                    if (viewRight != null) {
                        int center = getWidth() / 2 - child.getWidth() / 2;
                        int right = viewRight.getRight();
                        int cl = Math.max(center, right);
                        int cr = cl + child.getWidth();
                        int ct = child.getTop();
                        int cb = child.getBottom();
                        child.layout(cl, ct, cr, cb);
                    }
                }
            }
        }
    }
}
