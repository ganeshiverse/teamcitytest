package com.iversecomics.otto.event;

/**
 */
public class ComicReconciledEvent {
    private String name;

    public ComicReconciledEvent() {
    }

    public ComicReconciledEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
