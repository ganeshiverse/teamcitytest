package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Nutrient;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class SearchFoodGet extends AbsRequestGet<ResponseList<Nutrient>> {

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.SEARCH_FOOD_BY_NAME_POST);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param foodName length minimum 3 character
     */
    public static SearchFoodGet newFoodListSearch(String foodName) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (!TextUtils.isEmpty(foodName)) {
            params.add(new BasicNameValuePair("name", foodName));
        }
        return (SearchFoodGet) new SearchFoodGet().withParams(params);
    }
}
