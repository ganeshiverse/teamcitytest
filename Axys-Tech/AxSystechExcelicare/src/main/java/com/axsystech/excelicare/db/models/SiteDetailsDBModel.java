package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 11/07/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_SITE_DETAILS)
public class SiteDetailsDBModel extends DBUtils implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @DatabaseField(id = true, columnName = SITE_ID)
    private String siteId;

    @DatabaseField(columnName = SITE_NAME)
    private String siteName;

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public String toString() {
        return siteName;
    }
}
