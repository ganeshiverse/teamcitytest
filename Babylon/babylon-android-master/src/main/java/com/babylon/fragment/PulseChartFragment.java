package com.babylon.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.babylon.R;
import com.babylon.chart.OnLoadChartProgressListener;
import com.babylon.dialog.PulseInputMethodDialogFragment;
import com.babylon.enums.MonitorMeParam;
import com.babylon.utils.Constants;
import com.babylon.view.ChartView;

public class PulseChartFragment extends BaseMonitorMeParamsFragment
        implements OnLoadChartProgressListener {

    public static PulseChartFragment getInstance(long beginOfTheDayInMillis, String description,
                                                 boolean isMedicalKitExist) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        b.putString(Constants.DESCRIPTION, description);
        b.putBoolean(Constants.IS_KIT_BUTTON_EXIST, isMedicalKitExist);

        PulseChartFragment fragment = new PulseChartFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pulse_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String description = getArguments().getString(Constants.DESCRIPTION);
        boolean isMedicalKitExist = getArguments().getBoolean(Constants.IS_KIT_BUTTON_EXIST);

        ChartView chartView = (ChartView) view.findViewById(R.id.chartView);
        chartView.setOnLoadChartProgressListener(this);
        chartView.setDateInMillis(dateInMillis);
        chartView.setFeatureId(MonitorMeParam.RESTING_PULSE_ID.getId());
        chartView.setDescription(description);

        if (!isChartLoaded) {
            chartView.showChart();
        }
        isChartLoaded = true;

        setTitle(getString(R.string.resting_pulse_fragment_title));
        setRightImageButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment pulseInputDialog = PulseInputMethodDialogFragment.getInstance(dateInMillis);
                pulseInputDialog.show(getFragmentManager(), PulseInputMethodDialogFragment.TAG);
            }
        }, R.drawable.ic_menu_add);
        if (isMedicalKitExist && onMedicalKitClickListener != null) {
            setSecondRightImageButton(onMedicalKitClickListener.getOnKitClickListener(),
                    R.drawable.ic_menu_kit);
        }
    }

    @Override
    public void onStartLoadChartFromServer() {
        setProgressBarVisible(true);
    }

    @Override
    public void onFinishLoadChartFromServer() {
        setProgressBarVisible(false);
    }

    /* Listeners */
    @Override
    protected void onTopBarButtonPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}