package com.axsystech.excelicare.framework.ui.fragments.media.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 12/07/15.
 */
public enum MediaDetailsViewType implements Serializable {

    ADD(1),
    VIEW(2),
    EDIT(3);

    private final int id;

    private MediaDetailsViewType(int id) {
        this.id = id;
    }
}
