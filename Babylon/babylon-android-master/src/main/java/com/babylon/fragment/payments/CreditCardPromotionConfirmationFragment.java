package com.babylon.fragment.payments;


import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.babylon.R;
import com.babylon.adapter.PaymentCardsCursorAdapter;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.api.request.payments.CreditCardDelete;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.payments.PaymentCard;
import com.babylon.model.payments.Promotion;
import com.babylon.model.payments.Redemption;
import com.babylon.model.payments.Subscription;
import com.babylon.view.PaymentCardView;
import com.babylon.view.TopBarInterface;
import com.squareup.picasso.Picasso;

public class CreditCardPromotionConfirmationFragment extends BaseFragment implements View.OnClickListener,PaymentCardView.OnPaymentCardClickListener
{

    public static final String TAG = CreditCardPromotionConfirmationFragment.class.getSimpleName();
    private PaymentCardsListFragment.OnPaymentCardInteractionListener onPaymentCardInteractionListener;
    private PaymentCardView mPaymentCardView;
    private OnCardListRequestClickLister onCardListRequestClickLister;
    Button btnAddPromotionCode;
    private boolean isListItemsEditable = false;
    public int deletingPaymentCardId;
    private boolean isDeletingInProgress = false;
    private PaymentCardsCursorAdapter adapter;
    public static final String PROMOTION_FRAGMENT_TAG = "PROMO_FRAGMENT_TAG";
    public static boolean isOnTop = false;

    @Override
    public void onNewPaymentCardClick() {
        onCardListRequestClickLister.onCardListRequestClickLister();
    }

    @Override
    public void onPaymentCardClick(PaymentCard paymentCard)
    {
          onCardListRequestClickLister.onCardListRequestClickLister();
    }

    public interface DataProtocol
    {
        Redemption getRedemption();
        public PaymentCard getPaymentCard();
    }

    public interface OnCreditCardPromotionConfirmationListener
    {
        void onPromotionDone();
        public void onNewPaymentCardClick();
    }

    private OnCreditCardPromotionConfirmationListener promotionListener;
    private DataProtocol dataProtocol;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            promotionListener = (OnCreditCardPromotionConfirmationListener) activity;
            dataProtocol = (DataProtocol) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement OnPromotionConfirmationListener");
        }
        try {
            onPaymentCardInteractionListener = (PaymentCardsListFragment.OnPaymentCardInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            onCardListRequestClickLister = (OnCardListRequestClickLister) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PaymentCardView.OnPaymentCardClickListener");
        }


    }



    private final View.OnClickListener onListEditingClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //  setListItemsEnabled(!isListItemsEditable);
        }
    };

    @Override
    public void onDetach()
    {
        super.onDetach();
        promotionListener = null;
        dataProtocol = null;
        onCardListRequestClickLister = null;
    }


    public static CreditCardPromotionConfirmationFragment newInstance(String pActivityName) {
        CreditCardPromotionConfirmationFragment fragment = new CreditCardPromotionConfirmationFragment();
        Bundle args = new Bundle();
        args.putString(PROMOTION_FRAGMENT_TAG,pActivityName);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_credit_card_promotion_confirmation, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        boolean isPaymentCardCreated = dataProtocol.getPaymentCard().isCreated();

        adapter = new PaymentCardsCursorAdapter(getActivity(), onCreditCardDeleteListener,false);

        btnAddPromotionCode=(Button)view.findViewById(R.id.btnAddPromotionCode);
        if(isPaymentCardCreated) {
            btnAddPromotionCode.setVisibility(View.VISIBLE);
        }
        else{
            btnAddPromotionCode.setVisibility(View.GONE);
        }
        TextView planNameTextView = (TextView) view.findViewById(R.id.plan_name_text_view);
        TextView planStateTextView = (TextView) view.findViewById(R.id.plan_state_text_view);
        TextView planInfoTextView = (TextView) view.findViewById(R.id.plan_info_text_view);
        ImageView logoImageView = (ImageView) view.findViewById(R.id.promo_logo_image_view);
        btnAddPromotionCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                promotionListener.onPromotionDone();
            }
        });




        if (dataProtocol.getRedemption() != null)
        {
            Redemption redemption = dataProtocol.getRedemption();

            if (redemption.getSubscriptions() != null && !redemption.getSubscriptions().isEmpty())
            {
                Subscription subscription = redemption.getSubscriptions().get(0);
                if (!TextUtils.isEmpty(subscription.getState()))
                {
                    planStateTextView.setText(getString(R.string.promotion_state_active));
                    planStateTextView.setText(getString(R.string.promotion_state_plan, subscription.getState()));
                }
            }

            if (redemption.getPromotion() != null)
            {
                Promotion promotion = redemption.getPromotion();
                String planName = promotion.getName();
                String planDescription = promotion.getDescription();
                String creditCardText = promotion.getCreditCardRequiredText();

                int planDuration = promotion.getDuration();

                if (!TextUtils.isEmpty(planName))
                {
                    planNameTextView.setText(planName);
                }
                else
                {
                    planNameTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(planDescription))
                {
                    planStateTextView.setText(planDescription);
                }
                else
                {
                    planStateTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(creditCardText))
                {
                    planInfoTextView.setText(creditCardText);
                }
                else
                {
                    planInfoTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(promotion.getLogo()))
                {
                    String imageUrl = Urls.getRuby(promotion.getLogo());
                    Picasso.with(getActivity()).load(imageUrl).fit().centerInside().into(logoImageView);
                }
                else
                {
                    logoImageView.setVisibility(View.GONE);
                }
            }
            mPaymentCardView = (PaymentCardView) view.findViewById(R.id.payment_card_view);
            final PaymentCardViewController paymentCardController = new PaymentCardViewController();
            mPaymentCardView.showPaymentCard(paymentCardController, dataProtocol.getPaymentCard(),
                    this);
            mPaymentCardView.setClickable(true);


        }

        setTitle(R.string.welcome);
        setLeftButton(null,0);
    }

    @Override
    public void onResume() {
        isOnTop = true;
        super.onResume();
    }

    @Override
    public void onPause() {
        isOnTop = false;
        super.onPause();
    }



    @Override
    protected void onTopBarButtonPressed() {
        if (isDeletingInProgress) {
            Toast.makeText(getActivity(), R.string.wait_while_card_is_deleting, Toast.LENGTH_SHORT).show();
        } else {
            super.onTopBarButtonPressed();
        }
    }

    private final RequestListener<Void> deleteCardRequestListener = new RequestListenerErrorProcessed<Void>() {
        @Override
        public synchronized void onRequestComplete(Response<Void> response, String errorMessage) {
            if (isVisible() && getActivity() != null) {
                setProgressBarVisible(false);
                isDeletingInProgress = false;

                onPaymentCardInteractionListener.onPaymentCardDelete(deletingPaymentCardId);

                if (!TextUtils.isEmpty(errorMessage)) {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            }

        }
    };

    private final PaymentCardsCursorAdapter.OnCreditCardDeleteListener onCreditCardDeleteListener = new
            PaymentCardsCursorAdapter.OnCreditCardDeleteListener() {

                @Override
                public void onCreditCardDelete(int id) {
                    if (!isDeletingInProgress) {
                        setProgressBarVisible(true);

                        final CreditCardDelete deleteCreditCard = CreditCardDelete.getDeleteCreditCard(id);

                        deleteCreditCard.execute(getActivity(), deleteCardRequestListener);
                        deletingPaymentCardId = id;
                        isDeletingInProgress = true;
                    }
                }

                @Override
                public boolean isEditable() {
                    return isListItemsEditable;
                }
            };

    /*private final PaymentCardView.OnPaymentCardClickListener onNewPaymentCardClickListener = new PaymentCardView.OnPaymentCardClickListener()
    {
        @Override
        public void onNewPaymentCardClick()
        {
            *//*if (isListItemsEditable)
                            setListItemsEnabled(false);*//*

            onPaymentCardInteractionListener.onNewPaymentCardClick();
        }

        @Override
        public void onPaymentCardClick(PaymentCard paymentCard)
        {
            *//*if (isListItemsEditable)
                            setListItemsEnabled(false);*//*
            onPaymentCardInteractionListener.onPaymentCardSelect(paymentCard);
        }
    };*/

    public interface OnPaymentCardInteractionListener {
        public void onPaymentCardSelect(PaymentCard paymentCard);

        public void onPaymentCardDelete(int id);

        public void onNewPaymentCardClick();
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    @Override
    public void onClick(View view)
    {
        if (promotionListener != null)
        {
            switch (view.getId())
            {
                case R.id.topbar_right_text_button:
                    promotionListener.onPromotionDone();
                    break;
                default:
                    break;
            }
        }
    }

    public interface OnCardListRequestClickLister {
        public void onCardListRequestClickLister();
    }

}