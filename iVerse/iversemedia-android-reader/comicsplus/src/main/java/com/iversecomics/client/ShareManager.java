package com.iversecomics.client;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;

import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import java.io.IOException;
import java.net.URI;

public class ShareManager {

//	public static ShareManager instance = null;
    public static int METHOD = 111;
    public static final int METHOD_NONE = -1;
    public static final int METHOD_FACEBOOK = 0;
    public static final int METHOD_TWITTER = 1;

    SocialAuthAdapter socialAdapter;
    OnSocialPostListener socialListener;

    Activity context;
    Button sharebutton;
    Comic comic;

    public ShareManager() {
        initSocialAuth();
    }

//	public static ShareManager getInstance() {
//		if (instance == null) {
//			instance = new ShareManager();
//		}
//		return instance;
//	}

    public void initSocialAuth() {
        socialAdapter = new SocialAuthAdapter(new ResponseListener());

        // Add providers
        socialAdapter.addProvider(Provider.FACEBOOK, R.drawable.facebook);
        // added temporary callback url for twitter.
        socialAdapter.addCallBack(Provider.TWITTER, "http://www.iversemedia.com");
        socialAdapter.addProvider(Provider.TWITTER, R.drawable.twitter);
    }

    public void configurePostComic(Activity context, Button sharebutton,
                                   Comic comic, OnSocialPostListener listener) {
        socialListener = listener;
        socialAdapter.enable(sharebutton);
        this.sharebutton = sharebutton;
        this.comic = comic;
        this.context = context;
    }

    public void shareComicVia(Activity ctx, int method, Comic comic, OnSocialPostListener listener) {
        this.comic = comic;
        this.context = ctx;
        socialListener = listener;
        METHOD  = method;
        if (socialAdapter != null) {
            if (method == METHOD_FACEBOOK) {
                socialAdapter.authorize(this.context, Provider.FACEBOOK);
            } else if (method == METHOD_TWITTER) {
                socialAdapter.authorize(this.context, Provider.TWITTER);
            }
        }
    }


    String message;
    Bitmap bitmap;
    URI producturi;

    AsyncPrepareResourceTask resourcePrepareTask = null;

    private boolean prepareResources() throws IOException {
        producturi = ServerConfig.getDefault().getWebProductUri(comic.getComicBundleName());
        Uri imageuri = ServerConfig.getDefault().getLargeImageUri(comic.getImageFileName());
//        if(METHOD == METHOD_TWITTER) {
//            producturi = URI.create("");
//        }
        this.message =  context.getResources().getString(R.string.share_message, comic.getName(),AppConstants.SOCIAL_NETWORKING_APP_NAME)+"\n"+ producturi;

        // load image
        WebBitmapLoader webLoader = new WebBitmapLoader(IverseApplication
                .getApplication().getHttpClient());
        bitmap = webLoader.loadBitmap(URI.create(imageuri.toString()));

        return true;
    }

    /**
     * Listens Response from Library
     */
    private final class ResponseListener implements DialogListener {
        @Override
        public void onComplete(Bundle values) {
            Log.d("SocialAuthAdapter", "Authentication Successful");

            // Get name of provider after authentication
            final String providerName = values
                    .getString(SocialAuthAdapter.PROVIDER);
            Log.d("SocialAuthAdapter", "Provider Name = " + providerName);


            if (providerName.equals(Provider.FACEBOOK.toString()) ||
                    providerName.equals(Provider.TWITTER.toString())) {
                //launch
                if (resourcePrepareTask != null) {
                    return;
                }

                if(METHOD == METHOD_TWITTER) {
                    //Show alert like iOS
                    final Dialog altDialog = new Dialog(context);
                    altDialog.setTitle(context.getString(R.string.title_twitter));
                    altDialog.setContentView(R.layout.twitter_alert);
                    //producturi = URI.create("");

                    new Thread() {
                        @Override
                        public void run() {
                            try

                            {
                                Uri imageuri = ServerConfig.getDefault().getLargeImageUri(comic.getImageFileName());
                                WebBitmapLoader webLoader = new WebBitmapLoader(IverseApplication.getApplication().getHttpClient());
                                 final Bitmap bmp = webLoader.loadBitmap(URI.create(imageuri.toString()));

                                ((Activity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ImageView twitterImg = (ImageView)altDialog.findViewById(R.id.tweet_img);
                                        if(altDialog !=null && altDialog.isShowing())
                                            twitterImg.setImageBitmap(bmp);
                                    }
                                });
                            }

                            catch(Exception e){
                            }
                        }

                    }.start();

                    TextView tweetText_tv = (TextView)altDialog.findViewById(R.id.tweet_title);
                    tweetText_tv.setText(context.getResources().getString(R.string.share_message, comic.getName(),AppConstants.SOCIAL_NETWORKING_APP_NAME)+"\n");

                    Button postTweet_btn = (Button)altDialog.findViewById(R.id.post_tweet);
                    Button cancelTweet_btn = (Button)altDialog.findViewById(R.id.cancel_tweet);

                    postTweet_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            resourcePrepareTask = new AsyncPrepareResourceTask(providerName);
                            resourcePrepareTask.execute();
                            altDialog.dismiss();
                        }
                    });
                    cancelTweet_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            altDialog.dismiss();
                        }
                    });

                    altDialog.show();
                }
                else {
                    resourcePrepareTask = new AsyncPrepareResourceTask(providerName);
                    resourcePrepareTask.execute();
                }

            }
        }

        @Override
        public void onError(SocialAuthError error) {
            Log.d("ShareButton", "Authentication Error: " + error.getMessage());
        }

        @Override
        public void onCancel() {
            Log.d("ShareButton", "Authentication Cancelled");
        }

        @Override
        public void onBack() {
            Log.d("Share-Button", "Dialog Closed by pressing Back Key");
        }

    }

    public void releaseResources() {
        this.sharebutton = null;
        this.context = null;
        this.comic = null;
        if (this.bitmap != null && this.bitmap.isMutable())
            this.bitmap.recycle();
        this.bitmap = null;
        this.message = null;
    }

    // To get status of message after authentication
    private final class MessageListener implements SocialAuthListener<Integer> {
        @Override
        public void onExecute(String arg0, Integer arg1) {
            Integer status = arg1;
            if (socialListener != null) {
                if (status.intValue() == 200 || status.intValue() == 201
                        || status.intValue() == 204) {
                    socialListener.onPosted(true, null);
                } else {
                    socialListener.onPosted(false, null);
                }
            }
            //releaseResources();
        }

        @Override
        public void onError(SocialAuthError e) {
            if (socialListener != null) {
                socialListener.onPosted(false, null);
            }
            //releaseResources();
        }
    }

    public interface OnSocialPostListener {
        abstract void onPosted(boolean success, String provider);
    }


    public class AsyncPrepareResourceTask extends AsyncTask<Void, Void, Boolean> {

        public AsyncPrepareResourceTask(String method) {

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareResources();
            } catch (IOException e) {
                //releaseResources();
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }


        protected void onPostExecute(Boolean result) {
            if (result.booleanValue()) {
                //post
                try {
                    socialAdapter.uploadImageAsync(message,
                            AppConstants.SOCIAL_NETWORKING_APP_NAME + ".jpg",
                            bitmap, 80, new MessageListener());
                } catch (Exception e) {
                    //releaseResources();
                }

                resourcePrepareTask = null;
            }
        }
    }
}
