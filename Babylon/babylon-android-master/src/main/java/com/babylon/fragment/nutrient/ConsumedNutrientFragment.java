package com.babylon.fragment.nutrient;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.BarcodeScannerActivity;
import com.babylon.api.request.NutrientRecentGet;
import com.babylon.api.request.ScannedFoodGet;
import com.babylon.api.request.SearchFoodGet;
import com.babylon.api.request.base.AbsRequest;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.FoodSchema;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.fragment.BaseMonitorMeParamsLoaderFragment;
import com.babylon.model.Food;
import com.babylon.model.Nutrient;
import com.babylon.model.NutrientCategory;
import com.babylon.model.ScannedFood;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.*;
import com.babylon.validator.StringLengthMoreThanValidator;
import com.babylon.validator.ValidationController;
import com.babylon.view.CustomEditText;
import de.timroes.android.listview.EnhancedListView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ConsumedNutrientFragment extends BaseMonitorMeParamsLoaderFragment implements LoaderManager
        .LoaderCallbacks<Cursor>, View.OnClickListener{

    private static final String TAG = Cursor.class.getSimpleName();
    private static final String EXTRA_NUTRIENT_CATEGORY = "EXTRA_NUTRIENT_CATEGORY";

    private static final int FOOD_NAME_MIN_LENGTH = 3;
    private static final int CALORIES_MIN_VALUE = 0;
    private static final int FOOD_LIST_HANDLER_MESSAGE = 3246;

    private CaloriesConsumedCursorAdapter caloriesAdapter;
    private EnhancedListView caloriesListView;

    private CustomEditText caloriesEditText;
    private CustomEditText nutrientNameEditText;

    private TextView foodNumberTextView;

    private Nutrient nutrient;
    private Handler foodSearchHandler;

    private List<Food> undoableFood = new ArrayList<Food>();
    private com.babylon.model.NutrientCategory nutrientCategory;
    private int barcodeFoodResult;

    private ValidationController validationController;

    public static ConsumedNutrientFragment getInstance(long beginOfTheDayInMillis, NutrientCategory category) {
        final Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        b.putSerializable(EXTRA_NUTRIENT_CATEGORY, category);

        final ConsumedNutrientFragment fragment = new ConsumedNutrientFragment();
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        initFoodBroadcastReceiver();
        nutrientCategory = (NutrientCategory) getArguments().getSerializable(EXTRA_NUTRIENT_CATEGORY);

        super.onCreate(savedInstanceState);
    }

    private void initFoodBroadcastReceiver() {
        FoodReceiver foodReceiver = new FoodReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_FOOD_RECEIVE);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(foodReceiver, intentFilter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_consumed_nutrient, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        validationController = new ValidationController();

        initListView(view);
        initNutrientEditText(view);
        initCaloriesEditText(view);

        foodNumberTextView = (TextView) view.findViewById(R.id.text_view_nutrient_quantity);

        final ImageButton addCountImageBtn = (ImageButton) view.findViewById(R.id.btn_plus);
        addCountImageBtn.setOnClickListener(onAddQuantityClickListener);

        final ImageButton minusCountImageBtn = (ImageButton) view.findViewById(R.id.btn_minus);
        minusCountImageBtn.setOnClickListener(onMinusQuantityClickListener);

        final Button foodAddButton = (Button) view.findViewById(R.id
                .btn_add_nutrient);
        foodAddButton.setOnClickListener(this);

        final ImageButton barcodeImageBtn = (ImageButton) view.findViewById(R.id.btn_scan_barcode);
        barcodeImageBtn.setOnClickListener(this);

        final Button searchButton = (Button) view.findViewById(R.id.btn_search);
        searchButton.setOnClickListener(this);

        final String title = nutrientCategory.getTitle();
        setTitle(title);

        foodSearchHandler = getNutrientSearchHandler();
        caloriesAdapter = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setProgressBarVisible(false);
        onLoadedMonitorMeParamsFromServer();

        if (loadingRecentFoodAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
              loadingRecentFoodAsyncTask.execute(dateInMillis);//
        }
    }

    private void initCaloriesEditText(View view) {
        caloriesEditText = (CustomEditText) view.findViewById(R.id.edit_text_calories);
        caloriesEditText.setTextColor(android.R.color.white, android.R.color.white);

        caloriesEditText.addFilter(new MinMaxInputFilter(CALORIES_MIN_VALUE, Integer.MAX_VALUE), true);
        validationController.addValidation(caloriesEditText, null);
    }

    private void initNutrientEditText(View view) {
        nutrientNameEditText = (CustomEditText) view.findViewById(R.id.edit_text_nutrient_name);
        nutrientNameEditText.setTextColor(android.R.color.white, android.R.color.white);

        validationController.addValidation(nutrientNameEditText, new StringLengthMoreThanValidator(null,
                getString(R.string.calories_choose_meal_error_message),
                FOOD_NAME_MIN_LENGTH));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == barcodeFoodResult && resultCode == Activity.RESULT_OK) {
            setProgressBarVisible(true);
            new BarcodeFood().loadScannedFood(data);
        }
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(long dateFrom) {
        setProgressBarVisible(false);
        startLoaderManager();
    }

    @Override
    protected void startLoaderManager() {
        //empty
    }

    private void initListView(View view) {
        caloriesListView = (EnhancedListView) view.findViewById(R.id.fragment_in_calories_list_view);
        caloriesListView.setUndoStyle(EnhancedListView.UndoStyle.SINGLE_POPUP);
        caloriesListView.setDismissCallback(dismissCallback);
        caloriesListView.enableSwipeToDismiss();
        caloriesListView.setUndoHideDelay(getResources().getInteger(R.integer.undo_delay_in_millis));
        caloriesListView.setRequireTouchBeforeDismiss(false);
    }

    @Override
    protected void onTopBarButtonPressed() {
        UIUtils.hideSoftKeyboard(getActivity(), nutrientNameEditText.getEditText());
        super.onTopBarButtonPressed();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.btn_add_nutrient:
                saveFood();
                break;
            case R.id.btn_scan_barcode:
                barcodeFoodResult = 1;
                startActivityForResult(new Intent(getActivity(), BarcodeScannerActivity.class), barcodeFoodResult);
                break;
            case R.id.btn_search:
                search(nutrientNameEditText.getText());
                break;
            default:
                break;
        }
    }

    private boolean search(String searchText) {
        try {
            final String foodName = URLEncoder.encode(searchText, AbsRequest.UTF_8);
            onFoodSearch(foodName);
            return true;
        } catch (UnsupportedEncodingException e) {
            L.e(TAG, e.getMessage(), e);
        }
        return false;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        NutrientCategory category;

        switch (id) {
            case FoodSchema.Query.CURSOR_LOAD_TODAY_FOOD:
                category = (NutrientCategory) getArguments().getSerializable(EXTRA_NUTRIENT_CATEGORY);
                return new CursorLoader(getActivity(),
                        FoodSchema.CONTENT_URI, null,
                        FoodSchema.Query.DAY_SELECTION_BY_CATEGORY,
                        FoodSchema.Query.getDateSelectionArgumentsByCategory(dateInMillis, category),
                        FoodSchema.Query.SORT);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        int id = cursorLoader.getId();
        switch (id) {
            case FoodSchema.Query.CURSOR_LOAD_TODAY_FOOD:

                if (caloriesAdapter == null) {
                    caloriesAdapter = new CaloriesConsumedCursorAdapter(getActivity(), cursor);
                    caloriesListView.setAdapter(caloriesAdapter);
                } else {
                    caloriesAdapter.swapCursor(cursor);
                    caloriesListView.resetDeletedViews();
                }

                break;
            default:
                break;
        }
    }

    public LinearLayout getYesterdayFoodListView(List<Food> foodListResponse) {
        final LinearLayout yesterdayFoodListLayout = new LinearLayout(getActivity());
        yesterdayFoodListLayout.setOrientation(LinearLayout.VERTICAL);
        yesterdayFoodListLayout.removeAllViews();

        View recentSectionView = View.inflate(getActivity(), R.layout.view_recent_list_section, null);
        yesterdayFoodListLayout.addView(recentSectionView);

        for (final Food food : foodListResponse) {
            addNutrientToRecentList(yesterdayFoodListLayout, food);
        }

        return yesterdayFoodListLayout;
    }

    private void addNutrientToRecentList(LinearLayout yesterdayFoodListLayout, Food food) {
        View itemView = View.inflate(getActivity(), R.layout.list_item_calories_burnt, null);
        itemView.setTag(food);
        itemView.setOnClickListener(onSelectYesterdayFoodClickListener);

        TextView titleTextView = (TextView) itemView.findViewById(R.id.list_item_calories_title_text_view);
        TextView amountTextView = (TextView) itemView.findViewById(R.id.list_item_calories_amount_text_view);

        titleTextView.setText(food.getTitle());
        amountTextView.setText(String.valueOf(food.getCalories() * food.getCount()));

        yesterdayFoodListLayout.addView(itemView);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        /* data is not available anymore */
        caloriesAdapter.swapCursor(null);
    }

    private void onFoodSearch(String foodName) {
        if (!App.getInstance().isConnection()) {
            AlertDialogHelper.getInstance().showMessage(getActivity(), R.string.offline_error);
        } else if (validationController.performValidationEditText(nutrientNameEditText)) {
            setProgressBarVisible(true);
            final SearchFoodGet request = SearchFoodGet.newFoodListSearch(foodName);

            request.execute(getActivity(), new RequestListener<ResponseList<Nutrient>>() {
                @Override
                public void onRequestComplete(Response<ResponseList<Nutrient>> response) {
                    if (getActivity() != null) {
                        if (response.isSuccess()) {
                            setProgressBarVisible(false);
                            showFoundFood(response.getData().getList());
                        } else if (!TextUtils.isEmpty(response.getUserResponseError())) {
                            AlertDialogHelper.getInstance().showMessage(getActivity(), response.getUserResponseError());
                        }

                        getActivity().getSupportLoaderManager().destroyLoader(request.hashCode());
                    }
                }
            });
        }
    }

    private void showFoundFood(List<Nutrient> foodList) {
        if (foodSearchHandler != null) {
            if (!foodList.isEmpty()) {
                UIUtils.hideSoftKeyboard(getActivity(), nutrientNameEditText.getEditText());
                Message message = new Message();
                message.what = FOOD_LIST_HANDLER_MESSAGE;
                Bundle arguments = new Bundle();
                arguments.putSerializable(Constants.EXTRA_FOODS, new ArrayList<Nutrient>(foodList));
                message.setData(arguments);
                foodSearchHandler.sendMessage(message);
            } else {
                if (getActivity() != null) {
                    AlertDialogHelper.getInstance().showMessage(getActivity(),
                            R.string.calories_barcode_scan_meal_empty_message);
                }
            }
        }
    }

    private void saveFood() {
        boolean isInputValid = validationController.performValidation();

        if (isInputValid) {
            final Food food = new Food();

            if (nutrient == null) {
                nutrient = new Nutrient();
            }

            nutrient.setFoodFromNutrient(food);

            int foodCount = getPortionCount();
            food.setCount(foodCount);

            Integer calories = Integer.valueOf(caloriesEditText.getText().toString());
            food.setCalories(calories);

            food.setCategory(nutrientCategory.getValue());
            food.setDate(DateUtils.findOutSavingDate(dateInMillis));
            food.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
            food.setTitle(nutrientNameEditText.getText().toString().trim());

            DatabaseOperationUtils.getInstance().operateFood(food,
                    getActivity().getContentResolver(), AddNutrientAsyncQueryListener);

            cleanPrevNutrientData();

            UIUtils.hideSoftKeyboard(getActivity(), nutrientNameEditText.getEditText());
        }
    }

    private void cleanPrevNutrientData() {
        caloriesEditText.setText("");
        foodNumberTextView.setText(R.string.one);
        nutrientNameEditText.setText("");
        nutrient = null;
    }

    private int getPortionCount() {
        final String nutrientQuantity = foodNumberTextView.getText().toString();
        final int foodCount = Integer.valueOf(nutrientQuantity);

        return foodCount;
    }

    private void deleteFood(Cursor cursor) {
        if (!cursor.isClosed()) {
            final Food food = new Food();
            food.createFromCursor(cursor);
            food.setSyncWithServer(SyncUtils.SYNC_TYPE_DELETE);
            DatabaseOperationUtils.getInstance().operateFood(food, getActivity().getContentResolver());
        }
    }

    private void dismissFood(Cursor c) {
        final Food food = new Food();
        food.createFromCursor(c);

        undoableFood.add(food);

        deleteFood(c);
    }

    private Handler getNutrientSearchHandler() {
        return new Handler() {
            @Override
            public void handleMessage(Message message) {
                if (message.what == FOOD_LIST_HANDLER_MESSAGE && isVisible()) {
                    SearchNutrientsFragment chooseMealFragment = new SearchNutrientsFragment();
                    chooseMealFragment.setArguments(message.getData());
                    startFragment(chooseMealFragment);
                }
            }
        };
    }

    /* Local receiver for fragment communication */
    private class FoodReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null
                    && intent.hasExtra(Constants.EXTRA_FOOD)
                    && isVisible()) {
                final Nutrient food = (Nutrient) intent.getExtras().get(Constants.EXTRA_FOOD);
                ConsumedNutrientFragment.this.nutrient = food;
                if (food.getName() != null) {
                    nutrientNameEditText.setText(food.getName());
                }
                if (food.getKilocalories() != null) {
                    caloriesEditText.setText(food.getKilocalories().toString());
                }
            }
        }
    }

    private final EnhancedListView.OnDismissCallback dismissCallback = new EnhancedListView.OnDismissCallback() {
        private String title;

        @Override
        public EnhancedListView.Undoable onDismiss(EnhancedListView listView, int position) {
            final Cursor c = caloriesAdapter.getCursor();

            if (c.moveToPosition(position)) {
                title = c.getString(PhysicalActivitySchema.Query.TITLE);

                dismissFood(c);
                return undoable;
            } else {
                return null;
            }
        }

        private final EnhancedListView.Undoable undoable = new EnhancedListView.Undoable() {

            @Override
            public void undo() {
                int lastIndex = undoableFood.size() - 1;
                DatabaseOperationUtils.getInstance().operateFood(undoableFood.get(lastIndex),
                        getActivity().getContentResolver());
                undoableFood.remove(lastIndex);
            }

            @Override
            public void discard() {
                super.discard();
                int lastIndex = undoableFood.size() - 1;
                undoableFood.remove(lastIndex);
            }

            @Override
            public String getTitle() {
                return getString(R.string.undo_title, title);
            }
        };
    };

    private final DatabaseOperationUtils.AsyncQueryListener AddNutrientAsyncQueryListener = new DatabaseOperationUtils
            .AsyncQueryListener() {
        @Override
        public void onQueryComplete(int token, Object cookie, Cursor cursor) {

        }

        @Override
        public void onChangeComplete(int token, Object cookie, int result) {

        }

        @Override
        public void onInsertComplete(int token, Object cookie, Uri uri) {
            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_FOOD, SyncAdapter.SYNC_PUSH);
        }
    };

    private final View.OnClickListener onAddQuantityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int curNumber = Integer.valueOf(foodNumberTextView.getText().toString());
            final int newNumber = curNumber + 1;

            if (newNumber < Integer.MAX_VALUE) {
                foodNumberTextView.setText(String.valueOf(newNumber));
            }
        }
    };

    private final View.OnClickListener onSelectYesterdayFoodClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final Food food = (Food) v.getTag();

            final Nutrient yesterdayNutrient = new Nutrient();
            yesterdayNutrient.setId(food.getId());
            yesterdayNutrient.setName(food.getTitle());
            yesterdayNutrient.setKilocalories(food.getCalories());
            yesterdayNutrient.setPortionDetails(food.getPortionDetails());
            yesterdayNutrient.setPortionWeight(food.getPortionWeight());
            yesterdayNutrient.setAddPortionWeight(food.getAddPortionWeight());

            ConsumedNutrientFragment.this.nutrient = yesterdayNutrient;
            if (yesterdayNutrient.getName() != null) {
                nutrientNameEditText.setText(yesterdayNutrient.getName());
            }
            if (yesterdayNutrient.getKilocalories() != null) {
                caloriesEditText.setText(yesterdayNutrient.getKilocalories().toString());
            }
            if (nutrient.getCount() != null) {
                foodNumberTextView.setText(nutrient.getCount().toString());
            }
        }
    };


    private final View.OnClickListener onMinusQuantityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int curNumber = Integer.valueOf(foodNumberTextView.getText().toString());
            final int newNumber = curNumber - 1;

            if (newNumber > 0) {
                foodNumberTextView.setText(String.valueOf(newNumber));
            }
        }
    };

    private class BarcodeFood {
        private ScannedFoodGet request;

        public void loadScannedFood(Intent data) {
            final String barcode = data.getStringExtra(Constants.EXTRA_BARCODE);

            request = ScannedFoodGet.init(barcode);
            request.execute(getActivity(), requestListener);
        }

        private void showBarcodeMessage(String message) {
            if (TextUtils.isEmpty(message)) {
                AlertDialogHelper.getInstance().showMessage(getActivity(),
                        getString(R.string.calories_barcode_scan_meal_empty_message));
            } else {
                AlertDialogHelper.getInstance().showMessage(getActivity(), message);
            }
        }

        public List<Nutrient> convert(List<ScannedFood> foods) {
            final List<Nutrient> nutrients = new ArrayList<Nutrient>();

            for (ScannedFood food : foods) {
                final Nutrient scannedNutrient = new Nutrient();
                scannedNutrient.setName(food.getName());
                scannedNutrient.setKilocalories(Integer.valueOf(food.getKcal()));
                nutrients.add(scannedNutrient);
            }

            return nutrients;
        }

        private void processFoodResponse(Response<ResponseList<ScannedFood>> response) {
            final ResponseList<ScannedFood> responseData = response.getData();
            if (responseData == null) {
                showBarcodeMessage(getString(R.string.some_error));
            } else {
                final List<ScannedFood> scannedFood = responseData.getList();
                final List<Nutrient> nutrients = convert(scannedFood);
                showFoundFood(nutrients);
            }
        }

        private final RequestListener<ResponseList<ScannedFood>> requestListener = new
                RequestListener<ResponseList<ScannedFood>>() {
                    @Override
                    public void onRequestComplete(Response<ResponseList<ScannedFood>> response) {
                        final List<com.babylon.api.request.base.Error> errors = response.getErrors();
                        if (errors.isEmpty()) {
                            processFoodResponse(response);
                        } else {
                            final String message = errors.get(0).getUserMessage();
                            showBarcodeMessage(message);
                        }

                        setProgressBarVisible(false);

                        getActivity().getSupportLoaderManager().destroyLoader(request.hashCode());
                    }
                };
    }

    private final AsyncTask<Long, Void, Void> loadingRecentFoodAsyncTask = new AsyncTask<Long, Void, Void>() {

        @Override
        protected void onPreExecute() {
            setProgressBarVisible(true);
        }

        @Override
        protected Void doInBackground(Long... params) {
            final Long dateInMillis = params[0];
            NutrientRecentGet.newNutrientRecentGet(dateInMillis, nutrientCategory).execute(getActivity(),
                    recentFoodRequestListener);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    };

    private final RequestListener<ResponseList<Food>> recentFoodRequestListener
            = new RequestListener<ResponseList<Food>>() {
        @Override
        public void onRequestComplete(Response<ResponseList<Food>> response) {
            if (getActivity() != null && response != null && response.isSuccess()) {
                final List<Food> food = response.getData().getList();
                final LinearLayout yesterdayFoodFooterView = getYesterdayFoodListView(food);

                if (yesterdayFoodFooterView.getChildCount() > 1) {

                        caloriesListView.addFooterView(yesterdayFoodFooterView, null, false);

                }
            }
            getLoaderManager().initLoader(FoodSchema.Query.CURSOR_LOAD_TODAY_FOOD, null,
                    ConsumedNutrientFragment.this);
            setProgressBarVisible(false);
        }
    };
}
