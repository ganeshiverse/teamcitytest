package com.iversecomics.client.store.tasks;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchPublishersTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(FetchPublishersTask.class);
    private final String listId;
    public static final String PUBLISHER_UPDATED = "publisherupdated";
    private Context context;

    public FetchPublishersTask(ComicStore comicStore, Context context) {
        super(comicStore);
        this.listId = ListID.PUBLISHERS;
        this.context = context;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchPublishersTask other = (FetchPublishersTask) obj;
        if (listId == null) {
            if (other.listId != null) {
                return false;
            }
        } else if (!listId.equals(other.listId)) {
            return false;
        }
        return true;
    }


    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getPublishersList();
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            ResponseParser parser = new ResponseParser();
            parser.parsePublishers(json, updater);

            server.updateFreshness(listId, Time.HOUR * 3);

            final Intent intent = new Intent(PUBLISHER_UPDATED);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update publishers list");
        }
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listId == null) ? 0 : listId.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(listId);
    }
}
