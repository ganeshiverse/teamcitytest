package com.babylon.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import com.babylon.R;
import com.babylon.activity.TermsAndConditionsActivity;

public class TermsAndConditionsTextView extends TypefaceTextView {

    public TermsAndConditionsTextView(Context context) {
        super(context);
        initView();
    }

    public TermsAndConditionsTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    /*public TermsAndConditionsTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }*/

    private void initView() {
        setText(R.string.terms_and_condition_underline);
        setOnClickListener(onTermsAndConditionsClickListener);
    }

    private final OnClickListener onTermsAndConditionsClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            getContext().startActivity(new Intent(getContext(), TermsAndConditionsActivity.class));
        }
    };
}
