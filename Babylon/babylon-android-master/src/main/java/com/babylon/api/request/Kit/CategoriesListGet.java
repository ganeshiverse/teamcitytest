package com.babylon.api.request.Kit;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.babylon.App;
import com.babylon.Config;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Kit.Category;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.CommonHelper;
import com.babylon.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoriesListGet {

    public interface KitCategoriesAPItListener {
        public void onGetResponse(ArrayList<Category> categories);

    }
    private static String TAG  = CategoriesListGet.class.getSimpleName();


    protected String getUrl() {
        return Config.RUBY_SERVER_URL.concat(Urls.CATEGORIES);
    }


    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }


    private void getCategories() {

        final List<Category> CategoryList = new ArrayList<Category>();
        String mUri = CommonHelper.getUrl(Urls.CATEGORIES);
        JsonArrayRequest mArrayReq = new JsonArrayRequest(mUri, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String jsonString = response.toString();



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());


            }
        }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(getAuthHeader().getName(), getAuthHeader().getValue());
                return headers;
            }

        };

        if (App.getInstance().isConnection()) {
            mArrayReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(mArrayReq);
        }


    }

}
