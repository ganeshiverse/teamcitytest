package com.babylon.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.broadcast.SyncFinishedBroadcast;
import com.babylon.model.Patient;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;

public class ShopProfileActivity extends ProfileActivity {

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, ShopProfileActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.babylon.ACTION_SYNC_FINISHED");
        registerReceiver(syncFinishedBroadcastReceiver, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(syncFinishedBroadcastReceiver);
    }

    @Override
    public ProfileField[] getFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.HEIGHT,
                ProfileField.WEIGHT,
                ProfileField.GENDER,
                ProfileField.DATE_OF_BIRTH};
    }

    @Override
    public ProfileField[] getRequiredFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.GENDER};
    }

    @Override
    public void setTopBar() {
        setTitle(getString(R.string.categories_list_fragment_title));
        initializeTopBar();
    }

    @Override
    public int getLayoutResID() {
        return R.layout.activity_shop_profile;
    }

    @Override
    public int getBackgroundResId() {
        return R.color.pink_top_bar;
    }

    @Override
    public void onProfileSent() {
        SyncAdapter.performSyncAndPullReport(Constants.Sync_Keys.SYNC_PATIENT,
                SyncAdapter.SYNC_PUSH);
        setProgressBarVisible(true);
    }

    public static boolean isProfileValid() {
        final Patient patient = App.getInstance().getPatient();

        boolean result = true;

        if (TextUtils.isEmpty(patient.getGender())) {
            result = false;
        }

        return result;
    }

    private final BroadcastReceiver syncFinishedBroadcastReceiver = new SyncFinishedBroadcast() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            final int action = intent.getIntExtra(SyncAdapter.KEY_SYNC_ACTION, -1);
            final String type = intent.getStringExtra(SyncAdapter.KEY_SYNC_TYPE);

            if ((type.equals(Constants.Sync_Keys.SYNC_PATIENT)
                    && (action == SyncAdapter.SYNC_BOTH || action == SyncAdapter.SYNC_PUSH))) {
                finish();
                ShopCategoriesActivity.startActivity(ShopProfileActivity.this);
               // MonitorMeActivity.startActivity(ShopProfileActivity.this);
            }
        }
    };
}
