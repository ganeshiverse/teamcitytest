package com.babylon.api.request;


import com.babylon.BaseConfig;
import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;
import com.babylon.model.FileEntry;
import com.babylon.utils.L;
import com.parse.entity.mime.MultipartEntity;
import com.parse.entity.mime.content.FileBody;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUploadPost extends AbsRequestPost<FileEntry, FileEntry.File> {
    @Override
    protected String getUrl() {
        return Urls.get(Urls.UPLOAD_FILE_POST);
    }

    @Override
    public AbsRequestPost<FileEntry, FileEntry.File> withEntry(FileEntry fileEntry) {
        MultipartEntity multipartEntity = new MultipartEntity();

        new File(BaseConfig.CACHE_DIR).mkdirs();
        final File file = new File(fileEntry.getFilePath());
        try {
            file.createNewFile();
            final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(fileEntry.getData());
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }

        FileBody fileBody = new FileBody(file);
        multipartEntity.addPart("file", fileBody);
        getHttpRequest().setEntity(multipartEntity);
        return this;
    }

}
