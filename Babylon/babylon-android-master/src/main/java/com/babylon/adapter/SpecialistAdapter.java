package com.babylon.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.model.Specialist;
import com.babylon.utils.FontUtils;

import java.util.List;

/**
 * Shows menu items
 */
public class SpecialistAdapter extends BaseAdapter {
    public static final int TEXT_SIZE = 16;

    private Context context;
    private List<Specialist> specialists;

    public SpecialistAdapter(Context context, List<Specialist> specialists) {
        this.context = context;
        this.specialists = specialists;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Specialist getItem(int position) {
        return specialists.get(position);
    }

    @Override
    public int getCount() {
        return specialists.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Specialist specialist = specialists.get(position);

        if (convertView == null) {
            convertView = new TextView(context);
        }

        TextView textView = (TextView) convertView;
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE);
        int margin = context.getResources().getDimensionPixelSize(R.dimen.activity_margin);
        textView.setPadding(margin, margin, margin, margin);
        textView.setText(specialist.isCategory() ? specialist.getSpecialism() : specialist.getName());
        textView.setTypeface(FontUtils.getTypeface(specialist.isCategory() ?
                FontUtils.Font.RobotoBold : FontUtils.Font.RobotoLight));
        textView.setBackgroundColor(context.getResources().getColor(specialist.isSelected()
                && !specialist.isCategory() ?
                R.color.blue_top_bar : R.color.white));

        return convertView;
    }
}