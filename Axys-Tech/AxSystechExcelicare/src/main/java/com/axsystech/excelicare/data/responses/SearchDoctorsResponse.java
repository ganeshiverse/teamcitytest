package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SomeswarReddy on 08/12/2015.
 */
public class SearchDoctorsResponse extends BaseResponse implements Serializable {

    @SerializedName("TotalRecordCount")
    private String totalRecordCount;

    @SerializedName("data")
    private ArrayList<DoctorDetails> doctorDetailsAL;

    public String getTotalRecordCount() {
        return totalRecordCount;
    }

    public ArrayList<DoctorDetails> getDoctorDetailsAL() {
        return doctorDetailsAL;
    }

    public class DoctorDetails implements Serializable {

        @SerializedName("AdditionalInfo")
        private String additionalInfo;

        @SerializedName("ClinicName")
        private String clinicName;

        @SerializedName("ClinicianId")
        private String clinicianId;

        @SerializedName("ClinicianName")
        private String clinicianName;

        @SerializedName("ClinicianUserID")
        private String ClinicianUserID;

        @SerializedName("Email")
        private String email;

        @SerializedName("Experience")
        private String experience;

        @SerializedName("Fee")
        private String fee;

        @SerializedName("IsFavorite")
        private String isFavorite;

        @SerializedName("Likes")
        private String likes;

        @SerializedName("Location")
        private String location;

        @SerializedName("Message")
        private String message;

        @SerializedName("Photo")
        private String photo;

        @SerializedName("Qualification")
        private String qualification;

        @SerializedName("Rating")
        private String rating;

        @SerializedName("Speciality")
        private String speciality;

        @SerializedName("URL")
        private String URL;

        public String getAdditionalInfo() {
            return additionalInfo;
        }

        public String getClinicName() {
            return clinicName;
        }

        public String getClinicianId() {
            return clinicianId;
        }

        public String getClinicianName() {
            return clinicianName;
        }

        public String getClinicianUserID() {
            return ClinicianUserID;
        }

        public String getEmail() {
            return email;
        }

        public String getExperience() {
            return experience;
        }

        public String getFee() {
            return fee;
        }

        public String getIsFavorite() {
            return isFavorite;
        }

        public void setIsFavorite(String isFavorite) {
            this.isFavorite = isFavorite;
        }

        public String getLikes() {
            return likes;
        }

        public String getLocation() {
            return location;
        }

        public String getMessage() {
            return message;
        }

        public String getPhoto() {
            return photo;
        }

        public String getQualification() {
            return qualification;
        }

        public String getRating() {
            return rating;
        }

        public String getSpeciality() {
            return speciality;
        }

        public String getURL() {
            return URL;
        }
    }
}