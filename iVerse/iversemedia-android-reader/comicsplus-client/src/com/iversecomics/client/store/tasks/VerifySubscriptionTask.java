package com.iversecomics.client.store.tasks;

import android.content.Context;

import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;


/**
 * Submits user's subscription purchase info to iVerse server.
 * Use immediately after a subscription has been purchased from the Google Play store
 * so that server can track subscription status.
 */
public class VerifySubscriptionTask extends ComicStoreTask {
	private static final Logger LOG = LoggerFactory.getLogger(VerifySubscriptionTask.class);
    private String subscriptionProductSku;
	private String deviceId;
	private String installId;
	private String receiptData;


    public VerifySubscriptionTask(Context context, ComicStore comicStore, String subscriptionProductSku, String installId, String receiptData) {
        super(comicStore);
        this.subscriptionProductSku = subscriptionProductSku;
        this.deviceId = AndroidInfo.getUniqueDeviceID(context);
        this.installId = installId;
        this.receiptData = receiptData;
    }

	@Override
	public void execTask() {
		try {
			final JSONObject payload = new JSONObject();
            payload.put("productId", subscriptionProductSku);
            payload.put("deviceId", deviceId);
            payload.put("installId", installId);
            payload.put("receiptData", receiptData);

            ComicStore.ServerApi api = this.comicStore.getServerApi();
            JSONObject jsonResp = api.verifySubscription(payload);

            final String status = jsonResp.optString("status");

            if (status.equals("ok"))
                LOG.info("Subscription info submitted to iVerse server");
            else
                LOG.error("Error submitting subscription status to server: %s", status);

		} catch (Exception e) {
			LOG.warn(e, "Error submitting subscription status to server: " + e.getLocalizedMessage() );
		}
	}

    /* TODO: joliver 12/30/2013
    This is how the iOS version parses the server response:

    #pragma mark - Subscription Related
- (void)handleServerResponse:(NSDictionary*)resultsDictionary
{
    //Read the existing subscription state
    SubscriptionStatus lastStatus = [[NSUserDefaults standardUserDefaults] integerForKey:kLastSubscriptionStateKey];

    //Get the new status
    SubscriptionStatus newStatus = lastStatus;

    //Check for handle-able errors
    NSDictionary* errorDictionary = resultsDictionary[@"error"];
    if (errorDictionary != nil)
    {
        int errorCode = [errorDictionary[@"errorCode"] intValue];
        switch (errorCode)
        {
            case kInAppPurchaseNoError:
            {
                break;
            }
            case kInAppPurchaseErrorExpired:
            {
                //Subscription has expired
                DebugLog(@"Server reports expired subscription");
                newStatus = SubscriptionStatusExpired;
                break;
            }
            case kInAppPurchaseErrorServerDown:
            {
                //Display a message to the user
                DebugLog(@"iTunes Server Unavailable");
                [[ErrorDialog sharedInstance] displayErrorMessage:NSLocalizedString(@"Your subscription could not be verified at this time.  Please try again later.",@"Message for failure to validate subscription (server down)") title:NSLocalizedString(@"Unable to Verify Subscription",@"Title for failure to validate subscription")];
                return;
            }
            default:
            {
                DebugLog(@"Server reported unhandle-able error");
                NSString* errorMessage = NSLocalizedString(@"A server error prevented the validation of your subscription.  Click Restore iTunes Purchases in the Settings screen to retry.",@"Message for failure to validate subscription (other)");
#ifndef BUILD_MODE_APPSTORE
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"\n:%@", errorDictionary[@"errorMessage"]]];
#endif

                [[ErrorDialog sharedInstance] displayErrorMessage:errorMessage title:NSLocalizedString(@"Unexpected Server Error",@"Title for unexpected server error")];
                return;
            }
        }
    }
    else
    {
        //No error, so confirm the new status
        if ([resultsDictionary[@"status"] isEqualToString:@"ok"])
        {
            newStatus = SubscriptionStatusCurrent;
        }
    }

    //Ok, if the new status mismatches the old status, tell the user
    if (lastStatus != newStatus)
    {
        //Subscribed?
        if (newStatus == SubscriptionStatusCurrent)
        {
            DebugLog(@"Subscription purchase successful!");
            [[ErrorDialog sharedInstance] displayErrorMessage:NSLocalizedString(@"Your subscription is now active.  Enjoy!",@"Message for subscription active") title:NSLocalizedString(@"Subscription Activated",@"Title for subscription active")];
        }

        //Expired?
        if (newStatus == SubscriptionStatusExpired)
        {
            DebugLog(@"Subscription has expired!");
            [[ErrorDialog sharedInstance] displayErrorMessage:NSLocalizedString(@"Your subscription has expired.", @"Message got subscription expired") title:NSLocalizedString(@"Subscription Expired",@"Title for subscription expired")];
        }
    }

    //Write the new status
    [[NSUserDefaults standardUserDefaults] setInteger:newStatus forKey:kLastSubscriptionStateKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //Force update the server
    [self fetchConfigForced:YES];
}


     */

}
