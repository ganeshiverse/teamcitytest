package com.iversecomics.client.net;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

public class TrustAllManager implements X509TrustManager {

    @Override
    public void checkClientTrusted(X509Certificate[] cert, String authType) throws CertificateException {
        /* trust all */
    }

    @Override
    public void checkServerTrusted(X509Certificate[] cert, String authType) throws CertificateException {
        /* trust all */
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        /* no accepted issuers */
        return null;
    }

}
