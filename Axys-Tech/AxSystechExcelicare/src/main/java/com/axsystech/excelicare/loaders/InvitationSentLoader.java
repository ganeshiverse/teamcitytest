package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.InvitationReceivedDataResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 8/28/2015.
 */
public class InvitationSentLoader extends DataAsyncTaskLibLoader<InvitationReceivedDataResponse> {

    private String token;
    private String LMD;
    private String filter;
    private String criteria;
    private int pageIndex;
    private int pageSize;
    private String orderby;


    public InvitationSentLoader(Context context, String token, String LMD, String filter,String criteria,int pageIndex,int pageSize,String orderby) {
        super(context);
        this.token = token;
        this.LMD = LMD;
        this.filter = filter;
        this.criteria = criteria;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.orderby =orderby;
    }

    @Override
    protected InvitationReceivedDataResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCircleInvitaionSentContent(token, LMD, filter,criteria,pageIndex,pageSize,orderby);
    }
}
