package com.iversecomics.client.downloads;

import java.util.HashMap;
import java.util.Map;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.RemoteViews;

import com.iversecomics.client.R;
import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DownloadNotificationMonitor implements DownloadMonitor {
    private static class Progress {
        Uri    uri;
        String title;
        long   totalBytes;
        long   currentBytes;
    }

    private static final Logger LOG         = LoggerFactory.getLogger(DownloadNotificationMonitor.class);

    private static final int    ONGOING_ID  = 1;
    private static final int    FINISHED_ID = 2;
    private Context             context;
    private String              packageName;
    private NotificationManager notifMgr;
    private Map<Uri, Progress>  activeMap;

    public DownloadNotificationMonitor(Context context) {
        this.context = context;
        this.packageName = context.getPackageName();
        this.notifMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        this.activeMap = new HashMap<Uri, DownloadNotificationMonitor.Progress>();
    }

    @Override
    public synchronized void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        Progress prog = activeMap.get(uri);
        if (prog == null) {
            prog = new Progress();
            prog.title = title;
        }
        prog.uri = uri;
        prog.totalBytes = totalBytes;
        activeMap.put(uri, prog);
        updateOngoingNotification();
    }

    @Override
    public synchronized void downloadFailure(Uri uri, DownloadException failure) {
        Progress prog = activeMap.get(uri);
        if (prog != null) {
            activeMap.remove(uri);
            notifyComplete(prog, "Failed to download");
        }
        updateOngoingNotification();
    }

    @Override
    public synchronized void downloadProgress(Uri uri, long currentByte) {
        //synchronized (activeMap) {
            Progress prog = activeMap.get(uri);
            if (prog == null) {
                return;
            }
            prog.currentBytes = currentByte;
            activeMap.put(uri, prog);
        //}
        updateOngoingNotification();
    }

    @Override
    public synchronized void downloadState(Uri uri, DownloadStatus state, String message) {
        if (state.isCompleted()) {
            Progress prog = activeMap.get(uri);
            if (prog != null) {
                activeMap.remove(uri);
                //notifyComplete(prog, "Download complete");
            }
            updateOngoingNotification();
        }
    }

    private void notifyComplete(Progress prog, String status) {
        RemoteViews contentView = new RemoteViews(packageName, R.layout.notification_download);
        contentView.setTextViewText(R.id.name, prog.title);
        contentView.setTextViewText(R.id.status, status);
        contentView.setViewVisibility(R.id.progress_bar, View.GONE);

        // Create notification
        Notification notification = new Notification();
        notification.icon = R.drawable.icon;
        notification.when = System.currentTimeMillis();
        notification.contentView = contentView;

        // TODO: this will need to be moved to the comicsplus project for visibility to MainActivity
//        Intent intent = new Intent(context, MainActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
//        notification.contentIntent = contentIntent;

        // Present notification
        String tag = prog.uri.getLastPathSegment();
        notifMgr.notify(tag, FINISHED_ID, notification);
    }

    private void updateOngoingNotification() {
        boolean isactive = false;
        int count = 0;
        int max = 0;
        int progress = 0;
        StringBuilder titles = new StringBuilder();
        synchronized (activeMap) {
            for (Progress prog : activeMap.values()) {
                isactive = true;
                count++;
                max += prog.totalBytes;
                progress += prog.currentBytes;
                if (titles.length() > 0) {
                    titles.append(", ");
                }
                titles.append(prog.title);
            }
        }

        LOG.debug("Ongoing Notification: %,d of %,d (isactive=%b)", progress, max, isactive);

        if (!isactive) {
            notifMgr.cancel(ONGOING_ID);
            return;
        }

        RemoteViews contentView = new RemoteViews(packageName, R.layout.notification_download);
        contentView.setTextViewText(R.id.name, "Downloading Comics...");
        //String status = String.format("(%d comic%s) %s", count, (count > 1) ? "s" : "", titles);
        String status = titles.toString();
        contentView.setTextViewText(R.id.status, status);
        contentView.setProgressBar(R.id.progress_bar, max, progress, false);

        // Create notification
        Notification notification = new Notification();
        notification.icon = R.drawable.icon;
        notification.when = System.currentTimeMillis();
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.contentView = contentView;

        // TODO: this will need to be moved to the comicsplus project for visibility to MainActivity
//        Intent intent = new Intent(context, MyComicsActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
//        notification.contentIntent = contentIntent;

        // Present notification
        notifMgr.notify(ONGOING_ID, notification);
    }
}
