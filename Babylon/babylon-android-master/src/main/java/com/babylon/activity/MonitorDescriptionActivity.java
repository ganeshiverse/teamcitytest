package com.babylon.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.database.schema.MonitorCategorySchema;
import com.babylon.enums.MonitorCategory;
import com.babylon.model.Icon;
import com.babylon.model.MonitorMeCategory;
import com.babylon.utils.UIUtils;
import com.babylon.view.TopBarInterface;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MonitorDescriptionActivity extends BaseActivity
implements LoaderManager.LoaderCallbacks<Cursor> {

    private TextView titleTextView;
    private ImageView descriptionImageView;
    private TextView descriptionTextView;
    private LinearLayout medicalKitLayout;
    private ImageView medicalKitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_me_description);

        titleTextView = (TextView) findViewById(R.id.name_text_view);
        descriptionImageView = (ImageView) findViewById(R.id.description_image_view);
        descriptionTextView = (TextView) findViewById(R.id.description_text_view);
        medicalKitLayout = (LinearLayout) findViewById(R.id.order_linear_layout);
        medicalKitButton = (ImageView) findViewById(R.id.medical_kit_image_view);

        final ViewGroup container = (ViewGroup)findViewById(R.id.layout_container);
        container.setOnClickListener(onContainerClickListener);

        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String categoryId = extras.getString(Keys.MONITOR_CATEGORY_ID);
                if (categoryId != null && !TextUtils.isEmpty(categoryId)) {
                    initializeTopBar();
                    getSupportLoaderManager().initLoader(
                            MonitorCategorySchema.Query.CURSOR_LOAD_CATEGORY, extras, this);
                }
            }
        }

    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }

    private void showCategory(MonitorMeCategory category) {
        String name = category.getName();
        String description = category.getDescription();
        List<Icon> icons = category.getIconsDescription();
        String categoryId = category.getId();

        if (name != null && !TextUtils.isEmpty(name)) {
            setTitle(name);
            titleTextView.setText(name);
        }

        if (description != null && !TextUtils.isEmpty(description)) {
            descriptionTextView.setText(description);
        }

        if (categoryId != null && !TextUtils.isEmpty(categoryId)) {
            boolean isMedicalKitButtonExist = MonitorCategory.isMedicalKitExist(categoryId);
            if (isMedicalKitButtonExist) {
                medicalKitLayout.setVisibility(View.VISIBLE);
                medicalKitButton.setOnClickListener(onKitClickListener);
            }
        }

        if (icons != null && !icons.isEmpty()) {
            String imageUrl = UIUtils.getImageUrl(icons, this);
            if (imageUrl != null) {
                Picasso.with(this).load(imageUrl).into(descriptionImageView);
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle arguments) {
        final String categoryName = arguments.getString(Keys.MONITOR_CATEGORY_ID);

        if (!TextUtils.isEmpty(categoryName)) {
            final String selectionArgs[] = new String[]{
                    categoryName
            };
            return new CursorLoader(MonitorDescriptionActivity.this,
                    MonitorCategorySchema.CONTENT_URI,
                    null,
                    MonitorCategorySchema.Query.CATEGORY_SELECTION,
                    selectionArgs, null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            MonitorMeCategory category = new MonitorMeCategory();
            category.createFromCursor(cursor);
            showCategory(category);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        /* empty */
    }

    private final View.OnClickListener onContainerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener onKitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MonitorDescriptionActivity.this, ShopCategoriesActivity.class);
            startActivity(intent);
        }
    };
}
