package com.iversecomics.client.refresh;

import java.util.List;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class MessageTask extends Task {
    private static final Logger LOG = LoggerFactory.getLogger(MessageTask.class);
    private String              message;
    private List<String>        out;

    public MessageTask(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MessageTask other = (MessageTask) obj;
        if (message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!message.equals(other.message)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            // Fake taking some time.
            Thread.sleep(250);
        } catch (InterruptedException ignore) {
            /* ignore */
        }
        out.add(message);
        LOG.debug("Added message: \"%s\"", message);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        return result;
    }

    public void setOut(List<String> out) {
        this.out = out;
    }

    @Override
    public String toString() {
        return String.format("%s@%X - [%s]", this.getClass().getName(), this.hashCode(), this.message);
    }
}