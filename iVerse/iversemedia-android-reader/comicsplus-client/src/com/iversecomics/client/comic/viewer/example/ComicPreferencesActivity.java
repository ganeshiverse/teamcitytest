package com.iversecomics.client.comic.viewer.example;

import android.os.Bundle;

import com.iversecomics.client.comic.viewer.AbstractPreferencesActivity;
import com.iversecomics.client.comic.viewer.error.ErrorReporter;

/**
 * Entry point for the ComicPreferences activity.
 */
public class ComicPreferencesActivity extends AbstractPreferencesActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
        } catch (Throwable t) {
            ErrorReporter.reportException(this, t);
        }
    }

    @Override
    protected void save() {
        try {
            super.save();
        } catch (Throwable t) {
            ErrorReporter.reportException(this, t);
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
        } catch (Throwable t) {
            ErrorReporter.reportException(this, t);
        }
    }
}
