package com.iversecomics.client.util;

import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class UIUtil {
    public static void setTextView(View view, int viewId, String text) {
        TextView txtView = (TextView) view.findViewById(viewId);
        txtView.setText(text);
    }

    public static void setRating(View view, int ratingId, float val) {
        RatingBar rating = (RatingBar) view.findViewById(ratingId);
        rating.setRating(val);
    }

    public static void setButtonFaceText(View view, int viewId, String text) {
        Button button = (Button) view.findViewById(viewId);
        button.setText(text);
    }

    public static int dpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static void makeButton(final View view) {
        final ColorFilter filter = new LightingColorFilter(0xA0A0A0A0, 0);
        if (view instanceof ImageView) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        ((ImageView) view).setColorFilter(filter);
                    } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                        ((ImageView) view).setColorFilter(null);
                    }
                    return false;
                }
            });
        } else {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Drawable background = v.getBackground();
                        if (background != null) {
                            background.setColorFilter(filter);
                        }
                    } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                        Drawable background = v.getBackground();
                        if (background != null) {
                            background.setColorFilter(null);
                        }
                    }
                    return false;
                }
            });
        }
    }

}
