package com.iversecomics.client.account;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.json.JSONArray;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComicStoreLocatorActivity extends ListActivity {

    private class ComicStoreLocatorResponseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {

            progressDialog.dismiss();

            final String action = intent.getAction();
            if (ComicStoreLocatorTask.FINDSTORE_RESPONSE.equals(action)) {
                final Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    //Convert the string back to JSON
                    final String storesJson = bundle.getString("body");
                    JSONArray storeArray = null;
                    try {
                        storeArray = new JSONArray(storesJson);
                    } catch (JSONException e) {
                        return;
                    }

                    //Loop and build
                    ArrayList<StoreLocation> stores = new ArrayList<StoreLocation>();
                    for (int i = 0; i < storeArray.length(); i++) {
                        //Grab the JSON object
                        JSONObject aStore;
                        try {
                            aStore = (JSONObject) storeArray.get(i);

                            //Create the new store location
                            StoreLocation loc = new StoreLocation();

                            //Fill it
                            loc.address = aStore.getString("Address");
                            loc.address2 = aStore.getString("Address2");
                            loc.city = aStore.getString("City");
                            loc.state = aStore.getString("State");
                            loc.zip = aStore.getString("Zip");
                            loc.name = aStore.getString("Name");
                            loc.lat = aStore.getString("Lat");
                            loc.lng = aStore.getString("Lng");
                            loc.phone = aStore.getString("Phone");

                            //Add to the list
                            stores.add(loc);

                        } catch (JSONException e) {
                            return;
                        }
                    }

                    if (stores != null && stores.size() > 0) {
                        final ComicStoreAdaptor adapter = new ComicStoreAdaptor(ComicStoreLocatorActivity.this,
                                R.layout.item_comic_store_location, stores);
                        setListAdapter(adapter);
                    }
                }
            }
        }
    }

    ProgressDialog progressDialog;
    ComicStoreLocatorResponseReceiver comicStoreLocatorResponseReceiver = new ComicStoreLocatorResponseReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeLocateShopDialog();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    void makeLocateShopDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.dialog_store_finder, null);

        builder.setIcon(R.drawable.icon)
                .setView(textEntryView)
                .setTitle(R.string.locate_comic_shops_title)
                .setMessage(R.string.locate_comic_shops_message)
                .setPositiveButton(R.string.sign_up_zip_button,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final String zipCode = ((EditText) textEntryView.findViewById(R.id.zip_code)).getText().toString();
                                final Pattern pattern = Pattern
                                        .compile("^(\\d{5})(-\\d{4})?$");
                                final Matcher matcher = pattern
                                        .matcher(zipCode);

                                //got good 5 digit zip
                                if (matcher.find()) {
                                    if (matcher.group(0) != null) {
                                        executeZipLookup(matcher.group(0));
                                    }
                                }
                                //not a good zip
                                else {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(ComicStoreLocatorActivity.this);

                                    builder.setTitle(R.string.zip_code_required_title)
                                            .setMessage(R.string.zip_code_required_message)
                                            .setCancelable(true)
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                public void onClick(final DialogInterface dialog, final int id) {
                                                    makeLocateShopDialog();
                                                }
                                            })
                                            .create().show();
                                }
                            }
                        })
                .setNegativeButton(R.string.sign_up_locztion_button, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        executeLocationLookup();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface arg0) {
                        finish();

                    }
                }).create().show();
    }

    void executeZipLookup(final String zip) {
        final IverseApplication app = (IverseApplication) getApplication();

        final ComicStoreLocatorTask task = new ComicStoreLocatorTask(app.getComicStore());
        task.setZip(zip);

        app.getTaskPool().submit(task);
        progressDialog = ProgressDialog.show(ComicStoreLocatorActivity.this, "", "Finding stores...", true);
    }

    void executeLocationLookup() {
        final IverseApplication app = (IverseApplication) getApplication();

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location recentLoc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (recentLoc != null) {
            final ComicStoreLocatorTask task = new ComicStoreLocatorTask(app.getComicStore());
            task.setLatitude(recentLoc.getLatitude());
            task.setLongitude(recentLoc.getLongitude());

            app.getTaskPool().submit(task);
            progressDialog = ProgressDialog.show(ComicStoreLocatorActivity.this, "", "Finding stores...", true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter
                .addAction(ComicStoreLocatorTask.FINDSTORE_RESPONSE);
        LocalBroadcastManager.getInstance(this).registerReceiver(comicStoreLocatorResponseReceiver, intentFilter);
    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(comicStoreLocatorResponseReceiver);
    }

    @Override
    protected void onListItemClick(final ListView l, final View v, final int position, long id) {

        final StoreLocation store = (StoreLocation) (getListAdapter().getItem(position));
//		final IverseApplication app = (IverseApplication) getApplication();
        final AlertDialog.Builder builder = new AlertDialog.Builder(ComicStoreLocatorActivity.this);

        builder.setTitle(store.name);
        builder.setCancelable(true);

//    	Object telephoneService = app.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE); 
//    	if (telephoneService != null)
//    	{
        //We have phone - add the call button
        builder.setPositiveButton(R.string.call_button, new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                //Clean up the phone number
                String formattedPhone = store.phone.replaceAll("[^0-9]", "");

                //Place the phone call
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + formattedPhone)));
//		    		Intent intent = new Intent(Intent.ACTION_CALL, 
//		    				Uri.fromParts( 
//		    				        "tel", formattedPhone, null)); 
//		    				    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
//		    				    startActivity(intent); 		    		
            }
        });
//    	}

        builder.setNegativeButton(R.string.view_in_maps_button, new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                final String query = "geo:" +
                        store.getLat() +
                        "," +
                        store.getLng() +
                        "?=" + store.getAddress();

                final Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(query));
                startActivity(intent);
            }
        });

        builder.create().show();
    }
}
