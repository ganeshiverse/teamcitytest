package com.iversecomics.client.store.db;

import android.database.sqlite.SQLiteDatabase;

import com.iversecomics.client.store.model.Comic;

public interface ComicDBSubUpdater {
    void onUpdateEnd(BatchInserts batcher);

    void update(SQLiteDatabase db, BatchInserts batcher, Comic comic);
}
