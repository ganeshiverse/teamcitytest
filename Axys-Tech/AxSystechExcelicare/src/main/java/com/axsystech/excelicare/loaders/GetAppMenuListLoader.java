package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetAppMenuListLoader extends DataAsyncTaskLibLoader<AppMenuListResponse> {

    private String token;
    private String lmd;

    public GetAppMenuListLoader(Context context, String token, String lmd) {
        super(context);
        this.token = token;
        this.lmd = lmd;
    }

    @Override
    protected AppMenuListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getAppMenuList(token, lmd);
    }
}
