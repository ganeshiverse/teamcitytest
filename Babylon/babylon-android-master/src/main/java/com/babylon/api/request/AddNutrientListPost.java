package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Food;
import com.babylon.model.Nutrient;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;

public class AddNutrientListPost extends AbsRequestPost<Food.FoodList, ResponseList<Nutrient>> {

    @Override
    protected String getUrl() {
        return Urls.get(Urls.NUTRIENT_LIST);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }
}
