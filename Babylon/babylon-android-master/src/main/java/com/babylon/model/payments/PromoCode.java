package com.babylon.model.payments;

import com.babylon.model.BaseModel;

import java.io.Serializable;

public class PromoCode implements Serializable {

    @BaseModel.SkipFieldInContentValues
    private static final long serialVersionUID = 56858L;

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
