package com.axsystech.excelicare.network;

import com.axsystech.excelicare.network.exceptions.InternalServerException;
import com.axsystech.excelicare.util.CommonUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRoute;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.IOException;
import java.io.InputStream;

/**
 * Date: 07.05.14
 * Time: 11:23
 *
 * @author SomeswarReddy
 */
public final class AxSysTechHttpClient {

    public static final String DEFAULT_ENCODING = "UTF-8";
    private static final int CONNECTION_TIMEOUT = 60 * 1000;
    private static final int SO_TIMEOUT = 60 * 1000;
    private static final AxSysTechHttpClient INSTANCE = new AxSysTechHttpClient();
    private static final int MAX_CONNECTIONS = 10;
    private final DefaultHttpClient mHttpClient;

    private AxSysTechHttpClient() {
        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, SO_TIMEOUT);
        ConnManagerParams.setMaxTotalConnections(httpParams, MAX_CONNECTIONS);
        ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRoute() {
            @Override
            public int getMaxForRoute(final HttpRoute route) {
                return MAX_CONNECTIONS;
            }
        });
        HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(httpParams, DEFAULT_ENCODING);

        final SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        //schemeRegistry.register(new Scheme("https", createSslFactory(), 443));
        final ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(httpParams, schemeRegistry);
        mHttpClient = new DefaultHttpClient(cm, httpParams);
    }

    public static String executeMethod(final HttpRequestBase requestBase) throws IOException, InternalServerException {
        return INSTANCE.execute(requestBase);
    }

    private String execute(final HttpRequestBase requestBase) throws IOException, InternalServerException {
        if (null == requestBase) {
            throw new IllegalArgumentException("requestBase can't be null");
        }

        requestBase.setHeader("Accept", "application/json");
        requestBase.setHeader("Content-type", "application/json");
        requestBase.addHeader("Accept-Charset", DEFAULT_ENCODING);

        HttpEntity entry = null;

        try {
            final HttpResponse response = mHttpClient.execute(requestBase);
            if (null == response) {
                CommonUtils.checkInternet();
                throw new InternalServerException();
            }

            entry = response.getEntity();

            if (null != entry) {
                final InputStream inputStream = entry.getContent();
                if (null != inputStream) {
                    try {
                        return CommonUtils.readStreamAsString(inputStream, DEFAULT_ENCODING);
                    } finally {
                        inputStream.close();
                    }
                }
            }
        } catch (final IOException e) {
            CommonUtils.checkInternet();
            throw new InternalServerException(e);
        } finally {
            if (null != entry) {
                entry.consumeContent();
            }
        }
        throw new InternalServerException();
    }

}