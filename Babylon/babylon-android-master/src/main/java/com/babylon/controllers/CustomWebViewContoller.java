package com.babylon.controllers;


import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import java.util.Map;

public class CustomWebViewContoller extends WebView{
    public CustomWebViewContoller(Context context) {
        super(context);
    }

    public CustomWebViewContoller(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomWebViewContoller(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void goBack() {
        super.goBack();

    }

    @Override
    public void loadUrl(String url) {
        super.loadUrl(url);
    }

    @Override
    public void loadUrl(String url, Map<String, String> additionalHttpHeaders) {
        super.loadUrl(url, additionalHttpHeaders);
    }
}
