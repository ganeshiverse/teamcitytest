package com.babylon.api.request.triage;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.enums.Gender;
import com.babylon.model.check.export.Symptom;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class TriageGeneralSymptomsGet extends AbsRequestGet<ResponseList<Symptom>> {
    private static final String PARAM_GENDER = "gender";

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.TRIAGE_SYMPTOMS_GENERAL_GET);
        String endURL = TextUtils.concat(url, urlParams).toString();
        return endURL;
    }

    public static TriageGeneralSymptomsGet newSymptomsGeneralGet(Gender gender) {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (gender != null) {
            params.add(new BasicNameValuePair(PARAM_GENDER, String.valueOf(gender.ordinal())));
        }
        return (TriageGeneralSymptomsGet) new TriageGeneralSymptomsGet().withParams(params);
    }

}