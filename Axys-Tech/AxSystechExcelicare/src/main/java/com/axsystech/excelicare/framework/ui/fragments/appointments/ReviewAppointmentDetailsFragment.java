package com.axsystech.excelicare.framework.ui.fragments.appointments;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BookAppointmentResponse;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.data.responses.TimeSlotsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.messages.DoctorsSearchResultsFragments;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.loaders.BookAppointmentRequestLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by someswarreddy on 25/08/15.
 */
public class ReviewAppointmentDetailsFragment extends BaseFragment implements View.OnClickListener {

    private static final String BOOK_APPOINTMENT_LOADER = "com.axsystech.excelicare.framework.ui.fragments.appointments.BookAppointmentFragment.BOOK_APPOINTMENT_LOADER";

    private String TAG = ReviewAppointmentDetailsFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private ReviewAppointmentDetailsFragment mFragment;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectView(R.id.confirmedTextView)
    private TextView confirmedTextView;

    @InjectView(R.id.appointmentaWithtTextView)
    private TextView appointmentaWithtTextView;

    @InjectView(R.id.specialityTextView)
    private TextView specialityTextView;

    @InjectView(R.id.dateTimeTextView)
    private TextView dateTimeTextView;

    @InjectView(R.id.consultationModeTextView)
    private TextView consultationModeTextView;

    @InjectView(R.id.feeTextView)
    private TextView feeTextView;

    @InjectView(R.id.clinicTextView)
    private TextView clinicTextView;

    @InjectView(R.id.addressTextView)
    private TextView addressTextView;

    @InjectView(R.id.emailTextView)
    private TextView emailTextView;

    @InjectView(R.id.contactTextView)
    private TextView contactTextView;

    @InjectView(R.id.visitUsTextView)
    private TextView visitUsTextView;

    @InjectView(R.id.completeBookingButton)
    private Button completeBookingButton;

    @InjectView(R.id.bookingStatusTextView)
    private TextView bookingStatusTextView;

    // Intent Data
    private String mBookAppointmentReviewType = AxSysConstants.BOOK_APPOINTMENT_REVIEW_TYPE_BOOK;
    private ClinicianDetailsResponse.ClinicianDetailsData mSelectedClinicianDetailsData;
    private ClinicianDetailsResponse.ClinicDetails mSelectedClinicDetails;
    private int mSelectedClinicPosition;
    private Calendar mSelectedBookingCalendar = Calendar.getInstance();
    private TimeSlotsResponse.TimeSlotDetails mSelectedBookingTimeSlotObj;
    private String mSelectedConsultationMode = "In Person";
    private SearchDoctorsResponse.DoctorDetails mSelectedClinicianObject;

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(),"Appointment Details"));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.review_appointment_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        readIntentArgs();
        addEventListeners();

        preLoadData();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_APPOINTMENT_REVIEW_TYPE)) {
                mBookAppointmentReviewType = bundle.getString(AxSysConstants.EXTRA_APPOINTMENT_REVIEW_TYPE, AxSysConstants.BOOK_APPOINTMENT_REVIEW_TYPE_BOOK);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_CLINICIAN_DETAILS)) {
                mSelectedClinicianDetailsData = (ClinicianDetailsResponse.ClinicianDetailsData) bundle.getSerializable(AxSysConstants.EXTRA_CLINICIAN_DETAILS);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION)) {
                mSelectedClinicPosition = bundle.getInt(AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION, 0);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_APPOINTMENT_DATETIME)) {
                mSelectedBookingCalendar = (Calendar) bundle.getSerializable(AxSysConstants.EXTRA_BOOK_APPOINTMENT_DATETIME);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_APPOINTMENT_TIMESLOT)) {
                mSelectedBookingTimeSlotObj = (TimeSlotsResponse.TimeSlotDetails) bundle.getSerializable(AxSysConstants.EXTRA_BOOK_APPOINTMENT_TIMESLOT);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_BOOK_APPOINTMENT_CONS_MODE)) {
                mSelectedConsultationMode = bundle.getString(AxSysConstants.EXTRA_BOOK_APPOINTMENT_CONS_MODE, "In Person");
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT)) {
                mSelectedClinicianObject = (SearchDoctorsResponse.DoctorDetails) bundle.getSerializable(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT);
            }
        }
    }

    private void addEventListeners() {
        completeBookingButton.setOnClickListener(mFragment);
    }

    private void preLoadData() {
        if (mSelectedClinicianDetailsData != null && mSelectedClinicianDetailsData.getClinicDetailsArrayList() != null &&
                mSelectedClinicianDetailsData.getClinicDetailsArrayList().size() > mSelectedClinicPosition) {
            mSelectedClinicDetails = mSelectedClinicianDetailsData.getClinicDetailsArrayList().get(mSelectedClinicPosition);
        }

        if (mBookAppointmentReviewType == AxSysConstants.BOOK_APPOINTMENT_REVIEW_TYPE_BOOK) {
            // User is going to book appointment in this mode
            confirmedTextView.setVisibility(View.GONE);
            confirmedTextView.setText("");
            completeBookingButton.setText(getString(R.string.text_complete_booking));
            bookingStatusTextView.setVisibility(View.GONE);
            bookingStatusTextView.setText("");

            // load book appointment data
            reviewBookAppointmentData();
        }
    }

    private void reviewBookAppointmentData() {
        if (mSelectedClinicianDetailsData != null && mSelectedClinicDetails != null && mSelectedBookingTimeSlotObj != null) {
            appointmentaWithtTextView.setText(/*"Dr " +*/ (!TextUtils.isEmpty(mSelectedClinicianDetailsData.getClinicianName()) ? mSelectedClinicianDetailsData.getClinicianName() : ""));
            specialityTextView.setText(!TextUtils.isEmpty(mSelectedClinicianDetailsData.getSpeciality()) ? mSelectedClinicianDetailsData.getSpeciality() : "");

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
            SimpleDateFormat timeSlotFormat = new SimpleDateFormat(AxSysConstants.DF_TIMESLOT_TIME_FORMAT);

            String selectedTimeSlotDate = sdf.format(mSelectedBookingCalendar.getTime());
            String selectedTimeSlotTime = "";
            try {
                selectedTimeSlotTime = timeSlotFormat.format(timeSlotFormat.parse(mSelectedBookingTimeSlotObj.getTimeSlot()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dateTimeTextView.setText(selectedTimeSlotDate + " " + selectedTimeSlotTime);
            //Sravan
            //Commented if mselectedconsultionmode is empty statement
            //if (mSelectedConsultationMode.equals(""))

                consultationModeTextView.setText(mSelectedConsultationMode);
            //Sravan
            //added conditions to set icons
            if(mSelectedConsultationMode.equals("In Person")){
                consultationModeTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.inperson_timeslots, 0, 0, 0);
            } else if(mSelectedConsultationMode.equals("Call")){
                consultationModeTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.phone_timeslots, 0, 0, 0);
            } else if(mSelectedConsultationMode.equals("Video")) {
                consultationModeTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.video_timeslots, 0, 0, 0);
            }

            if (!TextUtils.isEmpty(mSelectedClinicDetails.getFee())) {
                feeTextView.setText(mSelectedClinicDetails.getFee());
            } else if (mSelectedClinicianObject != null && !TextUtils.isEmpty(mSelectedClinicianObject.getFee())) {
                feeTextView.setText(mSelectedClinicianObject.getFee());
            }
            clinicTextView.setText(mSelectedClinicDetails.getName());

            String address = mSelectedClinicDetails.getAddress1() +
                    (!TextUtils.isEmpty(mSelectedClinicDetails.getAddress2()) ? ", " + mSelectedClinicDetails.getAddress2() : "") +
                    (!TextUtils.isEmpty(mSelectedClinicDetails.getAddress3()) ? ", " + mSelectedClinicDetails.getAddress3() : "") +
                    (!TextUtils.isEmpty(mSelectedClinicDetails.getState()) ? ", " + mSelectedClinicDetails.getState() : "") +
                    (!TextUtils.isEmpty(mSelectedClinicDetails.getPostcode()) ? ", " + mSelectedClinicDetails.getPostcode() : "");
            addressTextView.setText(address);

            emailTextView.setText(!TextUtils.isEmpty(mSelectedClinicDetails.getEmail()) ? mSelectedClinicDetails.getEmail() : "");
            contactTextView.setText(!TextUtils.isEmpty(mSelectedClinicDetails.getPhone()) ? mSelectedClinicDetails.getPhone() : "");
            visitUsTextView.setText(!TextUtils.isEmpty(mSelectedClinicianDetailsData.getURL()) ? mSelectedClinicianDetailsData.getURL() : "");
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.completeBookingButton:
                if (mBookAppointmentReviewType == AxSysConstants.BOOK_APPOINTMENT_REVIEW_TYPE_BOOK) {
                    completeBooking();
                }
                break;

            default:
                break;
        }
    }

    private void completeBooking() {
        // create Book Appointment post body data
        JSONObject mainJson = new JSONObject();
        JSONObject appointmentDetailsJson = new JSONObject();
        SimpleDateFormat startAndEndDateFormat = new SimpleDateFormat(AxSysConstants.DF_BOOK_APPOINTMENT_REQUEST);
        SimpleDateFormat reqeustdatetimeFormat = new SimpleDateFormat(AxSysConstants.DF_APPOINTMENT_BOOKING_DATETIME);
        SimpleDateFormat timeSlotFormat = new SimpleDateFormat(AxSysConstants.DF_TIMESLOT_TIME_FORMAT);

        String selectedTimeSlotStartTime = "";
        String selectedTimeSlotEndTime = "";
        try {
            Date timeSlotStartDate = timeSlotFormat.parse(mSelectedBookingTimeSlotObj.getTimeSlot());
            selectedTimeSlotStartTime = timeSlotFormat.format(timeSlotStartDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(timeSlotStartDate);

            int slotLength = 30;
            if (!TextUtils.isEmpty(mSelectedBookingTimeSlotObj.getSlotLength())) {
                slotLength = Integer.parseInt(mSelectedBookingTimeSlotObj.getSlotLength());
            }
            calendar.add(Calendar.MINUTE, slotLength);

            selectedTimeSlotEndTime = timeSlotFormat.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            appointmentDetailsJson.put("clinicid", !TextUtils.isEmpty(mSelectedClinicDetails.getID()) ? mSelectedClinicDetails.getID() : "");
            appointmentDetailsJson.put("clinicianid", !TextUtils.isEmpty(mSelectedClinicianDetailsData.getClinicianId()) ? mSelectedClinicianDetailsData.getClinicianId() : "");
            appointmentDetailsJson.put("staffroleid", "0");
            appointmentDetailsJson.put("userid", mActiveUser.getEcUserID() + "");
            appointmentDetailsJson.put("patid", !TextUtils.isEmpty(mActiveUser.getPatientID()) ? mActiveUser.getPatientID() : "");
            appointmentDetailsJson.put("appttypeid", "502964");
            appointmentDetailsJson.put("startdatetime", startAndEndDateFormat.format(mSelectedBookingCalendar.getTime()) + " " + selectedTimeSlotStartTime);
            appointmentDetailsJson.put("enddatetime", startAndEndDateFormat.format(mSelectedBookingCalendar.getTime()) + " " + selectedTimeSlotEndTime);
            appointmentDetailsJson.put("reqeustdatetime", reqeustdatetimeFormat.format(Calendar.getInstance().getTime()));
            appointmentDetailsJson.put("description", "");
            appointmentDetailsJson.put("ticketno", "0");

            mainJson.put("Token", mActiveUser.getToken());
            mainJson.put("AppointmentDetails", appointmentDetailsJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            // Send Book Appointment Post request
            BookAppointmentRequestLoader bookAppointmentRequestLoader = new BookAppointmentRequestLoader(mActivity, mainJson.toString());
            if (getView() != null && isAdded()) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(BOOK_APPOINTMENT_LOADER), bookAppointmentRequestLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_print_option, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(BOOK_APPOINTMENT_LOADER)) {
            showToast(R.string.error_book_appointment);
        }

    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(BOOK_APPOINTMENT_LOADER)) {
            if (result != null && result instanceof BookAppointmentResponse) {
                BookAppointmentResponse bookAppointmentResponse = (BookAppointmentResponse) result;

                BookAppointmentResponse.BookAppointmentResult bookAppointmentResult = bookAppointmentResponse.getBookAppointmentResult();
                if (bookAppointmentResult != null) {
                    if (bookAppointmentResponse.isSuccess() && !TextUtils.isEmpty(bookAppointmentResult.getData())) {
                        if (!TextUtils.isEmpty(bookAppointmentResponse.getBookAppointmentResult().getMessageCaps())) {
                            showToast(bookAppointmentResponse.getBookAppointmentResult().getMessageCaps());
                        } else if (!TextUtils.isEmpty(bookAppointmentResponse.getBookAppointmentResult().getMessage())) {
                            showToast(bookAppointmentResponse.getBookAppointmentResult().getMessage());
                        } else {
                            showToast(R.string.success_book_appointment);
                        }

                        // clear all fragments and take user to appointment list screen
                        final FragmentManager fm = getActivity().getSupportFragmentManager();
                        int stackCount = fm.getBackStackEntryCount();
                        for (int i = 0; i < stackCount; ++i) {
                            int backStackId = fm.getBackStackEntryAt(i).getId();
                            final String backStackName = fm.getBackStackEntryAt(i).getName();

                            if (!TextUtils.isEmpty(backStackName)) {
                                if (!backStackName.equalsIgnoreCase(DoctorsSearchResultsFragments.class.getSimpleName())) {
                                    new Handler().post(new Runnable() {
                                        @Override
                                        public void run() {
                                            fm.popBackStack(backStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        }
                                    });

                                } else if (backStackName.equalsIgnoreCase(DoctorsSearchResultsFragments.class.getSimpleName())) {
                                    Fragment fragment = fm.findFragmentByTag(DoctorsSearchResultsFragments.class.getSimpleName());
                                    if (fragment instanceof DoctorsSearchResultsFragments) {
                                        DoctorsSearchResultsFragments doctorsSearchResultsFragments = (DoctorsSearchResultsFragments) fragment;
                                    }
                                }
                            }
                        }
                    } else {
                        if (!TextUtils.isEmpty(bookAppointmentResponse.getBookAppointmentResult().getMessageCaps())) {
                            showToast(bookAppointmentResponse.getBookAppointmentResult().getMessageCaps());
                        } else if (!TextUtils.isEmpty(bookAppointmentResponse.getBookAppointmentResult().getMessage())) {
                            showToast(bookAppointmentResponse.getBookAppointmentResult().getMessage());
                        }
                    }
                }
            }
        }
    }

}
