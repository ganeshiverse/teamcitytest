package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.shop.ShopCategoriesListAdapter;
import com.babylon.api.request.base.Urls;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Kit.Category;
import com.babylon.model.payments.Order;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.CommonHelper;
import com.babylon.view.TopBarInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopCategoriesListFragment extends BaseFragment implements AbsListView.OnItemClickListener {

    public static final String TAG = ShopCategoriesListFragment.class.getSimpleName();

    public interface KitCategoriesFragmentListener {
        public void onCategoriesListItemClicked(Category category);
        public void onCategoriesHelpClicked();

    }

    private ListView listView;
    private ShopCategoriesListAdapter adapter;
    private List<Category> categories = new ArrayList<Category>();
    private KitCategoriesFragmentListener listener;
    private Order mOrderList;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_kit_list_view, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getString(R.string.categories_list_fragment_title));
        listView = (ListView)view.findViewById(android.R.id.list);

        setProgressBarVisible(true);
       listView.setOnItemClickListener(this);

        if(categories.size()==0) {
            getCategories();
        }
        else  {
            adapter = new ShopCategoriesListAdapter(getActivity(), categories,onPropertyItemClickListener);
            listView.setAdapter(adapter);
            setProgressBarVisible(false);
        }

    }


    private final View.OnClickListener onItemClickListener = new
            View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onCategoriesHelpClicked();
                    }
                }
            };


    private ShopCategoriesListAdapter.OnPropertyItemClickListener onPropertyItemClickListener =
            new ShopCategoriesListAdapter.OnPropertyItemClickListener() {

        @Override public View.OnClickListener getOnPropertyClickListener() {
            return onItemClickListener;
        }
    };

    private void getCategories() {

        String mUri= CommonHelper.getUrl(Urls.CATEGORIES);
        JsonArrayRequest mArrayReq = new JsonArrayRequest(mUri,	new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String jsonString = response.toString();
                Type collectionType = new TypeToken<List<Category>>() { }.getType();
                List<Category> categoriesList = gson.fromJson(jsonString, collectionType);
                categories= categoriesList;
                if(categories.isEmpty()){

                    AlertDialogHelper.getInstance().showMessage(getActivity(),getString(R.string.category_empty_msg));

                }
                adapter = new ShopCategoriesListAdapter(getActivity(), categoriesList,onPropertyItemClickListener);
                listView.setAdapter(adapter);
                setProgressBarVisible(false);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,error.toString());
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                return headers;
            }

        };

      if (App.getInstance().isConnection()){
            mArrayReq.setRetryPolicy(new DefaultRetryPolicy(18000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

           App.getInstance().getRequestQueue().add(mArrayReq);
         }



    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (KitCategoriesFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PINK_THEME;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (listener != null) {
            listener.onCategoriesListItemClicked(adapter.getItem(position));
        }
    }



}