package com.babylon.utils;


/**
 * Created by Rahul.
 */
public enum MixPanelEventConstants
{
    //ASK
    USER_ASKS_QUESTION("Question Asked"),
    USER_RECEIVES_REPLY("Answer Read"),

    //WELCOME TOUR
    TOUR_STARTED("Welcome Tour Started"),
    TOUR_COMPLETED("Welcome Tour Completed"),
    TOUR_SKIPPED("Welcome Tour Skipped"),
    APP_INSTALLED("App Installed"),
    APP_CLOSED("App Closed"),

    //REGISTRATION
    //SIGN_UP_WITH_EMAIL("reg:email"),
    CONNECT_FACEBOOK("onFacebookRegistered"),
    SIGN_IN("Login"),
    //READ_TNC("reg:read terms and conditions"),
    PROMO_CODE("Promo Code"),
    REGISTRATION_STARTED("Registration Started"),
    REGISTRATION_COMPLETE("Registration Completed"),

    PAY_AS_YOU_GO("PAYG Plan Selected"),
    SUBSCRIPTION("Subscription Plan Selected"),
    SUBSCRIPTION_CANCELLED("Subscription Canceled"),
    PROMOTION("Promo Code Entered"),

    APP_LAUNCHED("App Open"),
    APP_RATING("App Rating"),
    NOTIFICATION_RECEIVED("Push Notification Received"),
    NOTIFICATION_READ("Push Notification Read"),
    FAMILY_ACCOUNT_CREATED("Family Member Added"),

    //CLINICAL EVENTS
    APPOINTMENT_TRIED("Appointment Tried"),
    GP_APPOINTMENT_BOOKED("Appointment Booked"),
    APPOINTMENTS_CANCELLED("Payment Canceled"),
    VIDEO_APPOINTMENT_ENDED("Video Appointment Completed"),
    PRESCRIPTION_OPENED("Prescription Opened"),
    SPECIALIST_BOOKED("Specialist Booked"),

    //CLINICAL RECORDS
    RECORD_OPENED("Opened Record"),
    SETTINGS_OPENED("Opened Settings"),
    KIT_BUTTON_TAPPED("KIT Button Tapped"),

    //PAYMENTS
    CARD_ADDED("Credit Card Added"),
    PAYMENT_SUCCESS("Payment Succeeded"),
    PAYMENT_FAIL("Payment Failed"),
    PAYMENT_REQUESTED("Payment Requested");

    private String mEventName;

    private MixPanelEventConstants(String eventName)
    {
        mEventName = eventName;
    }

    public String getEventName()
    {
        return mEventName;
    }
}