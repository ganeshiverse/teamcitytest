package com.babylon.model.payments;

import com.babylon.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Promotion implements Serializable {

    @BaseModel.SkipFieldInContentValues
    private static final long serialVersionUID = 43473473L;

    private Integer id;
    private String name;
    private String description;
    private Integer duration;
    private String promotion_type;
    @SerializedName("type_amount")
    private Integer typeAmount;
    @SerializedName("expires_on")
    private Date expiresOn;
    private String logo;
    @SerializedName("is_credit_card_required")
    private String is_credit_card_required;

    @SerializedName("credit_card_required_text")
    private String credit_card_required_text;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getIsCreditCardRequired()
    {
        return is_credit_card_required;
    }

    public String getCreditCardRequiredText()
    {
        return credit_card_required_text;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getPromotion_type() {
        return promotion_type;
    }

    public void setPromotion_type(String promotion_type) {
        this.promotion_type = promotion_type;
    }

    public Integer getTypeAmount() {
        return typeAmount;
    }

    public void setTypeAmount(Integer typeAmount) {
        this.typeAmount = typeAmount;
    }

    public Date getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(Date expiresOn) {
        this.expiresOn = expiresOn;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
