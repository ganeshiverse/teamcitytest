package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by hkyerra on 7/13/2015.
 */
public class GetDashBoardCardsLoader extends DataAsyncTaskLibLoader<GetDashBoardCardsResponse> {

    private String token;
    private String dashboardId;
    private String includeDesign;

    public GetDashBoardCardsLoader(Context context, String token, String dashboardId, String includeDesign) {
        super(context);
        this.token = token;
        this.dashboardId = dashboardId;
        this.includeDesign = includeDesign;
    }

    @Override
    protected GetDashBoardCardsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getDashboardCards(token, dashboardId, includeDesign);
    }
}
