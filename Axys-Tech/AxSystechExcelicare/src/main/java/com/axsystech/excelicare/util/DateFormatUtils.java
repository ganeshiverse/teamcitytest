package com.axsystech.excelicare.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by akurella on 10/28/2015.
 */
public class DateFormatUtils {

    public static String formatDefaultDate(String serverDateString,String formatStyle){
        String displayDate = "";
        SimpleDateFormat serverDateFormat = new SimpleDateFormat(formatStyle);
        try {
            Date serverDate = serverDateFormat.parse(serverDateString);
            SimpleDateFormat displayDateFormat = new SimpleDateFormat("MMMM d,yyyy");
            displayDate = displayDateFormat.format(serverDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayDate;
    }
}
