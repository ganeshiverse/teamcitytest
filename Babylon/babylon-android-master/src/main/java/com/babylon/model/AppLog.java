package com.babylon.model;

public class AppLog {
    private String message;
    private String patient_id;

    public AppLog(String message, String id) {
        this.message = message;
        this.patient_id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPatientId() {
        return patient_id;
    }

    public void setPatientId(String patient_id) {
        this.patient_id = patient_id;
    }
}
