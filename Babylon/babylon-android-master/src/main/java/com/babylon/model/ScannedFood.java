package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class ScannedFood extends ServerModel {
    private Serving serving;

    @SerializedName("description")
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setKcal(String kcal) {
        serving.kcal = kcal;
    }

    public String getName() {
        return name;
    }

    public String getKcal() {
        return serving.kcal;
    }

    public static class Serving {
        String kcal;

        @Override
        public String toString() {
            return "kcal = " + kcal;
        }
    }

    @Override
    public String toString() {
        return serving.toString() + ", name = " + name;
    }
}
