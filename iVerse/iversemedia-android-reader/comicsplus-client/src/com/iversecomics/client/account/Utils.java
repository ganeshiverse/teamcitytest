package com.iversecomics.client.account;

import android.content.Context;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.net.NetStatus;
import com.iversecomics.client.store.ServerConfig;


public class Utils {

    static public boolean isLoggedIn(final Context context) {
        final IverseApplication app = (IverseApplication) context.getApplicationContext();
        final String userAccountStatus = ServerConfig.getDefault().getUserAccountStatus();

        if (app == null || userAccountStatus == null) {
            return false;
        }

        return app.getOwnership().hasCredentials() &&
                userAccountStatus.equals("1") &&
                NetStatus.isAvailable(context);
    }

    static public void logOut(final Context context) {
        final IverseApplication app = (IverseApplication) context.getApplicationContext();
        app.getOwnership().clearCredentials();
    }

}
