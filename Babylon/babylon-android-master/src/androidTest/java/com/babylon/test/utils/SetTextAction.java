package com.babylon.test.utils;

import android.view.View;
import android.widget.EditText;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.matcher.ViewMatchers;

import org.hamcrest.Matcher;

public class SetTextAction implements ViewAction {
    public static SetTextAction setText(String text) {
        return new SetTextAction(text);
    }

    private final String stringToBeTyped;

    public SetTextAction(String stringToBeTyped) {
        this.stringToBeTyped = stringToBeTyped;
    }


    @Override
    public void perform(UiController uiController, View view) {
        ((EditText) view).setText(stringToBeTyped);
    }

    @Override
    public Matcher<View> getConstraints() {
        Matcher<View> standardConstraint = ViewMatchers.isDisplayingAtLeast(90);
        return standardConstraint;
    }

    @Override
    public String getDescription() {
        return "type text";
    }
}