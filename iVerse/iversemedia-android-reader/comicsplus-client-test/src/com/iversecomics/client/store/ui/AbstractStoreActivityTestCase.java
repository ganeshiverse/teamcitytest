package com.iversecomics.client.store.ui;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.CursorAdapter;

import com.iversecomics.client.refresh.NoopTask;
import com.iversecomics.client.refresh.TaskPool;
import com.iversecomics.client.ui.AbstractComicsPlusActivity;
import com.iversecomics.client.util.Time;

public abstract class AbstractStoreActivityTestCase<T extends AbstractComicsPlusActivity> extends
        ActivityInstrumentationTestCase2<T> {
    public AbstractStoreActivityTestCase(Class<T> activityClass) {
        super(activityClass);
    }

    protected void refresh(final CursorAdapter adapter) throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                assertTrue(adapter.getClass().getSimpleName() + ".cursor.requery() should succeed", adapter.getCursor()
                        .requery());
            }
        });
    }

    protected void sendUserRefresh(final AbstractComicsPlusActivity activity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.refreshViews(true);
            }
        });
    }

    protected void sleep(int timeDur, TimeUnit timeUnit) {
        long ms = TimeUnit.MILLISECONDS.convert(timeDur, timeUnit);
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ignore) {
            /* ignore */
        }
    }

    protected void waitForRefreshTasks(AbstractStoreActivity activity) throws Exception {
        // Wait for ServerConfig to finish itself.
        Future<?> serverConfig = activity.getServerConfigFuture();
        serverConfig.get(2, TimeUnit.SECONDS);

        // Trigger the refresh of the Views
        activity.refreshViews(true);
        TaskPool pool = activity.getTaskPool();
        Future<?> future = pool.submit(new NoopTask(Time.SECOND * 3));
        future.get(5, TimeUnit.SECONDS);
    }
}
