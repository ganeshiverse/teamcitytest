package com.babylon.fragment.stress;


import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.StressQuizCursorAdapter;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.MonitorMeParamsLoaderCallback;
import com.babylon.database.schema.StressQuizSchema;
import com.babylon.dialog.OkDialogFragment;
import com.babylon.enums.MonitorMeParam;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.StressQuestion;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.L;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StressQuizFragment extends BaseFragment implements StressQuizCursorAdapter.StressQuestionState {
    private static final String TAG = StressQuizFragment.class.getSimpleName();
    private static final int STRESS_QUIZ_LOADER_ID = 452;
    private static final int MONITOR_ME_PARAMS_LOADER_ID = 453;

    private Map<Integer, StressQuestion> questions = new HashMap<Integer, StressQuestion>();

    private StressQuizCursorAdapter adapter;
    private DailyFeatureMap stressFeature;

    private ListView listView;
    private long dateInMillis;

    public static StressQuizFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        StressQuizFragment fragment = new StressQuizFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stress_quiz, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dateInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);

        getLoaderManager().initLoader(MONITOR_ME_PARAMS_LOADER_ID, null, new ParamsLoader());
        getLoaderManager().initLoader(STRESS_QUIZ_LOADER_ID, null, stressQuestionsLoader);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.stress);

        listView = (ListView) view.findViewById(android.R.id.list);
        listView.setOnItemClickListener(onQuestionItemClickListener);

        final Button measureStressButton = (Button) view.findViewById(R.id.btn_stress_analyse);
        measureStressButton.setOnClickListener(onMeasureClickListener);
    }

    @Override
    protected void onTopBarButtonPressed() {
        setProgressBarVisible(true);
        super.onTopBarButtonPressed();
    }

    @Override
    public void onStop() {
        saveQuiz();
        super.onStop();
    }

    @Override
    public boolean isChecked(int id) {
        boolean result = false;

        if (questions.containsKey(id)) {
            StressQuestion question = questions.get(id);
            result = question.getIsChecked();
        }

        return result;
    }

    private void saveStress(int newStress) {
        Map<String, MonitorMeFeatures.Feature> featureMap = new HashMap<String, MonitorMeFeatures.Feature>();
        MonitorMeFeatures.Feature feature = new MonitorMeFeatures
                .Feature(MonitorMeParam.STRESS_ID.getId(),
                String.valueOf(newStress), dateInMillis);
        featureMap.put(MonitorMeParam.STRESS_ID.getId(), feature);

        DatabaseOperationUtils.getInstance().addMonitorMeFeatures(featureMap,
                getActivity().getContentResolver());

        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS,
                SyncAdapter.SYNC_PUSH);
    }

    private void saveQuiz() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                final List<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

                for (Map.Entry<Integer, StressQuestion> entry : questions.entrySet()) {
                    final ContentProviderOperation.Builder updateBuilder = getUpdateBuilder(entry);
                    operations.add(updateBuilder.build());
                }

                DatabaseOperationUtils.applyBatch(App.getInstance().getContentResolver(), operations);
                return null;
            }

            private ContentProviderOperation.Builder getUpdateBuilder(Map.Entry<Integer, StressQuestion> entry) {
                final ContentProviderOperation.Builder updateBuilder = ContentProviderOperation.newUpdate
                        (StressQuizSchema
                        .CONTENT_URI);

                final boolean isChecked = entry.getValue().getIsChecked();

                final String selection = StressQuizSchema.Columns.ID + " =? AND ("
                        + StressQuizSchema.Columns.CHECKED + " != ? OR "
                        + StressQuizSchema.Columns.CHECKED + " IS NULL )";
                final String isCheckedStr = String.valueOf(isChecked);
                final String[] selectionArgs = {
                        String.valueOf(entry.getValue().getId()),
                        isCheckedStr
                };

                updateBuilder.withSelection(selection, selectionArgs);
                updateBuilder.withValue(StressQuizSchema.Columns.CHECKED.getName(), isCheckedStr);
                return updateBuilder;
            }
        }.execute();
    }

    private void clearCache() {
        Iterator it = questions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            StressQuestion curQuestion = (StressQuestion) pairs.getValue();
            if (curQuestion.getIsChecked()) {
                curQuestion.setIsChecked(String.valueOf(false));
            }
        }
    }

    private int calculateStress() {
        int stressLevel = 0;

        for (Map.Entry<Integer, StressQuestion> entry : questions.entrySet()) {
            if (entry.getValue().getIsChecked()) {
                stressLevel++;
            }
        }

        return stressLevel;
    }

    private View.OnClickListener onMeasureClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setProgressBarVisible(true);

            final int newStress = calculateStress();

            saveStress(newStress);
            clearCache();

            showStressResultDialog(newStress);
        }
    };

    private void showStressResultDialog(int newStress) {
        final String resultMessage = new StressLevelDiagnose(newStress, questions.size()).getResultMessage();

        final OkDialogFragment okDialogFragment = OkDialogFragment.getInstance(resultMessage);
        okDialogFragment.setOnDismissListener(onStressResultDismissListener);
        okDialogFragment.show(getFragmentManager(), OkDialogFragment.TAG);
    }

    private final LoaderManager.LoaderCallbacks<Cursor> stressQuestionsLoader = new LoaderManager
            .LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(), StressQuizSchema.CONTENT_URI, StressQuizSchema.Query.PROJECTION,
                    null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if (adapter == null) {
                adapter = new StressQuizCursorAdapter(getActivity(), StressQuizFragment.this, data);
                listView.setAdapter(adapter);
            } else {
                adapter.swapCursor(data);
            }

            TextView emptyTextView = (TextView) getView().findViewById(android.R.id.empty);
            if (data.moveToFirst()) {
                emptyTextView.setVisibility(View.GONE);
                parseQuestions(data);
            } else {
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText(R.string.quiz_is_not_available_yet);
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }

        private void parseQuestions(Cursor c) {
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    final StressQuestion question = new StressQuestion();
                    question.createFromCursor(c);

                    questions.put(question.getId(), question);

                    c.moveToNext();
                }
            }
        }
    };

    private class ParamsLoader extends MonitorMeParamsLoaderCallback {

        @Override
        public Context getContext() {
            return getActivity();
        }

        @Override
        public List<MonitorMeParam> getMonitorMeParamList() {
            List<MonitorMeParam> featureList = new LinkedList<MonitorMeParam>();
            featureList.add(MonitorMeParam.STRESS_ID);
            return featureList;
        }

        @Override
        public void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> featureMap) {
            stressFeature = new DailyFeatureMap(featureMap);
        }
    }

    private AdapterView.OnItemClickListener onQuestionItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.check_box);
            boolean isChecked = !checkBox.isChecked();
            checkBox.setChecked(isChecked);

            int mapKey = (int) id;

            if (questions.containsKey(mapKey)) {
                final StressQuestion question = questions.get(mapKey);
                question.setIsChecked(String.valueOf(isChecked));
                questions.put(mapKey, question);
            } else {
                L.e(TAG, "Unknown id =" + String.valueOf(id));
            }
        }
    };

    private final DialogInterface.OnDismissListener onStressResultDismissListener = new DialogInterface
            .OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            getActivity().onBackPressed();
        }
    };
}
