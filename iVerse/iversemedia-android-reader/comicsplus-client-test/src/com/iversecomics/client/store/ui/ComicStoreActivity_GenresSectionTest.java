package com.iversecomics.client.store.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.LargeTest;

import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ui.adapters.GenreListAdapter;

public class ComicStoreActivity_GenresSectionTest extends AbstractComicStoreActivityTestCase {
    public ComicStoreActivity_GenresSectionTest() {
        super();

        Intent intent = new Intent();
        intent.putExtra("testing", true);
        intent.putExtra(ComicStoreActivity.BUNDLE_ACTIVE_PAGE, GenresListFragment.TAG);
        setActivityIntent(intent);
    }

    @LargeTest
    public void testOnResumeLoadListFromServer() throws Throwable {
        final ComicStoreActivity activity = getActivity();
        assertNotNull("Activity should have launched", activity);

        waitForRefreshTasks(activity);

        Fragment fragment = activity.getActiveFragment();
        MoreAsserts.assertAssignableFrom(GenresListFragment.class, fragment);

        GenresListFragment genresFrag = (GenresListFragment) fragment;
        GenreListAdapter listadapter = genresFrag.getAdapter();
        assertNotNull("GenreListAdapter should not be null", listadapter);

        refresh(listadapter);

        OurAsserts.assertAtLeast("GenreListAdapter.count", 5, listadapter.getCount());
    }
}
