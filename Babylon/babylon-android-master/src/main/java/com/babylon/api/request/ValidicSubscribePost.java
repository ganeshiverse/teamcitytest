package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;

public class ValidicSubscribePost extends AbsRequestPost<Void, Void> {

    @Override
    protected String getUrl() {
        return Urls.get(Urls.VALIDIC_SUBSCRIBE_POST);
    }
}
