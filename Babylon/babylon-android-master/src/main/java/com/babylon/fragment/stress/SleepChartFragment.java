package com.babylon.fragment.stress;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.babylon.R;
import com.babylon.chart.OnLoadChartProgressListener;
import com.babylon.enums.MonitorMeParam;
import com.babylon.fragment.AddingSleepFragment;
import com.babylon.fragment.BaseMonitorMeParamsFragment;
import com.babylon.utils.Constants;
import com.babylon.view.ChartView;

public class SleepChartFragment extends BaseMonitorMeParamsFragment
        implements OnLoadChartProgressListener {
    private ChartView chartView;

    public static SleepChartFragment getInstance(long beginOfTheDayInMillis, String description) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        b.putString(Constants.DESCRIPTION, description);

        SleepChartFragment fragment = new SleepChartFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sleep_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(R.string.sleep_title);

        chartView = (ChartView) view.findViewById(R.id.chartView);
        chartView.setOnLoadChartProgressListener(this);
        chartView.setDateInMillis(dateInMillis);
        chartView.setFeatureId(MonitorMeParam.SLEEP_ID.getId());

        final String description = getArguments().getString(Constants.DESCRIPTION);
        chartView.setDescription(description);

        if (!isChartLoaded) {
            chartView.showChart();
        }
        isChartLoaded = true;

        setRightImageButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startFragment(AddingSleepFragment.getInstance(dateInMillis));
            }
        }, R.drawable.ic_menu_add);

    }

    @Override
    public void onStartLoadChartFromServer() {
        setProgressBarVisible(true);
    }

    @Override
    public void onFinishLoadChartFromServer() {
        setProgressBarVisible(false);
    }

    /* Listeners */
    @Override
    protected void onTopBarButtonPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}