package com.babylon.model;

import java.util.Date;
import java.util.List;

/**
 * Model for display monitor me screen with categories and params
 */
public class MonitorMeReport extends BaseModel {

    @SkipFieldInContentValues
    private static final long serialVersionUID = 23462L;

    private Date date;
    private List<MonitorMeCategory> categories;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<MonitorMeCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<MonitorMeCategory> categories) {
        this.categories = categories;
    }
}
