package com.iversecomics.client.drawable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.iversecomics.client.bitmap.ImageRect;

public final class DrawableDebug {
    private static final int DEBUG_BOX_COLOR = Color.parseColor("#aaff0088");

    public static void box(Canvas canvas, ImageRect rect) {
        box(canvas, DEBUG_BOX_COLOR, rect);
    }

    public static void box(Canvas canvas, int color, ImageRect rect) {
        Paint paint = new Paint();

        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(color);
        paint.setStrokeWidth(2F);

        ImageRect inside = rect.insetRectF(1F);

        // Line NW -> SE "\"
        canvas.drawLine(inside.left, inside.top, inside.right, inside.bottom, paint);
        // Line SW -> NE "/"
        canvas.drawLine(inside.left, inside.bottom, inside.right, inside.top, paint);
        // Midpoint Line X "|"
        canvas.drawLine(inside.midX(), inside.top, inside.midX(), inside.bottom, paint);
        // Midpoint Line Y "-"
        canvas.drawLine(inside.left, inside.midY(), inside.right, inside.midY(), paint);
        // Box
        canvas.drawRect(inside, paint);
    }

    public static void spot(Canvas canvas, float x, float y) {
        float radius = 15F;
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#88ff8888"));
        RectF r = new RectF();
        r.left = x + 1F;
        r.top = y + 1F;
        r.right = r.left + radius;
        r.bottom = r.top + radius;
        canvas.drawRect(r, paint);

        r.bottom = y - 1F;
        r.right = x - 1F;
        r.top = r.bottom - radius;
        r.left = r.right - radius;
        canvas.drawRect(r, paint);

        paint.setColor(Color.parseColor("#8888ff88"));

        r.left = x + 1F;
        r.bottom = y - 1F;
        r.right = r.left + radius;
        r.top = r.bottom - radius;
        canvas.drawRect(r, paint);

        r.right = x - 1F;
        r.top = y + 1F;
        r.left = r.right - radius;
        r.bottom = r.top + radius;
        canvas.drawRect(r, paint);
    }

    private DrawableDebug() {
        /* ignore */
    }
}
