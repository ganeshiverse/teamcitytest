package com.iversecomics.client.drawable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;

import com.iversecomics.client.bitmap.ImageRect;

public class WarningTapeDrawable extends Drawable {
    private String    text;
    private boolean   textRepeat;
    private TextPaint paintText;
    private Paint     paintTape;
    private Paint     paintTapeShadow;
    private float     angle;
    private float     margin;
    private float     textSize;
    private float     textHeight;
    private float     textWidth;
    private float     tapeHeight;
    private float     textBaseline;
    private Matrix    matrix;
    private RectF     tapeRect;
    private RectF     tapeShadowRect;
    private RectF     clipRect;

    public WarningTapeDrawable() {
        super();

        text = "WARNING";
        textRepeat = true;
        angle = -15.0f;
        textSize = 18.0f;
        margin = textSize * 0.75f;

        paintText = new TextPaint();
        paintText.setAntiAlias(true);
        paintText.setColor(Color.BLACK);
        paintText.setTextSize(textSize);
        paintText.setTypeface(Typeface.DEFAULT_BOLD);

        paintTape = new Paint();
        paintTape.setAntiAlias(true);
        paintTape.setStyle(Paint.Style.FILL);

        paintTapeShadow = new Paint();
        paintTapeShadow.setAntiAlias(true);
        paintTapeShadow.setStyle(Paint.Style.FILL);

        recalcTextSizes();
        recalcSizes(getBounds());
    }

    private LinearGradient createLinearGradient(RectF rect, int colorTop, int colorBottom) {
        return new LinearGradient(rect.left, rect.top, rect.left, rect.bottom, colorTop, colorBottom, TileMode.CLAMP);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();

        canvas.clipRect(clipRect);

        Matrix m = new Matrix();
        m.set(canvas.getMatrix());
        m.preConcat(matrix);
        canvas.setMatrix(m);

        // Draw tape shadow
        canvas.drawRect(tapeShadowRect, paintTapeShadow);

        // Draw tape
        canvas.drawRect(tapeRect, paintTape);

        // Draw text
        float offx = margin + margin;
        do {
            canvas.drawText(text, offx, textBaseline, paintText);
            offx += margin + textWidth;
        } while (textRepeat && (offx < tapeRect.width()));
        canvas.restore();
    }

    public float getAngle() {
        return angle;
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

    public String getText() {
        return text;
    }

    public float getTextSize() {
        return textSize;
    }

    public boolean isTextRepeat() {
        return textRepeat;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        recalcSizes(bounds);
    }

    private void recalcSizes(Rect bounds) {
        ImageRect ir = new ImageRect(bounds);
        clipRect = ir.insetRectF(1f);

        float tapeWidth = (float) Math.sqrt((ir.width() * ir.width()) + (ir.height() * ir.height())) + textHeight;
        tapeRect = new RectF(0, 0, tapeWidth, tapeHeight);

        float shadowOffset = textHeight * 0.5f;
        float shadowTop = tapeHeight - 1f;
        tapeShadowRect = new RectF(0, shadowTop, tapeWidth, shadowTop + shadowOffset);

        matrix = new Matrix();
        matrix.preRotate(angle);
        matrix.postTranslate(-textHeight, ir.midY());

        int colorTop = Color.parseColor("#fdff75");
        int colorBottom = Color.parseColor("#d4d562");
        paintTape.setShader(createLinearGradient(tapeRect, colorTop, colorBottom));

        colorTop = Color.DKGRAY;
        colorBottom = Color.TRANSPARENT;
        paintTapeShadow.setShader(createLinearGradient(tapeShadowRect, colorTop, colorBottom));
    }

    private void recalcTextSizes() {
        Rect textBounds = new Rect();
        paintText.getTextBounds(text, 0, text.length(), textBounds);

        textHeight = textBounds.height();
        textWidth = textBounds.width();
        tapeHeight = textHeight + margin + margin;
        textBaseline = margin + textHeight;
    }

    @Override
    public void setAlpha(int alpha) {
        /* ignore */
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        /* ignore */
    }

    public void setText(String text) {
        this.text = text;
        recalcTextSizes();
    }

    public void setTextRepeat(boolean textRepeat) {
        this.textRepeat = textRepeat;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        this.margin = textSize * 0.75f;

        paintText.setTextSize(textSize);
        recalcTextSizes();
        recalcSizes(getBounds());
    }
}
