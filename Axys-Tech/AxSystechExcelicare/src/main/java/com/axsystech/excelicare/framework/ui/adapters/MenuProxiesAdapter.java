package com.axsystech.excelicare.framework.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.data.responses.ProxieDetails;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Date: 06.05.14
 * Time: 13:37
 *
 * @author SomeswarReddy
 */
public class MenuProxiesAdapter extends BaseExpandableListAdapter {
    private Context context;
    private CaredMemberClickListener caredMemberClickListener;
    private final LayoutInflater mLayoutInflater;
    private ArrayList<ProxieDetails> mProxieDetailsAL;

    public MenuProxiesAdapter(final Context context,CaredMemberClickListener caredMemberClickListener, ArrayList<ProxieDetails> proxieDetailsAL) {
        this.context=context;
        this.caredMemberClickListener = caredMemberClickListener;
        mLayoutInflater = LayoutInflater.from(AxSysTechApplication.getAppContext());
        this.mProxieDetailsAL = proxieDetailsAL;
    }

    public void updateProxieDetails(ArrayList<ProxieDetails> proxieDetailsAL) {
        this.mProxieDetailsAL = proxieDetailsAL;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return (mProxieDetailsAL != null && mProxieDetailsAL.size() > 0) ? mProxieDetailsAL.size() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mProxieDetailsAL.get(groupPosition);
    }

    @Override
    public AppMenuListResponse.Items getChild(int groupPosition, int childPosition) {
        return null;// not required as we return '0' for children count
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ProxieDetails proxieDetails = (ProxieDetails) getGroup(groupPosition);

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.leftmenu_header_view, null);
        }

        final ImageView leftmenuImageView = (ImageView) convertView.findViewById(R.id.leftmenuImageView);
        TextView menuItemGroupNameTextView = (TextView) convertView.findViewById(R.id.menuItemGroupNameTextView);
        ImageView collapsableImageView = (ImageView) convertView.findViewById(R.id.collapsableImageView);
        TextView proxyEmail = (TextView) convertView.findViewById(R.id.proxyEmail);
        Typeface regular = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(context.getAssets(), "fonts/if_std_bold.ttf");
        menuItemGroupNameTextView.setTypeface(regular);
        proxyEmail.setTypeface(regular);

        if (proxieDetails != null) {
            if (collapsableImageView != null)
                collapsableImageView.setVisibility(View.GONE);
            if (menuItemGroupNameTextView != null)
                menuItemGroupNameTextView.setText(proxieDetails.getSurName() + " " + proxieDetails.getForeName());
            if (proxyEmail != null) {
                proxyEmail.setVisibility(View.GONE);
                proxyEmail.setText(proxieDetails.getUserCircleMemberEmail_ID());
            }

            if (proxieDetails.getImageURL() != null && !TextUtils.isEmpty(proxieDetails.getImageURL())) {
                String imagePath = proxieDetails.getImageURL();
                if (!TextUtils.isEmpty(imagePath)) {
                    if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                        imagePath = "file://" + imagePath;
                    }
                }

                if (!TextUtils.isEmpty(imagePath)) {
                    if (leftmenuImageView != null)
                        leftmenuImageView.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(CommonUtils.dpToPx(50), CommonUtils.dpToPx(50));
                    if (leftmenuImageView != null)
                        leftmenuImageView.setLayoutParams(params);

                    Picasso.with(AxSysTechApplication.getAppContext())
                            .load(imagePath)
                            .placeholder(R.drawable.user_icon)
                            .error(R.drawable.user_icon)
                            .transform(new CircleTransform())
                            .into(leftmenuImageView);
                } else {
                    if (leftmenuImageView != null)
                        leftmenuImageView.setVisibility(View.GONE);
                }
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (caredMemberClickListener != null) {
                    caredMemberClickListener.onClickCaredMember(proxieDetails);
                }
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    public interface CaredMemberClickListener {
        public void onClickCaredMember(ProxieDetails proxieDetails);
    }

}
