package com.axsystech.excelicare.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;

import android.os.Environment;
import android.util.Log;

public class Trace {

    public static final int NONE = 0;
    public static final int ERRORS_ONLY = 1;
    public static final int ERRORS_WARNINGS = 2;
    public static final int ERRORS_WARNINGS_INFO = 3;
    public static final int ERRORS_WARNINGS_INFO_DEBUG = 4;

    // Errors + warnings + info + debug (default)
    private static final int LOGGING_LEVEL = ERRORS_WARNINGS_INFO_DEBUG;

    private static final boolean mShouldLogToFile = false;

    public static void e(String tag, String msg) {
        if (LOGGING_LEVEL >= 1) {
            Log.e(tag, msg);
            writeLogToFile(msg);
        }
    }

    public static void e(String tag, String msg, Exception e) {
        if (LOGGING_LEVEL >= 1) {
            Log.e(tag, msg, e);
            writeLogToFile(msg);
        }
    }

    public static void w(String tag, String msg) {
        if (LOGGING_LEVEL >= 2) {
            Log.w(tag, msg);
            writeLogToFile(msg);
        }
    }

    public static void i(String tag, String msg) {
        if (LOGGING_LEVEL >= 3) {
            Log.i(tag, msg);
            writeLogToFile(msg);
        }
    }

    public static void d(String tag, String msg) {
        if (LOGGING_LEVEL >= 4) {
            Log.d(tag, msg);
            writeLogToFile(msg);
        }
    }

    public static void writeLogToFile(String text) {
        if (!mShouldLogToFile) {
            return;
        }

        File logFile = new File(Environment.getExternalStorageDirectory()
                + "/" + AxSysTechApplication.getAppContext().getString(R.string.app_name) + ".txt");

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
