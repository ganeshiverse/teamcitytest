package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by vvydesai on 9/1/2015.
 */
public class InvitationReceivedDetails implements Serializable {

    @SerializedName("Invite_Id")
    private String Invite_Id;

    @SerializedName("MessageReceivedStatus_SLU")
    private String MessageReceivedStatus_SLU;

    @SerializedName("MessageSentStatus_SLU")
    private String MessageSentStatus_SLU;

    @SerializedName("MessageText")
    private String MessageText;

    @SerializedName("RecipientEmailAddress")
    private String RecipientEmailAddress;

    @SerializedName("RecipientForeName")
    private String RecipientForeName;

    @SerializedName("RecipientSurname")
    private String RecipientSurname;

    @SerializedName("Role_SLU")
    private String Role_SLU;

    @SerializedName("SendDate")
    private String SendDate;

    @SerializedName("SenderForeName")
    private String SenderForeName;

    @SerializedName("SenderName")
    private String SenderName;

    @SerializedName("SenderSurName")
    private String SenderSurName;

    @SerializedName("SenderUserID")
    private String SenderUserID;

    @SerializedName("StatusUpdatedDate")
    private String StatusUpdatedDate;

    @SerializedName("URL")
    private String URL;

    @SerializedName("UserCircle_ID")
    private String UserCircle_ID;


    public String getInvite_Id() {
        return Invite_Id;
    }

    public String getMessageReceivedStatus_SLU() {
        return MessageReceivedStatus_SLU;
    }

    public String getMessageSentStatus_SLU() {
        return MessageSentStatus_SLU;
    }

    public String getMessageText() {
        return MessageText;
    }

    public String getRecipientEmailAddress() {
        return RecipientEmailAddress;
    }

    public String getRecipientForeName() {
        return RecipientForeName;
    }

    public String getRecipientSurname() {
        return RecipientSurname;
    }

    public String getRole_SLU() {
        return Role_SLU;
    }

    public String getSendDate() {
        return SendDate;
    }

    public String getSenderForeName() {
        return SenderForeName;
    }

    public String getSenderName() {
        return SenderName;
    }

    public String getSenderSurName() {
        return SenderSurName;
    }

    public String getSenderUserID() {
        return SenderUserID;
    }

    public String getStatusUpdatedDate() {
        return StatusUpdatedDate;
    }

    public String getURL() {
        return URL;
    }

    public String getUserCircle_ID() {
        return UserCircle_ID;
    }


}