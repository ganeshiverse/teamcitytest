package com.babylon.utils;


import com.babylon.R;

/**
 * Created by Rahul.
 */
public enum ActionBarType
{
    CREATE_ACCOUNT (R.color.white, R.color.purple_top_bar, R.drawable.ic_back_arrow_purple, R.string.create_your_account_title),
    SIGNIN (R.color.white, R.color.purple_top_bar, R.drawable.ic_back_arrow_purple                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 , R.string.sign_in),
    RESET_PASSWORD (R.color.white, R.color.purple_top_bar, R.drawable.ic_back_arrow_purple, R.string.reset_password_title),
    SETTINGS(R.color.white, R.color.orange_bright,R.drawable.selector_orange_back_button, R.string.settings),
    INSURANCE(R.color.white, R.color.orange_bright, R.drawable.selector_orange_back_button, R.string.insurance_fragment_title),
    AboutApp(R.color.white, R.color.purple_top_bar, R.drawable.ic_back_arrow_purple, R.string.about_babylon_title),
    RATING_SCREEN(R.color.white, R.color.purple_top_bar, R.drawable.ic_back_arrow_purple, R.string.rating_screen_title);



    private int mBackgroundColourResId;
    private int mMenuBackgroundColourResId;
    private int mIconResId;
    private int mTitleResId;

    private ActionBarType(int backgroundColourResId, int menuBackgroundColourResId, int iconResId, int titleResId)
    {
        mBackgroundColourResId = backgroundColourResId;
        mMenuBackgroundColourResId = menuBackgroundColourResId;
        mIconResId = iconResId;
        mTitleResId = titleResId;
    }

    public int getBackgroundColourResId()
    {
        return mBackgroundColourResId;
    }

    public int getMenuBackgroundColourResId()
    {
        return mMenuBackgroundColourResId;
    }

    public int getIconResId()
    {
        return mIconResId;
    }

    public int getTitleResId()
    {
        return mTitleResId;
    }
}