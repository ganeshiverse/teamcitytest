package com.babylon.model.payments;

import com.google.gson.annotations.SerializedName;

public class NewPaymentCard {
    @SerializedName("number")
    private String cardNumber;
    //format: MM/yyyy
    @SerializedName("expiration_date")
    private String expiryDateString;
    private String cvv;

    public NewPaymentCard(String cardNumber, String cvv, String expiryDateString) {
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.expiryDateString = expiryDateString;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getExpiryDate() { return expiryDateString; }

    public void setExpiryDate(String expiryDateString) { this.expiryDateString = expiryDateString; }
}
