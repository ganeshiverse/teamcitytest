package com.axsystech.excelicare.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.axsystech.excelicare.data.responses.GCMResponse;
import com.axsystech.excelicare.framework.ui.activities.LoginActivity;
import com.axsystech.excelicare.network.ServerMethods;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by alanka on 10/14/2015.
 */
public class RegisterApp extends AsyncTask<Void, Void, GCMResponse> {

    private static final String TAG = "GCMRelated";
    Context ctx;
    GoogleCloudMessaging gcm;
    String SENDER_ID = "61570760270";
    String regid = null;
    private int appVersion;
    String token;

    public RegisterApp(Context ctx, GoogleCloudMessaging gcm, String token,int appVersion) {
        this.ctx = ctx;
        this.gcm = gcm;
        this.appVersion = appVersion;
        this.token = token;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected GCMResponse doInBackground(Void... arg0) {
        String msg = "";
        GCMResponse rsponse = null;

        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(ctx);
            }
            regid = gcm.register(SENDER_ID);
            msg = "Device registered, registration ID=" + regid;

            // You should send the registration ID to your server over HTTP,
            // so it can use GCM/HTTP or CCS to send messages to your app.
            // The request to your server should be authenticated if your app
            // is using accounts.
            rsponse = sendRegistrationIdToBackend();

            // For this demo: we don't need to send it because the device
            // will send upstream messages to a server that echo back the
            // message using the 'from' address in the message.

            // Persist the regID - no need to register again.
            storeRegistrationId(ctx, regid);
        } catch (IOException ex) {
            msg = "Error :" + ex.getMessage();
            // If there is an error, don't just keep trying to register.
            // Require the user to click a button again, or perform
            // exponential back-off.
        }
        return rsponse;

    }

    private void storeRegistrationId(Context ctx, String regid) {
        final SharedPreferences prefs = ctx.getSharedPreferences(LoginActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
        Log.i(TAG, "Saving regId on app version" + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("registration_id", regid);
        editor.putInt("appVersion", appVersion);
        editor.commit();

    }


    private GCMResponse sendRegistrationIdToBackend(){
        JSONObject dataObject = new JSONObject();
        try {

            dataObject.put("Device", "ANDROID");
            dataObject.put("Token", token );
            dataObject.put("deviceToken", regid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ServerMethods serverMethods = ServerMethods.getInstance();
        try {
            return serverMethods.postGcmRegistration(dataObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPostExecute(GCMResponse result) {
        super.onPostExecute(result);
        if (result != null){
            Toast.makeText(ctx, result.message, Toast.LENGTH_SHORT).show();
            Log.v(TAG, result.message);
        }
    }

}