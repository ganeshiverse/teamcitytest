package com.babylon.recognition;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.babylon.App;
import com.babylon.R;
import com.babylon.service.ActivityRecognitionService;
import com.babylon.utils.Constants;
import com.babylon.utils.L;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.ActivityRecognitionClient;

/**
 * Class for connecting to Location Services and activity recognition updates.
 * <b>
 * Note: Clients must ensure that Google Play services is available before requesting updates.
 * </b> Use GooglePlayServicesUtil.isGooglePlayServicesAvailable() to check.
 *
 *
 * To use a DetectionRequester, instantiate it and call requestUpdates(). Everything else is done
 * automatically.
 *
 */
public class DetectionRequester implements ConnectionCallbacks, OnConnectionFailedListener {

    private static final String TAG = DetectionRequester.class.getSimpleName();

    private Context mContext;

    private PendingIntent mActivityRecognitionPendingIntent;

    private ActivityRecognitionClient mActivityRecognitionClient;

    public DetectionRequester(Context context) {
        mContext = context;
        mActivityRecognitionPendingIntent = null;
        mActivityRecognitionClient = null;

    }
    /**
     * Returns the current PendingIntent to the caller.
     *
     * @return The PendingIntent used to request activity recognition updates
     */
    public PendingIntent getRequestPendingIntent() {
        return mActivityRecognitionPendingIntent;
    }

    /**
     * Sets the PendingIntent used to make activity recognition update requests
     * @param intent The PendingIntent
     */
    public void setRequestPendingIntent(PendingIntent intent) {
        mActivityRecognitionPendingIntent = intent;
    }

    /**
     * Start the activity recognition update request process by
     * getting a connection.
     */
    public void requestUpdates() {
        requestConnection();
    }

    /**
     * Make the actual update request. This is called from onConnected().
     */
    private void continueRequestActivityUpdates() {
        getActivityRecognitionClient().requestActivityUpdates(
                Constants.DETECTION_INTERVAL_MILLISECONDS,
                createRequestPendingIntent());
        requestDisconnection();
    }

    /**
     * Request a connection to Location Services. This call returns immediately,
     * but the request is not complete until onConnected() or onConnectionFailure() is called.
     */
    private void requestConnection() {
        getActivityRecognitionClient().connect();
    }

    /**
     * Get the current activity recognition client, or create a new one if necessary.
     * This method facilitates multiple requests for a client, even if a previous
     * request wasn't finished. Since only one client object exists while a connection
     * is underway, no memory leaks occur.
     *
     * @return An ActivityRecognitionClient object
     */
    private ActivityRecognitionClient getActivityRecognitionClient() {
        if (mActivityRecognitionClient == null) {

            mActivityRecognitionClient =
                    new ActivityRecognitionClient(mContext, this, this);
        }
        return mActivityRecognitionClient;
    }

    /**
     * Get the current activity recognition client and disconnect from Location Services
     */
    private void requestDisconnection() {
        getActivityRecognitionClient().disconnect();
    }

    /*
     * Called by Location Services once the activity recognition client is connected.
     *
     * Continue by requesting activity updates.
     */
    @Override
    public void onConnected(Bundle arg0) {
        L.i(TAG, mContext.getString(R.string.location_service_connected));
        continueRequestActivityUpdates();
    }

    /*
     * Called by Location Services once the activity recognition client is disconnected.
     */
    @Override
    public void onDisconnected() {
        // In debug mode, log the disconnection
        L.i(TAG, mContext.getString(R.string.location_service_disconnected));

        // Destroy the current activity recognition client
        mActivityRecognitionClient = null;
    }

    /**
     * Get a PendingIntent to send with the request to get activity recognition updates. Location
     * Services issues the Intent inside this PendingIntent whenever a activity recognition update
     * occurs.
     *
     * @return A PendingIntent for the IntentService that handles activity recognition updates.
     */
    private PendingIntent createRequestPendingIntent() {
        if (null != getRequestPendingIntent()) {
            return mActivityRecognitionPendingIntent;
        } else {
            Intent intent = new Intent(mContext, ActivityRecognitionService.class);
            /*
             * Return a PendingIntent to start the IntentService.
             * Always create a PendingIntent sent to Location Services
             * with FLAG_UPDATE_CURRENT, so that sending the PendingIntent
             * again updates the original. Otherwise, Location Services
             * can't match the PendingIntent to requests made with it.
             */
            PendingIntent pendingIntent = PendingIntent.getService(mContext, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            setRequestPendingIntent(pendingIntent);
            return pendingIntent;
        }

    }

    /*
     * Implementation of OnConnectionFailedListener.onConnectionFailed
     * If a connection or disconnection request fails, report the error
     * connectionResult is passed in from Location Services
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
//            try {
                sendConnectionFailureBroadcast();
//                connectionResult.startResolutionForResult((Activity) mContext,
//                        Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);
//            } catch (SendIntentException e) {
//                L.e(TAG, e.toString());
//            }
        } else {
//            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
//                    connectionResult.getErrorCode(),
//                    (Activity) mContext,
//                    Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);
//            if (dialog != null) {
//                dialog.show();
//            }
        }
    }

    private void sendConnectionFailureBroadcast() {
        Intent intent = new Intent(Constants.CONNECTION_FAILURE_FILTER);
        intent.setAction(Constants.DETECTIONREQUESTER_CONNECTION_FAILURE);
        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(intent);
    }
}

