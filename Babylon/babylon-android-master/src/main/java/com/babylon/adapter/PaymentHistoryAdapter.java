package com.babylon.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.controllers.BirthdayViewController;
import com.babylon.model.Region;
import com.babylon.model.payments.Transaction;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class PaymentHistoryAdapter extends ArrayAdapter<Object> {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());

    private final List<Transaction> payments;
    private final Context context;



    public PaymentHistoryAdapter(Context context, List<Transaction> payments) {
        super(context, R.layout.list_item_payment_history, payments.toArray());
        this.context = context;
        this.payments = payments;
    }


    @Override
    public int getCount() {
        return payments.size();
    }

    @Override
    public Object getItem(int position) {
        return payments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.list_item_payment_history, null);

            viewHolder.dateTextView = (TextView) convertView.findViewById(R.id.payment_date_text_view);
            viewHolder.planTextView = (TextView) convertView.findViewById(R.id.payment_plan_text_view);
            viewHolder.methodTextView = (TextView) convertView.findViewById(R.id.payment_method_text_view);
            viewHolder.amountTextView = (TextView) convertView.findViewById(R.id.payment_amount_text_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Transaction transaction = payments.get(position);

        // Date
        viewHolder.dateTextView.setText(simpleDateFormat.format(transaction.getCreatedAt()));

        // Type
        // TODO: timpreuss: I don't think the "payment_type" should be used here (it's a Rails polymorphic field)
        viewHolder.planTextView.setText(transaction.getPurchaseType());

        // Method
        String methodText = (transaction.getCreditCard() == null) ? "Free" : transaction.getCreditCard().getDisplayName();
        viewHolder.methodTextView.setText(methodText);

        // Amount
        Region region = App.getInstance().getCurrentRegion();
        String amountText = region.getFormattedPriceString(transaction.getPrice());
        viewHolder.amountTextView.setText(amountText);

        return convertView;
    }

    private static class ViewHolder {
        TextView dateTextView;
        TextView planTextView;
        TextView methodTextView;
        TextView amountTextView;
    }
}
