package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestDelete;
import com.babylon.api.request.base.Urls;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ActivityListDelete extends AbsRequestDelete<Void> {

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.ACTIVITY_LIST);
        return TextUtils.concat(url, urlParams).toString();
    }

    public static ActivityListDelete newActivityListDelete(List<String> deleteIds) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        int index = 0;
        for (String idString : deleteIds) {
            if (idString != null) {
                params.add(new BasicNameValuePair("activityIds[" + index + "]", idString));
            }
            index++;
        }
        return (ActivityListDelete) new ActivityListDelete().withParams(params);
    }
}
