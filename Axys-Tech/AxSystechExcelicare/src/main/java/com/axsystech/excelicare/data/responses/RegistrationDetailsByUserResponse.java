package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Date: 25.09.15
 * Time: 13:19
 *
 * @author SomeswarReddy
 */
public class RegistrationDetailsByUserResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private DataObject dataObject;

    public DataObject getDataObject() {
        return dataObject;
    }

    public class DataObject implements Serializable {

        @SerializedName("NavigateTo")
        private String navigateTo;

        @SerializedName("UserDetails")
        private ArrayList<UsersDetailsObject> userDetailsAL;

        @SerializedName("SystemPreferences")
        private SystemPreferencesObject systemPreferences;

        public String getNavigateTo() {
            return navigateTo;
        }

        public ArrayList<UsersDetailsObject> getUserDetailsAL() {
            return userDetailsAL;
        }

        public SystemPreferencesObject getSystemPreferences() {
            return systemPreferences;
        }
    }

}
