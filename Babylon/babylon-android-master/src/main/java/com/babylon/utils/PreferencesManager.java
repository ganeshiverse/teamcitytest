package com.babylon.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.model.MonitorMeCategory;
import com.babylon.model.Region;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class PreferencesManager {
    private SharedPreferences preferences;
    private static PreferencesManager preferencesManager = null;

    public static PreferencesManager getInstance(Context c) {
        if (preferencesManager == null) {
            preferencesManager = new PreferencesManager(c);
        }
        return preferencesManager;
    }

    public static PreferencesManager getInstance() {
        if (preferencesManager == null) {
            preferencesManager = new PreferencesManager(App.getInstance());
        }
        return preferencesManager;
    }

    private PreferencesManager(Context c) {
        preferences = PreferenceManager.getDefaultSharedPreferences(c);
    }

    private SharedPreferences getPreferences() {
        return preferences;
    }

    public void clear() {
        preferences.edit().clear().apply();
    }

    public Boolean isFirstLaunch() {
        return getBoolean(Keys.IS_FIRST_LAUNCH, true);
    }

    public void setIsFirstLaunch(boolean value) {
        setBoolean(Keys.IS_FIRST_LAUNCH, value);
    }


    public Boolean isFirstLaunchPromo() {
        return getBoolean(Keys.IS_FIRST_LAUNCH_PROMO, true);
    }

    public void setIsFirstPromo(boolean value) {
        setBoolean(Keys.IS_FIRST_LAUNCH_PROMO, value);
    }

    public Boolean isFirstLogin() {
        return getBoolean(Keys.IS_FIRST_LOGIN, true);
    }

    public void setIsFirstLogin(boolean value) {
        setBoolean(Keys.IS_FIRST_LOGIN, value);
    }

    public Boolean isFirstLaunchValidic() {
        return getBoolean(Keys.IS_FIRST_LAUNCH_VALIDIC, true);
    }

    public void setIsFirstLaunchValidic(boolean value) {
        setBoolean(Keys.IS_FIRST_LAUNCH_VALIDIC, value);
    }

    public Boolean isUserActive() {
        return getBoolean(Keys.IS_USER_ACTIVE, true);
    }

    public void setUserActive(boolean value) {
        setBoolean(Keys.IS_USER_ACTIVE, value);
    }

    public void setShouldShowTermsPopup(boolean value) {
        setBoolean(Keys.SHOULD_SHOW_TERMS_POPUP, value);
    }

    public boolean shouldShowTermsPopup() {
        return getBoolean(Keys.SHOULD_SHOW_TERMS_POPUP, false);
    }

    public void setPatientAddress(String value) {
        setString(Keys.PATIENT_ADDRESS, value);
    }

    public String getPatientAddress() {
       return getString(Keys.PATIENT_ADDRESS, null);
    }

    public void setPatientAddressId(int value) {
        setInt(Keys.PATIENT_ADDRESS_ID, value);
    }

    public int getPatientAddressId() {
        return getInt(Keys.PATIENT_ADDRESS_ID, 0);
    }


    public void clearSharedPreferences() {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.clear();
        editor.apply();
    }


    public int getInt(String key, int defaultValue) {
        return getPreferences().getInt(key, defaultValue);
    }



    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public long getLong(String key, long defaultValue) {
        return getPreferences().getLong(key, defaultValue);
    }

    public void setLong(String key, long value) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public float getFloat(String key, float defaultValue) {
        return getPreferences().getFloat(key, defaultValue);
    }

    public void setFloat(String key, float value) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public String getString(String key, String defaultValue) {
        return getPreferences().getString(key, defaultValue);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return getPreferences().getBoolean(key, defaultValue);
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public int getAppLaunchCount()
    {
        return getPreferences().getInt(Keys.APP_LAUNCH_COUNT, 1);
    }

    public void incrementAppLaunchCount()
    {
        int current_count = getPreferences().getInt(Keys.APP_LAUNCH_COUNT,1);
        current_count ++;
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.APP_LAUNCH_COUNT, current_count);
        editor.apply();
    }

    public void resetAppLaunchCount()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.APP_LAUNCH_COUNT,1);
        editor.apply();
    }

    public int getQuestionsAskedCount()
    {
        return getPreferences().getInt(Keys.QUESTIONS_ASKED_COUNT,1);
    }

    public void incrementQuestionsAskedCount()
    {
        int current_count = getPreferences().getInt(Keys.QUESTIONS_ASKED_COUNT,1);
        current_count ++;
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.QUESTIONS_ASKED_COUNT,current_count);
        editor.apply();
    }

    public void resetQuestionsAskedCount()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.QUESTIONS_ASKED_COUNT,1);
        editor.apply();
    }

    public int getNotificationsCount()
    {
        return getPreferences().getInt(Keys.NOTIFICATIONS_COUNT,1);
    }

    public void incrementNotificationsCount()
    {
        int current_count = getPreferences().getInt(Keys.NOTIFICATIONS_COUNT,1);
        current_count ++;
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.NOTIFICATIONS_COUNT,current_count);
        editor.apply();
    }

    public void resetNotificationsCount()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.NOTIFICATIONS_COUNT,1);
        editor.apply();
    }

    public int getAnswersReadCount()
    {
        return getPreferences().getInt(Keys.ANSWERS_READ_COUNT,1);
    }

    public void incrementAnswersReadCount()
    {
        int current_count = getPreferences().getInt(Keys.ANSWERS_READ_COUNT,1);
        current_count ++;
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.ANSWERS_READ_COUNT,current_count);
        editor.apply();
    }

    public void resetAnswersCount()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(Keys.ANSWERS_READ_COUNT,1);
        editor.apply();
    }

    public String getLastNotificationDate()
    {
        return getPreferences().getString(Keys.LAST_NOTIFICATION_DATE,getDateString());
    }

    public void setLastNotificationDate()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(Keys.LAST_NOTIFICATION_DATE,getDateString());
        editor.apply();
    }

    public String getLastAnswerReadDate()
    {
        return getPreferences().getString(Keys.LAST_ANSWER_READ_DATE,getDateString());
    }

    public void setLastAnswerReadDate()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(Keys.LAST_ANSWER_READ_DATE,getDateString());
        editor.apply();
    }

    public String getLastQuestionAskedDate()
    {
        return getPreferences().getString(Keys.LAST_QUESTION_ASKED_DATE,getDateString());
    }

    public void setLastQuestionAskedDate()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(Keys.LAST_QUESTION_ASKED_DATE,getDateString());
        editor.apply();
    }

    public String getLastAppLaunchDate()
    {
        return getPreferences().getString(Keys.LAST_APP_LAUNCH_DATE,getDateString());
    }

    public void setLastAppLaunchDate()
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(Keys.LAST_APP_LAUNCH_DATE,getDateString());
        editor.apply();
    }

    public Boolean isTrackingActivity() {
        return getBoolean(Keys.KEY_IS_TRACKING_ACTIVITY, true);
    }

    public void setTrackingActivity(boolean value) {
        setBoolean(Keys.KEY_IS_TRACKING_ACTIVITY, value);
    }

    public void setCurrentActivityType(int currentActivityType) {
        setInt(Constants.KEY_CURRENT_ACTIVITY_TYPE, currentActivityType);
    }

    public int getCurrentActivityType() {
        return getInt(Constants.KEY_CURRENT_ACTIVITY_TYPE, -1);
    }

    public void setShowContactAlert(boolean isShowContactAlert) {
        setBoolean(Constants.IS_SHOW_CONTACT_ALERT, isShowContactAlert);
    }

    public boolean isShowContactAlert() {
        return getPreferences().getBoolean(Constants.IS_SHOW_CONTACT_ALERT, true);
    }

    public void setManualHandlingAutoSignOut(boolean isManualHandlingAutoSignOut) {
        setBoolean(Keys.IS_MANUAL_HANDLING_AUTO_SIGN_OUT, isManualHandlingAutoSignOut);
    }

    public boolean isManualHandlingAutoSignOut() {
        return getPreferences().getBoolean(Keys.IS_MANUAL_HANDLING_AUTO_SIGN_OUT, false);
    }

    public void setFirstSync(boolean isFirstSync) {
        setBoolean(Keys.IS_FIRST_SYNC, isFirstSync);
    }

    public boolean isFirstSync() {
        return getPreferences().getBoolean(Keys.IS_FIRST_SYNC, true);
    }

    public void setFirstMonitorMeLaunch(boolean isFirstSync) {
        setBoolean(Keys.IS_FIRST_LAUNCH_MONITOR_ME, isFirstSync);
    }

    public boolean isFirstMonitorMeLaunch() {
        return getPreferences().getBoolean(Keys.IS_FIRST_LAUNCH_MONITOR_ME, true);
    }

    public void setRegions(Region[] regions) {
        String regionString = GsonWrapper.getGson().toJson(regions);
        setString(Keys.REGIONS_JSON, regionString);
    }



    public Region[] getRegions() {
        Region[] regions = null;
        String regionString = getPreferences().getString(Keys.REGIONS_JSON, "");
        if (!TextUtils.isEmpty(regionString)) {
            regions = GsonWrapper.getGson().fromJson(regionString, Region[].class);
        }
        return regions;
    }

    public Object getObject(String key, Class className) {
        String walkingJson = PreferencesManager.getInstance().getString(key, "{}");
        Object physicalActivity = GsonWrapper.getGson().fromJson(walkingJson, className);
        return physicalActivity;
    }

    public void setObject(String key, Object object) {
        PreferencesManager.getInstance().setString(key, GsonWrapper.getGson().toJson
                (object));
    }

    public void setMonitorCategories(List<MonitorMeCategory> categories) {
        String categoriesString = GsonWrapper.getTimestampGson().toJson(categories);
        setString(Keys.ALL_MONITOR_CATEGORIES, categoriesString);
    }

    public List<MonitorMeCategory> getMonitorCategories() {
        List<MonitorMeCategory> categories = null;
        String categoriesString = getPreferences().getString(Keys.ALL_MONITOR_CATEGORIES, "");
        if (!TextUtils.isEmpty(categoriesString)) {
            final Type listType = new TypeToken<ArrayList<MonitorMeCategory>>(){}.getType();
            categories = GsonWrapper.getTimestampGson().fromJson(categoriesString, listType);
        }
        return categories;
    }

    public String getDateString()
    {
        DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss", Locale.UK);
        dateFormatter.setLenient(false);
        Date today = new Date();
        return dateFormatter.format(today);
    }
}
