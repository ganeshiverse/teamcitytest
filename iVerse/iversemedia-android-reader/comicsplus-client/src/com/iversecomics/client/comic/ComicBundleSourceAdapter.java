package com.iversecomics.client.comic;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iversecomics.bundle.ComicBundle;
import com.iversecomics.bundle.ComicContent;
import com.iversecomics.bundle.ComicContent.Nav;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.bundle.Ownership;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class ComicBundleSourceAdapter implements IMyComicSourceAdapter {
    private static final Logger  LOG = LoggerFactory.getLogger(ComicBundleSourceAdapter.class);
    private File                 comicFile;
    private ComicBundle          cb;
    private ComicContent         content;
    private ComicMode            mode;
    private IComicSourceListener comicSourceListener;

    public ComicBundleSourceAdapter(File comicFile) {
        this.comicFile = comicFile;
        this.cb = new ComicBundle();
        this.mode = ComicMode.PAGE;
        this.comicSourceListener = new LogComicSourceListener();
    }

    @Override
    public void close() throws IOException {
        this.content.close();
    }

    public File getComicFile() {
        return comicFile;
    }

    public ComicMetadata getMetadata() {
        return cb.getMetadata();
    }

    @Override
    public ComicMode getComicMode() {
        return mode;
    }

    @Override
    public int getImageCount() {
        if (content != null && content.getPageNav() != null) {
            switch (mode) {
                case PAGE:
                    return content.getPageNav().size();
                default:
                case PANEL:
                    return content.getPanelNav().size();
            }
        } else {
            return 0;
        }
    }

    @Override
    public URI getImageURI(int index) {
        try {
            // LOG.debug("getImageURI(%d) [%s]", index, mode.name());
            switch (mode) {
            case PAGE:
                return toImageSegment(content.getPageNav(), index);
            default:
            case PANEL:
                return toImageSegment(content.getPanelNav(), index);
            }
        } catch (IOException e) {
            LOG.warn(e, "Unable to get image uri (%d)", index);
            return null;
        }
    }


    @Override
    public Bitmap loadComicPage(int pageIndex) {
        URI bitmapUri = getImageURI(pageIndex);

        String ssp = bitmapUri.getSchemeSpecificPart();
        int idx = ssp.indexOf('/');
        String modeStr = ssp.substring(0, idx);
        String indexStr = ssp.substring(idx + 1);

        ComicMode cmode = ComicMode.fromType(modeStr);
        int index = Integer.parseInt(indexStr);
        // LOG.debug("loadBitmap %s -> %s/%d", bitmapUri.toASCIIString(), cmode.name(), index);

        InputStream in = null;
        Bitmap bitmap = null;
        try {
            switch (cmode) {
                case PAGE:
                    in = content.getPageNav().getImageStream(index);
                    break;
                case PANEL:
                    in = content.getPanelNav().getImageStream(index);
                    break;
            }
            if (in == null) {
                throw new IOException("Invalid InputStream for " + bitmapUri.toASCIIString());
            }

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;

            bitmap =  BitmapFactory.decodeStream(in, null, opts);
            if (comicSourceListener != null) {
                comicSourceListener.onBitmapLoaded(pageIndex, bitmap);
            }
        }
        catch (IOException e) {
            LOG.error("loadComicPage exception: " + e.getLocalizedMessage());
        }
        finally {
            IOUtil.close(in);
        }
        return bitmap;
    }


    @Override
    public void open() throws IOException {
        cb.parse(comicFile);
        content = cb.getContent();
        content.open();

        comicSourceListener.onComicSourceUpdated(this);
    }

    @Override
    public void setComicMode(ComicMode mode) {
        LOG.debug("Setting ComicMode to %s", mode.name());
        if (mode != this.mode) {
            this.mode = mode;
            comicSourceListener.onComicSourceUpdated(this);
        }
    }


    @Override
    public void setComicSourceListener(IComicSourceListener listener) {
        this.comicSourceListener = listener;
        if (comicSourceListener == null) {
            comicSourceListener = new LogComicSourceListener();
        }
    }

    public void setOwnership(Ownership ownership) {
        this.cb.setOwnership(ownership);
    }

    private URI toImageSegment(Nav nav, int index) throws IOException {
        String uri = String.format("comic:%s/%d", mode.name(), index);
        return URI.create(uri);
    }

	@Override
	public void onOpenFinished(boolean bSuccess) {
		if(bSuccess)
			this.comicSourceListener.onComicSourceUpdated(this);
	}
}
