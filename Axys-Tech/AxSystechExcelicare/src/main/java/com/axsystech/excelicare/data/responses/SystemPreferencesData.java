package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 29/09/15.
 */
public class SystemPreferencesData implements Serializable {
    @SerializedName("Description")
    private String description;

    @SerializedName("ID")
    private String id;

    @SerializedName("Value")
    private String value;

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}