package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;

public class FitbitSubscribePost extends AbsRequestPost<Void, FitbitSubscribePost.SubscribeResult> {

    @Override
    protected String getUrl() {
        return Urls.get(Urls.FITBIT_SUBSCRIBE_POST);
    }

    public static class SubscribeResult {
        public String url;
    }
}
