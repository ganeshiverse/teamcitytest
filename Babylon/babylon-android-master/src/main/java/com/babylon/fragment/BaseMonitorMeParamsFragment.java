package com.babylon.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.utils.Constants;

import java.util.Date;

public class BaseMonitorMeParamsFragment extends BaseFragment {
    protected long dateInMillis;
    protected boolean isChartLoaded = false;
    protected OnlyChartFragment.OnMedicalKitClickListener onMedicalKitClickListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof OnlyChartFragment.OnMedicalKitClickListener) {
            onMedicalKitClickListener = (OnlyChartFragment.OnMedicalKitClickListener) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle extraArgs = getArguments();
        if (extraArgs != null) {
            dateInMillis = extraArgs.getLong(Constants.DATE_IN_MILLIS);
        }
    }

    protected void showCurrentDate() {
        String dateString = new MonitorMeDateViewController().getFormattedDay(new Date(dateInMillis));
        ((TextView) getView().findViewById(R.id.dateTextView)).setText(dateString);
    }
}
