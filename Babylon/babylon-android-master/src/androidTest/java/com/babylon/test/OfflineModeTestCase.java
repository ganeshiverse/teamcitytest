package com.babylon.test;

import android.test.AndroidTestCase;

import com.babylon.enums.Screen;

public class OfflineModeTestCase extends AndroidTestCase {

    public void testIsAskEnableOffline() {
        assertTrue(Screen.ask.isOfflineModeSupported());
    }

    public void testIsAskHistoryEnableOffline() {
        assertTrue(Screen.ask_history.isOfflineModeSupported());
    }

    public void testIsMonitorMeEnableOffline() {
        assertFalse(Screen.monitorMe.isOfflineModeSupported());
    }

    public void testIsSubsriptionsEnableOffline() {
        assertFalse(Screen.subscriptions.isOfflineModeSupported());
    }
}