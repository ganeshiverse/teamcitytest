package com.babylon.api.request.family;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Account;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import org.apache.http.message.BasicHeader;

public class FamilyAccountListGet extends AbsRequestGet<Account[]> {

    @Override
    protected String getUrl() {

        return Urls.getRuby(Urls.ACCOUNTS);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<Account[]> getResponseProcessor() {
        return new AccountResponseProcessor<>(getOutputType());
    }
}
