package com.babylon.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GsonWrapper {
    private static Gson gson;
    private static Gson timestampGson;

    protected GsonWrapper() {
    }

    public static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new DateAdapter())
                    .registerTypeAdapter(java.sql.Date.class, new DateAdapter())
                    .create();
        }
        return gson;
    }

    public static Gson getTimestampGson() {
        if (timestampGson == null) {
            timestampGson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new TimestampDateAdapter())
                    .registerTypeAdapter(java.sql.Date.class, new TimestampDateAdapter())
                    .create();
        }
        return timestampGson;
    }

    static class DateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            Date result = null;
            if (jsonElement != null) {
                String stringDate = jsonElement.getAsString();
                if (stringDate.matches("[0-9]+")) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date(jsonElement.getAsLong() * 1000));
                    result = calendar.getTime();
                } else {
                    result = DateUtils.stringToDate(stringDate);
                }
            }
            return result;
        }

        @Override
        public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonElement result = null;
            if (date != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATABASE_DATE_FORMAT, Locale.getDefault());
                String stringDate = simpleDateFormat.format(date);
                result = new JsonPrimitive(stringDate);
            }
            return result;
        }
    }

    static class TimestampDateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement jsonElement, Type type,
                                JsonDeserializationContext jsonDeserializationContext)
                throws JsonParseException {
            Date result = null;
            if (jsonElement != null) {
                Long timestamp = jsonElement.getAsLong();
                result = DateUtils.getDateFromTimestamp(timestamp);
            }
            return result;
        }

        @Override
        public JsonElement serialize(Date date, Type type,
                                     JsonSerializationContext jsonSerializationContext) {
            JsonElement result = null;
            if (date != null) {
                Long timestamp = DateUtils.getTimestamp(date);
                result = new JsonPrimitive(timestamp);
            }
            return result;
        }
    }

}
