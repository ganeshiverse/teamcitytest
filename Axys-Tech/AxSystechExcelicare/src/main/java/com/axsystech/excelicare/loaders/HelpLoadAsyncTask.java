package com.axsystech.excelicare.loaders;

import android.os.AsyncTask;

import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.data.responses.HelpResponse;
import com.axsystech.excelicare.network.ServerMethods;

/**
 * Created by vvydesai on 9/14/2015.
 */
public class HelpLoadAsyncTask extends AsyncTask<Void, Void, HelpResponse> {

    private String moduleId;
    private LoadHelpListener loadHelpListener;

    public HelpLoadAsyncTask(String moduleId, LoadHelpListener loadHelpListener) {
        this.moduleId = moduleId;
        this.loadHelpListener = loadHelpListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected HelpResponse doInBackground(Void... params) {
        try {
            return ServerMethods.getInstance().getHelpInfo(moduleId, GlobalDataModel.getInstance().getActiveUser().getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(HelpResponse helpResponse) {
        super.onPostExecute(helpResponse);

        if (helpResponse != null) {
            if (helpResponse != null) {
                if (loadHelpListener != null) {
                    loadHelpListener.onHelpLoadSuccess(helpResponse);
                }
            } else {
                if (loadHelpListener != null) {
                    loadHelpListener.onHelpLoadFailure("Unable to load url, please try again");
                }
            }
        } else {
            if (loadHelpListener != null) {
                loadHelpListener.onHelpLoadFailure("Unable to load url, please try again");
            }
        }
    }

    public interface LoadHelpListener {
        public void onHelpLoadSuccess(HelpResponse helpResponse);

        public void onHelpLoadFailure(String errorMessage);
    }
}
