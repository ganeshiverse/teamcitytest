package com.axsystech.excelicare.framework.ui.messages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AttachmentDetails;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.MessageDetailsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.GetMsgDeatailsLoader;
import com.axsystech.excelicare.loaders.MessageDeleteLoader;
import com.axsystech.excelicare.loaders.SavePriorityLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class MessageDetailsFragment extends BaseFragment implements View.OnClickListener {

    private static final String MSG_DEATILS_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment.MSG_DEATILS_LOADER";
    private static final String MSG_DELETE_LOADER = "com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment.MSG_DELETE_LOADER";
    private static final String MSG_SAVE_PRIORITY = "com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment.MSG_SAVE_PRIORITY";

    private String TAG = MessageDetailsFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mMessageId;

    @InjectSavedState
    private String mPhotoUrl;

    @InjectView(R.id.profileImageView)
    private ImageView profileImageView;

    // @InjectView(R.id.nameTextView)
    private TextView nameTextView;

    // @InjectView(R.id.dateTextView)
    private TextView dateTextView;

    //@InjectView(R.id.specialityTextView)
    private TextView specialityTextView;

    // @InjectView(R.id.toRecipientsTextView)
    private TextView toRecipientsTextView;

    //@InjectView(R.id.toMoreRecipientsTextView)
    private TextView toMoreRecipientsTextView;

    //@InjectView(R.id.ccRecipientsTextView)
    private TextView ccRecipientsTextView;

    //@InjectView(R.id.ccMoreRecipientsTextView)
    private TextView ccMoreRecipientsTextView;

    @InjectView(R.id.attachLayout)
    private LinearLayout attachLayout;

    @InjectView(R.id.attachImageView)
    private ImageView attachImageView;

    @InjectView(R.id.priorityCheckBox)
    private CheckBox priorityCheckBox;

    //@InjectView(R.id.subjectTextView)
    private TextView subjectTextView;

    private TextView subjectTitle;

    // @InjectView(R.id.messageTextView)
    private TextView attachmentsTextView;
    private TextView attachFileNameTextView;

    @InjectView(R.id.attachmentsListView)
    private ListView attachmentsListView;

    private TextView messageTextView;

    @InjectView(R.id.replyImageButton)
    private ImageView replyImageButton;

    @InjectView(R.id.replyAllImageButton)
    private ImageView replyAllImageButton;

    @InjectView(R.id.linearLayoutMessageDetailsFooter)
    private LinearLayout linearLayoutMessageDetailsFooter;

    @InjectView(R.id.forwardImageButton)
    private ImageView forwardImageButton;

    @InjectView(R.id.deleteImageButton)
    private ImageView deleteImageButton;
    boolean sendMessage = true;

    private TextView ccTextView;

    private MessageDetailsResponse msgDetailsResponse;
    private AttachmentsListAdapter attachmentsListAdapter;
    private ArrayList<MessageDetailsResponse.AttachmentsObject> attachmentDetailsAL = new ArrayList<MessageDetailsResponse.AttachmentsObject>();

    private String mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;

    @Override
    protected void initActionBar() {
        // update actionbar title
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.message_details, container, false);

        nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        dateTextView = (TextView) view.findViewById(R.id.dateTextView);
        toRecipientsTextView = (TextView) view.findViewById(R.id.toRecipientsTextView);
        specialityTextView = (TextView) view.findViewById(R.id.specialityTextView);
        toMoreRecipientsTextView = (TextView) view.findViewById(R.id.toMoreRecipientsTextView);
        ccRecipientsTextView = (TextView) view.findViewById(R.id.ccRecipientsTextView);
        ccMoreRecipientsTextView = (TextView) view.findViewById(R.id.ccMoreRecipientsTextView);
        subjectTextView = (TextView) view.findViewById(R.id.subjectTextView);
        subjectTitle = (TextView) view.findViewById(R.id.subjectTitle);
        messageTextView = (TextView) view.findViewById(R.id.messageTextView);
        attachmentsTextView = (TextView) view.findViewById(R.id.attachmentsTextView);
        // attachFileNameTextView = (TextView)view.findViewById(R.id.attachFileNameTextView);
        ccTextView = (TextView) view.findViewById(R.id.ccTextView);
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

        subjectTitle.setTypeface(bold);
        nameTextView.setTypeface(bold);
        dateTextView.setTypeface(regular);
        toMoreRecipientsTextView.setTypeface(regular);
        specialityTextView.setTypeface(regular);
        toMoreRecipientsTextView.setTypeface(regular);
        ccRecipientsTextView.setTypeface(regular);
        ccMoreRecipientsTextView.setTypeface(regular);
        subjectTextView.setTypeface(regular);
        messageTextView.setTypeface(regular);
        attachmentsTextView.setTypeface(bold);
        // attachFileNameTextView.setTypeface(bold);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mLayoutInflater = LayoutInflater.from(mActivity);

        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Message Providers")) {
                    if (m.getValue() == 1) {
                        sendMessage = true;
                    } else if (m.getValue() == 0) {
                        sendMessage = false;
                    }
                }
            }
        }

        initViews();
        readIntentArgs();
        addEventListeners();
        preloadData();
    }

    private void initViews() {

    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_MESSAGE_ID)) {
                mMessageId = bundle.getString(AxSysConstants.EXTRA_MESSAGE_ID);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_MESSAGE_PHOTO_URL)) {
                mPhotoUrl = bundle.getString(AxSysConstants.EXTRA_MESSAGE_PHOTO_URL);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }

        }
    }

    private void addEventListeners() {
        replyImageButton.setOnClickListener(this);
        replyAllImageButton.setOnClickListener(this);
        forwardImageButton.setOnClickListener(this);
        deleteImageButton.setOnClickListener(this);
        priorityCheckBox.setOnClickListener(this);

        if (sendMessage) {
            linearLayoutMessageDetailsFooter.setVisibility(View.VISIBLE);
        } else {
            linearLayoutMessageDetailsFooter.setVisibility(View.GONE);
        }
    }

    private void preloadData() {
        attachmentsListAdapter = new AttachmentsListAdapter(attachmentDetailsAL);
        attachmentsListView.setAdapter(attachmentsListAdapter);
        setListViewHeightBasedOnChildren(attachmentsListView);

        if (CommonUtils.hasInternet()) {
            // Reset global data here
            displayProgressLoader(false);
            GetMsgDeatailsLoader msgdetailsLoader = new GetMsgDeatailsLoader(mActivity, mActiveUser.getToken(), mMessageId);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_DEATILS_LOADER), msgdetailsLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(MSG_DEATILS_LOADER)) {
            showToast(R.string.invalid_server_response);
        } else if (id == getLoaderHelper().getLoaderId(MSG_DELETE_LOADER)) {
            showToast(R.string.error_deleting_message);
        } else if (id == getLoaderHelper().getLoaderId(MSG_SAVE_PRIORITY)) {
            showToast(R.string.error_updating_priority);
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(MSG_DEATILS_LOADER)) {
            if (result != null && result instanceof MessageDetailsResponse) {
                msgDetailsResponse = (MessageDetailsResponse) result;

                // Update message details to UI
                updateMessageDetailsToUI(msgDetailsResponse);
            }
        } else if (id == getLoaderHelper().getLoaderId(MSG_DELETE_LOADER)) {
            // {"Status":"Success","message":"Message Deleted"}

            if (result != null && result instanceof BaseResponse) {
                final BaseResponse deleteMsgResponse = (BaseResponse) result;

                if (deleteMsgResponse.getStatus() != null) {
                    if (deleteMsgResponse.isSuccess()) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                if (!TextUtils.isEmpty(deleteMsgResponse.getMessage())) {
                                    showToast(deleteMsgResponse.getMessage());
                                }

                                getActivity().getSupportFragmentManager().popBackStackImmediate();
                            }
                        });
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(MSG_SAVE_PRIORITY)) {
            if (result != null && result instanceof BaseResponse) {
                BaseResponse baseResponse = (BaseResponse) result;

                if (baseResponse.isSuccess()) {
                    if (!TextUtils.isEmpty(baseResponse.getMessage())) {
                        showToast(baseResponse.getMessage());
                    }
                }
            }
        }
    }

    private void updateMessageDetailsToUI(MessageDetailsResponse msgDetailsResponse) {
        if (msgDetailsResponse != null && msgDetailsResponse.getDataObject() != null) {
            MessageDetailsResponse.DataObject msgDetailsDataObj = msgDetailsResponse.getDataObject();

            //get the image from url
            if (!TextUtils.isEmpty(mPhotoUrl)) {
                String imagePath = mPhotoUrl;
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                Picasso.with(mActivity)
                        .load(imagePath)
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .transform(new CircleTransform())
                        .into(profileImageView);
            }

            nameTextView.setText(!TextUtils.isEmpty(msgDetailsDataObj.getSenderName()) ? msgDetailsDataObj.getSenderName() : "");

            dateTextView.setText(!TextUtils.isEmpty(msgDetailsDataObj.getMsgDate()) ? msgDetailsDataObj.getMsgDate() : "");

            if (!TextUtils.isEmpty(msgDetailsDataObj.getSenderspeciality())) {
                specialityTextView.setText(msgDetailsDataObj.getSenderspeciality());
                specialityTextView.setVisibility(View.VISIBLE);
            } else {
                specialityTextView.setVisibility(View.GONE);
            }

            final ArrayList<MessageDetailsResponse.RecipientsObject> recipentslist = msgDetailsDataObj.getRecipentsArraylist();
            final ArrayList<MessageDetailsResponse.AttachmentsObject> attachmentsObjects = msgDetailsDataObj.getAttachmentsArrayList();

            if (recipentslist != null && recipentslist.size() > 0) {
                // Get TO & CC recipients list
                final ArrayList<MessageDetailsResponse.RecipientsObject> toRecipentslist = new ArrayList<MessageDetailsResponse.RecipientsObject>();
                final ArrayList<MessageDetailsResponse.RecipientsObject> ccRecipentslist = new ArrayList<MessageDetailsResponse.RecipientsObject>();

                for (int idx = 0; idx < recipentslist.size(); idx++) {
                    MessageDetailsResponse.RecipientsObject rowObj = recipentslist.get(idx);

                    if (rowObj != null && !TextUtils.isEmpty(rowObj.getRecipientTypeLU())) {
                        if (rowObj.getRecipientTypeLU().equalsIgnoreCase(AxSysConstants.TO_RECIPIENT_TYPE_LU)) {
                            toRecipentslist.add(rowObj);
                        } else if (rowObj.getRecipientTypeLU().equalsIgnoreCase(AxSysConstants.CC_RECIPIENT_TYPE_LU)) {
                            ccRecipentslist.add(rowObj);
                        }
                    } else {
                        toRecipentslist.add(rowObj);
                    }
                }

                // Display TO recipients list
                if (toRecipentslist != null && toRecipentslist.size() > 0) {
                    MessageDetailsResponse.RecipientsObject toRecipientObject = toRecipentslist.get(0);
                    if (toRecipientObject != null) {
                        toRecipientsTextView.setText(!TextUtils.isEmpty(toRecipientObject.getRecipientName()) ? toRecipientObject.getRecipientName() : "");
                    }

                    if (toRecipentslist.size() == 1) {
                        toMoreRecipientsTextView.setVisibility(View.GONE);
                    } else if (toRecipentslist.size() > 1) {
                        toMoreRecipientsTextView.setVisibility(View.VISIBLE);
                        toMoreRecipientsTextView.setText("& " + (toRecipentslist.size() - 1) + getString(R.string.text_more));

                        toMoreRecipientsTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                toMoreRecipientsTextView.setVisibility(View.GONE);

                                String recipientsStr = toRecipientsTextView.getText().toString().trim() + ", ";
                                for (int idx = 1; idx < toRecipentslist.size(); idx++) {
                                    if (!TextUtils.isEmpty(toRecipentslist.get(idx).getRecipientName())) {
                                        recipientsStr += toRecipentslist.get(idx).getRecipientName();

                                        if (idx != (toRecipentslist.size() - 1)) {
                                            recipientsStr += ", ";
                                        }
                                    }
                                }

                                toRecipientsTextView.setText(recipientsStr);
                            }
                        });
                    }
                }

                // Display CC recipients list
                if (ccRecipentslist != null && ccRecipentslist.size() > 0) {
                    MessageDetailsResponse.RecipientsObject ccRecipientObject = ccRecipentslist.get(0);
                    if (ccRecipientObject != null) {
                        ccTextView.setVisibility(View.VISIBLE);
                        ccRecipientsTextView.setText(!TextUtils.isEmpty(ccRecipientObject.getRecipientName()) ? ccRecipientObject.getRecipientName() : "");
                    }

                    if (ccRecipentslist.size() == 1) {
                        ccMoreRecipientsTextView.setVisibility(View.GONE);
                    } else if (ccRecipentslist.size() > 1) {
                        ccMoreRecipientsTextView.setVisibility(View.VISIBLE);
                        ccMoreRecipientsTextView.setText("& " + (ccRecipentslist.size() - 1) + getString(R.string.text_more));

                        ccMoreRecipientsTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ccMoreRecipientsTextView.setVisibility(View.GONE);

                                String recipientsStr = ccRecipientsTextView.getText().toString().trim() + ", ";
                                for (int idx = 1; idx < ccRecipentslist.size(); idx++) {
                                    if (!TextUtils.isEmpty(ccRecipentslist.get(idx).getRecipientName())) {
                                        recipientsStr += ccRecipentslist.get(idx).getRecipientName();

                                        if (idx != (ccRecipentslist.size() - 1)) {
                                            recipientsStr += ", ";
                                        }
                                    }
                                }

                                ccRecipientsTextView.setText(recipientsStr);
                            }
                        });
                    }
                }

            }

            if (msgDetailsDataObj.getAttachmentsArrayList() != null && msgDetailsDataObj.getAttachmentsArrayList().size() > 0) {
                attachImageView.setVisibility(View.VISIBLE);
                attachLayout.setVisibility(View.VISIBLE);
                if (attachmentsObjects != null && attachmentsObjects.size() > 0) {
                    MessageDetailsResponse.AttachmentsObject attach = attachmentsObjects.get(0);
                    attachmentsListAdapter = new AttachmentsListAdapter(attachmentsObjects);
                    attachmentsListView.setAdapter(attachmentsListAdapter);

                }
            } else {
                attachLayout.setVisibility(View.GONE);
                attachImageView.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(msgDetailsDataObj.getPriority_Lu())) {
                if (msgDetailsDataObj.getPriority_Lu().equalsIgnoreCase(AxSysConstants.HIGH_PRIORITY_ID)) {
                    priorityCheckBox.setChecked(true);
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_high);
                    mPriorityLU = AxSysConstants.HIGH_PRIORITY_ID;
                } else {
                    priorityCheckBox.setChecked(false);
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                    mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
                }
            } else {
                priorityCheckBox.setChecked(false);
                priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
            }

            subjectTextView.setText(!TextUtils.isEmpty(msgDetailsDataObj.getSubject()) ? msgDetailsDataObj.getSubject() : "");

            messageTextView.setText(!TextUtils.isEmpty(msgDetailsDataObj.getMessage()) ? msgDetailsDataObj.getMessage() : "");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_msg_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return super.onOptionsItemSelected(item);

            case R.id.action_mark_as_unread:
                break;

            default:
                break;
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
        final FragmentTransaction ft = activityFragmentManager.beginTransaction();
        final MessageComposeFragment messageComposeFragment = new MessageComposeFragment();
        final Bundle bundle = new Bundle();
        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);

        switch (view.getId()) {
            case R.id.replyImageButton:
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Reply");
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_ALL);
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE, AxSysConstants.COMPOSE_MSG_TYPE_REPLY);
                bundle.putSerializable(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS, msgDetailsResponse);
                messageComposeFragment.setArguments(bundle);
                ft.add(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                ft.commit();

                break;

            case R.id.replyAllImageButton:
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Reply All");
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_ALL);
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE, AxSysConstants.COMPOSE_MSG_TYPE_REPLY_ALL);
                bundle.putSerializable(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS, msgDetailsResponse);
                messageComposeFragment.setArguments(bundle);
                ft.add(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                ft.commit();

                break;

            case R.id.forwardImageButton:
                if (CommonUtils.hasInternet()) {
                    if (msgDetailsResponse.getDataObject().getAttachmentsArrayList().size() > 0 && msgDetailsResponse.getDataObject().getAttachmentsArrayList() != null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Do you want to attach the original attachments ?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Forward");
                                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_ALL);
                                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE, AxSysConstants.COMPOSE_MSG_TYPE_FORWARD);
                                bundle.putSerializable(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS, msgDetailsResponse);
                                messageComposeFragment.setArguments(bundle);
                                ft.add(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                                ft.commit();

                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Forward");
                                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_ALL);
                                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE, AxSysConstants.COMPOSE_MSG_TYPE_FORWARD);
                                bundle.putSerializable(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS, msgDetailsResponse);
                                messageComposeFragment.setArguments(bundle);
                                ft.add(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                                ft.commit();
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    } else {
                        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Forward");
                        bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_ALL);
                        bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TYPE, AxSysConstants.COMPOSE_MSG_TYPE_FORWARD);
                        bundle.putSerializable(AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS, msgDetailsResponse);
                        messageComposeFragment.setArguments(bundle);
                        ft.add(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                        ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                        ft.commit();
                    }
                } else {
                    showToast(R.string.error_network_required);
                }

                break;

            case R.id.deleteImageButton:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
                alertDialogBuilder.setTitle(getString(R.string.text_alert));
                alertDialogBuilder.setMessage(getString(R.string.confirmation_text_generic_delete));
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (CommonUtils.hasInternet()) {
                            displayProgressLoader(false);
                            MessageDeleteLoader messageDeleteLoader = new MessageDeleteLoader(mActivity, mMessageId, mActiveUser.getToken());
                            if (isAdded() && getView() != null) {
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_DELETE_LOADER), messageDeleteLoader);
                            }
                        } else {
                            showToast(R.string.error_no_network);
                        }
                    }
                });
                alertDialogBuilder.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                break;

            case R.id.priorityCheckBox:
                if (priorityCheckBox.isChecked()) {
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_high);
                    mPriorityLU = AxSysConstants.HIGH_PRIORITY_ID;
                } else {
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                    mPriorityLU = AxSysConstants.MEDIUM_PRIORITY_ID;
                }

                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);

                    String messageId = "";
                    if (msgDetailsResponse != null && msgDetailsResponse.getDataObject() != null) {
                        messageId = !TextUtils.isEmpty(msgDetailsResponse.getDataObject().getId()) ? msgDetailsResponse.getDataObject().getId() : "";
                    }

                    SavePriorityLoader savePriorityLoader = new SavePriorityLoader(mActivity, messageId, mPriorityLU, mActiveUser.getToken());
                    if (getView() != null && isAdded()) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MSG_SAVE_PRIORITY), savePriorityLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }

                break;
        }
    }

    private class AttachmentsListAdapter extends BaseAdapter {

        private ArrayList<MessageDetailsResponse.AttachmentsObject> attachmentsAL;

        public AttachmentsListAdapter(ArrayList<MessageDetailsResponse.AttachmentsObject> attachmentsAL) {
            this.attachmentsAL = attachmentsAL;
        }

        public void updateAttachmentsData(ArrayList<MessageDetailsResponse.AttachmentsObject> attachmentsAL) {
            this.attachmentsAL = attachmentsAL;
            notifyDataSetChanged();
        }


        @Override
        public int getCount() {
            return (attachmentsAL != null && attachmentsAL.size() > 0) ? attachmentsAL.size() : 0;
        }

        @Override
        public MessageDetailsResponse.AttachmentsObject getItem(int position) {
            return attachmentsAL.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final MessageDetailsResponse.AttachmentsObject rowObj = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.attachments_list_row, parent, false);
            }
            String ATTACHMENT_TYPE_MEDIA_IMAGE = "153";
            String ATTACHMENT_TYPE_MEDIA_AUDIO = "157";
            String ATTACHMENT_TYPE_MEDIA_VIDEO = "156";
            ImageView attachmentImageView = (ImageView) convertView.findViewById(R.id.attachmentImageView);
            TextView attachmentNameTextView = (TextView) convertView.findViewById(R.id.attachmentNameTextView);
            ImageView removeAttachmentImageView = (ImageView) convertView.findViewById(R.id.removeAttachmentImageView);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");

            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            removeAttachmentImageView.setVisibility(View.GONE);
            attachmentNameTextView.setTypeface(bold);
            if (rowObj.getAttachmentType_LU().equalsIgnoreCase(ATTACHMENT_TYPE_MEDIA_IMAGE)) {
                attachmentImageView.setImageResource(R.drawable.messages_gallery);
                attachmentNameTextView.setText("Image.png");
            } else if (rowObj.getAttachmentType_LU().equalsIgnoreCase(ATTACHMENT_TYPE_MEDIA_AUDIO)) {
                attachmentImageView.setImageResource(R.drawable.messages_audio);
                attachmentNameTextView.setText("Audio mp3");
            } else if (rowObj.getAttachmentType_LU().equalsIgnoreCase(ATTACHMENT_TYPE_MEDIA_VIDEO)) {
                attachmentImageView.setImageResource(R.drawable.messages_video);
                attachmentNameTextView.setText("Vedio mp4");
            } else {
                attachmentImageView.setImageResource(R.drawable.message_attachment);
                attachmentNameTextView.setText("Heath Summary");
            }
            //   attachmentNameTextView.setText(!TextUtils.isEmpty(rowObj.getAttachmentName()) ? rowObj.getAttachmentName() : "");


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO Navigate to summary panel details if attachment type is Summary Panel

                }
            });

            return convertView;
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
