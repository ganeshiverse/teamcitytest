package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 29/09/15.
 */
public class ValidateSiteResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private UsersDetailsObject userSiteDetailsObject;

    public UsersDetailsObject getUserSiteDetailsObject() {
        return userSiteDetailsObject;
    }

}
