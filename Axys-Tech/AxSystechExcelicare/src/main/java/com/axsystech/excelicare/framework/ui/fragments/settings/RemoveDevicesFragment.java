package com.axsystech.excelicare.framework.ui.fragments.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleDetailsEditResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.CircleDetailEditFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.loaders.CircleDetailsDbLoader;
import com.axsystech.excelicare.loaders.CircleDetailsLoader;
import com.axsystech.excelicare.loaders.CircleMemberDeleteLoader;
import com.axsystech.excelicare.loaders.CircleMemberDetailsDbLoader;
import com.axsystech.excelicare.loaders.CircleMembersDetailsLoader;
import com.axsystech.excelicare.loaders.CirclesListDBLoader;
import com.axsystech.excelicare.loaders.GetCircleMemberListFromDbLoader;
import com.axsystech.excelicare.loaders.GetUserRegisteredListFronDbLoader;
import com.axsystech.excelicare.loaders.ListMyCirclesLoader;
import com.axsystech.excelicare.loaders.RemoveCircleResponse;
import com.axsystech.excelicare.loaders.RemoveDeviceFromDbLoader;
import com.axsystech.excelicare.loaders.RemoveDeviceLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alanka on 9/23/2015.
 */
public class RemoveDevicesFragment extends BaseFragment {

    private MainContentActivity mActivity;
    private String TAG = RemoveDevicesFragment.class.getSimpleName();

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    private RemoveDevicesFragment mFragment;

    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.removeDeviceList)
    private ListView mRegisteredDeviceListView;

    private ArrayList<DeviceUsersDbModel> dbDeviceUsersDbModels;

    DeviceUsersDbModel rowObject;

    private static final String GET_REGISTERED_DEVICE_LIST_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.RemoveDevicesFragment.GET_MEMBER_LIST_DB_LOADER";
    private static final String REMOVE_USER_REGISTER_DEVICE_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.RemoveDevicesFragment.REMOVE_USER_REGISTER_DEVICE_LOADER";
    private static final String REMOVE_USER_REGISTER_DEVICE_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.RemoveDevicesFragment.REMOVE_USER_REGISTER_DEVICE_FROM_DB_LOADER";

    @Override
    protected void initActionBar() {
        // update actionbar title
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(),mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    private void readIntentArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_removedevice, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        getCircleMembersListFromDb();
    }

    private void getCircleMembersListFromDb() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        final GetUserRegisteredListFronDbLoader userRegisteredListFronDbLoader = new GetUserRegisteredListFronDbLoader(mActivity);
        if (isAdded() && getView() != null) {
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_REGISTERED_DEVICE_LIST_DB_LOADER), userRegisteredListFronDbLoader);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_REGISTERED_DEVICE_LIST_DB_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                dbDeviceUsersDbModels = (ArrayList<DeviceUsersDbModel>) result;

                if (dbDeviceUsersDbModels != null) {
                    // Display members in ListView
                    RegisteredDeviceListAdapter mCircleMembersAdapter = new RegisteredDeviceListAdapter(dbDeviceUsersDbModels);
                    mRegisteredDeviceListView.setAdapter(mCircleMembersAdapter);
                } else {
                    showToast(R.string.no_registered_device_found);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(REMOVE_USER_REGISTER_DEVICE_LOADER)) {
            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponse = (BaseResponse) result;
                if (baseResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            final RemoveDeviceFromDbLoader removeDeviceFromDbLoader = new RemoveDeviceFromDbLoader(mActivity, rowObject.getEcUserId());
                            if (isAdded() && getView() != null) {
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REMOVE_USER_REGISTER_DEVICE_FROM_DB_LOADER), removeDeviceFromDbLoader);
                            }
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(REMOVE_USER_REGISTER_DEVICE_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                ArrayList<DeviceUsersDbModel> deviceUsersDbModels = (ArrayList<DeviceUsersDbModel>) result;
                if (deviceUsersDbModels != null) {
                    final GetUserRegisteredListFronDbLoader userRegisteredListFronDbLoader = new GetUserRegisteredListFronDbLoader(mActivity);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_REGISTERED_DEVICE_LIST_DB_LOADER), userRegisteredListFronDbLoader);
                    }
                }
            }
        }
    }


    private class RegisteredDeviceListAdapter extends BaseAdapter {

        private ArrayList<DeviceUsersDbModel> deviceUsersDbModels;

        public RegisteredDeviceListAdapter(ArrayList<DeviceUsersDbModel> membersInSelectedCircle) {
            this.deviceUsersDbModels = membersInSelectedCircle;
        }

        @Override
        public int getCount() {
            return deviceUsersDbModels != null && deviceUsersDbModels.size() > 0 ? deviceUsersDbModels.size() : 0;
        }

        @Override
        public DeviceUsersDbModel getItem(int position) {
            return deviceUsersDbModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.userpreferences_group_row, viewGroup, false);
            }

            TextView deviceTextView = (TextView) convertView.findViewById(R.id.textViewGroup);

            deviceTextView.setText(rowObject.getDeviceId());

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    //Compare userid with userdeviceid if both are equal logout of the application

                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());
                    alert.setTitle(getString(R.string.text_alert));
                    alert.setMessage(getString(R.string.confirmation_to_unregister_device));
                    alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (CommonUtils.hasInternet()) {
                                RemoveDeviceLoader removeDeviceLoader = new RemoveDeviceLoader(mActivity, rowObject.getEcUserId(), rowObject.getUserDeviceID(), mActiveUser.getToken()
                                );
                                if (mFragment.isAdded() && mFragment.getView() != null) {
                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REMOVE_USER_REGISTER_DEVICE_LOADER), removeDeviceLoader);
                                }
                            } else {
                                showToast(R.string.error_no_network);
                            }
                        }
                    });
                    alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

                    alert.show();
                    return false;
                }
            });

            return convertView;
        }
    }
}

