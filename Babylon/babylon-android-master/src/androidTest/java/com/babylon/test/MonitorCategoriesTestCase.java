package com.babylon.test;

import android.test.AndroidTestCase;

import com.babylon.enums.MonitorCategory;

public class MonitorCategoriesTestCase extends AndroidTestCase {

    public void testIsMedicalKitExistForFitness() {
        assertFalse(MonitorCategory.FITNESS.isMedicalKitExist());
    }

    public void testIsMedicalKitExistForMind() {
        assertFalse(MonitorCategory.MIND.isMedicalKitExist());
    }

    public void testIsMedicalKitExistForVitamins() {
        assertTrue(MonitorCategory.VITAMINS.isMedicalKitExist());
    }

    public void testIsMedicalKitExistForBond() {
        assertTrue(MonitorCategory.BONE.isMedicalKitExist());
    }
}
