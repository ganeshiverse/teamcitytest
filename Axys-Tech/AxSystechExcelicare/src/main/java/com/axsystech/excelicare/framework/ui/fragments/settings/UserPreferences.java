package com.axsystech.excelicare.framework.ui.fragments.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.data.responses.SettingsChildItem;
import com.axsystech.excelicare.data.responses.SettingsGroupItem;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.CirclesListDBLoader;
import com.axsystech.excelicare.loaders.ListMyCirclesLoader;
import com.axsystech.excelicare.loaders.UserPreferencesLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by alanka on 9/23/2015.
 */
public class UserPreferences extends BaseFragment {
    private String TAG = UserPreferences.class.getSimpleName();

    private UserPreferences mFragment;
    private MainContentActivity mActivity;

    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.expandableListViewUserPreferences)
    private ExpandableListView mExpandableListViewUserPreferences;

    private static final String LIST_MY_USER_PREFERENCES = "com.axsystech.excelicare.framework.ui.fragments.circles.UserPreferences.LIST_MY_USER_PREFERENCES";

    @Override
    protected void initActionBar() {
        // update actionbar title
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity,mActionBarTitle));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_userpreferences, container, false);
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        preloadData();
    }


    public void preloadData() {
        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            UserPreferencesLoader userPreferencesLoader = new UserPreferencesLoader(mActivity, mActiveUser.getToken());
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MY_USER_PREFERENCES), userPreferencesLoader);
            }
        } else {
            showToast(R.string.error_network_required);
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
        if (id == getLoaderHelper().getLoaderId(LIST_MY_USER_PREFERENCES)) {

        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
        if (id == getLoaderHelper().getLoaderId(LIST_MY_USER_PREFERENCES)) {
            if (result != null && result instanceof String) {
                ArrayList<SettingsGroupItem> settinsAL = null;

                String userPreferences = result.toString();
                try {
                    JSONObject mainJson = new JSONObject(userPreferences);

                    JSONObject dataJson = null;
                    if (mainJson.has("data")) {
                        dataJson = mainJson.getJSONObject("data");
                    }

                    Iterator<String> iter = dataJson.keys();
                    if (iter != null) {
                        settinsAL = new ArrayList<SettingsGroupItem>();

                        while (iter.hasNext()) {
                            String groupKey = iter.next();

                            SettingsGroupItem groupItem = new SettingsGroupItem();
                            groupItem.setGroupName(groupKey);


                            ArrayList<SettingsChildItem> childsAL = null;
                            // parse childs in group
                            try {
                                Object childObj = dataJson.get(groupKey);
                                if (childObj != null && childObj instanceof JSONObject) {
                                    JSONObject childJsonObj = (JSONObject) childObj;

                                    Iterator<String> childsIter = childJsonObj.keys();

                                    if (childsIter != null) {
                                        childsAL = new ArrayList<SettingsChildItem>();

                                        while (childsIter.hasNext()) {
                                            String childKey = childsIter.next();
                                            SettingsChildItem childItem = new SettingsChildItem();


                                            childItem.setChildName(childKey);
                                            childItem.setChildValue(childJsonObj.get(childKey).toString());

                                            Trace.d("DEBUG", "ChildValue" + childItem.getChildValue());

                                            childsAL.add(childItem);
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            groupItem.setSettingsChildItem(childsAL);
                            settinsAL.add(groupItem);

                            UserPreferencesAdapter adapter = new UserPreferencesAdapter(settinsAL);
                            mExpandableListViewUserPreferences.setAdapter(adapter);
                            for (int i = 0; i < adapter.getGroupCount(); i++)
                                mExpandableListViewUserPreferences.expandGroup(i);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                showToast(R.string.no_user_preferences);
            }
        }
    }

    public class UserPreferencesAdapter extends BaseExpandableListAdapter {
        ArrayList<SettingsGroupItem> mSettingsAL;

        public UserPreferencesAdapter(ArrayList<SettingsGroupItem> settingsList) {
            this.mSettingsAL = settingsList;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }


        @Override
        public SettingsChildItem getChild(int groupPosition, int childPosition) {
            return mSettingsAL.get(groupPosition).getSettingsChildItem().get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            SettingsChildItem settingsChildItem = getChild(groupPosition, childPosition);
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.userpreferences_child_row, null);

                holder = new ViewHolder();
                holder.childName = (TextView) convertView.findViewById(R.id.grp_child);
                holder.childValue = (TextView) convertView.findViewById(R.id.grp_childValue);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.childName.setText(settingsChildItem.getChildName() + ":");
            holder.childValue.setText(settingsChildItem.getChildValue());
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mSettingsAL.get(groupPosition).getSettingsChildItem().size();
        }

        @Override
        public SettingsGroupItem getGroup(int groupPosition) {
            return mSettingsAL.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return mSettingsAL.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            SettingsGroupItem settingsGroupItem = (SettingsGroupItem) getGroup(groupPosition);
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.userpreferences_group_row, null);

                holder = new ViewHolder();
                holder.groupHeader = (TextView) convertView.findViewById(R.id.textViewGroup);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.groupHeader.setText(settingsGroupItem.getGroupName());

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }


        class ViewHolder {
            TextView groupHeader, childName, childValue;
        }

    }
}
