package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.client.store.model.Genre;

public class GenresTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri      CONTENT_URI            = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "genres");

    public static final Uri      CONTENT_URI_BY_GENREID = Uri.withAppendedPath(CONTENT_URI, "byid");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of genres.
     */
    public static final String   CONTENT_TYPE           = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                                + "/vnd.iversecomics.store.genres";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single genre.
     */
    public static final String   CONTENT_ITEM_TYPE      = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                                + "/vnd.iversecomics.store.genres";

    public static final String   DEFAULT_SORT_ORDER     = "name ASC";
    public static final String   TABLE                  = "genres";

    // server id for genre
    public static final String   GENREID                = "genreId";
    // human readable name
    public static final String   NAME                   = "name";
    // banner_filename id
    public static final String   BANNER_FILENAME        = "bannerFilename";

    public static final String[] FULL_PROJECTION        = new String[] {
            _ID, NAME, GENREID, BANNER_FILENAME
                                                        };

    public static ContentValues asValues(Genre genre) {
        ContentValues values = new ContentValues();
        if (genre.getDbId() >= 0) {
            values.put(_ID, genre.getDbId());
        }
        values.put(GENREID, genre.getGenreId());
        values.put(NAME, genre.getName());
        values.put(BANNER_FILENAME, genre.getBannerFilename());
        return values;
    }

    public static Genre fromCursor(Cursor c) {
        Genre genre = new Genre();

        genre.setDbId(getLong(c, _ID));
        genre.setGenreId(getString(c, GENREID));
        genre.setName(getString(c, NAME));
        genre.setBannerFilename(getString(c, BANNER_FILENAME));

        return genre;
    }
}
