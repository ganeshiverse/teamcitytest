package com.axsystech.excelicare.framework.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.CustomDialog;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity;
import com.axsystech.excelicare.framework.ui.activities.BaseActivity;
import com.axsystech.excelicare.framework.ui.activities.LoginActivity;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.network.exceptions.InternalServerException;
import com.axsystech.excelicare.network.exceptions.InvalidSiteUrlException;
import com.axsystech.excelicare.network.exceptions.TokenExpiredException;
import com.mig35.injectorlib.utils.inject.Injector;
import com.mig35.loaderlib.exceptions.NoNetworkException;
import com.mig35.loaderlib.ui.LoaderFragment;

/**
 * Date: 06.05.14
 * Time: 14:34
 *
 * @author SomeswarReddy
 */
public abstract class BaseFragment extends LoaderFragment {

    private CustomDialog mDialog;

    private Injector mInjector;

    protected abstract void initActionBar();

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        mInjector = Injector.init(this);

        super.onCreate(savedInstanceState);

        mInjector.applyOnFragmentCreate(this, savedInstanceState);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mInjector.applyOnFragmentViewCreated(this);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        mInjector.applyOnFragmentSaveInstanceState(this, outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        initActionBar();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mInjector.applyOnFragmentDestroyView(this);
    }

    protected MainContentActivity getMainContentActivity() {
        return (MainContentActivity) getActivity();
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected void showDialog(final DialogFragment dialogFragment) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialogFragment.setTargetFragment(this, 0);
        ft.add(dialogFragment, null);
        ft.commitAllowingStateLoss();
    }

    public void showToast(final int toastMessage) {
        getBaseActivity().showToast(toastMessage);
    }

    public void showToast(final String toastMessage) {
        getBaseActivity().showToast(toastMessage);
    }

    public void displayProgressLoader(boolean isCancelable) {
        if (mDialog == null) {
            mDialog = new CustomDialog(getActivity());
        }
        mDialog.setCancelable(isCancelable);

        if (!mDialog.isShowing()) {
            if (getView() != null && isAdded()) {
                mDialog.show();
            }
        }
    }

    public void hideProgressLoader() {
        if (mDialog == null) {
            return;
        }

        if (getView() != null && isAdded()) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);

        getLoaderHelper().destroyAsyncLoader(id);
        // general base activity exception handle
        if (exception instanceof NoNetworkException) {
            showToast(R.string.error_no_network);
        } else if (exception instanceof InternalServerException) {
            if (!TextUtils.isEmpty(exception.getMessage())) {
                showToast(exception.getMessage());
            }
        } else if (exception instanceof Exception) {
            showToast(exception.getMessage());
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);

        getLoaderHelper().destroyAsyncLoader(id);
    }
}
