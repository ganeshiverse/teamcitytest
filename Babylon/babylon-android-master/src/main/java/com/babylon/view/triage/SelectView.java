package com.babylon.view.triage;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.adapter.check.ChildSymptomAdapter;
import com.babylon.model.check.export.Symptom;
import com.babylon.model.check.ViewState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectView extends LinearLayout {
    private TextView title;
    private ListView symptomsList;
    private List<Symptom> symptoms;

    public SelectView(Context context, Symptom symptom, AdapterView.OnItemClickListener continueListener) {
        super(context);
        initView(context, symptom, continueListener);
    }

    public SelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void initView(Context context, Symptom symptom, AdapterView.OnItemClickListener continueListener) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_check_select_symptom, this);

        title = (TextView) findViewById(R.id.title);
        title.setText(symptom.getName());
        symptomsList = (ListView) findViewById(R.id.symptoms);

        if(symptom.isSelectBox()) {
            symptoms = Arrays.asList(symptom.getChildrenList());
        } else {
            if(symptom.getChildrenList().length == 0) {
                /*it is end question; Should make answer YES or NO*/
                symptoms = new ArrayList<Symptom>();
                Symptom yes = new Symptom();
                yes.setId(ViewState.YES.getValue());
                yes.setName(context.getString(ViewState.YES.getTitle()));

                Symptom no = new Symptom();
                no.setId(ViewState.NO.getValue());
                no.setName(context.getString(ViewState.NO.getTitle()));

                symptoms.add(yes);
                symptoms.add(no);
            }
        }

        ChildSymptomAdapter adapter = new ChildSymptomAdapter(context, symptoms);
        symptomsList.setAdapter(adapter);
        symptomsList.setOnItemClickListener(continueListener);
    }
}
