package com.axsystech.excelicare.framework.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.BaseActivity;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.media.FilterType;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.views.ExpandableHeightGridView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.Set;

/**
 * Created by someswarreddy on 20/07/15.
 */

// This will be used for PHOTOS, VIDEOS, AUDIOS
public class MediaListAdapter extends BaseAdapter {

    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;

    private FilterType mSelectedFilterType;
    private NavigableMap<Integer, NavigableMap<String, ArrayList<MediaItemDbModel>>> mediaDetailsTM;

    private ArrayList<MediaItemDbModel> allMediaItemsAL;

    private User mActiveUser;

    private boolean shouldDisplayLocalMedia = true;

    public MediaListAdapter(MainContentActivity context, FilterType selectedFilterType,
                            NavigableMap<Integer, NavigableMap<String, ArrayList<MediaItemDbModel>>> mediaDetailsTM,
                            User loginUser, boolean shouldDisplayLocalMedia) {
        mActivity = context;
        mLayoutInflater = LayoutInflater.from(mActivity);
        this.mActiveUser = loginUser;
        this.shouldDisplayLocalMedia = shouldDisplayLocalMedia;

        updateAdapterData(selectedFilterType, mediaDetailsTM);
    }

    private void updateAdapterData(FilterType selectedFilterType, NavigableMap<Integer, NavigableMap<String, ArrayList<MediaItemDbModel>>> mediaDetailsTM) {
        this.mSelectedFilterType = selectedFilterType;
        this.mediaDetailsTM = mediaDetailsTM;

        // if user opted for "ALL" filter, then collect all media items from PHOTOS, VIDEOS, AUDIOS
        if (mSelectedFilterType == FilterType.FILTER_ALL) {
            if (this.mediaDetailsTM != null && this.mediaDetailsTM.size() > 0) {
                allMediaItemsAL = new ArrayList<MediaItemDbModel>();

                for (int idx = 0; idx < this.mediaDetailsTM.size(); idx++) {
                    NavigableMap<String, ArrayList<MediaItemDbModel>> indexedMap = this.mediaDetailsTM.get(idx);

                    if (indexedMap != null && indexedMap.size() > 0) {
                        Collection<ArrayList<MediaItemDbModel>> valuesCollection = indexedMap.values();
                        if (valuesCollection != null && valuesCollection.size() > 0) {
                            Iterator<ArrayList<MediaItemDbModel>> itr = valuesCollection.iterator();

                            while (itr.hasNext()) {
                                allMediaItemsAL.addAll(itr.next());
                            }
                        }
                    }
                }
            }
        }
    }

    public void updateMediaData(FilterType selectedFilterType, NavigableMap<Integer, NavigableMap<String, ArrayList<MediaItemDbModel>>> mediaDetailsTM) {
        updateAdapterData(selectedFilterType, mediaDetailsTM);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mSelectedFilterType == FilterType.FILTER_ALL) {

            return (allMediaItemsAL != null && allMediaItemsAL.size() > 0) ? 1 : 0;

        } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS && this.mediaDetailsTM != null && this.mediaDetailsTM.size() > FilterType.FILTER_PHOTOS.getFilterId()) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> photosTM = mediaDetailsTM.get(FilterType.FILTER_PHOTOS.getFilterId());

            return (photosTM != null && photosTM.size() > 0) ? photosTM.size() : 0;

        } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS && this.mediaDetailsTM != null && this.mediaDetailsTM.size() > FilterType.FILTER_VIDEOS.getFilterId()) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> videosTM = mediaDetailsTM.get(FilterType.FILTER_VIDEOS.getFilterId());

            return (videosTM != null && videosTM.size() > 0) ? videosTM.size() : 0;

        } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS && this.mediaDetailsTM != null && this.mediaDetailsTM.size() > FilterType.FILTER_AUDIOS.getFilterId()) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> audiosTM = mediaDetailsTM.get(FilterType.FILTER_AUDIOS.getFilterId());

            return (audiosTM != null && audiosTM.size() > 0) ? audiosTM.size() : 0;
        }

        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mSelectedFilterType == FilterType.FILTER_ALL) {

            return allMediaItemsAL;

        } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS && this.mediaDetailsTM != null) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> photosTM = mediaDetailsTM.get(FilterType.FILTER_PHOTOS.getFilterId());
            Set<String> keySet = photosTM.keySet();
            ArrayList<String> keySetAL = new ArrayList<String>(keySet);

            return (keySetAL != null && keySetAL.size() > position) ? photosTM.get(keySetAL.get(position)) : null;

        } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS && this.mediaDetailsTM != null) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> videosTM = mediaDetailsTM.get(FilterType.FILTER_VIDEOS.getFilterId());
            Set<String> keySet = videosTM.keySet();
            ArrayList<String> keySetAL = new ArrayList<String>(keySet);

            return (keySetAL != null && keySetAL.size() > position) ? videosTM.get(keySetAL.get(position)) : null;

        } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS && this.mediaDetailsTM != null) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> audiosTM = mediaDetailsTM.get(FilterType.FILTER_AUDIOS.getFilterId());
            Set<String> keySet = audiosTM.keySet();
            ArrayList<String> keySetAL = new ArrayList<String>(keySet);

            return (keySetAL != null && keySetAL.size() > position) ? audiosTM.get(keySetAL.get(position)) : null;
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.media_group_item, parent, false);
        }

        LinearLayout spacelayout = (LinearLayout)convertView.findViewById(R.id.spacelayout);

        TextView dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
        TextView dateTextView1 = (TextView) convertView.findViewById(R.id.dateTextView1);
        //dateTextView1.setVisibility(View.VISIBLE);

        TextView countTextView = (TextView) convertView.findViewById(R.id.mediaCountTextView);
        ExpandableHeightGridView gridView = (ExpandableHeightGridView) convertView.findViewById(R.id.mediaGridView);

        if (mSelectedFilterType == FilterType.FILTER_ALL) {
            // Set defaults
            spacelayout.setVisibility(View.GONE);
            dateTextView.setVisibility(View.GONE);
            countTextView.setVisibility(View.GONE);

            // set actuals
            ArrayList<MediaItemDbModel> allMediaItems = (ArrayList<MediaItemDbModel>) getItem(position);

            MediaGridViewAdapter mMediaGridViewAdapter = new MediaGridViewAdapter(mActivity, mSelectedFilterType, allMediaItems, mActiveUser, shouldDisplayLocalMedia);
            gridView.setAdapter(mMediaGridViewAdapter);
            gridView.setExpanded(true);

        } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS && this.mediaDetailsTM.size() > FilterType.FILTER_PHOTOS.getFilterId()) {
            spacelayout.setVisibility(View.VISIBLE);
            dateTextView.setVisibility(View.VISIBLE);
            //dateTextView1.setVisibility(View.VISIBLE);
            countTextView.setVisibility(View.VISIBLE);

            ArrayList<MediaItemDbModel> mediaItems = null;

            if (getItem(position) != null && getItem(position) instanceof ArrayList) {
                mediaItems = (ArrayList<MediaItemDbModel>) getItem(position);

                if (mediaItems != null && mediaItems.size() > 0) {
                    dateTextView.setText(CommonUtils.getMediaGroupDateString(mediaItems.get(0).getCreatedDate()));
                    countTextView.setText(mediaItems.size() + "");

                    MediaGridViewAdapter mMediaGridViewAdapter = new MediaGridViewAdapter(mActivity, mSelectedFilterType, mediaItems, mActiveUser, shouldDisplayLocalMedia);
                    gridView.setAdapter(mMediaGridViewAdapter);
                    gridView.setExpanded(true);
                }
            }

        } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS && this.mediaDetailsTM.size() > FilterType.FILTER_VIDEOS.getFilterId()) {
            spacelayout.setVisibility(View.VISIBLE);
            dateTextView.setVisibility(View.VISIBLE);
            countTextView.setVisibility(View.VISIBLE);

            ArrayList<MediaItemDbModel> mediaItems = null;

            if (getItem(position) != null && getItem(position) instanceof ArrayList) {
                mediaItems = (ArrayList<MediaItemDbModel>) getItem(position);

                if (mediaItems != null && mediaItems.size() > 0) {
                    dateTextView.setText(CommonUtils.getMediaGroupDateString(mediaItems.get(0).getCreatedDate()));
                    countTextView.setText(mediaItems.size() + "");

                    MediaGridViewAdapter mMediaGridViewAdapter = new MediaGridViewAdapter(mActivity, mSelectedFilterType, mediaItems, mActiveUser, shouldDisplayLocalMedia);
                    gridView.setAdapter(mMediaGridViewAdapter);
                    gridView.setExpanded(true);
                }
            }
        } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS && this.mediaDetailsTM.size() > FilterType.FILTER_AUDIOS.getFilterId())
        {
            spacelayout.setVisibility(View.VISIBLE);
            dateTextView.setVisibility(View.VISIBLE);
            countTextView.setVisibility(View.VISIBLE);

            ArrayList<MediaItemDbModel> mediaItems = null;

            if (getItem(position) != null && getItem(position) instanceof ArrayList) {
                mediaItems = (ArrayList<MediaItemDbModel>) getItem(position);

                if (mediaItems != null && mediaItems.size() > 0) {
                    dateTextView.setVisibility(View.VISIBLE);
                    dateTextView.setText(CommonUtils.getMediaGroupDateString(mediaItems.get(0).getCreatedDate()));
                    countTextView.setText(mediaItems.size() + "");
                    MediaGridViewAdapter mMediaGridViewAdapter = new MediaGridViewAdapter(mActivity, mSelectedFilterType, mediaItems, mActiveUser, shouldDisplayLocalMedia);
                    gridView.setAdapter(mMediaGridViewAdapter);
                    gridView.setExpanded(true);
                }
            }
        }

        return convertView;
    }

}
