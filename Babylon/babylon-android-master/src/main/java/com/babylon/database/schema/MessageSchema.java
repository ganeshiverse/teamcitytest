package com.babylon.database.schema;

import android.net.Uri;
import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;
import com.babylon.model.Message;
import com.babylon.sync.SyncUtils;

public interface MessageSchema {

    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";

    int QUESTIONS_WITH_FILES = 101;

    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.MESSAGE
            .getName()).build();
    Uri CONTENT_URI_QUESTIONS_WITH_FILES_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity")
            .appendPath(Table.MESSAGE.getName() + QUESTIONS_WITH_FILES).build();
    Uri PENDING_QUESTIONS_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendEncodedPath("pending_question")
            .build();
    Uri CONTENT_URI_QUESTIONS_WITH_FILES = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.MESSAGE.getName() + QUESTIONS_WITH_FILES).build();

    public static final String SORT_TIME_ASC = Table.MESSAGE.getName() + "." + Columns.DATE + " ASC";
    public static final String SORT_TIME_DESC = Table.MESSAGE.getName() + "." + Columns.DATE + " DESC";

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        CONSULTANT_ID("consultantId", DBType.INT),
        DATE("date", DBType.INT),
        DATE_SENT("dateSent", DBType.INT),
        IS_PENDING_DOCTOR("isPendingDoctor", DBType.INT),
        PATIENT_ID("patientId", DBType.INT),
        STATE_ID("stateId", DBType.INT),
        TEXT("text", DBType.TEXT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return columnName;
        }
    }

    public interface Query {
        /* Cursor loader type */
        public static final int CURSOR_LOAD_MESSAGES = 5685;
        public static final String DATE_SORT = Columns.DATE.getName() + " DESC";
        public static final String SELECT_SENDED_MESSAGES_ONLY = Columns.SYNC_WITH_SERVER + " = "
                + String.valueOf(SyncUtils.SYNC_TYPE_ADDED);
        public static final String QUESTION_SELECTION = Table.MESSAGE.getName() + "." + MessageSchema.Columns.ID.getName() + "=?";
        public static final String MESSAGES_WITHOUT_COMMENT_OR_UNREAD_COMMENT =
                Table.MESSAGE.getName() + "."+ Columns.STATE_ID.getName() + " = " + Message.OPEN_STATE_ID + " OR "
                + Table.MESSAGE.getName() + "." + Columns.STATE_ID.getName() + " = " + Message.IN_PROGRESS_STATE_ID
                + " OR (" + Table.COMMENT.getName() + "." + CommentSchema.Columns.IS_READ.getName() + " = 'false' AND "
                + Table.COMMENT.getName() + "." + CommentSchema.Columns.DATE + " >= ?)";

        String[] PROJECTION = {
                Table.MESSAGE.getName() + "."
                        + Columns._ID.getName(),
                Table.MESSAGE.getName() + "." + Columns.ID.getName(),
                Columns.SYNC_WITH_SERVER.getName(),
                Columns.CONSULTANT_ID.getName(),
                Columns.DATE.getName(),
                Columns.IS_PENDING_DOCTOR.getName(),
                Columns.PATIENT_ID.getName(),
                Columns.STATE_ID.getName(),
                Columns.TEXT.getName(),
                Columns.DATE_SENT.getName()
        };

        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int CONSULTANT_ID = 3;
        int DATE = 4;
        int DATE_SENT = 5;
        int IS_PENDING_DOCTOR = 6;
        int PATIENT_ID = 7;
        int STATE_ID = 8;
        int TEXT = 9;
    }

    public interface QueryWithFiles {

        String[] PROJECTION = {
                Table.MESSAGE.getName() + "."
                        + Columns._ID.getName(),
                Table.MESSAGE.getName() + "." + Columns.ID.getName(),
                Table.MESSAGE.getName() + "." + Columns.SYNC_WITH_SERVER.getName(),
                Table.MESSAGE.getName() + "." + Columns.CONSULTANT_ID.getName(),
                Table.MESSAGE.getName() + "." + Columns.DATE.getName(),
                Table.MESSAGE.getName() + "." + Columns.IS_PENDING_DOCTOR.getName(),
                Table.MESSAGE.getName() + "." + Columns.PATIENT_ID.getName(),
                Table.MESSAGE.getName() + "." + Columns.STATE_ID.getName(),
                Table.MESSAGE.getName() + "." + Columns.TEXT.getName(),
                Table.MESSAGE.getName() + "." + Columns.DATE_SENT.getName(),
                Table.FILE_ENTRY.getName() + "." + FileEntrySchema.Columns.ID.getName() + " as file_id",
                Table.FILE_ENTRY.getName() + "." + FileEntrySchema.Columns.NAME.getName(),
                Table.FILE_ENTRY.getName() + "." + FileEntrySchema.Columns.MIMETYPE.getName(),
                Table.FILE_ENTRY.getName() + "." + FileEntrySchema.Columns.DATE.getName() + " as file_date",
                Table.COMMENT.getName() + "." + CommentSchema.Columns.TEXT.getName() + " as comment_text",
                Table.COMMENT.getName() + "." + CommentSchema.Columns.DATE.getName() + " as comment_date",
        };

        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int CONSULTANT_ID = 3;
        int DATE = 4;
        int IS_PENDING_DOCTOR = 5;
        int PATIENT_ID = 6;
        int STATE_ID = 7;
        int TEXT = 8;
        int DATE_SENT = 9;
        int FILE_ID = 10;
        int FILE_NAME = 11;
        int MIMETYPE = 12;
        int FILE_DATE = 13;
        int COMMENT_TEXT = 14;
        int COMMENT_DATE = 15;
    }
}
