package com.babylon.controllers;

public interface OnInvalidCache {
    public void onInvalidCache();
}