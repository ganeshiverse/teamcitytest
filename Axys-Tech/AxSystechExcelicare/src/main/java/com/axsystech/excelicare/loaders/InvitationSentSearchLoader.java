package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.InvitationReceivedDataResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/7/2015.
 */
public class InvitationSentSearchLoader extends DataAsyncTaskLibLoader<InvitationReceivedDataResponse> {

    private String token;
    private String criteria;

    public InvitationSentSearchLoader(Context context,String token, String criteria) {
        super(context);
        this.token = token;
        this.criteria = criteria;
    }

    @Override
    protected InvitationReceivedDataResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getInvitationSentSearchResponse(token, criteria);
    }
}