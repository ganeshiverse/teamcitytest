
package com.babylon.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.PasswordScreenUserSelectAdapter;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Urls;
import com.babylon.api.request.family.AccountAuthenticatePost;
import com.babylon.model.Account;
import com.babylon.model.Region;
import com.babylon.utils.*;
import com.babylon.view.TickButtonWithProgress;
import com.babylonpartners.babylon.HomePageBackboneActivity;
import com.google.gson.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PasswordConfirmActivity extends BaseActivity implements View.OnClickListener {




    private Region region;
    private Spinner regionSpinner;
    private TickButtonWithProgress continueButton;
    private EditText passwordEditText;
    private String mUri;
    private JsonObjectRequest mJsonReq;
    private JsonArrayRequest mJsonArrReq;
    private RequestQueue mQueue;
    private ImageButton mBackButton;
    private WebView mWebView;
    private IWebViewControl mWebViewControl;
    public static Activity mainActivityInstance;
    private List<Account> mListOfAccounts = new ArrayList<Account>();
    private LinearLayout mPasswordEnterLayout;
    private RecyclerView mRecyclerView;
    public static String mCurrentUserId;
    private Account mCurrentAccount;
    private TextView mPasswordResetText;
    public static final int SWITCH_ACCOUNT_RESULT_CODE = RESULT_FIRST_USER;
    public static final String ACCOUNT_EXTRA_KEY = "ACCOUNT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_clinical_confirm_password);
        setTitle(R.string.confirm_pswd_title);

        passwordEditText = (EditText) findViewById(R.id.passwordEdt);
        mPasswordEnterLayout = (LinearLayout) findViewById(R.id.clinical_confirm_password_enter_view);
        continueButton = (TickButtonWithProgress) findViewById(R.id.submitButton);
        mPasswordResetText = (TextView) findViewById(R.id.reset_password_text);
        continueButton.setOnClickListener(this);
        continueButton.setVisibility(View.GONE);
        //Get user accounts
        getUserAccounts();

        mWebViewControl = (IWebViewControl) mainActivityInstance;

        //Recycler view for family accounts
        mRecyclerView = (RecyclerView) findViewById(R.id.clinical_confirm_password_user_select_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        //Actionbar back button
        mBackButton = (ImageButton) findViewById(R.id.topbar_left_image_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PasswordConfirmActivity.super.onBackPressed();
            }
        });
    }

    @Override
    protected void onStop() {
        PasswordScreenUserSelectAdapter.mSelectedPosition = -1;
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitButton:
                continueButton.setProgressState(true);
                if (TextUtils.isEmpty(App.getInstance().getPatient().getFacebookUserId())) {
                    submit();
                }
                else{
                    if (App.getInstance().getPatient().getId().equals(mCurrentAccount.getId())) {
                        Gson gson = new Gson();
                        String accountJson = gson.toJson(mCurrentAccount, Account.class);
                        Intent homePageIntent = new Intent(PasswordConfirmActivity.this, HomePageBackboneActivity.class);
                        homePageIntent.putExtra(ACCOUNT_EXTRA_KEY, accountJson);
                        homePageIntent.putExtra("PAGE1", BackbonePageConstants.CLINICAL_RECORDS);

                        startActivity(homePageIntent);
                        finish();
                    } else {
                        new AccountAuthenticatePost().withEntry(mCurrentAccount).execute(PasswordConfirmActivity.this,
                                accountAuthenticatePostRequestListener);
                    }

                }
                break;
        }
    }


    private void getUserAccounts() {
        final ProgressDialog progressDialog = getLoadingDialog(this);
        progressDialog.show();

        mUri = Urls.getRuby(Urls.ACCOUNTS);

        mQueue = App.getInstance().getRequestQueue();

        mJsonArrReq = new SecureJsonArrayRequest(mUri, PasswordConfirmActivity.this, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //Receive Account object from the API
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                JsonParser parser = new JsonParser();

                JsonArray responseObject = parser.parse(response.toString()).getAsJsonArray();
                Calendar c = Calendar.getInstance();

                //Adding user account by default
                Account userAccount = new Account(App.getInstance().getPatient().getId(), App.getInstance().getPatient().getFirstName());
                mListOfAccounts.add(userAccount);

                for (JsonElement tempAccountObj : responseObject) {
                    Account accountFromAPI = gson.fromJson(tempAccountObj, Account.class);
                    float age = CommonHelper.getAge(c.getTime(), accountFromAPI.getDateOfBirthAsDate());
                    if (age <= 16) {
                        mListOfAccounts.add(accountFromAPI);
                    }
                }

                if (progressDialog.isShowing()) progressDialog.dismiss();
                if (mListOfAccounts.size() > 0) {
                    mPasswordEnterLayout.setVisibility(View.GONE);
                    mRecyclerView.setAdapter(new PasswordScreenUserSelectAdapter(PasswordConfirmActivity.this, mListOfAccounts));
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mPasswordResetText.setVisibility(View.GONE);
                    mPasswordEnterLayout.setVisibility(View.VISIBLE);
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog.isShowing()) progressDialog.dismiss();
                        error.printStackTrace();

                    }
                });
        mQueue.add(mJsonArrReq);
    }


    private void submit() {
        mUri = getUrl();

        mQueue = App.getInstance().getRequestQueue();

        mJsonReq = new SecureJsonObjectRequest(Request.Method.POST,
                mUri, PasswordConfirmActivity.this,
                getJSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                continueButton.setProgressState(false);
                PasswordConfirmActivity.this.finish();

                Gson gson = new Gson();
                String accountJson = gson.toJson(mCurrentAccount, Account.class);
                setProgressBarVisible(true);

                if (App.getInstance().getPatient().getId().equals(mCurrentAccount.getId())) {
                    Intent homePageIntent = new Intent(PasswordConfirmActivity.this, HomePageBackboneActivity.class);
                    homePageIntent.putExtra(ACCOUNT_EXTRA_KEY, accountJson);
                    homePageIntent.putExtra("PAGE1", BackbonePageConstants.CLINICAL_RECORDS);

                    startActivity(homePageIntent);
                } else {
                    new AccountAuthenticatePost().withEntry(mCurrentAccount).execute(PasswordConfirmActivity.this,
                            accountAuthenticatePostRequestListener);
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        passwordEditText.setError("Incorrect password");
                        continueButton.setProgressState(false);
                    }
                });
        mQueue.add(mJsonReq);
    }


    public void userSelected(Account pUserAccount) {


        mCurrentAccount = pUserAccount;
        if (TextUtils.isEmpty(App.getInstance().getPatient().getFacebookUserId())){
            mPasswordEnterLayout.setVisibility(View.VISIBLE);
            continueButton.setVisibility(View.VISIBLE);
        }
        else{
            submit();
            mPasswordEnterLayout.setVisibility(View.GONE);
            continueButton.setVisibility(View.VISIBLE);
        }

    }

    private final RequestListener<Account> accountAuthenticatePostRequestListener = new
            RequestListenerErrorProcessed<Account>() {
                @Override
                public void onRequestComplete(com.babylon.api.request.base.Response<Account> response, String errorMessage) {
                    setProgressBarVisible(false);
                    if (response.isSuccess()) {
                        Account account = response.getData();
                        mCurrentAccount =account;

                        Gson gson = new Gson();
                        String accountJson = gson.toJson(mCurrentAccount, Account.class);
                        Intent homePageIntent = new Intent(PasswordConfirmActivity.this,HomePageBackboneActivity.class);
                        homePageIntent.putExtra(ACCOUNT_EXTRA_KEY,accountJson);
                        homePageIntent.putExtra("PAGE1", BackbonePageConstants.CLINICAL_RECORDS);
                        homePageIntent.putExtra("OPEN","OPEN_CLINICAL");
                        startActivity(homePageIntent);
                        finish();
                    } else {
                        displayErrorMessage(response);
                    }
                }


            };



    private void displayErrorMessage(com.babylon.api.request.base.Response response) {
        final String userResponseError = response.getUserResponseError();
        if (!TextUtils.isEmpty(userResponseError)) {
            AlertDialogHelper.getInstance().showMessage(this, userResponseError);
        }
    }

    public void userUnSelected() {
        mPasswordEnterLayout.setVisibility(View.GONE);
        mPasswordResetText.setVisibility(View.VISIBLE);
        continueButton.setVisibility(View.GONE);
    }

    public JSONObject getJSONObject()
    {
        JSONObject apiJsonObject = new JSONObject();
        JSONObject passwordObj = new JSONObject();
        try {
            passwordObj.put("password", passwordEditText.getText().toString());
            apiJsonObject.put("patient", passwordObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return apiJsonObject;
    }

   /* @Override
    public void onBackPressed() {
        mWebViewControl.webViewGoBack();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                PasswordConfirmActivity.super.onBackPressed();
            }
        }, 500);
    }*/

    protected String getUrl() {


        String url= Urls.getRuby(Urls.PASSWORD_CONFIRM);
        String patientId =  App.getInstance().getPatient().getId();
        url =url.concat(patientId+"/verify");
        return url;
    }

    public interface IWebViewControl {
        void webViewGoBack();
    }

    public static ProgressDialog getLoadingDialog(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        return progressDialog;
    }
}

