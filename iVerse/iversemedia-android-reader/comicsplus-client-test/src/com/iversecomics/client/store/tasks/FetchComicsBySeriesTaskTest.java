package com.iversecomics.client.store.tasks;

import android.content.ContentResolver;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.ContentChangeTracker;

public class FetchComicsBySeriesTaskTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testFetchComicsBySeries() throws Exception {
        startStubActivity();
        freshDB(getActivity());

        ContentResolver resolver = getActivity().getContentResolver();
        ContentChangeTracker observer = new ContentChangeTracker(resolver);
        observer.registerSelf(ComicsTable.CONTENT_URI, this);

        String seriesId = "com.iversecomics.archie.comics.archie.series";
        ComicStore comicStore = getComicStore();
        FetchComicsBySeriesTask task = new FetchComicsBySeriesTask(comicStore, seriesId);
        runTaskWaitForCompletion(task);

        observer.assertGotContentChangeNotification();

        int actualCount = getRowCount(resolver, ComicsTable.CONTENT_URI);
        OurAsserts.assertGreaterThan("Comics table rows", 10, actualCount);
    }
}
