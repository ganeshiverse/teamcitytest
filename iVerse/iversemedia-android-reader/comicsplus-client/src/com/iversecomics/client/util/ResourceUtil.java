package com.iversecomics.client.util;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public final class ResourceUtil {
    public static void free(Bitmap b) {
        if (b == null) {
            return;
        }
        if (b.isRecycled()) {
            return;
        }
        b.recycle();
    }

    public static void free(Drawable d) {
        if (d == null) {
            return;
        }

        if (d instanceof BitmapDrawable) {
            Bitmap b = ((BitmapDrawable) d).getBitmap();
            free(b);
        }
    }

    private ResourceUtil() {
        /* prevent instantiation */
    }
}
