package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/1/2015.
 */
public class CancelBookingLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String appointmentId;
    private String ticketNumber;


    public CancelBookingLoader(Context context, String token, String appointmentId, String ticketNumber) {
        super(context);
        this.token = token;
        this.appointmentId = appointmentId;
        this.ticketNumber = ticketNumber;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().cancelBooking(token, appointmentId, ticketNumber);
    }
}
