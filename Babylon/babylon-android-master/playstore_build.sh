#!/bin/sh

prompt_environment() {
  echo ""
  printf "Building for production"
  environment="production"
}

prompt_platform() {
  echo ""
  printf "Building for android"
  platform="android"
}

prompt_mobile_branch() {
  echo ""
  printf "Using babylon-android master branch"
  mobile_branch="master"
}

prompt_web_branch() {
  echo ""
  printf "Using babylon master branch"
  web_branch="master"
}

symlink_android() {
  rm -R $android_dir/src/main/assets/dist
  ln -s $www_dir/dist $android_dir/src/main/assets

  rm -R $android_dir/src/production/assets/dist
  ln -s $www_dir/dist $android_dir/src/production/assets

  rm -R $android_dir/src/releaseCandidate/assets/dist
  ln -s $www_dir/dist $android_dir/src/releaseCandidate/assets
}

reset() {
  git fetch
  git add . -A
  git reset --hard origin/$1 > /dev/null 2> /dev/null
  if [[ $? == 0 ]]; then
    git status
  else
    echo "Using local branch (no origin branch found)"
  fi
}

reset_local() {
  git add . -A
  git reset --hard $1
}

build_android() {
  cd $android_dir
  if [[ $install_to_device != "y" ]]; then
    ./gradlew assembleProductionRelease
  else
    ./gradlew installProductionRelease
  fi
}

prompt_continue_anyway() {
  echo ""
  printf "Continue anyway? Changes will be lost. (y/N) "
  read continue
  if [[ $continue != "y" ]]; then
    exit 1
  fi
}

prompt_install_to_device() {
  echo ""
  printf "Install completed build to device? (y/n) "
  read install_device
}

uncommited_changes() {
  echo ""
  echo "😱  You appear to have uncommited changes in $(pwd):"
  echo ""
  git status
  prompt_continue_anyway
}

branches_diverged() {
  echo ""
  echo "😱  You appear to have a diverged branch $(pwd):"
  echo ""
  git status
  prompt_continue_anyway
}

detect_changes() {
  if [[ $(git status | grep "to be committed") != "" ]]; then
    uncommited_changes
  else
    if [[ $(git status | grep "for commit:") != "" ]]; then
      uncommited_changes
    else
      if [[ $(git status | grep "Untracked files:") != "" ]]; then
        uncommited_changes
      fi
    fi
  fi
}

detect_ahead() {
  if [[ $(git status | grep "can be fast-forwarded.") == "" ]]; then
    if [[ $(git status | grep "is ahead of 'origin") != "" ]]; then
      branches_diverged
    else
      if [[ $(git status | grep " have diverged") != "" ]]; then
        branches_diverged
      fi
    fi
  fi
}

attempt_rebase_with_master() {
  echo "Attempting rebase with origin/master..."
  git fetch
  git rebase origin/master > /dev/null 2> /dev/null
  if [[ $? == 0 ]]; then
    echo "✅   Rebased with origin/master"
  else
    echo "⚠️   Rebase failed. Probably a conflict."
    git rebase --abort
  fi
}

switch_branch() {
  git checkout $1 > /dev/null 2> /dev/null
  if [[ $? != 0 ]]; then
    git checkout -b $1 > /dev/null 2> /dev/null
  fi
  current_branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
  if [[ $current_branch != $1 ]]; then
    echo "Failed to switch branch to $1"
    exit 1
  else
    echo "Switched to branch '$1'"
  fi
}

if [[ $BABYLON_WWW == "" ]]; then
  echo "Environment variable required: BABYLON_WWW"
  exit 1
fi
if [[ $BABYLON_IOS == "" ]]; then
  echo "Environment variable required: BABYLON_IOS"
  exit 1
fi
if [[ $BABYLON_ANDROID == "" ]]; then
  echo "Environment variable required: BABYLON_ANDROID"
  exit 1
fi

www_dir=$BABYLON_WWW
android_dir=$BABYLON_ANDROID

prompt_environment
prompt_platform

prompt_mobile_branch
prompt_web_branch

platform_dir=$android_dir

cd $www_dir

detect_changes
reset_local
switch_branch $web_branch
detect_ahead
reset $web_branch
attempt_rebase_with_master

cd $platform_dir

detect_changes
reset_local
switch_branch $mobile_branch
detect_ahead
reset $mobile_branch
attempt_rebase_with_master

$www_dir/build.sh $environment
symlink_android
prompt_install_to_device
build_android
