package com.iversecomics.client.bitmap;

import java.io.IOException;
import java.net.URI;

import android.graphics.Bitmap;

public interface IBitmapLoader {
    public String[] getUriSchemes();

    public Bitmap loadBitmap(URI bitmapUri) throws IOException;
}
