package com.axsystech.excelicare.framework.ui.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.CityListResponse;
import com.axsystech.excelicare.data.responses.CountryListResponse;
import com.axsystech.excelicare.data.responses.LookupsListObject;
import com.axsystech.excelicare.data.responses.SignUpDataModel;
import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.data.responses.AddProxyResponse;
import com.axsystech.excelicare.data.responses.SignUpResponse;
import com.axsystech.excelicare.data.responses.SiteDetailsModel;
import com.axsystech.excelicare.data.responses.SiteListResponse;
import com.axsystech.excelicare.data.responses.StateListResponse;
import com.axsystech.excelicare.db.ConverterDbToResponseModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.dialogs.MessageDialogFragment;
import com.axsystech.excelicare.loaders.CityListLoader;
import com.axsystech.excelicare.loaders.GetLookUpDetailsLoader;
import com.axsystech.excelicare.loaders.GetSiteDetailsFromDBLoader;
import com.axsystech.excelicare.loaders.GetSiteListLoader;
import com.axsystech.excelicare.loaders.SignUpLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.axsystech.excelicare.util.views.FloatLabeledSpinner;
import com.axsystech.excelicare.util.views.FloatLabeledTextView;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by someswarreddy on 29/06/15.
 */
public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private static final String SECURITY_QUESTIONS_LOADER = "com.axsystech.excelicare.framework.ui.activities.SignUpActivity.SECURITY_QUESTIONS_LOADER";
    private static final String GENDER_TYPES_LOADER = "com.axsystech.excelicare.framework.ui.activities.SignUpActivity.GENDER_TYPES_LOADER";
    private static final String REGISTER_NEW_USER_LOADER = "com.axsystech.excelicare.framework.ui.activities.SignUpActivity.REGISTER_NEW_USER_LOADER";
//    private static final String GET_SITE_DETAILS_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.activities.GET_SITE_DETAILS_FROM_DB_LOADER";

    private String TAG = SignUpActivity.class.getSimpleName();
    private SignUpActivity mActivity;

    private static final int DOB_DATE_PICKER_ID = 91;
    private static final int PRIVACY_POLICY_REQUEST_CODE = 92;
    private static final int CITY_LIST_REQUEST_CODE = 93;
    private static final int STATE_LIST_REQUEST_CODE = 94;
    private static final int COUNTRY_LIST_REQUEST_CODE = 95;

    @InjectView(R.id.emailIdEditTextContainer)
    private FloatLabeledEditText emailIdEditTextContainer;

    @InjectView(R.id.firstNamEditTextContainer)
    private FloatLabeledEditText firstNamEditTextContainer;

    @InjectView(R.id.lastNamEditTextContainer)
    private FloatLabeledEditText lastNamEditTextContainer;

    @InjectView(R.id.genderSpinnerTopContainer)
    private LinearLayout genderSpinnerTopContainer;

    @InjectView(R.id.genderSpinnerContainer)
    private FloatLabeledSpinner genderSpinnerContainer;

    @InjectView(R.id.genderSpinner)
    private Spinner genderSpinner;

    @InjectView(R.id.dateOfBirthTextViewTopContainer)
    private LinearLayout dateOfBirthTextViewTopContainer;

    @InjectView(R.id.dateOfBirthTextViewContainer)
    private FloatLabeledTextView dateOfBirthTextViewContainer;

    @InjectView(R.id.mobileNumberEditTextContainer)
    private FloatLabeledEditText mobileNumberEditTextContainer;

//    @InjectView(R.id.siteIdSpinnerTopContainer)
//    private LinearLayout siteIdSpinnerTopContainer;

//    @InjectView(R.id.siteIdSpinnerContainer)
//    private FloatLabeledSpinner siteIdSpinnerContainer;

    @InjectView(R.id.cityTextViewTopContainer)
    private LinearLayout cityTextViewTopContainer;

    @InjectView(R.id.cityTextViewContainer)
    private FloatLabeledTextView cityTextViewContainer;

    @InjectView(R.id.stateTextViewTopContainer)
    private LinearLayout stateTextViewTopContainer;

    @InjectView(R.id.stateTextViewContainer)
    private FloatLabeledTextView stateTextViewContainer;

    @InjectView(R.id.countryTextViewTopContainer)
    private LinearLayout countryTextViewTopContainer;

    @InjectView(R.id.countryTextViewContainer)
    private FloatLabeledTextView countryTextViewContainer;

    @InjectView(R.id.identification1EditTextTopContainer)
    private LinearLayout identification1EditTextTopContainer;

    @InjectView(R.id.identification1EditTextContainer)
    private FloatLabeledEditText identification1EditTextContainer;

    @InjectView(R.id.identification1HelpImageView)
    private ImageView identification1HelpImageView;

    @InjectView(R.id.identification2EditTextTopContainer)
    private LinearLayout identification2EditTextTopContainer;

    @InjectView(R.id.identification2EditTextContainer)
    private FloatLabeledEditText identification2EditTextContainer;

    @InjectView(R.id.identification2HelpImageView)
    private ImageView identification2HelpImageView;

    @InjectView(R.id.address1EditTextContainer)
    private FloatLabeledEditText address1EditTextContainer;

    @InjectView(R.id.address2EditTextContainer)
    private FloatLabeledEditText address2EditTextContainer;

    @InjectView(R.id.address3EditTextContainer)
    private FloatLabeledEditText address3EditTextContainer;

    @InjectView(R.id.postCodeEditTextContainer)
    private FloatLabeledEditText postCodeEditTextContainer;

    @InjectView(R.id.securityQuestion1SpinnerTopContainer)
    private LinearLayout securityQuestion1SpinnerTopContainer;

    @InjectView(R.id.securityQuestion1SpinnerContainer)
    private FloatLabeledSpinner securityQuestion1SpinnerContainer;

    @InjectView(R.id.answer1EditTextContainer)
    private FloatLabeledEditText answer1EditTextContainer;

    @InjectView(R.id.securityQuestion2SpinnerTopContainer)
    private LinearLayout securityQuestion2SpinnerTopContainer;

    @InjectView(R.id.securityQuestion2SpinnerContainer)
    private FloatLabeledSpinner securityQuestion2SpinnerContainer;

    @InjectView(R.id.answer2EditTextContainer)
    private FloatLabeledEditText answer2EditTextContainer;

    @InjectView(R.id.clearBtn)
    private Button clearBtn;

    @InjectView(R.id.signUpBtn)
    private Button signUpBtn;

    @InjectView(R.id.privacyPolicyTextView)
    private TextView privacyPolicyTextView;

    @InjectSavedState
    private boolean mIsSignUpTypeFull = false;

    @InjectSavedState
    private String mInputEmailID = "";

    @InjectSavedState
    private SignUpDataModel mSignupDataModel;

    private DatePickerDialog dobDatePickerDialog;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldAddNewProxie;

    private String selectedSiteId = AxSysConstants.DEFAULT_SITE_ID;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, getString(R.string.signup_text)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mActivity = this;

        mSignupDataModel = new SignUpDataModel();

        readIntentData();
        renderViewsBasedOnSignUpType();
        addEventListeners();
        loadDefaultsToSignupDataModel();
    }

    private void readIntentData() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ADD_NEW_PROXIE)) {
                mShouldAddNewProxie = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ADD_NEW_PROXIE, false);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_SITE_ID)) {
                selectedSiteId = bundle.getString(AxSysConstants.EXTRA_SELECTED_SITE_ID);
            }
        }

        if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null) {
            mIsSignUpTypeFull = CommonUtils.isSignUpTypeFull(GlobalDataModel.getInstance().getSystemPreferencesAL());
        }

        mSignupDataModel.setSiteId(selectedSiteId);
    }

    private void renderViewsBasedOnSignUpType() {
        // As per new flow we should not allow user to edit the Email ID
        emailIdEditTextContainer.setClickable(false);
        emailIdEditTextContainer.getEditText().setEnabled(false);
        //  *******************************

        // set Site List data to spinner
        /*ArrayList<SiteDetailsModel> siteDetailsAL = GlobalDataModel.getInstance().getSiteList();
        if (siteDetailsAL != null && siteDetailsAL.size() > 0) {
            ArrayAdapter<SiteDetailsModel> siteListAdapter =
                    new ArrayAdapter<SiteDetailsModel>(mActivity, android.R.layout.simple_spinner_item, siteDetailsAL);
            siteListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            siteIdSpinnerContainer.getSpinner().setAdapter(siteListAdapter);
        } else {
            if (CommonUtils.hasInternet()) {
                // Loader to fetch Site List from Excelicare server
                final GetSiteListLoader siteListLoader = new GetSiteListLoader(this, "");
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_SITE_LIST_LOADER), siteListLoader);
            } else {
                // get site details from DB
                final GetSiteDetailsFromDBLoader getSiteDetailsFromDBLoader = new GetSiteDetailsFromDBLoader(this);
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_SITE_DETAILS_FROM_DB_LOADER), getSiteDetailsFromDBLoader);
            }
        }*/

        // set hint for both identification input fields
        if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null) {
            identification1EditTextContainer.getEditText().setHint(CommonUtils.getIdentification1HintValue(GlobalDataModel.getInstance().getSystemPreferencesAL()));
            identification2EditTextContainer.getEditText().setHint(CommonUtils.getIdentification2HintValue(GlobalDataModel.getInstance().getSystemPreferencesAL()));
        }

        // Check for Security Questions visibility flag in SystemPreferences Data
        if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                !CommonUtils.shouldDisplaySecurityQuestions(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            // Hide if not required and do not call 'GET_LOOKUP_DETAILS_LOADER' API to retrieve Security Questions from server
            securityQuestion1SpinnerTopContainer.setVisibility(View.GONE);
            securityQuestion1SpinnerContainer.setVisibility(View.GONE);
            answer1EditTextContainer.setVisibility(View.GONE);
            securityQuestion2SpinnerTopContainer.setVisibility(View.GONE);
            securityQuestion2SpinnerContainer.setVisibility(View.GONE);
            answer2EditTextContainer.setVisibility(View.GONE);
        } else {
            // If we display Security Question options, get Security Question from server by calling "GET_LOOKUP_DETAILS_LOADER" api
            displayProgressLoader(false);
            // TODO Need to remove hard coded SystemLookUp & UserLookUp values
            final GetLookUpDetailsLoader securityQuesLoader = new GetLookUpDetailsLoader(mActivity, "", AxSysConstants.LOOKUP_SEQURITY_QUESTIONS, AxSysConstants.LOOKUP_USERLOOKUPS);
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SECURITY_QUESTIONS_LOADER), securityQuesLoader);

            //*************************
            securityQuestion1SpinnerContainer.displayMandatoryMark(true);
            securityQuestion2SpinnerContainer.displayMandatoryMark(true);
            //*************************
            answer1EditTextContainer.displayMandatoryMark(true);
            answer1EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
            //*************************
            answer2EditTextContainer.displayMandatoryMark(true);
            answer2EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
            //*************************
        }

        // Get gender details from server
        final GetLookUpDetailsLoader genderTypesLoader = new GetLookUpDetailsLoader(mActivity, "", AxSysConstants.LOOKUP_GENDER_TYPES, AxSysConstants.LOOKUP_USERLOOKUPS);
        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GENDER_TYPES_LOADER), genderTypesLoader);


        // Common Fields - Add mandatory field marks
        emailIdEditTextContainer.displayMandatoryMark(true);
        emailIdEditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
        //*************************
        firstNamEditTextContainer.displayMandatoryMark(true);
        firstNamEditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
        //*************************
        lastNamEditTextContainer.displayMandatoryMark(true);
        lastNamEditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
        //*************************
        genderSpinnerContainer.displayMandatoryMark(true);
        //*************************
        dateOfBirthTextViewContainer.displayMandatoryMark(true);
//        dateOfBirthTextViewContainer.getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
        //*************************
        if (CommonUtils.shouldDisplayMobileNumber(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            mobileNumberEditTextContainer.setVisibility(View.VISIBLE);

            if (CommonUtils.isMobileNumberManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                mobileNumberEditTextContainer.displayMandatoryMark(true);
                mobileNumberEditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
            }
        } else {
            mobileNumberEditTextContainer.setVisibility(View.GONE);
        }
        //*************************
        //siteIdSpinnerContainer.displayMandatoryMark(true);
        //*************************

        if (!mIsSignUpTypeFull) {
            // If SignUp Type is QUICK
            identification1EditTextTopContainer.setVisibility(View.GONE);
            identification2EditTextTopContainer.setVisibility(View.GONE);
            address1EditTextContainer.setVisibility(View.GONE);
            address2EditTextContainer.setVisibility(View.GONE);
            address3EditTextContainer.setVisibility(View.GONE);
            cityTextViewTopContainer.setVisibility(View.GONE);
            stateTextViewTopContainer.setVisibility(View.GONE);
            countryTextViewTopContainer.setVisibility(View.GONE);
            postCodeEditTextContainer.setVisibility(View.GONE);
        } else {
            // If SignUp Type is FULL
            if (CommonUtils.shouldDisplayPrimaryIdentifier(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                identification1EditTextTopContainer.setVisibility(View.VISIBLE);

                if (CommonUtils.isPrimaryIdentifierManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                    identification1EditTextContainer.displayMandatoryMark(true);
                    identification1EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
                }
            } else {
                identification1EditTextTopContainer.setVisibility(View.GONE);
            }
            //*************************
            if (CommonUtils.shouldDisplaySecondaryIdentifier(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                identification2EditTextTopContainer.setVisibility(View.VISIBLE);

                if (CommonUtils.isSecondaryIdentifierManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                    identification2EditTextContainer.displayMandatoryMark(true);
                    identification2EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
                }
            } else {
                identification2EditTextTopContainer.setVisibility(View.GONE);
            }
            //*************************
            if (CommonUtils.shouldDisplayAddressFields(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                if (CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                    address1EditTextContainer.setVisibility(View.VISIBLE);
                    address2EditTextContainer.setVisibility(View.VISIBLE);
                    address3EditTextContainer.setVisibility(View.VISIBLE);
                    cityTextViewTopContainer.setVisibility(View.VISIBLE);
                    stateTextViewTopContainer.setVisibility(View.VISIBLE);
                    countryTextViewTopContainer.setVisibility(View.VISIBLE);
                    postCodeEditTextContainer.setVisibility(View.VISIBLE);

                    address1EditTextContainer.displayMandatoryMark(true);
                    address1EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);

                    address2EditTextContainer.displayMandatoryMark(true);
                    address2EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);

                    address3EditTextContainer.displayMandatoryMark(true);
                    address3EditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);

                    cityTextViewContainer.displayMandatoryMark(true);
//                    cityTextViewContainer.getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);

                    stateTextViewContainer.displayMandatoryMark(true);
//                    stateTextViewContainer.getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);

                    countryTextViewContainer.displayMandatoryMark(true);
//                    countryTextViewContainer.getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);

                    postCodeEditTextContainer.displayMandatoryMark(true);
                    postCodeEditTextContainer.getEditText().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star_small, 0);
                }
            } else {
                address1EditTextContainer.setVisibility(View.GONE);
                address2EditTextContainer.setVisibility(View.GONE);
                address3EditTextContainer.setVisibility(View.GONE);
                cityTextViewTopContainer.setVisibility(View.GONE);
                stateTextViewTopContainer.setVisibility(View.GONE);
                countryTextViewTopContainer.setVisibility(View.GONE);
                postCodeEditTextContainer.setVisibility(View.GONE);
            }

        }

    }

    private void addEventListeners() {
        emailIdEditTextContainer.setOnClickListener(mActivity);
        firstNamEditTextContainer.setOnClickListener(mActivity);
        lastNamEditTextContainer.setOnClickListener(mActivity);
        genderSpinnerTopContainer.setOnClickListener(mActivity);
        mobileNumberEditTextContainer.setOnClickListener(mActivity);
        identification1EditTextContainer.setOnClickListener(mActivity);
        identification1HelpImageView.setOnClickListener(mActivity);
        identification2EditTextContainer.setOnClickListener(mActivity);
        identification2HelpImageView.setOnClickListener(mActivity);
        address1EditTextContainer.setOnClickListener(mActivity);
        address2EditTextContainer.setOnClickListener(mActivity);
        address3EditTextContainer.setOnClickListener(mActivity);
        postCodeEditTextContainer.setOnClickListener(mActivity);
        securityQuestion1SpinnerTopContainer.setOnClickListener(mActivity);
        securityQuestion2SpinnerTopContainer.setOnClickListener(mActivity);
        answer1EditTextContainer.setOnClickListener(mActivity);
        answer2EditTextContainer.setOnClickListener(mActivity);

        // addListenerForSiteIdSpinner();

        dateOfBirthTextViewTopContainer.setOnClickListener(mActivity);
        cityTextViewTopContainer.setOnClickListener(mActivity);
        stateTextViewTopContainer.setOnClickListener(mActivity);
        countryTextViewTopContainer.setOnClickListener(mActivity);
        clearBtn.setOnClickListener(mActivity);
        signUpBtn.setOnClickListener(mActivity);
        privacyPolicyTextView.setOnClickListener(mActivity);
    }

    private void loadDefaultsToSignupDataModel() {
        emailIdEditTextContainer.getEditText().setText(mInputEmailID);

        /*if (siteIdSpinnerContainer.getSpinner().getAdapter() != null) {
            if (siteIdSpinnerContainer.getSpinner().getAdapter().getItem(siteIdSpinnerContainer.getSpinner().getSelectedItemPosition()) instanceof SiteDetailsModel) {
                mSignupDataModel.setSiteId(((SiteDetailsModel) siteIdSpinnerContainer.getSpinner().getAdapter().
                        getItem(siteIdSpinnerContainer.getSpinner().getSelectedItemPosition())).getSiteId());
            }
        }*/

        // Restrict user to edit email id when we are going to add new proxie
        if (mShouldAddNewProxie) {
            emailIdEditTextContainer.setClickable(false);
            emailIdEditTextContainer.getEditText().setEnabled(false);
            emailIdEditTextContainer.getEditText().setText((mActiveUser != null && mActiveUser.getEmail() != null) ? mActiveUser.getEmail() : "");
        }
    }

    /*private void addListenerForSiteIdSpinner() {
        if (siteIdSpinnerContainer.getSpinner().getAdapter() != null) {

            siteIdSpinnerContainer.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mSignupDataModel.setSiteId(((SiteDetailsModel) siteIdSpinnerContainer.getSpinner().getAdapter().
                            getItem(siteIdSpinnerContainer.getSpinner().getSelectedItemPosition())).getSiteId());

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            if (siteIdSpinnerContainer.getSpinner().getAdapter().getItem(siteIdSpinnerContainer.getSpinner().getSelectedItemPosition()) instanceof SiteDetailsModel) {
                mSignupDataModel.setSiteId(((SiteDetailsModel) siteIdSpinnerContainer.getSpinner().getAdapter().
                        getItem(siteIdSpinnerContainer.getSpinner().getSelectedItemPosition())).getSiteId());
            }

            // Hide site id selector if there was only one in the list
            if (siteIdSpinnerContainer.getSpinner().getAdapter().getCount() == 1) {
                siteIdSpinnerTopContainer.setVisibility(View.GONE);
            } else {
                siteIdSpinnerTopContainer.setVisibility(View.VISIBLE);
            }
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        /*if (id == getLoaderHelper().getLoaderId(GET_SITE_LIST_LOADER)) {
            if (result != null && result instanceof SiteListResponse) {
                ArrayList<SiteDetailsModel> siteDetailsAL = GlobalDataModel.getInstance().getSiteList();
                if (siteDetailsAL != null && siteDetailsAL.size() > 0) {
                    GlobalDataModel.getInstance().setSiteList(siteDetailsAL);

                    ArrayAdapter<SiteDetailsModel> siteListAdapter =
                            new ArrayAdapter<SiteDetailsModel>(mActivity, android.R.layout.simple_spinner_item, siteDetailsAL);
                    siteListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    siteIdSpinnerContainer.getSpinner().setAdapter(siteListAdapter);
                }

                addListenerForSiteIdSpinner();
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_SITE_DETAILS_FROM_DB_LOADER)) {
            if (result != null && result instanceof List) {
                ArrayList<SiteDetailsDBModel> siteDetailsDBModels = (ArrayList<SiteDetailsDBModel>) result;

                ArrayList<SiteDetailsModel> siteDetailsResModels = ConverterDbToResponseModel.getSiteDetailsResponseModel(siteDetailsDBModels);

                // set Site List data to spinner
                if (siteDetailsResModels != null && siteDetailsResModels.size() > 0) {
                    GlobalDataModel.getInstance().setSiteList(siteDetailsResModels);

                    ArrayAdapter<SiteDetailsModel> siteListAdapter =
                            new ArrayAdapter<SiteDetailsModel>(this, android.R.layout.simple_spinner_item, siteDetailsResModels);
                    siteListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    siteIdSpinnerContainer.getSpinner().setAdapter(siteListAdapter);
                }

                addListenerForSiteIdSpinner();
            }
        } else*/
        if (id == getLoaderHelper().getLoaderId(SECURITY_QUESTIONS_LOADER)) {

            if (result != null && result instanceof LookUpDetailsResponse) {
                LookUpDetailsResponse lookUpDetailsResponse = (LookUpDetailsResponse) result;

                ArrayList<LookupsListObject> lookupsListObjects = lookUpDetailsResponse.getSystemLookupsList();
                if (lookupsListObjects != null && lookupsListObjects.size() > 0) {
                    // Add empty Security Question object as first element to Spinner
                    LookupsListObject lookupsListObject = new LookupsListObject();
                    lookupsListObject.setId(-1); // for validating Security Questions user input, check whether selected item ID is != -1
                    lookupsListObject.setValue("");

                    lookupsListObjects.add(0, lookupsListObject);

                    ArrayAdapter<LookupsListObject> securityQuestionAdapter =
                            new ArrayAdapter<LookupsListObject>(mActivity, android.R.layout.simple_spinner_item, lookupsListObjects);
                    securityQuestionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    securityQuestion1SpinnerContainer.getSpinner().setAdapter(securityQuestionAdapter);
                    securityQuestion2SpinnerContainer.getSpinner().setAdapter(securityQuestionAdapter);

                    securityQuestion1SpinnerContainer.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mSignupDataModel.setSecurityQues1("" + ((LookupsListObject) securityQuestion1SpinnerContainer.getSpinner().getAdapter().
                                    getItem(securityQuestion1SpinnerContainer.getSpinner().getSelectedItemPosition())).getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    securityQuestion2SpinnerContainer.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mSignupDataModel.setSecurityQues2("" + ((LookupsListObject) securityQuestion2SpinnerContainer.getSpinner().getAdapter().
                                    getItem(securityQuestion2SpinnerContainer.getSpinner().getSelectedItemPosition())).getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    mSignupDataModel.setSecurityQues1("" + ((LookupsListObject) securityQuestion1SpinnerContainer.getSpinner().getAdapter().
                            getItem(securityQuestion1SpinnerContainer.getSpinner().getSelectedItemPosition())).getId());

                    mSignupDataModel.setSecurityQues2("" + ((LookupsListObject) securityQuestion2SpinnerContainer.getSpinner().getAdapter().
                            getItem(securityQuestion2SpinnerContainer.getSpinner().getSelectedItemPosition())).getId());
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GENDER_TYPES_LOADER)) {

            if (result != null && result instanceof LookUpDetailsResponse) {
                LookUpDetailsResponse lookUpDetailsResponse = (LookUpDetailsResponse) result;

                ArrayList<LookupsListObject> lookupsListObjects = lookUpDetailsResponse.getSystemLookupsList();
                if (lookupsListObjects != null && lookupsListObjects.size() > 0) {
                    // Add empty Gender object as first element to Spinner
                    LookupsListObject lookupsListObject = new LookupsListObject();
                    lookupsListObject.setId(-1); // for validating GENDER user input, check whether selected item ID is != -1
                    lookupsListObject.setValue("");

                    lookupsListObjects.add(0, lookupsListObject);

                    ArrayAdapter<LookupsListObject> genderTypesAdapter =
                            new ArrayAdapter<LookupsListObject>(mActivity, android.R.layout.simple_spinner_item, lookupsListObjects);
                    genderTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    genderSpinnerContainer.getSpinner().setAdapter(genderTypesAdapter);

                    genderSpinnerContainer.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mSignupDataModel.setGender(((LookupsListObject) genderSpinnerContainer.getSpinner().getAdapter().
                                    getItem(genderSpinnerContainer.getSpinner().getSelectedItemPosition())).getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                    mSignupDataModel.setGender(((LookupsListObject) genderSpinnerContainer.getSpinner().getAdapter().
                            getItem(genderSpinnerContainer.getSpinner().getSelectedItemPosition())).getId());
                }
            }

        } else if (id == getLoaderHelper().getLoaderId(REGISTER_NEW_USER_LOADER)) {

            if (result != null) {
                if (result instanceof SignUpResponse) {
                    // If user registering new user account
                    SignUpResponse signUpResponse = (SignUpResponse) result;

                    // Update received token value in GlobalObject
                    if (signUpResponse != null && signUpResponse.getSignUpData() != null &&
                            !TextUtils.isEmpty(signUpResponse.getSignUpData().getToken())) {
//                        GlobalDataModel.getInstance().setSiteId(signUpResponse.getSignUpData().getSiteID());
                    }

                    if (signUpResponse != null && signUpResponse.isSuccess()) {
                        // Display token sent message
                        showToast(R.string.msg_token_sent);

                        // Navigate to ChangePasswordActivity once SignUp is done
                        Intent changePwdIntent = new Intent(mActivity, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, selectedSiteId);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_change_password));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, signUpResponse.getSignUpData().getToken());
                        startActivity(changePwdIntent);
//                    finish();
                    }
                } else if (result instanceof AddProxyResponse) {
                    // if user adding new proxy (cared member)
                    AddProxyResponse proxySignUpResponse = (AddProxyResponse) result;

                    // Update received token value in GlobalObject
                    if (proxySignUpResponse != null && proxySignUpResponse.getSignUpData() != null /*&&
                            !TextUtils.isEmpty(proxySignUpResponse.getSignUpData().getToken())*/) {
//                        GlobalDataModel.getInstance().setToken(proxySignUpResponse.getSignUpData().getToken());
//                        GlobalDataModel.getInstance().setSiteId(proxySignUpResponse.getSignUpData().getSiteID());
                    }

                    // Display token sent message
                    showToast(getString(R.string.success_proxy_added));

                    // Send back intent and finish
                    Intent backIntent = new Intent();
                    setResult(RESULT_OK, backIntent);
                    finish();
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if (id == getLoaderHelper().getLoaderId(SECURITY_QUESTIONS_LOADER)) {
            // Hide Security question options if not able to get them from server
            securityQuestion1SpinnerTopContainer.setVisibility(View.GONE);
            securityQuestion1SpinnerContainer.setVisibility(View.GONE);
            answer1EditTextContainer.setVisibility(View.GONE);
            securityQuestion2SpinnerTopContainer.setVisibility(View.GONE);
            securityQuestion2SpinnerContainer.setVisibility(View.GONE);
            answer2EditTextContainer.setVisibility(View.GONE);
        } else if (id == getLoaderHelper().getLoaderId(GENDER_TYPES_LOADER)) {

        } else if (id == getLoaderHelper().getLoaderId(REGISTER_NEW_USER_LOADER)) {
            if (!TextUtils.isEmpty(exception.getMessage())) {
                showToast(exception.getMessage());
            }
        }
    }

    @Override
    public void onClick(View view) {
        CommonUtils.hideSoftKeyboard(mActivity);
        switch (view.getId()) {
            case R.id.dateOfBirthTextViewTopContainer:
                showDialog(DOB_DATE_PICKER_ID);
                break;
            case R.id.dateOfBirthTextViewContainer:
                showDialog(DOB_DATE_PICKER_ID);
                break;
            case R.id.cityTextViewTopContainer:
                // Navigate to city list screen
                Intent cityListIntent = new Intent(mActivity, CityListActivity.class);
                startActivityForResult(cityListIntent, CITY_LIST_REQUEST_CODE);
                break;
            case R.id.stateTextViewTopContainer:
                // Navigate to State list screen
                Intent stateListIntent = new Intent(mActivity, StateListActivity.class);
                startActivityForResult(stateListIntent, STATE_LIST_REQUEST_CODE);
                break;
            case R.id.countryTextViewTopContainer:
                // Navigate to Country list screen
                Intent countryListIntent = new Intent(mActivity, CountryListActivity.class);
                startActivityForResult(countryListIntent, COUNTRY_LIST_REQUEST_CODE);
                break;
            case R.id.clearBtn:
                clearUserInputs();
                break;
            case R.id.signUpBtn:
                if (validateUserInputs()) {
                    displayProgressLoader(false);
                    final SignUpLoader signUpLoader = new SignUpLoader(mActivity, mIsSignUpTypeFull, mSignupDataModel,
                            CommonUtils.getDeviceId(getApplicationContext()), "1", mShouldAddNewProxie,
                            (mActiveUser != null ? mActiveUser.getEcUserID() : 0));
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REGISTER_NEW_USER_LOADER), signUpLoader);
                }
                break;
            case R.id.privacyPolicyTextView:
                Intent privacyPolicyIntent = new Intent(mActivity, PrivacyPolicyActivity.class);
                privacyPolicyIntent.putExtra(AxSysConstants.EXTRA_JUST_DISPLAY_PRIVACY_POLICY, true);
                startActivityForResult(privacyPolicyIntent, PRIVACY_POLICY_REQUEST_CODE);
                break;
            case R.id.emailIdEditTextContainer:
            case R.id.firstNamEditTextContainer:
            case R.id.lastNamEditTextContainer:
            case R.id.mobileNumberEditTextContainer:
            case R.id.identification1EditTextContainer:
            case R.id.identification2EditTextContainer:
            case R.id.address1EditTextContainer:
            case R.id.address2EditTextContainer:
            case R.id.address3EditTextContainer:
            case R.id.postCodeEditTextContainer:
            case R.id.answer1EditTextContainer:
            case R.id.answer2EditTextContainer:
                if (view instanceof FloatLabeledEditText) {
                    ((FloatLabeledEditText) view).getEditText().requestFocus();
                    CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                }
                break;
            case R.id.genderSpinnerTopContainer:
                genderSpinner.performClick();
                break;
            case R.id.securityQuestion1SpinnerTopContainer:
                securityQuestion1SpinnerContainer.getSpinner().performClick();
                break;
            case R.id.securityQuestion2SpinnerTopContainer:
                securityQuestion2SpinnerContainer.getSpinner().performClick();
                break;
            case R.id.identification1HelpImageView:
                // display hint text in dialog
                MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(MessageDialogFragment.MESSAGE_KEY, (GlobalDataModel.getInstance().getSystemPreferencesAL() != null) ?
                        !TextUtils.isEmpty(CommonUtils.getIdentification1Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL())) ?
                                CommonUtils.getIdentification1Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL()) : "" : "");
                messageDialogFragment.setArguments(bundle);
                showDialog(messageDialogFragment);
                break;
            case R.id.identification2HelpImageView:
                // display hint text in dialog
                MessageDialogFragment messageDialogFragment2 = new MessageDialogFragment();
                final Bundle bundle2 = new Bundle();
                bundle2.putString(MessageDialogFragment.MESSAGE_KEY, (GlobalDataModel.getInstance().getSystemPreferencesAL() != null) ?
                        !TextUtils.isEmpty(CommonUtils.getIdentification2Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL())) ?
                                CommonUtils.getIdentification2Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL()) : "" : "");
                messageDialogFragment2.setArguments(bundle2);
                showDialog(messageDialogFragment2);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == CITY_LIST_REQUEST_CODE) {
                if (data != null) {
                    if (data.getExtras() != null && data.getExtras().containsKey(AxSysConstants.EXTRA_SELECTED_CITY)) {
                        CityListResponse.CityDetails selectedCity = (CityListResponse.CityDetails) data.getSerializableExtra(AxSysConstants.EXTRA_SELECTED_CITY);

                        cityTextViewContainer.getTextView().setText(selectedCity.getCity());
                        mSignupDataModel.setCity(selectedCity.getCity());
                    }
                }
            } else if (requestCode == STATE_LIST_REQUEST_CODE) {
                if (data.getExtras() != null && data.getExtras().containsKey(AxSysConstants.EXTRA_SELECTED_STATE)) {
                    StateListResponse.StateDetails selectedState = (StateListResponse.StateDetails) data.getSerializableExtra(AxSysConstants.EXTRA_SELECTED_STATE);

                    stateTextViewContainer.getTextView().setText(selectedState.getValue());
                    mSignupDataModel.setState(selectedState.getValue());
                }
            } else if (requestCode == COUNTRY_LIST_REQUEST_CODE) {
                if (data.getExtras() != null && data.getExtras().containsKey(AxSysConstants.EXTRA_SELECTED_COUNTRY)) {
                    CountryListResponse.CountryDetails selectedCountry = (CountryListResponse.CountryDetails) data.getSerializableExtra(AxSysConstants.EXTRA_SELECTED_COUNTRY);

                    countryTextViewContainer.getTextView().setText(selectedCountry.getValue());
                    mSignupDataModel.setCountry(selectedCountry.getValue());
                }
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        if (id == DOB_DATE_PICKER_ID) {
            dobDatePickerDialog = new DatePickerDialog(mActivity, myDateListener, cyear, cmonth, cday);
            dobDatePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTime().getTime());

            return dobDatePickerDialog;
        }
        return super.onCreateDialog(id);
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            dateOfBirthTextViewContainer.getTextView().setText(new StringBuilder().append((day + "").length() <= 1 ? ("0" + day) : day).
                    append("-").
                    append(((month + 1) + "").length() <= 1 ? "0" + (month + 1) : (month + 1)).
                    append("-").
                    append(year));
            mSignupDataModel.setDob(dateOfBirthTextViewContainer.getTextView().getText().toString().trim());
        }
    };

    private boolean validateUserInputs() {

        String firstName = null;
        String lastName = null;
        String emailId = null;
        String address1 = null;
        String address2 = null;
        String address3 = null;

        try {
            emailId = URLEncoder.encode(emailIdEditTextContainer.getEditText().getText().toString().trim(), "UTF-8");
            firstName = URLEncoder.encode(firstNamEditTextContainer.getEditText().getText().toString().trim(), "UTF-8");
            lastName = URLEncoder.encode(lastNamEditTextContainer.getEditText().getText().toString().trim(), "UTF-8");
            address1 = URLEncoder.encode(address1EditTextContainer.getEditText().getText().toString().trim(), "UTF-8");
            address2 = URLEncoder.encode(address2EditTextContainer.getEditText().getText().toString().trim(), "UTF-8");
            address3 = URLEncoder.encode(address3EditTextContainer.getEditText().getText().toString().trim(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(emailIdEditTextContainer.getEditText().getText().toString().trim()) ||
                !CommonUtils.isValidEmail(emailIdEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_valid_email);
            emailIdEditTextContainer.getEditText().requestFocus();
            return false;
        } else {
            mSignupDataModel.setLoginName(emailIdEditTextContainer.getEditText().getText().toString().trim());
        }

        if (TextUtils.isEmpty(firstName)) {
            showToast(R.string.input_first_name);
            firstNamEditTextContainer.getEditText().requestFocus();
            return false;
        } else {
            mSignupDataModel.setFirstName(firstName);
        }

        if (TextUtils.isEmpty(lastName)) {
            showToast(R.string.input_last_name);
            lastNamEditTextContainer.getEditText().requestFocus();
            return false;
        } else {
            mSignupDataModel.setLastName(lastName);
        }

        if (mSignupDataModel.getGender() == -1) {
            showToast(R.string.select_gender);
            return false;
        }

        if (TextUtils.isEmpty(dateOfBirthTextViewContainer.getTextView().getText().toString().trim())) {
            showToast(R.string.input_dob);
            dateOfBirthTextViewContainer.requestFocus();
            return false;
        } else {
            mSignupDataModel.setDob(dateOfBirthTextViewContainer.getTextView().getText().toString().trim());
        }

        // check if Mobile number field is displayed & is mandatory
        if (mobileNumberEditTextContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isMobileNumberManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(mobileNumberEditTextContainer.getEditText().getText().toString().trim())) {
                showToast(R.string.input_mobile_number);
                mobileNumberEditTextContainer.getEditText().requestFocus();
                return false;
            } else if (!TextUtils.isEmpty(mobileNumberEditTextContainer.getEditText().getText().toString().trim()) &&
                    mobileNumberEditTextContainer.getEditText().getText().toString().trim().length() < 12) {
                showToast(R.string.input_12_mobile_number);
                mobileNumberEditTextContainer.getEditText().requestFocus();
                return false;
            }
            if (!TextUtils.isEmpty(mobileNumberEditTextContainer.getEditText().getText().toString().trim()) &&
                    mobileNumberEditTextContainer.getEditText().getText().toString().trim().length() > 12) {
                showToast(R.string.input_valid_mobile_number);
                mobileNumberEditTextContainer.getEditText().requestFocus();
                return false;
            } else {
                mSignupDataModel.setMobileNo(mobileNumberEditTextContainer.getEditText().getText().toString().trim());
            }
        }

        if (identification1EditTextTopContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isPrimaryIdentifierManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(identification1EditTextContainer.getEditText().getText().toString().trim())) {
                showToast(getString(R.string.text_enter_input) + CommonUtils.getIdentification1HintValue(GlobalDataModel.getInstance().getSystemPreferencesAL()));
                identification1EditTextContainer.getEditText().requestFocus();
                return false;
            } else {
                mSignupDataModel.setAadharCardNumber(identification1EditTextContainer.getEditText().getText().toString().trim());
            }
        }

        if (identification1EditTextTopContainer.getVisibility() == View.VISIBLE) {
            if (!TextUtils.isEmpty(identification1EditTextContainer.getEditText().getText().toString().trim()) &&
                    GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                    !TextUtils.isEmpty(CommonUtils.getIdentification1Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL()))) {

                if (identification1EditTextContainer.getEditText().getText().toString().trim().matches(
                        CommonUtils.getIdentification1Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL()))) {
                    mSignupDataModel.setAadharCardNumber(identification1EditTextContainer.getEditText().getText().toString().trim());
                } else {
                    showToast(getString(R.string.text_enter_input) + CommonUtils.getIdentification1HintValue(GlobalDataModel.getInstance().getSystemPreferencesAL()));
                    identification1EditTextContainer.getEditText().requestFocus();
                    return false;
                }
            }
        }

        if (identification2EditTextTopContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isSecondaryIdentifierManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {

            if (TextUtils.isEmpty(identification2EditTextContainer.getEditText().getText().toString().trim())) {
                showToast(getString(R.string.text_enter_input) + CommonUtils.getIdentification2HintValue(GlobalDataModel.getInstance().getSystemPreferencesAL()));
                identification2EditTextContainer.getEditText().requestFocus();
                return false;
            } else {
                mSignupDataModel.setDrivingLicenseNumber(identification2EditTextContainer.getEditText().getText().toString().trim());
            }
        }

        if (identification2EditTextTopContainer.getVisibility() == View.VISIBLE) {
            if (!TextUtils.isEmpty(identification2EditTextContainer.getEditText().getText().toString().trim()) &&
                    GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                    !TextUtils.isEmpty(CommonUtils.getIdentification2Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL()))) {

                if (identification2EditTextContainer.getEditText().getText().toString().trim().matches(
                        CommonUtils.getIdentification2Pattern(GlobalDataModel.getInstance().getSystemPreferencesAL()))) {
                    mSignupDataModel.setDrivingLicenseNumber(identification2EditTextContainer.getEditText().getText().toString().trim());
                } else {
                    showToast(getString(R.string.text_enter_input) + CommonUtils.getIdentification2HintValue(GlobalDataModel.getInstance().getSystemPreferencesAL()));
                    identification2EditTextContainer.getEditText().requestFocus();
                    return false;
                }
            }
        }

        if (address1EditTextContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            try {
                if (TextUtils.isEmpty(URLEncoder.encode(address1, "UTF-8"))) {
                    showToast(R.string.input_address1_details);
                    address1EditTextContainer.getEditText().requestFocus();
                    return false;
                } else {
                    mSignupDataModel.setAddress1(address1);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        if (address2EditTextContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(address2)) {
                showToast(R.string.input_address2_details);
                address2EditTextContainer.getEditText().requestFocus();
                return false;
            } else {
                mSignupDataModel.setAddress2(address2);
            }
        }

        if (address3EditTextContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(address3)) {
                showToast(R.string.input_address3_details);
                address3EditTextContainer.getEditText().requestFocus();
                return false;
            } else {
                mSignupDataModel.setAddress3(address3);
            }
        }

        if (cityTextViewTopContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(cityTextViewContainer.getTextView().getText().toString().trim())) {
                showToast(R.string.input_city);
                cityTextViewContainer.requestFocus();
                return false;
            } else {
                mSignupDataModel.setCity(cityTextViewContainer.getTextView().getText().toString().trim());
            }
        }

        if (stateTextViewTopContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(stateTextViewContainer.getTextView().getText().toString().trim())) {
                showToast(R.string.input_state);
                stateTextViewContainer.requestFocus();
                return false;
            } else {
                mSignupDataModel.setState(stateTextViewContainer.getTextView().getText().toString().trim());
            }
        }

        if (countryTextViewTopContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(countryTextViewContainer.getTextView().getText().toString().trim())) {
                showToast(R.string.input_country);
                countryTextViewContainer.requestFocus();
                return false;
            } else {
                mSignupDataModel.setCountry(countryTextViewContainer.getTextView().getText().toString().trim());
            }
        }

        if (postCodeEditTextContainer.getVisibility() == View.VISIBLE &&
                CommonUtils.isAddressFieldsManadatory(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            if (TextUtils.isEmpty(postCodeEditTextContainer.getEditText().getText().toString().trim())) {
                showToast(R.string.input_postcode);
                postCodeEditTextContainer.getEditText().requestFocus();
                return false;
            } else {
                mSignupDataModel.setPostCode(postCodeEditTextContainer.getEditText().getText().toString().trim());
            }
        }

        if (securityQuestion1SpinnerTopContainer.getVisibility() == View.VISIBLE) {
            if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                    CommonUtils.shouldDisplaySecurityQuestions(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                // If security questions are mandatory to SignUp, check for whether user selected any question or not by validing ID with "-1"
                if (!TextUtils.isEmpty(mSignupDataModel.getSecurityQues1()) &&
                        mSignupDataModel.getSecurityQues1().trim().equalsIgnoreCase("-1")) {
                    showToast(R.string.select_security_question1);
                    return false;
                }
            }
        }

        if (answer1EditTextContainer.getVisibility() == View.VISIBLE) {
            try {
                if (TextUtils.isEmpty(URLEncoder.encode(answer1EditTextContainer.getEditText().getText().toString().trim(), "UTF-8"))) {
                    showToast(R.string.input_answer1);
                    answer1EditTextContainer.getEditText().requestFocus();
                    return false;
                } else {
                    mSignupDataModel.setAddress1(address1EditTextContainer.getEditText().getText().toString().trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (securityQuestion2SpinnerTopContainer.getVisibility() == View.VISIBLE) {
            if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                    CommonUtils.shouldDisplaySecurityQuestions(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
                // If security questions are mandatory to SignUp, check for whether user selected any question or not by validing ID with "-1"
                if (!TextUtils.isEmpty(mSignupDataModel.getSecurityQues2()) &&
                        mSignupDataModel.getSecurityQues2().trim().equalsIgnoreCase("-1")) {
                    showToast(R.string.select_security_question2);
                    return false;
                }
            }
        }

        if (securityQuestion1SpinnerTopContainer.getVisibility() == View.VISIBLE && securityQuestion2SpinnerTopContainer.getVisibility() == View.VISIBLE) {
            // check if user selected same security questions
            if (mSignupDataModel.getSecurityQues1() != null && mSignupDataModel.getSecurityQues2() != null) {
                if (mSignupDataModel.getSecurityQues1().trim().equalsIgnoreCase(mSignupDataModel.getSecurityQues2().trim())) {
                    showToast(R.string.input_different_security_questions);
                    return false;
                }
            }
        }

        if (answer2EditTextContainer.getVisibility() == View.VISIBLE) {
            try {
                if (TextUtils.isEmpty(URLEncoder.encode(answer2EditTextContainer.getEditText().getText().toString().trim(), "UTF-8"))) {
                    showToast(R.string.input_answer2);
                    answer2EditTextContainer.getEditText().requestFocus();
                    return false;
                } else {
                    mSignupDataModel.setAddress1(address1EditTextContainer.getEditText().getText().toString().trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void clearUserInputs() {
        if (TextUtils.isEmpty(mInputEmailID) && !mShouldAddNewProxie) {
            // which means user came here from the link provided in login activity, this case we need to clear the email as well
            emailIdEditTextContainer.getEditText().getText().clear();
        }
        firstNamEditTextContainer.getEditText().getText().clear();
        lastNamEditTextContainer.getEditText().getText().clear();
        if (genderSpinner.getAdapter() != null &&
                genderSpinner.getAdapter().getCount() > 0) {
            genderSpinner.setSelection(0);
        }
        dateOfBirthTextViewContainer.getTextView().setText("");
        mobileNumberEditTextContainer.getEditText().getText().clear();

        if (identification1EditTextContainer.getVisibility() == View.VISIBLE) {
            identification1EditTextContainer.getEditText().getText().clear();
        }
        if (identification2EditTextContainer.getVisibility() == View.VISIBLE) {
            identification2EditTextContainer.getEditText().getText().clear();
        }
        if (address1EditTextContainer.getVisibility() == View.VISIBLE) {
            address1EditTextContainer.getEditText().getText().clear();
        }
        if (address2EditTextContainer.getVisibility() == View.VISIBLE) {
            address2EditTextContainer.getEditText().getText().clear();
        }
        if (address3EditTextContainer.getVisibility() == View.VISIBLE) {
            address3EditTextContainer.getEditText().getText().clear();
        }
        if (postCodeEditTextContainer.getVisibility() == View.VISIBLE) {
            postCodeEditTextContainer.getEditText().getText().clear();
        }
        if (securityQuestion1SpinnerContainer.getVisibility() == View.VISIBLE) {
            if (securityQuestion1SpinnerContainer.getSpinner().getAdapter() != null &&
                    securityQuestion1SpinnerContainer.getSpinner().getAdapter().getCount() > 0) {
                securityQuestion1SpinnerContainer.getSpinner().setSelection(0);
            }
        }
        if (answer1EditTextContainer.getVisibility() == View.VISIBLE) {
            answer1EditTextContainer.getEditText().getText().clear();
        }
        if (securityQuestion2SpinnerContainer.getVisibility() == View.VISIBLE) {
            if (securityQuestion2SpinnerContainer.getSpinner().getAdapter() != null &&
                    securityQuestion2SpinnerContainer.getSpinner().getAdapter().getCount() > 0) {
                securityQuestion2SpinnerContainer.getSpinner().setSelection(0);
            }
        }
        if (answer2EditTextContainer.getVisibility() == View.VISIBLE) {
            answer2EditTextContainer.getEditText().getText().clear();
        }
    }

}