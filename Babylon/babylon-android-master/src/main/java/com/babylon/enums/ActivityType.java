package com.babylon.enums;

import com.babylon.App;
import com.babylon.R;
import com.babylon.utils.L;

public enum ActivityType {
    RUNNING(1), WALKING(2), CYCLING(3), SLEEP(4);

    private static final String TAG = ActivityType.class.getSimpleName();

    private int value;

    private ActivityType(int type) {
        value = type;
    }

    public int getValue() {
        return value;
    }

    public String getTitle() {
        String title = "?";

        switch (this) {
            case RUNNING:
                title = App.getInstance().getString(R.string.title_running);
                break;
            case WALKING:
                title = App.getInstance().getString(R.string.title_walking);
                break;
            case CYCLING:
                title = App.getInstance().getString(R.string.title_cycling);
                break;
            case SLEEP:
                title = App.getInstance().getString(R.string.title_sleep);
                break;
            default:
                L.d(TAG, "Unknown title");
        }

        return title;
    }
}
