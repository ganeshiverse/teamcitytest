package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.axsystech.excelicare.util.CryptoEngine;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by vvydesai on 9/11/2015.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CIRCLES_MEMBER_LIST)
public class CircleMemberListDbModel extends DBUtils implements Serializable {

    private static final long serialVersionUID = 2L;

    @DatabaseField(id = true, columnName = CIRCLE_USERID)
    private String userId;

    @DatabaseField(columnName = CIRCLE_MEMBER_FORENAME)
    private String foreName;

    @DatabaseField(columnName = CIRCLE_MEMBER_SURNAME)
    private String surName;

    @DatabaseField(columnName = CIRCLE_USER_MEMBER_CIRCLEID)
    private String userCircleId;

    @DatabaseField(columnName = CIRCLE_USER_MEMBER_MAIl)
    private String userMemberMail;

    @DatabaseField(columnName = CIRCLE_USER_MEMBER_ID)
    private String userMemberId;

    @DatabaseField(columnName = CIRCLE_USER_TYPE)
    private String userType;

    @DatabaseField(columnName = CIRCLE_MEMBER_SPECIALITY)
    private String userMemberSpeciality;

    @DatabaseField(columnName = CIRCLE_URL)
    private String userUrl;

    @DatabaseField(columnName = CIRCLE_MEMBER_ROLETYPE_SLU)
    private String memberRoleTypeSlu;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() throws Exception {
        return CryptoEngine.decrypt(userId);
    }

    public void setUserId(String userId) throws Exception {
        this.userId = CryptoEngine.encrypt(userId);
    }

    public String getForeName() throws Exception {
        return CryptoEngine.decrypt(foreName);
    }

    public void setForeName(String foreName) throws Exception {
        this.foreName = CryptoEngine.encrypt(foreName);
    }

    public String getSurName() throws Exception {
        return CryptoEngine.decrypt(surName);
    }

    public void setSurName(String surName) throws Exception {
        this.surName = CryptoEngine.encrypt(surName);
    }

    public String getUserCircleId() {
        return userCircleId;
    }

    public void setUserCircleId(String userCircleId)  {
        this.userCircleId = userCircleId;
    }

    public String getUserMemberMail() throws Exception {
        return CryptoEngine.decrypt(userMemberMail);
    }

    public void setUserMemberMail(String userMemberMail) throws Exception {
        this.userMemberMail = CryptoEngine.encrypt(userMemberMail);
    }

    public String getUserMemberId() throws Exception {
        return CryptoEngine.decrypt(userMemberId);
    }

    public void setUserMemberId(String userMemberId) throws Exception {
        this.userMemberId = CryptoEngine.encrypt(userMemberId);
    }

    public String getUserType() throws Exception {
        return CryptoEngine.decrypt(userType);
    }

    public void setUserType(String userType) throws Exception {
        this.userType = CryptoEngine.encrypt(userType);
    }

    public String getUserMemberSpeciality() throws Exception {
        return CryptoEngine.decrypt(userMemberSpeciality);
    }

    public void setUserMemberSpeciality(String userMemberSpeciality) throws Exception {
        this.userMemberSpeciality = CryptoEngine.encrypt(userMemberSpeciality);
    }

    public String getUserUrl() throws Exception {
        return CryptoEngine.decrypt(userUrl);
    }

    public void setUserUrl(String userUrl) throws Exception {
        this.userUrl = CryptoEngine.encrypt(userUrl);
    }

    public String getMemberRoleTypeSlu() throws Exception {
        return CryptoEngine.decrypt(memberRoleTypeSlu);
    }

    public void setMemberRoleTypeSlu(String memberRoleTypeSlu) throws Exception {
        this.memberRoleTypeSlu = CryptoEngine.encrypt(memberRoleTypeSlu);
    }
}
