package com.axsystech.excelicare.data.responses;

/**
 * Created by alanka on 9/28/2015.
 */
public class SettingsChildItem {

    private String ChildName;
    private String ChildValue;

    public String getChildName() {
        return ChildName;
    }

    public void setChildName(String childName) {
        ChildName = childName;
    }

    public String getChildValue() {
        return ChildValue;
    }

    public void setChildValue(String childValue) {
        ChildValue = childValue;
    }
}
