package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/15/2015.
 */
public class GetCircleDetailsDbLoader extends DataAsyncTaskLibLoader<List<CircleDetailsDbModel>> {

    public GetCircleDetailsDbLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<CircleDetailsDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<CircleDetailsDbModel, Long> circleDetailsDao = helper.getDao(CircleDetailsDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<CircleDetailsDbModel>>() {
                @Override
                public List<CircleDetailsDbModel> call() throws Exception {
                    return circleDetailsDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
