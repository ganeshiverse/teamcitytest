package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.PhysicalActivity;
import com.babylon.utils.DateUtils;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ActivityListGet
        extends AbsRequestGet<ResponseList<PhysicalActivity>> {

    private static final String DATE_FROM = "dateFrom";
    private static final String DATE_TO = "dateTo";
    private final static String DATE_NOW = "dateNow";

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.ACTIVITY_LIST);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param dateFrom date, timestamp mills
     * @param dateTo   date, timestamp mills
     */
    public static ActivityListGet newActivityListGet(long dateFrom, long dateTo) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (dateFrom > 0) {
            params.add(new BasicNameValuePair(DATE_FROM, String.valueOf(dateFrom / 1000)));
        }
        if (dateTo > 0) {
            params.add(new BasicNameValuePair(DATE_TO, String.valueOf(dateTo / 1000)));
        }

        long currentTimeInSeconds = DateUtils.getDateNowInSeconds();
        params.add(new BasicNameValuePair(DATE_NOW, String.valueOf(currentTimeInSeconds)));

        return (ActivityListGet) new ActivityListGet().withParams(params);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }
}
