package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.loaders.GetSiteListLoader;
import com.axsystech.excelicare.loaders.GetSystemPreferencesDBLoader;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 12.05.14
 * Time: 13:00
 *
 * @author SomeswarReddy
 */
public class SplashActivity extends BaseActivity implements ConfirmationDialogFragment.OnConfirmListener {

    private String TAG = SplashActivity.class.getSimpleName();
    private SplashActivity mActivity;

    private static final String DELAY_LOADER = "com.axsystech.excelicare.framework.ui.activities.SplashActivity.DELAY_LOADER";

    private static final int APP_QUIT_DIALOG_ID = 100;

    private static final int MILLISECOND_DELAY = 2000;

    @Override
    protected void initActionBar() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mActivity = this;

        final DataAsyncTaskLibLoader<Boolean> delayLoader = new DataAsyncTaskLibLoader<Boolean>(this, true) {
            @Override
            protected Boolean performLoad() throws Exception {
                SystemClock.sleep(MILLISECOND_DELAY);
                return true;
            }
        };
        // Empty loader for Splash Delay
        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DELAY_LOADER), delayLoader);

        /*if(CommonUtils.hasInternet()) {
            // Loader to fetch Site List from Excelicare server
            final GetSiteListLoader siteListLoader = new GetSiteListLoader(this, "");
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_SITE_LIST_LOADER), siteListLoader);
        }*/

        if(CommonUtils.hasInternet()) {
            // get SystemPreferences from cache and then navigate to LogIn Page
            final GetSystemPreferencesDBLoader systemPreferencesDBLoader = new GetSystemPreferencesDBLoader(mActivity);
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SYSTEM_PREFERENCES_LOADER), systemPreferencesDBLoader);
        }
    }

    private void navigateNext() {
        long countOfLoggedInUsers = getCountOfSavedUsersInDB();
        Trace.d(TAG, "countOfLoggedInUsers:" + countOfLoggedInUsers);

        /*
        If some users are already logged-in in this device get them from the DB.
        If count exists navigate to LoginActivity, where check for the internet status and validate credentials based on connectivity.
        If NONE of the user logged-in with this device as them to register newly.
         */
        if (countOfLoggedInUsers > 0) {
            // Navigate user to LOGIN screen
            Intent loginIntent = new Intent(this, LoginActivity.class);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            startActivity(loginIntent);
            finish();
        } else {
            // If NO user registered with the app using this device, check for internet connection to set up new account
            if (CommonUtils.hasInternet()) {
                Intent intent = new Intent(this, AppWelcomeActivity.class);
                intent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, "");
                startActivity(intent);
                finish();
            } else {
                // IF NO internet found, prompt user and QUIT application
                displayQuitApplicationDialog();
            }
        }
    }

    private long getCountOfSavedUsersInDB() {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(mActivity, DatabaseHelper.class);
            final Dao<User, Long> userDao = helper.getDao(User.class);
            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Long>() {
                @Override
                public Long call() throws Exception {
                    return userDao.countOf();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return 0;
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(DELAY_LOADER)) {
            // check for the navigation conditions and take further step
            navigateNext();

        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if(id == getLoaderHelper().getLoaderId(SYSTEM_PREFERENCES_LOADER)) {

        }
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == APP_QUIT_DIALOG_ID) {
            // QUIT APP
            CommonUtils.quitApplication(this);
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == APP_QUIT_DIALOG_ID) {
            // QUIT APP
            CommonUtils.quitApplication(this);
        }
    }

    private void displayQuitApplicationDialog() {
        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.error_network_required));
        bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, APP_QUIT_DIALOG_ID);
        bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, false);
        confirmationDialogFragment.setArguments(bundle);
        showDialog(confirmationDialogFragment);
    }
}
