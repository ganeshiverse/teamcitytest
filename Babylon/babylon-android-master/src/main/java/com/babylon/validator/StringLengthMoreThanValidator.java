package com.babylon.validator;

public class StringLengthMoreThanValidator extends BaseValidator {
    private int length;

    public StringLengthMoreThanValidator(BaseValidator nextValidator, String errorMessage, int length) {
        super(nextValidator, errorMessage);
        this.length = length;
    }

    @Override
    public String validate(String string) {
        return string.length() < length ? errorMessage : null;
    }
}
