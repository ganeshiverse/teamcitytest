package com.iversecomics.client.my.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.client.store.db.AbstractTable;

public class MyComicsTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(MyComicsDB.CONTENT_URI, "comics");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of my comics.
     */
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/vnd.iversecomics.mycomics";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single user.
     */
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/vnd.iversecomics.mycomics";

    public static final String DEFAULT_SORT_ORDER = "name ASC";
    public static final String TABLE = "mycomics";

    public static final String ASSET_FILENAME = "assetFilename";
    public static final String ASSET_FILESIZE = "assetFilesize";
    public static final String COMICID = "comicId";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String SYNOPSIS = "synopsis";
    public static final String SERIESID = "seriesId";
    public static final String PRICING_TIER = "pricingTier";
    public static final String TYPE = "type";
    public static final String PUBLISHERID = "publisherId";
    public static final String PUBLISHER_NAME = "publisherName";
    public static final String GENRES = "genres";
    public static final String RATING = "rating";
    public static final String URI_COVER = "cover";
    public static final String STATUS = "status";
    public static final String TIMESTAMP_PURCHASED = "timestampPurchased";
    public static final String TIMESTAMP_ASSET = "timestampAsset";
    public static final String TIMESTAMP_RELEASED = "timestampReleased";
    public static final String MY_RATING = "myRating";
    public static final String BOOKMARK = "bookMark";
    public static final String ON_DEMAND = "onDemand";

    public static final String[] FULL_PROJECTION = new String[]{_ID, COMICID, NAME, ASSET_FILENAME,
            ASSET_FILESIZE, URI_COVER, TIMESTAMP_ASSET, TIMESTAMP_PURCHASED, TIMESTAMP_RELEASED, DESCRIPTION,
            PRICING_TIER, PUBLISHERID, GENRES, RATING, SERIESID, STATUS, SYNOPSIS, TYPE, MY_RATING, BOOKMARK};

    public static ContentValues asValues(MyComic comic) {
        ContentValues values = new ContentValues();
        if (comic.getDbId() >= 0) {
            values.put(_ID, comic.getDbId());
        }

        values.put(COMICID, comic.getComicId());
        values.put(NAME, comic.getName());
        values.put(ASSET_FILENAME, comic.getAssetFilename());
        values.put(ASSET_FILESIZE, comic.getAssetFilesize());
        values.put(DESCRIPTION, comic.getDescription());
        values.put(SYNOPSIS, comic.getSynopsis());
        values.put(SERIESID, comic.getSeriesId());
        values.put(PRICING_TIER, comic.getPricingTier());
        values.put(TYPE, comic.getType());
        values.put(PUBLISHERID, comic.getPublisherId());
        values.put(PUBLISHER_NAME, comic.getPublisherName());
        setStringList(values, GENRES, comic.getGenres());
        values.put(RATING, comic.getRating());
        setUri(values, URI_COVER, comic.getUriCover());
        values.put(STATUS, comic.getStatus());
        values.put(TIMESTAMP_PURCHASED, comic.getTimestampPurchased());
        values.put(TIMESTAMP_ASSET, comic.getTimestampAsset());
        values.put(TIMESTAMP_RELEASED, comic.getTimestampReleased());
        values.put(MY_RATING, comic.getMyRating());
        values.put(BOOKMARK, comic.getBookMark());
        values.put(ON_DEMAND, comic.getOnDemand());

        return values;
    }

    public static MyComic fromCursor(Cursor c) {
        MyComic comic = new MyComic();

        comic.setDbId(getLong(c, _ID));
        comic.setComicId(getString(c, COMICID));
        comic.setName(getString(c, NAME));
        comic.setAssetFilename(getString(c, ASSET_FILENAME));
        comic.setAssetFilesize(getLong(c, ASSET_FILESIZE));
        comic.setDescription(getOptionalString(c, DESCRIPTION));
        comic.setSynopsis(getOptionalString(c, SYNOPSIS));
        comic.setSeriesId(getOptionalString(c, SERIESID));
        comic.setPricingTier(getOptionalInteger(c, PRICING_TIER, -1));
        comic.setType(getOptionalString(c, TYPE));
        comic.setPublisherId(getString(c, PUBLISHERID));
        comic.setPublisherName(getOptionalString(c, PUBLISHER_NAME));
        comic.setGenres(getStringList(c, GENRES));
        comic.setRating(getDouble(c, RATING));
        comic.setUriCover(getUri(c, URI_COVER));
        comic.setStatus(getOptionalInteger(c, STATUS, ComicMetadata.STATUS_BAD));
        comic.setTimestampPurchased(getOptionalUtcTimestamp(c, TIMESTAMP_PURCHASED));
        comic.setTimestampAsset(getOptionalUtcTimestamp(c, TIMESTAMP_ASSET));
        comic.setTimestampReleased(getOptionalUtcTimestamp(c, TIMESTAMP_RELEASED));
        comic.setMyRating(getDouble(c, MY_RATING));
        comic.setBookMark(getOptionalString(c, BOOKMARK));
        comic.setOnDemand(getBoolean(c, ON_DEMAND));
        return comic;
    }
}
