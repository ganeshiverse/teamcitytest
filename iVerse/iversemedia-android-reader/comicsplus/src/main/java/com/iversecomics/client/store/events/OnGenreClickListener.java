package com.iversecomics.client.store.events;

import android.support.v4.app.Fragment;

import com.iversecomics.client.store.model.Genre;

public interface OnGenreClickListener {
    void onGenreClick(Fragment fragment, Genre genre);
}
