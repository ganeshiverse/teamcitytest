package com.iversecomics.ui;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.iversecomics.app.ComicApp;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.refresh.TaskCallback;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.GroupsTable;
import com.iversecomics.client.store.db.PublishersTable;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.store.tasks.FetchPublishersTask;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.ui.main.event.ShowGroupEvent;
import com.iversecomics.ui.main.event.ShowPublisherEvent;
import com.iversecomics.ui.widget.NavigationBar;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Used to display publishers, groups, subgroups
 */
public class PublishersFragment extends Fragment implements TwoWayAdapterView.OnItemClickListener {
    public static final String ARG_PUBLISHER_GROUP_ID = "com.iverse.ARG_PUBLSIHER_GROUP_ID";
    public static final String ARG_PARENT_GROUP_ID = "com.iverse.ARG_PARENT_GROUP_ID";
    public static final String ARG_TITLE = "com.iverse.ARG_TITLE";
    DataModel dataModel;
    private TwoWayGridView mGrid;
    private View mSpinner;
    private PublishersAdapter adapter;
    private ComicApp app;

    public static PublishersFragment newInstance() {
        Bundle args = new Bundle();
        PublishersFragment frag = new PublishersFragment();
        frag.setArguments(args);
        return frag;
    }

    public static final PublishersFragment newInstanceForPublisher(Publisher publisher) {
        Bundle args = new Bundle();
        args.putString(ARG_PUBLISHER_GROUP_ID, publisher.getPublisherId());
        args.putString(ARG_TITLE, publisher.getName());
        PublishersFragment frag = new PublishersFragment();
        frag.setArguments(args);
        return frag;
    }

    public static final PublishersFragment newInstanceForGroup(Group group) {
        Bundle args = new Bundle();
        args.putString(ARG_PARENT_GROUP_ID, group.getGroupId());
        args.putString(ARG_TITLE, group.getName());
        PublishersFragment frag = new PublishersFragment();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ComicApp.getInstance();
        if (getArguments().containsKey(ARG_PARENT_GROUP_ID)) {
            dataModel = new SubGroupsDataModel(app, getArguments().getString(ARG_PARENT_GROUP_ID));
        } else if (getArguments().containsKey(ARG_PUBLISHER_GROUP_ID)) {
            dataModel = new PublisherGroupsDataModel(app, getArguments().getString(ARG_PUBLISHER_GROUP_ID));
        } else {
            dataModel = new PublishersDataModel(app);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.page_publishers, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUiElements(view);
        startUpdate();
    }

    private void initUiElements(View view) {
        mGrid = (TwoWayGridView) view.findViewById(R.id.list);
        mSpinner = view.findViewById(R.id.spinner);
        NavigationBar navbar = (NavigationBar) view.findViewById(R.id.navbar);
        navbar.setNavigationListener((NavigationBar.NavigationListener) getActivity());

//        setTitle(navbar);
        mGrid.setOnItemClickListener(this);
    }

//    @Deprecated //to be updated in a feature version
//    private void setTitle(NavigationBar navbar) {
//        if (getArguments().containsKey(ARG_TITLE)) {
//            navbar.setTitle(getArguments().getString(ARG_TITLE));
//        } else {
//            navbar.setTitle(R.string.publishers_header);
//        }
//    }


    private void startUpdate() {
        TaskCallback mRefreshCallback = new TaskCallback() {
            @Override
            public void onTaskCompleted(Throwable error) {
                refreshList();
            }
        };
        dataModel.refresh(mRefreshCallback);
    }


    private void crossfade() {
        mGrid.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
        mGrid.setVisibility(View.VISIBLE);

        Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        assert fadeOut != null;
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //Do nothing
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mSpinner.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //Do nothing
            }
        });
        mSpinner.startAnimation(fadeOut);
    }

    private void refreshList() {
        if (mGrid != null) {
            crossfade();
            ComicStore.DatabaseApi databaseApi = ComicApp.getInstance().getComicStore().getDatabaseApi();

            Cursor cursor = null;
            cursor = dataModel.createCursor();
            adapter = new PublishersAdapter(getActivity(), cursor);
            mGrid.setAdapter(adapter);
        }
    }

    @Override
    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
        Object tag = view.getTag();
        dataModel.tagSelected(tag);
    }

    private static abstract class DataModel {
        ComicStore.DatabaseApi databaseApi;
        ComicApp app;

        protected DataModel(ComicApp app) {
            this.app = app;
            databaseApi = app.getComicStore().getDatabaseApi();
        }

        public abstract void refresh(TaskCallback mRefreshCallback);

        public abstract Cursor createCursor();

        public abstract Object createTag(Cursor cursor);

        public abstract void tagSelected(Object object);

        public abstract String getImageUrl(Object tag);
    }

    private static class PublishersDataModel extends DataModel {

        protected PublishersDataModel(ComicApp app) {
            super(app);
        }

        @Override
        public void refresh(TaskCallback mRefreshCallback) {
            FetchPublishersTask task = new FetchPublishersTask(app.getComicStore(), app.getApplicationContext());
            app.getTaskPool().submitIfExpired(app.getFreshness(), task, mRefreshCallback);
        }

        @Override
        public Cursor createCursor() {
            return databaseApi.getCursorPublishersAll();
        }

        @Override
        public Object createTag(Cursor cursor) {
            return PublishersTable.fromCursor(cursor);
        }

        @Override
        public String getImageUrl(Object tag) {
            return ((Publisher) tag).getImageUrl();
        }

        @Override
        public void tagSelected(Object object) {
            OttoBusProvider.getInstance().post(new ShowPublisherEvent((Publisher) object));
        }
    }

    private static class PublisherGroupsDataModel extends DataModel {
        private String publisherId;

        protected PublisherGroupsDataModel(ComicApp app, String publisherId) {
            super(app);
            this.publisherId = publisherId;
        }

        @Override
        public void refresh(TaskCallback mRefreshCallback) {
            mRefreshCallback.onTaskCompleted(null);
        }

        @Override
        public Cursor createCursor() {
            return databaseApi.getCursorGroupsByPublisher(publisherId);
        }

        @Override
        public Object createTag(Cursor cursor) {
            return GroupsTable.fromCursor(cursor);
        }

        @Override
        public String getImageUrl(Object tag) {
            return ((Group) tag).getImageUrl();
        }

        @Override
        public void tagSelected(Object object) {
            OttoBusProvider.getInstance().post(new ShowGroupEvent((Group) object));
        }
    }

    private static class SubGroupsDataModel extends DataModel {
        private String parentGroupId;

        protected SubGroupsDataModel(ComicApp app, String parentGroupId) {
            super(app);
            this.parentGroupId = parentGroupId;
        }

        @Override
        public void refresh(TaskCallback mRefreshCallback) {
            mRefreshCallback.onTaskCompleted(null);
        }

        @Override
        public Cursor createCursor() {
            return databaseApi.getCursorGroupsByParentGroupID(parentGroupId);
        }

        @Override
        public Object createTag(Cursor cursor) {
            return GroupsTable.fromCursor(cursor);
        }

        @Override
        public String getImageUrl(Object tag) {
            return ((Group) tag).getImageUrl();
        }

        @Override
        public void tagSelected(Object object) {
            OttoBusProvider.getInstance().post(new ShowGroupEvent((Group) object));
        }
    }

    public class PublishersAdapter extends CursorAdapter {

        private final LayoutInflater mInflater;

        public PublishersAdapter(Context context, Cursor c) {
            super(context, c, true);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.page_publishers_item, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Object tag = dataModel.createTag(cursor);
            view.setTag(tag);
            ImageLoader.getInstance().displayImage(dataModel.getImageUrl(tag), (ImageView) view);
        }
    }
}
