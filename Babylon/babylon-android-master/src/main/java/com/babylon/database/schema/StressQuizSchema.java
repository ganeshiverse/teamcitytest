package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public interface StressQuizSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.QUESTION.getName() + ") ON CONFLICT IGNORE";

    Uri BASE_CONTENT_URI = Uri.parse("content://" + DatabaseHelper.CONTENT_AUTHORITY);
    Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.STRESS_QUIZ.toString()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        QUESTION("question", DBType.TEXT),
        CHECKED("isChecked", DBType.TEXT);

        private String columnName;
        private DBType type;

        private Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        @Override
        public String toString() {
            return columnName;
        }
    }

    public interface Query {
        public static final String[] PROJECTION = {
                Columns._ID.getName(),
                Columns.ID.getName(),
                Columns.QUESTION.getName(),
                Columns.CHECKED.getName()
        };

        int _ID = 0;
        int ID = 1;
        int QUESTION = 2;
        int CHECKED = 3;
    }
}
