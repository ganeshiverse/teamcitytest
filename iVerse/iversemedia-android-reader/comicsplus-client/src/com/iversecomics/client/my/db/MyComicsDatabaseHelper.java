package com.iversecomics.client.my.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class MyComicsDatabaseHelper extends SQLiteOpenHelper {
    private static final Logger           LOG       = LoggerFactory.getLogger(MyComicsDatabaseHelper.class);
    private static MyComicsDatabaseHelper singleton = null;

    public static synchronized MyComicsDatabaseHelper getInstance(Context context) {
        if (singleton == null) {
            singleton = new MyComicsDatabaseHelper(context);
        }
        return singleton;
    }

    public MyComicsDatabaseHelper(Context context) {
        super(context, MyComicsDB.DB_NAME, null, MyComicsDB.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql;

        // Comics Table
        sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(MyComicsTable.TABLE);
        sql.append(" ( ").append(MyComicsTable._ID).append(" INTEGER PRIMARY KEY");
        sql.append(" , ").append(MyComicsTable.ASSET_FILENAME).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.ASSET_FILESIZE).append(" LONG");
        sql.append(" , ").append(MyComicsTable.COMICID).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.NAME).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.STATUS).append(" INTEGER");
        sql.append(" , ").append(MyComicsTable.URI_COVER).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.TIMESTAMP_ASSET).append(" INTEGER");
        sql.append(" , ").append(MyComicsTable.TIMESTAMP_PURCHASED).append(" INTEGER");
        sql.append(" , ").append(MyComicsTable.TIMESTAMP_RELEASED).append(" INTEGER");
        sql.append(" , ").append(MyComicsTable.DESCRIPTION).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.SYNOPSIS).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.PUBLISHERID).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.PUBLISHER_NAME).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.GENRES).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.SERIESID).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.PRICING_TIER).append(" INTEGER");
        sql.append(" , ").append(MyComicsTable.RATING).append(" DOUBLE");
        sql.append(" , ").append(MyComicsTable.MY_RATING).append(" DOUBLE");
        sql.append(" , ").append(MyComicsTable.BOOKMARK).append(" DOUBLE");
        sql.append(" , ").append(MyComicsTable.TYPE).append(" TEXT");
        sql.append(" , ").append(MyComicsTable.ON_DEMAND).append(" INTEGER");
        sql.append(" , UNIQUE ( ").append(MyComicsTable.COMICID);
        sql.append(" ) ON CONFLICT REPLACE");
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX comicIndexFilename ON ").append(MyComicsTable.TABLE);
        sql.append(" ( ").append(MyComicsTable.ASSET_FILENAME);
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX comicIndexComicId ON ").append(MyComicsTable.TABLE);
        sql.append(" ( ").append(MyComicsTable.COMICID);
        sql.append(" );");
        db.execSQL(sql.toString());

        sql = new StringBuilder();
        sql.append("CREATE INDEX comicIndexName ON ").append(MyComicsTable.TABLE);
        sql.append(" ( ").append(MyComicsTable.NAME);
        sql.append(" );");
        db.execSQL(sql.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            LOG.warn("Upgrading from version %d to %d, which will destroy all old data.", oldVersion, newVersion);
            rebuild(db);
        }
    }

    public void rebuild(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + MyComicsTable.TABLE);
        db.execSQL("DROP INDEX IF EXISTS comicIndexFilename");
        db.execSQL("DROP INDEX IF EXISTS comicIndexComicId");
        db.execSQL("DROP INDEX IF EXISTS comicIndexName");

        onCreate(db);
    }
}