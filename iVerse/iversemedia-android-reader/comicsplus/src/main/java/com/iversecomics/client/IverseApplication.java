package com.iversecomics.client;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.util.Config;

import com.crashlytics.android.Crashlytics;
import com.iversecomics.archie.android.BuildConfig;
import com.iversecomics.bundle.Ownership;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.downloads.DownloadNotificationMonitor;
import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.downloads.internal.android.DownloadMonitorBuffer;
import com.iversecomics.client.my.ComicStorageReconcileTask;
import com.iversecomics.client.my.MyDownloadReceiver;
import com.iversecomics.client.my.SDStateException;
import com.iversecomics.client.net.HTTPClient;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.refresh.TaskPool;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;

//@ReportsCrashes(formKey = "dEpSZWFHcVAtMXFyNG4yS1Y1cU1sTXc6MQ")
public class IverseApplication extends Application {

    private static final Logger LOG = LoggerFactory
            .getLogger(IverseApplication.class);
    private static IverseApplication application;
    private ComicStore comicStore;
    private TaskPool taskPool;
    private HTTPClient httpClient;
    private Freshness freshness;
    private Ownership ownership;
    private MyDownloadReceiver downloadReceiver;
    private ShareManager shareManager;

    public static IverseApplication getApplication() {
        return application;
    }

    public void comicStorageReconcileTask() {
        try {
            ComicStorageReconcileTask task = new ComicStorageReconcileTask(
                    getApplicationContext(), ownership);
            task.setRefreshAll(true);
            task.assertSdCardPresent();
            getTaskPool().submit(task);
        } catch (SDStateException e) {
            LOG.error("Comic reconcile failed");
        }
    }

    public BitmapManager createBitmapManager() {
        return new BitmapManager(getApplicationContext(), httpClient);
    }

    public ComicStore getComicStore() {
        return comicStore;
    }

    public Freshness getFreshness() {
        return freshness;
    }

    public HTTPClient getHttpClient() {
        return httpClient;
    }

    public Ownership getOwnership() {
        return ownership;
    }

    public TaskPool getTaskPool() {
        return taskPool;
    }

    public boolean isDebugEnabled() {
        return ((getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (Config.LOGD) {
            LOG.debug("onConfigurationChanged(" + newConfig + ")");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        // ACRA.init(this);
        //Crashlytics crash reporting tool initialization
        if(!BuildConfig.DEBUG) {
            Crashlytics.start(this);
        }
        DownloaderManager downloader = DownloaderManager.getInstance();
        downloader.setMaxSimultaneous(1);
        downloader.setUserAgent(AndroidInfo.getUserAgent(this));
        downloader.init(this);
        // downloader.addDownloadMonitor(new DownloadMonitorBuffer(new
        // LogMonitor()));
        DownloadNotificationMonitor dlmon = new DownloadNotificationMonitor(
                this);
        DownloadMonitorBuffer dlmonbuf = new DownloadMonitorBuffer(dlmon);
        downloader.addDownloadMonitor(dlmonbuf);

        Context context = getBaseContext();

        taskPool = new TaskPool();
        httpClient = new HTTPClient(context);
        comicStore = new ComicStore(context, httpClient);
        freshness = new Freshness(context);
        ownership = new Ownership();

        updateOwnership();

        downloadReceiver = new MyDownloadReceiver();
//        final IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(DownloaderConstants.ACTION_DOWNLOAD_COMPLETE);
//        LocalBroadcastManager.getInstance(context).registerReceiver(
//                downloadReceiver, intentFilter);

        shareManager = new ShareManager();

        OttoBusProvider.getInstance().register(downloadReceiver);
    }

    @Override
    public void onTerminate() {
        LOG.debug("onTerminate()");
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(
//                downloadReceiver);
        OttoBusProvider.getInstance().unregister(downloadReceiver);
        super.onTerminate();
    }

    // public void setOwnership(Ownership ownership) {
    // this.ownership = ownership;
    // updateOwnership();
    // }

    private void updateOwnership() {
        // Load from preferences
        final SharedPreferences preferences = getSharedPreferences(Preferences.NAME, 0);
        final String email = preferences.getString(Preferences.KEY_OWNER_EMAIL, Preferences.DEFAULT_EMAIL);
        final String password = preferences.getString(Preferences.KEY_OWNER_PASSWORD, Preferences.DEFAULT_PASSWORD);
        ownership.setEmailAddress(email);
        ownership.setPassword(password);
        ownership.setUniqueId(AndroidInfo.getUniqueDeviceID(this));
        ownership.clearDirty();
    }

    public ShareManager getShareManager() {
        return shareManager;
    }

}
