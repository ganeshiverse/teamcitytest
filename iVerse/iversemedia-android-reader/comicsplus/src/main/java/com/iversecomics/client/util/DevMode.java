package com.iversecomics.client.util;

import android.os.Build;

import com.iversecomics.client.IDebuggable;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class DevMode {
    private static final Logger LOG = LoggerFactory.getLogger(DevMode.class);

    private static Object callMethod(Object rules, String methodName) throws SecurityException, NoSuchMethodException,
            IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Class<?> paramTypes[] = null;
        Method method = rules.getClass().getMethod(methodName, paramTypes);
        Object params[] = null;
        return method.invoke(rules, params);
    }

    private static void callSetPolicy(Class<?> strictModeClass, String methodName, Class<?> policyClass,
                                      Object builtRules) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException,
            SecurityException, NoSuchMethodException {
        Class<?> paramTypes[] = new Class<?>[]{policyClass};
        Method method = strictModeClass.getMethod(methodName, paramTypes);
        Object params[] = new Object[]{builtRules};
        method.invoke(strictModeClass, params);
    }

    public static void enableStrictThreadTracking(boolean penaltyDeath) {
        if (!isAvailable()) {
            LOG.warn("Not Available in this API Level (%d >= 9)==false: enableStrictThreadTracking()",
                    Build.VERSION.SDK_INT);
            return;
        }

        try {
            Class<?> strictModeClass = Class.forName("android.os.StrictMode");
            Class<?> policyClass = Class.forName(strictModeClass.getName() + "$ThreadPolicy");
            Class<?> rulesClass = Class.forName(policyClass.getName() + "$Builder");
            Object rules = rulesClass.newInstance();
            // callMethod(rules, "detectDiskReads");
            // callMethod(rules, "detectDiskWrites");
            // callMethod(rules, "detectNetwork");
            callMethod(rules, "detectAll");
            callMethod(rules, "penaltyLog");
            if (penaltyDeath) {
                callMethod(rules, "penaltyDeath");
            }
            Object builtRules = callMethod(rules, "build");
            callSetPolicy(strictModeClass, "setThreadPolicy", policyClass, builtRules);
            LOG.warn("Enabled STRICT Thread Tracking");
        } catch (Throwable t) {
            LOG.warn(t, "Unable to enable strict thread tracking: " + t.getMessage());
        }
    }

    public static void enableStrictVMTracking(boolean penaltyDeath) {
        if (!isAvailable()) {
            LOG.warn("Not Available in this API Level (%d >= 9)==false: enableStrictVMTracking()",
                    Build.VERSION.SDK_INT);
            return;
        }

        try {
            Class<?> strictModeClass = Class.forName("android.os.StrictMode");
            Class<?> vmClass = Class.forName(strictModeClass.getName() + "$VmPolicy");
            Class<?> rulesClass = Class.forName(vmClass.getName() + "$Builder");
            Object rules = rulesClass.newInstance();
            // callMethod(rules, "detectLeakedSqlLiteObjects");
            // callMethod(rules, "detectLeakedClosableObjects");
            // callMethod(rules, "detectActivityLeaks");
            callMethod(rules, "detectAll");
            callMethod(rules, "penaltyLog");
            if (penaltyDeath) {
                callMethod(rules, "penaltyDeath");
            }
            Object builtRules = callMethod(rules, "build");
            callSetPolicy(strictModeClass, "setVmPolicy", vmClass, builtRules);
            LOG.warn("Enabled STRICT VM Tracking");
        } catch (Throwable t) {
            LOG.warn(t, "Unable to enable strict VM tracking: " + t.getMessage());
        }
    }

    public static boolean isAvailable() {
        return (Build.VERSION.SDK_INT >= 9);
    }

    public static void setDebuggable(Object obj, boolean flag) {
        if (obj instanceof IDebuggable) {
            ((IDebuggable) obj).setDebug(flag);
        }
    }
}
