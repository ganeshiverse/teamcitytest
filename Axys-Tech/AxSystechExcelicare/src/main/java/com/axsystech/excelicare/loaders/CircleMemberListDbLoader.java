package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/11/2015.
 */
public class CircleMemberListDbLoader extends DataAsyncTaskLibLoader<ArrayList<CircleMemberListDbModel>> {

    private ArrayList<CircleMemberListDbModel> circleMemberListDbModels;
    private String TAG = CircleMemberListDbLoader.class.getSimpleName();

    public CircleMemberListDbLoader(final Context context, final ArrayList<CircleMemberListDbModel> mediaItems) {
        super(context, true);
        this.circleMemberListDbModels = mediaItems;
    }

    @Override
    protected ArrayList<CircleMemberListDbModel> performLoad() throws Exception {
        if (circleMemberListDbModels == null)
            throw new SQLException("Unable to cache circle member list in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (circleMemberListDbModels != null && circleMemberListDbModels.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<CircleMemberListDbModel>>() {
                    @Override
                    public ArrayList<CircleMemberListDbModel> call() throws Exception {
                        for (final CircleMemberListDbModel dbModel : circleMemberListDbModels) {
                            final Dao<CircleMemberListDbModel, Long> memberListDbModelLongDao = helper.getDao(CircleMemberListDbModel.class);

                            memberListDbModelLongDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved circles members in DB...");
                        }

                        return circleMemberListDbModels;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return circleMemberListDbModels;
    }

}