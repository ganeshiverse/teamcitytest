package com.babylon.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;

public class OkDialogFragment extends DialogFragment {

    public static String TAG = OkDialogFragment.class.getSimpleName();

    private
    @Nullable
    DialogInterface.OnDismissListener onDismissListener;

    public static OkDialogFragment getInstance(String message) {
        OkDialogFragment fragment = new OkDialogFragment();

        final Bundle b = new Bundle();
        b.putString(Keys.MESSAGE, message);

        fragment.setArguments(b);

        return fragment;
    }

    public static OkDialogFragment getInstance(int messageId) {
        final String message = App.getInstance().getString(messageId);
        return getInstance(message);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setView(getContentView())
                .setPositiveButton(android.R.string.ok, onCancelClickListener)
                .create();
    }

    protected View getContentView() {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ok, null, false);

        final String message = getArguments().getString(Keys.MESSAGE);
        final TextView messageTextView = (TextView) view.findViewById(R.id.text_view_message);
        messageTextView.setText(message);

        return view;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    protected final DialogInterface.OnClickListener onCancelClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dismiss();
        }
    };
}
