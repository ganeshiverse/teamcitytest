package com.babylon.fragment;


import com.babylon.utils.DateUtils;

import java.util.Calendar;
import java.util.Date;

public abstract class ChartFragment extends BaseMonitorMeParamsLoaderFragment {
    private static int ONE_MONTH_BACK = -1;

    public static long getMonthFrom(long startDate) {
        Calendar c = getCalendarWithBeginDate(startDate);

        c.add(Calendar.MONTH, ONE_MONTH_BACK);

        return c.getTime().getTime();
    }

    private static Calendar getCalendarWithBeginDate(long startDate) {
        Date beginOfTheDay = DateUtils.getStartDate(new Date(startDate));

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(beginOfTheDay.getTime());

        return c;
    }

}