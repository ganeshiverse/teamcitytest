package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.data.responses.PasswordRulesResponse;
import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.framework.ui.dialogs.MessageDialogFragment;
import com.axsystech.excelicare.loaders.CacheSystemPreferencesLoader;
import com.axsystech.excelicare.loaders.ChangePasswordVerifyOtpLoader;
import com.axsystech.excelicare.loaders.GetPasswordRulesLoader;
import com.axsystech.excelicare.loaders.ResendOTPLoader;
import com.axsystech.excelicare.loaders.SaveClientUrlAndSiteDetailsDBLoader;
import com.axsystech.excelicare.loaders.SaveUserDetailsAndClientUrlDBLoader;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AESHelper;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.PreferencesUtil;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.AESService;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * This activity will be used to accept password details during registration process as well as it would be
 * displayed to verify OTP (this scenario occurs when user switch the device and trying to access the account).
 * Created by someswar on 6/30/2015.
 */
public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private static final String GET_PASSWORD_RULES_LOADER = "com.axsystech.excelicare.framework.ui.activities.ChangePasswordActivity.GET_PASSWORD_RULES_LOADER";
    private static final String VERIFY_OTP_AND_REGISTER_LOADER = "com.axsystech.excelicare.framework.ui.activities.ChangePasswordActivity.VERIFY_OTP_AND_REGISTER_LOADER";
    private static final String GENERATE_OTP_LOADER = "com.axsystech.excelicare.framework.ui.activities.ChangePasswordActivity.GENERATE_OTP_LOADER";
    private static final String CACHE_USER_DETAILS_CLIENT_URL_LOADER = "com.axsystech.excelicare.framework.ui.activities.ChangePasswordActivity.CACHE_USER_DETAILS_CLIENT_URL_LOADER";
//    private static final String SAVE_CLIENT_URL_SITE_DETAILS_LOADER = "com.axsystech.excelicare.framework.ui.activities.ChangePasswordActivity.SAVE_CLIENT_URL_SITE_DETAILS_LOADER";

    private ChangePasswordActivity mActivity;

    private String TAG = ChangePasswordActivity.class.getSimpleName();

    //@InjectView(R.id.tokenKeyEditText)
    private EditText tokenKeyEditText;

    @InjectView(R.id.generateTokenLayout)
    private LinearLayout generateTokenLayout;


    private TextView generateTokenTextView;
    private TextView fieldamadetarytextview;
    private TextView lostTokenRequestNewTextView;
    //@InjectView(R.id.passwordStrengthTextView)
    //private TextView passwordStrengthTextView;

    //@InjectView(R.id.newPasswordEditText)
    private EditText newPasswordEditText;

    //@InjectView(R.id.confirmPasswordEditText)
    private EditText confirmPasswordEditText;

    @InjectView(R.id.passwordHintImageView)
    private ImageView passwordHintImageView;

    //@InjectView(R.id.submitBtn)
    private Button submitBtn;

    @InjectView(R.id.confirmpasswordcontainer)
    private FloatLabeledEditText confirmpasswordcontainer;

    @InjectView(R.id.newpasswordcontainer)
    private FloatLabeledEditText newpasswordcontainer;

    private String mInputEmailID = "";
    String encryptedPasswordData;
    byte[] keyStart = "AXSYS".getBytes();
    @InjectSavedState
    private String mActionBarTitle;
    private PasswordRulesResponse mPasswordRulesResponse;
    private String selectedSiteId = AxSysConstants.DEFAULT_SITE_ID;

    @Override
    protected void initActionBar() {

        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, mActionBarTitle));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.changepasswordactionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_change_password);
        mActivity = this;

        generateTokenTextView = (TextView) findViewById(R.id.generateTokenTextView);
        fieldamadetarytextview = (TextView) findViewById(R.id.fieldamadetarytextview);
        lostTokenRequestNewTextView = (TextView) findViewById(R.id.lostTokenRequestNewTextView);
        submitBtn = (Button) findViewById(R.id.submitBtn);
        tokenKeyEditText = (EditText) findViewById(R.id.tokenKeyEditText);
        newPasswordEditText = (EditText) findViewById(R.id.newPasswordEditText);
        confirmPasswordEditText = (EditText) findViewById(R.id.confirmPasswordEditText);


        addEventListeners();
        readIntentData();
        preloadData();
    }

    private void readIntentData() {
        // Default title
        mActionBarTitle = getString(R.string.btn_change_password);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }

            // TODO remove this before going to release in market
            if (bundle.containsKey(AxSysConstants.EXTRA_VERIFY_TOKEN)) {
                tokenKeyEditText.setText(bundle.getString(AxSysConstants.EXTRA_VERIFY_TOKEN));

            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_SITE_ID)) {
                selectedSiteId = bundle.getString(AxSysConstants.EXTRA_SELECTED_SITE_ID);
            }
        }
    }

    /**
     * This method will hide/show the required UI elements based on the title value we received upon navigation to this screen.
     * If title is 'ChangePassword' - it will display all the fields in XML file,
     * If title is 'Verify OTP' - it will hide password fields.
     */
    private void preloadData() {
        // If user came here to verify OTP, hide password & confirm password fields
        if (mActionBarTitle.equalsIgnoreCase(getString(R.string.btn_verify_otp))) {
            newpasswordcontainer.setVisibility(View.GONE);
            confirmpasswordcontainer.setVisibility(View.GONE);
            newPasswordEditText.setVisibility(View.GONE);
            //passwordStrengthTextView.setVisibility(View.GONE);
            confirmPasswordEditText.setVisibility(View.GONE);
            passwordHintImageView.setVisibility(View.GONE);
        } else {
            // GetPasswordRules from server
            if (CommonUtils.hasInternet()) {
                displayProgressLoader(false);
                final GetPasswordRulesLoader passwordRulesLoader = new GetPasswordRulesLoader(this);
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_PASSWORD_RULES_LOADER), passwordRulesLoader);
            } else {
                showToast(R.string.error_no_network);
            }
        }
    }

    private void addEventListeners() {

        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        generateTokenTextView.setTypeface(regular);
        fieldamadetarytextview.setTypeface(regular);
        lostTokenRequestNewTextView.setTypeface(regular);
        submitBtn.setTypeface(bold);
        tokenKeyEditText.setTypeface(regular);
        newPasswordEditText.setTypeface(regular);
        confirmPasswordEditText.setTypeface(regular);

        generateTokenTextView.setOnClickListener(this);
        generateTokenLayout.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        passwordHintImageView.setOnClickListener(this);
        confirmpasswordcontainer.setOnClickListener(this);
        newPasswordEditText.addTextChangedListener(this);
        newpasswordcontainer.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_changepassword, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;

        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(GET_PASSWORD_RULES_LOADER)) {

            if (result != null && result instanceof PasswordRulesResponse) {
                mPasswordRulesResponse = (PasswordRulesResponse) result;
            }

        } else if (id == getLoaderHelper().getLoaderId(VERIFY_OTP_AND_REGISTER_LOADER)) {

            if (result != null && result instanceof ChangePasswordResponse) {
                ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) result;
//                try {
////                    ServerMethods.getInstance().setGlobalClientUrl(changePasswordResponse.getClientURL());
//
//                    // Save authenticated user details, siteid, clienturl in Database
//                    SaveClientUrlAndSiteDetailsDBLoader clientUrlAndSiteDetailsDBLoader = new SaveClientUrlAndSiteDetailsDBLoader(mActivity, changePasswordResponse);
//                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_CLIENT_URL_SITE_DETAILS_LOADER), clientUrlAndSiteDetailsDBLoader);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }

                if (changePasswordResponse.getChangePasswordDataObject() != null &&
                        !TextUtils.isEmpty(changePasswordResponse.getChangePasswordDataObject().getEcUserID()) &&
                        !TextUtils.isEmpty(changePasswordResponse.getChangePasswordDataObject().getLoginName()) &&
                        !TextUtils.isEmpty(changePasswordResponse.getChangePasswordDataObject().getSiteID())) {

                    // Convert 'ChangePasswordDataObject' as 'UsersDetailsObject', so that we can use single DB loader to cache details
                    UsersDetailsObject usersDetailsObject = getRegisteredUserDetailsObj(changePasswordResponse.getChangePasswordDataObject());

                    // Create dummy array list to forward it to Db loader
                    if (usersDetailsObject != null) {
                        ArrayList<UsersDetailsObject> usersDetailsAL = new ArrayList<UsersDetailsObject>();
                        usersDetailsAL.add(usersDetailsObject);


                        SaveUserDetailsAndClientUrlDBLoader saveUserDetailsAndClientUrlDBLoader = new SaveUserDetailsAndClientUrlDBLoader(this,
                                changePasswordResponse.getChangePasswordDataObject().getLoginName().trim(), usersDetailsAL);
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER), saveUserDetailsAndClientUrlDBLoader);
                    }
                }

                // Display 'Registration success' message
                if (!TextUtils.isEmpty(changePasswordResponse.getMessage())) {
                    showToast(changePasswordResponse.getMessage());
                    // Navigate to LogInActivity
                    /*Intent loginIntent = new Intent(this, LoginActivity.class);
                    loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(loginIntent);
                    finish();*/
                } else {
                    showToast(R.string.msg_registration_success);
                    // Navigate to LogInActivity
                    /*Intent loginIntent = new Intent(this, LoginActivity.class);
                    loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(loginIntent);
                    finish();*/
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER)) {
            if (result != null && result instanceof Boolean) {
                Boolean statusFlag = (Boolean) result;

                // Navigate to LogInActivity
                Intent loginIntent = new Intent(this, LoginActivity.class);
                loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(loginIntent);
                finish();
            }
        } else if (id == getLoaderHelper().getLoaderId(GENERATE_OTP_LOADER)) {
            if (result != null && result instanceof BaseResponse) {
                BaseResponse response = (BaseResponse) result;
                if (!TextUtils.isEmpty(response.getMessage())) {
                    showToast(response.getMessage());
                }
            }
        } /*else if(id == getLoaderHelper().getLoaderId(SAVE_CLIENT_URL_SITE_DETAILS_LOADER)) {
            if(result != null && result instanceof ClientUrlDbModel) {
                ClientUrlDbModel clientUrlDbModel = (ClientUrlDbModel) result;

                // Navigate to LogInActivity
                Intent loginIntent = new Intent(this, LoginActivity.class);
                loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(loginIntent);
                finish();
            }
        }*/
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        /*if(id == getLoaderHelper().getLoaderId(SAVE_CLIENT_URL_SITE_DETAILS_LOADER)) {
            showToast("Unable to cache verified user details in device database...");
        }*/
        if (id == getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER)) {
            // Do nothing
        }
    }

    private UsersDetailsObject getRegisteredUserDetailsObj(ChangePasswordResponse.ChangePasswordDataObject changePasswordDataObj) {
        UsersDetailsObject usersDetailsObject = new UsersDetailsObject();

        if (changePasswordDataObj != null) {
            usersDetailsObject.setSiteID(!TextUtils.isEmpty(changePasswordDataObj.getSiteID()) ? changePasswordDataObj.getSiteID() : "");
            usersDetailsObject.setSitename(!TextUtils.isEmpty(changePasswordDataObj.getSiteName()) ? changePasswordDataObj.getSiteName() : "");
            usersDetailsObject.setSitePhotoURL(!TextUtils.isEmpty(changePasswordDataObj.getSitePhotoURL()) ? changePasswordDataObj.getSitePhotoURL() : "");
            usersDetailsObject.setClientURL(!TextUtils.isEmpty(changePasswordDataObj.getClientURL()) ? changePasswordDataObj.getClientURL() : "");
            usersDetailsObject.setSiteDescription(!TextUtils.isEmpty(changePasswordDataObj.getSiteDescription()) ? changePasswordDataObj.getSiteDescription() : "");
            usersDetailsObject.setSiteInfoLink(!TextUtils.isEmpty(changePasswordDataObj.getSiteInfoLink()) ? changePasswordDataObj.getSiteInfoLink() : "");
            usersDetailsObject.setEcUserID(!TextUtils.isEmpty(changePasswordDataObj.getEcUserID()) ? changePasswordDataObj.getEcUserID() : "");
            usersDetailsObject.setLoginName(!TextUtils.isEmpty(changePasswordDataObj.getLoginName()) ? changePasswordDataObj.getLoginName() : "");
            usersDetailsObject.setForename(!TextUtils.isEmpty(changePasswordDataObj.getForename()) ? changePasswordDataObj.getForename() : "");
            usersDetailsObject.setDOB(!TextUtils.isEmpty(changePasswordDataObj.getDob()) ? changePasswordDataObj.getDob() : "");
            usersDetailsObject.setSurname(!TextUtils.isEmpty(changePasswordDataObj.getSurname()) ? changePasswordDataObj.getSurname() : "");
            usersDetailsObject.setAge(!TextUtils.isEmpty(changePasswordDataObj.getAge()) ? changePasswordDataObj.getAge() : "");
            usersDetailsObject.setGender(!TextUtils.isEmpty(changePasswordDataObj.getGender()) ? changePasswordDataObj.getGender() : "");
            usersDetailsObject.setGenderValue(!TextUtils.isEmpty(changePasswordDataObj.getGenderValue()) ? changePasswordDataObj.getGenderValue() : "");
            usersDetailsObject.setPhoto(!TextUtils.isEmpty(changePasswordDataObj.getPhoto()) ? changePasswordDataObj.getPhoto() : "");
            usersDetailsObject.setToken(!TextUtils.isEmpty(changePasswordDataObj.getToken()) ? changePasswordDataObj.getToken() : "");
        }
        return usersDetailsObject;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.newpasswordcontainer:*/
            case R.id.confirmpasswordcontainer:
                if (view instanceof FloatLabeledEditText) {
                    ((FloatLabeledEditText) view).getEditText().requestFocus();
                    CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                }
                break;
            case R.id.generateTokenTextView:
                displayProgressLoader(false);
                final ResendOTPLoader resentOtpLoader = new ResendOTPLoader(this, mInputEmailID, selectedSiteId, CommonUtils.getDeviceId(this));
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GENERATE_OTP_LOADER), resentOtpLoader);
                break;
            case R.id.submitBtn:
                if (validateUserInputs()) {
                    displayProgressLoader(false);
                    // TODO need to pass dynamic SiteID
                    /*byte[] password = newPasswordEditText.getText().toString().trim().getBytes();
                    try {
                        encryptedPasswordData = AESHelper.encrypt("AXSYS", newPasswordEditText.getText().toString().trim());
                        System.out.println("ChangePasswordEncryptDecrypt" + encryptedPasswordData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                    final ChangePasswordVerifyOtpLoader verifyOtpLoader = new ChangePasswordVerifyOtpLoader(this, CommonUtils.getDeviceId(getApplicationContext()),
                            tokenKeyEditText.getText().toString().trim(), selectedSiteId, newPasswordEditText.getText().toString().trim());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(VERIFY_OTP_AND_REGISTER_LOADER), verifyOtpLoader);

                    //final    VerifyOtpLoader = new ResendOTPLoader(this, mInputEmailID, selectedSiteId, CommonUtils.getDeviceId(this));
                    //getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GENERATE_OTP_LOADER), VerifyOtpLoader);
                }
                break;
            case R.id.passwordHintImageView:
                // display hint text in dialog
                if (mPasswordRulesResponse != null) {
                    String passwordRulesMsg = "<h3><b>Strong:</b></h3><p>" + mPasswordRulesResponse.getStrongPasswordRuleMsg() + "</p><br>" +
                            "<h3><b>Medium:</b></h3><p>" + mPasswordRulesResponse.getMediumPasswordRuleMsg() + "</p><br>" +
                            "<h3><b>Weak:</b></h3><p>" + mPasswordRulesResponse.getWeakPasswordRuleMsg() + "</p><br>";

                    MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
                    final Bundle bundle = new Bundle();
                    bundle.putString(MessageDialogFragment.MESSAGE_KEY, Html.fromHtml(passwordRulesMsg).toString());
                    messageDialogFragment.setArguments(bundle);
                    showDialog(messageDialogFragment);
                }
                break;


        }
    }

    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(tokenKeyEditText.getText().toString().trim())) {
            showToast(R.string.input_received_token);
            return false;
        }

        if (newPasswordEditText.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(newPasswordEditText.getText().toString().trim())) {
                showToast(R.string.input_new_password);
                return false;
            }
        }

        if (confirmPasswordEditText.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(confirmPasswordEditText.getText().toString().trim())) {
                showToast(R.string.input_confirm_password);
                return false;
            }
        }

        if (newPasswordEditText.getVisibility() == View.VISIBLE && confirmPasswordEditText.getVisibility() == View.VISIBLE) {
            if (!newPasswordEditText.getText().toString().trim().equals(confirmPasswordEditText.getText().toString().trim())) {
                showToast(R.string.input_pasword_not_matched);
                return false;
            }
        }

        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        String passwordText = newPasswordEditText.getText().toString().trim();

        if (!TextUtils.isEmpty(passwordText)) {
            //passwordStrengthTextView.setVisibility(View.VISIBLE);
            if (mPasswordRulesResponse.isStrongPassword(passwordText)) {
                // passwordStrengthTextView.setText(String.format(getString(R.string.msg_password_strength), getString(R.string.pwd_strong)));
                newpasswordcontainer.setHint("password strength is strong");
            } else if (mPasswordRulesResponse.isMediumPassword(passwordText)) {
                //passwordStrengthTextView.setText(String.format(getString(R.string.msg_password_strength), getString(R.string.pwd_medium)));
                newpasswordcontainer.setHint("password strength is medium");
            }
//            else if (mPasswordRulesResponse.isWeakPassword(passwordText)) {
//               myTextView.setHint("My conditional hint"); passwordStrengthTextView.setText(String.format(getString(R.string.msg_password_strength), " Weak"));
//            }
            else {
                //passwordStrengthTextView.setText(String.format(getString(R.string.msg_password_strength), getString(R.string.pwd_weak)));
                newpasswordcontainer.setHint("password strength is weak");
            }
        } else {
            newpasswordcontainer.setHint("New Password");
        }
    }
}