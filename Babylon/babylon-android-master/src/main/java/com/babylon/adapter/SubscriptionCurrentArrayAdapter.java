package com.babylon.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.model.Patient;
import com.babylon.model.Region;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SubscriptionCurrentArrayAdapter extends ArrayAdapter<Patient>{

    private final SimpleDateFormat simpleDateFormatExpiresOn = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    private final Context context;
    private final Patient mainPatient;
    private final List<Patient> patients;
    private static final int ITEM_TYPE_COUNT = 2;
    private static final int ITEM_TYPE_HEADER_CELL = 0;
    private static final int ITEM_TYPE_SUBSCRIPTION_CELL = 1;

    private OnSubscriptionCancelListener onSubscriptionCancelListener;


    public SubscriptionCurrentArrayAdapter(Context context, Patient mainPatient, List<Patient> patients, OnSubscriptionCancelListener onSubscriptionCancelListener) {
        super(context, R.layout.list_item_subscription_current, patients);
        this.context = context;
        this.mainPatient = mainPatient;
        this.patients = patients;
        this.onSubscriptionCancelListener = onSubscriptionCancelListener;
    }

    @Override
    public int getCount() { return (patients.size() == 0) ? 2 : patients.size()+3; }

    @Override
    public int getViewTypeCount() {
        return ITEM_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0 || position == 2) ? ITEM_TYPE_HEADER_CELL : ITEM_TYPE_SUBSCRIPTION_CELL;
    }

    private class ViewHolderCell {
        public TextView nameTextView;
        public TextView costTextView;
        public TextView expireTextView;
        public ImageButton cancelButton;
        public ImageView selectorImageView;
    }

    private class ViewHolderHeader {
        public TextView titleTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;

        int type = getItemViewType(position);

        if (type == ITEM_TYPE_SUBSCRIPTION_CELL) {

            ViewHolderCell viewHolder = new ViewHolderCell();
            if (view == null) {
                view = inflater.inflate(R.layout.list_item_subscription_current, parent, false);
                viewHolder.nameTextView = (TextView)view.findViewById(R.id.subscription_name_textview);
                viewHolder.costTextView = (TextView)view.findViewById(R.id.subscription_cost_textview);
                viewHolder.expireTextView = (TextView)view.findViewById(R.id.subscription_expire_textview);
                viewHolder.cancelButton = (ImageButton)view.findViewById(R.id.subscription_cancel_imagebutton);
                viewHolder.selectorImageView = (ImageView)view.findViewById(R.id.subscription_current_selector_arrow_imageview);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolderCell)view.getTag();
            }

            final Patient patient = getPatientAtPosition(position);

            if (patient != null) {

                viewHolder.cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSubscriptionCancelListener.onCancelSubscription(patient);
                    }
                });

                viewHolder.cancelButton.setVisibility(onSubscriptionCancelListener.isEditable() ? View.VISIBLE : View.GONE);
                viewHolder.selectorImageView.setVisibility(onSubscriptionCancelListener.isEditable() ? View.VISIBLE :
                        View.GONE);

                if (patient.getPromotion() != null) // promotion
                {
                    String expiryDateString = context.getString(R.string.subscription_current_notAvailable);

                    if (patient.getPromotion().getExpiresOn() != null && patient.getPromotion().getExpiresOn() instanceof Date) {
                        String templateString = context.getString(R.string.subscription_current_expiresTemplate);
                        expiryDateString = String.format(templateString, simpleDateFormatExpiresOn.format(patient.getPromotion().getExpiresOn()));
                    }

                    viewHolder.expireTextView.setVisibility(View.VISIBLE);
                    viewHolder.expireTextView.setText(expiryDateString);
                    viewHolder.expireTextView.setTypeface(null, Typeface.ITALIC);
                    viewHolder.costTextView.setText(patient.getPromotion().getName());
                    viewHolder.selectorImageView.setVisibility(View.GONE);
                }
                else // subscription or plan
                {
                    viewHolder.expireTextView.setVisibility(View.GONE);

                    // if the subscription is filled, use it
                    if (patient.getSubscription() != null && !patient.getSubscription().isFree())
                    {
                        Region region = App.getInstance().getCurrentRegion();
                        String costString = String.format(context.getString(R.string.subscription_current_perMonthTemplate), region.getFormattedPriceString(patient.getSubscription().getPrice()));
                        viewHolder.costTextView.setText(costString);
                    }
                    // if not, use the default plan of the region (normally PAYG)
                    else
                    {
                        viewHolder.costTextView.setText(patient.getSubscription().getPlan().getTitle());
                        viewHolder.cancelButton.setVisibility(View.GONE);
                    }
                }


                    viewHolder.nameTextView.setText(patient.getFirstName());

            }

        } else {

            ViewHolderHeader viewHolder = new ViewHolderHeader();
            if (view == null) {
                view = inflater.inflate(R.layout.list_item_subscription_subheader, parent, false);
                viewHolder.titleTextView = (TextView)view.findViewById(R.id.textview_subscription_subheader);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolderHeader)view.getTag();
            }

            if  (position == 0)
                viewHolder.titleTextView.setText(R.string.subscription_current_header_main);
            else
                viewHolder.titleTextView.setText(R.string.subscription_current_header_additional);
        }
        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) == ITEM_TYPE_SUBSCRIPTION_CELL;
    }

    public Patient getPatientAtPosition(int position) {
        Patient patient = null;
        if (getItemViewType(position) == ITEM_TYPE_SUBSCRIPTION_CELL) {
            patient = (position == 1) ? mainPatient : patients.get(position-3);
        }
        return patient;
    }




    public interface OnSubscriptionCancelListener {
        public void onCancelSubscription(Patient patient);
        public boolean isEditable();
    }
}
