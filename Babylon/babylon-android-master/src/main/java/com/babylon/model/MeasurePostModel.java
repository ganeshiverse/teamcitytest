package com.babylon.model;

import com.babylon.enums.MonitorMeParam;
import com.babylon.utils.L;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MeasurePostModel {
    private static final String TAG = MeasurePostModel.class.getSimpleName();

    private MeasureBatch measures = new MeasureBatch();

    public void add(MonitorMeFeatures.Feature monitorMeFeature) {
        Measure measure = new Measure(monitorMeFeature.getDate(), monitorMeFeature.getFloatValue());
        final MonitorMeParam monitorMeParam = MonitorMeParam.getMonitorMeParameter(monitorMeFeature.getId());

        switch (monitorMeParam) {
            case HEART_TREATMENT_ID:
                if (measures.heartTreatment == null) measures.heartTreatment = new LinkedList<>();
                measures.heartTreatment.add(measure);
                break;
            case DIABETIC_ID:
                if (measures.diabetic == null) measures.diabetic = new LinkedList<>();
                measures.diabetic.add(measure);
                break;
            case STRESS_ID:
                if (measures.stress == null) measures.stress = new LinkedList<>();
                measures.stress.add(measure);
                break;
            case SYSTOLIC_BP_ID:
                if (measures.systolicBP == null) measures.systolicBP = new LinkedList<>();
                measures.systolicBP.add(measure);
                break;
            case DIASTOLIC_BP_ID:
                if (measures.diastolicBP == null) measures.diastolicBP = new LinkedList<>();
                measures.diastolicBP.add(measure);
                break;
            case CHOLESTEROL_ID:
                if (measures.cholesterol == null) measures.cholesterol = new LinkedList<>();
                measures.cholesterol.add(measure);
                break;
            case CHOLESTEROL_HDL_ID:
                if (measures.cholesterolHDL == null) measures.cholesterolHDL = new LinkedList<>();
                measures.cholesterolHDL.add(measure);
                break;
            case CHOLESTEROL_LDL_ID:
                if (measures.cholesterolLDL == null) measures.cholesterolLDL = new LinkedList<>();
                measures.cholesterolLDL.add(measure);
                break;
            case CURRENT_HEIGHT_ID:
                if (measures.currentHeight == null) measures.currentHeight = new LinkedList<>();
                measures.currentHeight.add(measure);
                break;
            case WEIGHT_ID:
                if (measures.currentWeight == null) measures.currentWeight = new LinkedList<>();
                measures.currentWeight.add(measure);
                break;
            case GOAL_WEIGHT_ID:
                if (measures.goalWeight == null) measures.goalWeight = new LinkedList<>();
                measures.goalWeight.add(measure);
                break;
            case TRIGLYCERIDE_ID:
                if (measures.triglyceride == null) measures.triglyceride = new LinkedList<>();
                measures.triglyceride.add(measure);
                break;
            case RESTING_PULSE_ID:
                if (measures.heartRestingPulse == null) measures.heartRestingPulse = new LinkedList<>();
                measures.heartRestingPulse.add(measure);
                break;
            case SMOKER_ID:
                if (measures.smoke == null) measures.smoke = new LinkedList<>();
                measures.smoke.add(measure);
                break;
            default:
                L.d(TAG, "Unknown measure type");
        }
    }

    static class MeasureBatch {
        private List<Measure> heartTreatment; // possible heartTreatment values: 0,1
        private List<Measure> diabetic; // possible diabetic values: 0,1

        /**
         * possible values: 0, 1, 2
         * {@link com.babylon.enums.SmokeStatus}
         */
        private List<Measure> smoke;

        private List<Measure> stress;
        private List<Measure> systolicBP;
        private List<Measure> diastolicBP;
        private List<Measure> cholesterol;
        private List<Measure> cholesterolHDL;
        private List<Measure> cholesterolLDL;
        private List<Measure> currentHeight;
        private List<Measure> currentWeight;
        private List<Measure> goalWeight;
        private List<Measure> triglyceride;
        private List<Measure> heartRestingPulse;
    }

    static class Measure {
        /**
         * 	If you want that measure will be automatically replaced for this date use 1,
         * 	Otherwise use 0 for save this measure with current timestamp.
         *
         * 	Sometimes you don't know the exact time when value have been measured.
         * 	But would like to set this value at some day. In this case this value should be true.
         */
        private Integer autoDate = 1; // possible values : 0, 1
        private Long timestamp;
        private Float value;

        public Measure(Date timestamp, Float value) {
            this.timestamp = timestamp.getTime()
                    / android.text.format.DateUtils.SECOND_IN_MILLIS;
            this.value = value;
        }
    }
}
