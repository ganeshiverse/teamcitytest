package com.iversecomics.client.refresh;

import java.util.ArrayList;
import java.util.List;

import android.test.AndroidTestCase;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.MediumTest;

public class HashedFutureTaskTest extends AndroidTestCase {

    @MediumTest
    public void testCollectionContains() {
        HashedFutureTask<Void> ta = new HashedFutureTask<Void>(new MessageTask("A"), null);
        HashedFutureTask<Void> tb = new HashedFutureTask<Void>(new MessageTask("B"), null);

        List<HashedFutureTask<Void>> tasks = new ArrayList<HashedFutureTask<Void>>();
        tasks.add(ta);
        tasks.add(tb);

        assertTrue("Tasks should contain A", tasks.contains(ta));
        assertTrue("Tasks should contain B", tasks.contains(tb));

        HashedFutureTask<Void> b2 = new HashedFutureTask<Void>(new MessageTask("B"), null);
        assertTrue("Tasks should contain new B", tasks.contains(b2));

        HashedFutureTask<Void> tz = new HashedFutureTask<Void>(new MessageTask("Z"), null);
        assertFalse("Tasks should not contain Z", tasks.contains(tz));
    }

    @MediumTest
    public void testEqualsFailure() {
        HashedFutureTask<Void> ta = new HashedFutureTask<Void>(new MessageTask("A"), null);
        HashedFutureTask<Void> tb = new HashedFutureTask<Void>(new MessageTask("B"), null);

        assertFalse("Tasks should not be equal", ta.equals(tb));
        MoreAsserts.assertNotEqual("Task.hashcodes", ta.hashCode(), tb.hashCode());
    }

    @MediumTest
    public void testEqualsSuccess() {
        HashedFutureTask<Void> ta = new HashedFutureTask<Void>(new MessageTask("A"), null);
        HashedFutureTask<Void> tb = new HashedFutureTask<Void>(new MessageTask("A"), null);

        assertTrue("Tasks should be equal", ta.equals(tb));
        assertEquals("Task.hashcodes", ta.hashCode(), tb.hashCode());
    }
}
