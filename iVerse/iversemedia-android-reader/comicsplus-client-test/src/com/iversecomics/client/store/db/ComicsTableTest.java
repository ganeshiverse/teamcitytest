package com.iversecomics.client.store.db;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.util.text.StringUtils;

public class ComicsTableTest extends AbstractComicStoreProviderTestCase {
    private Comic querySingleComic(String msg, Uri contentUri, String selection, String selectionArgs[]) {
        Cursor c = null;
        try {
            c = resolver.query(contentUri, null, selection, selectionArgs, null);
            StringBuilder msgb = new StringBuilder();
            msgb.append(msg);
            msgb.append(" (where=").append(selection);
            msgb.append(") (whereArgs=").append(StringUtils.join(selectionArgs, "|"));
            msgb.append(")");
            assertDatabaseRowCount(msgb.toString(), c, 1);
            Comic comic = ComicsTable.fromCursor(c);
            return comic;
        } finally {
            DBUtil.close(c);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testComicInsertObject() {
        Comic comic = new Comic();
        comic.setComicId("test.comic.object");
        comic.setName("TestComicObject");

        ContentValues values = ComicsTable.asValues(comic);

        Uri uri = resolver.insert(ComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Comic", uri);
    }

    @MediumTest
    public void testComicInsertSimple() {
        ContentValues values = new ContentValues();
        values.put(ComicsTable.COMICID, "test.comic.simple");
        values.put(ComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(ComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Comic", uri);
    }

    @MediumTest
    public void testComicInsertThenInsert() {
        String id = "test.comic";

        ContentValues values = new ContentValues();
        values.put(ComicsTable.COMICID, id);
        values.put(ComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(ComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Comics", uri);

        // Now try to find it.
        String selection = String.format("%s.%s = ?", ComicsTable.TABLE, ComicsTable.COMICID);
        String selectionArgs[] = new String[] { id };
        String msg = "Comic (initial)";
        Comic comic1 = querySingleComic(msg, ComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestComic", comic1.getName());

        // Insert another entry, hopefully overwriting the original due to a proper constraint setup.
        ContentValues second = new ContentValues();
        second.put(ComicsTable.COMICID, id);
        second.put(ComicsTable.NAME, "Test Comic Post"); // New name (with space)

        Uri uriSecond = resolver.insert(ComicsTable.CONTENT_URI, second);
        assertDatabaseUriHasDBId("Comics", uriSecond);

        // Try to find it again.
        msg = "Comic (post insert)";
        Comic comic2 = querySingleComic(msg, ComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Comic Post", comic2.getName());
    }

    @MediumTest
    public void testComicInsertThenUpdate() {
        String id = "test.comic";

        ContentValues values = new ContentValues();
        values.put(ComicsTable.COMICID, id);
        values.put(ComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(ComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Comics", uri);

        // Now try to find it.
        String selection = String.format("%s.%s = ?", ComicsTable.TABLE, ComicsTable.COMICID);
        String selectionArgs[] = new String[] { id };
        String msg = "Comic (initial)";
        Comic comic1 = querySingleComic(msg, ComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestComic", comic1.getName());

        // Update entry
        ContentValues update = new ContentValues();
        update.put(ComicsTable.COMICID, id);
        update.put(ComicsTable.NAME, "Test Comic Post"); // New name (with space)

        String where = ComicsTable.COMICID + " = ?";
        String whereArgs[] = new String[] { id };
        int count = resolver.update(ComicsTable.CONTENT_URI, update, where, whereArgs);
        assertEquals("Update count", 1, count);

        // Try to find it again.
        msg = "Comic (post update)";
        Comic comic2 = querySingleComic(msg, ComicsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Comic Post", comic2.getName());
    }

    @MediumTest
    public void testComicQueryByComicId() {
        String comicId = "test.comic.query.bycomicid";
        ContentValues values = new ContentValues();
        values.put(ComicsTable.COMICID, comicId);
        values.put(ComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(ComicsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Comics", uri);

        Uri singleUri = Uri.withAppendedPath(ComicsTable.CONTENT_URI_BY_COMICID, comicId);

        // Try to fetch based on URI.
        String msg = "Specific Comic by uri+comicId";
        Comic comic = querySingleComic(msg, singleUri, null, null);
        assertEquals("Comic.comicId", comicId, comic.getComicId());
        assertEquals("Comic.name", values.get(ComicsTable.NAME), comic.getName());
    }

    @MediumTest
    public void testComicQueryByDBId() {
        String comicId = "test.comic.query.bydbid";
        ContentValues values = new ContentValues();
        values.put(ComicsTable.COMICID, comicId);
        values.put(ComicsTable.NAME, "TestComic");

        Uri uri = resolver.insert(ComicsTable.CONTENT_URI, values);
        long dbId = getDatabaseId("Comics", uri);

        Uri singleUri = ContentUris.withAppendedId(ComicsTable.CONTENT_URI, dbId);

        // Try to fetch based on URI.
        String msg = "Specific Comic by DB Id";
        Comic comic = querySingleComic(msg, singleUri, null, null);
        assertEquals("Comic.comicId", comicId, comic.getComicId());
        assertEquals("Comic.name", values.get(ComicsTable.NAME), comic.getName());
    }
}
