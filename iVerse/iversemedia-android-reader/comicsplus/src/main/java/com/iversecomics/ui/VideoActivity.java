package com.iversecomics.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.iversecomics.archie.android.R;

public class VideoActivity extends Activity {

    public static final String EXTRA_VIDEO_URL = "extraVideoUrl";

    ProgressDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        VideoView videoView = (VideoView) findViewById(R.id.demo_video);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.requestFocus();
        videoView.start();

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(EXTRA_VIDEO_URL)) {
            videoView.setVideoURI(Uri.parse(extras.getString(EXTRA_VIDEO_URL)));

            progress = ProgressDialog.show(this, "", "Loading...", true);

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    progress.dismiss();
                }
            });
        }

    }

}