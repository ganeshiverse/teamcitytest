package com.iversecomics.client.refresh;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * A specialized LinkedBlockingQueue that rejects offers of duplicate Runnables (tasks)
 */
public class HashedBlockingQueue extends LinkedBlockingQueue<Runnable> {
    private static final Logger LOG = LoggerFactory.getLogger(HashedBlockingQueue.class);
    private static final long serialVersionUID = -7044382014420373935L;
    private RecentHashes recentHashes;

    public HashedBlockingQueue() {
        this(new RecentHashes());
    }

    public HashedBlockingQueue(RecentHashes recentHashes) {
        this.recentHashes = recentHashes;
    }

    @Override
    public boolean offer(Runnable e) {
        if (recentHashes.hasSeen(e.hashCode())) {
            LOG.debug("Not adding recently seen runnable: %s", e);
            return false;
        } else if (contains(e)) {
            LOG.debug("Not adding duplicate runnable: %s", e);
            return false;
        }
        LOG.debug("Offered %s", e);
        return super.offer(e);
    }
}