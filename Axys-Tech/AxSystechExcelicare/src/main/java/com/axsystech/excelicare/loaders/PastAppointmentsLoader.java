package com.axsystech.excelicare.loaders;

        import android.content.Context;

        import com.axsystech.excelicare.data.responses.AppointmentListResponse;
        import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
        import com.axsystech.excelicare.network.ServerMethods;
        import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/27/2015.
 */
public class PastAppointmentsLoader extends DataAsyncTaskLibLoader<AppointmentListResponse> {

    private String token;
    private String panelId;
    private String patientId;
    private int pageCount;
    private String pageSize;
    private String isDictonaryResponse;

    public PastAppointmentsLoader(Context context, String token, String panelId, String patientId, int pageCount, String pageSize,
                                  String isDictonaryResponse) {
        super(context);
        this.token = token;
        this.panelId = panelId;
        this.patientId = patientId;
        this.pageCount = pageCount;
        this.pageSize = pageSize;
        this.isDictonaryResponse = isDictonaryResponse;
    }

    @Override
    protected AppointmentListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getPastAppointmentsList(token, panelId, patientId, pageCount, pageSize, isDictonaryResponse);
    }
}
