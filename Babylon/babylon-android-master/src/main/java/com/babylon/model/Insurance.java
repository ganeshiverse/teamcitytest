package com.babylon.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Babylon on 04/06/2015.
 */
public class Insurance {

    @Expose
    private Integer id;
    @Expose
    private String name;
    @SerializedName("take_payment")
    @Expose
    private Boolean takePayment;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @Expose
    private List<Object> memberships = new ArrayList<Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The takePayment
     */
    public Boolean getTakePayment() {
        return takePayment;
    }

    /**
     *
     * @param takePayment
     * The take_payment
     */
    public void setTakePayment(Boolean takePayment) {
        this.takePayment = takePayment;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The memberships
     */
    public List<Object> getMemberships() {
        return memberships;
    }

    /**
     *
     * @param memberships
     * The memberships
     */
    public void setMemberships(List<Object> memberships) {
        this.memberships = memberships;
    }

}