package com.iversecomics.client.account;

import android.app.AlertDialog;
import android.content.Context;

import com.iversecomics.archie.android.R;

public class EmailAddresRequiredDialogBuilder extends AlertDialog.Builder {

    public EmailAddresRequiredDialogBuilder(final Context context) {
        super(context);

        setIcon(R.drawable.icon);
        setTitle(R.string.email_address_required_title);
        setMessage(R.string.email_address_required_message);

    }
}
