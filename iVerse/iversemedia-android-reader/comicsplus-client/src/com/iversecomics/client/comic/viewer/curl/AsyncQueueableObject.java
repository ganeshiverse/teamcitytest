package com.iversecomics.client.comic.viewer.curl;

/**
 * Class which supports a Last-In/First-Out queue for operations that require background processing and an update to the UI thread
 *
 * Adapted from: https://github.com/elsewhat/com.elsewhat.android.slideshow/blob/master/src/com/elsewhat/android/slideshow/api/AsyncReadQueue.java
 * by dagfinn.parnas
 */
public interface AsyncQueueableObject {
    /**
     *
     * Perform the operation in a background thread
     * @return
     */
    public void performOperation();

    /**
     * Handle the result in the UI thread
     *
     * @param result
     */
    public void handleOperationResult();


}