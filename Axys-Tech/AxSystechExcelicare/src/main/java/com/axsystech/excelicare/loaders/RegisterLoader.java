package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class RegisterLoader extends DataAsyncTaskLibLoader<RegisterResponse> {

    private final String mSiteId;
    private final String mDeviceId;
    private final String mUserName;

    public RegisterLoader(final Context context, final String deviceId, final String siteId, final String userName) {
        super(context);
        mSiteId = siteId;
        mDeviceId = deviceId;
        mUserName = userName;
    }

    @Override
    protected RegisterResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getRegistrationsDetailsByDevice(mDeviceId, mSiteId, mUserName);
    }
}