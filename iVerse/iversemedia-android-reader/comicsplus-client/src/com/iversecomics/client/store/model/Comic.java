package com.iversecomics.client.store.model;

import android.net.Uri;

import com.iversecomics.client.store.ServerConfig;

public class Comic {

	private static final int MAX_IMAGE_SIZE = 5;
	
	private long   dbId = -1;
    private String comicId;
    private String name;
    private String comicBundleName;
    private double avgRating;
    private String description;
    private String imageFileName;
    private String legal;
    private int    number;
    private int    pricingTier;
    private boolean tapjoy;
    private String amountUSD;
    private String productType;
    private String rawName;
    private String seriesBundleName;
    private String sku;
    private String publisherId;
    private String publisherLegacyId;
    private String publisherName;
    private String assetTypes[];
    private boolean subscription;


    public Comic() {
        /* do nothing */
    }

    public Comic(String id, String name) {
        this.comicId = id;
        this.name = name;
    }

    public String getAmountUSD() {
        return amountUSD;
    }

    public String[] getAssetTypes() {
        return assetTypes;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public String getComicBundleName() {
        return comicBundleName;
    }

    public String getComicId() {
        return comicId;
    }

    public long getDbId() {
        return dbId;
    }

    public String getDescription() {
        return description;
    }

    public String getImageFileName() {
        return imageFileName;
    }
    
    public String getImageFileNameFromIndex(int index) {
    	if(index < 0 || index > MAX_IMAGE_SIZE) {
    		index = 0;
    	}
    	return getImageFileName() + "_" + index;
    }
    
    public Uri getPreviewImageUriForIndex(int index) {
    	return ServerConfig.getDefault().getPreviewImageUri(getImageFileNameFromIndex(index));
    }

    public String getLegal() {
        return legal;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public int getPricingTier() {
        return pricingTier;
    }

    public String getProductType() {
        return productType;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public String getPublisherLegacyId() {
        return publisherLegacyId;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public String getRawName() {
        return rawName;
    }

    public String getSeriesBundleName() {
        return seriesBundleName;
    }

    public String getSku() {
        return sku;
    }

    public boolean isFree() {
        return pricingTier == 0;
    }

    public void setAmountUSD(String amountUSD) {
        this.amountUSD = amountUSD;
    }

    public void setAssetTypes(String... types) {
        this.assetTypes = types;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public void setComicBundleName(String comicBundleName) {
        this.comicBundleName = comicBundleName;
    }

    public void setComicId(String comicId) {
        this.comicId = comicId;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setPricingTier(int pricingTier) {
        this.pricingTier = pricingTier;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public void setPublisherLegacyId(String publisherLegacyId) {
        this.publisherLegacyId = publisherLegacyId;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public void setRawName(String rawName) {
        this.rawName = rawName;
    }

    public void setSeriesBundleName(String seriesBundleName) {
        this.seriesBundleName = seriesBundleName;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

	public boolean getSubscription() {
		return subscription;
	}

	public void setSubscription(boolean subscription) {
		this.subscription = subscription;
	}
	
    public boolean isTapjoy() {
		return tapjoy;
	}

	public void setTapjoy(boolean tapjoy) {
		this.tapjoy = tapjoy;
	}

}
