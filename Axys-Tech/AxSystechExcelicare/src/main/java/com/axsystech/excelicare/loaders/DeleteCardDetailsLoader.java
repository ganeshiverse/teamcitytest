package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by SomeswarReddy on 7/13/2015.
 */
public class DeleteCardDetailsLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String deleteUrl;
    private String message;

    public DeleteCardDetailsLoader(Context context, String deleteUrl, String message) {
        super(context);
        this.deleteUrl = deleteUrl;
        this.message = message;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().deleteCardDetails(deleteUrl, message);
    }
}
