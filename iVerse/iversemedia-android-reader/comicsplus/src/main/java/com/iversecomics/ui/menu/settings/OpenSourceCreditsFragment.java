package com.iversecomics.ui.menu.settings;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.iversecomics.archie.android.R;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.ArrayList;

public class OpenSourceCreditsFragment extends ListFragment {

    private final String TAG = OpenSourceCreditsFragment.class.getSimpleName();
    private static final Logger LOG = LoggerFactory.getLogger(OpenSourceCreditsFragment.class);

    private OpenSourceCreditsListAdapter mAdapter;
    private ArrayList<OpenSourceCredit> mCredits;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (ViewGroup) inflater.inflate(R.layout.fragment_open_source_credits, container, false);

        rootView.findViewById(R.id.action_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCredits = new ArrayList<OpenSourceCredit>();
//        mCredits.add(new OpenSourceCredit("SlidingMenu", "Jeremy Feinstein", "https://github.com/jfeinstein10/SlidingMenu/blob/master/LICENSE.txt"));
        mCredits.add(new OpenSourceCredit("SlidingMenu", "Jeremy Feinstein", "file:///android_asset/apache2.0_license.html"));

        mAdapter = new OpenSourceCreditsListAdapter(getActivity());
        for (OpenSourceCredit credit : mCredits)
            mAdapter.add(credit);
        setListAdapter(mAdapter);
    }


    @Override
    public void onListItemClick(final ListView l, final View v, final int position, long id) {
        OpenSourceCredit credit = mCredits.get(position);
//	    Intent intent = new Intent(Intent.ACTION_VIEW);
//      intent.setData(Uri.parse(credit.licenseUrl));
//      startActivity(intent);

        int fragContainerId = ((ViewGroup) getView().getParent()).getId();
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
                .addToBackStack(null)
                .replace(fragContainerId, LicenseFragment.newInstance(credit.licenseUrl))
                .commit();
    }


    private class OpenSourceCreditsListAdapter extends ArrayAdapter<OpenSourceCredit> {
        LayoutInflater mInflater;

        Context context;
        ContentResolver contentResolver;

        public OpenSourceCreditsListAdapter(Context context) {
            super(context, R.layout.item_open_source_credit);
            this.context = context;
            this.contentResolver = context.getContentResolver();

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null)
                view = mInflater.inflate(R.layout.item_open_source_credit, parent, false);

            OpenSourceCredit credit = getItem(position);
            ((TextView) view.findViewById(R.id.name)).setText(credit.name);
            ((TextView) view.findViewById(R.id.author)).setText(credit.author);
            return view;
        }
    }


    class OpenSourceCredit {

        public OpenSourceCredit(String name, String author, String url) {
            this.name = name;
            this.author = author;
            this.licenseUrl = url;
        }

        String name;
        String author;
        String licenseUrl;
    }

}