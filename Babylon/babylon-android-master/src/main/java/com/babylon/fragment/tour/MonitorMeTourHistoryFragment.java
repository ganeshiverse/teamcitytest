package com.babylon.fragment.tour;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.babylon.R;


public class MonitorMeTourHistoryFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monitor_me_tour_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ImageButton okButton = (ImageButton) view.findViewById(R.id.okButton);
        okButton.setOnClickListener(onOkClickListener);
    }

    private final View.OnClickListener onOkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((OnCloseTourEventListener) getActivity()).onCloseTourEvent();
        }
    };
}