package com.iversecomics.app;


import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap.Config;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;

import com.iversecomics.bundle.Ownership;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Preferences;
import com.iversecomics.client.Storage;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.db.ComicStoreDatabaseHelper;
import com.iversecomics.client.store.tasks.ServerConfigTask;
import com.iversecomics.client.util.Time;
import com.iversecomics.data.DataProducer;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.logging.TimeLogger;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.LogoutEvent;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


public class ComicApp extends IverseApplication {
    private final static Logger LOG = LoggerFactory.getLogger(ComicApp.class);

    static final String FRESHNESS_ID = "databasefreshness";
    private static final boolean DEV_MODE = false;

    static ComicApp app;

    boolean CLEAR_DB_FOR_TEST = false;

    public static ComicApp getInstance() {
        return app;
    }

    DataProducer dataProducer;

    @Override
    public void onCreate() {
        super.onCreate();
        assertPackageValid();

        TimeLogger.TimePoint startTime = TimeLogger.newTimePoint("create app");
        // Otto
        OttoBusProvider.getInstance().register(this);

        if (AppConstants.DEBUG) {
            StrictMode.setThreadPolicy(new ThreadPolicy.Builder().detectNetwork().penaltyDialog().build());
            //StrictMode.setVmPolicy(new VmPolicy.Builder().detectAll().penaltyDeath().build());
        }

        app = this;
        initImageLoader();

        // initialize project configuration with values from AppConstants for build variants
        initializeConfig();

        // Initialize preferences
        ComicPreferences.initialize(getApplicationContext());

        dataProducer = new DataProducer(getComicStore());
        OttoBusProvider.getInstance().register(dataProducer);

        startTime.logEnd();
    }

    private void initImageLoader() {
        DisplayImageOptions defaultDisplayImageOptions = new DisplayImageOptions.Builder()
                .bitmapConfig(Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .displayer(new FadeInBitmapDisplayer(250))
                .resetViewBeforeLoading(true)
                .build();
        //Initialize Universal Image Loader
        ImageLoaderConfiguration imgConfig = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(3)
                .memoryCache(new WeakMemoryCache())
                .threadPriority(Thread.MAX_PRIORITY)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .defaultDisplayImageOptions(defaultDisplayImageOptions)
                .discCacheSize(50 * 1024 * 1024)
                .build();

        ImageLoader.getInstance().init(imgConfig);
        ImageLoader.getInstance().handleSlowNetwork(true);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        OttoBusProvider.getInstance().unregister(this);
        OttoBusProvider.getInstance().unregister(dataProducer);
    }

    /**
     * Logs out of the app.  To get notified of logout, you should register a BroadcastReceiver to listen to {@link #ACTION_LOGOUT}.
     */
    public void logout() {
        Ownership ownership = getOwnership();
        ownership.clearCredentials();
        final SharedPreferences preferences = getSharedPreferences(Preferences.NAME, 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString(Preferences.KEY_OWNER_EMAIL, ownership.getEmailAddress());
        editor.putString(Preferences.KEY_OWNER_PASSWORD, ownership.getPassword());
        editor.commit();

        final Task task = new ServerConfigTask(this, getComicStore());
        getTaskPool().submit(task);

        OttoBusProvider.getInstance().post(new LogoutEvent());
    }

    public void initializeConfig() {
        TimeLogger.TimePoint startTime = TimeLogger.newTimePoint("create app");
        initServerConfig();
        initStorage();
        checkClearDbForTest();
        startTime.logEnd();
    }

    private void initStorage() {
        // set a package specific directory using this running app context
        Storage.setExternalFilesDir(this.getBaseContext().getExternalFilesDir(null));
    }

    private void checkClearDbForTest() {
        if (CLEAR_DB_FOR_TEST) {
            LOG.warn("Testing Mode: Clearing Freshness Cache");
            long now = System.currentTimeMillis();
            getFreshness().clearExpired(now + Time.YEAR);
            LOG.warn("Testing Mode: Rebuilding Database");
            // Clear out the database
            ComicStoreDatabaseHelper helper = ComicStoreDatabaseHelper.getInstance(this);
            SQLiteDatabase db = helper.getWritableDatabase();
            helper.rebuild(db); // force rebuild

            ImageLoader.getInstance().getDiscCache().clear();
            ImageLoader.getInstance().getMemoryCache().clear();
        }
    }

    private void initServerConfig() {
        ServerConfigTask.setSharedSecret(AppConstants.SHARED_SECRET);
        ServerConfig.setServerUrl(DEV_MODE ? AppConstants.DEV_SERVER_URL : AppConstants.PROD_SERVER_URL);
    }

    public void checkFreshness() {
        long now = System.currentTimeMillis();
        getFreshness().clearExpired(now);
    }

    /**
     * runtime check to confirm that the package name was set correctly in
     * the build process
     */
    public void assertPackageValid() {
        //confirm that the config package name was set correctly in the build process
        if (!AppConstants.SKU_PACKAGE_NAME.equals(getPackageName())) {
            throw new IllegalStateException("ConfigProj.packageName does match " + getPackageName() +
                    " as set in this project's manifest");
        }
    }

    public DataProducer getDataProducer() {
        return dataProducer;
    }
}
