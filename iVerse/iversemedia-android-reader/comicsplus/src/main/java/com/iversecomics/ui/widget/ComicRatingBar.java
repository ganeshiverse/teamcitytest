package com.iversecomics.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.iversecomics.archie.android.R;

public class ComicRatingBar extends View {

    static final int DEFAULT_NUM_STARS = 5;
    static private final int MIN_WIDTH = 5;

    int numStars;
    int rating;

    public ComicRatingBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs,
                new int[]{android.R.attr.numStars, android.R.attr.rating}, 0, 0);
        try {
            numStars = ta.getInt(0, DEFAULT_NUM_STARS);
            int rating = ta.getInt(1, 0);
            if (numStars < 1) {
                numStars = 1;
            }
            setRating(rating);
        } finally {
            ta.recycle();
        }
    }

    Rect rectSrc = new Rect();
    Rect rectDst = new Rect();

    @Override
    public void onDraw(Canvas canvas) {

        int width = getWidth();
        int height = getHeight();

        Bitmap bitmapEmpty = ((BitmapDrawable) (getContext().getResources().getDrawable(R.drawable.rating_star_empty))).getBitmap();
        Bitmap bitmapFill = ((BitmapDrawable) (getContext().getResources().getDrawable(R.drawable.rating_star_fill))).getBitmap();

        rectSrc.left = 0;
        rectSrc.top = 0;
        rectSrc.right = bitmapEmpty.getWidth();
        rectSrc.bottom = bitmapEmpty.getHeight();
        rectDst.left = 0;
        rectDst.top = 0;
        rectDst.right = 0;
        rectDst.bottom = height;
        int starSize = width / numStars;
        for (int i = 0; i < rating; i++) {
            rectDst.left = starSize * i;
            rectDst.right = rectDst.left + starSize;
            canvas.drawBitmap(bitmapFill, rectSrc, rectDst, null);
        }
        for (int i = rating; i < numStars; i++) {
            rectDst.left = starSize * i;
            rectDst.right = rectDst.left + starSize;
            canvas.drawBitmap(bitmapEmpty, rectSrc, rectDst, null);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int n = (int) (event.getX() * numStars / getWidth());
            setRating(n + 1);
            if (listenerChange != null) {
                listenerChange.onChange(rating);
            }
        }
        return super.onTouchEvent(event);
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if (rating > numStars) {
            rating = numStars;
        }
        if (rating < 0) {
            rating = 0;
        }
        this.rating = rating;
        invalidate();
    }

    private int getWidthFromRatio(int height) {
        return height * numStars;
    }

    private int getHeightFromRatio(int width) {
        return width / numStars;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
            int desiredHeight = getHeightFromRatio(width);
            if (heightMode == MeasureSpec.EXACTLY) {
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                height = Math.min(desiredHeight, heightSize);
            } else {
                height = desiredHeight;
            }
        } else if (widthMode == MeasureSpec.AT_MOST) {
            if (heightMode == MeasureSpec.EXACTLY) {
                int desiredWidth = getWidthFromRatio(heightSize);
                width = Math.min(desiredWidth, widthSize);
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                int desiredWidth = MIN_WIDTH;
                int desiredHeight = getHeightFromRatio(MIN_WIDTH);
                width = Math.min(desiredWidth, widthSize);
                height = Math.min(desiredHeight, heightSize);
            } else {
                int desiredWidth = MIN_WIDTH;
                width = Math.min(desiredWidth, widthSize);
                height = getHeightFromRatio(width);
            }
        } else {
            if (heightMode == MeasureSpec.EXACTLY) {
                width = getWidthFromRatio(heightSize);
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                width = MIN_WIDTH;
                int desiredHeight = getHeightFromRatio(MIN_WIDTH);
                height = Math.min(desiredHeight, heightSize);
            } else {
                width = MIN_WIDTH;
                height = getHeightFromRatio(MIN_WIDTH);
            }
        }
        setMeasuredDimension(width, height);
    }

    ChangeListener listenerChange;

    public void setOnChangeListener(ChangeListener listener) {
        listenerChange = listener;
    }

    public static interface ChangeListener {
        public void onChange(int rating);
    }
}
