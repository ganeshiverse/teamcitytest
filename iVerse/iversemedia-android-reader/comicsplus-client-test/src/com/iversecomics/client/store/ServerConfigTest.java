package com.iversecomics.client.store;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import android.content.res.AssetManager;
import android.test.ActivityTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.io.IOUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.json.JSONTokener;

public class ServerConfigTest extends ActivityTestCase {
    private void assertUri(String msg, String expectedUri, URI actualUri) {
        assertEquals(msg, expectedUri, actualUri.toASCIIString());
    }

    private JSONObject loadAssetJsonObject(String assetName) throws IOException, JSONException {
        AssetManager am = getInstrumentation().getContext().getAssets();
        InputStream stream = null;
        InputStreamReader reader = null;
        try {
            stream = am.open(assetName);
            reader = new InputStreamReader(stream);
            JSONTokener jsontokener = new JSONTokener(reader);
            return new JSONObject(jsontokener);
        } finally {
            IOUtil.close(reader);
            IOUtil.close(stream);
        }
    }

    @MediumTest
    public void testParseJson() throws IOException, JSONException {
        ServerConfig config = new ServerConfig();
        JSONObject json = loadAssetJsonObject("api2-getConfig.json");
        config.setFromServerResponse(json);

        assertEquals("ServerConfig.serverApiVersion", 2, config.getServerApiVersion());

        assertUri(
                "ServerConfig.productsTopFreeUri",
                "https://development.iverseapps.com/products.php?p=613d32266c3d31&v=c90d1a861c4b0058fe492f17dcb4f609&action=free",
                config.getProductsTopFreeUri());

        assertUri(
                "ServerConfig.productsTopFreeUri",
                "https://development.iverseapps.com/products.php?p=613d32266c3d31&v=c90d1a861c4b0058fe492f17dcb4f609&action=search&terms=apple%20pie",
                config.getProductsBySearchUri("apple pie"));
    }
}
