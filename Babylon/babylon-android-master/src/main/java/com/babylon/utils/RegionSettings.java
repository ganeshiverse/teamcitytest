package com.babylon.utils;

import android.support.annotation.Nullable;
import com.babylon.App;
import com.babylon.model.Region;

public class RegionSettings {
    public boolean isMonitorMeEnabled() {
        final int regionId = App.getInstance().getPatient().getRegionId();

        boolean isMonitorMeEnabled = true;
        Region region = getRegion(regionId);

        if (region != null)
        {
            isMonitorMeEnabled = region.getEnableMonitorMe();
        }

        return isMonitorMeEnabled;
    }

    private
    @Nullable
    Region getRegion(int regionId) {
        Region region = null;
        final Region[] regions = PreferencesManager.getInstance().getRegions();
        if (regions != null) {
            for (Region countryRegion : regions) {
                if (countryRegion.getId() == regionId) {
                    region = countryRegion;
                    break;
                }
            }
        }

        return region;
    }
}
