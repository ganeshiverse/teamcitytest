package com.axsystech.excelicare.framework.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;

import com.axsystech.excelicare.R;
import com.mig35.injectorlib.utils.inject.InjectSavedState;

/**
 * Date: 17.06.14
 * Time: 22:11
 *
 * @author SomeswarReddy
 */
public class MessageDialogFragment extends BaseDialogFragment {

    public static final String MESSAGE_KEY = "com.axsystech.excelicare.framework.ui.dialogs.MessageDialogFragment.MESSAGE_KEY";

    @InjectSavedState
    private String mMessage;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null == getArguments() || TextUtils.isEmpty(getArguments().getString(MESSAGE_KEY))) {
            dismiss();
        } else {
            mMessage = getArguments().getString(MESSAGE_KEY);
        }
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (null != mMessage) {
            builder.setMessage(mMessage);
        }
        builder.setPositiveButton(R.string.m_done, null);

        return builder.create();
    }
}
