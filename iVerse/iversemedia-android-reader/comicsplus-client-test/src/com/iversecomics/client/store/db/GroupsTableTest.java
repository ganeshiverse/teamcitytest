package com.iversecomics.client.store.db;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.util.text.StringUtils;

public class GroupsTableTest extends AbstractComicStoreProviderTestCase {
    private Group querySingleGroup(String msg, Uri contentUri, String selection, String selectionArgs[]) {
        Cursor c = null;
        try {
            c = resolver.query(contentUri, null, selection, selectionArgs, null);
            StringBuilder msgb = new StringBuilder();
            msgb.append(msg);
            msgb.append(" (where=").append(selection);
            msgb.append(") (whereArgs=").append(StringUtils.join(selectionArgs, "|"));
            msgb.append(")");
            assertDatabaseRowCount(msgb.toString(), c, 1);
            Group group = GroupsTable.fromCursor(c);
            return group;
        } finally {
            DBUtil.close(c);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testGroupInsertObject() {
        Group group = new Group();
        group.setGroupId("test.group.object");
        group.setName("TestGroupObject");

        ContentValues values = GroupsTable.asValues(group);

        Uri uri = resolver.insert(GroupsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Group", uri);
    }

    @MediumTest
    public void testGroupInsertSimple() {
        ContentValues values = new ContentValues();
        values.put(GroupsTable.GROUPID, "test.group.simple");
        values.put(GroupsTable.NAME, "TestGroup");

        Uri uri = resolver.insert(GroupsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Group", uri);
    }

    @MediumTest
    public void testGroupInsertThenInsert() {
        String id = "test.group";

        ContentValues values = new ContentValues();
        values.put(GroupsTable.GROUPID, id);
        values.put(GroupsTable.NAME, "TestGroup");

        Uri uri = resolver.insert(GroupsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Groups", uri);

        // Now try to find it.
        String selection = GroupsTable.GROUPID + " = ?";
        String selectionArgs[] = new String[] { id };
        String msg = "Group (initial)";
        Group p1 = querySingleGroup(msg, GroupsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestGroup", p1.getName());

        // Insert another entry, hopefully overwriting the original due to a proper constraint setup.
        ContentValues second = new ContentValues();
        second.put(GroupsTable.GROUPID, id);
        second.put(GroupsTable.NAME, "Test Group Post"); // New name (with space)

        Uri uriSecond = resolver.insert(GroupsTable.CONTENT_URI, second);
        assertDatabaseUriHasDBId("Groups", uriSecond);

        // Try to find it again.
        msg = "Group (post insert)";
        Group p2 = querySingleGroup(msg, GroupsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Group Post", p2.getName());
    }

    @MediumTest
    public void testGroupInsertThenUpdate() {
        String id = "test.group";

        ContentValues values = new ContentValues();
        values.put(GroupsTable.GROUPID, id);
        values.put(GroupsTable.NAME, "TestGroup");

        Uri uri = resolver.insert(GroupsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Groups", uri);

        // Now try to find it.
        String selection = GroupsTable.GROUPID + " = ?";
        String selectionArgs[] = new String[] { id };
        String msg = "Group (initial)";
        Group p1 = querySingleGroup(msg, GroupsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "TestGroup", p1.getName());

        // Update entry
        ContentValues update = new ContentValues();
        update.put(GroupsTable.GROUPID, id);
        update.put(GroupsTable.NAME, "Test Group Post"); // New name (with space)

        String where = GroupsTable.GROUPID + " = ?";
        String whereArgs[] = new String[] { id };
        int count = resolver.update(GroupsTable.CONTENT_URI, update, where, whereArgs);
        assertEquals("Update count", 1, count);

        // Try to find it again.
        msg = "Group (post update)";
        Group p2 = querySingleGroup(msg, GroupsTable.CONTENT_URI, selection, selectionArgs);
        assertEquals(msg, "Test Group Post", p2.getName());
    }

    @MediumTest
    public void testGroupQueryByDBId() {
        String id = "test.group.query.bydbid";
        ContentValues values = new ContentValues();
        values.put(GroupsTable.GROUPID, id);
        values.put(GroupsTable.NAME, "TestGroup");

        Uri uri = resolver.insert(GroupsTable.CONTENT_URI, values);
        long dbId = getDatabaseId("Groups", uri);

        Uri singleUri = ContentUris.withAppendedId(GroupsTable.CONTENT_URI, dbId);

        String msg = "Query Group by DB Id";
        Group p1 = querySingleGroup(msg, singleUri, null, null);
        assertEquals(msg, id, p1.getGroupId());
        assertEquals(msg, "TestGroup", p1.getName());
    }

    @MediumTest
    public void testGroupQueryByGroupId() {
        String groupId = "test.group.query.bygroupid";
        ContentValues values = new ContentValues();
        values.put(GroupsTable.GROUPID, groupId);
        values.put(GroupsTable.NAME, "TestGroup");

        Uri uri = resolver.insert(GroupsTable.CONTENT_URI, values);
        assertDatabaseUriHasDBId("Groups", uri);

        Uri singleUri = Uri.withAppendedPath(GroupsTable.CONTENT_URI_BY_GROUPID, groupId);

        String msg = "Query Group by Group Id";
        Group p1 = querySingleGroup(msg, singleUri, null, null);
        assertEquals(msg, groupId, p1.getGroupId());
        assertEquals(msg, "TestGroup", p1.getName());
    }
}
