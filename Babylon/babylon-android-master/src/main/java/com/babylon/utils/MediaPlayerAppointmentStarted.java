package com.babylon.utils;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.PowerManager;

import com.babylon.App;
import com.babylon.R;

import java.io.IOException;

public class MediaPlayerAppointmentStarted {

    private static final String TAG = MediaPlayerAppointmentStarted.class.getSimpleName();
    private MediaPlayer mediaPlayer;

    public void play() {
        init();
        try {
            mediaPlayer.setDataSource(App.getInstance(), Uri.parse("android.resource://" + "com.babylon" + "/" + R.raw.ring_starting_video_call));

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.stop();
                }
            });

            mediaPlayer.setWakeMode(App.getInstance(), PowerManager.PARTIAL_WAKE_LOCK);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }

    private void init() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }

        mediaPlayer = new MediaPlayer();
    }

    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }
}
