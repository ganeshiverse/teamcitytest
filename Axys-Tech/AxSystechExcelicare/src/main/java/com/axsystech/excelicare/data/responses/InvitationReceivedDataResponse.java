package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class InvitationReceivedDataResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private DataObject dataObject;

    public DataObject getDataObject() {
        return dataObject;
    }

    public class DataObject implements Serializable {

        @SerializedName("Details")
        private ArrayList<InvitationReceivedDetails> dataDetailslist;

        public ArrayList<InvitationReceivedDetails> getDataDetailslist() {
            return dataDetailslist;
        }

        public String getLMD() {
            return LMD;
        }

        @SerializedName("LMD")
        private String LMD;
    }

}
