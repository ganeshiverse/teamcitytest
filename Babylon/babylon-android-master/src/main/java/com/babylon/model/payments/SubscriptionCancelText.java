package com.babylon.model.payments;

import com.google.gson.annotations.SerializedName;

public class SubscriptionCancelText {

    @SerializedName("cancel_title")
    private String Title;
    @SerializedName("cancel_text")
    private String Text;
    @SerializedName("cancel_cancel_button_text")
    private String CancelButtonText;
    @SerializedName("perform_cancel_button_text")
    private String PerformButtonText;

    public SubscriptionCancelText(String title, String message, String cancelButtonText, String performButtonText) {
        this.Title = title;
        this.Text = message;
        this.CancelButtonText = cancelButtonText;
        this.PerformButtonText = performButtonText;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getCancelButtonText() {
        return CancelButtonText;
    }

    public void setCancelButtonText(String cancelButtonText) {
        CancelButtonText = cancelButtonText;
    }

    public String getPerformButtonText() {
        return PerformButtonText;
    }

    public void setPerformButtonText(String performButtonText) {
        PerformButtonText = performButtonText;
    }
}
