package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.GetMediaItemResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 20/09/15.
 */
public class GetMediaItemLoader extends DataAsyncTaskLibLoader<GetMediaItemResponse>{

    private long mediaId;
    private String token;

    public GetMediaItemLoader(Context context, Long mediaId, String token) {
        super(context);
        this.mediaId = mediaId;
        this.token = token;
    }

    @Override
    protected GetMediaItemResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMediaItem(mediaId, token);
    }
}
