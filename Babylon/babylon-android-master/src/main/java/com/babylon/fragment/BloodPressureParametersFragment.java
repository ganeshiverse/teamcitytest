package com.babylon.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.MonitorMeParamsLoaderCallback;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.BaseModel;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.Patient;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.UIUtils;
import com.babylon.validator.ValidationController;
import com.babylon.validator.ValueBetweenValidator;
import com.babylon.view.CustomEditText;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Fragment {@link android.support.v4.app.Fragment} for viewing and changing user parameters
 */
public class BloodPressureParametersFragment extends BaseMonitorMeParamsLoaderFragment implements View.OnClickListener {

    private static final int SYSTOLIC_MAX_VALUE = 300;
    private static final int SYSTOLIC_MIN_VALUE = 70;
    private static final int DISTOLIC_MAX_VALUE = 200;
    private static final int DIASTOLIC_MIN_VALUE = 20;

    private Patient originalPatient;
    private Patient newPatient;

    private CustomEditText systolicBPEditText;
    private CustomEditText diastolicBPEditText;

    private DailyFeatureMap dailyFeatureMap;
    private List<MonitorMeParam> featureList;

    public static BloodPressureParametersFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        BloodPressureParametersFragment fragment = new BloodPressureParametersFragment();
        fragment.setArguments(b);
        return fragment;
    }

    private ValidationController validationController = new ValidationController();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        originalPatient = App.getInstance().getPatient();
        newPatient = (Patient) originalPatient.cloneObject();
        featureList = initMonitorMeParams();

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blood_pressure_parameters, container, false);
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(final long dateFrom) {
        final long endOfTheDay = DateUtils.getDayStartMills(dateInMillis, 1);
        MonitorMeParamsGet.newMonitorMeParamsGet(dateInMillis, endOfTheDay,
                MonitorMeParam.getParameterList(featureList)).execute(getActivity(),
                new RequestListener<ResponseList<MonitorMeFeatures>>() {
                    @Override
                    public void onRequestComplete(Response<ResponseList<MonitorMeFeatures>> response) {
                        if (getActivity() != null) {
                            MonitorMeParamSync.saveMonitorMeParams(getActivity().getContentResolver(), response,
                                    dateInMillis, endOfTheDay);
                            startLoaderManager();
                            onLoadedMonitorMeParamsFromServer();
                            setProgressBarVisible(false);
                        }
                    }
                }
        );
    }

    @Override
    protected void startLoaderManager() {

        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_USER_PARAMS,
                MonitorMeParamsLoaderCallback.createBundle(dateInMillis),
                new ParamsLoader());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.weight_parameters_input_data);

        if (getArguments() != null) {
            dateInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
        }

        String dateString = new MonitorMeDateViewController().getFormattedDay(new Date(dateInMillis));
        ((TextView) view.findViewById(R.id.dateTextView)).setText(dateString);

        view.findViewById(R.id.acceptButton).setOnClickListener(this);

        systolicBPEditText = (CustomEditText) getView().findViewById(R.id.edit_text_systolic_bp);
        diastolicBPEditText = (CustomEditText) getView().findViewById(R.id.edit_text_diastolic_bp);

        initValidation(systolicBPEditText, diastolicBPEditText, validationController, getActivity());
    }

    public static void initValidation(CustomEditText systolicBPEditText, CustomEditText diastolicBPEditText,
                                      ValidationController validationController, Activity activity) {
        systolicBPEditText.setEmptyValidationErrorText(activity.getString(R.string.systolic_bp_validation));
        diastolicBPEditText.setEmptyValidationErrorText(activity.getString(R.string.diastolic_bp_validation));

        systolicBPEditText.validationAfterTextChanged(validationController);
        diastolicBPEditText.validationAfterTextChanged(validationController);


        ValueBetweenValidator systolicBPValueValidator = new ValueBetweenValidator(null,
                activity.getString(R.string.validator_field_value_between, SYSTOLIC_MIN_VALUE,
                        SYSTOLIC_MAX_VALUE), SYSTOLIC_MIN_VALUE, SYSTOLIC_MAX_VALUE
        );

        ValueBetweenValidator diastolicBPValueValidator = new ValueBetweenValidator(null,
                activity.getString(R.string.validator_field_value_between, DIASTOLIC_MIN_VALUE,
                        DISTOLIC_MAX_VALUE),
                DIASTOLIC_MIN_VALUE, DISTOLIC_MAX_VALUE
        );

        validationController.addValidation(systolicBPEditText, systolicBPValueValidator);
        validationController.addValidation(diastolicBPEditText, diastolicBPValueValidator);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptButton:
                saveNewData();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isBackAllowed() {
        UIUtils.hideSoftKeyboard(getActivity(), systolicBPEditText.getEditText());
        return true;
    }

    public void saveNewData() {
        if (validationController.performValidation()) {
            if (wasDataUpdated()) {
                putMonitorMeParamNotZero(MonitorMeParam.SYSTOLIC_BP_ID, systolicBPEditText.getEditText(), dailyFeatureMap);
                putMonitorMeParamNotZero(MonitorMeParam.DIASTOLIC_BP_ID, diastolicBPEditText.getEditText(), dailyFeatureMap);

                DatabaseOperationUtils.getInstance().addMonitorMeFeatures(dailyFeatureMap.getFeatureMap(),
                        getActivity().getContentResolver());
                SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);
            }

            super.onTopBarButtonPressed();
        }
    }

    /**
     * Determine if patient personal data was updated
     *
     * @return true if patient personal data was updated
     * false if patient personal data wasn't updated
     */
    private boolean wasPatientUpdated() {
        boolean gender = TextUtils.equals(newPatient.getGender(), originalPatient.getGender());
        boolean smoker = TextUtils.equals(newPatient.getSmokingStatus(), originalPatient.getSmokingStatus());
        boolean birthday = newPatient.getBirthdayTime() == originalPatient.getBirthdayTime();

        boolean wasPatientUpdated = !(gender && birthday && smoker);

        return wasPatientUpdated;
    }

    /**
     * Define if patient personal data or daily report was updated
     *
     * @return true if patient personal data or daily report was updated
     * false if patient personal data and daily report wasn't updated
     */
    private boolean wasDataUpdated() {
        //Patient data
        boolean wasPatientUpdated = wasPatientUpdated();

        boolean systolicBP = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.SYSTOLIC_BP_ID))
                .compareTo(UIUtils.getFloat(systolicBPEditText)) == 0;
        boolean diastolicBP = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.DIASTOLIC_BP_ID))
                .compareTo(UIUtils.getFloat(diastolicBPEditText)) == 0;

        return wasPatientUpdated || !(systolicBP && diastolicBP);
    }

    /**
     * Convert input number in {@param editText} in float format
     *
     * @param editText view {@link android.view.View} for input
     * @return input number in float format
     */
    private float getFloatValue(EditText editText) {
        return BaseModel.getFloat(editText.getText().toString());
    }

    private class ParamsLoader extends MonitorMeParamsLoaderCallback {

        @Override
        public Context getContext() {
            return getActivity();
        }

        @Override
        public List<MonitorMeParam> getMonitorMeParamList() {
            return featureList;
        }

        @Override
        public void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> featureMap) {
            dailyFeatureMap = new DailyFeatureMap(featureMap);

            if (!featureMap.isEmpty()) {
                showNotZeroValue(systolicBPEditText.getEditText(), MonitorMeParam.SYSTOLIC_BP_ID, dailyFeatureMap);
                showNotZeroValue(diastolicBPEditText.getEditText(), MonitorMeParam.DIASTOLIC_BP_ID, dailyFeatureMap);
            }
        }
    }

    private List<MonitorMeParam> initMonitorMeParams() {
        List<MonitorMeParam> featureList =
                new LinkedList<MonitorMeParam>();
        featureList.add(MonitorMeParam.SYSTOLIC_BP_ID);
        featureList.add(MonitorMeParam.DIASTOLIC_BP_ID);
        return featureList;
    }


}