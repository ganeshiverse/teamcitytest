package com.babylon.fragment.payments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.OrderDetailsArrayAdapter;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Patient;
import com.babylon.model.payments.OrderDetail;
import com.babylon.model.payments.PaymentCard;
import com.babylon.utils.PreferencesManager;
import com.babylon.view.PaymentCardView;

import java.util.List;

public class OrderDetailsListFragment extends BaseFragment implements PaymentCardView.OnPaymentCardClickListener {

    public static final String TAG = OrderDetailsListFragment.class.getSimpleName();
    public static final String SUBSCRIPTION_TAG="subscription";
    public static final String APPOINTMENT_TAG="appointment";
    private OnPayClickListener onPayClickListener;
    private OnCardListRequestClickLister onCardListRequestClickLister;
    private DataProtocol dataProtocol;
    private boolean  payClicked=false;
    private OrderAddressFragmentListener listener;

    private OrderDetailsArrayAdapter adapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            onPayClickListener = (OnPayClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPayClickListener");
        }

        try {
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }

        try {
            onCardListRequestClickLister = (OnCardListRequestClickLister) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PaymentCardView.OnPaymentCardClickListener");
        }

        try {
            listener = (OrderAddressFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(R.string.order_details_title);

        initRightImageButton();

        final List<OrderDetail> orderDetails = dataProtocol.getOrderDetails();


        if (orderDetails != null) {
            adapter = new OrderDetailsArrayAdapter(getActivity(), orderDetails);

            final ListView listView = (ListView) view.findViewById(android.R.id.list);
            showFooter(listView);
            listView.setAdapter(adapter);
        }
    }

    private void initRightImageButton() {
        setRightButton(onPayMenuClickListener, R.string.order_details_pay);
        boolean isPaymentCardCreated = dataProtocol.getPaymentCard().isCreated();
        if(!TextUtils.isEmpty(dataProtocol.getOrderTotal())){
            if(TextUtils.isEmpty(PreferencesManager.getInstance().getPatientAddress())){
                isPaymentCardCreated=false;
            }
        }
        setRightButtonEnabled(isPaymentCardCreated, isPaymentCardCreated ? ORANGE_THEME : R.color
                .subscription_menu_font_disable);

    }

    private void showFooter(ListView listView) {
        final View footerView = getActivity().getLayoutInflater().inflate(R.layout.list_footer_order_details, null,
                false);
        showSubTitle(footerView);

        final PaymentCardView paymentCardView = (PaymentCardView) footerView.findViewById(R.id
                .payment_card_view);
        paymentCardView.showPaymentCard(new PaymentCardViewController(), dataProtocol.getPaymentCard(), this);
        paymentCardView.setClickable(true);

        if(!TextUtils.isEmpty(dataProtocol.getOrderTotal())){
            TextView tvTotal=(TextView)footerView.findViewById(R.id.tvTotal);
            TextView tvAddress=(TextView)footerView.findViewById(R.id.tvAddress);
            ImageView imgLocation=(ImageView)footerView.findViewById(R.id.location_image_view);

            final TextView subTitleTextView = (TextView) footerView.findViewById(R.id
                    .order_details_sub_title_text_view);
            subTitleTextView.setVisibility(View.GONE);
              if(!TextUtils.isEmpty(PreferencesManager.getInstance().getPatientAddress())){
                tvAddress.setText(PreferencesManager.getInstance().getPatientAddress());

            }

            tvTotal.setVisibility(View.VISIBLE);

            tvTotal.setText(getString(R.string.total_price)+" "+ App.getInstance().getCurrentRegion()
                    .getFormattedPriceString
                    (Double.valueOf(dataProtocol.getOrderTotal())));


            final RelativeLayout addressLayout = (RelativeLayout) footerView.findViewById(R.id
                    .layout_address);
            addressLayout.setVisibility(View.VISIBLE);
            addressLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (listener != null) {

                        listener.onAddressClicked();

                    }

                }
            });

        }

        listView.addFooterView(footerView, null, false);
    }

    private void showSubTitle(View footerView) {
        final String subTitle = dataProtocol.getOrderDetailsSubTitle();

        if (!TextUtils.isEmpty(subTitle)) {
            final TextView subTitleTextView = (TextView) footerView.findViewById(R.id
                    .order_details_sub_title_text_view);
            subTitleTextView.setVisibility(View.VISIBLE);
            subTitleTextView.setText(subTitle);
        }
        else if(dataProtocol.getOrderDetails().get(0).getName().compareToIgnoreCase(SUBSCRIPTION_TAG)==0)
        {
            final TextView subTitleTextView = (TextView) footerView.findViewById(R.id
                    .order_details_sub_title_text_view);
            subTitleTextView.setVisibility(View.VISIBLE);
            subTitleTextView.setText(getString(R.string.subscription_dialog_title));
            if(dataProtocol.IsSpecialist()){
                subTitleTextView.setVisibility(View.GONE);
            }
        }
        else if(!(dataProtocol.getPatient().getSubscription().getPlan().isDefault()))
        {
            final TextView subTitleTextView = (TextView) footerView.findViewById(R.id
                    .order_details_sub_title_text_view);
            subTitleTextView.setVisibility(View.VISIBLE);
            subTitleTextView.setText(getString(R.string.subscription_dialog_title));
            if(dataProtocol.IsSpecialist()){
                subTitleTextView.setVisibility(View.GONE);
            }
        }



    }

    @Override
    public void onDetach() {
        super.onDetach();

        onPayClickListener = null;
        onCardListRequestClickLister = null;
    }

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    private final View.OnClickListener onPayMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           /* if(payClicked==false) {
                payClicked = true;*/
                onPayClickListener.onPayClick();
           // }
        }
    };

    @Override
    public void onNewPaymentCardClick() {
        onCardListRequestClickLister.onCardListRequestClickLister();
    }

    @Override
    public void onPaymentCardClick(PaymentCard paymentCard) {
        onCardListRequestClickLister.onCardListRequestClickLister();
    }

    public interface OnPayClickListener {
        /**
         * Method calls onFinish of payment process,
         * in which we need pay for different type of product
         */

        public void onPayClick();
    }

    public interface DataProtocol {
        public PaymentCard getPaymentCard();

        public List<com.babylon.model.payments.OrderDetail> getOrderDetails();

        public String getOrderDetailsSubTitle();
        public String getOrderTotal();


        public Patient getPatient();
        public boolean IsSpecialist();

    }

    public interface OnCardListRequestClickLister {
        public void onCardListRequestClickLister();
    }

    public interface OrderAddressFragmentListener {

        public void onAddressClicked();
    }
}
