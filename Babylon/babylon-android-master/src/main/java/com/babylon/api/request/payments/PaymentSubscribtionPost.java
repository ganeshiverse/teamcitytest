package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.payments.Subscription;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.annotations.SerializedName;

import org.apache.http.message.BasicHeader;

public class PaymentSubscribtionPost extends AbsRequestPost<PaymentSubscribtionPost.PlanParams, Subscription> {

    public static class PlanParams {
        @SerializedName("plan_id")
        public Integer planId;
        @SerializedName("patient_id")
        public String patientId;
        public PlanParams(Integer planId, String patientId) {
            this.planId = planId;
            this.patientId = patientId;
        }
    }
    
    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.SUBSCRIBE_POST);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<Subscription> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }
}
