package com.iversecomics.client.downloads.internal.data;

import java.io.File;
import java.net.URI;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.downloads.internal.DownloaderReceiver;

public class DownloadData implements Parcelable {

    /**
     * The Database ID
     */
    private long                                         id     = -1;

    /**
     * The Name of the Download
     */
    private String                                       name;

    /**
     * The Timestamp this entry was created
     */
    private long                                         timestampCreated;

    /**
     * The latest Download URI.
     * <p>
     * Can change as a result of time, redirects, etc..
     */
    private Uri                                          uri;
    /**
     * The Download Status
     */
    private DownloadStatus                               status = DownloadStatus.PENDING;
    private String                                       statusMessage;
    private File                                         localFile;
    private long                                         contentProgress;

    private long                                         contentLength;

    /**
     * The HTTP ETag used for resumable downloads
     */
    private String                                       etag;

    /**
     * The number of failed attempts on this download (so far)
     */
    private int                                          numFailed;

    /**
     * When a retry is needed, this is the timestamp when to try the retry after.
     */
    private long                                         timestampRetryAfter;

    /**
     * timestamp for tracking updates in the DownloaderService
     */
    private long                                         timestampUpdated;

    /**
     * An optional reference contentUri that the calling program can provide and will be returned to the
     * {@link BroadcastReceiver} listening for the DOWNLOAD_COMPLETE action.
     */
    private Uri                                          referenceContentUri;

    private String payload;

    private String coverImageFileName;

    public static final Parcelable.Creator<DownloadData> CREATOR;

    static {
        CREATOR = new Creator<DownloadData>() {
            @Override
            public DownloadData createFromParcel(Parcel source) {
                return new DownloadData(source);
            }

            @Override
            public DownloadData[] newArray(int size) {
                return new DownloadData[size];
            }
        };
    }


    public static DownloadData parseIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        
        String type = extras.getString("download.type");
        if (TextUtils.isEmpty(type)) {
            type = "simple";
        }

        // Is defined as simple intent extras.
        if ("simple".equals(type)) {
            DownloadData data = new DownloadData();
            data.setName(extras.getString("download.name"));
            int statusCode = extras.getInt("download.status", DownloadStatus.PENDING.getCode());
            data.setStatus(statusCode);
            data.setTimestampRetryAfter(extras.getLong("download.timestampRetryAfter"));
            String rawstr = extras.getString("download.referenceContentUri");
            if (!TextUtils.isEmpty(rawstr)) {
                data.setReferenceContentUri(Uri.parse(rawstr));
            }
            rawstr = extras.getString("download.uri");
            if (!TextUtils.isEmpty(rawstr)) {
                data.setUri(Uri.parse(rawstr));
            }
            rawstr = extras.getString("download.localFile");
            if (!TextUtils.isEmpty(rawstr)) {
                data.setLocalFile(new File(rawstr));
            }
            return data;
        }

        if ("parcel".equals(type)) {
            DownloadData data = extras.getParcelable("download");
            return data;
        }

        return null;
    }

    public DownloadData() {
        /* default constructor */
        this.timestampCreated = System.currentTimeMillis();
    }

    private DownloadData(Parcel in) {
        /* Load from parcel in same order as written */
        this.id = in.readLong();
        this.name = in.readString();
        this.timestampCreated = in.readLong();
        this.uri = in.readParcelable(null);
        int statusCode = in.readInt();
        this.status = new DownloadStatus(statusCode);
        this.statusMessage = in.readString();
        String filename = in.readString();
        if (!TextUtils.isEmpty(filename)) {
            this.localFile = new File(filename);
        } else {
            this.localFile = null;
        }
        this.contentLength = in.readLong();
        this.contentProgress = in.readLong();
        this.etag = in.readString();
        this.numFailed = in.readInt();
        this.timestampRetryAfter = in.readLong();
        this.timestampUpdated = in.readLong();
        String refuri = in.readString();
        if (!TextUtils.isEmpty(refuri)) {
            this.referenceContentUri = Uri.parse(refuri);
        } else {
            this.referenceContentUri = null;
        }
        
        final String jsonPayload = in.readString();
        if (!TextUtils.isEmpty(jsonPayload)) {
        	payload = jsonPayload;
        }
        else {
        	payload = null;
        }

        String coverFileName = in.readString();
        if (!TextUtils.isEmpty(coverFileName))
            this.coverImageFileName = coverFileName;
    }

    @Override
    public int describeContents() {
        return 0; /* default for android.os.Parcelable */
    }

    public long getContentLength() {
        return contentLength;
    }

    public long getContentProgress() {
        return contentProgress;
    }

    public String getEtag() {
        return etag;
    }

    public long getId() {
        return id;
    }

    public File getLocalFile() {
        return localFile;
    }

    public String getName() {
        return name;
    }

    public int getNumFailed() {
        return numFailed;
    }

    /**
     * @return 0-100
     */
    public int getPercentageComplete() {
        if (contentLength <= 0 || contentProgress <= 0)
            return 0;

        return (int)(contentProgress * 100 / contentLength);
    }

    public Uri getReferenceContentUri() {
        return referenceContentUri;
    }

    public DownloadStatus getStatus() {
        return status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public long getTimestampCreated() {
        return timestampCreated;
    }

    public long getTimestampRetryAfter() {
        return timestampRetryAfter;
    }

    public long getTimestampUpdated() {
        return timestampUpdated;
    }

    public Uri getUri() {
        return uri;
    }

    public boolean hasContentLength() {
        return (this.contentLength > 0);
    }

    public boolean hasEtag() {
        return !TextUtils.isEmpty(etag);
    }

    public void setContentLength(long size) {
        this.contentLength = size;
    }

    public void setContentProgress(long progress) {
        this.contentProgress = progress;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * Populate an Intent's Bundle/Extra information with the download details.
     * <p>
     * This is used by {@link DownloaderManager} and {@link DownloaderReceiver} to enqueue a potential/pending download.
     * 
     * @param intent
     *            the intent to populate
     */
    public void setIntent(Intent intent) {
        intent.setData(this.uri);
        intent.putExtra("download.type", "simple"); // Parse Flag
        intent.putExtra("download.name", this.name);
        intent.putExtra("download.status", this.status.getCode());
        intent.putExtra("download.timestampRetryAfter", this.timestampRetryAfter);
        if (this.referenceContentUri != null) {
            intent.putExtra("download.referenceContentUri", this.referenceContentUri.toString());
        } else {
            intent.putExtra("download.referenceContentUri", "");
        }
        intent.putExtra("download.uri", this.uri.toString());
        if (this.localFile != null) {
            intent.putExtra("download.localFile", this.localFile.getAbsolutePath());
        } else {
            intent.putExtra("download.localFile", "");
        }
    }

    public void setLocalFile(File localFile) {
        this.localFile = localFile;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumFailed(int numFailed) {
        this.numFailed = numFailed;
    }

    public void setReferenceContentUri(Uri referenceContentUri) {
        this.referenceContentUri = referenceContentUri;
    }

    public void setStatus(DownloadStatus status) {
        this.status = status;
    }

    public void setStatus(int statusCode) {
        this.status = new DownloadStatus(statusCode);
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public void setTimestampCreated(long timestampCreated) {
        this.timestampCreated = timestampCreated;
    }

    public void setTimestampRetryAfter(long timestampRetryAfter) {
        this.timestampRetryAfter = timestampRetryAfter;
    }

    public void setTimestampUpdated(long timestampUpdated) {
        this.timestampUpdated = timestampUpdated;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public void setUri(URI uri) {
        this.uri = Uri.parse(uri.toASCIIString());
    }

    public String getVerifyPurchasePayload()
    {
        return payload;
    }

    public void setVerifyPurchasePayload(final String payload)
    {
        this.payload = payload;
    }

    public String getCoverImageFileName() {
        return coverImageFileName;
    }

    public void setCoverImageFileName(String fileName) {
        this.coverImageFileName = fileName;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("DownloadData[");
        buf.append(uri);
        buf.append(",status=").append(status);
        if (!TextUtils.isEmpty(statusMessage)) {
            buf.append(",statusMessage=").append(statusMessage);
        }
        if (status.isActive()) {
            buf.append(",currentBytes=").append(this.contentProgress);
            buf.append(",totalBytes=").append(this.contentLength);
        }
        buf.append("]");
        return buf.toString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(this.id);
        out.writeString(this.name);
        out.writeLong(this.timestampCreated);
        out.writeParcelable(this.uri, flags);
        out.writeInt(this.status.getCode());
        out.writeString(this.statusMessage);
        if (this.localFile == null) {
            out.writeString("");
        } else {
            out.writeString(this.localFile.getAbsolutePath());
        }
        out.writeLong(this.contentLength);
        out.writeLong(this.contentProgress);
        out.writeString(this.etag);
        out.writeInt(this.numFailed);
        out.writeLong(this.timestampRetryAfter);
        out.writeLong(this.timestampUpdated);
        if (this.referenceContentUri == null) {
            out.writeString("");
        } else {
            out.writeString(this.referenceContentUri.toString());
        }

        if (payload == null) {
        	out.writeString("");
        }
        else {
        	out.writeString(payload);
        }

        if (coverImageFileName ==  null)
            out.writeString("");
        else
            out.writeString(coverImageFileName);
    }

}
