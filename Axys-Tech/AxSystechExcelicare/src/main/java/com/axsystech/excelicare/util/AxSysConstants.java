package com.axsystech.excelicare.util;

import android.os.Environment;

import com.axsystech.excelicare.app.AxSysTechApplication;

import java.io.File;

/**
 * Created by someswar on 7/1/2015.
 */
public class AxSysConstants {

    public static final String APP_DIRECTORY_PATH = "com.axsystech.excelicare.util.AxSysConstants.APP_DIRECTORY_PATH";
    public static final String BASE_APP_DIRECTORY = File.separator + ".nomedia" + File.separator + "ExcelicareAppDir" + File.separator;
    public static final double MANDATORY_SPACE_NEEDED = 200.0; //MB

    // Left menu navigation ID's
    public static final String MENU_ID_DASHBOARD = "1357";
    public static final String MENU_ID_MEDIA_ITEMS = "116";
    public static final String MENU_ID_CIRCLES = "1362";
    public static final String CIRCLE_INVITATIONS_SENT = "1366";
    public static final String CIRCLE_INVITATIONS_RECIEVED = "1365";
    public static final String MENU_ID_MESSAGES = "152";
    public static final String MENU_ID_MESSAGE_INBOX = "1359";
    public static final String MENU_ID_MESSAGE_SENT = "1360";
    public static final String MENU_ID_APPOINTMENTS_LIST = "54";
    public static final String MENU_ID_BOOK_APPOINTMENTS = "1361";
    public static final String MENU_ID_MESSAGE_TO_CIRCLE = "1358";
    public static final String MENU_ID_MESSAGE_TO_PROVIDER = "1354";
    public static final String MENU_ID_USERPREFERENCES = "171";
    public static final String MENU_ID_REMOVE_DEVICES = "1379";
    public static final String MENU_ID_LOGOUT = "1032";
    public static int MSG_MAIL_ID = -1;
    public static final String AADHAR_NUMBER_ID = "3133";
    public static final String DRIVING_LICENCE_NUMBER_ID = "15313";
    public static final String CITY_LIST_ID = "7000019";
    public static final String CITY_LIST_TOKEN = "AXSYS";
    public static final String STATES_LOOKUP_ID = "2574";
    public static final String COUNTRY_LOOKUP_ID = "104";

    public static final String DEFAULT_SITE_ID = "101";

    public static final String INVALID_SERVER_RESPONSE = "Invalid response received from server, please try again";

    public static final String GET_MEDIA_ITEM_LOADER = "com.axsystech.excelicare.util.AxSysConstants.GET_MEDIA_ITEM_LOADER";

    // Intent extra keys
    public static final String EXTRA_VERIFY_TOKEN = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_VERIFY_TOKEN";
    public static final String LOGOUT_INTENT_FILTER = "com.axsystech.excelicare.framework.ui.activities.LOGOUT_INTENT_FILTER";
    public static final String EXTRA_SELECTED_CITY = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CITY";
    public static final String EXTRA_SELECTED_STATE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_STATE";
    public static final String EXTRA_SELECTED_COUNTRY = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_COUNTRY";
    public static final String EXTRA_NAVIGATE_TO = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_NAVIGATE_TO";
    public static final String EXTRA_INPUT_EMAIL = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_INPUT_EMAIL";
    public static final String EXTRA_SELECTED_SITE_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_SITE_ID";
    public static final String EXTRA_SHOULD_ADD_NEW_PROXIE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SHOULD_ADD_NEW_PROXIE";
    public static final String EXTRA_SHOULD_ENABLE_DATETIME_PICKER = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SHOULD_DISABLE_DATETIME_PICKER";
    public static final String EXTRA_JUST_DISPLAY_PRIVACY_POLICY = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_JUST_DISPLAY_PRIVACY_POLICY";
    public static final String EXTRA_SHOULD_ENABLE_LEFT_NAV = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV";
    // when user navigating to MediaFragment from MessageCompose screen, this would be false, otherwise we would send true
    public static final String EXTRA_SHOULD_DISPLAY_LOCAL_MEDIA = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SHOULD_DISPLAY_LOCAL_MEDIA";
    public static final String EXTRA_SEARCH_LOCATION_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SEARCH_LOCATION_ID";
    public static final String EXTRA_SEARCH_SPECIALITY_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SEARCH_SPECIALITY_ID";
    public static final String EXTRA_CLINIC_TITLE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_CLINIC_TITLE";
    public static final String EXTRA_ACTIONBAR_TITLE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_ACTIONBAR_TITLE";
    public static final String EXTRA_SUMMARY_SECTIONS_LIST = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SUMMARY_SECTIONS_LIST";
    public static final String EXTRA_CIRCLE_MEMBER_TARGET = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_CIRCLE_MEMBER_TARGET";
    public static final String EXTRA_MEDIA_FILE_PATH = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_MEDIA_FILE_PATH";
    public static final String EXTRA_SELECTED_MEDIA_TYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE";
    public static final String EXTRA_SELECTED_MENU_DASHBOARD_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MENU_DASHBOARD_ID";
    public static final String EXTRA_SELECTED_MENU_MODULE_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MENU_MODULE_ID";
    public static final String EXTRA_SELECTED_CARD_RECORD_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CARD_RECORD_ID";
    public static final String EXTRA_SELECTED_MEDIA_OBJ = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEDIA_OBJ";
    public static final String EXTRA_MEDIA_DETAILS_VIEWTYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_MEDIA_DETAILS_VIEWTYPE";
    public static final String EXTRA_MESSAGE_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_MESSAGE_ID";
    public static final String EXTRA_MESSAGE_PHOTO_URL = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_MESSAGE_PHOTO_URL";
    public static final String EXTRA_SELECTED_CIRCLE_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CIRCLE_OBJECT";
    public static final String EXTRA_SELECTED_MEMBER_MAILID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID";
    public static final String EXTRA_SELECTED_CIRCLEID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CIRCLEID";
    public static final String EXTRA_SELECTED_CIRCLENAME = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CIRCLENAME";
    public static final String EXTRA_SELECTED_CIRCLETYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CIRCLETYPE";
    public static final String EXTRA_SELECTED_MEMBER_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_ID";
    public static final String EXTRA_SELECTED_MEMBER_USER_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_USER_ID";
    public static final String EXTRA_SELECTED_MEMBER_MAIL = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_MAIL";
    public static final String EXTRA_SELECTED_MEMBER_NAME = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_NAME";
    public static final String EXTRA_SELECTED_MEMBER_ROLE_TYPE_SLU = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_ROLE_TYPE_SLU";
    public static final String EXTRA_SELECTED_MEMBER_END_REASON_COMMENT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_END_REASON_COMMENT";
    public static final String EXTRA_CIRCLES_INVITE_TARGET = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_CIRCLES_INVITE_TARGET";
    public static final String EXTRA_SELECTED_MEMBER_DETAIL_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_MEMBER_DETAIL_OBJECT";
    public static final String EXTRA_SELECTED_CIRCLE_EDIT_DETAIL_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CIRCLE_EDIT_DETAIL_OBJECT";
    public static final String EXTRA_MEMBERS_IN_SELECTED_CIRCLE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_MEMBERS_IN_SELECTED_CIRCLE";
    public static final String EXTRA_SPECIALITY_LOOKUPS_LIST = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SPECIALITY_LOOKUPS_LIST";
    public static final String EXTRA_LOCATION_LOOKUPS_LIST = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_LOCATION_LOOKUPS_LIST";
    public static final String EXTRA_DOCTORS_SEARCH_RESULTS_LIST = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_DOCTORS_SEARCH_RESULTS_LIST";
    public static final String EXTRA_CLINICIAN_DETAILS = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_CLINICIAN_DETAILS";
    public static final String EXTRA_SELECTED_CLINICIAN_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT";
    public static final String EXTRA_SELECTED_CLINIC_POSITION = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION";
    public static final String EXTRA_APPOINTMENT_REVIEW_TYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_APPOINTMENT_REVIEW_TYPE";
    public static final String EXTRA_BOOK_APPOINTMENT_DATETIME = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_APPOINTMENT_DATETIME";
    public static final String EXTRA_BOOK_APPOINTMENT_TIMESLOT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_APPOINTMENT_TIMESLOT";
    public static final String EXTRA_BOOK_APPOINTMENT_CONS_MODE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_APPOINTMENT_CONS_MODE";
    public static final String EXTRA_APPOINTMENT_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_APPOINTMENT_ID";
    public static final String EXTRA_APPOINTMENT_TYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_APPOINTMENT_TYPE";
    public static final String EXTRA_PROVIDER_SOURCE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_PROVIDER_SOURCE";
    public static final String EXTRA_COMPOSE_MSG_TO = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_COMPOSE_MSG_TO";
    public static final String EXTRA_COMPOSE_MSG_DETAILS = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_COMPOSE_MSG_DETAILS";
    public static final String EXTRA_COMPOSE_MSG_TYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_COMPOSE_MSG_TYPE";

    public static final String EXTRA_SELECTED_CIRCLE_SYSTEMCIRCLE = "SystemCircle";
    public static final String EXTRA_SELECTED_CIRCLE_CARECIRCLE = "CareCircle";

    public static final String EXTRA_UPLOAD_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_UPLOAD_MEDIA_OBJECT";
    public static final String EXTRA_SAVED_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_SAVED_MEDIA_OBJECT";
//    public static final String EXTRA_DELETED_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_DELETED_MEDIA_OBJECT";

    public static final String SERVICE_MEDIA_RECEIVER = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_MEDIA_RECEIVER";

    public static final String SERVICE_UPLOAD_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_UPLOAD_MEDIA_OBJECT";
    public static final String SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT";
    public static final String SERVICE_UPLOADED_MEDIA_OBJECT_RESPONSE = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_UPLOADED_MEDIA_OBJECT_RESPONSE";
    public static final String SERVICE_ERROR_UPLOADING_MEDIA = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_ERROR_UPLOADING_MEDIA";

    public static final String SERVICE_DELETE_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_DELETE_MEDIA_OBJECT";
    public static final String SERVICE_CURRENT_DELETING_MEDIA_OBJECT = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_CURRENT_DELETING_MEDIA_OBJECT";
    public static final String SERVICE_DELETED_MEDIA_OBJECT_RESPONSE = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_DELETED_MEDIA_OBJECT_RESPONSE";
    public static final String SERVICE_ERROR_DELETING_MEDIA = "com.axsystech.excelicare.util.AxSysConstants.SERVICE_ERROR_DELETING_MEDIA";

    //Invitation Details
    public static final String EXTRA_BOOK_INVITATION_MEMBER_NAME = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_INVITATION_MEMBER_NAME";
    public static final String EXTRA_BOOK_INVITATION_DATE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_INVITATION_DATE";
    public static final String EXTRA_BOOK_INVITATION_TEXT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_INVITATION_TEXT";
    public static final String EXTRA_BOOK_INVITATION_ID = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_INVITATION_ID";
    public static final String EXTRA_BOOK_INVITATION_SENT = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_INVITATION_SENT";
    public static final String EXTRA_BOOK_INVITATION_RECEIVED = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_INVITATION_RECEIVED";
    public static final String EXTRA_BOOK_MESSAGE_STATUS = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_BOOK_MESSAGE_STATUS";

//    public static final String BCR_MEDIA_DELETED = "com.axsystech.excelicare.util.AxSysConstants.BCR_MEDIA_DELETED";

    // Circles
    public static final String INVITE_CIRCLE_MEMBER = "INVITE_CIRCLE_MEMBER";
    public static final String INVITE_CIRCLE_DOCTOR = "INVITE_CIRCLE_DOCTOR";

    // Messages
    public static final String TO_RECIPIENT_TYPE_LU = "15319";
    public static final String CC_RECIPIENT_TYPE_LU = "15320";

    public static final String HIGH_PRIORITY_ID = "5311";
    public static final String MEDIUM_PRIORITY_ID = "5313";
    public static final String LOW_PRIORITY_ID = "5312";

    public static final String SUMMARY_SECTIONS_PANEL_ID = "7000026";
    public static final String SUMMARY_ATTACHMENT_TYPE_LU = "1054";
    public static final String MEDIA_AUDIO_ATTACHMENT_ID = "157";
    public static final String MEDIA_IMAGES_ATTACHMENT_ID = "153";
    public static final String MEDIA_VIDEO_ATTACHMENT_ID = "156";

    public static final String EXTRA_ATTACHMENT_TYPE = "com.axsystech.excelicare.util.AxSysConstants.EXTRA_ATTACHMENT_TYPE";
    public static final String ATTACHMENT_TYPE_SUMMARY = "ATTACHMENT_TYPE_SUMMARY";
    public static final String ATTACHMENT_TYPE_MEDIA_IMAGE = "ATTACHMENT_TYPE_MEDIA_IMAGE";
    public static final String ATTACHMENT_TYPE_MEDIA_AUDIO = "ATTACHMENT_TYPE_MEDIA_AUDIO";
    public static final String ATTACHMENT_TYPE_MEDIA_VIDEO = "ATTACHMENT_TYPE_MEDIA_VIDEO";

    // Compose message to - types
    public static final String COMPOSE_MSG_TO_ALL = "MSG_TO_ALL";
    public static final String COMPOSE_MSG_TO_CIRCLE = "MSG_TO_CIRCLE";
    public static final String COMPOSE_MSG_TO_PROVIDERS = "MSG_TO_PROVIDERS";

    public static final String COMPOSE_MSG_TYPE_REPLY = "COMPOSE_MSG_TYPE_REPLY";
    public static final String COMPOSE_MSG_TYPE_REPLY_ALL = "COMPOSE_MSG_TYPE_REPLY_ALL";
    public static final String COMPOSE_MSG_TYPE_FORWARD = "COMPOSE_MSG_TYPE_FORWARD";

    // Book Appointment review types
    public static final String BOOK_APPOINTMENT_REVIEW_TYPE_BOOK = "BOOK";

    public static final String PROVIDER_SOURCE_COMPOSE = "COMPOSE";
    public static final String PROVIDER_SOURCE_APPOINTMENTS = "APPOINTMENTS";


    // System preference constant ID's
    public static final String REG_IDENFICATION1_PATTERN_ID = /*"1160"*/ "30";
    public static final String REG_IDENFICATION2_PATTERN_ID = /*"1161"*/ "41";
    public static final String REG_UPLOAD_SIZE_ID = "2118";
    public static final String REG_SIGNUP_TYPE_ID = "2187";
    public static final String REG_TOGGLE_SEQURITY_QUESTIONS_ID = "2188";
    public static final String REG_TOGGLE_PRIVACY_POLICY_ID = "2189";

    public static final String APP_DATE_ENTRY_FORMAT_ID = "2195";
    public static final String APP_TIME_ENTRY_FORMAT_ID = "2196";
    public static final String APP_MEDIA_THUMBNAIL_SIZE_ID = "2198";
    public static final String APP_INVITATION_MSG_ID = "2199";
    public static final String APP_DATE_TIME_ENTRY_FORMAT_ID = "2200";
    public static final String APP_DATE_DISPLAY_FORMAT_ID = "2201";

    // SignUp input field validator ID's
    public static final String REG_TOGGLE_MOBILE_NO_ID = "2213";
    public static final String REG_IS_MOBILE_NO_MANDATORY = "2217";

    public static final String REG_TOGGLE_PRIMARY_IDENTIFIER_ID = "2214";
    public static final String REG_IS_PRIMARY_IDENTIFIER_MANDATORY = "2218";

    public static final String REG_TOGGLE_SECONDARY_IDENTIFIER_ID = "2215";
    public static final String REG_IS_SECONDARY_IDENTIFIER_MANDATORY = "2219";

    public static final String REG_TOGGLE_ADDRESS_FILEDS_ID = "2216";
    public static final String REG_IS_ADDRESS_FILEDS_MANDATORY = "2220";

    // Media related intent ID's
    public static final int INTENT_ID_TAKE_PHOTO = 50;
    public static final int INTENT_ID_PICK_PHOTO = 51;
    public static final int INTENT_ID_RECORD_VIDEO = 52;
    public static final int INTENT_ID_PICK_VIDEO = 53;
    public static final int MEDIA_DETAILS_REQUEST_CODE = 191;
    public static final int ADD_PROXY_REQUEST_CODE = 192;

    public static final File APP_MENU_LIST_FILE = new File(AxSysTechApplication.getAppContext().getFilesDir(), "AppMenuListData.ser");

    public static final String MEDIA_DIRECTORY_NAME = "ExcelicareMediaDir";
    public static final String MEDIA_IMAGE_PREFIX = "IMG_";
    public static final String MEDIA_VIDEO_PREFIX = "VID_";
    public static final String MEDIA_AUDIO_PREFIX = "AUD_";

    public static final String DF_MEDIA_CALENDAR_DATE = "com.axsystech.excelicare.util.AxSysConstants.DF_MEDIA_CALENDAR_DATE";
    public static final String DF_MEDIA_DETAILS_DATE_TIME = "dd MMM yyyy hh:mm a";
    public static final String DF_MEDIA_DETAILS_ONLY_DATE = "dd MMM yyyy";
    public static final String DF_MEDIA_DETAILS_HEADER_DATE_FORMAT = "MMM dd, yyyy";
    public static final String DF_MEDIA_ITEM_SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"; //"2015-07-22 17:17:00"
    public static final String DF_TIMESLOT_REQUEST_DATE_FORMAT = "yyyy-MM-dd"; // 2015-08-22
    public static final String DF_TIMESLOT_DISPLAY_DATE_FORMAT = "EEEE, dd/MM/yyyy"; // Monday, 24/08/2015
    public static final String DF_TIMESLOT_TIME_FORMAT = "HH:mm:ss"; //"17:18:50"
    public static final String DF_TIMESLOT_DISPLAY_FORMAT = "HH:mm"; //"17:18"
    public static final String DF_BOOK_APPOINTMENT_REQUEST = "yyyy/MM/dd";
    public static final String DF_APPOINTMENT_BOOKING_DATETIME = "yyyy/MM/dd HH:mm:ss";

    /**/public static final String DF_MEDIA_DETAILS_ONLY_TIME = "hh:mm a";
    // Dashboard card related ID's
    public static final String DBC_COMPONENT_IMAGE_ID = "16";
    public static final String DBC_COMPONENT_LABLE_ID = "4";
    public static final String DBC_COMPONENT_BUTTON_ID = "9";

    public static final String DBC_DATAENTRYCARD_ID = "15309";
    public static final String DBC_URL_DATACRD = "15311"; // For URL / PDF cards

    public static final String DBC_COMPOSITECARD_ID = "15308";
    public static final String DBC_LISTCARD_ID = "15310";

    // URL cards related ID's
    public static final String URL_CARD_NORMAL = "0";
    public static final String URL_CARD_BROWSER = "1";
    public static final String URL_CARD_POPUP = "2";

    // Wizards
    public static final String WIZARDS_YES = "1";
    public static final String WIZARDS_NO = "0";

    //Previous button
    public static final String DBC_PREV_BTN_ID = "btnPrevious";
    public static final String DBC_NEXT_BTN_ID = "btnNext";
    public static final String DBC_MENU_BTN_ID = "btnMenu";
    public static final String DBC_ADDNEW_BTN_ID = "btn+";
    public static final String DBC_ADD_BTN_ID = "btnAdd";

    // Broadcast Receiver constants
    public static final String BCR_UPDATE_MEDIA_SCREEN = "com.axsystech.excelicare.util.AxSysConstants.BCR_UPDATE_MEDIA_SCREEN";

    // LOOK UP ID's
    public static final String LOOKUP_SEQURITY_QUESTIONS = "2748";
    public static final String LOOKUP_GENDER_TYPES = "132";
    public static final String LOOKUP_PROXY = "3134";
    public static final String LOOKUP_CLINICAL_CARE_MANAGER = "3135";
    public static final String LOOKUP_SPECIALITIES = "134";
    public static final String LOOKUP_USERLOOKUPS = "0";

    // Panel ID's
    public static final String PANEL_ID_APPOINTMENTS = "1112";
    public static final String PANEL_ID_PAST_APPOINTMENTS = "7000035";
    public static final String PANEL_ID_CANCELLED_APPOINTMENTS = "10237";

    // Messages static id's
    public static final String MSG_INBOX_PANEL_ID = "43";
    public static final String MSG_SENT_PANEL_ID = "74";

    //Circles Invitation Status
    public static final String PENDING = "3143";
    public static final String ACCEPTED = "3141";
    public static final String DECLINED = "3142";

    public static final String LOGIN_MODULE_ID = "1030";

    //Private key for AES Encryption and Decryption
    public static final String AES_KEY = "AXSYS";

    public static final String SENDER_ID = "61570760270";
    public static final String GCM_REGISTRATION_ID = "regid key";
    public static final String TOKEN = "token key";
    public static final String LOGIN_TOKEN = "login_key";

}
