package com.babylon.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.activity.BaseActivity;
import com.babylon.model.Patient;
import com.babylon.model.PhysicalActivity;
import com.babylon.recognition.DetectionRemover;
import com.babylon.recognition.DetectionRequester;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.AutoTrackedActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import name.bagi.levente.pedometer.PedometerSettings;
import name.bagi.levente.pedometer.R;
import name.bagi.levente.pedometer.StepService;

import java.util.Date;

public class TrackingService extends StepService {

    private static final String TAG = TrackingService.class.getSimpleName();

    /**
     * Receives messages from activity.
     */
    private final IBinder mBinder = new TrackingStepBinder();

    Thread serviceThread;

    private DetectionRequester mDetectionRequester;
    private DetectionRemover mDetectionRemover;
    private int steps;

    //We do not process distance
    private float distance;

    public static boolean isRunning() {
        ActivityManager manager = (ActivityManager) App.getInstance().getSystemService(ACTIVITY_SERVICE);
        if (manager != null
                && manager.getRunningServices(Integer.MAX_VALUE) != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (TrackingService.class.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class TrackingStepBinder extends Binder {
        public TrackingService getService() {
            return TrackingService.this;
        }
    }

    @Override
    public void onCreate() {
        PreferencesManager.getInstance().setTrackingActivity(true);
        serviceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                runThread();
            }
        });
        serviceThread.start();
        dataChangedListener = new OnDataChangedListener() {
            @Override
            public void onCaloriesChanged(int calories) {
                onCaloriesChange(calories);
            }

            @Override
            public void onDistanceChanged(float distanceInKm) {
                onDistanceChange(distanceInKm);
            }

            @Override
            public void onStepsChanged(int steps) {
                onStepsChange(steps);
            }
        };

        if (!SyncUtils.isLoggedIn()) {
            resetValues();
        }
    }

    private void runThread() {
        super.onCreate();
        final Context context = App.getInstance();
        mDetectionRequester = new DetectionRequester(context);
        mDetectionRemover = new DetectionRemover(context);

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.registerReceiver(connectionFailureReceiver, new IntentFilter(Constants.CONNECTION_FAILURE_FILTER));

        IntentFilter mBroadcastFilter = new IntentFilter(Constants.KEY_ACTION_REFRESH_STATUS);
        mBroadcastFilter.addCategory(Constants.KEY_CATEGORY_LOCATION_SERVICES);
        localBroadcastManager.registerReceiver(activityChangedReceiver, mBroadcastFilter);
        startRecognitionUpdate();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(App.getInstance()).unregisterReceiver(connectionFailureReceiver);
        LocalBroadcastManager.getInstance(App.getInstance()).unregisterReceiver(activityChangedReceiver);

        stopRecognitionUpdate();
        if (serviceThread != null && !serviceThread.isInterrupted()) {
            serviceThread.interrupt();
        }

        PreferencesManager.getInstance().setTrackingActivity(false);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        L.i(TAG, "[SERVICE] onBind");
        return mBinder;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void reloadSettings() {
        Patient patient = App.getInstance().getPatient();
        if (patient != null && patient.getWeight() != null) {
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                    .putString(PedometerSettings.WEIGHT_KEY, patient.getWeight().toString()).commit();
        }
        super.reloadSettings();
    }

    private void stopRecognitionUpdate() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS
                && mDetectionRemover != null && mDetectionRequester != null
                && mDetectionRequester.getRequestPendingIntent() != null) {
            mDetectionRemover.removeUpdates(mDetectionRequester.getRequestPendingIntent());
            mDetectionRequester.getRequestPendingIntent().cancel();
        }
    }

    private void startRecognitionUpdate() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            mDetectionRequester.requestUpdates();
        }
    }

    BroadcastReceiver connectionFailureReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.DETECTIONREMOVER_CONNECTION_FAILURE)) {
                stopRecognitionUpdate();
            } else if (action.equals(Constants.DETECTIONREQUESTER_CONNECTION_FAILURE)) {
                startRecognitionUpdate();
            }
        }
    };

    BroadcastReceiver activityChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getIntExtra(Constants.KEY_ACTIVITY_TYPE, -1) != -1) {
                //received an intent from ActivityRecognitionService when activity has changed
                L.i(TAG, "Received changed physical activity");
            }
        }
    };

    /**
     * Show a notification while this service is running.
     */
    protected void showNotification() {
        Notification notification = new Notification.Builder(getBaseContext())
                .setSmallIcon(R.drawable.ic_notification)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MIN)
                .setContentText(getText(R.string.notification_subtitle))
                .setContentTitle(getText(R.string.app_name))
                .build();

        notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(this, BaseActivity.class));
        intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.LOCATION_TRACKING_FRAGMENT_TAG);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        mNM.notify(R.string.app_name, notification);
    }

    private void onCaloriesChange(int calories) {
        PhysicalActivity walkingActivity = AutoTrackedActivity.readFromCache();
        if (walkingActivity != null) {

            Date walkingDate = walkingActivity.getStartDate();
            Date date = DateUtils.getStartDate(new Date());

            walkingActivity.setStartDate(date);
            walkingActivity.setEndDate(date);

            walkingActivity.setCalories(calories);
            walkingActivity.setSteps(steps);

            saveActivity(walkingActivity, walkingDate);
        }
    }

    private void saveActivity(PhysicalActivity walkingActivity, Date walkingDate) {
        if (walkingDate != null
                && walkingDate.getTime() < walkingActivity.getStartDate().getTime()) {
            resetValues();
        }

        boolean needCleanUp = PreferencesManager.getInstance(App.getInstance()).getBoolean(Constants
                .NEED_CLEAN_UP, false);

        if(walkingActivity != null && needCleanUp) {
            PreferencesManager.getInstance(App.getInstance()).setBoolean(Constants
                    .NEED_CLEAN_UP, false);
            resetValues();
        } else {
            PreferencesManager.getInstance().setObject(Keys.WALKING_ACTIVITY_KEY, walkingActivity);
        }
    }

    private void onDistanceChange(float distance) {
        this.distance = distance;
    }

    private void onStepsChange(int steps) {
        this.steps = steps;
    }
}