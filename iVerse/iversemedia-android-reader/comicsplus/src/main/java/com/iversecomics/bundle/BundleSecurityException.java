package com.iversecomics.bundle;

public class BundleSecurityException extends BundleReadException {
    private static final long serialVersionUID = 8086439615891383110L;

    public BundleSecurityException(String message, Throwable cause) {
        super(message, cause);
    }

    public BundleSecurityException(String message) {
        super(message);
    }
}
