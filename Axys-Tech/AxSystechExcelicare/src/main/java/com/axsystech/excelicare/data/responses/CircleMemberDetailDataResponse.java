package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vvydesai on 8/26/2015.
 */
public class CircleMemberDetailDataResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private DataObjectMembersList dataObjectMembersList;

    public DataObjectMembersList getDataObjectMembersList() {
        return dataObjectMembersList;
    }

    public class DataObjectMembersList implements Serializable {

        @SerializedName("Address1")
        private String Address1;

        @SerializedName("Address2")
        private String Address2;

        @SerializedName("Address3")
        private String Address3;

        @SerializedName("Email")
        private String Email;

        @SerializedName("EndReasonComment")
        private String EndReasonComment;

        @SerializedName("EndReason_LU")
        private String EndReason_LU;

        @SerializedName("ForeName")
        private String ForeName;

        @SerializedName("IsActive")
        private String IsActive;

        @SerializedName("LMD")
        private String LMD;

        @SerializedName("LastModifiedDate")
        private String LastModifiedDate;

        @SerializedName("LastModifiedUserID")
        private String LastModifiedUserID;

        @SerializedName("Phone")
        private String Phone;

        @SerializedName("RoleType_SLU")
        private String RoleType_SLU;

        @SerializedName("StartDate")
        private String StartDate;

        @SerializedName("StopDate")
        private String StopDate;

        @SerializedName("SurName")
        private String SurName;

        @SerializedName("URL")
        private String URL;

        @SerializedName("UserAuthorityType_SLU")
        private String UserAuthorityType_SLU;

        @SerializedName("UserCircleMemberEmail_ID")
        private String UserCircleMemberEmail_ID;

        @SerializedName("UserCircleMember_ID")
        private String UserCircleMember_ID;

        @SerializedName("UserCircle_ID")
        private String UserCircle_ID;

        @SerializedName("UserID")
        private String UserID;

        public String getAddress1() {
            return Address1;
        }

        public String getAddress2() {
            return Address2;
        }

        public String getAddress3() {
            return Address3;
        }

        public String getEmail() {
            return Email;
        }

        public String getEndReasonComment() {
            return EndReasonComment;
        }

        public String getEndReason_LU() {
            return EndReason_LU;
        }

        public String getForeName() {
            return ForeName;
        }

        public String getIsActive() {
            return IsActive;
        }

        public String getLMD() {
            return LMD;
        }

        public String getLastModifiedDate() {
            return LastModifiedDate;
        }

        public String getLastModifiedUserID() {
            return LastModifiedUserID;
        }

        public String getPhone() {
            return Phone;
        }

        public String getRoleType_SLU() {
            return RoleType_SLU;
        }

        public String getStartDate() {
            return StartDate;
        }

        public String getStopDate() {
            return StopDate;
        }

        public String getSurName() {
            return SurName;
        }

        public String getURL() {
            return URL;
        }

        public String getUserAuthorityType_SLU() {
            return UserAuthorityType_SLU;
        }

        public String getUserCircleMemberEmail_ID() {
            return UserCircleMemberEmail_ID;
        }

        public String getUserCircleMember_ID() {
            return UserCircleMember_ID;
        }

        public String getUserCircle_ID() {
            return UserCircle_ID;
        }

        public String getUserID() {
            return UserID;
        }

    }

}
