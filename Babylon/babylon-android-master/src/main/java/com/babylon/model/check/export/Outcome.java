package com.babylon.model.check.export;

import com.google.gson.annotations.SerializedName;

public class Outcome extends BaseExportModel {
    String name;
    String description;
    int gender;
    int bodyPartId;
    int sort;
    @SerializedName("book_appointment")
    int bookAppointment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBodyPartId() {
        return bodyPartId;
    }

    public void setBodyPartId(int bodyPartId) {
        this.bodyPartId = bodyPartId;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getBookAppointment() {
        return bookAppointment;
    }

    public void setBookAppointment(int bookAppointment) {
        this.bookAppointment = bookAppointment;
    }

    public int getGender() {

        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }


}
