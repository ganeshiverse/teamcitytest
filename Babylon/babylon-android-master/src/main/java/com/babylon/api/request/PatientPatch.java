package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestPatch;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.http.message.BasicHeader;

public class PatientPatch extends AbsRequestPatch<Patient, Patient> {

    private String entryId;

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    public AbsRequestPatch<Patient, Patient> withEntry(Patient entry) {
        entryId = entry.getId();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("patient", getGson().toJsonTree(entry));
        jsonBody = jsonObject.toString();
        return this;
    }

    /**
     * Builds the PATCH request body using only the supplied field names.
     * @param entry The patient object to build this request from
     * @param fieldsToPatch The fields to patch
     * @return this
     */
    public AbsRequestPatch<Patient, Patient> withEntry(Patient entry, String[] fieldsToPatch) {
        entryId = entry.getId();
        JsonObject jsonObject = new JsonObject();
        JsonElement patientElement = getGson().toJsonTree(entry);
        JsonObject patientObject = patientElement.getAsJsonObject();
        JsonObject filteredObject = new JsonObject();
        //Copy properties from the 'full' patient object that match the field arguments into the 'filtered' patient object
        for(String fieldName : fieldsToPatch) {
            filteredObject.add(fieldName, patientObject.get(fieldName));
        }
        jsonObject.add("patient", filteredObject);
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        String url = Urls.getRuby(Urls.PATIENTS);
        return TextUtils.concat(url, "/", entryId).toString();
    }

    @Override
    protected AbsResponseProcessor<Patient> getResponseProcessor() {
        return new RubyResponseProcessor<Patient>(getOutputType());
    }
}
