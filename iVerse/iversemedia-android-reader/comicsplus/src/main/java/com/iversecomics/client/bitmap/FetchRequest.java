package com.iversecomics.client.bitmap;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class FetchRequest implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(FetchRequest.class);
    private LateBitmapRef ref;
    private IBitmapLoader loader;
    private URI uri;
    private Bitmap bitmap;
    private Handler handler;

    public FetchRequest(LateBitmapRef ref, Handler handler) {
        this.ref = ref;
        this.uri = this.ref.getBitmapUri();
        this.handler = handler;
    }

    /**
     * NOTE: Only called via Handler to allow UI thread to process the assignment.
     */
    public void deliverBitmap() {
        ref.getListener().setLateBitmap(bitmap, ref.getTag());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchRequest other = (FetchRequest) obj;
        if (uri == null) {
            if (other.uri != null) {
                return false;
            }
        } else if (!uri.equals(other.uri)) {
            return false;
        }
        return true;
    }

    public IBitmapLoader getLoader() {
        return loader;
    }

    public boolean hasBitmap() {
        return (this.bitmap != null);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        try {
            LOG.debug("Loading Bitmap: " + ref.getBitmapUri().toASCIIString());
            bitmap = loader.loadBitmap(ref.getBitmapUri());
            if (LOG.isDebugEnabled()) {
                long end = System.currentTimeMillis();
                long dur = (end - start);
                long bitmapSize = bitmap.getRowBytes() * bitmap.getHeight();
                LOG.debug("Load of Bitmap took %,d ms - %,d bytes", dur, bitmapSize);
            }
            Message msg = handler.obtainMessage();
            msg.obj = this;
            handler.sendMessage(msg);
        } catch (IOException e) {
            long end = System.currentTimeMillis();
            long dur = (end - start);
            LOG.error(e, "Failed to load bitmap (%d ms): %s", dur, ref.getBitmapUri().toASCIIString());
        }
    }

    public void setLoader(IBitmapLoader loader) {
        this.loader = loader;
    }
}
