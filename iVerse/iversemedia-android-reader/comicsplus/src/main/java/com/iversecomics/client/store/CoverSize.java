package com.iversecomics.client.store;

import android.content.Context;
import android.net.Uri;
import android.view.View;

import com.iversecomics.client.util.Dim;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public enum CoverSize {
    /**
     * For small images
     */
    SMALL(Dim.COVER_SMALL),

    /**
     * For medium images
     */
    MEDIUM(Dim.COVER_MEDIUM),

    /**
     * For tablet sized medium images
     */
    TABLET_MEDIUM(Dim.COVER_TABLET_MEDIUM),

    /**
     * For large images
     */
    LARGE(Dim.COVER_LARGE),

    /**
     * For tablet sized retina large images
     */
    TABLET_LARGE(Dim.COVER_TABLET_LARGE),;
    private static final Logger LOG = LoggerFactory.getLogger(CoverSize.class);
    private Dim dim;

    private CoverSize(Dim dim) {
        this.dim = dim;
    }

    public static CoverSize bestFit(Context context, int dimenHeightRes) {
        int heightPx = context.getResources().getDimensionPixelSize(dimenHeightRes);
        return bestFit(heightPx);
    }

    /**
     * @param testHeight desired height
     * @return closest size to given height (might be smaller)
     */
    public static CoverSize bestFit(int testHeight) {
        CoverSize[] values = CoverSize.values();
        for (int i = 0; i < values.length - 1; i++) {
            CoverSize size = values[i];
            CoverSize nextSize = values[i + 1];

            //If our height exceeds both, continue to the next size up
            if (testHeight > size.dim.getHeight() && testHeight >= nextSize.dim.getHeight())
                continue;

            //Ok, height falls between these two, err on the closer of the two, only accepting the smaller if we're within 15% of the requested height
            int thisDistance = Math.abs(testHeight - size.dim.getHeight());
            int nextDistance = Math.abs(testHeight - nextSize.dim.getHeight());
            if (thisDistance < nextDistance && (size.dim.getHeight() / testHeight) > 0.8) { //smallest is less than 20% smaller
                return size;
            } else {
                //Otherwise, return the larger of the two
                return nextSize;
            }
        }

        return TABLET_LARGE; //fallback
    }

    /**
     * @param testHeight desired height
     * @return closest size larger than given
     */
    public static CoverSize bestFitUp(int testHeight) {
        CoverSize[] values = CoverSize.values();// kept sorted
        for (int i = 0; i < values.length - 1; i++) {
            CoverSize size = values[i];
            if (testHeight >= size.dim.getHeight()) {
                return size;
            }
        }
        return TABLET_LARGE; //fallback
    }

    /**
     * Attempt to figure out the best CoverSize based on the width and height provided;
     *
     * @param testWidth  the test width
     * @param testHeight the test height
     * @return the best fit cover size
     */
    public static CoverSize bestFit(int testWidth, int testHeight) {
        return bestFit(testHeight);
    }

    public static CoverSize bestFit(View view) {
        CoverSize fit = bestFit(view.getWidth(), view.getHeight());
        LOG.debug("Best Fit (%d x %d) = %s (%d x %d)", view.getWidth(), view.getHeight(), fit.name(),
                fit.dim.getWidth(), fit.dim.getHeight());
        return fit;
    }

    public Dim getDim() {
        return dim;
    }

    public Uri getServerUri(String fileName) {
        switch (this) {
            case TABLET_LARGE:
                return ServerConfig.getDefault().getiPadLargeImageUri(fileName);
            case LARGE:
                return ServerConfig.getDefault().getLargeImageUri(fileName);
            case SMALL:
                return ServerConfig.getDefault().getSmallImageUri(fileName);
            case TABLET_MEDIUM:
                return ServerConfig.getDefault().getiPadMediumImageUri(fileName);
            case MEDIUM:
            default:
                return ServerConfig.getDefault().getMediumImageUri(fileName);
        }
    }
}
