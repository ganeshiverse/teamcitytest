package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Table to track the ordering of Comics from the server side.
 */
public class OrderingTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri    CONTENT_URI        = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "ordering");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of my comics.
     */
    public static final String CONTENT_TYPE       = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                          + "/vnd.iversecomics.store.comics";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single user.
     */
    public static final String CONTENT_ITEM_TYPE  = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                          + "/vnd.iversecomics.store.comics";

    public static final String DEFAULT_SORT_ORDER = "idx ASC";
    public static final String TABLE              = "ordering";

    public static final String COMICID            = "comicId";
    public static final String LISTID             = "listId";
    public static final String INDEX              = "idx";
    public static final String TIMESTAMP          = "timestamp";

    public static final String PROJECTION_FULL[]  = new String[] {
            TABLE + "." + _ID, TABLE + "." + COMICID, LISTID, INDEX, TIMESTAMP
                                                  };
}
