package com.iversecomics.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;

import com.iversecomics.archie.android.R;
import com.iversecomics.bundle.BundleSecurityException;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Storage;
import com.iversecomics.client.comic.ComicBundleSourceAdapter;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.ui.ComicReaderPhotoViewActivity;
import com.iversecomics.ui.LoginActivity;

import java.io.File;
import java.io.IOException;

public class ComicOpenHelper {
    private static final Logger LOG = LoggerFactory
            .getLogger(ComicOpenHelper.class);

    static public void startComicReader(final MyComic myComic, final Activity activity) {

        File comicFile = null;
        try {
            comicFile = new Storage().getFile(myComic.getAssetFilename());
        } catch (IOException e) {
            LOG.warn(e, "Unable to open storage??");

            return;
        }

        //force the security check to happen right here
        final ComicBundleSourceAdapter comicSource;
        comicSource = new ComicBundleSourceAdapter(comicFile);

        final IverseApplication app = (IverseApplication) activity.getApplication();
        comicSource.setOwnership(app.getOwnership());

        //opening the comic fill force the security status to update in the MyComic instance
        try {
            comicSource.open();
            comicSource.close();
        } catch (IOException e) {
            LOG.warn(e, "Error when opening comic source instance");
            myComic.setStatus(ComicMetadata.STATUS_BAD);

            if (e instanceof BundleSecurityException) {
                myComic.setStatus(ComicMetadata.STATUS_NOTYOURS);
            }
        }

        final ComicMetadata cm = comicSource.getMetadata();
        myComic.setStatus(cm.getStatus());
        final MyComicsModel mcm = new MyComicsModel(activity);
        mcm.update(myComic);

        // Confirm the status is good before opening
        if (checkComicStatusBeforeLaunch(myComic.getStatus(), cm, activity)) {
            final Intent intent = new Intent(activity, ComicReaderPhotoViewActivity.class);
            intent.putExtra(ComicReaderPhotoViewActivity.EXTRA_COMIC_BUNDLE_NAME, myComic.getComicId());
            activity.startActivity(intent);
        }
    }

    static boolean checkComicStatusBeforeLaunch(final int comicStatus, final ComicMetadata cm, final Activity activity) {
        String title = null;
        String message = null;
        final Resources res = activity.getResources();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        switch (comicStatus) {
            case ComicMetadata.STATUS_NOTYOURS:
                title = res.getString(R.string.comic_open_not_yours_dialog_title);

                if (cm.getRegisteredTo() != null) {
                    final String s = res.getString(R.string.comic_open_not_yours_dialog_message);
                    message = String.format(s, cm.getRegisteredTo());
                } else {
                    message = res.getString(R.string.comic_open_not_yours_dialog_title);
                }

                builder
                        .setPositiveButton(R.string.cancel, null)
                        .setNegativeButton(R.string.login_positive_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Offline support
                                if(ConnectionHelper.isConnectedToInternet(activity)) {
                                    activity.startActivity(new Intent(activity, LoginActivity.class));
                                }else{
                                    ConnectionHelper.showNoInternetMessage();
                                }
                            }
                        });
                break;

            case ComicMetadata.STATUS_BAD:
                title = res.getString(R.string.comic_open_bad_comic_dialog_title);
                message = res.getString(R.string.comic_open_bad_comic_dialog_message);
                builder
                        .setPositiveButton(android.R.string.ok, null);

                break;

            case ComicMetadata.STATUS_LIB_FAILURE:
                title = res.getString(R.string.comic_open_error_dialog_title);
                message = res.getString(R.string.comic_open_error_dialog_message);
                builder
                        .setPositiveButton(android.R.string.ok, null);
                break;

        }

        if (title != null || message != null) {
            builder.setIcon(android.R.drawable.stat_sys_warning)
                    .setTitle(title)
                    .setMessage(message)
                    .setCancelable(false)
                    .create()
                    .show();

            return false;
        }

        return true;
    }
}
