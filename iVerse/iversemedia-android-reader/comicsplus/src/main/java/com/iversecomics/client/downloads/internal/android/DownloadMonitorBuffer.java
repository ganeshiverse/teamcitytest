package com.iversecomics.client.downloads.internal.android;

import android.net.Uri;

import com.iversecomics.client.downloads.DownloaderManager;
import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;

/**
 * A buffer of monitoring events, to limit the pace of updates to the actual monitor based on time and progress byte
 * count.
 */
public class DownloadMonitorBuffer implements DownloadMonitor {
    private DownloadMonitor child;

    // The # of bytes sent to notification flows
    private long lastNotifiedBytes = 0;
    // The last timestamp of the notification flows were ... uh ... notified.
    private long lastNotifiedTime = 0;
    private int minProgressStep;
    private long minProgressTime;
    private long totalBytes;

    public DownloadMonitorBuffer(DownloadMonitor child) {
        this.child = child;
        this.minProgressStep = DownloaderManager.getInstance().getNotificationMinProgressStep();
        this.minProgressTime = DownloaderManager.getInstance().getNotificationMinProgressTime();
    }

    @Override
    public void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        this.totalBytes = totalBytes;
        // Pass on immediately.
        child.downloadConnected(uri, title, totalBytes, etag);
    }

    @Override
    public void downloadFailure(Uri uri, DownloadException failure) {
        // Pass on immediately.
        child.downloadFailure(uri, failure);
    }

    @Override
    public void downloadProgress(Uri uri, long currentByte) {
        if ((totalBytes > 0) && (currentByte == totalBytes)) {
            // Immediate notification (we are done)
            child.downloadProgress(uri, currentByte);
            lastNotifiedBytes = 0;
            return;
        }

        if ((currentByte - lastNotifiedBytes) < minProgressStep) {
            // Not large enough of a step.
            return;
        }

        long now = System.currentTimeMillis();
        if ((now - lastNotifiedTime) < minProgressTime) {
            // Not enough time has passed
            return;
        }

        lastNotifiedBytes = currentByte;
        lastNotifiedTime = now;
        child.downloadProgress(uri, currentByte);
    }

    @Override
    public void downloadState(Uri uri, DownloadStatus state, String message) {
        // Pass on immediately.
        child.downloadState(uri, state, message);
    }
}
