package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;

public class PatientPost extends AbsRequestPost<Patient, Patient> {

    @Override
    protected String getUrl() {
        return Urls.get(Urls.PATIENT_DATE_POST);
    }

}
