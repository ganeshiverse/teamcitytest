package com.babylon.fragment.payments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.payments.OnAddFamilyToPlanListener;
import com.babylon.api.request.DoctorBiographyGet;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Doctor;
import com.babylon.model.payments.Appointment;
import com.babylon.utils.DateUtils;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.view.TopBarInterface;
import com.babylonpartners.babylon.CircleTransform;
import com.babylonpartners.babylon.HomePageActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppointmentConfirmationFragment extends BaseFragment
        implements View.OnClickListener {

    public static final String TAG = AppointmentConfirmationFragment.class.getSimpleName();

    public interface OnAppointmentConfirmationListener extends OnAddFamilyToPlanListener {
        void onDoctorBiographyClick();

        void onAddCalendarEventClick();

        void onAddBabylonContactClick();

        void onAppointmentDone();
    }

    public interface DataProtocol {
        public Appointment getAppointment();
    }

    private OnAppointmentConfirmationListener appointmentListener;
    private DataProtocol dataProtocol;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            appointmentListener = (OnAppointmentConfirmationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAppointmentConfirmationListener");
        }
        try {
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        appointmentListener = null;
        Intent i = new Intent(getActivity(), HomePageActivity.class);
        i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                .FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        getActivity().finish();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_appointment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        setProgressBarVisible(true);
        setBackButtonVisible(false);

        DoctorBiographyGet.getAlliantsDoctorBiography(dataProtocol.getAppointment()
                .getConsultantId()).execute(getActivity(), onDoctorRequestListener);

        showAppointmentTime();

        TextView calendarTextView = (TextView) view.findViewById(
                R.id.appointment_add_to_calendar_text_view);
        TextView babylonContactTextView = (TextView) view.findViewById(
                R.id.appointment_add_contact_text_view);
        TextView familyPlanTextView = (TextView) view.findViewById(
                R.id.appointment_family_plan_text_view);
        LinearLayout doctorLayout = (LinearLayout) view.findViewById(
                R.id.appointment_gp_linear_layout);

        doctorLayout.setOnClickListener(this);
        calendarTextView.setOnClickListener(this);
        babylonContactTextView.setOnClickListener(this);
        familyPlanTextView.setOnClickListener(this);

        if(dataProtocol.getAppointment().getConsultantRole().compareToIgnoreCase("specialist") == 0)
        {
            //Promotion MixPanel Event
            JSONObject props = new JSONObject();
            try
            {
                props.put("GP Appointment Hour Slot", dataProtocol.getAppointment().getStartTime());
                props.put("Consultant ID", dataProtocol.getAppointment().getConsultantId());
                props.put("Consultant Role", dataProtocol.getAppointment().getConsultantRole());
                props.put("Is Using Promotion", dataProtocol.getAppointment().getIsPromotion());
                props.put("State", dataProtocol.getAppointment().getState());
                if(dataProtocol.getAppointment().videoSessionId() != null)
                {
                    props.put("Type Of Appointment","Video Session");
                }
                else
                {
                    props.put("Type Of Appointment","Phone Appointment");
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            MixPanelHelper helper = new MixPanelHelper(getActivity(),props,null);
            helper.sendEvent(MixPanelEventConstants.SPECIALIST_BOOKED);
        }
        else
        {
            //Mix Panel Event for appointment booked
            JSONObject props = new JSONObject();
            try {
                props.put("GP Appointment Hour Slot", dataProtocol.getAppointment().getStartTime());
                props.put("Consultant ID", dataProtocol.getAppointment().getConsultantId());
                props.put("Consultant Role", dataProtocol.getAppointment().getConsultantRole());
                props.put("Is Using Promotion", dataProtocol.getAppointment().getIsPromotion());
                props.put("State", dataProtocol.getAppointment().getState());
                if(dataProtocol.getAppointment().videoSessionId() != null)
                {
                    props.put("Type Of Appointment","Video Session");
                }
                else
                {
                    props.put("Type Of Appointment","Phone Appointment");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Bundle fb_parameters = new Bundle();
            fb_parameters.putString("GP Appointment Hour Slot", dataProtocol.getAppointment().getStartTime());
            fb_parameters.putInt("Consultant ID", dataProtocol.getAppointment().getConsultantId());


            MixPanelHelper helper = new MixPanelHelper(getActivity(), props, fb_parameters);
            helper.sendEvent(MixPanelEventConstants.GP_APPOINTMENT_BOOKED);
        }

        setTitle(R.string.appointment_title, R.dimen.text_size_label_15sp);
        setRightButton(this, R.string.done);
    }

    private void showAppointmentTime() {
        final TextView timeTextView = (TextView) getView().findViewById(R.id.appointment_time_text_view);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.HOUR_MINUTE1_FORMAT,
                Locale.getDefault());
        final String startTime = dataProtocol.getAppointment().getStartTime();

        if (!TextUtils.isEmpty(startTime)) {
            long millisecondsWhen = DateUtils.timeToLong(startTime, DateUtils.YT_DATE_FORMAT_ZZZ);
            final String when = simpleDateFormat.format(new Date(millisecondsWhen));
            timeTextView.setText(when);
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }

    @Override
    public void onClick(View view) {
        if (appointmentListener != null) {
            switch (view.getId()) {
                case R.id.topbar_right_text_button:
                    appointmentListener.onAppointmentDone();
                    break;
                case R.id.appointment_gp_linear_layout:
                    appointmentListener.onDoctorBiographyClick();
                    break;
                case R.id.appointment_add_to_calendar_text_view:
                    appointmentListener.onAddCalendarEventClick();
                    break;
                case R.id.appointment_add_contact_text_view:
                    appointmentListener.onAddBabylonContactClick();
                    break;
                case R.id.appointment_family_plan_text_view:
                    appointmentListener.onAddFamilyToPlanClick();
                    break;
                default:
                    break;
            }
        }
    }

    private void showDoctorBiography(Doctor doctor) {
        final String name = doctor.getName();
        final TextView doctorNameTextView = (TextView) getView().findViewById(R.id.appointment_name_text_view);
        doctorNameTextView.setText(name);

        final ImageView doctorImageView = (ImageView) getView().findViewById(R.id.appointment_avatar_image_view);
        final String avatar = doctor.getAvatar();

        if (!TextUtils.isEmpty(avatar)) {
            Picasso.with(getActivity()).load(Urls.getRuby(avatar))
                    .transform(new CircleTransform()).into(doctorImageView);
        }
    }

    private final RequestListenerErrorProcessed<Doctor> onDoctorRequestListener = new
            RequestListenerErrorProcessed<Doctor>() {
                @Override
                public void onRequestComplete(Response<Doctor> response, String errorMessage) {
                    if (getActivity() != null) {
                        setProgressBarVisible(false);
                        if (TextUtils.isEmpty(errorMessage)) {
                            final Doctor doctor = response.getData();
                            if (doctor != null) {
                                showDoctorBiography(doctor);
                            }
                        } else {
                            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };
}
