package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.payments.PromotionTransactionActivity;
import com.babylon.adapter.RegionArrayAdapter;
import com.babylon.api.request.PatientPatch;
import com.babylon.api.request.RegionsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.controllers.payments.PromoCodeController;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.model.payments.Redemption;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.view.TickButtonWithProgress;

import org.apache.http.HttpStatus;

import java.util.Locale;

public class TermsPopupActivity extends BaseActivity implements View.OnClickListener {

    private Region region;
    private Spinner regionSpinner;
    private TickButtonWithProgress continueButton;
    private EditText promoCodeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_popup);

        promoCodeEditText = (EditText) findViewById(R.id.promo_code_edit_text);
        TextView termsTextView = (TextView) findViewById(R.id.terms_popup_continuing);
        termsTextView.setOnClickListener(this);

        continueButton = (TickButtonWithProgress) findViewById(R.id.terms_popup_continue_button);
        continueButton.setOnClickListener(this);

        regionSpinner = (Spinner) findViewById(R.id.terms_popup_location_spinner);
        regionSpinner.setVisibility(View.INVISIBLE);

        loadLocations();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.terms_popup_continuing:
                startTermsActivity();
                break;
            case R.id.terms_popup_continue_button:
                continueButton.setProgressState(true);
                patchRegion();
                break;
        }
    }

    private void loadLocations() {
        continueButton.setProgressState(true);
        RegionsGet regionsGet = new RegionsGet();
        regionsGet.execute(this, new RequestListener<Region[]>() {
            @Override
            public void onRequestComplete(Response<Region[]> response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    Region[] regions = response.getData();
                    if (regions != null && regions.length > 0) {
                        continueButton.setProgressState(false);
                        int selectedRegionIndex = 0;
                        Address currentAddress = App.getInstance().getAddress();
                        if (currentAddress != null) {
                            String countryCode = new Locale("", currentAddress.getCountryCode()).getISO3Country();
                            for (int regionIndex = 0; regionIndex < regions.length; ++regionIndex) {
                                if (regions[regionIndex].getCountryCode().equals(countryCode)) {
                                    selectedRegionIndex = regionIndex;
                                    break;
                                }
                            }
                        }
                        Region selectedRegion = regions[selectedRegionIndex];
                        TermsPopupActivity.this.region = selectedRegion;
                        regionSpinner.setVisibility(View.VISIBLE);
                        regionSpinner.setEnabled(true);

                        final RegionArrayAdapter adapter = new RegionArrayAdapter(TermsPopupActivity.this,
                                R.layout.spinner_item_region, regions);
                        regionSpinner.setAdapter(adapter);
                        regionSpinner.setSelection(selectedRegionIndex);
                        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                TermsPopupActivity.this.region = adapter.getItem(position);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // empty
                            }
                        });
                    } else {
                        requestFailed();
                    }
                } else {
                    requestFailed();
                }
            }

            private void requestFailed() {
                continueButton.setProgressState(false);
                continueButton.setEnabled(false);
                AlertDialogHelper.getInstance().showMessage(TermsPopupActivity.this, getString(R.string.offline_error));
            }
        });
    }

    private void startTermsActivity() {
        Intent intent = new Intent(this, TermsAndConditionsActivity.class);
        startActivity(intent);
    }

    private void patchRegion() {
        Patient patient = App.getInstance().getPatient();
        patient.setRegionId(region.getId());
        patient.setIsUpdated(false);
        new PatientPatch().withEntry(patient, new String[] {"region_id"} ).execute(this, new RequestListener<Patient>() {
            @Override
            public void onRequestComplete(Response<Patient> response) {
                Patient responsePatient = response.getData();
                App.getInstance().setPatient(responsePatient);
                continueButton.setProgressState(false);
                if (response.getStatus() == HttpStatus.SC_OK) {
                    String promoCode = promoCodeEditText.getText().toString().trim();
                    if (!TextUtils.isEmpty(promoCode)) {
                        sendPromoCode(promoCode);
                    } else {
                        finishActivity(responsePatient);
                    }
                } else {
                    //Error patching region
                    continueButton.setProgressState(false);
                    continueButton.setEnabled(true);
                    AlertDialogHelper.getInstance().showMessage(TermsPopupActivity.this, getString(R.string.some_error));
                }
            }
        });
    }

    private void sendPromoCode(String promoCode) {
        final PromoCodeController promoCodeController = new PromoCodeController();
        continueButton.setEnabled(false);

        promoCodeController.sendPromoCode(this, promoCode, -1,
                new PromoCodeController.PromoCodeValidationListener() {
                    @Override
                    public void onValidPromoCode(@Nullable Redemption redemption) {
                        PromotionTransactionActivity.startPromotionTransactionActivity(
                                TermsPopupActivity.this, redemption);
                        continueButton.setEnabled(true);
                    }

                    @Override
                    public void onInvalidPromoCode(@Nullable String error) {
                        if (!TextUtils.isEmpty(error)) {
                            Toast.makeText(TermsPopupActivity.this,
                                    error, Toast.LENGTH_SHORT).show();
                        }
                        continueButton.setEnabled(true);
                    }
                });
    }

    private void finishActivity(Patient responsePatient) {
        //Success
        setResult(Activity.RESULT_OK);
        if (responsePatient.getRegionId() != null && responsePatient.getPracticeId() == null) {
            Intent intent = new Intent(TermsPopupActivity.this, SelectPracticeActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PromotionTransactionActivity.PROMOTION_REQUEST_CODE:
                Patient patient = App.getInstance().getPatient();
                finishActivity(patient);
                break;
            default:
                break;
        }
    }
}
