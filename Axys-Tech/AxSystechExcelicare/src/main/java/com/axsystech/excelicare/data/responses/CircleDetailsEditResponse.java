package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class CircleDetailsEditResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private DataObjectCircleDetails dataObject;

    public DataObjectCircleDetails getDataObject() {
        return dataObject;
    }

    public class DataObjectCircleDetails implements Serializable {

        @SerializedName("CircleName")
        private String CircleName;

        @SerializedName("Description")
        private String Description;

        @SerializedName("IsProtected")
        private String IsProtected;

        @SerializedName("LMD")
        private String LMD;

        @SerializedName("StartDate")
        private String StartDate;

        @SerializedName("StopDate")
        private String StopDate;

        @SerializedName("StopReasonDesc")
        private String StopReasonDesc;

        @SerializedName("StopReason_SLU")
        private String StopReason_SLU;

        @SerializedName("SystemCircle_Id")
        private String SystemCircle_Id;

        @SerializedName("UserCircle_ID")
        private String UserCircle_ID;

        @SerializedName("UserID")
        private String UserID;

        @SerializedName("UserType")
        private String UserType;

        public String getCircleName() {
            return CircleName;
        }

        public String getDescription() {
            return Description;
        }

        public String getIsProtected() {
            return IsProtected;
        }

        public String getLMD() {
            return LMD;
        }

        public String getStartDate() {
            return StartDate;
        }

        public String getStopDate() {
            return StopDate;
        }

        public String getStopReasonDesc() {
            return StopReasonDesc;
        }

        public String getStopReason_SLU() {
            return StopReason_SLU;
        }

        public String getSystemCircle_Id() {
            return SystemCircle_Id;
        }

        public String getUserCircle_ID() {
            return UserCircle_ID;
        }

        public String getUserID() {
            return UserID;
        }

        public String getUserType() {
            return UserType;
        }
    }

}
