package com.axsystech.excelicare;

import android.content.Context;
import android.os.AsyncTask;

import com.axsystech.excelicare.data.responses.GCMResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.SharedPreferenceUtil;
import com.axsystech.excelicare.util.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

/**
 * Async Task to authenticate the User and get the session token
 */
public class GCMRegistrationTask extends AsyncTask<Void, Void, GCMResponse> {
    private Context activity;
    private String mRegId;

    public GCMRegistrationTask(Context context, String regId) {
        this.activity = context;
        this.mRegId = regId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // show loader
    }

    @Override
    protected GCMResponse doInBackground(Void... arg0) {
//		BookFlightsModuleEngine engine = new BookFlightsModuleEngine("");
//		engine.sendGCMRegistrationToServer(activity,mRegId);

        return sendRegistrationIdToBackend();
    }

    @Override
    protected void onPostExecute(GCMResponse result) {
        super.onPostExecute(result);
        if (result != null){
            Trace.d("push status",result.mStatus);
            Trace.d("push message",result.message);
        }
    }

    private GCMResponse sendRegistrationIdToBackend() {
        URI url = null;
        GCMResponse gcmBeam = null;
        String token = SharedPreferenceUtil.getStringFromSP(activity, AxSysConstants.TOKEN);
        JSONObject dataObject = new JSONObject();
        try {
            dataObject.put("Device", "ANDROID");
            dataObject.put("Token", token);
            dataObject.put("deviceToken", mRegId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Trace.d("push --",dataObject.toString());
        if (CommonUtils.hasInternet()) {
            // Reset global data here
            try {
                gcmBeam = ServerMethods.getInstance().postGcmRegistration(dataObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return gcmBeam;
    }

     /*   try {
            System.out.println("http://115.119.121.38:8989/ECService_PHRV64Axsys_DEV/AxRSSecurity.svc/RegisterDevicePushNotification" + dataObject);
            url = new URI("http://115.119.121.38:8989/ECService_PHRV64Axsys_DEV/AxRSSecurity.svc/RegisterDevicePushNotification" + dataObject);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet();
        request.setURI(url);
        try {
            httpclient.execute(request);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/

}