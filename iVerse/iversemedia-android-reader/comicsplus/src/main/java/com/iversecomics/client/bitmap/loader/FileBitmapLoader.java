package com.iversecomics.client.bitmap.loader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iversecomics.client.bitmap.IBitmapLoader;
import com.iversecomics.io.IOUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;

public class FileBitmapLoader implements IBitmapLoader {
    @Override
    public String[] getUriSchemes() {
        return new String[]{"file"};
    }

    @Override
    public Bitmap loadBitmap(URI bitmapUri) throws IOException {
        File bitmapFile = new File(bitmapUri);

        FileInputStream stream = null;
        try {
            stream = new FileInputStream(bitmapFile);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            return BitmapFactory.decodeStream(stream, null, opts);
        } finally {
            IOUtil.close(stream);
        }
    }
}
