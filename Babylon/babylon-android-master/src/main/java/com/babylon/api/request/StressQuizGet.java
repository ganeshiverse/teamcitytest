package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.StressQuestion;

public class StressQuizGet extends AbsRequestGet<ResponseList<StressQuestion>> {
    @Override
    protected String getUrl() {
        return Urls.get(Urls.STRESS_QUIZ_GET);
    }
}
