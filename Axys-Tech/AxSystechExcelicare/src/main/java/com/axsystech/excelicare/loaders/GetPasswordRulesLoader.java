package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.PasswordRulesResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetPasswordRulesLoader extends DataAsyncTaskLibLoader<PasswordRulesResponse> {

    public GetPasswordRulesLoader(final Context context) {
        super(context);
    }

    @Override
    protected PasswordRulesResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getPasswordRules();
    }
}
