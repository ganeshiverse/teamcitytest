package com.iversecomics.client.account;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;

public class PasswordRequestActivity extends Activity {
    private class PasswordReminderResponseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if(progressDialog !=null)
                progressDialog.dismiss();
            if (PasswordRequestTask.PASSWORD_REQUEST_SERVER_RESPONSE.equals(action)) {
                final Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    if (bundle.getString("status").equals("ok")) {
                        makeSuccessfulReminderDialog(bundle.getString("userName"));
                    } else {
                        makeInvalidAccountDialog();
                    }
                }
            }
        }
    }

    ProgressDialog progressDialog;
    PasswordReminderResponseReceiver passwordReminderResponseReceiver = new PasswordReminderResponseReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logo);
        makeEmailInputDialog();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    void makeEmailInputDialog() {
        final EmailInputDialog eid = new EmailInputDialog(PasswordRequestActivity.this);
        eid.setCancelable(false);
        eid.setCancelable(false);
        eid.setButton(EmailInputDialog.BUTTON_POSITIVE, getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userEmail = eid.getEmailAddress();
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
                    new EmailAddresRequiredDialogBuilder(PasswordRequestActivity.this)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    makeEmailInputDialog();
                                }
                            }).setCancelable(false).show();
                } else {
                    final IverseApplication app = (IverseApplication) getApplication();
                    final PasswordRequestTask task = new PasswordRequestTask(app.getComicStore());
                    task.setUserName(userEmail);

                    app.getTaskPool().submit(task);
                    progressDialog = ProgressDialog.show(PasswordRequestActivity.this, "", "Requesting password...", true);
                }
            }
        });
        eid.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface arg0) {
                finish();
            }
        });
        eid.show();
    }

    @Override
    public void onResume() {
        super.onResume();

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PasswordRequestTask.PASSWORD_REQUEST_SERVER_RESPONSE);
        LocalBroadcastManager.getInstance(this).registerReceiver(passwordReminderResponseReceiver, intentFilter);
    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(passwordReminderResponseReceiver);
    }

    void makeSuccessfulReminderDialog(final String emailAddress) {
        final Resources res = getResources();
        final String message = String.format(res.getString(R.string.password_reset_message), emailAddress);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.icon)
                .setTitle(R.string.password_reset_title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }

                })
                .create().show();
    }

    void makeInvalidAccountDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.icon)
                .setTitle(R.string.invalid_account_title)
                .setMessage(R.string.invalid_account_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }

                })
                .create().show();
    }
}
