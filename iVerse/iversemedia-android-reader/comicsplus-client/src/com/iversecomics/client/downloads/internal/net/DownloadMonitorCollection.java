package com.iversecomics.client.downloads.internal.net;

import java.util.ArrayList;

import android.net.Uri;

import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class DownloadMonitorCollection extends ArrayList<DownloadMonitor> implements DownloadMonitor {
    private static final Logger LOG              = LoggerFactory.getLogger(DownloadMonitorCollection.class);
    private static final long   serialVersionUID = -9067356618003459015L;

    @Override
    public void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        for (DownloadMonitor monitor : this) {
            try {
                monitor.downloadConnected(uri, title, totalBytes, etag);
            } catch (Throwable t) {
                LOG.warn(t, "Unable to notify of download connected");
            }
        }
    }

    @Override
    public void downloadFailure(Uri uri, DownloadException failure) {
        for (DownloadMonitor monitor : this) {
            try {
                monitor.downloadFailure(uri, failure);
            } catch (Throwable t) {
                LOG.warn(t, "Unable to notify of download failure");
            }
        }
    }

    @Override
    public void downloadProgress(Uri uri, long currentByte) {
        for (DownloadMonitor monitor : this) {
            try {
                monitor.downloadProgress(uri, currentByte);
            } catch (Throwable t) {
                LOG.warn(t, "Unable to nofity of download progress");
            }
        }
    }

    @Override
    public void downloadState(Uri uri, DownloadStatus state, String message) {
        for (DownloadMonitor monitor : this) {
            try {
                monitor.downloadState(uri, state, message);
            } catch (Throwable t) {
                LOG.warn(t, "Unable to notify of download state");
            }
        }
    }
}
