package com.iversecomics.client.account;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.iversecomics.client.R;

public class EmailInputDialog extends AlertDialog {

	public EmailInputDialog(final Context context) {
		super(context);
        setIcon(R.drawable.icon);
        setTitle(R.string.email_address_required_title);
        
        final String message = context.getResources().getString(R.string.email_address_required_message);
        setMessage(message);
        
        final LayoutInflater factory = LayoutInflater.from(getContext());
        View v = factory.inflate(R.layout.login_dialog, null);
        setView(v);
        (v.findViewById(R.id.user_password)).setVisibility(View.GONE);
	}

	public String getEmailAddress()
	{
		final EditText e =(EditText)findViewById(R.id.user_email);
		return e.getText().toString();
	}
}
