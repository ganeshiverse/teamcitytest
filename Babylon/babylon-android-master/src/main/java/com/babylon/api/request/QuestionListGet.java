package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Message;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class QuestionListGet extends AbsRequestGet<ResponseList<Message>> {

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.QUESTION_LIST_GET);
        return TextUtils.concat(url, urlParams).toString();
    }

    public static QuestionListGet newQuestionListGet(int offset,
                                                     int limit,
                                                     int stateId,
                                                     int isPendingDoctor) {

        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (offset != -1) {
            params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
        }
        if (limit != -1) {
            params.add(new BasicNameValuePair("limit", String.valueOf(limit)));
        }
        if (stateId != -1) {
            params.add(new BasicNameValuePair("stateId", String.valueOf(stateId)));
        }
        if (isPendingDoctor != -1) {
            params.add(new BasicNameValuePair("isPendingDoctor", String.valueOf(isPendingDoctor)));
        }
        return (QuestionListGet) new QuestionListGet().withParams(params);
    }

    public static QuestionListGet newQuestionListGet(int offset,
                                                     int limit) {
        return QuestionListGet.newQuestionListGet(offset, limit, -1, -1);
    }
}
