package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;

/**
 * Created by Babylon on 12/05/2015.
 */

public class ProductList {

    @Expose
    private Integer id;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

}