package com.babylon.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.babylon.R;
import com.babylon.view.TopBarInterface;

public class TermsAndConditionsActivity extends BaseActivity {

    private static final String TERMS_URL = "http://www.babylonhealth.com/termsmobile";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        setTitle(R.string.terms_and_condition);

        WebView webView = (WebView) findViewById(R.id.tearms_and_conditions_webview);
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                if (url.startsWith("mailto:"))
                {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse(url));
                    if (intent.resolveActivity(getPackageManager()) != null)
                    {
                        startActivity(intent);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        });
        webView.loadUrl(TERMS_URL);

        initializeTopBar();
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PURPLE_THEME;
    }
}
