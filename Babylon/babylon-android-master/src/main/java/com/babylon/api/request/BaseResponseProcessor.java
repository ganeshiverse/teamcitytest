package com.babylon.api.request;

import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Error;
import com.babylon.api.request.base.Response;
import com.babylon.utils.L;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BaseResponseProcessor<O> extends AbsResponseProcessor<O> {

    private static final String TAG = BaseResponseProcessor.class.getSimpleName();

    public BaseResponseProcessor(Type type) {
        super(type);
    }

    protected void addError(Response<O> result, String message) {
        List<Error> errors = new ArrayList<Error>();

        try {
            LinkedTreeMap errorsElements = getGson().fromJson(message, LinkedTreeMap.class);

            for (Object key : errorsElements.keySet()) {
                String subject = key.toString();

                if (subject.equalsIgnoreCase("base"))
                    subject = "";

                Object errorsForCurrentKey = errorsElements.get(key);
                if (errorsForCurrentKey instanceof ArrayList)
                {
                    ArrayList<String> errorsForCurrentKeyArray = (ArrayList<String>)errorsForCurrentKey;
                    // just get the first error message
                    String errorMessage = errorsForCurrentKeyArray.get(0);

                    String error = toTitleCase(subject).trim() + " " + errorMessage.trim();
                    errors.add(new Error(error));
                }
            }
        }
        catch (Exception ex)
        {
            // fall back to the ugly unformatted error message
            errors.add(new Error(message));
        }

        result.setErrors(errors);
        L.e(TAG, message);
    }

    private String toTitleCase(String s){
        String[] parts = s.split("_");
        String camelCaseString = "";
        for (String part : parts){
            camelCaseString += " " + toProperCase(part);
        }
        return camelCaseString;
    }

    private String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }

    protected void addUnknownError(Response<O> result, Exception e) {
        String message = "Unknown error";

        if (e != null && e.getCause() != null) {
            message = e.getCause().getMessage();
        }

        addError(result, message);
    }

    protected void addUnknownError(Response<O> result) {
        addUnknownError(result, null);
    }

    public Response<O> getResponse(HttpResponse httpResponse) {
        StatusLine statusLine = httpResponse.getStatusLine();
        Response<O> result = new Response<O>();
        if (statusLine != null) {
            try {
                result = getGson().fromJson(getStringResponse(httpResponse), getResponseType());
            } catch (JsonParseException | NumberFormatException e) {
                addUnknownError(result, e);
            }
//            int statusCode = statusLine.getStatusCode();
//            if (statusCode == HttpStatus.SC_OK) {
//                result = getGson().fromJson(getStringResponse(httpResponse), entityType);
//            } else {
//                addError(result, statusCode, statusLine.getReasonPhrase());
//            }
        } else {
            addUnknownError(result);
        }
        return result;
    }

    protected ParameterizedType getResponseType() {
        final Type cls = getType();
        return new ParameterizedType() {

            @Override
            public Type getRawType() {
                return Response.class;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

            @Override
            public Type[] getActualTypeArguments() {
                return new Type[]{cls};
            }
        };
    }
}
