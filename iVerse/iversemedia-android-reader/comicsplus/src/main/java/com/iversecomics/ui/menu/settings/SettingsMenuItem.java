package com.iversecomics.ui.menu.settings;

import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;

/**
 * Created by jeffaoliver on 11/15/13.
 */
public class SettingsMenuItem {

    enum SettingsMenuItemType {
        Languages,
        NotifyMeFor,
        DisplayOverlays,
        OnDemandSubscription,
        RestorePurchases,
        ViewCredits
    }

    ;

    public SettingsMenuItemType itemType;
    public String title;
    public String secondary;
    public Object data;
    public boolean hasSubMenu;
    public ComicPreferences.ComicPreferenceKeys preferenceKey;
    public String languagePreferenceKey;

    public SettingsMenuItem(String title, SettingsMenuItemType itemType) {
        this.title = title;
        this.itemType = itemType;
    }

    public SettingsMenuItem(String title, SettingsMenuItemType itemType, boolean hasSubMenu) {
        this.title = title;
        this.itemType = itemType;
        this.hasSubMenu = hasSubMenu;
    }

    public SettingsMenuItem(String title, SettingsMenuItemType itemType, ComicPreferences.ComicPreferenceKeys preferenceKey) {
        this.title = title;
        this.itemType = itemType;
        this.preferenceKey = preferenceKey;
    }

    public SettingsMenuItem(String title, SettingsMenuItemType itemType, String languagePreferenceKey) {
        this.title = title;
        this.itemType = itemType;
        this.languagePreferenceKey = languagePreferenceKey;
    }

}
