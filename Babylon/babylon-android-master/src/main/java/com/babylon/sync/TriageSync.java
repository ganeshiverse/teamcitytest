package com.babylon.sync;


import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;

import com.babylon.api.request.base.Response;
import com.babylon.api.request.triage.TriageSymptomsPost;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.Table;
import com.babylon.database.schema.CheckResultSchema;
import com.babylon.model.check.CheckOutcome;
import com.babylon.model.check.CheckResult;
import com.babylon.service.TriageSyncService;

import java.util.ArrayList;


public class TriageSync extends AbsSyncStrategy {
    public static final String KEY_EXPORT_IN_PROGRESS = "KEY_EXPORT_IN_PROGRESS";
    public static final String KEY_EXPORT_DONE = "KEY_EXPORT_DONE";
    public static final String KEY_LAST_SYNC_DATE = "KEY_LAST_SYNC_DATE";
    public static final String KEY_ERROR = "KEY_ERROR";
    //Action for Broadcast receiver
    public static final String ACTION_EXPORT_IN_PROGRESS = "com.babylon.ACTION_EXPORT_IN_PROGRESS";

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        Cursor updateCursor = getUpdateCursor(contentResolver, Table.CHECK_RESULT, CheckResultSchema.CONTENT_URI, null);
        if (updateCursor != null && updateCursor.getCount() > 0) {
            ArrayList<ContentProviderOperation> batch = new ArrayList<>();
            while (updateCursor.moveToNext()) {
                CheckResult checkResult = new CheckResult();
                checkResult.createFromCursor(updateCursor);
                Response<CheckOutcome> response = TriageSymptomsPost.newTriageSymptomsPost(checkResult).execute();
                if (response.getStatus() == 200) {
                    batch.add(ContentProviderOperation
                            .newDelete(CheckResultSchema.CONTENT_URI)
                            .withSelection(
                                    CheckResultSchema.Columns._ID.getName() + "=?",
                                    new String[]{String.valueOf(updateCursor.getInt(CheckResultSchema.Query._ID))})
                            .build());
                }
            }
            DatabaseOperationUtils.applyBatch(contentResolver, batch);
            updateCursor.close();
        }
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        TriageSyncService.startSync();
    }
}
