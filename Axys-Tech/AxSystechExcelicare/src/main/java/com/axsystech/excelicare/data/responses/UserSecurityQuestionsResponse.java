package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 04/09/15.
 */
public class UserSecurityQuestionsResponse extends BaseResponse implements Serializable {

    // {"Status":"Success.","data":{"Question1":"120092","Question2":"120087"},"LMD":null,"Reason":null}

    @SerializedName("data")
    private DataObject dataObject;

    @SerializedName("LMD")
    private String LMD;

    public DataObject getDataObject() {
        return dataObject;
    }

    public String getLMD() {
        return LMD;
    }

    public class DataObject implements Serializable {

        @SerializedName("Question1")
        private String question1;

        @SerializedName("Question2")
        private String question2;

        public String getQuestion1Text() {
            return question1Text;
        }

        public void setQuestion1Text(String question1Text) {
            this.question1Text = question1Text;
        }

        @SerializedName("Question1Text")
        private String question1Text;

        public String getQuestion2Text() {
            return question2Text;
        }

        public void setGetQuestion2Text(String question2Text) {
            this.question2Text = question2Text;
        }

        @SerializedName("Question2Text")
        private String question2Text;

        public String getQuestion1() {
            return question1;
        }

        public String getQuestion2() {
            return question2;
        }
    }
}
