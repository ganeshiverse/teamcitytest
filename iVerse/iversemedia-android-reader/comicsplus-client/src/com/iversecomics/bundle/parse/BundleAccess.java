package com.iversecomics.bundle.parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.iversecomics.bundle.BundleReadException;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.io.RandomAccessFileInputStream;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class BundleAccess extends RandomAccessFile {
    private final static Logger LOG = LoggerFactory.getLogger(BundleAccess.class);
    private static final int    MAX_IMAGE_SIZE = 2000000;

    /** The initial 6 bytes of the file to detect a proper Comic Bundle */
    private static final String MAGIC          = "IVERSE";

    public BundleAccess(File file, String mode) throws FileNotFoundException {
        super(file, mode);
    }

    public void fileSeek(long newPosition) throws IOException {
        if (newPosition < 0) {
            throw new IllegalArgumentException("Cannot seek past begining <" + newPosition + ">");
        }
        if (newPosition > super.length()) {
            throw new IllegalArgumentException("Cannot seek past end <" + newPosition + ">");
        }
        super.seek(newPosition);
    }

    public InputStream getRawImageStream(long position) throws IOException {
        super.seek(position);
        long length = super.readLong();
        LOG.debug("getRawImageStream(0x%x) len:%,d", position, length);
        if (length > MAX_IMAGE_SIZE) {
            throw new IOException(String.format("Excessive image length %,d", length));
        }
        return new RandomAccessFileInputStream(this, length);
    }

    public byte[] readChecksum() throws IOException {
        LOG.debug("Checksum");
        int len = super.readShort();
        byte buf[] = new byte[len];
        super.readFully(buf, 0, len);
        return buf;
    }

    public Calendar readDate() throws IOException {
        LOG.debug("Read Date");
        String rawDate = super.readUTF();
        String pattern = "yyyy-MM-dd";
        Locale locale = Locale.US;
        SimpleDateFormat format = new SimpleDateFormat(pattern, locale);
        Calendar cal = Calendar.getInstance(locale);
        try {
            cal.setTime(format.parse(rawDate));
            return cal;
        } catch (ParseException e) {
            throw new BundleReadException("Invalid date: unable to parse <" + rawDate + "> as pattern <" + pattern
                    + ">", e);
        }
    }

    public String readEncryptedString() throws IOException, GeneralSecurityException {
        int len = super.readShort();
        byte buf[] = new byte[len];
        super.readFully(buf, 0, len);
        return new String(buf, "UTF-8");
    }

    public long readFileOffsets() throws IOException {
        long offset = super.getFilePointer(); // remember this offset
        long len = super.readLong();
        LOG.debug("Read File Offsets: offset=%s len=%d%n", offset, len);
        fileSeek(len + super.getFilePointer()); // skip file content
        return offset; // return initial offset
    }

    public short readHeader() throws IOException {
        // The magic bytes
        byte magic[] = new byte[MAGIC.length()];
        super.readFully(magic);
        String actualMagic = new String(magic);
        if (!MAGIC.equals(actualMagic)) {
            throw new BundleReadException("Invalid ComicBundle header: <" + actualMagic + ">");
        }

        return super.readShort();
    }

    public void copyHeadersAndAddEmail(BundleAccess orig) throws IOException {
        byte magic[] = new byte[MAGIC.length()];
        orig.readFully(magic);
        write(magic);
        writeShort(orig.readShort());
        
    	String emailAddress = IverseApplication.getApplication().getOwnership().getEmailAddress();
    	writeShort(AbstractParseBundle.OWNER_EMAIL);
    	writeUTF(emailAddress);
    	
    	//copy the rest of the file
    	FileChannel sourceChannel = orig.getChannel();
    	FileChannel destChannel = getChannel();
    	
    	destChannel.transferFrom(sourceChannel, getFilePointer() , sourceChannel.size());
        seek(0);
    }
    
    
    public long readImageOffset() throws IOException {
        LOG.debug("Read Image Offset");
        long offset = super.getFilePointer(); // remember this offset
        long len = super.readLong();
        fileSeek(len + super.getFilePointer()); // skip image content
        return offset; // return initial offset
    }

    public String readString() throws IOException {
        LOG.debug("Read String");
        return super.readUTF();
    }
}
