package com.babylon.utils;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A request for retrieving a {@link String} response body at a given URL
 */
public class SecureStringRequest extends StringRequest
{
    private String mUrl;
    private String mRequestMethod;

	public SecureStringRequest(int method, String url, Listener<String> listener,
                               ErrorListener errorListener)
	{
        super(method, url, listener, errorListener);
        mUrl = url;
	}

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError
    {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
        headers.put("Content-Type", "application/json");

        return headers;
    }
}