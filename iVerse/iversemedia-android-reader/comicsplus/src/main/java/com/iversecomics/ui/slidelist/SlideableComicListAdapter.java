package com.iversecomics.ui.slidelist;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.ui.widget.AspectConstrainedFrame;

/**
 */
public abstract class SlideableComicListAdapter extends CursorAdapter {
    protected final Context context;
    protected final int viewId;
    protected LayoutInflater mInflater;
    protected MyComicsModel mMyComicsModel;
    protected float maxComicHeight = -1;


    public SlideableComicListAdapter(Context context, Cursor c, int viewId) {
        super(context, c, true);
        this.context = context.getApplicationContext();
        this.viewId = viewId;
        mInflater = LayoutInflater.from(context);
        mMyComicsModel = new MyComicsModel(context);
        maxComicHeight = calculateHeight();
    }

    private float calculateHeight() {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (float) (0.65 * Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels) * 16.0 / 9);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mInflater.inflate(viewId, parent, false);
        AspectConstrainedFrame frame = (AspectConstrainedFrame) view.findViewById(R.id.constraining_frame);
        if (frame != null) {
            frame.setMaxHeight(maxComicHeight);
        }
        return view;
    }


}
