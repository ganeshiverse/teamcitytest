package com.babylon.fragment.shop;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.shop.AddressListAdapter;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Kit.Address;
import com.babylon.model.Kit.SdkAddress;
import com.babylon.model.Region;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.CommonHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.view.TopBarInterface;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class AddressListFragment extends BaseFragment  {

    public static final String TAG = AddressListFragment.class.getSimpleName();
    private static final String ns = null;

    public interface AddressFragmentListener {
        public void onAdressItemClicked(Address address);

    }

    private ListView listView;
    private AddressListAdapter adapter;
    private AddressFragmentListener listener;
    private SdkAddress address;
    private List<Address> addressList;
    private EditText edtPostcode,edtStreet,edtTown,edtCounty,edtAddPostcode;
    Button btnSearch,btnConfirmAddress;
    private static final String ADDRESS_TAG="Address1";
    private static final String TOWN_TAG="Town";
    private static final String COUNTY_TAG="County";
    private static final String POSTCODE_TAG="Postcode";
    private static final String PREMISE_DATA_TAG="PremiseData";
    private static final String ERROR_MSG_TAG="ErrorMessage";
    private static final String ERROR_CODE_TAG="ErrorNumber";
    private static  final String UK_CODE="GBR";
    private String errormessage;
    private LinearLayout llyoutAddAddress;
    private RelativeLayout llyoutSearchAddress;

    public static AddressListFragment newInstance() {
        AddressListFragment fragment = new AddressListFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_address_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getString(R.string.address_fragment_title));
        edtPostcode=(EditText)view.findViewById(R.id.edtPassword);
        edtStreet=(EditText)view.findViewById(R.id.edtStreet);
        edtCounty=(EditText)view.findViewById(R.id.edtCounty);
        edtAddPostcode=(EditText)view.findViewById(R.id.edtPostcode);
        edtTown=(EditText)view.findViewById(R.id.edTown);
        btnSearch=(Button)view.findViewById(R.id.btnSearch);
        listView = (ListView)view.findViewById(android.R.id.list);
        btnConfirmAddress=(Button)view.findViewById(R.id.btnConfirm);
        llyoutSearchAddress=(RelativeLayout)view.findViewById(R.id.lytSearchAddress);
        llyoutAddAddress=(LinearLayout)view.findViewById(R.id.lytAddAddress);

        if(!TextUtils.isEmpty(edtPostcode.getText())){
            getAddress(edtPostcode.getText().toString());
        }

        for(Region region: PreferencesManager.getInstance().getRegions())
        {
            if(region.getId()==App.getInstance().getPatient().getRegionId()) {
                if(region.getCountryCode().compareToIgnoreCase("GBR")!=0)
                {
                    llyoutSearchAddress.setVisibility(View.GONE);
                    llyoutAddAddress.setVisibility(View.VISIBLE);
                }
                else
                {
                    llyoutSearchAddress.setVisibility(View.VISIBLE);
                    llyoutAddAddress.setVisibility(View.GONE);
                }
            }
        }

        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (listener != null) {
                    listener.onAdressItemClicked(adapter.getItem(position));
                }
            }
        });



// Documentation at: http://www.postcodesoftware.co.uk/sdk.htm#13
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(edtPostcode.getText())) {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    getAddress(edtPostcode.getText().toString());
                }
            }
        });

        btnConfirmAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (listener != null) {

                    if(TextUtils.isEmpty(edtStreet.getText()) || TextUtils.isEmpty(edtCounty.getText())){

                        Toast.makeText(getActivity().getApplicationContext(),
                                getString(R.string.address_manadatoy_toast), Toast.LENGTH_LONG).show();

                    }
                    else{

                        Address address=new Address();
                        address.setFirstLine(edtStreet.getText().toString());
                        address.setSecondLine(edtTown.getText().toString());
                        address.setThirdLine(edtCounty.getText().toString());
                        address.setPostCode(edtAddPostcode.getText().toString());
                        listener.onAdressItemClicked(address);

                    }

                }


            }
        });

    }


    private void getAddress(String postCode) {

        String uri= "http://ws1.postcodesoftware.co.uk/lookup" +
                ".asmx/getAddress?account="+getString(R.string.postcode_sdk_account)
                +"&password="+getString(R.string.postcode_sdk_password)+"&postcode="+postCode;
        uri= uri.replace(" ", "%20");
        StringRequest strReq = new StringRequest(com.android.volley.Request.Method.GET,
                uri, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d(TAG, response.toString());
                parseAddress(response);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        });

        if (App.getInstance().isConnection()){
            strReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(strReq);
        }


    }

    public SdkAddress parse(String response) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(new StringReader(response));
        parser.nextTag();
        return readEntry(parser);

    }


    private SdkAddress readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        String Address1 = null;
        String Town = null;
        String County = null;
        String Postcode = null;
        String PremiseData = null;
        String ErrorMessage=null;
        int ErrorNumber=0;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch(name)
            {
                case ADDRESS_TAG: Address1 = readTag(parser, ADDRESS_TAG);
                    break;
                case TOWN_TAG:  Town = readTag(parser, TOWN_TAG);
                    break;
                case COUNTY_TAG:  County = readTag(parser, COUNTY_TAG);
                    break;
                case POSTCODE_TAG:  Postcode = readTag(parser, POSTCODE_TAG);
                    break;
                case PREMISE_DATA_TAG:  PremiseData = readTag(parser, PREMISE_DATA_TAG);
                    break;
                case ERROR_MSG_TAG:  ErrorMessage = readTag(parser,ERROR_MSG_TAG );
                    break;
                case ERROR_CODE_TAG:  ErrorNumber = readIntTag(parser, ERROR_CODE_TAG);
                    break;
                default:skip(parser);
                    break;

            }

        }
        return new SdkAddress(Address1, Town, County,Postcode,PremiseData,ErrorNumber,ErrorMessage);
    }


    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


    private String readTag(XmlPullParser parser,String pTag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, pTag);
        String value = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, pTag);
        return value;
    }

    private int readIntTag(XmlPullParser parser,String pTag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, pTag);
        int value= readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, pTag);
        return value;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private int readInt(XmlPullParser parser) throws IOException, XmlPullParserException {
        int result=0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.valueOf(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (AddressFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }




    private void parseAddress(final String response) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    addressList = new ArrayList<Address>();
                    address = parse(response);
                    if(!TextUtils.isEmpty(address.PremiseData)) {

                        String[] address_parts = address.PremiseData.split(";");


                        for (String part : address_parts) {

                            part = part.replace("||", "|");
                            part = part.replace("|", ", ");
                            part = part.replaceFirst(",", "");
                            part = part.replace("/", ", ");
                            part=part.trim();
                            Address each_address=new Address();
                            each_address.setFirstLine(part.concat(" " + address.Address1));
                            each_address.setSecondLine(address.Town);
                            each_address.setThirdLine(address.County);
                            each_address.setPostCode(address.Postcode);
                           /* part = part.concat(" " + address.Address1 + ", " +
                                    "" + address.Town + ", " + address.County +
                                    ", " +
                                    "" + address.Postcode);*/
                            // part = part.replace("/", ", ");
                            addressList.add(each_address);

                        }
                    }

                }
                catch (XmlPullParserException ex){
                    Log.d(TAG,ex.toString());
                }
                catch (IOException ex){
                    Log.d(TAG,ex.toString());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {


                if (addressList != null && addressList.size()>0 ) {
                    setAddressListAdapter();
                }
                else if(!TextUtils.isEmpty(address.ErrorMessage)){

                    if(address.ErrorMessage.compareToIgnoreCase("Credits expired")==0) {
                        errormessage = getString(R.string.edt_postcode_credit_msg);
                    }
                    else {
                        errormessage=getString(R.string.edt_postcode_error_msg);
                    }
                    if (addressList != null ) {
                        setAddressListAdapter();
                    }
                    AlertDialogHelper.getInstance().showMessage(getActivity(),errormessage);
                  /*  Toast.makeText(getActivity().getApplicationContext(), errormessage
                            , Toast.LENGTH_LONG).show();*/
                }

            }
        }.execute();
    }

    private void setAddressListAdapter() {
        adapter = new AddressListAdapter(getActivity(), addressList);
        listView.setAdapter(adapter);
    }
}