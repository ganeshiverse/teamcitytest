package com.iversecomics.util.text;

public final class StringUtils {

    private StringUtils() {
        /* prevent instantiation */
    }

    public static String join(String items[], String delimiter) {
        if ((items == null) || (items.length <= 0)) {
            return "";
        }

        StringBuilder buffer = new StringBuilder();

        boolean needDelim = false;
        for (String str : items) {
            if (needDelim) {
                buffer.append(delimiter);
            }
            buffer.append(str);
            needDelim = true;
        }

        return buffer.toString();
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0 || s.equals("null");
    }
}
