package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Date: 07.05.14
 * Time: 12:37
 *
 * @author SomeswarReddy
 */
public class LoginResponse extends BaseResponse {

    @SerializedName("data")
    private LoginDataObject loginDataObject;

    public LoginDataObject getLoginDataObject() {
        return loginDataObject;
    }

    public class LoginDataObject {
        @SerializedName("LoginUser")
        private LoginUserResponse mLoginUser;

        @SerializedName("UserPreferences")
        private UserPreferencesResponse mUserPreferences;

        public LoginUserResponse getLoginUser() {
            return mLoginUser;
        }

        public UserPreferencesResponse getUserPreferences() {
            return mUserPreferences;
        }
    }

    public class LoginUserResponse extends BaseResponse {

        @SerializedName("data")
        private UserDataObject userDataObject;

        public UserDataObject getUserDataObject() {
            return userDataObject;
        }

        public class UserDataObject {
            @SerializedName("EcUserID")
            private long ecUserID;
            @SerializedName("Forename")
            private String forename;
            @SerializedName("patientID")
            private String patientID;
            @SerializedName("Photo")
            private String photo;
            @SerializedName("SiteID")
            private String siteID;
            @SerializedName("Sitename")
            private String sitename;
            @SerializedName("Surname")
            private String surname;
            @SerializedName("token")
            public String token;
            @SerializedName("UserDeviceID")
            private String userDeviceID;
            @SerializedName("loginName")
            private String loginName;

            private String email;

            public long getEcUserID() {
                return ecUserID;
            }

            public String getForename() {
                return forename;
            }

            public String getPatientID() {
                return patientID;
            }

            public String getPhoto() {
                return photo;
            }

            public String getSiteID() {
                return siteID;
            }

            public String getSitename() {
                return sitename;
            }

            public String getSurname() {
                return surname;
            }

            public String getToken() {
                return token;
            }

            public String getUserDeviceID() {
                return userDeviceID;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getLoginName() {
                return loginName;
            }
        }
    }

    public class UserPreferencesResponse extends BaseResponse {

    }

}
