package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Babylon on 15/05/2015.
 */
public class Address {


    @Expose
    private int id;
    @SerializedName("first_line")
    @Expose
    private String firstLine;
    @SerializedName("second_line")
    @Expose
    private String secondLine;
    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("third_line")
    @Expose
    private String thirdLine;

    @SerializedName("post_code")
    @Expose
    private String postCode;

    /**
     *
     * @return
     * The firstLine
     */
    public String getFirstLine() {
        return firstLine;
    }

    /**
     *
     * @param firstLine
     * The first_line
     */
    public void setFirstLine(String firstLine) {
        this.firstLine = firstLine;
    }

    /**
     *
     * @return
     * The secondLine
     */
    public String getSecondLine() {
        return secondLine;
    }

    /**
     *
     * @param secondLine
     * The second_line
     */
    public void setSecondLine(String secondLine) {
        this.secondLine = secondLine;
    }

    /**
     *
     * @return
     * The thirdLine
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param thirdLine
     * The third_line
     */
    public void setCity(String thirdLine) {
        this.city = thirdLine;
    }

    /**
     *
     * @return
     * The postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     *
     * @param postCode
     * The post_code
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public int getId(){return  id;}

    public  void setId(int id){this.id=id;}

    public String getThirdLine() {
        return thirdLine;
    }


    public void setThirdLine(String thirdLine) {
        this.thirdLine = thirdLine;
    }

}