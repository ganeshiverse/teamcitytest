package com.babylon.utils.samsung.health;

import android.support.annotation.Nullable;

import com.babylon.enums.SamsungHealthPermission;
import com.babylon.utils.DateUtils;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import java.util.Date;

public class WeightReporter extends BaseReporter {

    @Override
    protected String getDataType() {
        return SamsungHealthPermission.WEIGHT_READ.getName();
    }

    @Override
    protected void readData(@Nullable String dataTypeName) {
        long dateFromInMillis = getSyncDateFromInMillis();
        long minDayStartMills = DateUtils.getDayStartMills(new Date(),
                - DateUtils.DAYS_IN_YEAR);
        long endOfCurrentDate = DateUtils.getDayStartMills(1);

        HealthDataResolver.Filter filter = HealthDataResolver.Filter.and(
                HealthDataResolver.Filter.greaterThanEquals(HealthConstants.Weight.START_TIME,
                        minDayStartMills),
                HealthDataResolver.Filter.lessThan(HealthConstants.Weight.START_TIME,
                        System.currentTimeMillis()),
                HealthDataResolver.Filter.greaterThanEquals(HealthConstants.Weight.CREATE_TIME,
                        dateFromInMillis),
                HealthDataResolver.Filter.lessThan(HealthConstants.StepCount.CREATE_TIME,
                        endOfCurrentDate));

        HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                .setDataType(getDataType())
                .setProperties(new String[] {
                        HealthConstants.Weight.WEIGHT,
                        HealthConstants.Weight.START_TIME })
                .setFilter(filter)
                .setSort(HealthConstants.Weight.START_TIME, HealthDataResolver.SortOrder.DESC)
                .build();

        requestData(request, resultListener);
    }

    private final HealthResultHolder.ResultListener<ReadResult> resultListener =
            new HealthResultHolder.ResultListener<ReadResult>() {
                @Override
                public void onResult(ReadResult result) {
                    new WeightProcessor().execute(result);
                }
            };
}
