package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vvydesai on 8/31/2015.
 */
public class AppointmentListResponse extends BaseResponse implements Serializable {

    @SerializedName("DatacardId")
    private String datacardId;

    @SerializedName("Panel")
    private Panel panel;

    public String getDatacardId() {
        return datacardId;
    }

    public Panel getPanel() {
        return panel;
    }

    public class Panel implements Serializable {

        @SerializedName("Id")
        private String id;

        @SerializedName("Name")
        private String name;

        @SerializedName("RowCount")
        private String rowCount;

        @SerializedName("data")
        private ArrayList<AppointmentDetails> appointmentDetailsAL;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getRowCount() {
            return rowCount;
        }

        public ArrayList<AppointmentDetails> getAppointmentDetailsAL() {
            return appointmentDetailsAL;
        }
    }

    public class AppointmentDetails implements Serializable {

        @SerializedName("AppointmentDateTime")
        private String appointmentDateTime;

        @SerializedName("AppointmentRequestedDatetime")
        private String appointmentRequestedDatetime;

        @SerializedName("AppointmentStartTime")
        private String appointmentStartTime;

        @SerializedName("AppointmentType")
        private String appointmentType;

        @SerializedName("ClinicName")
        private String clinicName;

        @SerializedName("Doctor")
        private String doctor;

        @SerializedName("ID")
        private String ID;

        @SerializedName("PatientName")
        private String patientName;

        @SerializedName("Reason")
        private String reason;

        @SerializedName("Speciality")
        private String speciality;

        public String getAppointmentDateTime() {
            return appointmentDateTime;
        }

        public String getAppointmentRequestedDatetime() {
            return appointmentRequestedDatetime;
        }

        public String getAppointmentStartTime() {
            return appointmentStartTime;
        }

        public String getAppointmentType() {
            return appointmentType;
        }

        public String getClinicName() {
            return clinicName;
        }

        public String getDoctor() {
            return doctor;
        }

        public String getID() {
            return ID;
        }

        public String getPatientName() {
            return patientName;
        }

        public String getReason() {
            return reason;
        }

        public String getSpeciality() {
            return speciality;
        }
    }
}
