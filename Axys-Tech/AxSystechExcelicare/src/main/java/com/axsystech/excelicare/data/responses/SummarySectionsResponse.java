package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 07/10/15.
 */
public class SummarySectionsResponse extends BaseResponse implements Serializable {

    @SerializedName("DatacardId")
    private String datacardId;

    @SerializedName("Panels")
    private String panels;

    @SerializedName("Panel")
    private SummaryPanelObject panelObject;

    public String getDatacardId() {
        return datacardId;
    }

    public String getPanels() {
        return panels;
    }

    public SummaryPanelObject getPanelObject() {
        return panelObject;
    }

    public class SummaryPanelObject implements Serializable {

        @SerializedName("Id")
        private String id;

        @SerializedName("Name")
        private String name;

        @SerializedName("RowCount")
        private int rowCount;

        @SerializedName("Token")
        private String token;

        @SerializedName("data")
        private ArrayList<SummaryPanelDetailsObj> summaryPanelDetailsObjAL;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getRowCount() {
            return rowCount;
        }

        public String getToken() {
            return token;
        }

        public ArrayList<SummaryPanelDetailsObj> getSummaryPanelDetailsObjAL() {
            return summaryPanelDetailsObjAL;
        }
    }

    public class SummaryPanelDetailsObj implements Serializable {

        @SerializedName("ComprehensiveHealthSummaryList")
        private String comprehensiveHealthSummaryList;

        @SerializedName("DashboardId")
        private String dashboardId;

        @SerializedName("PanelID")
        private String panelID;

        private boolean isSelected;

        public String getComprehensiveHealthSummaryList() {
            return comprehensiveHealthSummaryList;
        }

        public String getDashboardId() {
            return dashboardId;
        }

        public String getPanelID() {
            return panelID;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setIsSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }
    }
}
