package com.iversecomics.client.downloads.internal;


import org.apache.http.StatusLine;

/**
 * The remote service is unavailable.
 */
public class ServiceUnavailableException extends DownloadException {
    private static final long serialVersionUID = 1194781373270431663L;
    /**
     * Optional value possibly passed by server indicating when to retry
     * the failed request after.
     */
    private long              retryAfter       = -1;

    public ServiceUnavailableException(StatusLine statusLine) {
        super(statusLine);
    }

    public ServiceUnavailableException(StatusLine statusLine, long retryAfter) {
        super(statusLine);
        this.retryAfter = retryAfter;
    }

    public long getRetryAfter() {
        return retryAfter;
    }
}
