package com.babylon.model.payments;

import com.babylon.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PaymentError implements Serializable {

    @BaseModel.SkipFieldInContentValues
    private static final long serialVersionUID = 1713L;

    @SerializedName("errors")
    private Error error;

    @SerializedName("user_message")
    private String userMessage;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public class Error implements Serializable {

        @BaseModel.SkipFieldInContentValues
        private static final long serialVersionUID = 8539L;

        @SerializedName("base")
        private List<String> baseErrors;

        public List<String> getBaseErrors() {
            return baseErrors;
        }

        public void setBaseErrors(List<String> baseErrors) {
            this.baseErrors = baseErrors;
        }
    }
}
