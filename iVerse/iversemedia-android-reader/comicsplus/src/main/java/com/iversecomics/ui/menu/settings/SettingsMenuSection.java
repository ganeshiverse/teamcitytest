package com.iversecomics.ui.menu.settings;

import android.util.Pair;

import java.util.Collections;
import java.util.List;

/**
 * Created by jeffaoliver on 11/15/13.
 */
public class SettingsMenuSection extends Pair<String, List<SettingsMenuItem>> {

    public final String title;
    public final List<SettingsMenuItem> menuItems;

    /**
     * Constructor for a SettingsMenuSection.
     *
     * @param sectionTitle the title of the section
     * @param menuItemList list of MenuItem for this section
     */
    public SettingsMenuSection(String sectionTitle, List<SettingsMenuItem> menuItemList) {
        super(sectionTitle, menuItemList);
        title = sectionTitle;
        if (menuItemList == null) {
            menuItems = Collections.emptyList();
        } else {
            menuItems = menuItemList;
        }
    }

    public int size() {
        return menuItems.size();
    }
}
