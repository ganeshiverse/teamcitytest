package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.SaveActionResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by SomeswarReddy on 7/13/2015.
 */
public class SaveCardDetailsLoader extends DataAsyncTaskLibLoader<SaveActionResponse> {

    private String clientUrl;
    private String saveUrl;
    private String token;
    private String returnFormat;
    private String strParamData;

    public SaveCardDetailsLoader(Context context, String saveUrl, String token, String returnFormat, String strParamData) {
        super(context);
        this.clientUrl = clientUrl;
        this.saveUrl = saveUrl;
        this.token = token;
        this.returnFormat = returnFormat;
        this.strParamData = strParamData;
    }

    @Override
    protected SaveActionResponse performLoad() throws Exception {
        return ServerMethods.getInstance().saveCardDetails(saveUrl, token, returnFormat, strParamData);
    }
}
