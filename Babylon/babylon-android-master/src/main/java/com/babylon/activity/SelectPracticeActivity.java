package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.api.request.JoinPracticePost;
import com.babylon.api.request.PracticesGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.model.Patient;
import com.babylon.model.Practice;
import com.babylon.view.TopBarInterface;
import com.babylonpartners.babylon.HomePageActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.*;
import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SelectPracticeActivity extends BaseActivity implements View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap map;
    private View mapView;
    private Map<Marker, Practice> markers = new HashMap<Marker, Practice>();
    private Practice selectedPractice = null;
    private LinearLayout unpartneredInfoWindow;
    private TextView addressField;
    private TextView telField;

    private static final int ON_CONFIRM_PRACTICE_ACTIVITY_RESULT = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_practice);

        setProgressBarVisible(true);
        loadItems();
       // setRightImageButtonVisible(false );

        int TIME_OUT=3000;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                setProgressBarVisible(false);
                Boolean allowSkip = getIntent().getBooleanExtra(Keys.KEY_ALLOW_SKIP_MAP, false);
                initViews(allowSkip);

            }
        }, TIME_OUT);



    }

    private void loadItems() {
       /* Boolean allowSkip = getIntent().getBooleanExtra(Keys.KEY_ALLOW_SKIP_MAP, false);
        initViews(allowSkip);*/

        unpartneredInfoWindow = (LinearLayout) findViewById(R.id.unpartnered_info_window);
        unpartneredInfoWindow.setVisibility(View.INVISIBLE);

        addressField = (TextView) findViewById(R.id.marker_practice_address);
        telField = (TextView) findViewById(R.id.marker_practice_tel);
        telField.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        telField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(String.format("tel:%s", selectedPractice.getTelNumber())));
                startActivity(intent);
            }
        });

        // Get a handle to the Map Fragment
        mapView = findViewById(R.id.map);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMyLocationEnabled(true);
        map.setOnMarkerClickListener(this);
        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (unpartneredInfoWindow.getVisibility() == View.VISIBLE) {
                    unpartneredInfoWindow.setVisibility(View.INVISIBLE);
                }
            }
        });
        Location currentLocation = App.getInstance().getLocation();
        if (currentLocation != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(),
                    currentLocation.getLongitude()), 7.0f));
        }
        loadPractices();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ON_CONFIRM_PRACTICE_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                onPracticeSelectionConfirmed(selectedPractice);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (unpartneredInfoWindow.getVisibility() == View.VISIBLE) {
            closeUnpartneredPracticeWindow();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.topbar_right_text_button) {
            onSkipButtonPressed();
        }
    }

    private void initViews(Boolean allowSkip) {
        initializeTopBar();
        if(allowSkip) {
            setRightImageButtonVisible(false);
            setRightButton(this, R.string.select_practice_skip);
        }

        setTitle(getString(R.string.select_practice_title));
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PURPLE_THEME;
    }


    void addPracticeMarker(LatLng position, Practice practice) {
        Marker marker = map.addMarker(new MarkerOptions()
                .position(position)
                .title(practice.getName()));
        if (practice.isBabylonPartner()) {
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.btn_babylon_pin));
        } else {
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.btn_neutral_pin));
        }
        markers.put(marker, practice);
    }

    void fitMarkerBounds() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        Iterator it = markers.keySet().iterator();
        while (it.hasNext()) {
            Marker marker = (Marker) it.next();
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int padding = 0;
        CameraUpdate cam = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.animateCamera(cam);
    }

    void onPracticeSelectionConfirmed(final Practice practice) {
        JoinPracticePost joinPracticePost = JoinPracticePost.newAlliantsJoinPracticePost(App
                .getInstance().getPatient().getRegionId(), practice.getId());
        joinPracticePost.execute(this, new RequestListener<Patient>() {
            @Override
            public void onRequestComplete(Response<Patient> response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    processResponse(response);

                    Intent result = new Intent();
                    result.putExtra("practice_id", practice.getId());
                    setResult(Activity.RESULT_OK, result);
                    finish();
                } else {
                    Toast.makeText(App.getInstance(), getString(R.string.select_practice_error),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void processResponse(Response<Patient> response) {
        if (response.getData() != null) {
            Patient patient = response.getData();
            App.getInstance().setPatient(patient);
        }
    }

    private void loadPractices() {
        PracticesGet practicesGet = PracticesGet.newAlliantsPracticesGet(App.getInstance().getPatient
                ().getRegionId());
        practicesGet.execute(this, new RequestListener<Practice[]>() {
            @Override
            public void onRequestComplete(Response<Practice[]> response) {
                if (response.getStatus() == HttpStatus.SC_OK) {
                    if (response.getData() != null) {
                        Practice[] practices = response.getData();
                        if (practices.length != 0) {
                            for (Practice practice : practices) {
                                LatLng latLng = new LatLng(practice.getLatitude(), practice.getLongitude());
                                addPracticeMarker(latLng, practice);
                            }
                            fitMarkerBounds();
                        }
                    }
                } else {
                    Toast.makeText(App.getInstance(), getString(R.string.select_practice_load_error),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (unpartneredInfoWindow.getVisibility() == View.VISIBLE) {
            return true;
        }

        selectedPractice = markers.get(marker);
        if (selectedPractice.isBabylonPartner()) {
            Intent confirmPracticeIntent = new Intent(this, ConfirmPracticeActivity.class);
            Bundle practiceData = new Bundle();
            practiceData.putString(Keys.PRACTICE_NAME_KEY, selectedPractice.getName());
            practiceData.putString(Keys.PRACTICE_ADDRESS_KEY, selectedPractice.getAddress());
            confirmPracticeIntent.putExtras(practiceData);
            startActivityForResult(confirmPracticeIntent, ON_CONFIRM_PRACTICE_ACTIVITY_RESULT);
            return true;
        } else {
            layoutInfoWindow(marker);
            unpartneredInfoWindow.setVisibility(View.VISIBLE);
            mapView.setEnabled(false);
            map.getUiSettings().setAllGesturesEnabled(false);
            map.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            return true;
        }
    }

    public void layoutInfoWindow(Marker marker) {
        Practice practice = getPractice(marker);
        if (practice != null && !practice.isBabylonPartner()) {
            addressField.setText(practice.getAddress().replace("\\n", "\n"));
            telField.setText(practice.getTelNumber());
        }
    }

    public void onSkipButtonPressed() {
       // setResult(Activity.RESULT_OK);
        Intent intent = new Intent(SelectPracticeActivity.this, HomePageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                .FLAG_ACTIVITY_CLEAR_TOP);

        Bundle  arguments = new Bundle();

        arguments.putBoolean(Keys.SHOULD_START_HOME_ACTIVITY, true);
        intent.putExtras(arguments);
        startActivity(intent);
        finish();
    }

    public Practice getPractice(Marker marker) {
        return markers.get(marker);
    }

    private void closeUnpartneredPracticeWindow() {
        map.getUiSettings().setAllGesturesEnabled(true);
        unpartneredInfoWindow.setVisibility(View.INVISIBLE);
        mapView.setEnabled(true);
    }

    public void onUnpartneredPracticeDoneClicked(View view) {
        closeUnpartneredPracticeWindow();
    }

    public void onUnpartneredPracticeCloseClicked(View view) {
        closeUnpartneredPracticeWindow();
    }
}
