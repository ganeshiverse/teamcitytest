package com.iversecomics.client.store.ui;


public abstract class AbstractComicStoreActivityTestCase extends AbstractStoreActivityTestCase<ComicStoreActivity> {
    public AbstractComicStoreActivityTestCase() {
        super(ComicStoreActivity.class);
    }
}
