package com.iversecomics.client.refresh;

import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.util.Time;

public class FreshnessTest extends AbstractStubActivityTestCase {
    @MediumTest
    public void testClearExpired() {
        startStubActivity();
        Freshness fresh = new Freshness(getActivity());

        // Add some keys that will expire in an hour
        fresh.setExpiresAfter("test.one", Time.HOUR);
        fresh.setExpiresAfter("test.two", Time.HOUR);
        fresh.setExpiresAfter("test.three", Time.HOUR);
        fresh.setExpiresAfter("test.four", Time.HOUR);

        // Add one key that will expire quickly.
        fresh.setExpiresAfter("test.old", Time.MINUTE * 5);

        // Test that the keys are still valid.
        assertTrue("Key[test.one] is fresh", fresh.isFresh("test.one"));
        assertTrue("Key[test.two] is fresh", fresh.isFresh("test.two"));
        assertTrue("Key[test.three] is fresh", fresh.isFresh("test.three"));
        assertTrue("Key[test.four] is fresh", fresh.isFresh("test.four"));
        assertTrue("Key[test.old] is fresh", fresh.isFresh("test.old"));

        // Clear expired (pretend its 10 minutes from now)
        long now = System.currentTimeMillis();
        fresh.clearExpired(now + (Time.MINUTE * 10));

        // Test that the remaining keys are still valid.
        assertTrue("Key[test.one] is fresh", fresh.isFresh("test.one"));
        assertTrue("Key[test.two] is fresh", fresh.isFresh("test.two"));
        assertTrue("Key[test.three] is fresh", fresh.isFresh("test.three"));
        assertTrue("Key[test.four] is fresh", fresh.isFresh("test.four"));
        assertFalse("Key[test.old] is fresh", fresh.isFresh("test.old"));
    }

    @MediumTest
    public void testIsFresh() {
        startStubActivity();
        Freshness fresh = new Freshness(getActivity());

        String key = getName();
        long expiryAge = Time.HOUR;

        // Initial test on empty prefs.
        assertFalse("Empty prefs, should need a refresh", fresh.isFresh(key));

        // Update to now.
        fresh.setExpiresAfter(key, expiryAge);

        // Shouldn't need a refresh (not expired yet)
        assertTrue("Recently updated, shouldn't need refresh", fresh.isFresh(key));

        // Update to the past.
        long now = System.currentTimeMillis();
        long thepast = now - expiryAge;
        fresh.setExpiresOn(key, thepast); // set that last expired in the past

        // Test again.
        assertFalse("Updated prefs, should need a refresh", fresh.isFresh(key));
    }

    @MediumTest
    public void testIsFreshOnInitial() {
        startStubActivity();
        Freshness fresh = new Freshness(getActivity());

        assertFalse("Empty prefs, should need a refresh", fresh.isFresh("test"));
    }

}
