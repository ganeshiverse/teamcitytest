package com.iversecomics.client.refresh;


public interface IRefreshable {
    /**
     * Tests the object to know if it is fresh.
     *
     * @return true if the object has fresh data, that is not expired.
     */
    boolean isFresh(Freshness freshness);

    /**
     * Asks the object to refresh.
     *
     * @param force force the refresh even if the data is fresh, nothing happens if the data is fresh, and force is false.
     */
    void refresh(Freshness freshness, boolean force);
}
