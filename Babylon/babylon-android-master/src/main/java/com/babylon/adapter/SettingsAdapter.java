package com.babylon.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.babylon.R;
import com.babylon.model.ItemData;

import java.util.ArrayList;
import java.util.List;

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ItemViewHolder>
{
    public interface OnPropertyItemClickListener {
        View.OnClickListener getOnPropertyClickListener();
    }

    protected ImageLoader loader;
    public int mSelectedItem = -1;
    private List<ItemData> mListOfItems = new ArrayList<>();
    private Context mContext;
    private OnPropertyItemClickListener onPropertyItemClickListener;

    public SettingsAdapter(Context context, List<ItemData> usersList, OnPropertyItemClickListener onPropertyItemClickListener)
    {
        this.onPropertyItemClickListener=onPropertyItemClickListener;
        mContext=context;
        mListOfItems = usersList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        ItemViewHolder pvh = new ItemViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position)
    {
        holder.personName.setText(mListOfItems.get(position).getTitle());
        holder.llyItem.setTag(mListOfItems.get(position).getTitle());
        Resources resources = mContext.getResources();
        final int resourceId = resources.getIdentifier(mListOfItems.get(position).getImageUrl(), "drawable",
                mContext.getPackageName());
        holder.icon.setImageDrawable(resources.getDrawable(resourceId));
       /* if (onPropertyItemClickListener != null) {
            holder.cv.setOnClickListener(onPropertyItemClickListener
                    .getOnPropertyClickListener());

        }*/

    }

    @Override
    public int getItemCount()
    {
        return mListOfItems.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {
        TextView personName;
        RelativeLayout llyItem;
        ImageView icon;
       // public CardView cv;

        ItemViewHolder(View itemView)
        {
            super(itemView);
            personName = (TextView)itemView.findViewById(R.id.item_title);
            //cv = (CardView)itemView.findViewById(R.id.cv);
            llyItem=(RelativeLayout)itemView.findViewById(R.id.llyItem);
            icon=(ImageView)itemView.findViewById(R.id.item_icon);
            if (onPropertyItemClickListener != null) {
                llyItem.setOnClickListener(onPropertyItemClickListener
                        .getOnPropertyClickListener());
            }
               /* cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toast.makeText(mContext, mListOfItems.get(getPosition()).getTitle(),Toast.LENGTH_SHORT)
                        // .show();
                        if (onPropertyItemClickListener != null) {

                                onPropertyItemClickListener
                                        .getOnPropertyClickListener();

                        }
                    }

                });*/

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}