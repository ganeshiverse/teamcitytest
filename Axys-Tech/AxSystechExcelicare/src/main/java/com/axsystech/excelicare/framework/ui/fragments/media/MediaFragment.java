package com.axsystech.excelicare.framework.ui.fragments.media;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AttachmentDetails;
import com.axsystech.excelicare.data.responses.GetFoldersListResponse;
import com.axsystech.excelicare.data.responses.GetMediaItemResponse;
import com.axsystech.excelicare.data.responses.HelpResponse;
import com.axsystech.excelicare.data.responses.ListMediaItemThumbNailsResponse;
import com.axsystech.excelicare.data.responses.UploadMediaItemResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.activities.MediaDetailsActivity;
import com.axsystech.excelicare.framework.ui.adapters.MediaListAdapter;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.audio.AudioRecordingFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.models.MediaDetailsViewType;
import com.axsystech.excelicare.framework.ui.fragments.media.services.UploadMediaResultReceiver;
import com.axsystech.excelicare.framework.ui.fragments.media.services.UploadMediaService;
import com.axsystech.excelicare.loaders.GetFolderListLoader;
import com.axsystech.excelicare.loaders.GetMediaItemsFromDBLoader;
import com.axsystech.excelicare.loaders.HelpLoader;
import com.axsystech.excelicare.loaders.MediaItemsThumbnailsListLoader;
import com.axsystech.excelicare.loaders.SaveMediaFoldersDBLoader;
import com.axsystech.excelicare.loaders.SaveMediaItemsDBLoader;
import com.axsystech.excelicare.loaders.UpdateMediaItemDBLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;


public class MediaFragment extends BaseFragment implements View.OnClickListener, UploadMediaResultReceiver.Receiver,
        ConfirmationDialogFragment.OnConfirmListener {

    private String TAG = MediaFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private MediaFragment mFragment;

    private static final String HELP_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.MediaFragment.HELP_LOADER";
    private static final String GET_FOLDER_LIST_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.GET_FOLDER_LIST_LOADER";
    private static final String LIST_MEDIA_ITEM_THUMBNAILS_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.LIST_MEDIA_ITEM_THUMBNAILS_LOADER";
    private static final String SAVE_MEDIA_FOLDERS_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.SAVE_MEDIA_FOLDERS_LOADER";
    private static final String SAVE_MEDIA_ITEMS_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.SAVE_MEDIA_ITEMS_LOADER";
    private static final String GET_MEDIA_ITEMS_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.GET_MEDIA_ITEMS_FROM_DB_LOADER";

    private static final String UPDATE_MEDIA_ITEM_IN_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.media.UPDATE_MEDIA_ITEM_IN_DB_LOADER";

    private static final int DELETE_MEDIA_DIALOG_ID = 107;
    private static final int UPLOAD_MEDIA_DIALOG_ID = 108;

    @InjectView(R.id.mediaListviewContainer)
    private FrameLayout mediaListviewContainer;

    @InjectView(R.id.mediaListView)
    private ListView mMediaListView;

    @InjectView(R.id.hintTextView)
    private TextView mHintTextView;

    @InjectView(R.id.singleChoiceLinearLayout)
    private LinearLayout singleChoiceLinearLayout;

    @InjectView(R.id.filter_btn)
    private ImageButton mFilterbtn;

    @InjectView(R.id.help_btn)
    private ImageButton mHelperbtn;

    @InjectView(R.id.multipeChoiceLinearLayout)
    private LinearLayout multipeChoiceLinearLayout;

    @InjectView(R.id.uploadImageView)
    private ImageView uploadImageView;

    @InjectView(R.id.shareImageView)
    private ImageView shareImageView;

    @InjectView(R.id.deleteImageView)
    private ImageView deleteImageView;

    @InjectView(R.id.itemsselected)
    private TextView itemsselected;

    @InjectSavedState
    private User mActiveUser;

   // private  TextView itemsselected;

    private ArrayList<FilterType> mFilterTypesAL;

    private FilterType mSelectedFilterType = FilterType.FILTER_ALL;

    private NavigableMap<Integer, NavigableMap<String, ArrayList<MediaItemDbModel>>> mMediaItemsTM;
    private MediaListAdapter mMediaListAdapter;

    private IntentFilter mediaIntentFilter = new IntentFilter();

    private UploadMediaResultReceiver uploadMediaResultReceiver = null;

    private boolean shouldDisplayLocalMedia = true;

    private MediaAttachmentsListener mediaAttachmentsListener;

    public MediaAttachmentsListener getMediaAttachmentsListener() {
        return mediaAttachmentsListener;
    }

    public void setMediaAttachmentsListener(MediaAttachmentsListener mediaAttachmentsListener) {
        this.mediaAttachmentsListener = mediaAttachmentsListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragament_media, container, false);

        view.setFocusableInTouchMode(true);
        view.setClickable(true);
        return view;
    }


    @Override
    protected void initActionBar() {
        if (mSelectedFilterType == FilterType.FILTER_ALL) {
            mActivity.getSupportActionBar().setTitle(getString(R.string.menu_item_media) + "-" + FilterType.FILTER_ALL.getFilterName());
        } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS) {
            mActivity.getSupportActionBar().setTitle(FilterType.FILTER_PHOTOS.getFilterName());
        } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS) {
            mActivity.getSupportActionBar().setTitle(FilterType.FILTER_VIDEOS.getFilterName());
        } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS) {
            mActivity.getSupportActionBar().setTitle("Record New Audio");
        }

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            boolean mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        mediaIntentFilter.addAction(AxSysConstants.BCR_UPDATE_MEDIA_SCREEN);
//        mediaIntentFilter.addAction(AxSysConstants.BCR_MEDIA_DELETED);

        initViews();
        readIntentArgs();

        addEventListeners();
        preLoadData();
    }

    private void initViews() {
        mFilterTypesAL = FilterType.getAllFilters(mSelectedFilterType);
    }

    private void addEventListeners() {

       // Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        //dateTextView.setTypeface(bold);
        mFilterbtn.setOnClickListener(this);
        mHelperbtn.setOnClickListener(this);

        uploadImageView.setOnClickListener(this);
        shareImageView.setOnClickListener(this);
        deleteImageView.setOnClickListener(this);
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_SHOULD_DISPLAY_LOCAL_MEDIA)) {
                shouldDisplayLocalMedia = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_DISPLAY_LOCAL_MEDIA, true);

                if(shouldDisplayLocalMedia) {
                    // if we navigated here from Menu, by default disable multiple selection mode and leave it for user
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(false);
                } else {
                    // if we navigated here from MessageCompose screen, by default enable multiple selection mode
                    if(mSelectedFilterType != FilterType.FILTER_ALL) {
                        GlobalDataModel.getInstance().setMultipleSelectionEnabled(true);
                    } else {
                        GlobalDataModel.getInstance().setMultipleSelectionEnabled(false);
                    }
                }
            }
        }
    }

    public void preLoadData() {
        initialiseMediaDataObjects();

        // set adapter & notify data to listview
        mMediaListAdapter = new MediaListAdapter(mActivity, mSelectedFilterType, mMediaItemsTM, mActiveUser, shouldDisplayLocalMedia);
        mMediaListView.setAdapter(mMediaListAdapter);

        updateMediaAdapter();

        // Get Media Folders list from server
        displayProgressLoader(false);
            final GetFolderListLoader getFolderListLoader = new GetFolderListLoader(mActivity, mActiveUser.getToken(), mActiveUser.getUserDeviceID() + "");
        if (isAdded() && getView() != null) {
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_FOLDER_LIST_LOADER), getFolderListLoader);
        }


    }

    private void initialiseMediaDataObjects() {
        if (mMediaItemsTM == null) {
            mMediaItemsTM = new TreeMap<Integer, NavigableMap<String, ArrayList<MediaItemDbModel>>>();

            // PHOTOS
            NavigableMap<String, ArrayList<MediaItemDbModel>> photosTM = new TreeMap<String, ArrayList<MediaItemDbModel>>(Collections.reverseOrder());

            // VIDEOS
            NavigableMap<String, ArrayList<MediaItemDbModel>> videosTM = new TreeMap<String, ArrayList<MediaItemDbModel>>(Collections.reverseOrder());

            // AUDIOS
            NavigableMap<String, ArrayList<MediaItemDbModel>> audiosTM = new TreeMap<String, ArrayList<MediaItemDbModel>>(Collections.reverseOrder());

            // Add media categories to All TreeMap
            mMediaItemsTM.put(FilterType.FILTER_PHOTOS.getFilterId(), photosTM);
            mMediaItemsTM.put(FilterType.FILTER_VIDEOS.getFilterId(), videosTM);
            mMediaItemsTM.put(FilterType.FILTER_AUDIOS.getFilterId(), audiosTM);
        }
    }

    public void addNewOrUpdateMediaItemToMediaAdapter(MediaItemDbModel mediaItemDbModel) {
        initialiseMediaDataObjects();

        if (mMediaItemsTM != null && mediaItemDbModel != null) {

            if (mediaItemDbModel.getMediaType() == FilterType.FILTER_PHOTOS) {
                // Going to menu_empty PHOTO
                NavigableMap<String, ArrayList<MediaItemDbModel>> photosTM = mMediaItemsTM.get(FilterType.FILTER_PHOTOS.getFilterId());

                if (photosTM != null) {
                    String createdDate = CommonUtils.getMediaGroupDateString(mediaItemDbModel.getCreatedDate());

                    if (photosTM.get(createdDate) != null) {
                        ArrayList<MediaItemDbModel> mediaAL = photosTM.get(createdDate);

                        int mediaIndex = getIndexIfMediaAlreadyAdded(mediaAL, mediaItemDbModel);
                        if(mediaIndex == -1) {
                            // media not added before
                            mediaAL.add(mediaItemDbModel);
                        } else {
                            // media already added before now just update it
                            mediaAL.set(mediaIndex, mediaItemDbModel);
                        }
                    } else {
                        ArrayList<MediaItemDbModel> mediaAL = new ArrayList<MediaItemDbModel>();

                        mediaAL.add(mediaItemDbModel);
                        photosTM.put(createdDate, mediaAL);
                    }
                }
            } else if (mediaItemDbModel.getMediaType() == FilterType.FILTER_VIDEOS) {
                // Going to menu_empty VIDEOS
                NavigableMap<String, ArrayList<MediaItemDbModel>> videosTM = mMediaItemsTM.get(FilterType.FILTER_VIDEOS.getFilterId());

                if (videosTM != null) {
                    String createdDate = CommonUtils.getMediaGroupDateString(mediaItemDbModel.getCreatedDate());

                    if (videosTM.get(createdDate) != null) {
                        ArrayList<MediaItemDbModel> mediaAL = videosTM.get(createdDate);

                        int mediaIndex = getIndexIfMediaAlreadyAdded(mediaAL, mediaItemDbModel);
                        if(mediaIndex == -1) {
                            // media not added before
                            mediaAL.add(mediaItemDbModel);
                        } else {
                            // media already added before now just update it
                            mediaAL.set(mediaIndex, mediaItemDbModel);
                        }
                    } else {
                        ArrayList<MediaItemDbModel> mediaAL = new ArrayList<MediaItemDbModel>();

                        mediaAL.add(mediaItemDbModel);
                        videosTM.put(createdDate, mediaAL);
                    }
                }
            } else if (mediaItemDbModel.getMediaType() == FilterType.FILTER_AUDIOS) {
                // Going to menu_empty VIDEOS
                NavigableMap<String, ArrayList<MediaItemDbModel>> audiosTM = mMediaItemsTM.get(FilterType.FILTER_AUDIOS.getFilterId());

                if (audiosTM != null) {
                    String createdDate = CommonUtils.getMediaGroupDateString(mediaItemDbModel.getCreatedDate());

                    if (audiosTM.get(createdDate) != null) {
                        ArrayList<MediaItemDbModel> mediaAL = audiosTM.get(createdDate);

                        int mediaIndex = getIndexIfMediaAlreadyAdded(mediaAL, mediaItemDbModel);
                        if(mediaIndex == -1) {
                            // media not added before
                            mediaAL.add(mediaItemDbModel);
                        } else {
                            // media already added before now just update it
                            mediaAL.set(mediaIndex, mediaItemDbModel);
                        }
                    } else {
                        ArrayList<MediaItemDbModel> mediaAL = new ArrayList<MediaItemDbModel>();

                        mediaAL.add(mediaItemDbModel);
                        audiosTM.put(createdDate, mediaAL);
                    }
                }
            }
        }

        updateMediaAdapter();
    }

    private void deleteMediaItem(MediaItemDbModel deletedMediaItem) {
        if (mMediaItemsTM != null && deletedMediaItem != null) {

            if (deletedMediaItem.getMediaType() == FilterType.FILTER_PHOTOS) {
                // Going to menu_empty PHOTO
                NavigableMap<String, ArrayList<MediaItemDbModel>> photosTM = mMediaItemsTM.get(FilterType.FILTER_PHOTOS.getFilterId());

                if (photosTM != null) {
                    String createdDate = CommonUtils.getMediaGroupDateString(deletedMediaItem.getCreatedDate());

                    if (photosTM.get(createdDate) != null) {
                        ArrayList<MediaItemDbModel> mediaAL = photosTM.get(createdDate);

                        int mediaIndex = getIndexIfMediaAlreadyAdded(mediaAL, deletedMediaItem);
                        if(mediaIndex == -1) {
                            // do nothing, no media exists
                        } else {
                            // media already added before now just update it
                            mediaAL.remove(mediaIndex);

                            // remove the entire row in TreeMap if there were no media for the give date string
                            if(mediaAL != null && mediaAL.size() == 0) {
                                photosTM.remove(createdDate);
                            }
                        }
                    }
                }
            } else if (deletedMediaItem.getMediaType() == FilterType.FILTER_VIDEOS) {
                // Going to menu_empty VIDEOS
                NavigableMap<String, ArrayList<MediaItemDbModel>> videosTM = mMediaItemsTM.get(FilterType.FILTER_VIDEOS.getFilterId());

                if (videosTM != null) {
                    String createdDate = CommonUtils.getMediaGroupDateString(deletedMediaItem.getCreatedDate());

                    if (videosTM.get(createdDate) != null) {
                        ArrayList<MediaItemDbModel> mediaAL = videosTM.get(createdDate);

                        int mediaIndex = getIndexIfMediaAlreadyAdded(mediaAL, deletedMediaItem);
                        if(mediaIndex == -1) {
                            // do nothing, no media exists
                        } else {
                            // media already added before now just update it
                            mediaAL.remove(mediaIndex);

                            // remove the entire row in TreeMap if there were no media for the give date string
                            if(mediaAL != null && mediaAL.size() == 0) {
                                videosTM.remove(createdDate);
                            }
                        }
                    }
                }
            } else if (deletedMediaItem.getMediaType() == FilterType.FILTER_AUDIOS) {
                // Going to menu_empty VIDEOS
                NavigableMap<String, ArrayList<MediaItemDbModel>> audiosTM = mMediaItemsTM.get(FilterType.FILTER_AUDIOS.getFilterId());

                if (audiosTM != null) {
                    String createdDate = CommonUtils.getMediaGroupDateString(deletedMediaItem.getCreatedDate());

                    if (audiosTM.get(createdDate) != null) {
                        ArrayList<MediaItemDbModel> mediaAL = audiosTM.get(createdDate);

                        int mediaIndex = getIndexIfMediaAlreadyAdded(mediaAL, deletedMediaItem);
                        if(mediaIndex == -1) {
                            // do nothing, no media exists
                        } else {
                            // media already added before now just update it
                            mediaAL.remove(mediaIndex);

                            // remove the entire row in TreeMap if there were no media for the give date string
                            if(mediaAL != null && mediaAL.size() == 0) {
                                audiosTM.remove(createdDate);
                            }
                        }
                    }
                }
            }
        }

        updateMediaAdapter();
    }

    private int getIndexIfMediaAlreadyAdded(ArrayList<MediaItemDbModel> mediaAL, MediaItemDbModel mediaItemDbModel) {
        if(mediaItemDbModel != null && mediaAL != null && mediaAL.size() > 0) {
            for(int idx = 0; idx < mediaAL.size(); idx ++) {
                MediaItemDbModel mediaObj = mediaAL.get(idx);

                if(mediaObj != null) {
                    if (mediaObj.getMediaId() == mediaItemDbModel.getMediaId()) {
                        return idx;
                    }
                }
            }
        }

        return -1;
    }

    private void updateMediaAdapter() {
        if (mMediaListAdapter != null) {
            // check if any media has been added to Data Object, if not hide ListView and display hint text view
            if (isAnyMediaAddedToDataObject(mSelectedFilterType)) {
                mMediaListView.setVisibility(View.VISIBLE);
                mHintTextView.setVisibility(View.GONE);
            } else {
                mMediaListView.setVisibility(View.GONE);
                mHintTextView.setVisibility(View.VISIBLE);

            }

            mMediaListAdapter.updateMediaData(mSelectedFilterType, mMediaItemsTM);
            mediaListviewContainer.performClick();
        }
    }

    private boolean isAnyMediaAddedToDataObject(FilterType selectedFilterType) {
        if (mMediaItemsTM != null) {
            // PHOTOS
            NavigableMap<String, ArrayList<MediaItemDbModel>> photosTM = mMediaItemsTM.get(FilterType.FILTER_PHOTOS.getFilterId());

            // VIDEOS
            NavigableMap<String, ArrayList<MediaItemDbModel>> videosTM = mMediaItemsTM.get(FilterType.FILTER_VIDEOS.getFilterId());

            // AUDIOS
            NavigableMap<String, ArrayList<MediaItemDbModel>> audiosTM = mMediaItemsTM.get(FilterType.FILTER_AUDIOS.getFilterId());

            boolean isPhotosAvailable = (photosTM != null && photosTM.size() > 0);
            boolean isVideosAvailable = (videosTM != null && videosTM.size() > 0);
            boolean isAudiosAvailable = (audiosTM != null && audiosTM.size() > 0);

            if (selectedFilterType == FilterType.FILTER_ALL) {
                return (isPhotosAvailable || isVideosAvailable || isAudiosAvailable);
            } else if (selectedFilterType == FilterType.FILTER_PHOTOS) {
                return isPhotosAvailable;
            } else if (selectedFilterType == FilterType.FILTER_VIDEOS) {
                return isVideosAvailable;
            } else if (selectedFilterType == FilterType.FILTER_AUDIOS) {
                return isAudiosAvailable;
            }
        }

        return false;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.filter_btn:
                displayFilterOptions(mSelectedFilterType, mFilterTypesAL, "Filter");

                break;

            case R.id.help_btn:
                if (CommonUtils.hasInternet()) {
                    final HelpLoader helpLoader = new HelpLoader(mActivity, AxSysConstants.LOGIN_MODULE_ID,
                            mActiveUser != null ? mActiveUser.getToken() : "");
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(HELP_LOADER), helpLoader);
                } else {
                    showToast(R.string.error_no_network);
                }
                break;
            case R.id.uploadImageView:
                if(isAnyMediaSelectedForMultipleUploadOrDelete()) {
                    final ConfirmationDialogFragment uploadConfirmationDialogFragment = new ConfirmationDialogFragment();
                    final Bundle bundle1 = new Bundle();
                    bundle1.putString(ConfirmationDialogFragment.MESSAGE_KEY, String.format(getString(R.string.upload_multiple_media_confirmation),
                            mSelectedFilterType.getFilterName().toLowerCase()));
                    bundle1.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, UPLOAD_MEDIA_DIALOG_ID);
                    bundle1.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                    uploadConfirmationDialogFragment.setArguments(bundle1);
                    showDialog(uploadConfirmationDialogFragment);
                } else {
                    showToast(R.string.select_media_to_upload);
                }
                break;

            case R.id.shareImageView:
                break;

            case R.id.deleteImageView:
                if(isAnyMediaSelectedForMultipleUploadOrDelete()) {
                    final ConfirmationDialogFragment deleteConfirmationDialogFragment = new ConfirmationDialogFragment();
                    final Bundle bundle2 = new Bundle();
                    bundle2.putString(ConfirmationDialogFragment.MESSAGE_KEY, String.format(getString(R.string.delete_multiple_media_confirmation),
                            mSelectedFilterType.getFilterName().toLowerCase()));
                    bundle2.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, DELETE_MEDIA_DIALOG_ID);
                    bundle2.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                    deleteConfirmationDialogFragment.setArguments(bundle2);
                    showDialog(deleteConfirmationDialogFragment);
                } else {
                    showToast(R.string.select_media_to_delete);
                }
                break;


            default:
                break;
        }
    }

    private boolean isAnyMediaSelectedForMultipleUploadOrDelete() {
        NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(mSelectedFilterType.getFilterId());
        if (mediaTM != null && mediaTM.size() > 0) {
            Iterator<String> itr = mediaTM.keySet().iterator();

            while (itr.hasNext()) {
                ArrayList<MediaItemDbModel> mediaAL = mediaTM.get(itr.next());

                if (mediaAL != null && mediaAL.size() > 0) {
                    for (MediaItemDbModel mediaItem : mediaAL) {
                        itemsselected.setText("items are selected");
                        if (mediaItem.isSelected()) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_media, menu);
        MenuItem actionAddMedia = menu.findItem(R.id.action_add_media);
        MenuItem actionSelectDeselectMedia = menu.findItem(R.id.action_select_deselect_media);

        if (mSelectedFilterType == FilterType.FILTER_ALL) {
            actionAddMedia.setVisible(false);
            actionSelectDeselectMedia.setVisible(false);

            // if we navigated here from MessageCompose screen, by default enable multiple selection mode
            if(mSelectedFilterType != FilterType.FILTER_ALL) {
                GlobalDataModel.getInstance().setMultipleSelectionEnabled(true);
            } else {
                GlobalDataModel.getInstance().setMultipleSelectionEnabled(false);
            }

        } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS) {
            if(shouldDisplayLocalMedia) {
                // from left menu option
                actionAddMedia.setVisible(true);
                actionAddMedia.setIcon(R.drawable.media_plus);

                actionSelectDeselectMedia.setVisible(true);
                if (GlobalDataModel.getInstance().isMultipleSelectionEnabled()) {
                    actionSelectDeselectMedia.setIcon(R.drawable.deselect);
                } else {
                    actionSelectDeselectMedia.setIcon(R.drawable.done);
                }
            } else {
                // from message compose screen
                actionAddMedia.setVisible(false);
                actionSelectDeselectMedia.setVisible(true);
                actionSelectDeselectMedia.setIcon(R.drawable.done);

                singleChoiceLinearLayout.setVisibility(View.VISIBLE);
                multipeChoiceLinearLayout.setVisibility(View.GONE);

                // if we navigated here from MessageCompose screen, by default enable multiple selection mode
                if(mSelectedFilterType != FilterType.FILTER_ALL) {
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(true);
                } else {
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(false);
                }
            }
        } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS) {
            if(shouldDisplayLocalMedia) {
                // from left menu option
                actionAddMedia.setVisible(true);
                actionAddMedia.setIcon(R.drawable.media_plus);

                actionSelectDeselectMedia.setVisible(true);
                if (GlobalDataModel.getInstance().isMultipleSelectionEnabled()) {
                    actionSelectDeselectMedia.setIcon(R.drawable.deselect);
                } else {
                    actionSelectDeselectMedia.setIcon(R.drawable.done);
                }
            } else {
                // from message compose screen
                actionAddMedia.setVisible(false);
                actionSelectDeselectMedia.setVisible(true);
                actionSelectDeselectMedia.setIcon(R.drawable.done);

                singleChoiceLinearLayout.setVisibility(View.VISIBLE);
                multipeChoiceLinearLayout.setVisibility(View.GONE);

                // if we navigated here from MessageCompose screen, by default enable multiple selection mode
                if(mSelectedFilterType != FilterType.FILTER_ALL) {
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(true);
                } else {
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(false);
                }
            }
        } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS) {
            if(shouldDisplayLocalMedia) {
                // from left menu option
                actionAddMedia.setVisible(true);
                actionAddMedia.setIcon(R.drawable.media_plus);

                actionSelectDeselectMedia.setVisible(true);
                if (GlobalDataModel.getInstance().isMultipleSelectionEnabled()) {
                    actionSelectDeselectMedia.setIcon(R.drawable.deselect);
                } else {
                    actionSelectDeselectMedia.setIcon(R.drawable.done);
                }
            } else {
                // from message compose screen
                actionAddMedia.setVisible(false);
                actionSelectDeselectMedia.setVisible(true);
                actionSelectDeselectMedia.setIcon(R.drawable.done);

                singleChoiceLinearLayout.setVisibility(View.VISIBLE);
                multipeChoiceLinearLayout.setVisibility(View.GONE);

                // if we navigated here from MessageCompose screen, by default enable multiple selection mode
                if(mSelectedFilterType != FilterType.FILTER_ALL) {
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(true);
                } else {
                    GlobalDataModel.getInstance().setMultipleSelectionEnabled(false);
                }

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;

            case R.id.action_add_media:
                if (mSelectedFilterType == FilterType.FILTER_ALL) {

                } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS) {
                    showImagePicker(getString(R.string.picker_title_choose_image));
                } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS) {
                    showVideoPicker(getString(R.string.picker_title_capture_video));
                } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS) {
                    mSelectedFilterType = FilterType.FILTER_AUDIOS;

                    AudioRecordingFragment audioRecordingFragment = new AudioRecordingFragment();
                    Bundle b = new Bundle();
                    audioRecordingFragment.setArguments(b);
                    FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                    ft.add(R.id.container_layout, audioRecordingFragment, AudioRecordingFragment.class.getSimpleName());
                    ft.addToBackStack(null);
                    ft.commit();
                }
                break;

            case R.id.action_select_deselect_media:

                if(shouldDisplayLocalMedia) {
                    // from Menu options
                    updateMultipleSelectionMenuIcon();
                    itemsselected.setText("items are selected");
                } else {
                    // take the selected media information to message compose fragment
                    NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(mSelectedFilterType.getFilterId());
                    if (mediaTM != null && mediaTM.size() > 0) {
                        Iterator<String> itr = mediaTM.keySet().iterator();

                        ArrayList<AttachmentDetails> attachmentsAL = new ArrayList<AttachmentDetails>();

                        while (itr.hasNext()) {
                            ArrayList<MediaItemDbModel> mediaAL = mediaTM.get(itr.next());

                            if (mediaAL != null && mediaAL.size() > 0) {
                                for (MediaItemDbModel mediaItem : mediaAL) {

                                    if (mediaItem.isSelected()) {
                                        itemsselected.setText("items are selected");
                                        // Add selected media to attachments list datasource
                                        AttachmentDetails attachmentDetails = new AttachmentDetails();
                                        if(mSelectedFilterType == FilterType.FILTER_PHOTOS) {
                                            attachmentDetails.setAttachmentType(AxSysConstants.ATTACHMENT_TYPE_MEDIA_IMAGE);
                                        } else if(mSelectedFilterType == FilterType.FILTER_VIDEOS) {
                                            attachmentDetails.setAttachmentType(AxSysConstants.ATTACHMENT_TYPE_MEDIA_VIDEO);
                                        } else if(mSelectedFilterType == FilterType.FILTER_AUDIOS) {
                                            attachmentDetails.setAttachmentType(AxSysConstants.ATTACHMENT_TYPE_MEDIA_AUDIO);
                                        }

                                        attachmentDetails.setAttachment(mediaItem.getMediaId() + "");

                                        attachmentDetails.setAttachmentName(mediaItem.getMediaName());

                                        attachmentDetails.setAttachmentSize("");

                                        if(mSelectedFilterType == FilterType.FILTER_PHOTOS) {
                                            attachmentDetails.setAttachmentType_LU(AxSysConstants.MEDIA_IMAGES_ATTACHMENT_ID);
                                        } else if(mSelectedFilterType == FilterType.FILTER_VIDEOS) {
                                            attachmentDetails.setAttachmentType_LU(AxSysConstants.MEDIA_VIDEO_ATTACHMENT_ID);
                                        } else if(mSelectedFilterType == FilterType.FILTER_AUDIOS) {
                                            attachmentDetails.setAttachmentType_LU(AxSysConstants.MEDIA_AUDIO_ATTACHMENT_ID);
                                        }

                                        attachmentDetails.setDashboardId("");

                                        attachmentsAL.add(attachmentDetails);
                                    }
                                }
                            }
                        }

                        // if any media selected for message attachments
                        if(attachmentsAL != null && attachmentsAL.size() > 0) {
                            itemsselected.setText("items are selected");
                            // take the selected media attachments details to Message Compose Screen
                            if(mediaAttachmentsListener != null) {
                                itemsselected.setText("items are selected");
                                mediaAttachmentsListener.onMediaSelectedForMsgAttachments(attachmentsAL);
                            }
                            mActivity.onBackPressed();
                        } else {
                            showToast(R.string.select_atleast_one_media);
                        }

                    } else {
                        showToast(R.string.select_atleast_one_media);
                    }
                }
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateMultipleSelectionMenuIcon() {
        GlobalDataModel.getInstance().setMultipleSelectionEnabled(!GlobalDataModel.getInstance().isMultipleSelectionEnabled());

        if(GlobalDataModel.getInstance().isMultipleSelectionEnabled()) {
            singleChoiceLinearLayout.setVisibility(View.GONE);
            multipeChoiceLinearLayout.setVisibility(View.VISIBLE);
            itemsselected.setText("items are selected");
        } else {
            singleChoiceLinearLayout.setVisibility(View.VISIBLE);
            multipeChoiceLinearLayout.setVisibility(View.GONE);

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    if(mMediaItemsTM != null) {
                        NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(mSelectedFilterType.getFilterId());
                        if (mediaTM != null && mediaTM.size() > 0) {
                            Iterator<String> itr = mediaTM.keySet().iterator();

                            while (itr.hasNext()) {
                                ArrayList<MediaItemDbModel> mediaAL = mediaTM.get(itr.next());

                                if (mediaAL != null && mediaAL.size() > 0) {
                                    for (MediaItemDbModel mediaItem : mediaAL) {
                                        if (mediaItem.isSelected()) {
                                            mediaItem.setSelected(false);
                                        }
                                    }
                                }
                            }
                            if (mMediaListAdapter != null) mMediaListAdapter.notifyDataSetChanged();
                        }
                    }
                }
            });
        }

        mActivity.supportInvalidateOptionsMenu();
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_FOLDER_LIST_LOADER)) {
            // listMediaItemThumbnails
            displayProgressLoader(false);
            MediaItemsThumbnailsListLoader mediaItemsThumbnailsListLoader = new MediaItemsThumbnailsListLoader(mActivity, "",
                    mActiveUser.getToken(), mActiveUser.getPatientID(), 1, 100);
            if(isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MEDIA_ITEM_THUMBNAILS_LOADER), mediaItemsThumbnailsListLoader);
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_MEDIA_FOLDERS_LOADER)) {
             // listMediaItemThumbnails
            displayProgressLoader(false);
            MediaItemsThumbnailsListLoader mediaItemsThumbnailsListLoader = new MediaItemsThumbnailsListLoader(mActivity, "",
                    mActiveUser.getToken(), mActiveUser.getPatientID(), 1, 100);
            if(isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MEDIA_ITEM_THUMBNAILS_LOADER), mediaItemsThumbnailsListLoader);
            }
        } else if (id == getLoaderHelper().getLoaderId(LIST_MEDIA_ITEM_THUMBNAILS_LOADER)) {

             // get all media items from 'MediaItems' database table
            final GetMediaItemsFromDBLoader getMediaItemsFromDBLoader = new GetMediaItemsFromDBLoader(mActivity);
            if(isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MEDIA_ITEMS_FROM_DB_LOADER), getMediaItemsFromDBLoader);
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_MEDIA_ITEMS_LOADER)) {

            // get all media items from 'MediaItems' database table
            final GetMediaItemsFromDBLoader getMediaItemsFromDBLoader = new GetMediaItemsFromDBLoader(mActivity);
            if(isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MEDIA_ITEMS_FROM_DB_LOADER), getMediaItemsFromDBLoader);
            }
        } else if (id == getLoaderHelper().getLoaderId(HELP_LOADER)) {
            // show toast to check for connectivity
            showToast(R.string.error_loading_help_info);
        }  else if(id == getLoaderHelper().getLoaderId(AxSysConstants.GET_MEDIA_ITEM_LOADER)) {

            showToast(R.string.error_loading_media_info);
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_FOLDER_LIST_LOADER)) {

            if (result != null && result instanceof GetFoldersListResponse) {
                GetFoldersListResponse foldersListResponse = (GetFoldersListResponse) result;

                // Save Folder details in DB
                final SaveMediaFoldersDBLoader saveSiteDetailsLoader = new SaveMediaFoldersDBLoader(mActivity, foldersListResponse);
                if(isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_MEDIA_FOLDERS_LOADER), saveSiteDetailsLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_MEDIA_FOLDERS_LOADER)) {

            // listMediaItemThumbnails

            MediaItemsThumbnailsListLoader mediaItemsThumbnailsListLoader = new MediaItemsThumbnailsListLoader(mActivity, "",
                    mActiveUser.getToken(), mActiveUser.getPatientID(), 1, 100);
            if(isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MEDIA_ITEM_THUMBNAILS_LOADER), mediaItemsThumbnailsListLoader);
            }
        } else if (id == getLoaderHelper().getLoaderId(LIST_MEDIA_ITEM_THUMBNAILS_LOADER)) {

              if (result != null && result instanceof ListMediaItemThumbNailsResponse) {
                ListMediaItemThumbNailsResponse mediaItemThumbNailsResponse = (ListMediaItemThumbNailsResponse) result;

                // Convert 'MediaItemServerModel' to 'MediaItemDbModel' in-order to save them in DB
                ArrayList<MediaItemDbModel> mediaItemsAL = ConverterResponseToDbModel.getMediaItemsAsDbModel(mActiveUser,
                        mediaItemThumbNailsResponse.getMediaItemServerModelArrayList());

                // Save Media item details received from server in DB
                final SaveMediaItemsDBLoader saveMediaItemsDBLoader = new SaveMediaItemsDBLoader(mActivity, mediaItemsAL);
                if(isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_MEDIA_ITEMS_LOADER), saveMediaItemsDBLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_MEDIA_ITEMS_LOADER)) {

            if (result != null && result instanceof ArrayList) {
                // get all media items from 'MediaItems' database table
                final GetMediaItemsFromDBLoader getMediaItemsFromDBLoader = new GetMediaItemsFromDBLoader(mActivity);
                if(isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MEDIA_ITEMS_FROM_DB_LOADER), getMediaItemsFromDBLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_MEDIA_ITEMS_FROM_DB_LOADER)) {
            if (result != null && result instanceof List) {
                ArrayList<MediaItemDbModel> allMediaItemsFromDb = (ArrayList<MediaItemDbModel>) result;

                if (allMediaItemsFromDb != null && allMediaItemsFromDb.size() > 0) {
                    // clear before refreshing
                    mMediaItemsTM = null;
                    initialiseMediaDataObjects();

                    for (MediaItemDbModel mediaItemDbModel : allMediaItemsFromDb) {
                        if(shouldDisplayLocalMedia) {
                            // if we should display the local media add all
                            addNewOrUpdateMediaItemToMediaAdapter(mediaItemDbModel);
                        } else {
                            // if we came here from message compose screen, do not display local media in the list
                            if(mediaItemDbModel.isUploaded()) {
                                addNewOrUpdateMediaItemToMediaAdapter(mediaItemDbModel);
                            }
                        }
                    }
                } else {
                    // clear before refreshing
                    mMediaItemsTM = null;
                    initialiseMediaDataObjects();

                    updateMediaAdapter();
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(HELP_LOADER)) {
            if (result != null && result instanceof HelpResponse) {
                final HelpResponse helpResponse = (HelpResponse) result;
                if (helpResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            CommonUtils.displayWebViewInDialog(mActivity, helpResponse.getData(), getString(R.string.m_help));
                        }
                    });
                }
            }
        } else if(id == getLoaderHelper().getLoaderId(AxSysConstants.GET_MEDIA_ITEM_LOADER)) {

            if(result != null && result instanceof GetMediaItemResponse) {
                GetMediaItemResponse mediaItemResponse = (GetMediaItemResponse) result;

                // create original media and it should be stored in device folder for offline access
                // "http://forum.androidappmasters.com/styles/prosilver/theme/images/site_logo.png"

                String mediaUrl = (!TextUtils.isEmpty(mediaItemResponse.getMediaItemData().getOriginalURL()) ?
                        mediaItemResponse.getMediaItemData().getOriginalURL() : (!TextUtils.isEmpty(mediaItemResponse.getMediaItemData().getPreviewURL()) ?
                        mediaItemResponse.getMediaItemData().getPreviewURL() : null));

                if(!TextUtils.isEmpty(mediaUrl)) {
                    // starting new Async Task
                    DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL(mediaUrl, mediaItemResponse);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        downloadFileFromURL.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        downloadFileFromURL.execute();
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(UPDATE_MEDIA_ITEM_IN_DB_LOADER)) {

            if (result != null && result instanceof MediaItemDbModel) {
                MediaItemDbModel updatedMedia = (MediaItemDbModel) result;

                addNewOrUpdateMediaItemToMediaAdapter(updatedMedia);
            }
        }
    }

    /**
     * Background Async Task to download file
     * */
    private class DownloadFileFromURL extends AsyncTask<Void, Void, File> {

        private String mediaUrl;
        private GetMediaItemResponse mediaItemResponse;

        public DownloadFileFromURL(String mediaUrl, GetMediaItemResponse mediaItemResponse) {
            this.mediaUrl = mediaUrl;
            this.mediaItemResponse = mediaItemResponse;
        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            displayProgressLoader(false);
        }

        @Override
        protected File doInBackground(Void... params) {
            int count;
            File mediaStorageDir = CommonUtils.createMediaFolder();

            // Create a media file name
            String fileName = System.currentTimeMillis() + ""; // mediaUrl.substring(mediaUrl.lastIndexOf("/") + 1)
            String fileExt = ".jpg";
            if(!TextUtils.isEmpty(mediaItemResponse.getMediaItemData().getFileName())) {
                fileExt = mediaItemResponse.getMediaItemData().getFileName().substring(mediaItemResponse.getMediaItemData().getFileName().indexOf("."));
            }

            File destinationFile = new File(mediaStorageDir.getPath() + File.separator + fileName + fileExt);

            try {
                URL url = new URL(mediaUrl);
                URLConnection conection = url.openConnection();
                conection.connect();

                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(destinationFile);

                byte data[] = new byte[1024];

                while ((count = input.read(data)) != -1) {
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Trace.e("Error: ", e.getMessage());
                destinationFile = null;
            }

            return destinationFile;
        }

        @Override
        protected void onPostExecute(File destinationFile) {
            super.onPostExecute(destinationFile);
            // dismiss the dialog after the file was downloaded
            hideProgressLoader();

            if(destinationFile != null && destinationFile.length() > 0) {

                mediaItemResponse.getMediaItemData().setPreviewURL(destinationFile.getPath());

                // Prepare 'MediaItemDbModel' object by copying single media item details received from server
                MediaItemDbModel mediaItemDbModel = ConverterResponseToDbModel.getMediaItemDbModelForMediaDetailsServerObj(mActiveUser, mediaItemResponse);

                Intent viewIntent = new Intent(mActivity, MediaDetailsActivity.class);
                viewIntent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_OBJ, mediaItemDbModel);
                viewIntent.putExtra(AxSysConstants.EXTRA_MEDIA_DETAILS_VIEWTYPE, MediaDetailsViewType.VIEW);
                mActivity.startActivityForResult(viewIntent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);
            } else {
                showToast(R.string.error_loading_media_info);
            }
        }

    }

    public void displayFilterOptions(final FilterType selectedFilterType, final ArrayList<FilterType> filtersList, final String alertTitle) {
        int imagesCount = getMediaCount(FilterType.FILTER_PHOTOS);
        int videosCount = getMediaCount(FilterType.FILTER_VIDEOS);
        int audiosCount = getMediaCount(FilterType.FILTER_AUDIOS);

        final String[] filters = FilterType.getAllFilters(selectedFilterType, filtersList, imagesCount, videosCount, audiosCount);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(alertTitle);
        builder.setCancelable(true);
        builder.setItems(filters, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String filterName = filters[item];
                filterName = filterName.substring(0, filterName.indexOf("(")).trim();

                mFilterTypesAL = FilterType.getAllFilters(filterName);
                mSelectedFilterType = FilterType.getFilterTypeForName(filterName);

                mActivity.supportInvalidateOptionsMenu();

                if (mSelectedFilterType == FilterType.FILTER_ALL) {
                    mActivity.getSupportActionBar().setTitle(getString(R.string.menu_item_media) + "-" + FilterType.FILTER_ALL.getFilterName());
                } else if (mSelectedFilterType == FilterType.FILTER_PHOTOS) {
                    mActivity.getSupportActionBar().setTitle(FilterType.FILTER_PHOTOS.getFilterName());
                } else if (mSelectedFilterType == FilterType.FILTER_VIDEOS) {
                    mActivity.getSupportActionBar().setTitle(FilterType.FILTER_VIDEOS.getFilterName());
                } else if (mSelectedFilterType == FilterType.FILTER_AUDIOS) {
                    mActivity.getSupportActionBar().setTitle(FilterType.FILTER_AUDIOS.getFilterName());
                }

                if (mMediaListAdapter != null) {
                    updateMediaAdapter();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showImagePicker(String alertTitle) {
        final CharSequence[] items = {
                getString(R.string.m_take_photo),
                getString(R.string.m_pick_photo)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(alertTitle);
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.m_take_photo))) {
                    captureImage();

                } else if (items[item].equals(getString(R.string.m_pick_photo))) {
                    pickImageFromGallery();

                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showVideoPicker(String alertTitle) {
        final CharSequence[] items = {
                getString(R.string.m_record_video),
                getString(R.string.m_pick_video)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(alertTitle);
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.m_record_video))) {
                    captureVideo();
                } else if (items[item].equals(getString(R.string.m_pick_video))) {
                    pickVideoFromGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void captureVideo() {
        startActivityForResult(new Intent(MediaStore.ACTION_VIDEO_CAPTURE), AxSysConstants.INTENT_ID_RECORD_VIDEO);
    }

    public void pickVideoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        startActivityForResult(intent, AxSysConstants.INTENT_ID_PICK_VIDEO);
    }

    @InjectSavedState
    private Uri mCapturedImageFileUri; // file url to store image

    /*
     * Capturing Camera Image will lauch camera app requrest image capture
	 */
    private void captureImage() {
        // start the image capture Intent
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mCapturedImageFileUri = Uri.fromFile(getOutputImageFile());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageFileUri);
        startActivityForResult(intent, AxSysConstants.INTENT_ID_TAKE_PHOTO);
    }

    public void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, AxSysConstants.INTENT_ID_PICK_PHOTO);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mSelectedFilterType == FilterType.FILTER_ALL) {

            mActivity.getSupportActionBar().setTitle(getString(R.string.menu_item_media) + "-" + FilterType.FILTER_ALL.getFilterName());

        } else {
            mActivity.getSupportActionBar().setTitle(mSelectedFilterType.getFilterName());
        }

        // Register broadcast receivers
        mActivity.registerReceiver(broadCastReceiver, mediaIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Un-register BroadCast Receivers
        mActivity.unregisterReceiver(broadCastReceiver);
    }

    private BroadcastReceiver broadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CommonUtils.hideSoftKeyboard(mActivity);

            if(intent.getAction() == AxSysConstants.BCR_UPDATE_MEDIA_SCREEN) {
                // get all media items from 'MediaItems' database table
                final GetMediaItemsFromDBLoader getMediaItemsFromDBLoader = new GetMediaItemsFromDBLoader(mActivity);
                if (isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MEDIA_ITEMS_FROM_DB_LOADER), getMediaItemsFromDBLoader);
                }
            } /*else if(intent.getAction() == AxSysConstants.BCR_MEDIA_DELETED) {
                Bundle extras = intent.getExtras();

                if (extras != null && extras.containsKey(AxSysConstants.EXTRA_DELETED_MEDIA_OBJECT)) {
                    MediaItemDbModel mediaItemDbModel = (MediaItemDbModel) extras.getSerializable(AxSysConstants.EXTRA_DELETED_MEDIA_OBJECT);

                    deleteMediaItem(mediaItemDbModel);
                }
            } */
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == mActivity.RESULT_OK) {
            if (requestCode == AxSysConstants.INTENT_ID_TAKE_PHOTO) {
                setCapturedImage();

            } else if (requestCode == AxSysConstants.INTENT_ID_PICK_PHOTO) {
                setGalleryImage(data);

            } else if (requestCode == AxSysConstants.INTENT_ID_RECORD_VIDEO) {
                selectVideo(data, true);

            } else if (requestCode == AxSysConstants.INTENT_ID_PICK_VIDEO) {
                selectVideo(data, false);

            } else if (requestCode == AxSysConstants.MEDIA_DETAILS_REQUEST_CODE) {
                if (data != null) {
                    if (data.hasExtra(AxSysConstants.EXTRA_SAVED_MEDIA_OBJECT)) {
                        MediaItemDbModel mediaItemDbModel = (MediaItemDbModel) data.getSerializableExtra(AxSysConstants.EXTRA_SAVED_MEDIA_OBJECT);

                        addNewOrUpdateMediaItemToMediaAdapter(mediaItemDbModel);

                    } else if(data.hasExtra(AxSysConstants.EXTRA_UPLOAD_MEDIA_OBJECT)) {
                        MediaItemDbModel mediaItemDbModel = (MediaItemDbModel) data.getSerializableExtra(AxSysConstants.EXTRA_UPLOAD_MEDIA_OBJECT);

                        uploadMediaToServer(mediaItemDbModel);
                    }
                }
            }
        }
    }

    public void uploadMediaToServer(MediaItemDbModel mediaItemDbModel) {
        addNewOrUpdateMediaItemToMediaAdapter(mediaItemDbModel);

        // start intent service to upload media items to server
        /* Starting Download Service */
        if(uploadMediaResultReceiver == null) {
            uploadMediaResultReceiver = new UploadMediaResultReceiver(new Handler());
        }
        uploadMediaResultReceiver.setReceiver(mFragment);

        Intent intent = new Intent(Intent.ACTION_SYNC, null, mActivity, UploadMediaService.class);
        /* Send optional extras to Download IntentService */
        intent.putExtra(AxSysConstants.SERVICE_UPLOAD_MEDIA_OBJECT, mediaItemDbModel);
        intent.putExtra(AxSysConstants.SERVICE_MEDIA_RECEIVER, uploadMediaResultReceiver);
        mActivity.startService(intent);
    }

    public void deleteMediaFromServer(MediaItemDbModel mediaItemDbModel) {
        // start intent service to upload media items to server
        /* Starting Download Service */
        if(uploadMediaResultReceiver == null) {
            uploadMediaResultReceiver = new UploadMediaResultReceiver(new Handler());
        }
        uploadMediaResultReceiver.setReceiver(mFragment);

        Intent intent = new Intent(Intent.ACTION_SYNC, null, mActivity, UploadMediaService.class);
        /* Send optional extras to Download IntentService */
        intent.putExtra(AxSysConstants.SERVICE_DELETE_MEDIA_OBJECT, mediaItemDbModel);
        intent.putExtra(AxSysConstants.SERVICE_MEDIA_RECEIVER, uploadMediaResultReceiver);
        mActivity.startService(intent);
    }

    public void setCapturedImage() {
        File destination = new File(mCapturedImageFileUri.getPath());

        // navigate to details page to save media, pass file path to details fragment
        if (destination.exists() && !destination.isDirectory()) {
            mSelectedFilterType = FilterType.FILTER_PHOTOS;

            // navigate to details page to save media, pass file path to details fragment
            Intent intent = new Intent(mActivity, MediaDetailsActivity.class);
            intent.putExtra(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER, false);
            intent.putExtra(AxSysConstants.EXTRA_MEDIA_FILE_PATH, mCapturedImageFileUri);
            intent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE, mSelectedFilterType);
            startActivityForResult(intent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);
        }
    }

    public void setGalleryImage(Intent data) {
        if (data != null && data.getData() != null) {
            Uri selectedImageUri = data.getData();

            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, projection, null, null, null);

            String selectedImagePath = "";

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                selectedImagePath = cursor.getString(column_index);
            }

            File destination = new File(selectedImagePath);

            if (destination.exists() && !destination.isDirectory()) {
                mSelectedFilterType = FilterType.FILTER_PHOTOS;

                // navigate to details page to save media, pass file path to details fragment
                Intent intent = new Intent(mActivity, MediaDetailsActivity.class);
                intent.putExtra(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER, true);
                intent.putExtra(AxSysConstants.EXTRA_MEDIA_FILE_PATH, Uri.fromFile(destination));
                intent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE, mSelectedFilterType);
                startActivityForResult(intent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);
            }
        }
    }

    public void selectVideo(Intent data, boolean isNewlyRecorded) {
        Uri selectedImage = data.getData();
        Trace.i(TAG, "Videotest:" + data.getData());

        String[] filePath = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
        String picturePath = "";

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePath[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        }

        // navigate to details page to save media, pass file path to details fragment
        if (!TextUtils.isEmpty(picturePath)) {
            mSelectedFilterType = FilterType.FILTER_VIDEOS;

            Intent intent = new Intent(mActivity, MediaDetailsActivity.class);
            if (isNewlyRecorded) {
                intent.putExtra(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER, false);
            } else {
                intent.putExtra(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER, true);
            }
            intent.putExtra(AxSysConstants.EXTRA_MEDIA_FILE_PATH, Uri.fromFile(new File(picturePath)));
            intent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE, mSelectedFilterType);
            startActivityForResult(intent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);
        }
    }

    /*
     * returns image path
     */
    private File getOutputImageFile() {

        // External sdcard location
        File mediaStorageDir = CommonUtils.createMediaFolder();

        // Create a media file name
//        File imageFile = new File(mediaStorageDir.getPath() + File.separator + AxSysConstants.MEDIA_IMAGE_PREFIX + System.currentTimeMillis() + ".jpg");
        File imageFile = new File(Environment.getExternalStorageDirectory(), AxSysConstants.MEDIA_IMAGE_PREFIX + System.currentTimeMillis() + ".jpg");

        return imageFile;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case UploadMediaService.STATUS_RUNNING:

                if(resultData != null) {
                    if(resultData.containsKey(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT)) {
                        MediaItemDbModel uploadingMediaObj = (MediaItemDbModel) resultData.getSerializable(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT);
                        uploadingMediaObj.setIsEdited(true);
                        uploadingMediaObj.setIsUploaded(false);
                        uploadingMediaObj.setIsUploading(true);

                        addNewOrUpdateMediaItemToMediaAdapter(uploadingMediaObj);

                    } else if(resultData.containsKey(AxSysConstants.SERVICE_CURRENT_DELETING_MEDIA_OBJECT)) {

                        MediaItemDbModel deletingMediaObj = (MediaItemDbModel) resultData.getSerializable(AxSysConstants.SERVICE_CURRENT_DELETING_MEDIA_OBJECT);
                        deletingMediaObj.setIsEdited(false);
                        deletingMediaObj.setIsDeleted(true);
                        deletingMediaObj.setIsUploaded(false);
                        deletingMediaObj.setIsUploading(true);

                        updateMediaAdapter();
                    }
                }
                break;
            case UploadMediaService.STATUS_FINISHED:
                if(resultData != null) {
                    if(resultData.containsKey(AxSysConstants.SERVICE_UPLOADED_MEDIA_OBJECT_RESPONSE)) {

                        UploadMediaItemResponse uploadMediaItemResponse = (UploadMediaItemResponse)
                                resultData.getSerializable(AxSysConstants.SERVICE_UPLOADED_MEDIA_OBJECT_RESPONSE);

                        MediaItemDbModel uploadedMediaObj = null;
                        if(resultData.containsKey(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT)) {
                            uploadedMediaObj = (MediaItemDbModel) resultData.getSerializable(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT);
                        }

                        if(uploadedMediaObj != null) {
                            uploadedMediaObj.setIsEdited(false);
                            uploadedMediaObj.setIsUploaded(true);
                            uploadedMediaObj.setIsUploading(false);

                            if(uploadMediaItemResponse != null) {
                                // update DB once upload is done with new media id
                                final UpdateMediaItemDBLoader updateMediaItemDBLoader = new UpdateMediaItemDBLoader(mActivity, uploadedMediaObj, uploadMediaItemResponse.getData());
                                if (isAdded() && getView() != null) {
                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(UPDATE_MEDIA_ITEM_IN_DB_LOADER), updateMediaItemDBLoader);
                                }
                            }
                        }

                    } else if(resultData.containsKey(AxSysConstants.SERVICE_DELETED_MEDIA_OBJECT_RESPONSE)) {

                        MediaItemDbModel deleteMediaItem = (MediaItemDbModel)
                                resultData.getSerializable(AxSysConstants.SERVICE_DELETED_MEDIA_OBJECT_RESPONSE);

                        deleteMediaItem(deleteMediaItem);

                        // if there were no media after deleting or updating multiple media, toggle the multiple selection mark
//                        if(mMediaItemsTM != null) {
//                            NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(mSelectedFilterType.getFilterId());
//                            if (mediaTM != null && mediaTM.size() > 0) {
//                                // do nothing
//                            } else {
//                                updateMultipleSelectionMenuIcon();
//                            }
//                        }
                    }
                }

                break;
            case UploadMediaService.STATUS_ERROR:
                if(resultData != null) {
                    if(resultData.containsKey(AxSysConstants.SERVICE_ERROR_UPLOADING_MEDIA)) {

                        if(resultData.containsKey(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT)) {
                            MediaItemDbModel uploadingMediaObj = (MediaItemDbModel) resultData.getSerializable(AxSysConstants.SERVICE_CURRENT_UPLOADING_MEDIA_OBJECT);
                            uploadingMediaObj.setIsEdited(true);
                            uploadingMediaObj.setIsUploaded(false);
                            uploadingMediaObj.setIsUploading(true);
                        }

                        String errorString = resultData.getString(AxSysConstants.SERVICE_ERROR_UPLOADING_MEDIA);
                        showToast(errorString);

                    } else if(resultData.containsKey(AxSysConstants.SERVICE_ERROR_DELETING_MEDIA)) {

                        if(resultData.containsKey(AxSysConstants.SERVICE_CURRENT_DELETING_MEDIA_OBJECT)) {
                            MediaItemDbModel deletedMediaObj = (MediaItemDbModel) resultData.getSerializable(AxSysConstants.SERVICE_CURRENT_DELETING_MEDIA_OBJECT);

                            deletedMediaObj.setIsEdited(false);
                            deletedMediaObj.setIsDeleted(true);
                            deletedMediaObj.setIsUploaded(false);
                            deletedMediaObj.setIsUploading(true);
                        }

                        String errorString = resultData.getString(AxSysConstants.SERVICE_ERROR_DELETING_MEDIA);
                        showToast(errorString);
                    }
                }
                break;
        }
    }

    @Override
    public void onConfirm(int dialogId) {
        if(dialogId == UPLOAD_MEDIA_DIALOG_ID) {
            if(mMediaItemsTM != null) {
                NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(mSelectedFilterType.getFilterId());
                if (mediaTM != null && mediaTM.size() > 0) {
                    Iterator<String> itr = mediaTM.keySet().iterator();

                    while (itr.hasNext()) {
                        ArrayList<MediaItemDbModel> mediaAL = mediaTM.get(itr.next());

                        if (mediaAL != null && mediaAL.size() > 0) {
                            for (MediaItemDbModel mediaItem : mediaAL) {

                                if (mediaItem.isSelected()) {
                                    mediaItem.setSelected(false);

                                    mediaItem.setIsEdited(true);
                                    mediaItem.setIsUploaded(mediaItem.isUploaded());
                                    mediaItem.setIsUploading(true);

                                    uploadMediaToServer(mediaItem);
                                }
                            }
                        }
                    }
                    updateMultipleSelectionMenuIcon();

                }
            }
        } else if (dialogId == DELETE_MEDIA_DIALOG_ID) {
            if(mMediaItemsTM != null) {
                NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(mSelectedFilterType.getFilterId());
                if (mediaTM != null && mediaTM.size() > 0) {
                    Iterator<String> itr = mediaTM.keySet().iterator();

                    while (itr.hasNext()) {
                        ArrayList<MediaItemDbModel> mediaAL = mediaTM.get(itr.next());

                        if (mediaAL != null && mediaAL.size() > 0) {
                            for (MediaItemDbModel mediaItem : mediaAL) {

                                if (mediaItem.isSelected()) {
                                    mediaItem.setSelected(false);

                                    mediaItem.setIsEdited(false);
                                    mediaItem.setIsDeleted(true);
                                    mediaItem.setIsUploaded(mediaItem.isUploaded());
                                    mediaItem.setIsUploading(true);

                                    deleteMediaFromServer(mediaItem);
                                }
                            }
                        }
                    }
                    updateMultipleSelectionMenuIcon();
                }
            }
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if(dialogId == UPLOAD_MEDIA_DIALOG_ID) {

        } else if (dialogId == DELETE_MEDIA_DIALOG_ID) {

        }
    }

    private int getMediaCount(FilterType selectedFilterType) {
        int mCount = 0;

        if(mMediaItemsTM != null) {
            NavigableMap<String, ArrayList<MediaItemDbModel>> mediaTM = mMediaItemsTM.get(selectedFilterType.getFilterId());
            if (mediaTM != null && mediaTM.size() > 0) {
                Iterator<String> itr = mediaTM.keySet().iterator();

                while (itr.hasNext()) {
                    ArrayList<MediaItemDbModel> mediaAL = mediaTM.get(itr.next());

                    if (mediaAL != null && mediaAL.size() > 0) {
                        mCount += mediaAL.size();
                    }
                }
            }
        }

        return mCount;
    }

    public interface MediaAttachmentsListener {
        public void onMediaSelectedForMsgAttachments(ArrayList<AttachmentDetails> mediaAttachmentsAL);
    }

}