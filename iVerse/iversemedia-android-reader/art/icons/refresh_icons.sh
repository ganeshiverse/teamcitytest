#!/bin/bash

RESDIR="../../comicsplus-client/res"

function GenWhiteIcon()
{
  ICON=$1
  PNG=${ICON//.svg}.png

  convert -resize 72x72 \
          -background none \
          $ICON \
          -negate $RESDIR/drawable-hdpi/$PNG
  
  convert -resize 48x48 \
          -background none \
          $ICON \
          -negate $RESDIR/drawable-mdpi/$PNG
}

function GenActionBarIcon()
{
  ICON=$1
  PNG=${ICON//.svg}.png

  cat $ICON | \
    sed -e "s/inkscape:pageopacity=\"[0-9.]\"/inkscape:pageopacity=\"0\"/g" | \
    convert -background none \
        - $RESDIR/drawable-hdpi/$PNG
}

ICONS=`ls -1 icon*.svg | grep -v template`

for ICON in $ICONS
do
    GenWhiteIcon $ICON
done

ICONS=`ls -1 actionbar_icon*.svg`

for ICON in $ICONS
do
    GenActionBarIcon $ICON
done

