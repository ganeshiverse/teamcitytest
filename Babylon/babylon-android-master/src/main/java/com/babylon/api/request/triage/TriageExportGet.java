package com.babylon.api.request.triage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.util.Log;

import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Error;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.database.BabylonContentProvider;
import com.babylon.database.check.CheckDatabaseHelper;
import com.babylon.database.schema.check.BodyPartColorSchema;
import com.babylon.database.schema.check.BodyPartSchema;
import com.babylon.database.schema.check.OutcomeSchema;
import com.babylon.database.schema.check.SymptomHashSchema;
import com.babylon.database.schema.check.SymptomSchema;
import com.babylon.model.check.export.BaseExportModel;
import com.babylon.model.check.export.BodyPart;
import com.babylon.model.check.export.BodyPartColor;
import com.babylon.model.check.export.Outcome;
import com.babylon.model.check.export.Symptom;
import com.babylon.model.check.export.SymptomHash;
import com.babylon.sync.TriageSync;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TriageExportGet extends AbsRequestGet<Void> {

    private final ContentResolver contentResolver;

    public TriageExportGet(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    protected String getUrl() {
        return Urls.get(Urls.TRIAGE_EXPORT);
    }

    @Override
    protected AbsResponseProcessor<Void> getResponseProcessor() {
        return new StreamResponseProcessor();
    }

    enum ExportRootNode {
        data, bodyPart, outcome, symptoms, map, bodyPartColor, lastUpdatedDate
    }

    class StreamResponseProcessor extends AbsResponseProcessor<Void> {

        private static final String ID_KEY = "id";
        private static final String HASH_KEY = "hash";
        private static final String BODY_PART_KEY = "bodyPartId";
        private static final String OUTCOME_KEY = "outcomeId";
        private static final String STATE_KEY = "state";

        private static final int MAX_OPERATION_SIZE = 5000;
        private long lastUpdateDate;

        public StreamResponseProcessor() {
            super(Void.class);
        }

        @Override
        public Response<Void> getResponse(HttpResponse httpResponse) {
            Response result = new Response();
            result.setStatus(Response.RESULT_ERROR);
            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine != null) {
                try {
                    InputStream ungzippedContent = AndroidHttpClient.getUngzippedContent(
                            httpResponse.getEntity());
                    processJSONResponse(ungzippedContent);
                    result.setStatus(Response.RESULT_OK);
                } catch (IOException | InterruptedException | JsonSyntaxException e) {
                    addError(result, e);
                }
            }
            return result;
        }

        private void addError(Response result, Exception e) {
            List<Error> errors = new ArrayList<>();
            String message = App.getInstance().getString(R.string.check_connection_problem);
            errors.add(new Error(e.toString(), message));
            result.setErrors(errors);
            Log.e(TAG, e.toString());
        }

        private void processJSONResponse(InputStream content) throws IOException,
                InterruptedException, JsonSyntaxException {
            JsonReader reader = new JsonReader(new InputStreamReader(content, "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                JsonToken token = reader.peek();
                switch (token) {
                    case BEGIN_OBJECT:
                        reader.beginObject();
                        break;
                    case END_OBJECT:
                        reader.endObject();
                        break;
                    case NAME:
                        String name = reader.nextName();
                        processRootObject(reader, name);
                        break;
                    case END_DOCUMENT:
                        //store last sync date only json fully parsed
                        PreferencesManager
                                .getInstance()
                                .setLong(TriageSync.KEY_LAST_SYNC_DATE, lastUpdateDate);
                        return;
                }
            }
            reader.close();
        }

        private void processRootObject(JsonReader reader,
                                       String name) throws IOException, InterruptedException {
            Class cls;
            final ExportRootNode exportRootNode;
            Uri uri;
            try {
                exportRootNode = ExportRootNode.valueOf(name);
                if (ExportRootNode.lastUpdatedDate.equals(exportRootNode)) {
                    lastUpdateDate = reader.nextLong();
                } else {
                    switch (exportRootNode) {
                        case bodyPart:
                            cls = BodyPart.class;
                            uri = BodyPartSchema.CONTENT_URI;
                            break;
                        case outcome:
                            cls = Outcome.class;
                            uri = OutcomeSchema.CONTENT_URI;
                            break;
                        case symptoms:
                            cls = Symptom.class;
                            uri = SymptomSchema.CONTENT_URI;
                            break;
                        case map:
                            cls = SymptomHash.class;
                            uri = SymptomHashSchema.CONTENT_URI;
                            break;
                        case bodyPartColor:
                            cls = BodyPartColor.class;
                            uri = BodyPartColorSchema.CONTENT_URI;
                            break;
                        case data:
                        default:
                            cls = null;
                            uri = null;
                    }
                    processModelsArray(reader, cls, uri);
                }
            } catch (IllegalArgumentException e) {
                L.e(TAG, "Error in Export stream parsing; Verify JSON fields: " + e.toString(), e);
            }
        }

        private void processModelsArray(JsonReader reader, Class cls, Uri uri) throws IOException,
                InterruptedException, JsonSyntaxException {
            if (cls != null && uri != null) {
                checkSyncInterruption();

                String table = BabylonContentProvider.getTable(uri);
                int index = 0;

                reader.beginArray();

                SQLiteDatabase writableDatabase = CheckDatabaseHelper.getInstance(App.getInstance())
                        .getWritableDatabase();
                writableDatabase.beginTransaction();

                while (reader.hasNext()) {
                    StateContentValues stateContentValues = getContentValues(reader, cls);

                    ContentValues contentValues = stateContentValues.getContentValues();
                    String id = contentValues.getAsString(ID_KEY);
                    switch (stateContentValues.getState()) {
                        case BaseExportModel.STATE_NEW:
                            writableDatabase.insert(table, null, contentValues);
                            break;
                        case BaseExportModel.STATE_UPDATE:
                            writableDatabase.update(table, contentValues,
                                    "id=?", new String[]{id});
                            break;
                        case BaseExportModel.STATE_DELETE:
                            writableDatabase.delete(table, "id=?", new String[]{id});
                            break;
                        default:
                            continue;
                    }

                    index++;
                    if (index == MAX_OPERATION_SIZE) {
                        writableDatabase.setTransactionSuccessful();
                        writableDatabase.endTransaction();

                        index = 0;
                        writableDatabase.beginTransaction();
                        System.gc();
                    }
                }

                if (index != 0) {
                    writableDatabase.setTransactionSuccessful();
                }
                writableDatabase.endTransaction();
                reader.endArray();
            }
        }

        private StateContentValues getSymptomHashContentValues(JsonReader reader)
                throws IOException {
            int fieldSize = 4;
            StateContentValues stateContentValues = new StateContentValues();
            ContentValues contentValues = new ContentValues(fieldSize);

            reader.beginObject();
            while (reader.hasNext()) {
                try {
                    String name = reader.nextName();
                    switch (name) {
                        case STATE_KEY:
                            stateContentValues.setState(reader.nextInt());
                            break;
                        case ID_KEY:
                        case HASH_KEY:
                        case BODY_PART_KEY:
                        case OUTCOME_KEY:
                            contentValues.put(name, reader.nextString());
                            break;
                        default:
                            reader.skipValue();
                    }
                } catch (IllegalStateException e) {
                    reader.skipValue();
                }
            }
            reader.endObject();

            stateContentValues.setContentValues(contentValues);
            return stateContentValues;
        }

        /**
         * model.toContentValues() method based on reflection, which slowly then manual creating
         * of contentValues. For rows which often meets from one class we need manual creating
         *
         * @param reader json reader
         * @return ContentValues object
         */
        private StateContentValues getContentValues(JsonReader reader, Class cls)
                throws IOException {
            StateContentValues contentValues;
            if (cls == SymptomHash.class) {
                contentValues = getSymptomHashContentValues(reader);
            } else {
                BaseExportModel model = getGson().fromJson(reader, cls);
                contentValues = new StateContentValues();
                contentValues.setState(model.getState());
                contentValues.setContentValues(model.toContentValues());
            }
            return contentValues;
        }

        class StateContentValues {
            private int state;
            private ContentValues contentValues;

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public ContentValues getContentValues() {
                return contentValues;
            }

            public void setContentValues(ContentValues contentValues) {
                this.contentValues = contentValues;
            }
        }

        private void checkSyncInterruption() throws InterruptedException {
            boolean isThreadInterrupted = Thread.currentThread().isInterrupted();
            if (isThreadInterrupted) {
                throw new InterruptedException("Triage sync is interrupted");
            }
        }
    }
}
