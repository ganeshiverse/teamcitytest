package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by grajarapu on 8/20/2015.
 */
public class MsgInboxAndSentSearchLoader extends DataAsyncTaskLibLoader<MessagesInboxAndSentResponse> {

    private String token;
    private String panelId;
    private String patientId;
    private String filterCriteria;
    private int start;
    private int max;
    private int IsDictionaryJSONResponse;

    public MsgInboxAndSentSearchLoader(Context context, String token, String panelId, String patientId, int start, int max, int IsDictionaryJSONResponse, String FilterCriteria) {
        super(context);
        this.token = token;
        this.panelId = panelId;
        this.patientId = patientId;
        this.start = start;
        this.max = max;
        this.IsDictionaryJSONResponse = IsDictionaryJSONResponse;
        this.filterCriteria = FilterCriteria;
    }

    @Override
    protected MessagesInboxAndSentResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMessageSentSearchContent(token, panelId, patientId, start, max, IsDictionaryJSONResponse, filterCriteria);
    }
}