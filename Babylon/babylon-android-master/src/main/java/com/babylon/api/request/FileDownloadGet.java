package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.model.FileEntry;
import com.babylon.utils.L;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class FileDownloadGet extends AbsRequestGet<FileEntry> {
    public static final String TAG = FileDownloadGet.class.getSimpleName();

    public static final String FILE_ID = "fileId";

    @Override
    protected String getUrl() {
        String fileId = getParam(params, FILE_ID);
        String url = Urls.get(Urls.DOWNLOAD_FILE_GET);
        return String.format(url, fileId);
    }

    @Override
    protected Response<FileEntry> processResponse(HttpResponse httpResponse) {
        final FileEntry fileEntry = new FileEntry();
        Response<FileEntry> result = new Response<FileEntry>();
        try {
            final byte[] data = EntityUtils.toByteArray(httpResponse.getEntity());
            fileEntry.setData(data);
            result.setData(fileEntry);
            result.setStatus(Response.RESULT_OK);
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }
        return result;
    }
}
