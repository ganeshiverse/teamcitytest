package com.babylon.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.PasswordConfirmActivity;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Account;
import com.babylon.utils.CommonHelper;
import com.babylon.utils.VolleySingleton;
import com.babylonpartners.babylon.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PasswordScreenUserSelectAdapter extends RecyclerView.Adapter<PasswordScreenUserSelectAdapter.PersonViewHolder>
{

    protected ImageLoader loader;
    public static PasswordConfirmActivity mPasswordActivityInstance;
    public int mSelectedItem = -1;
    private List<Account> mListOfAccounts = new ArrayList<>();
    public static int mSelectedPosition = -1;
    private int KEY = 1;
    private Context mContext;
    private String mUri;

    public PasswordScreenUserSelectAdapter(Context context, List<Account> usersList)
    {
        mPasswordActivityInstance = (PasswordConfirmActivity)context;
        loader = VolleySingleton.getInstance(context).getImageLoader();
        mListOfAccounts = usersList;
        mContext = context;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.password_user_select_layout, parent, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position)
    {
        holder.personPhoto.setSelected(position == mSelectedItem);
        holder.personName.setText(mListOfAccounts.get(position).getFirstName());
       // holder.personPhoto.setImageResource(R.drawable.profile_avatar);
        holder.tick.setVisibility(View.GONE);

        //Get User Avatar
        if (mListOfAccounts.get(position).getAvatar() != null)
        {
            byte[] decodedString = Base64.decode(mListOfAccounts.get(position).getAvatar(), Base64.DEFAULT);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            CircleTransform xform = new CircleTransform();
            Bitmap circleBitmap = xform.transform(decodedBitmap);
            decodedBitmap.recycle();
            holder.personPhoto.setImageBitmap(circleBitmap);
        }
        else
        {
            //holder.personPhoto.setImageResource(R.drawable.profile_avatar);
            getAvatarImage(holder,mListOfAccounts.get(position).getId());
            //Picasso.with(mContext).load("http://qa1.babylontesting.co.uk/api/v1/accounts/3646/avatar_image").into(holder.personPhoto);
        }
        holder.personPhoto.setTag(mListOfAccounts.get(position));
        holder.personPhoto.setTag(R.string.KEY,position);
        holder.tick.setTag(R.string.KEY,position);
        toggleSelections(position, holder);
    }

    public void toggleSelections(int position,PersonViewHolder holder)
    {
        if(position == mSelectedPosition)
        {
           holder.tick.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount()
    {
        return mListOfAccounts.size();
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder
    {

        TextView personName;
        ImageView personPhoto;
        ImageView tick;

        PersonViewHolder(View itemView)
        {
            super(itemView);
            personName = (TextView)itemView.findViewById(R.id.password_select_patient_name);
            personPhoto = (ImageView)itemView.findViewById(R.id.password_select_patient_picture);
            tick = (ImageView) itemView.findViewById(R.id.password_select_patient_picture_tick);

            personPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    Account clickedUserAccount = (Account) personPhoto.getTag();
                    PasswordConfirmActivity.mCurrentUserId = clickedUserAccount.getId();

                    //mSelectedItem = getAdapterPosition();
                     //notifyDataSetChanged();
                    if (personPhoto.isSelected())
                    {
                        mPasswordActivityInstance.userUnSelected();
                        personPhoto.setSelected(false);
                        tick.setVisibility(View.GONE);
                        tick.setSelected(false);
                        //personPhoto.setImageResource(R.drawable.profile_avatar);
                    }
                    else
                    {
                        mPasswordActivityInstance.userSelected(clickedUserAccount);
                        //notifyDataSetChanged();
                       // personPhoto.setImageResource(R.drawable.btn_ask_tick);
                        personPhoto.setSelected(true);
                        tick.setVisibility(View.VISIBLE);
                        tick.setSelected(true);
                        mSelectedPosition = (Integer) personPhoto.getTag(R.string.KEY);
                        notifyDataSetChanged();
                    }
                }
            });

            tick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (tick.isSelected())
                    {
                        tick.setVisibility(View.GONE);
                        tick.setSelected(false);
                    }
                    else
                    {
                        tick.setSelected(true);
                        mSelectedPosition = (Integer) tick.getTag(R.string.KEY);
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void getAvatarImage(final PersonViewHolder holder,String id)
    {
        mUri = Urls.getRuby(String.format("/api/v1/accounts/%s/avatar_image",id));
        // mUri = "http://qa1.babylontesting.co.uk/api/v1/accounts/3646/avatar_image";

        ImageRequest jsonObjReq = new ImageRequest(mUri,new com.android.volley.Response.Listener<Bitmap>()
        {
            @Override
            public void onResponse(Bitmap response)
            {
                CircleTransform xform = new CircleTransform();
                Bitmap circleBitmap = xform.transform(response);
                response.recycle();
                holder.personPhoto.setImageBitmap(circleBitmap);
            }
        }, 200, 200,
                Bitmap.Config.RGB_565,new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Adapter", "Error");
                holder.personPhoto.setImageResource(R.drawable.profile_avatar);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "image/*");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }
    }

}