package com.iversecomics.client.util;

import android.graphics.RectF;

public class Dim {
    public static final Dim PAGE                   = new Dim(933, 1400);
    public static final Dim PANEL                  = new Dim(480, 320);

    public static final Dim FEATURED               = new Dim(149, 85);
    public static final Dim FEATURED_LARGE         = new Dim(768, 353);

    public static final Dim COVER_SMALL            = new Dim(28, 42);
    public static final Dim COVER_MEDIUM           = new Dim(58, 87);
    public static final Dim COVER_TABLET_MEDIUM    = new Dim(150, 225);
    public static final Dim COVER_LARGE            = new Dim(320, 480);
    public static final Dim COVER_TABLET_LARGE     = new Dim(683, 1024);

    public static final Dim PUBLISHER_ICON         = new Dim(47, 45);
    public static final Dim PUBLISHER_BANNER       = new Dim(320, 73);
    public static final Dim PUBLISHER_BANNER_LARGE = new Dim(768, 122);

    public static final Dim FEATURED_SLOT          = new Dim(222, 137);
    public static final Dim FEATURED_SLOT_LARGE    = new Dim(444, 274);

    private int             width;
    private int             height;

    public Dim(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public RectF toRectF() {
        return new RectF(0, 0, width, height);
    }

    @Override
    public String toString() {
        return "Dim[" + width + " x " + height + "]";
    }
}
