package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bhargav.kadi on 7/29/2015.
 */
public class MessagesInboxAndSentResponse extends BaseResponse implements Serializable {

    @SerializedName("DatacardId")
    private String datacardId;

    @SerializedName("Panel")
    private PanelObject panel;

    public String getDatacardId() {
        return datacardId;
    }

    public PanelObject getPanel() {
        return panel;
    }

    public class PanelObject implements Serializable {
        @SerializedName("Id")
        private int id;

        @SerializedName("Name")
        private String name;

        @SerializedName("RowCount")
        private int rowCount;

        @SerializedName("data")
        private ArrayList<DataObject> dataObjectsAL;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getRowCount() {
            return rowCount;
        }

        public ArrayList<DataObject> getDataObjectsAL() {
            return dataObjectsAL;
        }
    }

    public class DataObject implements Serializable {
        @SerializedName("Attachments")
        private String attachments;

        @SerializedName("Header")
        private String header;

        @SerializedName("ID")
        private String id;

        @SerializedName("Message")
        private String message;

        @SerializedName("Photourl")
        private String photourl;

        @SerializedName("Priority")
        private String priority;

        @SerializedName("ReadStatus")
        private String readStatus;

        @SerializedName("ReceivedOn")
        private String receivedOn;

        @SerializedName("SentDate")
        private String sentDate;

        @SerializedName("SentTo")
        private String SentTo;

        @SerializedName("Sender")
        private String sender;

        @SerializedName("Speciality")
        private String speciality;

        @SerializedName("Subject")
        private String subject;

        @SerializedName("Usertype")
        private String usertype;

        public String getUsertype() {
            return usertype;
        }

        public String getAttachments() {
            return attachments;
        }

        public String getId() {
            return id;
        }

        public String getMessage() {
            return message;
        }

        public String getPhotourl() {
            return photourl;
        }

        public String getPriority() {
            return priority;
        }

        public String getReadStatus() {
            return readStatus;
        }

        public String getReceivedOn() {
            return receivedOn;
        }

        public String getSender() {
            return sender;
        }

        public String getSpeciality() {
            return speciality;
        }

        public String getSubject() {
            return subject;
        }

        public String getSentTo() {
            return SentTo;
        }

        public String getHeader() {
            return header;
        }

        public String getSentDate() {
            return sentDate;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }


    }
}
