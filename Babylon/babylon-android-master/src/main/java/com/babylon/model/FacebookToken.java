package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class FacebookToken {
    @SerializedName("facebook_access_token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
