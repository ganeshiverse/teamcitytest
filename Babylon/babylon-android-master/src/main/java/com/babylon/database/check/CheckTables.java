package com.babylon.database.check;

import com.babylon.database.ITable;
import com.babylon.database.schema.check.BodyPartColorSchema;
import com.babylon.database.schema.check.BodyPartSchema;
import com.babylon.database.schema.check.OutcomeSchema;
import com.babylon.database.schema.check.SymptomHashSchema;
import com.babylon.database.schema.check.SymptomSchema;

public enum CheckTables implements ITable {
    BODY_PART("body_part", BodyPartSchema.class),
    SYMPTOM("symptom", SymptomSchema.class),
    SYMPTOM_HASHES("symptom_hash", SymptomHashSchema.class),
    OUTCOME("outcome", OutcomeSchema.class),
    BODY_PART_COLOR("body_part_color", BodyPartColorSchema.class);

    private String name;
    private Class schema;

    private CheckTables(String name, Class schema) {
        this.name = name;
        this.schema = schema;
    }

    public String getName() {
        return name;
    }

    public Class getSchema() {
        return schema;
    }

    public String toString() {
        return name;
    }
}
