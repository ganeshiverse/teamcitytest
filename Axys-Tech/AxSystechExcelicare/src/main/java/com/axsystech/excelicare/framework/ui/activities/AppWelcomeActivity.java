package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.data.responses.RegistrationDetailsByUserResponse;
import com.axsystech.excelicare.data.responses.UsersDetailsObject;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.loaders.CacheDeviceUsersLoader;
import com.axsystech.excelicare.loaders.CacheSystemPreferencesLoader;
import com.axsystech.excelicare.loaders.GetRegistrationDetailsByUserLoader;
import com.axsystech.excelicare.loaders.GetSiteListLoader;
import com.axsystech.excelicare.loaders.RegisterLoader;
import com.axsystech.excelicare.loaders.SaveUserDetailsAndClientUrlDBLoader;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.NavigationUtils;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * Date: 06.05.14
 * Time: 13:38
 *
 * @author SomeswarReddy
 */
public class AppWelcomeActivity extends BaseActivity implements View.OnClickListener, ConfirmationDialogFragment.OnConfirmListener {

    //    private static final String CACHE_DEVICE_USERS_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.CACHE_DEVICE_USERS_LOADER";
    private static final String CACHE_USER_DETAILS_CLIENT_URL_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.CACHE_USER_DETAILS_CLIENT_URL_LOADER";
    private static final String CACHE_SYSTEM_PREFERENCES_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.CACHE_SYSTEM_PREFERENCES_LOADER";
    //    private static final String REGISTRATION_DETAILS_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.REGISTRATION_DETAILS_LOADER";
    private static final String REGISTRATION_DETAILS_BY_USER_LOADER = "com.axsystech.excelicare.framework.ui.activities.AppWelcomeActivity.REGISTRATION_DETAILS_BY_USER_LOADER";

    private String TAG = AppWelcomeActivity.class.getSimpleName();
    private AppWelcomeActivity mActivity;

    private static final int SIGNUP_CONFIRMATION_DIALOG_ID = 102;
    private static final int SETUP_CANCELLED_DIALOG_ID = 103;
    private static final int PRIVACY_POLICY_REQUEST_CODE = 104;

    @InjectView(R.id.topLogoImageView)
    private ImageView mTopLogoImageView;

    @InjectView(R.id.infoImageView)
    private ImageView mInfoImageView;

    @InjectView(R.id.siteSelectorLinkcheckedTextView)
    private CheckBox siteSelectorLinkchecktextView;

    //@InjectView(R.id.emailIdEditText)
    private EditText mEmailIdEditText;

    //@InjectView(R.id.privacyPolicyLinkTextView)
    private TextView privacyPolicyLinkTextView;

    @InjectView(R.id.siteSelectorLinkTextView)
    private TextView siteSelectorLinkTextView;

    //@InjectView(R.id.continueBtn)
    private TextView mContinueBtn;

    //@InjectView(R.id.cancelBtn)
    private TextView mCancelBtn;

    TextView mChooseSiteTextView;

    //    private RegisterResponse registerResponse;
    private RegistrationDetailsByUserResponse registerResponse;

    private String mInputEmailID = "";

    @Override
    protected void initActionBar() {

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_registration);
        mActivity = this;

        readIntentData();
        addEventListeners();
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
           /* if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }

            if (!TextUtils.isEmpty(mInputEmailID)) {
                mEmailIdEditText.setText(mInputEmailID);
            }*/
        }
    }

    private void addEventListeners() {

        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        mContinueBtn = (TextView) findViewById(R.id.continueBtn);
        mCancelBtn = (TextView) findViewById(R.id.cancelBtn);
        mChooseSiteTextView = (TextView) findViewById(R.id.chooseSiteTextView);
        privacyPolicyLinkTextView = (TextView) findViewById(R.id.privacyPolicyLinkTextView);
        mEmailIdEditText = (EditText) findViewById(R.id.emailIdEditText);

        mContinueBtn.setTypeface(bold);
        mCancelBtn.setTypeface(bold);
        mChooseSiteTextView.setTypeface(regular);
        privacyPolicyLinkTextView.setTypeface(regular);
        mEmailIdEditText.setTypeface(regular);

        mCancelBtn.setOnClickListener(this);
        mContinueBtn.setOnClickListener(this);
        privacyPolicyLinkTextView.setOnClickListener(this);
        siteSelectorLinkTextView.setOnClickListener(this);
        siteSelectorLinkchecktextView.setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.continueBtn:
                CommonUtils.hideSoftKeyboard(this);
                if (validateUserInputs()) {
                    displayProgressLoader(false);

                    // should pass dynamic site id once 'Site' field added in Screen
//                    final RegisterLoader registerActivity = new RegisterLoader(this, CommonUtils.getDeviceId(getApplicationContext()),
//                                                                      "101", getStringTextView(mEmailIdEditText));
//                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_LOADER), registerActivity);

                    // According to the new registration we should call 'GetRegistrationDetailsByUser' API
                    if (!isFinishing()) {
                        final GetRegistrationDetailsByUserLoader registrationDetailsByUserLoader = new GetRegistrationDetailsByUserLoader(this,
                                getStringTextView(mEmailIdEditText), CommonUtils.getDeviceId(getApplicationContext()),
                                AxSysConstants.DEFAULT_SITE_ID, "0");
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER), registrationDetailsByUserLoader);
                    }
                }
                break;

            case R.id.cancelBtn:
                CommonUtils.hideSoftKeyboard(this);
                // Alert user for signup confirmation
                final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.setup_cancelled_quit));
                bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, SETUP_CANCELLED_DIALOG_ID);
                bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                confirmationDialogFragment.setArguments(bundle);
                showDialog(confirmationDialogFragment);
                break;

            case R.id.privacyPolicyLinkTextView:
                Intent privacyPolicyIntent = new Intent(this, LoginPricavyPolicy.class);
                privacyPolicyIntent.putExtra(AxSysConstants.EXTRA_JUST_DISPLAY_PRIVACY_POLICY, true);
                startActivityForResult(privacyPolicyIntent, PRIVACY_POLICY_REQUEST_CODE);
                break;

            case R.id.siteSelectorLinkTextView:
                siteSelectorLinkTextView.setVisibility(View.GONE);
                siteSelectorLinkchecktextView.setVisibility(View.VISIBLE);
                CommonUtils.hideSoftKeyboard(this);
                if (validateUserInputs()) {
                    displayProgressLoader(false);

                    if (!isFinishing()) {
                        final GetRegistrationDetailsByUserLoader registrationDetailsByUserLoader = new GetRegistrationDetailsByUserLoader(this,
                                getStringTextView(mEmailIdEditText), CommonUtils.getDeviceId(getApplicationContext()),
                                AxSysConstants.DEFAULT_SITE_ID, "1");
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER), registrationDetailsByUserLoader);
                    }
                }
                break;
            case R.id.siteSelectorLinkcheckedTextView:
                siteSelectorLinkchecktextView.setVisibility(View.INVISIBLE);
                siteSelectorLinkTextView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        if (id == getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER)) {
            showToast(R.string.invalid_server_response);
        } else if (id == getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER)) {
            // Start caching System Preference details in DB so that it will be used in offline mode
            if (registerResponse.getDataObject() != null &&
                    registerResponse.getDataObject().getSystemPreferences() != null &&
                    registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL() != null &&
                    registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL().size() > 0) {

                CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
            } else {
                // Here decide navigation to further screens based on 'NavigateTo' value received from server
                decideNavigation(registerResponse);
            }
        } else if (id == getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER)) {
            // Here decide navigation to further screens based on 'NavigateTo' value received from server
            decideNavigation(registerResponse);
        }/* else if(id == getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_LOADER)) {
            showToast(R.string.invalid_server_response);
        } else if(id == getLoaderHelper().getLoaderId(CACHE_DEVICE_USERS_LOADER)) {
            // Here decide navigation to further screens based on 'NavigateTo' value received from server
            decideNavigation(registerResponse);
        } */
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_BY_USER_LOADER)) {
            if (result != null && result instanceof RegistrationDetailsByUserResponse) {
                registerResponse = (RegistrationDetailsByUserResponse) result;

                // Cache user details in device DB and it will be used in Login screen where we will fetch Client Url for the selected Site ID
                if (registerResponse.getDataObject() != null &&
                        registerResponse.getDataObject().getUserDetailsAL() != null &&
                        registerResponse.getDataObject().getUserDetailsAL().size() > 0) {

                    SaveUserDetailsAndClientUrlDBLoader saveUserDetailsAndClientUrlDBLoader = new SaveUserDetailsAndClientUrlDBLoader(this,
                            mEmailIdEditText.getText().toString().trim(), registerResponse.getDataObject().getUserDetailsAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER), saveUserDetailsAndClientUrlDBLoader);

                } else if (registerResponse.getDataObject() != null &&
                        registerResponse.getDataObject().getSystemPreferences() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL().size() > 0) {

                    // Start caching System Preference details in DB so that it will be used in offline mode
                    CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                            registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
                } else {
                    // Here decide navigation to further screens based on 'NavigateTo' value received from server
                    decideNavigation(registerResponse);
                }
            }

        } else if (id == getLoaderHelper().getLoaderId(CACHE_USER_DETAILS_CLIENT_URL_LOADER)) {
            if (result != null && result instanceof Boolean) {
                Boolean statusFlag = (Boolean) result;

                // Start caching System Preference details in DB so that it will be used in offline mode
                if (registerResponse.getDataObject() != null &&
                        registerResponse.getDataObject().getSystemPreferences() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL() != null &&
                        registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL().size() > 0) {

                    CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                            registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
                } else {
                    // Here decide navigation to further screens based on 'NavigateTo' value received from server
                    decideNavigation(registerResponse);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                ArrayList<SystemPreferenceDBModel> systemPreferenceDBModelAL = (ArrayList<SystemPreferenceDBModel>) result;

                // Cache SystemPreference data in Global Object
                GlobalDataModel.getInstance().setSystemPreferencesAL(systemPreferenceDBModelAL);
            }

            // Here decide navigation to further screens based on 'NavigateTo' value received from server
            decideNavigation(registerResponse);
        } /*else  if (id == getLoaderHelper().getLoaderId(REGISTRATION_DETAILS_LOADER)) {

            if(result != null && result instanceof  RegisterResponse) {
                registerResponse = (RegisterResponse) result;
                // Need to cache the Client URL that we need to use for Login Request
                try {
                    // Cache ClientURL in Global Object
//                    ServerMethods.getInstance().setGlobalClientUrl(registerResponse.getClientUrl());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                // Cache the Device Users in DB so that it will be used in offline mode
                CacheDeviceUsersLoader cacheSystemConfigsLoader = new CacheDeviceUsersLoader(mActivity, registerResponse);
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_DEVICE_USERS_LOADER), cacheSystemConfigsLoader);
            }

        } else if(id == getLoaderHelper().getLoaderId(CACHE_DEVICE_USERS_LOADER)) {
            if (result != null && result instanceof Boolean) {
                boolean statusFlag = (Boolean) result;

//                if(statusFlag) {
                    // Start caching System Preference details in DB so that it will be used in offline mode
                    CacheSystemPreferencesLoader cacheSystemPreferencesLoader = new CacheSystemPreferencesLoader(mActivity,
                    registerResponse.getDataObject().getSystemPreferences().getSystemPreferencesDataAL());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CACHE_SYSTEM_PREFERENCES_LOADER), cacheSystemPreferencesLoader);
//                }
            }
        }*/
    }

    /**
     * This method will be used to decide navigation based on 'NavigateTo' flag we received from server.
     * <p/>
     * 1- Login
     * 2- Change Password
     * 3- Signup
     * 4- Verify OTP
     * 5- Site Selector
     *
     * @param registerResponse
     */
    private void decideNavigation(RegistrationDetailsByUserResponse registerResponse) {
        try {
            if (registerResponse != null) {
                if (!TextUtils.isEmpty(registerResponse.getDataObject().getNavigateTo())) {
                    if (NavigationUtils.LOGIN.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        startActivity(loginIntent);
                        finish();

                    } else if (NavigationUtils.CHANGE_PASSWORD.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to CHANGE PASSWORD screen
                        Intent changePwdIntent = new Intent(this, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_change_password));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, AxSysConstants.DEFAULT_SITE_ID);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, getTokenForGivenUser(getStringTextView(mEmailIdEditText), registerResponse));
                        startActivity(changePwdIntent);

                    } else if (NavigationUtils.SIGNUP.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Alert user for signup confirmation
                        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                        final Bundle bundle = new Bundle();
                        bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.signup_confirmation_message));
                        bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, SIGNUP_CONFIRMATION_DIALOG_ID);
                        bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                        confirmationDialogFragment.setArguments(bundle);
                        showDialog(confirmationDialogFragment);

                    } else if (NavigationUtils.VERIFY_OPT.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to CHANGE PASSWORD screen for Verifying OTP
                        Intent changePwdIntent = new Intent(this, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_verify_otp));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, AxSysConstants.DEFAULT_SITE_ID);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, getTokenForGivenUser(getStringTextView(mEmailIdEditText), registerResponse));
                        startActivity(changePwdIntent);

                    } else if (NavigationUtils.SITE_SELECTOR.containsString(registerResponse.getDataObject().getNavigateTo())) {
                        // Navigate user to Site Selector Screen
                        Intent siteSelctionIntent = new Intent(this, SiteSelectorActivity.class);
                        siteSelctionIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.registered_sites_text));
                        siteSelctionIntent.putExtra(AxSysConstants.EXTRA_NAVIGATE_TO, NavigationUtils.SIGNUP.toString());
                        siteSelctionIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        startActivity(siteSelctionIntent);
                    } else {
                        // IMPORTANT: Default case
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        startActivity(loginIntent);
                        finish();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method will return the token for the given user mail id. FYI, this token value will be there
     * in UserDetails Data object, see 'GetRegistrationDetailsByUser' response for more information.
     *
     * @param emailId          user email id
     * @param registerResponse registration details response received from server
     * @return token value for the given user email id
     */
    private String getTokenForGivenUser(String emailId, RegistrationDetailsByUserResponse registerResponse) {
        if (registerResponse != null && registerResponse.getDataObject() != null) {
            if (registerResponse.getDataObject().getUserDetailsAL() != null &&
                    registerResponse.getDataObject().getUserDetailsAL().size() > 0) {

                ArrayList<UsersDetailsObject> userDetailsAL = registerResponse.getDataObject().getUserDetailsAL();
                for (int idx = 0; idx < userDetailsAL.size(); idx++) {
                    UsersDetailsObject indexedObj = userDetailsAL.get(idx);

                    if (!TextUtils.isEmpty(indexedObj.getLoginName()) && indexedObj.getLoginName().trim().equalsIgnoreCase(emailId)) {
                        return (!TextUtils.isEmpty(indexedObj.getToken())) ? indexedObj.getToken() : "";
                    }
                }
            }
        }

        return "";
    }

    /**
     * This method will be used to navigate to Privacy Policy or SignUp page based on the System Preference
     * value.
     */
    private void navigateToPrivacyPolicyOrSignUp() {
        if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                CommonUtils.shouldDisplayPrivacyPolicy(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            // Navigate to PRIVACY POLICY screen
            Intent signUpIntent = new Intent(this, PrivacyPolicyActivity.class);
            signUpIntent.putExtra(AxSysConstants.EXTRA_NAVIGATE_TO, NavigationUtils.SIGNUP.toString());
            signUpIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, AxSysConstants.DEFAULT_SITE_ID);
            signUpIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
            startActivity(signUpIntent);
        } else {
            // Navigate user to SIGNUP screen
            Intent signUpIntent = new Intent(this, SignUpActivity.class);
            signUpIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
            signUpIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, AxSysConstants.DEFAULT_SITE_ID);
            startActivity(signUpIntent);
        }
    }

    /**
     * This method will be used to validate user inputs.
     *
     * @return true or false based on mandatory field conditions
     */
    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(mEmailIdEditText.getText().toString().trim())) {
            showToast(R.string.input_email);
            siteSelectorLinkchecktextView.setVisibility(View.INVISIBLE);
            siteSelectorLinkTextView.setVisibility(View.VISIBLE);
            mEmailIdEditText.requestFocus();

            return false;
        }

        if (!TextUtils.isEmpty(mEmailIdEditText.getText().toString().trim()) &&
                !CommonUtils.isValidEmail(mEmailIdEditText.getText().toString().trim())) {
            showToast(R.string.input_valid_email);
            mEmailIdEditText.requestFocus();

            return false;
        }

        return true;
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == SIGNUP_CONFIRMATION_DIALOG_ID) {
            // Check SystemPreferences in-order to navigate Privacy Policy or SignUp Screen
            navigateToPrivacyPolicyOrSignUp();
        } else if (dialogId == SETUP_CANCELLED_DIALOG_ID) {
            CommonUtils.quitApplication(this);
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == SIGNUP_CONFIRMATION_DIALOG_ID) {

        } else if (dialogId == SETUP_CANCELLED_DIALOG_ID) {

        }
    }

    /*
    private void decideNavigation(RegisterResponse registerResponse) {
        try {
            if (registerResponse != null) {
                if(!TextUtils.isEmpty(registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getNavigateTo())) {
                    if(NavigationUtils.LOGIN.containsString(registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getNavigateTo())) {
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        startActivity(loginIntent);
                        finish();

                    } else if(NavigationUtils.CHANGE_PASSWORD.containsString(registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getNavigateTo())) {
                        // Navigate user to CHANGE PASSWORD screen
                        Intent changePwdIntent = new Intent(this, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_change_password));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getToken());
                        startActivity(changePwdIntent);

                    } else if(NavigationUtils.SIGNUP.containsString(registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getNavigateTo())) {
                        // Alert user for signup confirmation
                        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                        final Bundle bundle = new Bundle();
                        bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.signup_confirmation_message));
                        bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, SIGNUP_CONFIRMATION_DIALOG_ID);
                        bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                        confirmationDialogFragment.setArguments(bundle);
                        showDialog(confirmationDialogFragment);

                    } else if(NavigationUtils.VERIFY_OPT.containsString(registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getNavigateTo())) {
                        // Navigate user to CHANGE PASSWORD screen for Verifying OTP
                        Intent changePwdIntent = new Intent(this, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_verify_otp));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        // TODO remove this before going to release in market
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_VERIFY_TOKEN, registerResponse.getDataObject().getCurrentUser().getCurrentUserData().getToken());
                        startActivity(changePwdIntent);

                    } else {
                        // IMPORTANT: Default case
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, getStringTextView(mEmailIdEditText));
                        startActivity(loginIntent);
                        finish();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } */
}
