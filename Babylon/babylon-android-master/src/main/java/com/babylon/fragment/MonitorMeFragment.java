package com.babylon.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.format.DateUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.MonitorMeAdapter;
import com.babylon.database.schema.MonitorMeReportSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.enums.TimeInterval;
import com.babylon.fragment.nutrient.ConsumedNutrientsByCategoryFragment;
import com.babylon.fragment.stress.SleepChartFragment;
import com.babylon.fragment.stress.StressChartFragment;
import com.babylon.model.MonitorMeCategory;
import com.babylon.model.MonitorMeLightFeatures;
import com.babylon.model.MonitorMeReport;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Show monitor me dashboard with all medical properties
 */

public class MonitorMeFragment extends BaseFragment {

    private static final int LOADER_ID = 101;
    private static final String EXTRA_DAY_NUMBER_AGO = "EXTRA_DAY_NUMBER_AGO";
    private static final String TAG = MonitorMeFragment.class.getSimpleName();

    private ListView monitorMeListView;
    private MonitorMeAdapter adapter;

    /**
     * Monitor report is data for show view in list, which is holder for
     * static and dynamic monitor report data
     */
    private MonitorMeReport monitorReport;

    public static MonitorMeFragment getInstance(int dayNumberAgo) {
        final Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_DAY_NUMBER_AGO, dayNumberAgo);

        final MonitorMeFragment fragment = new MonitorMeFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monitor_me, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        monitorMeListView = (ListView) view.findViewById(R.id.fragment_monitor_me_list_view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadStaticMonitorReportData();
        getLoaderManager().restartLoader(LOADER_ID, null, monitorMeCallback);
    }

    @Override
    protected void onTopBarButtonPressed() {
        getActivity().finish();
    }

    private void loadStaticMonitorReportData() {
        List<MonitorMeCategory> monitorCategories = PreferencesManager.getInstance()
                .getMonitorCategories();
        monitorReport = new MonitorMeReport();
        monitorReport.setCategories(monitorCategories);
    }

    private void showCategories(List<MonitorMeCategory> categories) {

        if (categories != null) {
            if (adapter == null) {
                adapter = new MonitorMeAdapter(getActivity(), categories);
                adapter.setOnPropertyItemClickListener(getOnPropertyItemClickListener());
                monitorMeListView.setAdapter(adapter);
            } else {
                adapter.setNewData(categories);
                adapter.notifyDataSetChanged();
            }
        }
    }

    /* Click listener for monitor me properties */
    private MonitorMeAdapter.OnPropertyItemClickListener getOnPropertyItemClickListener() {
        return new MonitorMeAdapter.OnPropertyItemClickListener() {
            @Override
            public View.OnClickListener getOnPropertyClickListener(final int featureId,
                                                                   final String paramName,
                                                                   final String description) {
                long beginOfTheDay = getBeginOfTheDay();
                return getOnClickListener(featureId, paramName, description, getActivity(),
                        beginOfTheDay);
            }
        };
    }

    private LoaderManager.LoaderCallbacks<Cursor> monitorMeCallback = new LoaderManager
            .LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            final long beginOfTheDay = getBeginOfTheDay();
            final long endOfTheDay = beginOfTheDay + DateUtils.DAY_IN_MILLIS;

            final String selection = MonitorMeReportSchema.Columns.DATE.getName() + " >= ? AND "
                    + MonitorMeReportSchema.Columns.DATE.getName() + " < ?";
            final String[] selectionArgs = new String[]{
                    String.valueOf(beginOfTheDay),
                    String.valueOf(endOfTheDay)
            };

            return new CursorLoader(getActivity(), MonitorMeReportSchema.CONTENT_URI,
                    MonitorMeReportSchema.Query.PROJECTION,
                    selection, selectionArgs, null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            View categoryLayout = getView().findViewById(R.id.layout_category);
            TextView emptyTextView = (TextView) getView().findViewById(android.R.id.empty);
            List<MonitorMeCategory> categories;

            L.d(TAG, "is cursor moveToFirst " + data.moveToFirst());
            if (data.moveToFirst()) {
                if (monitorReport.getCategories() == null) {
                    //we should reload static report because it could be updated via prefs
                    loadStaticMonitorReportData();
                    L.d(TAG, "Categories is empty");
                }
                if (monitorReport.getCategories() != null) {
                    L.d(TAG, "Show data");
                    configViewForShowingCategories(categoryLayout, emptyTextView);

                    final String featuresString = data.getString(MonitorMeReportSchema.Query.FEATURES);
                    final Type listType = new TypeToken<ArrayList<
                            MonitorMeLightFeatures.LightFeature>>() {}.getType();

                    List<MonitorMeLightFeatures.LightFeature> features =
                            GsonWrapper.getTimestampGson().fromJson(featuresString, listType);

                    categories = getCategoriesWithFeatures(features);

                    showCategories(categories);
                } else {
                    showLoadingView(categoryLayout, emptyTextView);
                }
            } else {
                showLoadingView(categoryLayout, emptyTextView);
            }

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    private void showLoadingView(View categoryLayout, TextView emptyTextView) {
        List<MonitorMeCategory> categories;
        showEmptinessDescMessage(emptyTextView);

        categories = new ArrayList<>();
        showCategories(categories);

        if (categoryLayout.getVisibility() != View.GONE) {
            categoryLayout.setVisibility(View.GONE);
        }
    }

    private List<MonitorMeCategory> getCategoriesWithFeatures(List<MonitorMeLightFeatures
            .LightFeature> features) {
        List<MonitorMeCategory> categories = monitorReport.getCategories();
        SparseArray<MonitorMeLightFeatures.LightFeature> lightFeatures = new SparseArray<>();

        for (MonitorMeLightFeatures.LightFeature lightFeature : features) {
            try {
                lightFeatures.append(Integer.parseInt(lightFeature.getId()), lightFeature);
            } catch (NumberFormatException e) {
                L.e(TAG, e.getMessage(), e);
            }
        }

        for (MonitorMeCategory category : categories) {
            for (MonitorMeCategory.Feature feature : category.getFeatures()) {
                MonitorMeLightFeatures.LightFeature lightFeature =
                        lightFeatures.get(feature.getFeatureId());

                if (lightFeature != null) {
                    feature.setState(lightFeature.getState());
                    String targetValue = lightFeature.getTargetValue();
                    if (targetValue != null) {
                        try {
                            feature.setTargetValue(Double.valueOf(targetValue));
                        } catch (NumberFormatException e) {
                            L.e(TAG, e.getMessage(), e);
                        }
                    }
                    feature.setViewValue(lightFeature.getViewValue());
                }
            }
        }

        return categories;
    }

    private void configViewForShowingCategories(View categoryLayout, TextView emptyTextView) {
        if (categoryLayout.getVisibility() != View.VISIBLE) {
            categoryLayout.setVisibility(View.VISIBLE);
        }

        if (emptyTextView.getVisibility() != View.GONE) {
            emptyTextView.setVisibility(View.GONE);
        }
    }

    private void showEmptinessDescMessage(TextView emptyTextView) {
        if (emptyTextView.getVisibility() != View.VISIBLE) {
            emptyTextView.setVisibility(View.VISIBLE);
        }

        if (!App.getInstance().isConnection()) {
            emptyTextView.setText(R.string.offline_error);
        } else {
            emptyTextView.setText(R.string.monitor_me_loading);
        }
    }

    public View.OnClickListener getOnClickListener(final int featureId,
                                                   final String paramName,
                                                   final String description,
                                                   final FragmentActivity activity,
                                                   final long beginOfTheDay) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MonitorMeParam monitorMeParam = getMonitorMeParam(featureId);
                if (monitorMeParam != null) {
                    switch (monitorMeParam) {
                        case CALORIES_BURNT_ID:
                        case DISTANCE_ID:
                            startFragment(BurntCaloriesFragment.getInstance(beginOfTheDay));
                            break;
                        case STEPS_ID:
                        case CYCLE_ID:
                        case WALK_AND_RUN_ID:
                            startFragment(ActivitiesOnlyChartFragment.getInstance(paramName,
                                    description, beginOfTheDay,
                                    monitorMeParam.getId(), true,
                                    TimeInterval.ONE_WEEK.getId()));
                            break;
                        case CALORIES_REMAINING_ID:
                            startFragment(ConsumedNutrientsByCategoryFragment
                                    .getInstance(beginOfTheDay));
                            break;
                        case CALORIES_IN_ID:
                            startFragment(ConsumedNutrientsByCategoryFragment
                                    .getInstance(beginOfTheDay));
                            break;
                        case WEIGHT_ID:
                            startFragment(WeightChartFragment.getInstance(
                                    beginOfTheDay, description));
                            break;
                        case BMI_ID:
                            startFragment(OnlyChartFragment.getInstance(paramName,
                                    monitorMeParam.isMedicalKitExist(), description,
                                    beginOfTheDay, MonitorMeParam.BMI_ID.getId()));
                            break;
                        case BODY_FAT_ID:
                            startFragment(OnlyChartFragment.getInstance(paramName,
                                    monitorMeParam.isMedicalKitExist(), description,
                                    beginOfTheDay, MonitorMeParam.BODY_FAT_ID.getId()));
                            break;
                        case CHOLESTEROL_ID:
                            startFragment(CholesterolChartFragment.getInstance(
                                    beginOfTheDay, MonitorMeParam.CHOLESTEROL_ID.getId(),
                                    R.string.total_cholesterol, description,
                                    monitorMeParam.isMedicalKitExist()));
                            break;
                        case CHOLESTEROL_HDL_ID:
                            startFragment(CholesterolChartFragment.getInstance(
                                    beginOfTheDay, MonitorMeParam.CHOLESTEROL_HDL_ID.getId(),
                                    R.string.hdl_cholesterol, description,
                                    monitorMeParam.isMedicalKitExist()));
                            break;
                        case RESTING_PULSE_ID:
                            startFragment(PulseChartFragment.getInstance(
                                    beginOfTheDay, description,
                                    monitorMeParam.isMedicalKitExist()));
                            break;
                        case BLOOD_PRESSURE_ID:
                            startFragment(BloodChartFragment.getInstance(
                                    beginOfTheDay, description,
                                    monitorMeParam.isMedicalKitExist()));
                            break;
                        case HEART_STROKE_RISK_ID:
                            startFragment(HeartRiskParametersFragment.getInstance(
                                    beginOfTheDay, monitorMeParam.isMedicalKitExist()));
                            break;
                        case SMOKER_ID:
                            startFragment(HeartRiskParametersFragment.getInstance(
                                    beginOfTheDay, monitorMeParam.isMedicalKitExist()));
                            break;
                        case REFRESHED_ID:
                            startFragment(SleepChartFragment.getInstance(
                                    beginOfTheDay, getString(R.string.sleep_description)));
                            break;
                        case SLEEP_ID:
                            startFragment(SleepChartFragment.getInstance(
                                    beginOfTheDay, description));
                            break;
                        case STRESS_ID:
                            startFragment(StressChartFragment.getInstance(
                                    beginOfTheDay, description));
                            break;
                        case UREA_ID:
                        case CREATININE_ID:
                        case URIC_ACID_ID:
                        case PC_RATIO_ID:
                        case BILIRUBIN_ID:
                        case ALP_ID:
                        case AST_ID:
                        case ALT_ID:
                        case LDH_ID:
                        case GAMMA_GT_ID:
                        case PROTEIN_ID:
                        case ALBUMIN_ID:
                        case CALCIUM_ID:
                        case PHOSPHATE_ID:
                        case VIT_D_ID:
                        case HBA1C_ID:
                        case TSH_ID:
                        case FT4_ID:
                        case T3_ID:
                        case HB_ID:
                        case WCC_ID:
                        case PIT_ID:
                        case MCV_ID:
                        case FOLATE_ID:
                        case FERRITINE_ID:
                        case VIT_B_ID:
                        case IRON_ID:
                        case TIBC_ID:
                            startFragment(OnlyChartFragment.getInstance(paramName,
                                    monitorMeParam.isMedicalKitExist(), description, beginOfTheDay,
                                    monitorMeParam.getId(), false,
                                    TimeInterval.ONE_YEAR.getId()));
                            break;
                        default:
                            break;
                    }
                }
            }
        };
    }

    private static MonitorMeParam getMonitorMeParam(int featureId) {
        MonitorMeParam monitorMeParam = null;

        for (MonitorMeParam param : MonitorMeParam.values()) {
            if (param.getId().equalsIgnoreCase(String.valueOf(featureId))) {
                monitorMeParam = param;
                break;
            }
        }

        return monitorMeParam;
    }

    private long getBeginOfTheDay() {
        final Bundle b = getArguments();
        int dayNumberAgo = 0;

        if (b != null) {
            dayNumberAgo = -b.getInt(EXTRA_DAY_NUMBER_AGO);
        }

        final long beginOfTheDay = com.babylon.utils.DateUtils.getDayStartMills(dayNumberAgo);

        return beginOfTheDay;
    }
}