package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vvydesai on 9/14/2015.
 */
public class HelpResponse extends BaseResponse {

    @SerializedName("data")
    private String data;

    @SerializedName("LMD")
    private String LMD;

    public String getData() {
        return data;
    }

    public String getLMD() {
        return LMD;
    }
}
