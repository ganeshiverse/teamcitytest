package com.babylon.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.model.NutrientByCategory;
import com.babylon.model.NutrientCategory;

import java.util.List;

public class NutrientsCategoriesArrayAdapter extends ArrayAdapter {

    private OnAddNutrientClick onAddNutrientClick;
    private List<NutrientByCategory> categories;

    public NutrientsCategoriesArrayAdapter(Context context, List<NutrientByCategory> categories,
                                           OnAddNutrientClick onAddNutrientClick) {
        super(context, R.layout.list_item_nutrient_category, categories);
        this.categories = categories;
        this.onAddNutrientClick = onAddNutrientClick;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.list_item_nutrient_category, null);
            holder = new ViewHolder();
            holder.categoryTextView = (TextView) convertView.findViewById(R.id.text_view_category);
            holder.kcalTextView = (TextView) convertView.findViewById(R.id.text_view_kcal);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.categoryTextView.setText(categories.get(position).getCategory().getTitle());

        showCalories(position, holder);
        initAddFoodButton(position, convertView);


        return convertView;
    }

    private void initAddFoodButton(final int position, View convertView) {
        final ImageButton addButton = (ImageButton) convertView.findViewById(R.id.btn_add_food);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final NutrientCategory category = categories.get(position).getCategory();
                onAddNutrientClick.onAddNutrientClick(category);
            }
        });
    }

    private void showCalories(int position, ViewHolder holder) {
        final String caloriesStr = String.valueOf(categories.get(position).getCalories());
        final String caloriesFormatted = String.format(getContext().getString(R.string
                .calories_kcal_pattern), caloriesStr);

        holder.kcalTextView.setText(caloriesFormatted);
    }

    public void setCategory(List<NutrientByCategory> categories) {
        this.categories = categories;
    }

    private static class ViewHolder {
        TextView categoryTextView;
        TextView kcalTextView;
    }

    public interface OnAddNutrientClick {
        public void onAddNutrientClick(NutrientCategory category);
    }
}
