package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 14/09/15.
 */

@DatabaseTable(tableName = DBUtils.TABLE_NAME_DEVICE_USERS)
    public class DeviceUsersDbModel extends DBUtils implements Serializable {

    @DatabaseField(id = true, columnName = EC_USER_ID)
    private String ecUserId;

    @DatabaseField(columnName = ADDRESS)
    private String address;

    @DatabaseField(columnName = DOB)
    private String dob;

    @DatabaseField(columnName = DEVICE_ID)
    private String deviceId;

    @DatabaseField(columnName = FORENAME)
    private String forename;

    @DatabaseField(columnName = MOBILE_NO)
    private String mobileNo;

    @DatabaseField(columnName = PRIMARY_IDENTIFIER)
    private String primaryIdentifier;

    @DatabaseField(columnName = GENDER)
    private String gender;

    @DatabaseField(columnName = SITE_ID)
    private String siteID;

    @DatabaseField(columnName = SURNAME)
    private String surname;

    @DatabaseField(columnName = USER_DEVICE_ID)
    private String UserDeviceID;

    @DatabaseField(columnName = LOGIN_NAME)
    private String loginName;

    @DatabaseField(columnName = TOKEN)
    private String token;

    public String getEcUserId() {
        return ecUserId;
    }

    public void setEcUserId(String ecUserId) {
        this.ecUserId = ecUserId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPrimaryIdentifier() {
        return primaryIdentifier;
    }

    public void setPrimaryIdentifier(String primaryIdentifier) {
        this.primaryIdentifier = primaryIdentifier;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSiteID() {
        return siteID;
    }

    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserDeviceID() {
        return UserDeviceID;
    }

    public void setUserDeviceID(String userDeviceID) {
        UserDeviceID = userDeviceID;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
