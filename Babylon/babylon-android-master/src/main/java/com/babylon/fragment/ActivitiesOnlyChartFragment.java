package com.babylon.fragment;

import android.os.Bundle;
import android.view.View;
import com.babylon.R;
import com.babylon.utils.Constants;

public class ActivitiesOnlyChartFragment extends OnlyChartFragment {

    public static ActivitiesOnlyChartFragment getInstance(String titleName,
                                                String description, long beginOfTheDayInMillis,
                                                String featureId, Boolean showDateRadioGroup,
                                                String defaultPeriodId) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TITLE_NAME, titleName);
        bundle.putString(Constants.DESCRIPTION, description);
        bundle.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        bundle.putString(Constants.CHART_FEATURE_ID, featureId);
        if (defaultPeriodId != null) {
            bundle.putString(Constants.DEFAULT_PERIOD_ID, defaultPeriodId);
        }
        if (showDateRadioGroup != null) {
            bundle.putBoolean(Constants.SHOW_DATE_RADIO_GROUP, showDateRadioGroup);
        }

        ActivitiesOnlyChartFragment fragment = new ActivitiesOnlyChartFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRightImageButton(onDataSourceClickListener, R.drawable.ic_menu_sync);
    }

    private final View.OnClickListener onDataSourceClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startFragment(new DataSourcesFragment());
        }
    };
}
