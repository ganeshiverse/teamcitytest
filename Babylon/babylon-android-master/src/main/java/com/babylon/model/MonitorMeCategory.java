package com.babylon.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MonitorMeCategory extends BaseModel {

    @SkipFieldInContentValues
    private static final long serialVersionUID = 4563L;

    private String id;
    private String name;
    private Integer sort;
    private String description;

    private List<Icon> iconsDescription;
    private List<Icon> iconsReport;

    @SkipFieldInContentValues
    @SerializedName("measures")
    private List<Feature> features;

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Icon> getIconsDescription() {
        return iconsDescription;
    }

    public void setIconsDescription(List<Icon> iconsDescription) {
        this.iconsDescription = iconsDescription;
    }

    public List<Icon> getIconsReport() {
        return iconsReport;
    }

    public void setIconsReport(List<Icon> iconsReport) {
        this.iconsReport = iconsReport;
    }

    public static class Feature implements Serializable {

        @SkipFieldInContentValues
        private static final long serialVersionUID = 2459L;

        private String title;
        private String units;
        private String description;
        private int featureId;

        /* dynamic data which added from another request */

        private String viewValue;
        private Double targetValue;
        /** State: 1 - correct, 2 - warning 3 - bad */
        private int state;

        public String getViewValue() {
            return viewValue;
        }

        public void setViewValue(String viewValue) {
            this.viewValue = viewValue;
        }

        public Double getTargetValue() {
            return targetValue;
        }

        public void setTargetValue(Double targetValue) {
            this.targetValue = targetValue;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUnits() {
            return units;
        }

        public void setUnits(String units) {
            this.units = units;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getFeatureId() {
            return featureId;
        }

        public void setFeatureId(int featureId) {
            this.featureId = featureId;
        }
    }
}


