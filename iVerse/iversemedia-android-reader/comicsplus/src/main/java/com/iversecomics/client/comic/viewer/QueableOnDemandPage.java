package com.iversecomics.client.comic.viewer;

import android.graphics.Bitmap;

import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class QueableOnDemandPage implements AsyncQueueableObject {

    private WebBitmapLoader mLoader;
    private URI mPageUri;
    private int mPageIndex;
    private Bitmap mBitmap;
    private boolean mWasGarbageCollected = false;
    private boolean mWasOutOfMemory = false;

    private static final Logger LOG = LoggerFactory.getLogger(QueableOnDemandPage.class);

    public QueableOnDemandPage(WebBitmapLoader loader, int pageIndex, URI pageUri) {
        this.mLoader = loader;
        this.mPageIndex = pageIndex;
        this.mPageUri = pageUri;
    }

    @Override
    public void performOperation() {

        try {
            mBitmap = mLoader.loadBitmap(mPageUri);
        } catch (OutOfMemoryError e) {
            LOG.error("out of memory loading bitmap in QueableOnDemandPage %s", e.getLocalizedMessage());
            mWasOutOfMemory = true;
        } catch (IOException e) {
            LOG.error("error loading bitmap in QueableOnDemandPage %s", e.getLocalizedMessage());
        }
    }

    @Override
    public void handleOperationResult() {

        if (mWasOutOfMemory || mWasGarbageCollected)
            return;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public URI getPageUri() {
        return mPageUri;
    }

    public int getPageIndex() {
        return mPageIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof QueableOnDemandPage) {
            QueableOnDemandPage qodp = (QueableOnDemandPage) obj;
            return mPageUri != null &&
                    qodp.getPageUri() != null &&
                    mPageUri.equals(qodp.getPageUri());
        } else {
            return false;
        }
    }
}
