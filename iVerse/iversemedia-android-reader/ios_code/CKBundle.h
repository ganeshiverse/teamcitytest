//
//  CKBundle.h
//  Comics
//
//  Created by Derek Stutsman on 7/8/14.
//  Copyright (c) 2014 iVerse Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@import Foundation;
@import UIKit;

typedef NS_ENUM(NSInteger, CKBundlePresentation)
{
    CKBundlePresentationVertical,
    CKBundlePresentationHorizontal
};

typedef NS_ENUM(NSInteger, CKBundlePagination)
{
    CKBundlePaginationStandard,
    CKBundlePaginationManga
};

typedef NS_ENUM(NSInteger, CKBundleTransition)
{
    CKBundleTransitionStandard,
    CKBundleTransitionFade
};

typedef NS_ENUM(NSInteger, CKBundleType)
{
    CKBundleTypeInstalled,
    CKBundleTypeRemote,
    CKBundleTypePreview
};

typedef void (^CKBundleImageResult)(UIImage *image);

@class CKStoreProduct, CKBundlePageInfo;

@interface CKBundle : NSObject
{
}

@property (strong, nonatomic) NSMutableArray *pages;
@property (strong, nonatomic) NSMutableArray *writers;
@property (strong, nonatomic) NSMutableArray *artists;
@property (strong, nonatomic) NSMutableArray *categories;
@property (strong, nonatomic) NSString *generatedDate;
@property (strong, nonatomic) NSString *generatedBy;
@property (strong, nonatomic) NSString *productID;
@property (strong, nonatomic) NSString *synopsis;
@property (strong, nonatomic) NSString *issueNumber;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *productType;
@property (strong, nonatomic) NSString *parentProductID;
@property (strong, nonatomic) NSString *productDescription;
@property (strong, nonatomic) NSString *publisherID;
@property (strong, nonatomic) NSString *publisherName;
@property (strong, nonatomic) NSString *videoURL;
@property (strong, nonatomic) NSString *publisherDate;
@property (strong, nonatomic) NSString *copyright;
@property (strong, nonatomic) NSError *error;
@property (assign, nonatomic) CKBundlePresentation presentation;
@property (assign, nonatomic) CKBundlePagination pagination;
@property (assign, nonatomic) CKBundleTransition transition;
@property (assign, nonatomic) CKBundleType type;

- (instancetype) initWithFileAtPath:(NSString*)path;                  //Used for installed products
- (instancetype) initWithProductPreview:(CKStoreProduct*)aProduct;    //Used for previews
- (instancetype) initWithDictionary:(NSDictionary*)dictionary;        //Used for on-demand

- (BOOL)checksumIsValid;                                    // For verifying the download worked
- (void)signBundle;                                         // To bind this to the user
- (BOOL)isAuthentic;                                        // To check that it hasn't been tampered with

- (UIImage*)coverImage;
- (UIImage*)bundleImageForPage:(CKBundlePageInfo*)pageInfo size:(CGFloat)size;
- (void)fetchBundleImageForPage:(CKBundlePageInfo*)pageInfo size:(CGFloat)size response:(CKBundleImageResult)response;

@end