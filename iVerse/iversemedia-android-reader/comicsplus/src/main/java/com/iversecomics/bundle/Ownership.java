package com.iversecomics.bundle;

public class Ownership {
    private String emailAddress;
    private String password;
    private String uniqueId;
    private boolean dirty = false;
    private int userPoints;

    public boolean hasCredentials() {
        return !(emailAddress.equals("") || password.equals(""));
    }

    public void clearCredentials() {
        emailAddress = "";
        password = "";
        dirty = true;
    }

    public void clearDirty() {
        this.dirty = false;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        dirty = true;
    }

    public void setPassword(final String password) {
        this.password = password;
        dirty = true;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
        dirty = true;
    }

    public int getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(int userPoints) {
        this.userPoints = userPoints;
    }
}
