package com.babylon.database.schema.check;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;

import static com.babylon.database.check.CheckTables.BODY_PART;

public interface BodyPartSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("check_entity").appendPath(BODY_PART.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        NAME("name", DBType.TEXT),
        GENDER("gender", DBType.TEXT),
        PARENT_ID("parentId", DBType.TEXT),
        SORT("sort", DBType.TEXT),
        USE_PARENT_SYMPTOMS("useParentSymptoms", DBType.TEXT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public interface Query {

        String[] PROJECTION = {
                BODY_PART + "." + Columns._ID,
                BODY_PART + "." + Columns.ID,
                BODY_PART + "." + Columns.SYNC_WITH_SERVER,
                BODY_PART + "." + Columns.NAME,
                BODY_PART + "." + Columns.GENDER,
                BODY_PART + "." + Columns.PARENT_ID,
                BODY_PART + "." + Columns.SORT,
                BODY_PART + "." + Columns.USE_PARENT_SYMPTOMS
        };

        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int NAME = 3;
        int GENDER = 4;
        int PARENT_ID = 5;
        int SORT = 6;
        int USE_PARENT_SYMPTOMS = 7;
    }
}