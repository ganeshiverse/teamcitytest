package com.iversecomics.client.comic;

import android.graphics.Bitmap;

public interface IComicSourceListener {

    public void onComicSourceUpdated(IComicSourceAdapter adapter);

    public void onBitmapLoaded(int index, Bitmap bitmap);
}
