package com.iversecomics.client.bitmap;

import android.graphics.Bitmap;

import java.io.IOException;
import java.net.URI;

public interface IBitmapLoader {
    public String[] getUriSchemes();

    public Bitmap loadBitmap(URI bitmapUri) throws IOException;
}
