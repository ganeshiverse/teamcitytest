package com.iversecomics.client.util;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

public class PatternFileFilter implements FileFilter {
    private Pattern filenamePattern;

    public PatternFileFilter(Pattern filenamePat) {
        this.filenamePattern = filenamePat;
    }

    @Override
    public boolean accept(File path) {
        if (!path.isFile()) {
            return false;
        }
        return filenamePattern.matcher(path.getName()).matches();
    }
}