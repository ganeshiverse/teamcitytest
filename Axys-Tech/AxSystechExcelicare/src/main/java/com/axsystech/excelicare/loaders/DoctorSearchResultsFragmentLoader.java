package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by sbommineni on 11/14/2015.
 */
public class DoctorSearchResultsFragmentLoader extends DataAsyncTaskLibLoader<SearchDoctorsResponse> {

    private String token;
    private String searchCriteria;
    private String pageNumber;
    private String pageSize;

    public DoctorSearchResultsFragmentLoader(final Context context, String token, String searchCriteria, String pageNumber, String pageSize) {
        super(context);
        this.token = token;
        this.searchCriteria = searchCriteria;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    @Override
    protected SearchDoctorsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getDoctorResultSearch(token, searchCriteria, pageNumber, pageSize);
    }
}
