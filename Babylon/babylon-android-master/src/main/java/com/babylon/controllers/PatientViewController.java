package com.babylon.controllers;

import android.text.InputFilter;
import android.widget.EditText;

import com.babylon.model.Patient;
import com.babylon.utils.DecimalDigitsInputFilter;

public class PatientViewController {
    private static final int DECIMAL_DIGIT_NUMBER = 2;
    private static final int WEIGHT_DIGIT_NUMBER = 3;
    private static final int HEIGHT_DIGIT_NUMBER = 3;

    private static final float MIN_WEIGHT = 1;
    public static final float MAX_WEIGHT = 729;
    private static final float MAX_HEIGHT = 285f;

    private final Patient patient;

    public PatientViewController (final Patient patient) {
        this.patient = patient;
    }

    public void initWeightEditText(final EditText weightEditText) {
        final float weight = patient.getWeight();
        final int weightRound = getRoundValue(weight);

        setValue(weightRound, weightEditText);

        weightEditText.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(WEIGHT_DIGIT_NUMBER,
                DECIMAL_DIGIT_NUMBER, MIN_WEIGHT, MAX_WEIGHT)});
    }

    public void initHeightEditText(final EditText heightEditText) {
        final float height = patient.getHeight();
        final int heightRound = getRoundValue(height);

        setValue(heightRound, heightEditText);

        heightEditText.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(HEIGHT_DIGIT_NUMBER,
                DECIMAL_DIGIT_NUMBER, 0, MAX_HEIGHT)});
    }

    private void setValue(int value,  EditText heightEditText) {
        if(value > 0) {
            heightEditText.setText(String.valueOf(value));
        }
    }
    private int getRoundValue(float value) {
        int result = Math.round(value);
        return result;
    }
}
