package com.babylon.controllers;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;

import com.babylon.Keys;
import com.babylon.activity.QuestionActivity;
import com.babylon.database.Table;
import com.babylon.database.schema.MessageSchema;
import com.babylon.utils.DateUtils;

import java.lang.ref.WeakReference;
import java.util.Date;

public class AskController implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final int DAY_FOR_SAVE_UNREAD_COMMENTS = -1;
    protected final static int LOADER_ID_PENDING_MESSAGE = 1;

    protected WeakReference<FragmentActivity> activity;

    public AskController(FragmentActivity activity) {
        this.activity = new WeakReference<>(activity);
    }

    public void start() {
        start(null);
    }

    public void start(Bundle extras) {
        String questionId = null;
        if (extras != null) {
            questionId = extras.getString(Keys.QUESTION_ID);
        }
        if (TextUtils.isEmpty(questionId)) {
            activity.get().getSupportLoaderManager().restartLoader(LOADER_ID_PENDING_MESSAGE, null, this);
        } else {
            startQuestionActivity(questionId);
        }
    }

    private void startQuestionActivity(String questionId) {
        startQuestionActivity(questionId, true);
    }

    private void startQuestionActivity(String questionId, boolean isActivityClearTask) {
        Intent intent = new Intent(activity.get(), QuestionActivity.class);
        intent.putExtra(Keys.QUESTION_ID, questionId);
        if (isActivityClearTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        activity.get().startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        long newCommentsMills = DateUtils.getDayStartMills(new Date(), DAY_FOR_SAVE_UNREAD_COMMENTS);
        long newCommentSeconds = DateUtils.millsToSeconds(newCommentsMills);
        final String projection[] = new String[]{
                Table.MESSAGE.getName() + "." + MessageSchema.Columns.ID.getName(),
        };
        final String selectionArgs[] = new String[]{
                String.valueOf(newCommentSeconds)
        };
        return new CursorLoader(activity.get(), MessageSchema.PENDING_QUESTIONS_URI,
                projection, MessageSchema.Query.MESSAGES_WITHOUT_COMMENT_OR_UNREAD_COMMENT,
                selectionArgs, MessageSchema.SORT_TIME_DESC);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        String questionId = null;
        if (cursor.moveToFirst()) {
            final int questionIdIndex = cursor.getColumnIndex(MessageSchema.Columns.ID.getName());
            questionId = cursor.getString(questionIdIndex);
        }

        boolean isQuestionExist = !TextUtils.isEmpty(questionId);

        if (isQuestionExist) {
            startQuestionActivity(questionId, false);
        } else {
            startAskActivity();
        }

        activity.get().getSupportLoaderManager().destroyLoader(LOADER_ID_PENDING_MESSAGE);
    }

    private void startAskActivity() {
        startAskActivity(null);
    }

    public void startAskActivity(Integer action) {
        Intent intent = new Intent(activity.get(), com.babylon.activity.AskQuestionActivity.class);
        if (action != null) {
            intent.putExtra(Keys.PHOTO_ACTION, action);
        }
        activity.get().startActivity(intent);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
}

