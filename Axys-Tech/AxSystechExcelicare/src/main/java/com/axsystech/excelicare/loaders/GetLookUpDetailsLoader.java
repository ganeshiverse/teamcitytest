package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetLookUpDetailsLoader extends DataAsyncTaskLibLoader<LookUpDetailsResponse> {

    private String token;
    private String dictionartID;
    private String searchString;

    public GetLookUpDetailsLoader(final Context context, String token, String dictionartID, String searchString) {
        super(context);
        this.token = token;
        this.dictionartID = dictionartID;
        this.searchString = searchString;
    }

    @Override
    protected LookUpDetailsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getLookupDetails(token, dictionartID, searchString);
    }
}