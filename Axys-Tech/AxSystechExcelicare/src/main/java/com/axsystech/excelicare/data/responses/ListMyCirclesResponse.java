package com.axsystech.excelicare.data.responses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class ListMyCirclesResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private CirclesDataObject circlesDataObject;

    public CirclesDataObject getCirclesDataObject() {
        return circlesDataObject;
    }

    public class CirclesDataObject implements Serializable {

        @SerializedName("Details")
        private ArrayList<CircleDetails> circleDetailsArrayList;

        @SerializedName("LMD")
        private String LMD;

        public ArrayList<CircleDetails> getCircleDetailsArrayList() {
            return circleDetailsArrayList;
        }

        public String getLMD() {
            return LMD;
        }
    }

    public class CircleDetails implements Serializable {

        @SerializedName("CircleMemberCount")
        private int circleMemberCount;

        @SerializedName("CircleName")
        private String circleName;

        @SerializedName("IsProtected")
        private boolean isProtected;

        @SerializedName("StartDate")
        private String startDate;

        @SerializedName("StopDate")
        private String stopDate;

        @SerializedName("StopReasonDesc")
        private String stopReasonDesc;

        @SerializedName("StopReason_SLU")
        private String stopReason_SLU;

        @SerializedName("SystemCircle_Id")
        private String systemCircle_Id;

        @SerializedName("UserCircle_ID")
        private String userCircle_ID;

        @SerializedName("UserID")
        private long userID;

        @SerializedName("UserType")
        private String userType;

        public boolean isSystemCircle() {
            if (!TextUtils.isEmpty(systemCircle_Id)) {
                return true;
            }

            return false;
        }

        public int getCircleMemberCount() {
            return circleMemberCount;
        }

        public String getCircleName() {
            return circleName;
        }

        public boolean isProtected() {
            return isProtected;
        }

        public String getStartDate() {
            return startDate;
        }

        public String getStopDate() {
            return stopDate;
        }

        public String getStopReasonDesc() {
            return stopReasonDesc;
        }

        public String getStopReason_SLU() {
            return stopReason_SLU;
        }

        public String getSystemCircle_Id() {
            return systemCircle_Id;
        }

        public String getUserCircle_ID() {
            return userCircle_ID;
        }

        public void setSystemCircle_Id(String systemCircle_Id) {
            this.systemCircle_Id = systemCircle_Id;
        }

        public long getUserID() {
            return userID;
        }

        public String getUserType() {
            return userType;
        }
    }
}
