package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/1/2015.
 */
public class InvitationAcceptLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String inviteReceivedId;

    public InvitationAcceptLoader(Context context, String token, String inviteReceivedId) {
        super(context);
        this.token = token;
        this.inviteReceivedId = inviteReceivedId;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getInviteAccept(token, inviteReceivedId);
    }
}
