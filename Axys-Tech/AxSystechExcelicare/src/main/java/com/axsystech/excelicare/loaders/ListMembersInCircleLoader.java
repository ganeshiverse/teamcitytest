package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class ListMembersInCircleLoader extends DataAsyncTaskLibLoader<ListMembersInCircleResponse> {

    private String token;
    private String userCircleId;
    private String criteria;
    int pageIndex;
    int pageSize;
    String orderBy;

    public ListMembersInCircleLoader(Context context, String token, String userCircleId, String criteria, int pageIndex, int pageSize, String orderBy) {
        super(context);
        this.token = token;
        this.userCircleId = userCircleId;
        this.criteria = criteria;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.orderBy = orderBy;
    }

    @Override
    protected ListMembersInCircleResponse performLoad() throws Exception {
        return ServerMethods.getInstance().listMembersInCircle(token, userCircleId, criteria,pageIndex,pageSize,orderBy);
    }
}
