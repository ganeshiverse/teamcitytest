package com.babylon.api.request.family;

import com.babylon.api.request.RubyResponseProcessor;
import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Account;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class AccountAuthenticatePost extends AbsRequestPost<Account, Account> {

    private String entryId;

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    public AbsRequestPost<Account, Account> withEntry(Account entry) {
        entryId = entry.getId();
        return this;
    }

    @Override
    protected String getUrl() {
        return String.format(Urls.getRuby(Urls.ACCOUNTS_AUTHENTICATE), entryId);
    }

    @Override
    protected AbsResponseProcessor<Account> getResponseProcessor() {
        return new RubyResponseProcessor<Account>(getOutputType());
    }
}
