package com.babylon.sync;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.babylon.broadcast.SignOutBroadcastReceiver;
import com.babylon.broadcast.SyncFinishedBroadcast;
import com.babylon.sync.monitor.FoodSync;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.sync.monitor.MonitorReportSync;
import com.babylon.sync.monitor.PhysicalActivitySync;
import com.babylon.sync.monitor.StressQuizSync;
import com.babylon.utils.Constants;
import com.babylon.utils.RegionSettings;

import java.util.HashMap;
import java.util.Map;

/**
 * Define a sync adapter for the app.
 * <p/>
 * <p>This class is instantiated in {@link com.babylon.sync.service.SyncService}, which also binds SyncAdapter to the
 * system. SyncAdapter should only be initialized in SyncService, never anywhere else.
 * <p/>
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by SyncService.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String TAG = SyncAdapter.class.getSimpleName();
    public static final String ACTION_NETWORK_ERROR = "com.babylon.ACTION_NETWORK_ERROR";
    public static final String KEY_RESPONSE_ENTITY = "key_response_entity";
    /**
     * Content resolver, for performing database operations.
     */
    private final ContentResolver mContentResolver;

    public static final String KEY_SYNC_TYPE = "sync_operation";
    public static final String KEY_SYNC_ACTION = "sync_action";
    public static final String KEY_IS_SYNCED_MONITOR_ME_REPORT = "sync_should_sync_monitor_me_report";

    public static final int SYNC_PUSH = 201;
    public static final int SYNC_PULL = 202;
    public static final int SYNC_BOTH = 203;


    public static void performSync(String syncType, int syncAction) {
        performSync(syncType, new Bundle(), syncAction);
    }

    public static void performSync(String syncType, Bundle bundle, int syncAction) {
        bundle.putString(KEY_SYNC_TYPE, syncType);
        bundle.putInt(KEY_SYNC_ACTION, syncAction);

        SyncUtils.triggerRefresh(bundle);
    }

    public static void performSyncAndPullReport(String syncType, int syncAction) {
        performSyncAndPullReport(syncType, new Bundle(), syncAction);
    }

    public static void performSyncAndPullReport(String syncType, Bundle bundle, int syncAction) {
        bundle.putString(KEY_SYNC_TYPE, syncType);
        bundle.putInt(KEY_SYNC_ACTION, syncAction);
        bundle.putBoolean(KEY_IS_SYNCED_MONITOR_ME_REPORT, true);

        SyncUtils.triggerRefresh(bundle);
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    final static Map<String, AbsSyncStrategy> syncStrategyMap = new HashMap<String, AbsSyncStrategy>();

    static {
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_PATIENT, new PatientSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_MESSAGES, new MessagesSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_SINGLE_MESSAGE, new SingleMessageSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_REGION, new RegionSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_TRIAGE, new TriageSync());

        final boolean isMonitorMeEnabled = new RegionSettings().isMonitorMeEnabled();
        if (isMonitorMeEnabled) {
            addMonitorMeSync();
        }

    }

    private static void addMonitorMeSync() {
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, new MonitorMeParamSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_MONITOR_ME_CATEGORIES,
                new MonitorCategoriesSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_PHYSICAL_ACTIVITY, new PhysicalActivitySync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_FOOD, new FoodSync());
        syncStrategyMap.put(Constants.Sync_Keys.SYNC_STRESS_QUIZ, new StressQuizSync());
    }

    /**
     * Called by the Android system in response to a request to run the sync adapter. The work required to read data
     * from the network, parse it, and store it in the content provider is done here. Extending
     * AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter run on a background thread. For this
     * reason, blocking I/O and other long-running tasks can be run <em>in situ</em>, and you don't have to set up a
     * separate thread for them. .
     * <p/>
     * <p>This is where we actually perform any work required to perform a sync. {@link
     * android.content.AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread, so it is
     * safe to peform blocking I/O here.
     * <p/>
     * <p>The syncResult argument allows you to pass information back to the method that triggered the sync.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        if (SyncUtils.isLoggedIn()) {
            if (extras != null) {
                final String syncType = extras.getString(KEY_SYNC_TYPE);
                final int syncAction = extras.getInt(KEY_SYNC_ACTION, SYNC_BOTH);

                if (TextUtils.isEmpty(syncType)) {
                    autoSync(new Bundle());
                } else {
                    AbsSyncStrategy syncStrategy;

                    if (syncType.equals(Constants.Sync_Keys.SYNC_MONITOR_ME_REPORT)) {
                        syncStrategy = new MonitorReportSync();
                    } else {
                        syncStrategy = syncStrategyMap.get(syncType);
                    }

                    if (syncStrategy != null) {
                        switch (syncAction) {
                            case SYNC_PUSH:
                                syncStrategy.push(mContentResolver, extras);
                                break;
                            case SYNC_PULL:
                                syncStrategy.pull(mContentResolver, extras);
                                break;
                            case SYNC_BOTH:
                                syncStrategy.push(mContentResolver, extras);
                                syncStrategy.pull(mContentResolver, extras);
                                break;
                        }
                    }

                    boolean shouldMonitorMeParamsBeSynced = extras.getBoolean(KEY_IS_SYNCED_MONITOR_ME_REPORT, false);

                    if (shouldMonitorMeParamsBeSynced) {
                        new MonitorReportSync().pull(getContext().getContentResolver(), extras);
                    }

                    SyncFinishedBroadcast.sendBroadcast(syncType, syncAction);
                }
            } else {
                autoSync(new Bundle());
            }
        }
    }

    private void autoSync(Bundle bundle) {
        new PatientSync().push(getContext().getContentResolver(), bundle);
        new PatientSync().pull(getContext().getContentResolver(), bundle);

        new RegionSync().pull(getContext().getContentResolver(), bundle);
        new StressQuizSync().pull(getContext().getContentResolver(), bundle);
        new MonitorCategoriesSync().pull(getContext().getContentResolver(), bundle);

        new MessagesSync().pull(getContext().getContentResolver(), bundle);
        new MessagesSync().push(getContext().getContentResolver(), bundle);

        //support offline mode
        new MonitorMeParamSync().push(getContext().getContentResolver(), bundle);
        new PhysicalActivitySync().push(getContext().getContentResolver(), bundle);
        new FoodSync().push(getContext().getContentResolver(), bundle);

        new MonitorReportSync().pull(getContext().getContentResolver(), bundle);

        SignOutBroadcastReceiver.sendBroadcast();
    }

}