package com.iversecomics.client.store.tasks;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchGenresTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(FetchGenresTask.class);
    private final String        freshnessKey;

    public FetchGenresTask(ComicStore comicStore) {
        super(comicStore);
        this.freshnessKey = "genres";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchGenresTask other = (FetchGenresTask) obj;
        if (freshnessKey == null) {
            if (other.freshnessKey != null) {
                return false;
            }
        } else if (!freshnessKey.equals(other.freshnessKey)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getGenresList();
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            ResponseParser parser = new ResponseParser();
            parser.parseGenres(json, updater);
            server.updateFreshness(freshnessKey, Time.HOUR * 3);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update genres list");
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((freshnessKey == null) ? 0 : freshnessKey.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(freshnessKey);
    }
}
