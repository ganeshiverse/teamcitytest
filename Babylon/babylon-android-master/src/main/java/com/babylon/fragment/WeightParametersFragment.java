package com.babylon.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.MonitorMeParamsLoaderCallback;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.BaseModel;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.Patient;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.UIUtils;
import com.babylon.validator.ValidationController;
import com.babylon.validator.ValueBetweenValidator;
import com.babylon.view.CustomEditText;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WeightParametersFragment extends BaseMonitorMeParamsLoaderFragment implements View.OnClickListener {

    public static final float MIN_WEIGHT = 1f;
    public static final float MAX_WEIGHT = 999f;
    public static final float MIN_HEIGHT = 55f;
    public static final float MAX_HEIGHT = 285f;

    private CustomEditText weightEditText;
    private CustomEditText goalWeightEditText;
    private DailyFeatureMap dailyFeatureMap;

    public static WeightParametersFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        WeightParametersFragment fragment = new WeightParametersFragment();
        fragment.setArguments(b);
        return fragment;
    }

    private ValidationController validationController = new ValidationController();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_weight_parameters, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String dateString = new MonitorMeDateViewController().getFormattedDay(dateInMillis);

        ((TextView) getView().findViewById(R.id.dateTextView)).setText(dateString);

        weightEditText = (CustomEditText) getView().findViewById(R.id.fragment_weight_edit_text);
        goalWeightEditText = (CustomEditText) getView().findViewById(R.id.fragment_weight_goal_edit_text);

        getView().findViewById(R.id.acceptButton).setOnClickListener(this);

        validationController = new ValidationController();

        ValueBetweenValidator weightValueValidator = new ValueBetweenValidator(null,
                getActivity().getString(R.string.validator_field_value_between, MIN_WEIGHT, MAX_WEIGHT),
                MIN_WEIGHT, MAX_WEIGHT);

        weightEditText.validationAfterTextChanged(validationController);
        weightEditText.getEditText().setKeyListener(DigitsKeyListener.getInstance(false, true));
        validationController.addValidation(weightEditText, weightValueValidator);

        goalWeightEditText.validationAfterTextChanged(validationController);
        goalWeightEditText.getEditText().setKeyListener(DigitsKeyListener.getInstance(false, true));
        validationController.addValidation(goalWeightEditText, weightValueValidator);

        setTitle(getString(R.string.weight_parameters_input_data));

        setPatientData();
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(final long dateFrom) {
        List<MonitorMeParam> featureList = new LinkedList<MonitorMeParam>();
        featureList.add(MonitorMeParam.WEIGHT_ID);
        featureList.add(MonitorMeParam.GOAL_WEIGHT_ID);

        final long dateTo = DateUtils.getDayStartMills(dateInMillis, 1);

        MonitorMeParamsGet.newMonitorMeParamsGet(dateInMillis, dateTo,
                MonitorMeParam.getParameterList(featureList)).execute(getActivity(),
                new RequestListener<ResponseList<MonitorMeFeatures>>() {
                    @Override
                    public void onRequestComplete(Response<ResponseList<MonitorMeFeatures>> response) {
                        if (getActivity() != null) {
                            MonitorMeParamSync.saveMonitorMeParams(getActivity().getContentResolver(), response,
                                    dateInMillis, dateTo);
                            getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_DAY_PARAMS,
                                    MonitorMeParamsLoaderCallback.createBundle(dateInMillis),
                                    new ParamsLoader());
                            onLoadedMonitorMeParamsFromServer();
                            setProgressBarVisible(false);
                        }
                    }
                }
        );
    }

    @Override
    protected void startLoaderManager() {
        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_DAY_PARAMS,
                MonitorMeParamsLoaderCallback.createBundle(dateInMillis), new ParamsLoader());
    }

    /* Android listeners */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.acceptButton:
                saveNewData();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isBackAllowed() {
        UIUtils.hideSoftKeyboard(getActivity(), weightEditText.getEditText());
        return true;
    }

    private void saveNewData() {
        if (validationController.performValidation()) {
            final float weight = BaseModel.getFloat(weightEditText.getText());
            final float goalWeight = BaseModel.getFloat(goalWeightEditText.getText());

            final boolean isMonitorMeParamTheSame = isMonitorMeParamTheSame(goalWeight, weight);

            if (!isMonitorMeParamTheSame) {
                saveMonitorMeParams(weight, goalWeight);
                SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);
            }

            super.onTopBarButtonPressed();
        }
    }

    /* Listeners */
    @Override
    protected void onTopBarButtonPressed() {
        if (isBackAllowed()) {
            UIUtils.hideSoftKeyboard(getActivity(), weightEditText.getEditText());
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    /* Methods */

    private void setPatientData() {
        Patient patient = App.getInstance().getPatient();

        if (patient != null) {
            if (patient.getBirthday() == null) {
                patient.setBirthday(new Date());
            }
        }
    }

    private void saveMonitorMeParams(float weight, float goalWeight) {
        if (goalWeight != BaseModel.getFloat(dailyFeatureMap.getFeature(MonitorMeParam.GOAL_WEIGHT_ID).getValue())) {
            dailyFeatureMap.setValue(MonitorMeParam.GOAL_WEIGHT_ID, goalWeight);
        }

        if (weight != BaseModel.getFloat(dailyFeatureMap.getFeatureValue(MonitorMeParam.WEIGHT_ID))) {
            dailyFeatureMap.setValue(MonitorMeParam.WEIGHT_ID, weight);
        }

        DatabaseOperationUtils.getInstance().addMonitorMeFeatures(dailyFeatureMap.getFeatureMap(),
                getActivity().getContentResolver());
    }

    private boolean isMonitorMeParamTheSame(float goalWeight, float currentWeight) {
        boolean isMonitorMeParamTheSame = true;
        if (dailyFeatureMap != null) {
            final float prevGoalWeight = BaseModel.getFloat(dailyFeatureMap.getFeatureValue(MonitorMeParam
                    .GOAL_WEIGHT_ID));
            final float prevCurrentWeight = BaseModel.getFloat(dailyFeatureMap.getFeatureValue(MonitorMeParam
                    .WEIGHT_ID));
            isMonitorMeParamTheSame = goalWeight == prevGoalWeight
                    && currentWeight == prevCurrentWeight;
        }
        return isMonitorMeParamTheSame;
    }

    private boolean isPatientTheSame(float weight) {
        final Patient originalPatient = App.getInstance().getPatient();
        return weight == originalPatient.getWeight();
    }

    private class ParamsLoader extends MonitorMeParamsLoaderCallback {

        @Override
        public Context getContext() {
            return getActivity();
        }

        @Override
        public List<MonitorMeParam> getMonitorMeParamList() {
            List<MonitorMeParam> featureList =
                    new LinkedList<MonitorMeParam>();
            featureList.add(MonitorMeParam.GOAL_WEIGHT_ID);
            featureList.add(MonitorMeParam.WEIGHT_ID);
            featureList.add(MonitorMeParam.CURRENT_HEIGHT_ID);
            return featureList;
        }

        @Override
        public void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> featureMap) {
            dailyFeatureMap = new DailyFeatureMap(featureMap);

            if (!featureMap.isEmpty()) {
                MonitorMeFeatures.Feature goalWeightFeature = dailyFeatureMap.getFeature(
                        MonitorMeParam.GOAL_WEIGHT_ID);
                if (BaseModel.getFloat(goalWeightFeature.getValue()) != 0) {
                    goalWeightEditText.setText(goalWeightFeature.getValue());
                }

                MonitorMeFeatures.Feature weightFeature = dailyFeatureMap.getFeature(
                        MonitorMeParam.WEIGHT_ID);
                if (BaseModel.getFloat(weightFeature.getValue()) != 0) {
                    weightEditText.setText(weightFeature.getValue());
                }
            }
        }
    }
}
