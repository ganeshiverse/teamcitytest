package com.iversecomics.client.bitmap;

import android.graphics.Bitmap;

public interface ILateBitmapListener {
    public void setLateBitmap(Bitmap bitmap, String tag);
}
