package com.babylon.api.request.payments;

import com.babylon.api.request.RubyResponseProcessor;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.SubscriptionCancelText;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import org.apache.http.message.BasicHeader;

public class SubscriptionGetCancelText extends AbsRequestGet<SubscriptionCancelText> {

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.SUBSCRIPTION_GET_CANCEL_TEXT);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<SubscriptionCancelText> getResponseProcessor() {
        return new RubyResponseProcessor<SubscriptionCancelText>(getOutputType());
    }
}
