package com.axsystech.excelicare.framework.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.mig35.injectorlib.utils.inject.InjectSavedState;

import java.util.Calendar;

/**
 * Created by SomeswarReddy on 7/13/2015.
 */
public class DatePickerFragment extends DialogFragment {

    DatePickerDialog.OnDateSetListener ondateSet;

    @InjectSavedState
    private DatePickerDialog datePickerDialog;

    public DatePickerFragment() {
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    private int year, month, day;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        datePickerDialog = new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        // datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTime().getTime());

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            try {
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return datePickerDialog;
    }

}
