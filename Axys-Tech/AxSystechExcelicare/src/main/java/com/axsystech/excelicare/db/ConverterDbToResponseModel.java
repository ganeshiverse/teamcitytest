package com.axsystech.excelicare.db;

import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.data.responses.LoginUserResponse;
import com.axsystech.excelicare.data.responses.SiteDetailsModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.db.models.User;

import java.util.ArrayList;

/**
 * This class holds the methods to convert Database model objects to Response model objects.
 * Date: 16.05.14
 * Time: 16:37
 *
 * @author SomeswarReddy
 */
public final class ConverterDbToResponseModel {

    private ConverterDbToResponseModel() {
    }

    public static ArrayList<SiteDetailsModel> getSiteDetailsResponseModel(ArrayList<SiteDetailsDBModel> siteDetailsDbModels) {
        ArrayList<SiteDetailsModel> siteDetailsResModelAL = null;

        if (siteDetailsDbModels != null && siteDetailsDbModels.size() > 0) {
            siteDetailsResModelAL = new ArrayList<SiteDetailsModel>();

            for (SiteDetailsDBModel dbModel : siteDetailsDbModels) {
                SiteDetailsModel resModel = new SiteDetailsModel();

                resModel.setSiteId(dbModel.getSiteId());
                resModel.setSiteName(dbModel.getSiteName());

                siteDetailsResModelAL.add(resModel);
            }
        }

        return siteDetailsResModelAL;
    }

}