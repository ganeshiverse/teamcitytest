package com.iversecomics.client.store.json;

import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;

public interface IComicStoreParsingEvent {
    void onParsedComic(Comic comic);

    void onParsedGenre(Genre genre);

    void onParsedGroup(Group group);

    void onParsedPublisher(Publisher publisher);

    void onParsedFeaturedSlot(FeaturedSlot slot);

    void onParsedPromotion(Promotion promotion);

    void onParseEnd();

    void onParseStart();
}
