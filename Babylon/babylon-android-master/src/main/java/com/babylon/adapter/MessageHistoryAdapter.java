package com.babylon.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.database.schema.MessageSchema;
import com.babylon.utils.DateUtils;

import java.util.Date;

public class MessageHistoryAdapter extends CursorAdapter {

    public MessageHistoryAdapter(Activity activity, Cursor cursor) {
        super(activity, cursor, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder viewHolder = (ViewHolder) view.getTag();

        String title = cursor.getString(MessageSchema.Query.TEXT);
        long dateMills = cursor.getLong(MessageSchema.Query.DATE);

        if (dateMills != 0) {
            Date date = DateUtils.getDateFromTimestamp(dateMills);
            if (date != null) {
                String dateString = DateUtils.dateToString(date,
                        DateUtils.getInstance().getDateFormat(DateUtils.FULL_SIMPLE_DATE_FORMAT));
                viewHolder.dateTextView.setText(dateString);
            }
        }

        boolean isTextEmpty = TextUtils.isEmpty(title);
        viewHolder.titleTextView.setText(isTextEmpty ?
                context.getString(R.string.ask_history_audio_question) : title);
        viewHolder.titleTextView.setTextColor(context.getResources().getColor(isTextEmpty ?
                R.color.white_ask : R.color.white));
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = View.inflate(context, R.layout.list_item_message_history, null);
        final ViewHolder holder = new ViewHolder();
        holder.titleTextView = (TextView) view.findViewById(R.id.list_item_message_title_text_view);
        holder.dateTextView = (TextView) view.findViewById(R.id.list_item_message_date_text_view);
        view.setTag(holder);
        return view;
    }

    private static class ViewHolder {
        TextView titleTextView;
        TextView dateTextView;
    }
}