package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CardServiceDataResponse;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by hkyerra on 7/13/2015.
 */
public class DashBoardCardServiceLoader extends DataAsyncTaskLibLoader<CardServiceDataResponse> {

    private String cardServiceUrl;

    public DashBoardCardServiceLoader(Context context, String cardServiceUrl) {
        super(context);
        this.cardServiceUrl = cardServiceUrl;
    }

    @Override
    protected CardServiceDataResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getSingleCardServiceData(cardServiceUrl);
    }
}
