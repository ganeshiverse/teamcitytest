package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.data.responses.CityListResponse;
import com.axsystech.excelicare.data.responses.CountryListResponse;
import com.axsystech.excelicare.loaders.CountryListLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * Created by someswar on 6/30/2015.
 */
public class CountryListActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private static final String GET_COUNTRY_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.CountryListActivity.GET_COUNTRY_LIST_LOADER";

    private CountryListActivity mActivity;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.listView)
    private ListView listView;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    private ArrayList<CountryListResponse.CountryDetails> countryDetailsAL;
    private CountryListAdapter countryListAdapter;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity,getString(R.string.signup_text)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_message_inbox);
        mActivity = this;

        preloadData();
        loadCityList();
        readIntentData();
        addEventListeners();

    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_country));
    }

    private void loadCityList() {
        // get City list from Server
        if(CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            CountryListLoader countryListLoader = new CountryListLoader(mActivity, "", AxSysConstants.COUNTRY_LOOKUP_ID, "");
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_COUNTRY_LIST_LOADER), countryListLoader);
        } else {
            showToast(R.string.error_loading_countries);
        }
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if(id == getLoaderHelper().getLoaderId(GET_COUNTRY_LIST_LOADER)) {
            if(result != null && result instanceof CountryListResponse) {
                CountryListResponse countryListResponse = (CountryListResponse) result;

                if(countryListResponse.getSystemLookupsAL() != null && countryListResponse.getSystemLookupsAL().size() > 0) {
                    countryDetailsAL = countryListResponse.getSystemLookupsAL().get(0).getCountryeDetailsAL();
                    // Bind data to listview
                    if(countryListAdapter == null) {
                        countryListAdapter = new CountryListAdapter(countryDetailsAL);
                        listView.setAdapter(countryListAdapter);
                    } else {
                        countryListAdapter.updateData(countryDetailsAL);
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if(id == getLoaderHelper().getLoaderId(GET_COUNTRY_LIST_LOADER)) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                // update to list adapter with original items
                if(countryListAdapter != null) {
                    countryListAdapter.updateData(countryDetailsAL);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if(text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    private class CountryListAdapter extends BaseAdapter {

        private ArrayList<CountryListResponse.CountryDetails> countryDetailsArrayList;

        public CountryListAdapter(ArrayList<CountryListResponse.CountryDetails> countryDetailsArrayList) {
            this.countryDetailsArrayList = countryDetailsArrayList;
        }

        public void updateData(ArrayList<CountryListResponse.CountryDetails> stateDetailsArrayList) {
            this.countryDetailsArrayList = stateDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (countryDetailsArrayList != null && countryDetailsArrayList.size() > 0) ? countryDetailsArrayList.size() : 0 ;
        }

        @Override
        public CountryListResponse.CountryDetails getItem(int position) {
            return countryDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final CountryListResponse.CountryDetails rowObj = getItem(position);

            if(convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.circles_list_row, parent, false);
            }

            TextView cityNameTextView = (TextView) convertView.findViewById(R.id.circleNameTextView);
            cityNameTextView.setText(!TextUtils.isEmpty(rowObj.getValue()) ? rowObj.getValue() : "");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(AxSysConstants.EXTRA_SELECTED_COUNTRY, rowObj);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });

            return convertView;
        }
    }

}
