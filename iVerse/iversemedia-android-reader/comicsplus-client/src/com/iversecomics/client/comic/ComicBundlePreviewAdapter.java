package com.iversecomics.client.comic;

import android.graphics.Bitmap;

import com.iversecomics.bundle.ComicBundle;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.bitmap.loader.WebBitmapLoader;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class ComicBundlePreviewAdapter implements IMyComicSourceAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(ComicBundlePreviewAdapter.class);
    Bitmap[] previewBitmaps = null;
    private Comic comic;
    private IComicSourceListener comicSourceListener;
    private WebBitmapLoader bitmapLoader;
    private BitmapManager bitmapManager;

    public ComicBundlePreviewAdapter(Comic comic) {
        this.comic = comic;
        final IverseApplication iverse = IverseApplication.getApplication();
        bitmapManager = iverse.createBitmapManager();
        bitmapLoader = new WebBitmapLoader(iverse.getHttpClient());
    }

    @Override
    public int getImageCount() {
        return ComicBundle.PREVIEW_PAGE_SIZE;
    }

    @Override
    public URI getImageURI(int index) {
        return URI.create(comic.getPreviewImageUriForIndex(index).toString());
    }

    @Override
    public void setComicSourceListener(IComicSourceListener listener) {
        this.comicSourceListener = listener;
        if (comicSourceListener == null) {
            comicSourceListener = new LogComicSourceListener();
        }
    }

    @Override
    public ComicMode getComicMode() {
        return ComicMode.PREVIEW;
    }

    @Override
    public void setComicMode(ComicMode mode) {
        //nothing to do
    }

    @Override
    public Bitmap loadComicPage(int index) {
        Bitmap bitmap = bitmapManager.getCacheDir().getBitmap(getImageURI(index));
        if (comicSourceListener != null) {
            comicSourceListener.onBitmapLoaded(index, bitmap);
        }
        return bitmap;
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public void open() throws IOException {
        for (int i = 0; i < getImageCount(); i++) {
            URI imageURI = getImageURI(i);
            if (bitmapManager.getCacheDir().getBitmap(imageURI) == null) {
                Bitmap bitmap = bitmapLoader.loadBitmap(imageURI);
                bitmapManager.getCacheDir().addBitmap(imageURI, bitmap);
                bitmap.recycle();
            }
        }
    }

    @Override
    public void onOpenFinished(boolean bSuccess) {
        if (bSuccess && this.comicSourceListener != null)
            this.comicSourceListener.onComicSourceUpdated(this);
    }
}
