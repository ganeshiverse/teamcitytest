package com.babylon.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.babylon.R;
import com.babylon.utils.L;
import com.squareup.picasso.Picasso;

public class WelcomeTourFragment extends Fragment {

    public interface WelcomeTourListener {
        void closeTour();
    }

    public static final String TAG = WelcomeTourFragment.class.getSimpleName();
    private ImageView imageView;
    private WelcomeTourListener welcomeTourListener;
    private int screenResourceId;

    public static WelcomeTourFragment newInstance(int screenResourceId, boolean isLastScreen) {
        WelcomeTourFragment fragment = new WelcomeTourFragment();
        Bundle args = new Bundle();
        args.putInt("screenResourceId", screenResourceId);
        args.putBoolean("isLastScreen", isLastScreen);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_welcome_tour, container, false);
        imageView = (ImageView)rootView.findViewById(R.id.welcome_tour_imageview);

        screenResourceId = getArguments().getInt("screenResourceId");
        boolean isLastScreen = getArguments().getBoolean("isLastScreen");

        if (!isLastScreen) {
            ImageButton continueButton = (ImageButton) rootView.findViewById(R.id.tour_continue_button);
            continueButton.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            imageView.setImageResource(screenResourceId);
        } catch (OutOfMemoryError e) {
            L.e(TAG, e.getMessage(), e);
            Picasso.with(getActivity()).load(screenResourceId).into(imageView);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        imageView.setImageResource(0);
        imageView.setImageBitmap(null);
        System.gc();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof WelcomeTourListener) {
            welcomeTourListener = (WelcomeTourListener) activity;
        }
    }

    public void onContinueClick(View view) {
        if(welcomeTourListener != null) {
            welcomeTourListener.closeTour();
        }
    }
}
