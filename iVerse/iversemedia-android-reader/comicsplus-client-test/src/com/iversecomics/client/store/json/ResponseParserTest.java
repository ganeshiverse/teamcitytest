package com.iversecomics.client.store.json;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.net.Uri;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.json.JSONObject;

public class ResponseParserTest extends AbstractStubActivityTestCase {
    private static class ParseCollector implements IComicStoreParsingEvent {
        public List<Comic>     comics     = new ArrayList<Comic>();
        public List<String>    featured   = new ArrayList<String>();
        public List<Genre>     genres     = new ArrayList<Genre>();
        public Set<Group>      groups     = new HashSet<Group>();
        public List<Publisher> publishers = new ArrayList<Publisher>();

        @Override
        public void onParsedComic(Comic comic) {
            comics.add(comic);
        }

        @Override
        public void onParsedGenre(Genre genre) {
            genres.add(genre);
        }

        @Override
        public void onParsedGroup(Group group) {
            groups.add(group);
        }

        @Override
        public void onParsedPublisher(Publisher publisher) {
            publishers.add(publisher);
        }

        @Override
        public void onParseEnd() {
            /* ignore */
        }

        @Override
        public void onParseStart() {
            comics.clear();
            featured.clear();
            genres.clear();
            publishers.clear();
        }
    }

    @MediumTest
    public void testParseComic() throws Exception {
        JSONObject json = loadAssetJsonObject("api2-product-startrek-year-4.json");
        ResponseParser parser = new ResponseParser();
        ParseCollector collector = new ParseCollector();
        parser.parseComics(json, collector);

        assertEquals("Comics list size", 1, collector.comics.size());

        String bundleName = "com.iversecomics.idw.star.trek.year.four.enterprise.experiment.one";
        Comic trek = null;
        for (Comic comic : collector.comics) {
            if (bundleName.equals(comic.getComicBundleName())) {
                trek = comic;
            }
        }

        assertNotNull("Should have found the star trek comic", trek);
        assertEquals("Comic.name", "Star Trek: Year Four  Enterprise Experiment #1", trek.getName());

        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getSmallImageUri(trek.getImageFileName());

        assertEquals("Comic.urlCoverSmall", "http://iversemedia.cachefly.net/public/store/cover/small/0000000089.png",
                uri.toString());
    }

    @MediumTest
    public void testParseComicsListNew() throws Exception {
        JSONObject json = loadAssetJsonObject("api2-products-new-releases.json");
        ResponseParser parser = new ResponseParser();
        ParseCollector collector = new ParseCollector();
        parser.parseComics(json, collector);

        assertEquals("Comics list size", 50, collector.comics.size());

        String bettyBundleName = "com.iversecomics.archie.comics.betty.one.hundred.forty.three.07012011";
        Comic betty = null;
        for (Comic comic : collector.comics) {
            if (bettyBundleName.equals(comic.getComicBundleName())) {
                betty = comic;
            }
        }

        assertNotNull("Should have found the Betty #143 comic", betty);
        assertEquals("Comic.name", "Betty #143", betty.getName());

        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getSmallImageUri(betty.getImageFileName());

        assertEquals("Comic.urlCoverSmall", "http://iversemedia.cachefly.net/public/store/cover/small/8000000327.png",
                uri.toString());
    }

    @MediumTest
    public void testParseComicsListTopPaid() throws Exception {
        JSONObject json = loadAssetJsonObject("api2-products-top-paid.json");
        ResponseParser parser = new ResponseParser();
        ParseCollector collector = new ParseCollector();
        parser.parseComics(json, collector);

        assertEquals("Comics list size", 50, collector.comics.size());

        String farscapeBundleId = "com.iversecomics.boom.studios.farscape.one";
        Comic farscape = null;
        for (Comic comic : collector.comics) {
            if (farscapeBundleId.equals(comic.getComicBundleName())) {
                farscape = comic;
            }
        }

        assertNotNull("Should have found the farscape comic", farscape);
        assertEquals("Comic.name", "Farscape #1", farscape.getName());

        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getSmallImageUri(farscape.getImageFileName());

        assertEquals("Comic.urlCoverSmall", "http://iversemedia.cachefly.net/public/store/cover/small/0000000042.png",
                uri.toString());
    }

    @MediumTest
    public void testParseGenreList() throws Exception {
        JSONObject json = loadAssetJsonObject("api2-categories-list.json");
        ResponseParser parser = new ResponseParser();
        ParseCollector collector = new ParseCollector();
        parser.parseGenres(json, collector);

        assertEquals("Genre List Size", 10, collector.genres.size());

        Genre scifi = null;
        for (Genre genre : collector.genres) {
            if ("sci.fi".equals(genre.getLegacyKey())) {
                scifi = genre;
            }
        }

        assertNotNull("Should have found the sci.fi genre", scifi);
        assertEquals("Scifi.name", "Sci-Fi", scifi.getName());
    }

    @MediumTest
    public void testParsePublisher() throws Exception {
        JSONObject json = loadAssetJsonObject("api2-supplier-1821.json");
        ResponseParser parser = new ResponseParser();
        ParseCollector collector = new ParseCollector();
        parser.parsePublishers(json, collector);

        assertEquals("Publisher list size", 1, collector.publishers.size());

        Publisher zed = null;
        for (Publisher publisher : collector.publishers) {
            Log.d("TEST", "publisher = " + publisher.getName());
            if ("1821 Comics".equals(publisher.getName())) {
                zed = publisher;
            }
        }

        assertNotNull("Should have found the zed publisher", zed);
        assertEquals("Zed.name", "1821 Comics", zed.getName());
        assertEquals("Zed.bannerImageFileName", "publisher_1821", zed.getBannerImageFileName());

        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getPublisherBannerImageUri(zed.getBannerImageFileName());

        assertEquals("Zed.bannerImageFileName to URI",
                "http://iversemedia.cachefly.net/public/store/publisher/banner/publisher_1821.png", uri.toString());
    }

    @MediumTest
    public void testParsePublisherList() throws Exception {
        JSONObject json = loadAssetJsonObject("api2-supplier-list.json");
        ResponseParser parser = new ResponseParser();
        ParseCollector collector = new ParseCollector();
        parser.parsePublishers(json, collector);

        assertEquals("Publisher list size", 46, collector.publishers.size());
        assertEquals("Groups list size", 576, collector.groups.size());

        Publisher red5 = null;
        for (Publisher publisher : collector.publishers) {
            if ("Red 5 Comics".equals(publisher.getName())) {
                red5 = publisher;
            }
        }

        assertNotNull("Should have found the red5 publisher", red5);
        assertEquals("Red5.name", "Red 5 Comics", red5.getName());

        ServerConfig serverConfig = createTestServerConfig();
        Uri uri = serverConfig.getPublisherBannerImageUri(red5.getBannerImageFileName());
        assertEquals("Red5.bannerImageFileName to URI",
                "http://iversemedia.cachefly.net/public/store/publisher/banner/publisher_red_five.png", uri.toString());
    }

}
