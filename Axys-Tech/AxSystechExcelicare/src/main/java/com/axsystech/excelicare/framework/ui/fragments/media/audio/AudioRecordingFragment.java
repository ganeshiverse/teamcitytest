package com.axsystech.excelicare.framework.ui.fragments.media.audio;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.activities.MediaDetailsActivity;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.FilterType;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;

import java.io.File;
import java.io.IOException;


public class AudioRecordingFragment extends BaseFragment implements OnClickListener, ConfirmationDialogFragment.OnConfirmListener {

    private MainContentActivity mActivity;
    private static final String LOG_TAG = "AudioRecordingFragment";

    private static final int SAVE_AUDIO_CONFIRMATION_DIALOG_ID = 105;

    private ImageView mRecordButton = null;
    protected ExtAudioRecorder mExtAudioRecorder;
    private TextView tv_start;
    private TextView tv_end;
    private SeekBar seek;
    private TextView mListenButton = null;
    private Button mDonebtn;
    private Button mDonebtn1;
    private Button mSaveAudio;

    private MediaPlayer mPlayer = null;

    private String mFilePath = null;

    private boolean isStopped = false;

    private boolean mRecordbtnClick;
    private boolean isFinalRecordDone = false;
    //for update listen button text.If it is true listen button otherwise pause
    private boolean isItListenerOrPause;

    private Handler mHandler = new Handler();

    private Chronometer mRecText;

    private GIFDecoderView mRecordAnimation;

    //for maintain paused state duration
    private int pausedSate;

    protected long totalDuration;
    //for timer update
    private long timeWhenStopped = 0;
    public String oldfilePath;

    @InjectSavedState
    private User mActiveUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recordingvoice, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        initViews();
        readIntentArgs();
        clearOldData();
    }

    @Override
    protected void initActionBar() {
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, "Record New Audio"));
        mActivity.disableDrawerToggle();
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
    }

    public void initViews() {
        mRecordButton = (ImageView) getView().findViewById(R.id.btn_record);
        mListenButton = (TextView) getView().findViewById(R.id.btn_listen);
        tv_start = (TextView) getView().findViewById(R.id.tv_start);
        tv_end = (TextView) getView().findViewById(R.id.tv_end);
        mDonebtn = (Button) getView().findViewById(R.id.btn_done);
        mDonebtn1 = (Button) getView().findViewById(R.id.btn_done1);

        mSaveAudio = (Button) getView().findViewById(R.id.btn_save_audio);
        mRecordAnimation = ((GIFDecoderView) getView().findViewById(R.id.img_recording));
        seek = (SeekBar) getView().findViewById(R.id.seekBar1);
        mRecText = (Chronometer) getView().findViewById(R.id.tv_recordtext);
        mRecordButton.setVisibility(View.VISIBLE);
        mListenButton.setVisibility(View.INVISIBLE);
        mSaveAudio.setEnabled(false);

        mDonebtn.setOnClickListener(this);
        mRecordButton.setOnClickListener(this);
        mListenButton.setOnClickListener(this);
        mRecordAnimation.setOnClickListener(this);
        mSaveAudio.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This deals with start recording a new voice note.
     */
    private void startPlaying() {
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(mFilePath);
                mPlayer.prepare();
            } catch (IOException e) {
            }
        }
        mPlayer.seekTo(pausedSate);
        mPlayer.start();

        updateProgressBar();
        tv_start.setText("0");

        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                totalDuration = mPlayer.getDuration();
            }
        });
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            /**
             * When user stops moving the progress hanlder
             * */
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mPlayer != null) {
                    int currentPosition = progressToTimer(
                            seekBar.getProgress(), (int) totalDuration);

                    // forward or backward to certain seconds
                    mPlayer.seekTo(currentPosition);
                }
                // update timer progress again
                updateProgressBar();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(mUpdateTimeTask);

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (fromUser) {
                   /* mPlayer.seekTo(progress);
                    seek.setProgress(progress);*/
                    int value = seekBar.getProgress();
                    String valueString = value + "";
                    seekBar.setThumb(writeOnDrawable(R.drawable.slider, valueString));
                    Log.d("TAG", "Thumb is being touched");
                }
                pausedSate = progress;


                mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mediaPlayer.seekTo(0);
                        pausedSate = 0;
                        isItListenerOrPause = false;
                        mListenButton.setText(getString(R.string.text_listen));

                    }
                });
            }
        });

    }

    public BitmapDrawable writeOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId)
                .copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();

        paint.setColor(Color.BLACK);
        paint.setTextSize(20);

        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, 0, bm.getHeight() / 2, paint);

        return new BitmapDrawable(bm);
    }

    private void stopPlaying() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
            mPlayer.seekTo(0);
            mPlayer.start();
        }
    }

    private void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (mPlayer != null) {
                totalDuration = mPlayer.getDuration();
                long currentDuration = mPlayer.getCurrentPosition();

                // Displaying time completed playing
                tv_start.setText("" + milliSecondsToTimer(currentDuration));
                // Displaying Total Duration time
                tv_end.setText("" + milliSecondsToTimer(totalDuration));

                // Updating progress bar
                int progress = (int) (getProgressPercentage(
                        currentDuration, totalDuration));
                seek.setProgress(progress);
            }
            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };


    private void setName() {
        mFilePath = Constants.TEMP_VOICE_FILES_STORAGE_PATH;
        File info_dir = new File(mFilePath);
        if (!info_dir.exists())
            info_dir.mkdirs();

        File initial_file = new File(Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav");
        if (initial_file.exists())
            mFilePath = Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/temp_two.wav";
        else
            mFilePath = Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav";

        File data = new File(mFilePath);
        if (!data.exists()) {
            try {
                data.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void startRecording() {
        mRecText.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
        mRecText.start();

        mRecordAnimation.setVisibility(View.VISIBLE);
        mRecordButton.setVisibility(View.INVISIBLE);
        setName();
        // Start recording
        mExtAudioRecorder = ExtAudioRecorder.getInstanse(false);
        oldfilePath = mFilePath;
        mExtAudioRecorder.setOutputFile(oldfilePath);
        mExtAudioRecorder.prepare();

        try {
            mExtAudioRecorder.start();
            // Start animating the image
            if (mRecordAnimation != null) {
                mRecordAnimation.setVisibility(View.VISIBLE);
                mRecordAnimation.playFull();
            }
            mRecText.setVisibility(View.VISIBLE);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops recording and appends the voice file is first one exists
     */
    public void stopRecordingAndRename() {
        try {
            stopRecording();
            mRecordAnimation.setVisibility(View.INVISIBLE);
            mRecordButton.setVisibility(View.VISIBLE);
            timeWhenStopped = mRecText.getBase() - SystemClock.elapsedRealtime();
            mRecText.stop();

            File first_file = new File(Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav");
            File second_file = new File(Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav");
            File final_file = new File(Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav");

            if (first_file.exists() && second_file.exists()) {
                if (!final_file.exists())
                    final_file.createNewFile();

                mExtAudioRecorder.CombineWaveFile(first_file.getAbsolutePath(), second_file.getAbsolutePath(), final_file.getAbsolutePath());
                first_file.delete();
                second_file.delete();
                File newFile = new File(Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav");
                if (!newFile.exists())
                    newFile.createNewFile();
                final_file.renameTo(newFile);
                mFilePath = Constants.TEMP_VOICE_FILES_STORAGE_PATH + "/audio_" + System.currentTimeMillis() + ".wav";
            }
        } catch (Throwable e) {
        }
    }


    public void stopRecording() {
        // Stop recording
        if (mExtAudioRecorder != null) {
            mExtAudioRecorder.stop();
            mExtAudioRecorder.release();
        }
    }

    /**
     * Checks for temp voice files and deletes if there are any.
     */
    private void clearOldData() {
        File vDir = new File(Constants.TEMP_VOICE_FILES_STORAGE_PATH);
        if (vDir != null && vDir.isDirectory()) {
            File[] vFiles = vDir.listFiles();
            for (int i = 0; i < vFiles.length; i++) {
                if (!vFiles[i].isDirectory())
                    vFiles[i].delete();
            }
        }
    }


    public void stopPlayer() {

        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    /**
     * This method is used to show or hide the required views to the screen
     */
    private void showOrHideViews(boolean isVisible) {
        if (isVisible) {

            mListenButton.setVisibility(View.VISIBLE);
            // et_voice_titile.setText(getVoiceFileName());
            mRecText.setVisibility(View.GONE);
        } else {

            mListenButton.setVisibility(View.GONE);
        }
    }


    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    public static String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    /**
     * Function to get Progress percentage
     *
     * @param currentDuration
     * @param totalDuration
     */
    public static int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done:
                if (!mRecordbtnClick) {
                    showToast(R.string.error_record_audio);
                    showOrHideViews(false);
                } else {
                    stopRecordingAndRename();
                    isFinalRecordDone = true;
                    ((ImageView) getActivity().findViewById(R.id.btn_record)).setImageResource(R.drawable.recordmic);
                    showOrHideViews(true);
                    mListenButton.setVisibility(View.VISIBLE);
                    //mDonebtn1.setVisibility(View.VISIBLE);
                    //mDonebtn.setVisibility(View.GONE);
                    mSaveAudio.setVisibility(View.VISIBLE);
                    mSaveAudio.setEnabled(true);
                    mDonebtn.setEnabled(false);

                    //for update end duration here
                    MediaPlayer mp = MediaPlayer.create(getActivity(), Uri.parse(mFilePath));
                    int duration = mp.getDuration();

                    Trace.d("Duration:::", "Duration::::" + milliSecondsToTimer(duration));
                    tv_end.setText(milliSecondsToTimer(duration));
                }

                break;
            case R.id.btn_record:
                if (isFinalRecordDone) {
                    mRecordButton.setClickable(false);
                } else {
                    mRecordbtnClick = true;
                    if (isStopped)
                        isStopped = false;
                    stopPlaying();
                    startRecording();
                }
                break;
            case R.id.btn_listen:

                isItListenerOrPause = !isItListenerOrPause;
                isStopped = true;
                // if the voice is getting recorded or is not created, don't do any action
                if ((mExtAudioRecorder != null && mExtAudioRecorder.getState() == ExtAudioRecorder.State.RECORDING) || mExtAudioRecorder == null) {
                    stopRecordingAndRename();
                }
                if (mExtAudioRecorder != null) {
                    final File fi = new File(mFilePath);
                    if (fi.exists() && !fi.isDirectory()) {
                        if (isItListenerOrPause) {
                            mListenButton.setText(getString(R.string.text_pause));
                            if (mPlayer != null)
                                pausedSate = mPlayer.getCurrentPosition();
                            startPlaying();

                        } else {

                            mListenButton.setText(getString(R.string.text_listen));
                            if (mPlayer != null && mPlayer.isPlaying()) {
                                mPlayer.pause();
                                pausedSate = mPlayer.getCurrentPosition();
                            }
                        }
                    } else
                        showToast("No record to share");
                    // Util.showAlert(getActivity(), Constants.PROJECT_TITLE, getResources().getString(R.string.alert_no_record_share), false);
                } else {
                    showToast("No record to share");
                }
                break;
            case R.id.img_recording:
                mRecordAnimation.stopRendering();
                stopRecordingAndRename();
                mRecordAnimation.setVisibility(View.INVISIBLE);
                mRecordButton.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_save_audio:
                final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.save_audio_confirmation));
                bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, SAVE_AUDIO_CONFIRMATION_DIALOG_ID);
                bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                confirmationDialogFragment.setArguments(bundle);
                showDialog(confirmationDialogFragment);
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == mActivity.RESULT_OK) {
            if (requestCode == AxSysConstants.MEDIA_DETAILS_REQUEST_CODE) {
                if (data != null) {
                    if (data.hasExtra(AxSysConstants.EXTRA_SAVED_MEDIA_OBJECT)) {
                        MediaItemDbModel mediaItemDbModel = (MediaItemDbModel) data.getSerializableExtra(AxSysConstants.EXTRA_SAVED_MEDIA_OBJECT);

                        mActivity.onMediaSaved(mediaItemDbModel);

                        mActivity.getSupportFragmentManager().popBackStack();
                    } else if (data.hasExtra(AxSysConstants.EXTRA_UPLOAD_MEDIA_OBJECT)) {
                        MediaItemDbModel mediaItemDbModel = (MediaItemDbModel) data.getSerializableExtra(AxSysConstants.EXTRA_UPLOAD_MEDIA_OBJECT);

                        mActivity.onMediaUpload(mediaItemDbModel);

                        mActivity.getSupportFragmentManager().popBackStack();
                    }
                }
            }
        }
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == SAVE_AUDIO_CONFIRMATION_DIALOG_ID) {
            final File fi = new File(mFilePath);
            if (fi.exists() && !fi.isDirectory()) {

                Intent intent = new Intent(mActivity, MediaDetailsActivity.class);
                intent.putExtra(AxSysConstants.EXTRA_MEDIA_FILE_PATH, Uri.fromFile(fi));
                intent.putExtra(AxSysConstants.EXTRA_SHOULD_ENABLE_DATETIME_PICKER, false);
                intent.putExtra(AxSysConstants.EXTRA_SELECTED_MEDIA_TYPE, FilterType.FILTER_AUDIOS);
                startActivityForResult(intent, AxSysConstants.MEDIA_DETAILS_REQUEST_CODE);

            }
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == SAVE_AUDIO_CONFIRMATION_DIALOG_ID) {
            mActivity.onBackPressed();
        }
    }
}

