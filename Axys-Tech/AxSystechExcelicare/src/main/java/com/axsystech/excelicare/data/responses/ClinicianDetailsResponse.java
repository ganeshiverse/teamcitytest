package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class ClinicianDetailsResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private ClinicianDetailsData clinicianDetailsData;

    public ClinicianDetailsData getClinicianDetailsData() {
        return clinicianDetailsData;
    }

    public class ClinicianDetailsData implements Serializable {

        @SerializedName("AdditionalInfo")
        private String additionalInfo;

        @SerializedName("ClinicDetails")
        private ArrayList<ClinicDetails> clinicDetailsArrayList;

        @SerializedName("ClinicianId")
        private String clinicianId;

        @SerializedName("ClinicianName")
        private String clinicianName;

        @SerializedName("ClinicianUserID")
        private String clinicianUserID;

        @SerializedName("Email")
        private String email;

        @SerializedName("Experience")
        private String experience;

        @SerializedName("likes")
        private String likes;

        @SerializedName("IsFavorite")
        private String isFavorite;

        @SerializedName("Location")
        private String location;

        @SerializedName("Message")
        private String message;

        @SerializedName("Phone")
        private String phone;

        @SerializedName("Photo")
        private String photo;

        @SerializedName("Qualification")
        private String qualification;

        @SerializedName("Rating")
        private String rating;

        @SerializedName("Speciality")
        private String speciality;

        @SerializedName("URL")
        private String URL;

        public String getAdditionalInfo() {
            return additionalInfo;
        }

        public ArrayList<ClinicDetails> getClinicDetailsArrayList() {
            return clinicDetailsArrayList;
        }

        public String getClinicianId() {
            return clinicianId;
        }

        public String getClinicianName() {
            return clinicianName;
        }

        public String getClinicianUserID() {
            return clinicianUserID;
        }

        public String getEmail() {
            return email;
        }

        public String getExperience() {
            return experience;
        }

        public String getLikes() {
            return likes;
        }

        public String getLocation() {
            return location;
        }

        public String getMessage() {
            return message;
        }

        public String getPhone() {
            return phone;
        }

        public String getPhoto() {
            return photo;
        }

        public String getQualification() {
            return qualification;
        }

        public String getRating() {
            return rating;
        }

        public String getSpeciality() {
            return speciality;
        }

        public String getURL() {
            return URL;
        }

        public String getIsFavorite() {
            return isFavorite;
        }

        public void setIsFavorite(String isFavorite) {
            this.isFavorite = isFavorite;
        }

    }

    public class ClinicDetails implements Serializable {

        @SerializedName("Address1")
        private String address1;

        @SerializedName("Address2")
        private String address2;

        @SerializedName("Address3")
        private String address3;

        @SerializedName("City")
        private String city;

        @SerializedName("Email")
        private String email;

        @SerializedName("Fee")
        private String fee;

        @SerializedName("ID")
        private String ID;

        @SerializedName("Mode")
        private String mode;

        @SerializedName("Name")
        private String name;

        @SerializedName("Phone")
        private String phone;

        @SerializedName("Postcode")
        private String postcode;

        @SerializedName("Slot")
        private String slot;

        @SerializedName("State")
        private String state;

        @SerializedName("URL")
        private String URL;

        @SerializedName("Timings")
        private ArrayList<Timings> timingsArrayList;

        public String getAddress1() {
            return address1;
        }

        public String getAddress2() {
            return address2;
        }

        public String getAddress3() {
            return address3;
        }

        public String getCity() {
            return city;
        }

        public String getEmail() {
            return email;
        }

        public String getFee() {
            return fee;
        }

        public String getID() {
            return ID;
        }

        public String getMode() {
            return mode;
        }

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }

        public String getPostcode() {
            return postcode;
        }

        public String getSlot() {
            return slot;
        }

        public String getState() {
            return state;
        }

        public String getURL() {
            return URL;
        }

        public ArrayList<Timings> getTimingsArrayList() {
            return timingsArrayList;
        }
    }

    public class Timings implements Serializable {

        @SerializedName("TimingSlots")
        private String timingSlots;

        public String getTimingSlots() {
            return timingSlots;
        }
    }

}
