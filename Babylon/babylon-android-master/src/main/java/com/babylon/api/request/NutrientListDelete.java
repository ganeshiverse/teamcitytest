package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestDelete;
import com.babylon.api.request.base.Urls;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class NutrientListDelete extends AbsRequestDelete<Void> {

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.NUTRIENT_LIST);
        return TextUtils.concat(url, urlParams).toString();
    }

    public static NutrientListDelete newFoodListDelete(List<String> deleteIds) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        int index = 0;
        for (String idString : deleteIds) {
            if (idString != null) {
                params.add(new BasicNameValuePair("patientNutrientsIds[" + index + "]", idString));
            }
            index++;
        }

        return (NutrientListDelete) new NutrientListDelete().withParams(params);
    }
}
