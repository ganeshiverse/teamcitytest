package com.axsystech.excelicare.framework.ui.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AppMenuListResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleUserPermissionsDataResponse;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.data.responses.ProxieDetails;
import com.axsystech.excelicare.data.responses.ProxyListResponse;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.adapters.MenuOptionsAdapter;
import com.axsystech.excelicare.framework.ui.adapters.MenuProxiesAdapter;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.framework.ui.fragments.DashboardFragment;
import com.axsystech.excelicare.framework.ui.fragments.EmptyFragment;
import com.axsystech.excelicare.framework.ui.fragments.appointments.AppointmentListFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.CirclesListFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.InvitationSentFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.MediaDetailsFragment;
import com.axsystech.excelicare.framework.ui.fragments.media.MediaFragment;
import com.axsystech.excelicare.framework.ui.fragments.settings.RemoveDevicesFragment;
import com.axsystech.excelicare.framework.ui.fragments.settings.UserPreferences;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageSentFragment;
import com.axsystech.excelicare.framework.ui.messages.SearchDoctorsFragment;
import com.axsystech.excelicare.loaders.AppMenuSerializationLoader;
import com.axsystech.excelicare.loaders.ChangeAccountLoader;
import com.axsystech.excelicare.loaders.CircleUserPermissionsLoader;
import com.axsystech.excelicare.loaders.DeleteProfilePhotoLoader;
import com.axsystech.excelicare.loaders.DisplayProxyListLoader;
import com.axsystech.excelicare.loaders.GetAppMenuListLoader;
import com.axsystech.excelicare.loaders.LogoutLoader;
import com.axsystech.excelicare.loaders.RemoveCircleLoader;
import com.axsystech.excelicare.loaders.SaveUserDBLoader;
import com.axsystech.excelicare.loaders.UploadProfilePicLoader;
import com.axsystech.excelicare.loaders.UrlCardAsyncTask;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.RegisterApp;
import com.axsystech.excelicare.util.SharedPreferenceUtil;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * Date: 06.05.14
 * Time: 13:42
 *
 * @author SomeswarReddy
 */
public class MainContentActivity extends BaseActivity implements UrlCardAsyncTask.LoadUrlCardListener,
        MenuProxiesAdapter.CaredMemberClickListener, View.OnClickListener, ConfirmationDialogFragment.OnConfirmListener,
        MediaDetailsFragment.OnMediaSavedListener {

    private String TAG = MainContentActivity.class.getSimpleName();

    private static final String GET_APP_MENU_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.GET_APP_MENU_LIST_LOADER";
    private static final String APP_MENU_LIST_SERIALIZATION_LOADER = "com.axsystech.excelicare.framework.ui.activities.APP_MENU_LIST_SERIALIZATION_LOADER";
    private static final String GET_PROXY_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.GET_PROXY_LIST_LOADER";
    private static final String CHANGE_ACCOUNT_LOADER = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.CHANGE_ACCOUNT_LOADER";

    private static final String SAVE_USER_IN_DB_LOADER = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.SAVE_USER_IN_DB_LOADER";
    private static final String DELETE_PROFILE_PHOTO_LOADER = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.DELETE_PROFILE_PHOTO_LOADER";
    private static final String UPLOAD_PROFILE_PHOTO_LOADER = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.UPLOAD_PROFILE_PHOTO_LOADER";
    private static final String GET_CIRCLE_USER_PERMISSIONS = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.GET_CIRCLE_USER_PERMISSIONS";

    private static final int DELETE_PHOTO_DIALOG_ID = 105;

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_GROUP_POSITION = "selected_navigation_drawer_group_position";
    private static final String STATE_SELECTED_CHILD_POSITION = "selected_navigation_drawer_child_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    private static final String NAVIGATION_FRAGMENT_TAGS = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.NAVIGATION_FRAGMENT_TAGS_%d_%d";
    private static final String LOGOUT = "com.axsystech.excelicare.framework.ui.activities.MainContentActivity.LOGOUT";

    private MainContentActivity mActivity;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerExpandableListView;

    private LinearLayout mLeftMenuHeaderLayout;
    private ImageView mUserIconImageView;
    private TextView mEditPhotoTextView;
    private TextView usernameTextView;
    private TextView siteIdTextView;
    private CheckBox mProxyDropdownCheckbox;

    private LinearLayout mLeftMenuFooterLayout;
    private TextView mAddNewProxieTextView;

    private int mCurrentSelectedGroupPosition = 0;
    private int mCurrentSelectedChildPosition = 0;

    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    @InjectSavedState
    private User mActiveUser;

    private Toolbar mToolbar;

    String memberId;
    String moduleNameKey;
    private ArrayList<ProxieDetails> mProxieDetailsAL;

    private MenuOptionsAdapter mMenuOptionsAdapter;
    private MenuProxiesAdapter mMenuProxiesAdapter;

    private boolean mIsSelectedLoginUser = true;

    public static HashMap<String, Integer> stringIntegerHashMap = new HashMap<>();

    GoogleCloudMessaging gcm;
    String regId;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = this;

        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        // Adding tool bar as actionbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        initViews();
        updateLeftMenuHeaderView();
        registerWithGCM();

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedGroupPosition = savedInstanceState.getInt(STATE_SELECTED_GROUP_POSITION);
            mCurrentSelectedChildPosition = savedInstanceState.getInt(STATE_SELECTED_CHILD_POSITION);
            mFromSavedInstanceState = true;
        }

        // Load MenuOptions from server
        if (mActiveUser != null && mActiveUser.getToken() != null) {
            final GetAppMenuListLoader menuListLoader = new GetAppMenuListLoader(mActivity, mActiveUser.getToken(), "");
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_APP_MENU_LIST_LOADER), menuListLoader);
        }

        //service call for Proxy Users List
        if (mActiveUser != null && mActiveUser.getToken() != null) {
            final DisplayProxyListLoader displayProxy = new DisplayProxyListLoader(mActivity, mActiveUser.getToken(), "", "1");
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_PROXY_LIST_LOADER), displayProxy);
        }
    }


    private void registerWithGCM() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            regId = getRegistrationId();
            if (!regId.isEmpty()) {

            } else {
                if (checkPlayServices()) {
                    Trace.i(TAG, "before registration: id" + regId);
                    gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    regId = getRegistrationId();
                    Trace.i(TAG, "after registration: id" + regId);
                    if (regId.isEmpty()) {
                        String token = SharedPreferenceUtil.getStringFromSP(mActivity, AxSysConstants.LOGIN_TOKEN);
                        //new RegisterApp(getApplicationContext(),gcm,token,getAppVersion()).execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Device already registered", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.i(TAG, "No valid google play services apk found");
                }
            }
        }
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion();
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private int getAppVersion() {
        try {
            PackageInfo packageInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(LoginActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    @Override
    protected void initActionBar() {
        // update actionbar title

    }

    /**
     * This method will return the toolbar wchich is attached to screen actionbar
     *
     * @return {@link Toolbar}
     */
    public Toolbar getToolBar() {
        return mToolbar;
    }

    /**
     * This method is used to instantiate the required UI elements in the screen.
     */
    private void initViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerExpandableListView = (ExpandableListView) findViewById(R.id.leftMenuExpandableListView);
        mDrawerExpandableListView.setGroupIndicator(null);
        mDrawerExpandableListView.setChildIndicator(null);
        mDrawerExpandableListView.setChildDivider(getResources().getDrawable(R.color.left_memu_view_color));
        mDrawerExpandableListView.setDivider(getResources().getDrawable(R.color.left_memu_view_color));
        mDrawerExpandableListView.setDividerHeight(2);
//        setLeftNavDrawerWidth();

        // This will be called when user click on Group item in MenuOptions or MenuProxies adapter
        mDrawerExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (mDrawerExpandableListView.getExpandableListAdapter() != null &&
                        mDrawerExpandableListView.getExpandableListAdapter() instanceof MenuOptionsAdapter) {
                    // If user click on group option, just expand it when selected group having child items
                    if (mMenuOptionsAdapter.getChildrenCount(groupPosition) > 0) {
                        return false;
                    }

                    selectItem(groupPosition, 0, true);
                }

                return true;
            }
        });

        // This will be called when user click on Child item in MenuOptions or MenuProxies adapter
        mDrawerExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // navigate to appropriate fragment on child item click
                selectItem(groupPosition, childPosition, false);

                return false;
            }
        });

        // Expandable ListView Header view
        //mLeftMenuHeaderLayout = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.leftmenu_header_layout, null);
        mUserIconImageView = (ImageView)findViewById(R.id.userIconImageView);
        mUserIconImageView.setOnClickListener(this);
        mEditPhotoTextView = (TextView)findViewById(R.id.editPhotoTextView);
        mEditPhotoTextView.setOnClickListener(this);
        usernameTextView = (TextView)findViewById(R.id.usernameTextView);
        siteIdTextView = (TextView)findViewById(R.id.siteIdTextView);
        mProxyDropdownCheckbox = (CheckBox)findViewById(R.id.proxyDropdownCheckbox);
       // mDrawerExpandableListView.addHeaderView(mLeftMenuHeaderLayout);

        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        usernameTextView.setTypeface(bold);
        siteIdTextView.setTypeface(regular);
        mProxyDropdownCheckbox.setTypeface(regular);

        // Expandable ListView Footer view - to add & display proxies
        mLeftMenuFooterLayout = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.leftmenu_bottom_layout, null);
        mAddNewProxieTextView = (TextView) mLeftMenuFooterLayout.findViewById(R.id.addProxyButton);
        mAddNewProxieTextView.setTypeface(regular);
        mDrawerExpandableListView.addFooterView(mLeftMenuFooterLayout);
        mLeftMenuFooterLayout.setVisibility(View.GONE);

        // based on the checkbox checked status we will set appropriate adapter for ExpandableListView - I mean either MenuOprionsAdapter or MenuProxiesAdapter
        mProxyDropdownCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Enable Arrow Up here - display proxies
                    mLeftMenuFooterLayout.setVisibility(View.VISIBLE);
                } else {
                    // Enable Arrow Down here - display menu list
                    mLeftMenuFooterLayout.setVisibility(View.GONE);
                }
                if (isChecked) {
                    // Menu Proxies
                    if (mMenuProxiesAdapter != null) {
                        mDrawerExpandableListView.setAdapter(mMenuProxiesAdapter);
                        mMenuProxiesAdapter.updateProxieDetails(mProxieDetailsAL);
                    } else {
                        mMenuProxiesAdapter = new MenuProxiesAdapter(mActivity, mActivity, mProxieDetailsAL);
                        mDrawerExpandableListView.setAdapter(mMenuProxiesAdapter);
                    }
                } else {
                    //
                    mDrawerExpandableListView.setAdapter(mMenuOptionsAdapter);
                    mMenuOptionsAdapter.notifyDataSetChanged();
                }
            }
        });

        // This will be called when user click on 'Add New Account' footer element added to ExpandableListView
        mAddNewProxieTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addproxyIntent = new Intent(getApplicationContext(), SignUpActivity.class);
                addproxyIntent.putExtra(AxSysConstants.EXTRA_SHOULD_ADD_NEW_PROXIE, true);
                addproxyIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, mActiveUser.getSiteID());
                startActivityForResult(addproxyIntent, AxSysConstants.ADD_PROXY_REQUEST_CODE);
            }
        });
    }

    /**
     * This method will prepare the Header view added to ExpandableListView, this header view will display active user informatio like
     * Profile Pic, active user name, active user site id, etc
     */
    private void updateLeftMenuHeaderView() {
        // read login user object from global data model
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        // Display logged-in user info and profile pic
        if (mActiveUser != null) {
            if (!TextUtils.isEmpty(mActiveUser.getPhoto())) {
                String imagePath = mActiveUser.getPhoto();
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                Picasso picasso = new Picasso.Builder(mActivity).listener(
                        new Picasso.Listener() {
                            @Override
                            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                                exception.printStackTrace();
                            }

                        }).build();
                picasso.load(imagePath)
                        .fit()
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .transform(new CircleTransform())
                        .into(mUserIconImageView);

            }

            usernameTextView.setText(mActiveUser.getForename() + " " + mActiveUser.getSurname());
            siteIdTextView.setText(!TextUtils.isEmpty(mActiveUser.getSitename()) ? mActiveUser.getSitename() : "");

            // Update Edit Profile photo label based on Photo URL availablity in DB
            updateEditPhotoLabel();
        }
    }

    /**
     * This method will update the lable displayed below the active user profile pic, i.e. 'Delete Photo, or 'Add Photo'
     */
    private void updateEditPhotoLabel() {
        String userProfilePic = getProfileUrlOfLogInUserInDB(mActiveUser);

        if (!TextUtils.isEmpty(userProfilePic)) {
            // user profile pic available in DB
            mEditPhotoTextView.setText(getString(R.string.delete_photo));

            // Update latest profile pic
            if (!TextUtils.isEmpty(mActiveUser.getPhoto())) {
                String imagePath = mActiveUser.getPhoto();
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                Picasso.with(mActivity)
                        .load(imagePath)
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .transform(new CircleTransform())
                        .into(mUserIconImageView);
            }
        } else {
            // user profile pic not available in DB
            mEditPhotoTextView.setText(getString(R.string.add_photo));

            Picasso.with(mActivity)
                    .load("https://www.google.com")
                    .placeholder(R.drawable.user_icon)
                    .error(R.drawable.user_icon)
                    .transform(new CircleTransform())
                    .into(mUserIconImageView);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == AxSysConstants.ADD_PROXY_REQUEST_CODE) {
                // Load MenuOptions from server
//                final GetAppMenuListLoader menuListLoader = new GetAppMenuListLoader(mActivity, GlobalDataModel.getInstance().getToken(), "");
//                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_APP_MENU_LIST_LOADER), menuListLoader);

                //service call for Proxy Users List
                if (mActiveUser != null && mActiveUser.getToken() != null) {
                    final DisplayProxyListLoader displayProxy = new DisplayProxyListLoader(mActivity, mActiveUser.getToken(), "", "1");
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_PROXY_LIST_LOADER), displayProxy);
                }
            } else if (requestCode == AxSysConstants.INTENT_ID_TAKE_PHOTO) {
                setCapturedImage();

            } else if (requestCode == AxSysConstants.INTENT_ID_PICK_PHOTO) {
                setGalleryImage(data);

            }
        }
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_GROUP_POSITION, mCurrentSelectedGroupPosition);
        outState.putInt(STATE_SELECTED_CHILD_POSITION, mCurrentSelectedChildPosition);
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        if (mDrawerToggle != null) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(GET_APP_MENU_LIST_LOADER)) {

            if (result != null && result instanceof AppMenuListResponse) {
                AppMenuListResponse appMenuListResponse = (AppMenuListResponse) result;

                // Add BaseExpandableListAdapter for menu options expandable listview
                setUpMenuOptions(appMenuListResponse);

                // Cache Application menu list in Files and use them in offline mode
                final AppMenuSerializationLoader appMenuSerializationLoader = new AppMenuSerializationLoader(mActivity, appMenuListResponse);
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(APP_MENU_LIST_SERIALIZATION_LOADER), appMenuSerializationLoader);

                // Select either the default item (0) or the last selected item.
                // Pre-select the first item in first group
                new Handler().post(new Runnable() {
                    public void run() {
                        if (mDrawerExpandableListView.getExpandableListAdapter() != null &&
                                mDrawerExpandableListView.getExpandableListAdapter() instanceof MenuOptionsAdapter) {
                            if (mMenuOptionsAdapter.getChildrenCount(mCurrentSelectedGroupPosition) > 0) {
                                selectItem(mCurrentSelectedGroupPosition, 0, false);
                            } else if (mMenuOptionsAdapter.getGroupCount() > 0) {
                                selectItem(0, 0, true);
                            }
                        }
                    }
                });
            }

        } else if (id == getLoaderHelper().getLoaderId(APP_MENU_LIST_SERIALIZATION_LOADER)) {
            if (result != null && result instanceof Boolean) {
                if ((Boolean) result) {
                    // SERIALIZATION SUCCESS
                    Trace.d(TAG, "SUCCESS : Serialized data is saved in" + AxSysConstants.APP_MENU_LIST_FILE.getAbsolutePath());
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_PROXY_LIST_LOADER)) {

            if (result != null && result instanceof ProxyListResponse) {
                ProxyListResponse proxyUserList = (ProxyListResponse) result;

                // Update received token value in GlobalObject
                if (proxyUserList.getProxyListResponseData() != null &&
                        proxyUserList.getProxyListResponseData().getProxieDetailsAL() != null &&
                        proxyUserList.getProxyListResponseData().getProxieDetailsAL().size() > 0) {

                    // cache the retrieved proxies in activity global variable
                    mProxieDetailsAL = proxyUserList.getProxyListResponseData().getProxieDetailsAL();
                } else {
                    mProxieDetailsAL = new ArrayList<ProxieDetails>();
                }

                // if user selected proxy is a cared member (not login user), then add LoginUser as proxy to switch user back
                if (!mIsSelectedLoginUser) {
                    // Add LoginUser into Cared members list
                    ProxieDetails loginUserAsProxy = new ProxieDetails();
                    loginUserAsProxy.setUserID(GlobalDataModel.getInstance().getLoginUser().getEcUserID() + "");
                    loginUserAsProxy.setForeName(GlobalDataModel.getInstance().getLoginUser().getForename());
                    loginUserAsProxy.setSurName(GlobalDataModel.getInstance().getLoginUser().getSurname());
                    loginUserAsProxy.setImageURL(GlobalDataModel.getInstance().getLoginUser().getPhoto());
                    loginUserAsProxy.setIsLoginUser(true);

                    if (mProxieDetailsAL != null) {
                        mProxieDetailsAL.add(loginUserAsProxy);
                    }
                }

                if (mProxyDropdownCheckbox.isChecked()) {
                    if (mMenuProxiesAdapter != null) {
                        mMenuProxiesAdapter.updateProxieDetails(mProxieDetailsAL);
                    } else {
                        mMenuProxiesAdapter = new MenuProxiesAdapter(mActivity, mActivity, mProxieDetailsAL);
                        mDrawerExpandableListView.setAdapter(mMenuProxiesAdapter);
                    }
                }

                mProxyDropdownCheckbox.setChecked(false);

            }
        } else if (id == getLoaderHelper().getLoaderId(CHANGE_ACCOUNT_LOADER)) {
            if (result != null && result instanceof LoginResponse) {
                LoginResponse loginResponse = (LoginResponse) result;

                User activeUser = ConverterResponseToDbModel.getUser(loginResponse, "", "");

                // Cache Login, Active user details in Global Object
                if (activeUser != null) {
                    GlobalDataModel.getInstance().setActiveUser(activeUser);
                    mActiveUser = activeUser;

                    updateLeftMenuHeaderView();
                }

                // Load MenuOptions from server
                if (mActiveUser != null && mActiveUser.getToken() != null) {
                    final GetAppMenuListLoader menuListLoader = new GetAppMenuListLoader(mActivity, mActiveUser.getToken(), "");
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_APP_MENU_LIST_LOADER), menuListLoader);
                }

                //service call for Proxy Users List
                if (mActiveUser != null && mActiveUser.getToken() != null) {
                    final DisplayProxyListLoader displayProxy = new DisplayProxyListLoader(mActivity, mActiveUser.getToken(), "", "1");
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_PROXY_LIST_LOADER), displayProxy);
                }

                if (mActiveUser != null && mActiveUser.getToken() != null) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        CircleUserPermissionsLoader circleUserPermissionsLoader = new CircleUserPermissionsLoader(mActivity, mActiveUser.getToken(), memberId);
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CIRCLE_USER_PERMISSIONS), circleUserPermissionsLoader);

                    }
                }

            }
        } else if (id == getLoaderHelper().getLoaderId(DELETE_PROFILE_PHOTO_LOADER)) {
            if (result != null && result instanceof BaseResponse) {
                BaseResponse baseResponse = (BaseResponse) result;

                if (!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                }

                if (/*baseResponse.isSuccess()*/ true) {
                    // Photo delete success
                    // Update empty photo url for active user in Db
                    mActiveUser.setPhoto("");

                    final SaveUserDBLoader saveUserDBLoader = new SaveUserDBLoader(this, mActiveUser);
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_USER_IN_DB_LOADER), saveUserDBLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_USER_IN_DB_LOADER)) {
            if (result != null && result instanceof User) {
                User savedUserObj = (User) result;
                Trace.d(TAG, "savedUserObj:" + savedUserObj);

                // Update Edit Profile photo label based on Photo URL availablity in DB
                updateEditPhotoLabel();
            }
        } else if (id == getLoaderHelper().getLoaderId(UPLOAD_PROFILE_PHOTO_LOADER)) {
            if (result != null && result instanceof BaseResponse) {
                BaseResponse baseResponse = (BaseResponse) result;

                if (!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                }

                if (/*baseResponse.isSuccess()*/ true) {
                    // Photo delete success
                    // Update empty photo url for active user in Db
                    if (mImageFileUri != null && !TextUtils.isEmpty(mImageFileUri.getPath())) {
                        mActiveUser.setPhoto(mImageFileUri.getPath());
                    }

                    final SaveUserDBLoader saveUserDBLoader = new SaveUserDBLoader(this, mActiveUser);
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_USER_IN_DB_LOADER), saveUserDBLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(LOGOUT)) {
            if (result != null && result instanceof BaseResponse) {
                BaseResponse baseResponse = (BaseResponse) result;
                stringIntegerHashMap.clear();
                ;
                // Navigate user to LOGIN screen
                Intent loginIntent = new Intent(this, LoginActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(loginIntent);
                finish();
            }

        } else if (id == getLoaderHelper().getLoaderId(GET_CIRCLE_USER_PERMISSIONS)) {
            if (result != null && result instanceof CircleUserPermissionsDataResponse) {
                CircleUserPermissionsDataResponse circleUserPermissionsDataResponse = (CircleUserPermissionsDataResponse) result;

                if (circleUserPermissionsDataResponse.getDataObject() != null) {

                    ArrayList<CircleUserPermissionsDataResponse.ModuleList> permissionToModuleListAl = circleUserPermissionsDataResponse.getDataObject().getPermissionToModuleListsAl();
                    ArrayList<CircleUserPermissionsDataResponse.ModuleList> groupsModuleListAl = circleUserPermissionsDataResponse.getDataObject().getModuleListsAl();

                    for (int i = 0; i < permissionToModuleListAl.size(); i++) {

                        CircleUserPermissionsDataResponse.ModuleList moduleList = permissionToModuleListAl.get(i);
                        ArrayList<CircleUserPermissionsDataResponse.AreaModuleList> areaModuleLists = moduleList.getAreaModuleListAl();

                        if (areaModuleLists != null && areaModuleLists.size() > 0) {

                            for (int j = 0; j < areaModuleLists.size(); j++) {

                                moduleNameKey = areaModuleLists.get(j).getAreaName();

                                CircleUserPermissionsDataResponse.AreaModuleList areaModuleList = areaModuleLists.get(j);
                                ArrayList<CircleUserPermissionsDataResponse.GroupsModuleList> groupsModuleListsAl = areaModuleList.getGroupsModuleListAl();

                                for (int k = 0; k < groupsModuleListsAl.size(); k++) {

                                    String defaultValue = groupsModuleListsAl.get(k).getPermissionsModuleListAL().get(k).getDefaultValue();
                                    stringIntegerHashMap.put(moduleNameKey, Integer.parseInt(groupsModuleListsAl.get(k).getPermissionsModuleListAL().get(k).getDefaultValue()));

                                    System.out.println("HashMapValues" + stringIntegerHashMap.toString());

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);

        if (id == getLoaderHelper().getLoaderId(GET_APP_MENU_LIST_LOADER)) {
            // Check for the menu list in file and display them if available
            final AppMenuDeSerializationAsyncTask appMenuDeSerializationTask = new AppMenuDeSerializationAsyncTask();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                appMenuDeSerializationTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                appMenuDeSerializationTask.execute();
            }

        } else if (id == getLoaderHelper().getLoaderId(APP_MENU_LIST_SERIALIZATION_LOADER)) {
            Trace.d(TAG, "FAILURE : Serialized data is not saved in " + AxSysConstants.APP_MENU_LIST_FILE.getAbsolutePath());

        } else if (id == getLoaderHelper().getLoaderId(GET_PROXY_LIST_LOADER)) {

        } else if (id == getLoaderHelper().getLoaderId(DELETE_PROFILE_PHOTO_LOADER)) {

        } else if (id == getLoaderHelper().getLoaderId(UPLOAD_PROFILE_PHOTO_LOADER)) {

        }
    }

    /**
     * This class is defined to load the Serialised MenuList object and will be used to diaplay menu options in Offline mode
     */
    private class AppMenuDeSerializationAsyncTask extends AsyncTask<Void, Void, AppMenuListResponse> {

        @Override
        protected AppMenuListResponse doInBackground(Void... params) {
            AppMenuListResponse appMenuListResponse = null;

            try {
                FileInputStream fileIn = new FileInputStream(AxSysConstants.APP_MENU_LIST_FILE);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                appMenuListResponse = (AppMenuListResponse) in.readObject();

                in.close();
                fileIn.close();
            } catch (IOException i) {
                i.printStackTrace();
                return null;
            } catch (ClassNotFoundException c) {
                c.printStackTrace();
                return null;
            }

            return appMenuListResponse;
        }

        @Override
        protected void onPostExecute(AppMenuListResponse appMenuListResponse) {
            super.onPostExecute(appMenuListResponse);

            if (appMenuListResponse != null && appMenuListResponse instanceof AppMenuListResponse) {
                Trace.d(TAG, "SUCCESS : De-Serialized data:" + appMenuListResponse);

                // Add BaseExpandableListAdapter for menu options expandable listview
                setUpMenuOptions(appMenuListResponse);

                // Select either the default item (0) or the last selected item.
                // Pre-select the first item in first group
                new Handler().post(new Runnable() {
                    public void run() {
                        if (mDrawerExpandableListView.getExpandableListAdapter() != null &&
                                mDrawerExpandableListView.getExpandableListAdapter() instanceof MenuOptionsAdapter) {
                            if (mMenuOptionsAdapter.getChildrenCount(mCurrentSelectedGroupPosition) > 0) {
                                selectItem(mCurrentSelectedGroupPosition, 0, false);
                            } else if (mMenuOptionsAdapter.getGroupCount() > 0) {
                                selectItem(0, 0, true);
                            }
                        }
                    }
                });
            } else {
                Trace.d(TAG, "FAILURE : De-Serialized data is not retrieved from " + AxSysConstants.APP_MENU_LIST_FILE.getAbsolutePath());
            }
        }
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     */
    public void setUpMenuOptions(AppMenuListResponse appMenuListResponse) {
        mMenuOptionsAdapter = new MenuOptionsAdapter(mActivity, appMenuListResponse);

        mDrawerExpandableListView.setAdapter(mMenuOptionsAdapter);
        mDrawerExpandableListView.setItemChecked(mCurrentSelectedGroupPosition, true);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(mActivity,                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */) {
            @Override
            public void onDrawerClosed(final View drawerView) {
                super.onDrawerClosed(drawerView);

                syncState();
            }

            @Override
            public void onDrawerOpened(final View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivity);
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                syncState();
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        ((ActionBarActivity) mActivity).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ActionBarActivity) mActivity).getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * This method when user click on Group or Child item in displayed MenuOptions list
     *
     * @param groupPosition  selected group position
     * @param childPosition  selected child position
     * @param isGroupClicked flag to decide whether group selected or child selected
     */
    public void selectItem(final int groupPosition, final int childPosition, boolean isGroupClicked) {
        mCurrentSelectedGroupPosition = groupPosition;
        mCurrentSelectedChildPosition = childPosition;

        final String fragmentTag = String.format(NAVIGATION_FRAGMENT_TAGS, groupPosition, childPosition);

        if (isGroupClicked) {
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }

            if (mDrawerExpandableListView.getExpandableListAdapter() != null &&
                    mDrawerExpandableListView.getExpandableListAdapter() instanceof MenuOptionsAdapter) {
                // If user click on group option, just expand it
                if (mMenuOptionsAdapter.getGroup(groupPosition) instanceof AppMenuListResponse.MenuListData) {
                    final AppMenuListResponse.MenuListData selectedGroupMenuItem = (AppMenuListResponse.MenuListData) mMenuOptionsAdapter.getGroup(groupPosition);
                    navigateOnMenuGroupItemSelected(fragmentTag, selectedGroupMenuItem);
                }
            }
        } else {
            // if user click on child option, navigate to appropriate fragment
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }

            if (mDrawerExpandableListView.getExpandableListAdapter() != null &&
                    mDrawerExpandableListView.getExpandableListAdapter() instanceof MenuOptionsAdapter) {
                final AppMenuListResponse.Items selectedChildMenuItem = mMenuOptionsAdapter.getChild(groupPosition, childPosition);
                navigateOnMenuChildItemSelected(fragmentTag, selectedChildMenuItem);
            }
        }

        if (mDrawerExpandableListView.getExpandableListAdapter() != null &&
                mDrawerExpandableListView.getExpandableListAdapter() instanceof MenuOptionsAdapter) {
            mMenuOptionsAdapter.notifyDataSetChanged();
        }

        // we should remove all loaders from running because they will not removed before fragment attach again
        // ((FragmentLoaderTaskListener) fragment).getLoaderHelper().removeAllLoadersFromRunningLoaders();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * This method will be called when user click on MenuOptions group item (if no childs are available to group, this method will be called
     * to navigate appropriate module based on module id of selected group item)
     *
     * @param fragmentTag           string value to uniquely identify the navigating fragment
     * @param selectedGroupMenuItem selected group item object ({@link AppMenuListResponse.MenuListData})
     *                              - which will contain the {@link com.axsystech.excelicare.data.responses.AppMenuListResponse.Params}
     *                              object where you will have module id and dashboard id, etc
     */
    private void navigateOnMenuGroupItemSelected(String fragmentTag, AppMenuListResponse.MenuListData selectedGroupMenuItem) {
        // we use ActivityManager because we manipulate activities state and views
        if (selectedGroupMenuItem != null) {
            // Update action bar title based on selected menu item
            ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, selectedGroupMenuItem.getText()));

            if (selectedGroupMenuItem.getParams() != null && !TextUtils.isEmpty(selectedGroupMenuItem.getParams().getModuleID())) {
                // navigate based on Module ID
                // Check 'OutputType' param, based on that display card whether in Dashboard, Popup, Browser
                if (!TextUtils.isEmpty(selectedGroupMenuItem.getParams().getOutputType())) {
                    if (selectedGroupMenuItem.getParams().getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_BROWSER)) {
                        // Display card in Browser
                        displayProgressLoader(false);
                        UrlCardAsyncTask urlCardAsyncTask = new UrlCardAsyncTask(
                                !TextUtils.isEmpty(selectedGroupMenuItem.getParams().getValue()) ? selectedGroupMenuItem.getParams().getValue() : "",
                                "1", selectedGroupMenuItem.getParams().getOutputType().trim(), mActivity);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            urlCardAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            urlCardAsyncTask.execute();
                        }
                    } else if (selectedGroupMenuItem.getParams().getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_POPUP)) {
                        // Display card in PopUp
                        displayProgressLoader(false);
                        UrlCardAsyncTask urlCardAsyncTask = new UrlCardAsyncTask(
                                !TextUtils.isEmpty(selectedGroupMenuItem.getParams().getValue()) ? selectedGroupMenuItem.getParams().getValue() : "",
                                "1", selectedGroupMenuItem.getParams().getOutputType().trim(), mActivity);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            urlCardAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            urlCardAsyncTask.execute();
                        }

                    } else /*if(params.getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_NORMAL))*/ {
                        navigateBasedOnModuleId(selectedGroupMenuItem.getText(), fragmentTag, selectedGroupMenuItem.getParams().getModuleID(),
                                selectedGroupMenuItem.getParams().getValue(), selectedGroupMenuItem.getParams().getRecordId(), true, false);
                    }
                } else {
                    navigateBasedOnModuleId(selectedGroupMenuItem.getText(), fragmentTag, selectedGroupMenuItem.getParams().getModuleID(),
                            selectedGroupMenuItem.getParams().getValue(), selectedGroupMenuItem.getParams().getRecordId(), true, false);
                }
            }
        }
    }

    /**
     * This method will called when user click on child item in displayed menu options.
     *
     * @param fragmentTag           string value to uniquely identify the navigating fragment
     * @param selectedChildMenuItem selected child item object ({@link com.axsystech.excelicare.data.responses.AppMenuListResponse.Items})
     *                              - which will contain the {@link com.axsystech.excelicare.data.responses.AppMenuListResponse.Params}
     *                              object where you will have module id and dashboard id, etc
     */
    private void navigateOnMenuChildItemSelected(String fragmentTag, AppMenuListResponse.Items selectedChildMenuItem) {
        // we use ActivityManager because we manipulate activities state and views
        if (selectedChildMenuItem != null) {
            // Update action bar title based on selected menu item
            ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(selectedChildMenuItem.getText());

            if (selectedChildMenuItem.getParams() != null && !TextUtils.isEmpty(selectedChildMenuItem.getParams().getModuleID())) {
                // navigate based on Module ID

                // Check 'OutputType' param, based on that display card whether in Dashboard, Popup, Browser
                if (!TextUtils.isEmpty(selectedChildMenuItem.getParams().getOutputType())) {
                    if (selectedChildMenuItem.getParams().getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_BROWSER)) {
                        // Display card in Browser
                        displayProgressLoader(false);
                        UrlCardAsyncTask urlCardAsyncTask = new UrlCardAsyncTask(
                                !TextUtils.isEmpty(selectedChildMenuItem.getParams().getValue()) ? selectedChildMenuItem.getParams().getValue() : "",
                                "1", selectedChildMenuItem.getParams().getOutputType().trim(), mActivity);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            urlCardAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            urlCardAsyncTask.execute();
                        }
                    } else if (selectedChildMenuItem.getParams().getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_POPUP)) {
                        // Display card in PopUp
                        displayProgressLoader(false);
                        UrlCardAsyncTask urlCardAsyncTask = new UrlCardAsyncTask(
                                !TextUtils.isEmpty(selectedChildMenuItem.getParams().getValue()) ? selectedChildMenuItem.getParams().getValue() : "",
                                "1", selectedChildMenuItem.getParams().getOutputType().trim(), mActivity);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            urlCardAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            urlCardAsyncTask.execute();
                        }

                    } else /*if(params.getOutputType().trim().equalsIgnoreCase(AxSysConstants.URL_CARD_NORMAL))*/ {
                        navigateBasedOnModuleId(selectedChildMenuItem.getText(), fragmentTag, selectedChildMenuItem.getParams().getModuleID(),
                                selectedChildMenuItem.getParams().getValue(), selectedChildMenuItem.getParams().getRecordId(), true, false);
                    }
                } else {
                    navigateBasedOnModuleId(selectedChildMenuItem.getText(), fragmentTag, selectedChildMenuItem.getParams().getModuleID(),
                            selectedChildMenuItem.getParams().getValue(), selectedChildMenuItem.getParams().getRecordId(), true, false);
                }
            }
        }
    }

    /**
     * This method will be used to get the fragment manager object
     *
     * @return {@link FragmentManager} object reference
     */
    public FragmentManager getSupportFragmentMngr() {
        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();

        /**
         * This will be called when any fragment added or removed from fragment backstack,
         * I mean it will be called whenever the count of the backstack is changing upon user actions
         */
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Trace.i(TAG, "back stack changed");
                int backCount = getSupportFragmentManager().getBackStackEntryCount();
                if (backCount == 0) {
                    // block where back has been pressed. since backstack is zero.
                    if (mActivity instanceof MainContentActivity) {
                        mActivity.enableDraweToggle();
                    }
                }

                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_layout);
                if (fragment != null) {
                    fragment.onResume();
                }
            }
        });

        return fragmentManager;
    }

    /**
     * This method will be used to call the appropriate fragment or activity based on selected option module id, module id will there in selected menu option
     * {@link com.axsystech.excelicare.data.responses.AppMenuListResponse.Params} object.
     *
     * @param actionBarTitle      slected menu option title
     * @param fragmentTag         string value to uniquely identify the navigating fragment
     * @param moduleId            selecte menu option module id
     * @param dashboardId         selected menu option dashboard id
     * @param recordId            selected menu option record id
     * @param shouldEnableLeftNav flag to decide whether menu option should displayed or not
     * @param addToBackStack      flag to decide whether navigating fragment should be added to backstack or no
     * @see com.axsystech.excelicare.data.responses.AppMenuListResponse.Params
     */
    public void navigateBasedOnModuleId(String actionBarTitle, String fragmentTag, String moduleId, String dashboardId, String recordId,
                                        boolean shouldEnableLeftNav, boolean addToBackStack) {
        final FragmentManager activityFragmentManager = getSupportFragmentMngr();
        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

        if (moduleId != null) {
            if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_MEDIA_ITEMS)) {
                // navigate to Media module
                MediaFragment mediaFragment = new MediaFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_DISPLAY_LOCAL_MEDIA, true);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                mediaFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, mediaFragment, MediaFragment.class.getSimpleName());
                if (addToBackStack) {
                    ft.addToBackStack(DashboardFragment.class.getSimpleName());
                }

            } else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_DASHBOARD)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                DashboardFragment dashboardFragment = new DashboardFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                bundle.putString(AxSysConstants.EXTRA_SELECTED_MENU_DASHBOARD_ID, dashboardId);
                bundle.putString(AxSysConstants.EXTRA_SELECTED_MENU_MODULE_ID, moduleId);
                bundle.putString(AxSysConstants.EXTRA_SELECTED_CARD_RECORD_ID, recordId);
                dashboardFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, dashboardFragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(DashboardFragment.class.getSimpleName());
                }
            }
            // Circles
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_CIRCLES)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                CirclesListFragment circlesListFragment = new CirclesListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "My Care Circles");
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                circlesListFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, circlesListFragment, CirclesListFragment.class.getSimpleName());
                if (addToBackStack) {
                    ft.addToBackStack(CirclesListFragment.class.getSimpleName());
                }
            }
            //Invitations Received
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.CIRCLE_INVITATIONS_RECIEVED)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                InvitationReceivedFragment invitationReceivedFragment = new InvitationReceivedFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                invitationReceivedFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, invitationReceivedFragment, InvitationReceivedFragment.class.getSimpleName());
                if (addToBackStack) {
                    ft.addToBackStack(InvitationReceivedFragment.class.getSimpleName());
                }
            }
            //Invitations Sent
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.CIRCLE_INVITATIONS_SENT)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                InvitationSentFragment invitationSentFragment = new InvitationSentFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                invitationSentFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, invitationSentFragment, InvitationSentFragment.class.getSimpleName());
                if (addToBackStack) {
                    ft.addToBackStack(InvitationSentFragment.class.getSimpleName());
                }
            }
            //Message inbox
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_MESSAGE_INBOX)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                final MessageInboxFragment messageInboxfragment = new MessageInboxFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                messageInboxfragment.setArguments(bundle);
                ft.replace(R.id.container_layout, messageInboxfragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(MessageInboxFragment.class.getSimpleName());
                }
            }
            //Message Sent
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_MESSAGE_SENT)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                final MessageSentFragment messageSentfragment = new MessageSentFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                messageSentfragment.setArguments(bundle);
                ft.replace(R.id.container_layout, messageSentfragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(MessageSentFragment.class.getSimpleName());
                }
            }
            // Message to Circle
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_MESSAGE_TO_CIRCLE)) {
                // Navigate to appropriate fragment based on the selected menu item text
                //  clearFragmentsBackStackInActivity(mActivity);
                MessageComposeFragment messageComposeFragment = new MessageComposeFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_compose_message));
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_CIRCLE);
                messageComposeFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
//                if (addToBackStack) {
                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
//                }
            }
            // Message to Doctor / HCP
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_MESSAGE_TO_PROVIDER)) {
                // Navigate to appropriate fragment based on the selected menu item text
                // clearFragmentsBackStackInActivity(mActivity);

                MessageComposeFragment messageComposeFragment = new MessageComposeFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_compose_message));
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_COMPOSE_MSG_TO, AxSysConstants.COMPOSE_MSG_TO_PROVIDERS);
                messageComposeFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
//                if (addToBackStack) {
                ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
//                }
            }
            // Navigate to Appointment List Fragment
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_APPOINTMENTS_LIST)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                AppointmentListFragment appointmentListFragment = new AppointmentListFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                appointmentListFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, appointmentListFragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(AppointmentListFragment.class.getSimpleName());
                }
            }
            // Navigate to Searech Doctors Fragment
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_BOOK_APPOINTMENTS)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                SearchDoctorsFragment searchDoctorsFragment = new SearchDoctorsFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_find_doctors));
                bundle.putString(AxSysConstants.EXTRA_PROVIDER_SOURCE, AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS);
                searchDoctorsFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, searchDoctorsFragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(SearchDoctorsFragment.class.getSimpleName());
                }
            }
            // Navigate to User Prefrences List Fragment
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_USERPREFERENCES)) {
                // Navigate to appropriate fragment based on the selected menu item text
  //            clearFragmentsBackStackInActivity(mActivity);

                UserPreferences userPreferencesFragment = new UserPreferences();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_user_preferences));
                userPreferencesFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, userPreferencesFragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(UserPreferences.class.getSimpleName());
                }
            }
            // Navigate to User Prefrences List Fragment
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_REMOVE_DEVICES)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                RemoveDevicesFragment removeDevicesFragment = new RemoveDevicesFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_unregister_devices));
                removeDevicesFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, removeDevicesFragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(RemoveDevicesFragment.class.getSimpleName());
                }
            }

            // Logout
            else if (moduleId.trim().equalsIgnoreCase(AxSysConstants.MENU_ID_LOGOUT)) {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);


                if (CommonUtils.hasInternet()) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.text_alert));
                    alert.setMessage(getString(R.string.confirmation_to_logout));
                    alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {
                                if (CommonUtils.hasInternet()) {
                                    displayProgressLoader(false);
                                    LogoutLoader logoutLoader = new LogoutLoader(mActivity, mActiveUser.getToken());
                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LOGOUT), logoutLoader);

                                } else {
                                    showToast(R.string.error_no_network);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }
                    });
                    alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

                    alert.show();
                } else {

                }

            }
            // Empty fragment
            else {
                // Navigate to appropriate fragment based on the selected menu item text
//            clearFragmentsBackStackInActivity(mActivity);

                EmptyFragment emptyFragment = new EmptyFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, shouldEnableLeftNav);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, actionBarTitle);
                emptyFragment.setArguments(bundle);
                ft.add(R.id.container_layout, emptyFragment, EmptyFragment.class.getSimpleName());
                if (addToBackStack) {
                    ft.addToBackStack(EmptyFragment.class.getSimpleName());
                }
            }

            ft.commit();
        }
    }

    @Override
    public void onMediaSaved(MediaItemDbModel mediaItemDbModel) {
        // Get Current visible fragment from container & pass saved media to respective fragment
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentByTag(MediaFragment.class.getSimpleName());
        if (fragment instanceof MediaFragment) {
            MediaFragment mediaFragment = (MediaFragment) fragment;

            mediaFragment.addNewOrUpdateMediaItemToMediaAdapter(mediaItemDbModel);
        }
    }

    @Override
    public void onMediaUpload(MediaItemDbModel mediaItemDbModel) {
        // Get Current visible fragment from container & pass saved media to respective fragment
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentByTag(MediaFragment.class.getSimpleName());
        if (fragment instanceof MediaFragment) {
            MediaFragment mediaFragment = (MediaFragment) fragment;

            mediaFragment.uploadMediaToServer(mediaItemDbModel);
        }
    }

    /**
     * Set Dynamically left Navigation drawer width
     */
    private void setLeftNavDrawerWidth() {
        String devRes = CommonUtils.getDeviceResolution((ActionBarActivity) mActivity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mDrawerExpandableListView.getLayoutParams();
        boolean isTablet = CommonUtils.isTablet(mActivity);

        if (isTablet) {
            params.width = (int) (Integer.parseInt(devRes.substring(0, devRes.indexOf("X"))) * 3.5 / 5);
        } else {
            params.width = (int) (Integer.parseInt(devRes.substring(0, devRes.indexOf("X"))) * 3.5 / 4);
        }
        mDrawerExpandableListView.setLayoutParams(params);
    }

    /**
     * Enables the drawer functionality.
     */
    public void enableDraweToggle() {
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    /**
     * Locks the drawer functionality and remove's the drawer icon on action
     */
    public void disableDrawerToggle() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        removeListenerandLockNavBar();
    }

    /**
     * Removes the listener for the navigation drawer and marks the mode as
     * locked.
     */
    private void removeListenerandLockNavBar() {
        mDrawerLayout.setDrawerListener(null);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onUrlCardLoadSuccess(GetDashBoardCardsResponse dashBoardCardsResponse, String outputType) {
        hideProgressLoader();

        GetDashBoardCardsResponse.ListObject cardObject = dashBoardCardsResponse.getListObject().get(0);
        if (cardObject.getTypeLU().trim().equalsIgnoreCase(AxSysConstants.DBC_URL_DATACRD)) {
            // If card type is URL CARD , just prepare the header, footer and load webview with Service URL
            if (outputType.equalsIgnoreCase(AxSysConstants.URL_CARD_BROWSER)) {
                // Load URL in Browser
                String cardUrl = cardObject.getServiceURL();
                if (cardUrl != null && !cardUrl.startsWith("http")) {
                    cardUrl = "http://" + cardUrl;
                }

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(cardUrl));
                startActivity(browserIntent);

            } else if (outputType.equalsIgnoreCase(AxSysConstants.URL_CARD_POPUP)) {
                // Load Url Card as popup
                String cardUrl = cardObject.getServiceURL();
                if (cardUrl != null && !cardUrl.startsWith("http")) {
                    cardUrl = "http://" + cardUrl;
                }

                CommonUtils.displayWebViewInDialog(mActivity, cardUrl, dashBoardCardsResponse.getName());
            }
        }
    }

    @Override
    public void onUrlCardLoadFailure(String errorMessage) {
        hideProgressLoader();
        showToast(errorMessage);
    }

    @Override
    public void onClickCaredMember(ProxieDetails proxieDetails) {
        // Update global variable in-order to verify that the selected cared member is proxy or login user
        mIsSelectedLoginUser = proxieDetails.isLoginUser();

        memberId = proxieDetails.getUserCircleMember_ID();

        // send change account request
        displayProgressLoader(true);
        ChangeAccountLoader changeAccountLoader = null;

        if (proxieDetails.isLoginUser()) {
            changeAccountLoader = new ChangeAccountLoader(mActivity, GlobalDataModel.getInstance().getLoginUser().getToken(), "0");
        } else {
            changeAccountLoader = new ChangeAccountLoader(mActivity, mActiveUser.getToken(), proxieDetails.getUserID());
        }
        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CHANGE_ACCOUNT_LOADER), changeAccountLoader);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editPhotoTextView:
                if (CommonUtils.hasInternet()) {
                    if (mEditPhotoTextView.getText().toString().equalsIgnoreCase(getString(R.string.delete_photo))) {
                        // Delete profile picture after user confirmation
                        final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                        final Bundle bundle = new Bundle();
                        bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.delete_media_confirmation));
                        bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, DELETE_PHOTO_DIALOG_ID);
                        bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                        confirmationDialogFragment.setArguments(bundle);
                        showDialog(confirmationDialogFragment);
                    } else if (mEditPhotoTextView.getText().toString().equalsIgnoreCase(getString(R.string.add_photo))) {
                        // Display photo pick options to user
                        showImagePicker(getString(R.string.picker_title_choose_image));
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            case R.id.userIconImageView:
                // Display photo pick options to user
                showImagePicker(getString(R.string.picker_title_choose_image));
                break;

            default:
                break;
        }
    }

    @Override
    public void onConfirm(int dialogId) {
        if (dialogId == DELETE_PHOTO_DIALOG_ID) {
            displayProgressLoader(false);
            // Delete profile photo
            DeleteProfilePhotoLoader deleteProfilePhotoLoader = new DeleteProfilePhotoLoader(mActivity, mActiveUser.getToken(), mActiveUser.getPatientID());
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(DELETE_PROFILE_PHOTO_LOADER), deleteProfilePhotoLoader);
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == DELETE_PHOTO_DIALOG_ID) {

        }
    }

    /**
     * This method will be used to get the profile pic url of the active user.
     *
     * @param activeUser active {@link User} reference
     * @return Url of the active user
     */
    private String getProfileUrlOfLogInUserInDB(final User activeUser) {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(mActivity, DatabaseHelper.class);
            final Dao<User, Long> userDao = helper.getDao(User.class);
            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<String>() {
                @Override
                public String call() throws Exception {
                    User dbUser = userDao.queryForSameId(activeUser);

                    if (dbUser != null && !TextUtils.isEmpty(dbUser.getPhoto())) {
                        return dbUser.getPhoto();
                    }

                    return null;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return null;
    }

    public void showImagePicker(String alertTitle) {
        final CharSequence[] items = {
                getString(R.string.m_take_photo),
                getString(R.string.m_pick_photo)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(alertTitle);
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.m_take_photo))) {
                    captureImage();

                } else if (items[item].equals(getString(R.string.m_pick_photo))) {
                    pickImageFromGallery();

                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @InjectSavedState
    private Uri mImageFileUri; // file url to store image

    /*
     * Capturing Camera Image will lauch camera app requrest image capture
	 */
    private void captureImage() {
        // start the image capture Intent
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mImageFileUri = Uri.fromFile(getOutputImageFile());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageFileUri);
        startActivityForResult(intent, AxSysConstants.INTENT_ID_TAKE_PHOTO);
    }

    public void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, AxSysConstants.INTENT_ID_PICK_PHOTO);
    }

    /*
         * returns image path
         */
    private File getOutputImageFile() {

        // External sdcard location
        File mediaStorageDir = CommonUtils.createMediaFolder();

        // Create a media file name
//        File imageFile = new File(mediaStorageDir.getPath() + File.separator + AxSysConstants.MEDIA_IMAGE_PREFIX + System.currentTimeMillis() + ".jpg");
        File imageFile = new File(Environment.getExternalStorageDirectory(), AxSysConstants.MEDIA_IMAGE_PREFIX + System.currentTimeMillis() + ".jpg");

        return imageFile;
    }

    public void setCapturedImage() {
        File destination = new File(mImageFileUri.getPath());

        // navigate to details page to save media, pass file path to details fragment
        if (destination.exists() && !destination.isDirectory()) {
            uploadProfilePic(mImageFileUri);
        }
    }

    public void setGalleryImage(Intent data) {
        if (data != null && data.getData() != null) {
            Uri selectedImageUri = data.getData();

            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cursor = mActivity.getContentResolver().query(selectedImageUri, projection, null, null, null);

            String selectedImagePath = "";

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                selectedImagePath = cursor.getString(column_index);
            }

            File destination = new File(selectedImagePath);

            if (destination.exists() && !destination.isDirectory()) {
                mImageFileUri = Uri.fromFile(destination);
                uploadProfilePic(mImageFileUri);
            }
        }
    }

    private void uploadProfilePic(Uri imageFileUri) {
        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            UploadProfilePicLoader uploadProfilePicLoader = new UploadProfilePicLoader(mActivity, mActiveUser.getToken(), mActiveUser.getPatientID(), imageFileUri);
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(UPLOAD_PROFILE_PHOTO_LOADER), uploadProfilePicLoader);
        } else {
            showToast(R.string.error_no_network);
        }
    }

}
