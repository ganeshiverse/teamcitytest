package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/15/2015.
 */
public class CircleDetailsDbLoader extends DataAsyncTaskLibLoader<ArrayList<CircleDetailsDbModel>> {

    private ArrayList<CircleDetailsDbModel> circleDetailsDbModels;
    private String TAG = CircleMemberListDbLoader.class.getSimpleName();

    public CircleDetailsDbLoader(final Context context, final ArrayList<CircleDetailsDbModel> mediaItems) {
        super(context, true);
        this.circleDetailsDbModels = mediaItems;
    }

    @Override
    protected ArrayList<CircleDetailsDbModel> performLoad() throws Exception {
        if (circleDetailsDbModels == null)
            throw new SQLException("Unable to cache circle details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (circleDetailsDbModels != null && circleDetailsDbModels.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<CircleDetailsDbModel>>() {
                    @Override
                    public ArrayList<CircleDetailsDbModel> call() throws Exception {
                        for (final CircleDetailsDbModel dbModel : circleDetailsDbModels) {
                            final Dao<CircleDetailsDbModel, Long> circleDetailsDbModelLongDao = helper.getDao(CircleDetailsDbModel.class);

                            circleDetailsDbModelLongDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved circle details in DB...");
                        }

                        return circleDetailsDbModels;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return circleDetailsDbModels;
    }
}