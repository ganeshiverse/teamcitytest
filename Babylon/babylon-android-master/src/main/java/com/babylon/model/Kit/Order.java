package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Babylon on 12/05/2015.
 */
public class Order {
    @SerializedName("products")
    @Expose

    private List<ProductList> products = new ArrayList<ProductList>();

    /**
     *
     * @return
     * The products
     */
    public List<ProductList> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<ProductList> products) {
        this.products = products;
    }



}
