package com.babylon.fragment.check;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.activity.BaseActivity;
import com.babylon.controllers.AskController;
import com.babylon.controllers.AskInfoController;
import com.babylon.utils.SelectImageManager;

public class SkinFragment extends BaseCheckFragment
        implements AskInfoController.OnExistQuestionLoadListener,
        View.OnClickListener {
    private Button goToAsk;
    private TextView askActionTextView;
    private LinearLayout photoLayout;
    private Button photoButton;
    private Button photoGalleryButton;
    private BaseActivity baseActivity;

    public static SkinFragment newInstance() {
        return new SkinFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_skin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            baseActivity = (BaseActivity) activity;
        }
    }

    private void initViews(View view) {
        goToAsk = (Button) view.findViewById(R.id.go_to_ask);
        askActionTextView = (TextView) view.findViewById(R.id.ask_action_text_view);
        photoLayout = (LinearLayout) view.findViewById(R.id.layoutPhoto);
        photoButton = (Button) view.findViewById(R.id.btnTakePhoto);
        photoGalleryButton = (Button) view.findViewById(R.id.btnExistingPhoto);

        photoLayout.setVisibility(View.GONE);
        getActivity().setTitle(R.string.skin);
    }

    @Override
    public void onResume() {
        super.onResume();
        showQuestionState();
    }

    @Override
    public void onExistQuestionLoad(boolean isQuestionExist) {
        if (baseActivity != null) {
            baseActivity.setProgressBarVisible(false);
        }

        goToAsk.setVisibility(isQuestionExist ? View.VISIBLE : View.GONE);
        photoLayout.setVisibility(!isQuestionExist ? View.VISIBLE : View.GONE);

        askActionTextView.setVisibility(View.VISIBLE);
        askActionTextView.setText(getString(isQuestionExist ?
                R.string.skin_question_title : R.string.skin_ask_title));
    }

    @Override
    public void onClick(View view) {
        AskController askController = new AskController(getActivity());
        switch (view.getId()) {
            case R.id.go_to_ask:
                askController.start();
                break;
            case R.id.btnTakePhoto:
                askController.startAskActivity(SelectImageManager.CAMERA);
                break;
            case R.id.btnExistingPhoto:
                askController.startAskActivity(SelectImageManager.GALLERY);
                break;
            default:
                break;
        }
    }

    private void initListeners() {
        goToAsk.setOnClickListener(this);
        photoButton.setOnClickListener(this);
        photoGalleryButton.setOnClickListener(this);
    }

    private void showQuestionState() {
        goToAsk.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        askActionTextView.setVisibility(View.GONE);

        if (baseActivity != null) {
            baseActivity.setProgressBarVisible(true);
        }
        new AskInfoController(getActivity()).start(SkinFragment.this);
    }
}