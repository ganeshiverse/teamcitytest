package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 11/07/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_MEDIA_FOLDERS)
public class MediaFoldersModel extends DBUtils implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = MEDIA_FOLDER_PK)
    private int mediaFolderPrimaryKey;

    @DatabaseField(columnName = MEDIA_FOLDER_ID)
    private int mediaFolderId;

    @DatabaseField(columnName = MEDIA_SUB_FOLDER_ID)
    private int mediaSubFolderId;

    @DatabaseField(columnName = MEDIA_FOLDER_NAME)
    private String mediaFolderName;

    @DatabaseField(columnName = MEDIA_SUB_FOLDER_NAME)
    private String mediaSubFolderName;

    public int getMediaFolderId() {
        return mediaFolderId;
    }

    public void setMediaFolderId(int mediaFolderId) {
        this.mediaFolderId = mediaFolderId;
    }

    public int getMediaSubFolderId() {
        return mediaSubFolderId;
    }

    public void setMediaSubFolderId(int mediaSubFolderId) {
        this.mediaSubFolderId = mediaSubFolderId;
    }

    public String getMediaFolderName() {
        return mediaFolderName;
    }

    public void setMediaFolderName(String mediaFolderName) {
        this.mediaFolderName = mediaFolderName;
    }

    public String getMediaSubFolderName() {
        return mediaSubFolderName;
    }

    public void setMediaSubFolderName(String mediaSubFolderName) {
        this.mediaSubFolderName = mediaSubFolderName;
    }

    public int getMediaFolderPrimaryKey() {
        return mediaFolderPrimaryKey;
    }
}
