package com.axsystech.excelicare.framework.ui.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.axsystech.excelicare.data.responses.HelpResponse;
import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.data.responses.SiteDetailsModel;
import com.axsystech.excelicare.db.ConverterDbToResponseModel;
import com.axsystech.excelicare.db.models.ClientUrlAndUserDetailsDbModel;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.db.models.SiteDetailsDBModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment;
import com.axsystech.excelicare.loaders.GetSiteDetailsFromDBLoader;
import com.axsystech.excelicare.loaders.GetUserDBLoader;
import com.axsystech.excelicare.loaders.GetUserSiteDetailsDBLoader;
import com.axsystech.excelicare.loaders.GetUserSiteDetailsFromDBLoader;
import com.axsystech.excelicare.loaders.HelpLoadAsyncTask;
import com.axsystech.excelicare.loaders.HelpLoader;
import com.axsystech.excelicare.loaders.InvitationReceivedSearchLoader;
import com.axsystech.excelicare.loaders.ListMyCirclesLoader;
import com.axsystech.excelicare.loaders.LoginLoader;
import com.axsystech.excelicare.loaders.SaveUserDBLoader;
import com.axsystech.excelicare.loaders.UrlCardAsyncTask;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.AESHelper;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.NavigationUtils;
import com.axsystech.excelicare.util.PreferencesUtil;
import com.axsystech.excelicare.util.RegisterApp;
import com.axsystech.excelicare.util.SharedPreferenceUtil;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.AESService;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.crypto.EncryptedPrivateKeyInfo;

/**
 * Date: 06.05.14
 * Time: 11:24
 *
 * @author SomeswarReddy
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener,
        ConfirmationDialogFragment.OnConfirmListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private static final int MAX_NUMBER_ATTEMPTS_TO_LOGIN = 4;
    private static final int PRIVACY_POLICY_REQUEST_CODE = 93;

    private static final int LOGIN_CONFIRMATION_DIALOG_ID = 94;
    private static final int CONFIRM_PASSWORD_RESET_DIALOG_ID = 95;

    byte[] keyStart = "AXSYS".getBytes();

    //private static final String GET_USER_SITE_DETAILS_LOADER = "com.axsystech.excelicare.framework.ui.activities.GET_USER_SITE_DETAILS_LOADER";
    private static final String GET_USER_SITE_DETAILS_FROM_DB_LOADER = LoginActivity.class.getName() + ".GET_USER_SITE_DETAILS_FROM_DB_LOADER";
    private static final String LOGIN_LOADER = "com.axsystech.excelicare.framework.ui.activities.LOGIN_LOADER";
    private static final String SAVE_USER_IN_DB_LOADER = "com.axsystech.excelicare.framework.ui.activities.SAVE_USER_IN_DB_LOADER";
    private static final String GET_USER_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.activities.GET_USER_FROM_DB_LOADER";
    private static final String HELP_LOADER = "com.axsystech.excelicare.framework.ui.activities.LoginActivity.HELP_LOADER";

    @InjectView(R.id.emailIdEditText)
    private EditText mEmailEditText;

    @InjectView(R.id.passwordEditText)
    private EditText mPasswordEditText;

    @InjectView(R.id.siteIdSpinnerContainer)
    private LinearLayout mSiteIdSpinnerContainer;

    @InjectView(R.id.notvaliduserTextview)
    private TextView mNotValidUserTextView;

    @InjectView(R.id.infoImageView)
    private ImageView mInfoImageView;

    @InjectView(R.id.siteIdSpinner)
    private Spinner mSiteIdSpinner;

    @InjectView(R.id.rememberMeCheckBox)
    private CheckBox mRememberMeCheckBox;

    //@InjectView(R.id.forgotPasswordButton)
    private TextView mForgotPasswordButton;

    //@InjectView(R.id.loginButton)
    private TextView mLoginButton;

    @InjectView(R.id.newUserSignUpLayout)
    private LinearLayout newUserSignUpLayout;

    private TextView mNewUserTextView;
    private TextView mSignUpTextView;
    private TextView mPrivacyPolicyHintsTextView;
    private TextView mPrivactPolicyTextView;

    @InjectView(R.id.privacyPolicyLinkLayout)
    private LinearLayout privacyPolicyLinkLayout;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private User mLoginUser;

    @InjectSavedState
    private String mClientUrl;

    private String mSelectedSiteId;

    @InjectSavedState
    private boolean mRememberMe;


    @InjectSavedState
    private int mCountFailedLogin;

    private String mInputEmailID = "";

    private LoginActivity mActivity;

    String encryptedPasswordData;

    //Gcm Push Notifications Implementation
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String GCMTAG = "GCMRelated";
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    String regid;

    @Override
    protected void initActionBar() {

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mActivity = this;

        initView();
        readIntentData();
        preloadData();
        addEventListeners();
    }

    private void initView() {

        mForgotPasswordButton = (TextView) findViewById(R.id.forgotPasswordButton);
        mLoginButton = (TextView) findViewById(R.id.loginButton);
        mNewUserTextView = (TextView) findViewById(R.id.newUserTextView);
        mSignUpTextView = (TextView) findViewById(R.id.signUpTextView);
        mPrivacyPolicyHintsTextView = (TextView) findViewById(R.id.privacyPolicyHintsTextView);
        mPrivactPolicyTextView = (TextView) findViewById(R.id.privactPolicyTextView);

        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        mForgotPasswordButton.setTypeface(regular);
        mLoginButton.setTypeface(bold);
        mNewUserTextView.setTypeface(regular);
        mSignUpTextView.setTypeface(bold);
        mPrivacyPolicyHintsTextView.setTypeface(regular);
        mEmailEditText.setTypeface(regular);
        mPasswordEditText.setTypeface(regular);

        mLoginButton.setOnClickListener(this);
        mRememberMeCheckBox.setOnCheckedChangeListener(this);
        mForgotPasswordButton.setOnClickListener(this);
        newUserSignUpLayout.setOnClickListener(this);
        privacyPolicyLinkLayout.setOnClickListener(this);
        mInfoImageView.setOnClickListener(this);
        mSiteIdSpinner.setOnItemSelectedListener(this);
    }

    private void readIntentData() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }
        }
    }

    private void preloadData() {
        mClientUrl = ServerMethods.getInstance().getGlobalClientUrl();
        mSelectedSiteId = AxSysConstants.DEFAULT_SITE_ID;

        if (!TextUtils.isEmpty(mInputEmailID)) {
            mEmailEditText.setText(mInputEmailID);

            /*
             Get site and clientUrl details from Database.
              If there are multiple sites available display SiteId selector otherwise hide it.
              */
            if (!TextUtils.isEmpty(mEmailEditText.getText().toString().trim())) {
//                GetUserSiteDetailsDBLoader userSiteDetailsDBLoader = new GetUserSiteDetailsDBLoader(mActivity, mEmailEditText.getText().toString().trim());
//                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_LOADER), userSiteDetailsDBLoader);

                GetUserSiteDetailsFromDBLoader userSiteDetailsFromDBLoader = new GetUserSiteDetailsFromDBLoader(mActivity, mEmailEditText.getText().toString().trim());
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER), userSiteDetailsFromDBLoader);
            }
        }
    }

    private void addEventListeners() {
        mEmailEditText.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_NEXT) {
                            /*
                             Get site and clientUrl details from Database.
                              If there are multiple sites available display SiteId selector otherwise hide it.
                              */
                            if (!TextUtils.isEmpty(mEmailEditText.getText().toString().trim())) {
//                                GetUserSiteDetailsDBLoader userSiteDetailsDBLoader = new GetUserSiteDetailsDBLoader(mActivity, mEmailEditText.getText().toString().trim());
//                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_LOADER), userSiteDetailsDBLoader);

                                GetUserSiteDetailsFromDBLoader userSiteDetailsFromDBLoader = new GetUserSiteDetailsFromDBLoader(mActivity, mEmailEditText.getText().toString().trim());
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER), userSiteDetailsFromDBLoader);
                            }

                            mPasswordEditText.requestFocus();

                            return true;
                        }
                        return false;
                    }
                });

        mEmailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Trace.d(TAG, "got the focus");
                } else {
                    Trace.d(TAG, "lost the focus");
                    if (!TextUtils.isEmpty(mEmailEditText.getText().toString().trim())) {
//                                GetUserSiteDetailsDBLoader userSiteDetailsDBLoader = new GetUserSiteDetailsDBLoader(mActivity, mEmailEditText.getText().toString().trim());
//                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_LOADER), userSiteDetailsDBLoader);

                        GetUserSiteDetailsFromDBLoader userSiteDetailsFromDBLoader = new GetUserSiteDetailsFromDBLoader(mActivity, mEmailEditText.getText().toString().trim());
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER), userSiteDetailsFromDBLoader);
                    }
                }
            }
        });

    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                if (validateUserInputs()) {
                    onLoginClick();
                }
                break;
            case R.id.forgotPasswordButton:
                if (TextUtils.isEmpty(mEmailEditText.getText().toString().trim())) {
                    showToast(R.string.input_email_cannot_empty);
                    mEmailEditText.requestFocus();
                } else if (!CommonUtils.isValidEmail(mEmailEditText.getText().toString().trim())) {
                    showToast(R.string.input_valid_email);
                    mEmailEditText.requestFocus();
                } else {
                    final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                    final Bundle bundle = new Bundle();
                    bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.confirm_password_reset));
                    bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, CONFIRM_PASSWORD_RESET_DIALOG_ID);
                    bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                    confirmationDialogFragment.setArguments(bundle);
                    showDialog(confirmationDialogFragment);
                }
                break;
            case R.id.newUserSignUpLayout:
//                if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
//                        CommonUtils.shouldDisplayPrivacyPolicy(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
//                    // Navigate to PRIVACY POLICY screen
//                    Intent signUpIntent = new Intent(this, PrivacyPolicyActivity.class);
//                    signUpIntent.putExtra(AxSysConstants.EXTRA_NAVIGATE_TO, NavigationUtils.SIGNUP.toString());
//                    signUpIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, "");
//                    startActivity(signUpIntent);
//                } else {
                // Navigate user to SIGNUP screen  - As per new Registration flow
                Intent intent = new Intent(this, AppWelcomeActivity.class);
                //intent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mEmailEditText.getText().toString().trim());
                startActivity(intent);
//                }
                break;
            case R.id.privacyPolicyLinkLayout:
                Intent privacyPolicyIntent = new Intent(this, LoginPricavyPolicy.class);
                privacyPolicyIntent.putExtra(AxSysConstants.EXTRA_JUST_DISPLAY_PRIVACY_POLICY, true);
                startActivityForResult(privacyPolicyIntent, PRIVACY_POLICY_REQUEST_CODE);
                break;
            case R.id.infoImageView:
                if (CommonUtils.hasInternet()) {
                    final HelpLoader helpLoader = new HelpLoader(mActivity, AxSysConstants.LOGIN_MODULE_ID,
                            mActiveUser != null ? mActiveUser.getToken() : "");
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(HELP_LOADER), helpLoader);
                } else {
                    showToast(R.string.error_no_network);
                }
                break;
            default:
                break;
        }
    }

    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(mEmailEditText.getText().toString().trim()) ||
                !CommonUtils.isValidEmail(mEmailEditText.getText().toString().trim())) {
            showToast(R.string.input_valid_email);
            mEmailEditText.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(mPasswordEditText.getText().toString().trim())) {
            showToast(R.string.input_password);
            mPasswordEditText.requestFocus();
            return false;
        }

        return true;
    }

    public void onLoginClick() {
        CommonUtils.hideSoftKeyboard(this);
        displayProgressLoader(false);

        /*//byte[] password = getStringTextView(mPasswordEditText).getBytes();
        try {
            encryptedPasswordData = AESHelper.encrypt("AXSYS", getStringTextView(mPasswordEditText));
            System.out.println("LoginEncryptDecrypt" + encryptedPasswordData);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        if (CommonUtils.hasInternet()) {
            // validate user credentials with server
            final LoginLoader loginLoader = new LoginLoader(this, CommonUtils.getDeviceId(getApplicationContext()),
                    getStringTextView(mEmailEditText), getStringTextView(mPasswordEditText));
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LOGIN_LOADER), loginLoader);
        } else {
            /*
             check for the user credentials in DB, and if available login user by validating with DB credentials,
             1. If the credentials were not matched with DB ask prompt user to check connectivity
             2. If the credentials were matched with DB then take user to Dashboard screen
              */
            final GetUserDBLoader getUserDbBLoader = new GetUserDBLoader(this, getStringTextView(mEmailEditText), getStringTextView(mPasswordEditText));
            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_FROM_DB_LOADER), getUserDbBLoader);
        }
    }

    @Override
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean checked) {
        mRememberMe = checked;
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);

        if (id == getLoaderHelper().getLoaderId(LOGIN_LOADER)) {
            mCountFailedLogin++;
            if (mCountFailedLogin == MAX_NUMBER_ATTEMPTS_TO_LOGIN) {
//                mLoginButton.setEnabled(false);
//                mPasswordEditText.setText("");
                showToast(R.string.message_please_use_forgot_password);
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_USER_IN_DB_LOADER)) {
            if (exception != null) {
                showToast(exception.getMessage());
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_USER_FROM_DB_LOADER)) {
            if (exception != null) {
                showToast(exception.getMessage());
            } else {
                // show toast to check for connectivity
                showToast(R.string.error_invalid_credentials_db);
            }
        } else if (id == getLoaderHelper().getLoaderId(HELP_LOADER)) {
            // show toast to check for connectivity
            showToast(getString(R.string.error_loading_help_info));
        } else if (id == getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER)) {
            showToast(getString(R.string.error_fetching_sites));
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(LOGIN_LOADER)) {

            final LoginResponse authResponse = (LoginResponse) result;

            if (authResponse != null && authResponse.getLoginDataObject() != null && authResponse.getLoginDataObject().getLoginUser() != null &&
                    authResponse.getLoginDataObject().getLoginUser().getUserDataObject() != null && authResponse.getLoginDataObject().getLoginUser().getUserDataObject().token != null) {
                String token = authResponse.getLoginDataObject().getLoginUser().getUserDataObject().token;
                SharedPreferenceUtil.saveStringInSP(mActivity, AxSysConstants.LOGIN_TOKEN, token);
            }


            if (TextUtils.isEmpty(mInputEmailID)) {
                mInputEmailID = mEmailEditText.getText().toString().trim();
            }
            mLoginUser = ConverterResponseToDbModel.getUser(authResponse, mInputEmailID, mPasswordEditText.getText().toString().trim());

            // Cache Login, Active user details in Global Object
            if (mLoginUser != null) {
                GlobalDataModel.getInstance().setLoginUser(mLoginUser);
                GlobalDataModel.getInstance().setActiveUser(mLoginUser);
            }

            // Display 'Login success' message
            if (!TextUtils.isEmpty(authResponse.getMessage())) {
                showToast(authResponse.getMessage());
            }

            //Gcm Register Device service

           /* if (checkPlayServices()) {
                gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                regid = getRegistrationId(getApplicationContext());

                if (regid.isEmpty()) {
                    Log.i(GCMTAG, "Logged For GCM");
                    new RegisterApp(getApplicationContext(), gcm, getAppVersion(getApplicationContext())).execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Device already Registered", Toast.LENGTH_SHORT).show();
                }
            } else {
                Log.i(GCMTAG, "No valid Google Play Services APK found.");
            }*/


            // cache logged-in user details in DB and navigate to Dashbaord
            userLoggedNavigateToDashBoard();

        } else if (id == getLoaderHelper().getLoaderId(SAVE_USER_IN_DB_LOADER)) {
            if (result != null && result instanceof User) {
                User savedUserObj = (User) result;
                Trace.d(TAG, "savedUserObj:" + savedUserObj);

                // once user details saved in DB then navigate to MainContentActivity
                Intent intent = new Intent(this, MainContentActivity.class);
                startActivity(intent);
                finish();
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_USER_FROM_DB_LOADER)) {
            // Validate input user credentials with retrieved user details and login user accordingly
            if (result != null && result instanceof List) {
                ArrayList<User> userArrayList = (ArrayList<User>) result;

                if (userArrayList != null && userArrayList.size() > 0) {
                    boolean isUserExistsInDB = false;

                    for (User userObj : userArrayList) {
                        if (!TextUtils.isEmpty(userObj.getEmail()) && !TextUtils.isEmpty(userObj.getPassword())) {

                            if (userObj.getEmail().trim().equals(mEmailEditText.getText().toString().trim())) {
                                isUserExistsInDB = true;
                            }

                            if (isUserExistsInDB && userObj.getPassword().trim().equals(mPasswordEditText.getText().toString().trim())) {
                                // user credentials are matched from DB, navigate to MainContentActivity
                                // once user details saved in DB then navigate to MainContentActivity

                                // need to prepare User object in offline mode
                                mLoginUser = userObj;

                                // Cache Login, Active user details in Global Object
                                if (mLoginUser != null) {
                                    GlobalDataModel.getInstance().setLoginUser(mLoginUser);
                                    GlobalDataModel.getInstance().setActiveUser(mLoginUser);
                                }

                                Intent intent = new Intent(this, MainContentActivity.class);
                                startActivity(intent);
                                finish();

                                break;
                            }
                        }
                    }

                    // if user exists in DB and credentials were not matched
                    if (isUserExistsInDB) {
                        showToast(R.string.error_invalid_credentials_db);
                    } else {
                        // if no user exists with the given username
                        showToast(R.string.error_first_login);
                    }

                } else {
                    // if no user exists with the given username
                    showToast(R.string.error_first_login);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(HELP_LOADER)) {
            if (result != null && result instanceof HelpResponse) {
                final HelpResponse helpResponse = (HelpResponse) result;

                if (helpResponse.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            CommonUtils.displayWebViewInDialog(mActivity, helpResponse.getData(), getString(R.string.m_help));
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_FROM_DB_LOADER)) {
            if (result != null && result instanceof List) {
                ArrayList<ClientUrlAndUserDetailsDbModel> clientUrlAndUserDetailsDbModelAL = (ArrayList<ClientUrlAndUserDetailsDbModel>) result;

                if (clientUrlAndUserDetailsDbModelAL != null && clientUrlAndUserDetailsDbModelAL.size() > 0) {
                    // set Site List data to spinner
                    ArrayAdapter<ClientUrlAndUserDetailsDbModel> siteListAdapter =
                            new ArrayAdapter<ClientUrlAndUserDetailsDbModel>(this, R.layout.login_spinner_item, clientUrlAndUserDetailsDbModelAL);
                    siteListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    mSiteIdSpinner.setAdapter(siteListAdapter);
                    mClientUrl = ((ClientUrlAndUserDetailsDbModel) mSiteIdSpinner.getAdapter().getItem(mSiteIdSpinner.getSelectedItemPosition())).getClientUrl();
                    mSelectedSiteId = ((ClientUrlAndUserDetailsDbModel) mSiteIdSpinner.getAdapter().getItem(mSiteIdSpinner.getSelectedItemPosition())).getSiteId();
                    // This is the default Client Url
                    ServerMethods.getInstance().setGlobalClientUrl(mClientUrl);

                    // Hide site id selector if there was only one in the list
                    if (clientUrlAndUserDetailsDbModelAL.size() == 1) {
                        mSiteIdSpinnerContainer.setVisibility(View.GONE);

                        // If Site Selector is not displayed to user, pre-select the single site client url and save for Login authentication
                        mClientUrl = ((ClientUrlAndUserDetailsDbModel) mSiteIdSpinner.getAdapter().getItem(0)).getClientUrl();
                        mSelectedSiteId = ((ClientUrlAndUserDetailsDbModel) mSiteIdSpinner.getAdapter().getItem(0)).getSiteId();
                        ServerMethods.getInstance().setGlobalClientUrl(mClientUrl);
                    } else {
                        mSiteIdSpinnerContainer.setVisibility(View.VISIBLE);
                    }
                } else {

                    // Negative case,,, if user cleared the data in device settings we will run into this negative case.
                    // I mean, we will cache the SiteId and clientUrl details once user authenticated OTP in change password screen.
                    // If user cleared the data from settings and navigated to login screen, we cannot have single site details in DB.

                    mClientUrl = ServerMethods.getInstance().getGlobalClientUrl();
                    mSelectedSiteId = AxSysConstants.DEFAULT_SITE_ID;
                  /*  mNotValidUserTextView.setVisibility(View.VISIBLE);
                    mSiteIdSpinnerContainer.setVisibility(View.GONE);
                    mPasswordEditText.setVisibility(View.GONE);*/

                    //Alert change for login screen

                    final ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
                    final Bundle bundle = new Bundle();
                    bundle.putString(ConfirmationDialogFragment.MESSAGE_KEY, getString(R.string.login_confirmation_message));
                    bundle.putInt(ConfirmationDialogFragment.DIALOG_ID_KEY, LOGIN_CONFIRMATION_DIALOG_ID);
                    bundle.putBoolean(ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY, true);
                    confirmationDialogFragment.setArguments(bundle);
                    showDialog(confirmationDialogFragment);
                }
            }
        }
         /*else if(id == getLoaderHelper().getLoaderId(GET_USER_SITE_DETAILS_LOADER)) {
            if(result != null && result instanceof List) {
                ArrayList<ClientUrlDbModel> clientUrlDbModelList = (ArrayList<ClientUrlDbModel>) result;

                if(clientUrlDbModelList != null && clientUrlDbModelList.size() > 0) {
                    // set Site List data to spinner
                    ArrayAdapter<ClientUrlDbModel> siteListAdapter =
                            new ArrayAdapter<ClientUrlDbModel>(this, android.R.layout.simple_spinner_item, clientUrlDbModelList);
                    siteListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    mSiteIdSpinner.setAdapter(siteListAdapter);
                    mClientUrl = ((ClientUrlDbModel) mSiteIdSpinner.getAdapter().getItem(mSiteIdSpinner.getSelectedItemPosition())).getClientURL();
                    // This is the default Client Url
                    ServerMethods.getInstance().setGlobalClientUrl(mClientUrl);

                    // Hide site id selector if there was only one in the list
                    if (clientUrlDbModelList.size() == 1) {
                        mSiteIdSpinnerContainer.setVisibility(View.GONE);

                        // If Site Selector is not displayed to user, pre-select the single site client url and save for Login authentication
                        mClientUrl = ((ClientUrlDbModel) mSiteIdSpinner.getAdapter().getItem(0)).getClientURL();
                        ServerMethods.getInstance().setGlobalClientUrl(mClientUrl);
                    } else {
                        mSiteIdSpinnerContainer.setVisibility(View.VISIBLE);
                    }
                } else {

                     // Negative case,,, if user cleared the data in device settings we will run into this negative case.
                     // I mean, we will cache the SiteId and clientUrl details once user authenticated OTP in change password screen.
                     // If user cleared the data from settings and navigated to login screen, we cannot have single site details in DB.

                    mClientUrl = ServerMethods.getInstance().getGlobalClientUrl();
                }
            }
        }*/
    }

    private void userLoggedNavigateToDashBoard() {
        if (mRememberMe) {
            PreferencesUtil.rememberMe(getApplication(), mLoginUser.getEcUserID());
        } else {
            PreferencesUtil.forgotMe(getApplication());
        }

        // Cache logged-in user details in DB
        final SaveUserDBLoader saveUserDBLoader = new SaveUserDBLoader(this, mLoginUser);
        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_USER_IN_DB_LOADER), saveUserDBLoader);
    }

    @Override
    public void onConfirm(int dialogId) {
//        mCountFailedLogin = 0;
//        mLoginButton.setEnabled(true);
        if (dialogId == LOGIN_CONFIRMATION_DIALOG_ID) {
            Intent intent = new Intent(this, AppWelcomeActivity.class);
            intent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mEmailEditText.getText().toString().trim());
            startActivity(intent);
        } else if (dialogId == CONFIRM_PASSWORD_RESET_DIALOG_ID) {
            Intent intent = new Intent(this, ForgotPasswordActivity.class);
            intent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, mSelectedSiteId);
            intent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mEmailEditText.getText().toString().trim());
            startActivity(intent);
        }
    }

    @Override
    public void onCancel(int dialogId) {
        if (dialogId == LOGIN_CONFIRMATION_DIALOG_ID) {

        } else if (dialogId == CONFIRM_PASSWORD_RESET_DIALOG_ID) {

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (id == R.id.siteIdSpinner) {
            mClientUrl = ((ClientUrlAndUserDetailsDbModel) mSiteIdSpinner.getAdapter().getItem(mSiteIdSpinner.getSelectedItemPosition())).getClientUrl();
            mSelectedSiteId = ((ClientUrlAndUserDetailsDbModel) mSiteIdSpinner.getAdapter().getItem(mSiteIdSpinner.getSelectedItemPosition())).getSiteId();

            // Save the selected site clientUrl in SP and use it for authentication and other
            ServerMethods.getInstance().setGlobalClientUrl(mClientUrl);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    //CkeckPlay Services availability
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(GCMTAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(GCMTAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(getApplicationContext());
        if (registeredVersion != currentVersion) {
            Log.i(GCMTAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(LoginActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
