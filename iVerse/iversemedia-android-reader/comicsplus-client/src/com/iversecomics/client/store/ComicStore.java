package com.iversecomics.client.store;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.http.NameValuePair;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.iversecomics.client.net.HTTPClient;
import com.iversecomics.client.net.JsonRest;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ComicStoreDatabaseHelper;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.FeaturedSlotTable;
import com.iversecomics.client.store.db.GenresTable;
import com.iversecomics.client.store.db.GroupsTable;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.db.OrderingTable;
import com.iversecomics.client.store.db.PromotionTable;
import com.iversecomics.client.store.db.PublishersTable;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.util.text.StringUtils;

public class ComicStore {
    public class DatabaseApi {
        /**
         * Clear out old search results from the DB.
         */
        public void clearOldSearchResults() {
            String where = OrderingTable.LISTID + " = ?";
            String whereArgs[] = {
                ListID.SEARCH
            };
            contentResolver.delete(OrderingTable.CONTENT_URI, where, whereArgs);
        }

        /**
         * Get the Comic based on the {@link Comic#getComicBundleName()}
         *
         * @param comicBundleName
         *            the comic bundle name
         * @return the {@link Comic} if found, null otherwise.
         */
        public Comic getComicByBundleName(String comicBundleName) {
            Cursor c = null;
            try {
                String sortOrder = ComicsTable.NAME + " ASC";
                String where = ComicsTable.TABLE + "." + ComicsTable.COMIC_BUNDLE_NAME + " = ?";
                String whereArgs[] = new String[] {
                    comicBundleName
                };
                c = contentResolver.query(ComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
                if (c.moveToFirst() && !c.isAfterLast()) {
                    return ComicsTable.fromCursor(c);
                }
                return null;
            } finally {
                DBUtil.close(c);
            }
        }

        /**
         * Get the Comic based on server provided Id {@link Comic#getComicId()}
         *
         * @param comicId
         *            the comic id
         * @return the {@link Comic} if found, null otherwise
         */
        public Comic getComicById(String comicId) {
            if (comicId == null) {
                return null;
            }
            Cursor c = null;
            try {
                String sortOrder = ComicsTable.NAME + " ASC";
                String where = ComicsTable.TABLE + "." + ComicsTable.COMICID + " = ?";
                String whereArgs[] = new String[] {
                    comicId
                };
                c = contentResolver.query(ComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
                if (c.moveToFirst() && !c.isAfterLast()) {
                    return ComicsTable.fromCursor(c);
                }
                return null;
            } finally {
                DBUtil.close(c);
            }
        }

        /**
         * Get Comics by Genre.
         * <p>
         *
         * @param genreId
         *            the genre server Id {@link Genre#getGenreId()}
         * @return the Cursor to the list of comics.
         */
        public Cursor getCursorComicsByGenre(String genreId) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getComicsByGenre(genreId), -1);
        }

        /**
         * Get the Comics by Publisher.
         *
         * @param publisherId
         *            the publisher server Id {@link Publisher#getPublisherId()}
         * @return the Cursor to the list of comics.
         */
        public Cursor getCursorComicsByPublisher(String publisherId) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getComicsByPublisher(publisherId), -1);
        }

        /**
         * Get the Comics by Series Bundle Name.
         *
         * @param seriesId
         *            the publisher server Id {@link Comic#getSeriesBundleName()}
         * @return the Cursor to the list of comics.
         */
        public Cursor getCursorComicsBySeries(String seriesId) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getComicsBySeries(seriesId), -1);
        }

        public Cursor getCursorComicsFeatured(int limit) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.FEATURED_COMICS, limit, true, false);
        }

        public Cursor getCursorComicsFeaturedOnDemand(String categoryId, int limit) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getFeaturedComicsByCategoryId(categoryId), limit, true, true);
        }

        public Cursor getCursorComicsNew() {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.NEW_RELEASES, -1);
        }

        public Cursor getCursorComicsByReleaseDate(String releaseDate) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getComicsByReleaseDate(releaseDate), -1);
        }

        private Cursor getCursorComicsOrdered(String projection[], String listId, int limitCount) {
            return getCursorComicsOrdered(projection, listId, limitCount, false, false);
        }

        /**
         *
         * @param projection The columns to include in the result.
         * @param listId The list id in the OrderingTable used to join with the ComicTable.
         * @param limitCount The maximum
         * @param filterSubscription Should the results be filtered on the subscription / onDemand column?
         * @param subscription If filterSubscription, this is the value that it should be.
         * @return
         */
        private Cursor getCursorComicsOrdered(String projection[], String listId, int limitCount, boolean filterSubscription, boolean subscription) {
            String sortOrder = OrderingTable.TABLE + "." + OrderingTable.INDEX + " ASC";
            if (limitCount > 0) {
                sortOrder += " LIMIT " + limitCount;
            }
            StringBuilder where = new StringBuilder();
            where.append("( ( ").append(OrderingTable.TABLE).append('.').append(OrderingTable.LISTID);
            where.append(" = ? ) AND ( ");
            where.append(ComicsTable.TABLE).append('.').append(ComicsTable.COMICID);
            where.append(" = ");
            where.append(OrderingTable.TABLE).append('.').append(OrderingTable.COMICID);
            if (filterSubscription) {
                where.append(" )  AND ( ");
                where.append(ComicsTable.TABLE).append('.').append(ComicsTable.SUBSCRIPTION);
                if (subscription) {
                    where.append(" = 1");
                } else {
                    where.append(" = 0");
                }
            }
            where.append(" ) ) ");
            String whereArgs[] = {
                listId,
            };
            DBUtil.debugQuery(ComicsTable.CONTENT_URI, ComicsTable.TABLE + ", " + OrderingTable.TABLE, projection, where.toString(), whereArgs, sortOrder);
            return contentResolver.query(ComicsTable.CONTENT_URI, projection, where.toString(), whereArgs, sortOrder);
        }

        public Cursor getCursorComicsSearch() {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.SEARCH, -1);
        }

        public Cursor getCursorComicsTopFree() {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.TOP_FREE, -1);
        }

        public Cursor getCursorComicsTopPaid() {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.TOP_PAID, -1);
        }

        public Cursor getCursorComicsByPromotion(int promotionId) {
            return getCursorComicsByPromotion(String.valueOf(promotionId));
        }

        public Cursor getCursorComicsByPromotion(String promotionId) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getComicsByPromotion(promotionId), -1);
        }

        public Cursor getCursorComicsByGroupId(String groupId) {
            String projection[] = ComicsTable.PROJECTION_BRIEF;
            return getCursorComicsOrdered(projection, ListID.getComicsByGroup(groupId), -1);
        }

        public Cursor getCursorGenresAll() {
            String sortOrder = GenresTable.NAME + " ASC";
            return contentResolver.query(GenresTable.CONTENT_URI, null, null, null, sortOrder);
        }

        public Cursor getCursorGroupsAll() {
            String sortOrder = GroupsTable.NAME + " ASC";
            return contentResolver.query(GroupsTable.CONTENT_URI, null, null, null, sortOrder);
        }

        public Group getGroupByGroupId(String groupId) {
            String sortOrder = GroupsTable.NAME + " ASC";
            String where = GroupsTable.TABLE + "." + GroupsTable.GROUPID + " = ?";
            String whereArgs[] = new String[] {
            		groupId
            };

            Cursor c = null;
            try {
            	c = contentResolver.query(GroupsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
            	if (c.moveToFirst() && !c.isAfterLast()) {
            		return GroupsTable.fromCursor(c);
            	}
            	return null;
            } finally {
            	DBUtil.close(c);
            }

        }

        public Cursor getCursorGroupsByPublisher(String publisherId) {
            String sortOrder = GroupsTable.NAME + " ASC";
            String where = GroupsTable.PUBLISHERID + " = ?";
            String whereArgs[] = new String[] {
            	publisherId
            };
            return contentResolver.query(GroupsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
        }

        public Cursor getCursorGroupsByParentGroupID(String parentGroupId) {
            String sortOrder = GroupsTable.NAME + " ASC";
            String where = GroupsTable.PARENTID + " = ?";
            String whereArgs[] = new String[] {
            	parentGroupId
            };
            return contentResolver.query(GroupsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
        }

        public Cursor getCursorPublishersAll() {
            String sortOrder = PublishersTable.NAME + " COLLATE NOCASE ASC";
            return contentResolver.query(PublishersTable.CONTENT_URI, null, null, null, sortOrder);
        }

        public Cursor getCursorFeaturedSlots() {
        	return contentResolver.query(FeaturedSlotTable.CONTENT_URI, null, "subscription = 0", null, null);
        }

        public Cursor getCursorOnDemandFeaturedSlots() {
            return contentResolver.query(FeaturedSlotTable.CONTENT_URI, null, "subscription = 1", null, null);
        }

        public Cursor getCursorPublishersFeatured() {
            String sortOrder = PublishersTable.NAME + " ASC";
            String where = PublishersTable.FEATURED + " = ?";
            String whereArgs[] = new String[] {
                Boolean.TRUE.toString()
            };
            return contentResolver.query(PublishersTable.CONTENT_URI, null, where, whereArgs, sortOrder);
        }

        public Cursor getCursorPromotion()
        {
        	return contentResolver.query(PromotionTable.CONTENT_URI, null, null, null, null);
        }

        public Promotion getPromotion(String promotionId) {
            Cursor c = null;
            try {
                String where = PromotionTable.PROMOTIONID + " = ?";
                String whereArgs[] = new String[] {
                        promotionId
                };
                c = contentResolver.query(PromotionTable.CONTENT_URI, null, where, whereArgs, null);
                if (c.moveToFirst() && !c.isAfterLast()) {
                    return PromotionTable.fromCursor(c);
                }
                return null;
            } finally {
                DBUtil.close(c);
            }
        }

        public Publisher getPublisher(String publisherId) {
            Cursor c = null;
            try {
                String sortOrder = PublishersTable.NAME + " ASC";
                String where = PublishersTable.PUBLISHERID + " = ?";
                String whereArgs[] = new String[] {
                    publisherId
                };
                c = contentResolver.query(PublishersTable.CONTENT_URI, null, where, whereArgs, sortOrder);
                if (c.moveToFirst() && !c.isAfterLast()) {
                    return PublishersTable.fromCursor(c);
                }
                return null;
            } finally {
                DBUtil.close(c);
            }
        }


    }

    public class ServerApi {
        private JsonRest            rest;
        private ConnectivityManager connectivityMgr;
        private Freshness           freshness;
        public static final String DONT_THROW_ERROR = "dont_throw";
        public static final String SERVER_SERSPONSE = "on_server_response_error";

        public ServerApi(HTTPClient http) {
            this.rest = new JsonRest(http);
            this.connectivityMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            this.freshness = new Freshness(context);
        }

        public void assertNetworkAvailable() throws ComicStoreUnavailableException {
            if (connectivityMgr == null) {
                return;
            }
            NetworkInfo activeInfo = connectivityMgr.getActiveNetworkInfo();
            if (activeInfo == null) {
                throw new ComicStoreUnavailableException("No network connection(s) available");
            }
            if (!activeInfo.isConnected()) {
                throw new ComicStoreUnavailableException("Not connected to network");
            }
        }

        private void assertSuccess(JSONObject ret) throws ComicStoreException {
            if (ret == null) {
                throw new ComicStoreResponseFailureException("No response");
            }

            String status = ret.optString("status");
            if (TextUtils.isEmpty(status)) {
                throw new ComicStoreResponseFailureException("Response status blank");
            }
            if (!"ok".equals(status)) {
                String reason = ret.optString("reason");
                if (!TextUtils.isEmpty(reason)) {
                    LOG.warn(reason);
                }
               // throw new ComicStoreResponseFailureException("Response '" + status + "'");
            }
        }

        /**
         * Get a specific comic by the comicBundleName
         *
         * @param comicBundleName
         *            the comicBundleName
         * @return
         * @throws ComicStoreException
         */
        public JSONObject getComic(String comicBundleName) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsViewUri(comicBundleName));
        }

        public JSONObject getComicsByGenre(String genreId, int numberToSkip, int limit) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsByCategoryUri(genreId, numberToSkip, limit));
        }

        public JSONObject getComicsByGroup(String groupId) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsByGroupUri(groupId));
        }

        public JSONObject getComicsByPublisher(String publisherId) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsBySupplierUri(publisherId));
        }

        public JSONObject getComicsBySeries(String seriesId) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsBySeriesUri(seriesId));
        }

        public JSONObject getComicsSearch(String query) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsBySearchUri(query));
        }

        public JSONObject getFeaturedComicsList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsByFeaturedUri());
        }

        public JSONObject getFeaturedComicsListByCategoryString(String categoryID) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsByFeaturedCategoryUri(categoryID));
        }

        public JSONObject getGenresList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getCategoryListUri());
        }

        public JSONObject getNewReleasesList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsNewReleasesUri());
        }

        public JSONObject getPublisher(String publisherId) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getSupplierViewUri(publisherId));
        }

        public JSONObject getPublishersList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getSupplierListUri());
        }

        public JSONObject getProductReleaseDatesUri() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductReleaseDatesUri());
        }

        public JSONObject getGroupListUri() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getGroupListUri());
        }

        public JSONObject getGroupsByGroupUri(final String groupId) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getGroupsByGroupUri(groupId));
        }

        public JSONObject getGroupsBySupplierUri(final String supplierId) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getGroupsBySupplierUri(supplierId));
        }

        public JSONObject getProductsByReleaseDateUri(final String date) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsByReleaseDateUri(date));
        }

        public JSONObject getServerConfig(JSONObject payload) throws ComicStoreException {
            URI apiuri = ServerConfig.getDefault().getUriMain(); // should always work!
            return postJsonObject(apiuri, payload);
        }

        public JSONObject getTopFreeList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsTopFreeUri());
        }

        public JSONObject getTopPaidList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsTopPaidUri());
        }

        public JSONObject getPrivacyPolicy() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getPrivacyPolicyUri());
        }

        public JSONObject getStoreLocatorByZip(String zip) throws ComicStoreException {
            return getJsonObject(ServerConfig.getDefault().getComicShopByZIP(zip));
        }

        public JSONObject getStoreLocatorByLocation(double latitude, double longitude) throws ComicStoreException {
            return getJsonObject(ServerConfig.getDefault().getComicShopByLocation(longitude, latitude));
        }

        public JSONObject getComicsByPromotion(String id) throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getProductsByPromotionUri(id));
        }

        public JSONObject getFeaturedSlots() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getFeaturedSlotsUri());
        }

        public JSONObject getOnDemandSlots() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getOnDemandUri());
        }

        public JSONObject getPromotionList() throws ComicStoreException {
            return requestJsonObject(ServerConfig.getDefault().getPromotionListUri());
        }



        private String postJsonObjectForString(final URI apiuri, final List<NameValuePair> payload) throws ComicStoreException {
            try {
                if (apiuri == null) {
                    throw new IllegalArgumentException("apiurl cannot be null");
                }
                assertNetworkAvailable();
                LOG.debug("POST String from %s", apiuri.toASCIIString());

                final String ret = rest.postForm(apiuri, payload);

                return ret;
            } catch (IOException e) {
                throw new ComicStoreException("Failed to communicate with server", e);
            } catch (Throwable t) {
                throw new ComicStoreException("Server failure", t);
            }
        }

        private JSONObject postJsonObject(URI apiuri, JSONObject payload) throws ComicStoreException {
            try {
                if (apiuri == null) {
                    throw new IllegalArgumentException("apiurl cannot be null");
                }
                assertNetworkAvailable();
                LOG.debug("POST JSON Object from %s", apiuri.toASCIIString());
                LOG.debug("PAYLOAD: %s", payload.toString());

                //allow some response error statuses to be returned normally and not as thrown errors
                final String status = payload.isNull(SERVER_SERSPONSE) ?
                		"" : (payload.remove(SERVER_SERSPONSE)).toString();

                final JSONObject ret = rest.post(apiuri, payload);

                LOG.debug("RESPONSE: %s", ret.toString());

                if(!status.equals(DONT_THROW_ERROR))
                	assertSuccess(ret);

                return ret;
            } catch (IOException e) {
                throw new ComicStoreException("Failed to communicate with server", e);
            } catch (JSONException e) {
                throw new ComicStoreException("Server response failure", e);
            } catch (Throwable t) {
                throw new ComicStoreException("Server failure", t);
            }
        }

        //Raw version that doesn't look for a status field
        private JSONObject getJsonObject(URI apiuri) throws ComicStoreException {
            try {
                if (apiuri == null) {
                    throw new IllegalArgumentException("apiurl cannot be null");
                }
                assertNetworkAvailable();
                LOG.debug("Requesting JSON Object from %s", apiuri.toASCIIString());
                JSONObject ret = rest.get(apiuri);
                return ret;
            } catch (IOException e) {
                throw new ComicStoreException("Failed to communicate with server", e);
            } catch (JSONException e) {
                throw new ComicStoreException("Server response failure", e);
            } catch (Throwable t) {
                throw new ComicStoreException("Server failure", t);
            }
        }

        //Used for our server APIs which return a status field
        private JSONObject requestJsonObject(URI apiuri) throws ComicStoreException {
        	try {
                JSONObject ret = getJsonObject(apiuri);
                assertSuccess(ret);
                return ret;
        	} catch (Throwable t) {
        		throw new ComicStoreException("Server failure", t);
        	}
        }

        public void updateFreshness(String listId, long validForMillis) {
            this.freshness.setExpiresOn(listId, System.currentTimeMillis() + validForMillis);
        }

        public JSONObject verifyPurchase(JSONObject payload) throws ComicStoreException {
            return postJsonObject(ServerConfig.getDefault().getVerifyPurchaseUri(), payload);
        }

        public JSONObject verifySubscription(JSONObject payload) throws ComicStoreException {
            return postJsonObject(ServerConfig.getDefault().getPurchaseSubscriptionUri(), payload);
        }

        public JSONObject submitRating(JSONObject payload) throws ComicStoreException {
            return postJsonObject(ServerConfig.getDefault().getSubmitRatingUri(), payload);
        }

        public JSONObject submitUserAccount(JSONObject payload) throws ComicStoreException {
            return postJsonObject(ServerConfig.getDefault().getUserAccountsUri(), payload);
        }

        public JSONObject submitOnDemandComicRequest(JSONObject payload) throws ComicStoreException {
            return postJsonObject(ServerConfig.getDefault().getOnDemandUri(), payload);
        }
    }

    private static final Logger      LOG = LoggerFactory.getLogger(ComicStore.class);

    private Context                  context;
    private ContentResolver          contentResolver;
    private ServerApi                serverApi;
    private DatabaseApi              databaseApi;
    private ComicStoreDatabaseHelper dbHelper;
    private SQLiteDatabase           db;

    public ComicStore(Context context, HTTPClient http) {
        this.context = context;
        this.contentResolver = context.getContentResolver();
        this.serverApi = new ServerApi(http);
        this.dbHelper = ComicStoreDatabaseHelper.getInstance(context);
        this.db = dbHelper.getWritableDatabase();
        this.databaseApi = new DatabaseApi();
    }

    public ContentResolver getContentResolver() {
        return contentResolver;
    }

    public DatabaseApi getDatabaseApi() {
        return databaseApi;
    }

    public ServerApi getServerApi() {
        return serverApi;
    }

    // TODO: move to ServerApi and rename as DatabaseUpdateVisitor
    public ComicStoreDBUpdater newDBUpdater() {
        return new ComicStoreDBUpdater(db, contentResolver);
    }
}
