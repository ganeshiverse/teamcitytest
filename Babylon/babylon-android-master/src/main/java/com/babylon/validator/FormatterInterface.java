package com.babylon.validator;

public interface FormatterInterface {
    public String format(String source);
}
