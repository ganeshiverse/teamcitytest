package com.axsystech.excelicare.framework.ui.fragments.appointments;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AppointmentDetailsResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.AppointmentDetailsLoader;
import com.axsystech.excelicare.loaders.CancelBookingLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.Map;

/**
 * Created by vvydesai on 8/31/2015.
 */
public class CancelAppointmentFragment extends BaseFragment implements View.OnClickListener {

    private static final String GET_APPOINTMENT_DETAILS = "com.axsystech.excelicare.framework.ui.fragments.appointments.CancelAppointmentFragment.GET_APPOINTMENT_DETAILS";
    private static final String CANCEL_BOOKING = "com.axsystech.excelicare.framework.ui.fragments.appointments.CancelAppointmentFragment.CANCEL_BOOKING";

    private String TAG = CancelAppointmentFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.confirmedTextView)
    private TextView confirmedTextView;

    @InjectView(R.id.appointmentaWithtTextView)
    private TextView appointmentaWithtTextView;

    @InjectView(R.id.specialityTextView)
    private TextView specialityTextView;

    @InjectView(R.id.dateTimeTextView)
    private TextView dateTimeTextView;

    @InjectView(R.id.consultationModeTextView)
    private TextView consultationModeTextView;

    @InjectView(R.id.feeTextView)
    private TextView feeTextView;

    @InjectView(R.id.clinicTextView)
    private TextView clinicTextView;

    @InjectView(R.id.addressTextView)
    private TextView addressTextView;

    @InjectView(R.id.emailTextView)
    private TextView emailTextView;

    @InjectView(R.id.contactTextView)
    private TextView contactTextView;

    @InjectView(R.id.visitUsTextView)
    private TextView visitUsTextView;

    @InjectView(R.id.cancelBookingButton)
    private Button cancelBookingButton;

    boolean bookAppointments = true;

   /* @InjectView(R.id.bookingStatusTextView)
    private TextView bookingStatusTextView;*/

    @InjectSavedState
    private String appointmentId;

    @InjectSavedState
    private String appointmentType;

    private AppointmentDetailsResponse.AppointmentDetailsData appointmentDetailsData;
    private CancelCallBackListener cancelCallBackListener;

    public void setCancelCallBackListener(CancelCallBackListener cancelCallBackListener) {
        this.cancelCallBackListener = cancelCallBackListener;
    }

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(),mActionBarTitle));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.appointment_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mLayoutInflater = LayoutInflater.from(mActivity);

        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Book Appointments")) {
                    if (m.getValue() == 1) {
                        bookAppointments = true;
                    } else if (m.getValue() == 0) {
                        bookAppointments = false;
                    }
                }
            }
        }

        readIntentArgs();
        preLoadData();
        addEventListeners();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_APPOINTMENT_ID)) {
                appointmentId = bundle.getString(AxSysConstants.EXTRA_APPOINTMENT_ID);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_APPOINTMENT_TYPE)) {
                appointmentType = bundle.getString(AxSysConstants.EXTRA_APPOINTMENT_TYPE);
            }
        }
    }

    private void preLoadData() {
        cancelBookingButton.setText(getString(R.string.text_cancel_booking));
        confirmedTextView.setText(appointmentType);

        // get Selected appointment details from server
        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            AppointmentDetailsLoader appointmentDetailsLoader = new AppointmentDetailsLoader(mActivity, mActiveUser.getToken(), appointmentId, "1");
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_APPOINTMENT_DETAILS), appointmentDetailsLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    private void addEventListeners() {
        cancelBookingButton.setOnClickListener(this);

        if (bookAppointments) {
            cancelBookingButton.setVisibility(View.VISIBLE);
        } else {
            cancelBookingButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_APPOINTMENT_DETAILS)) {
            if (result != null && result instanceof AppointmentDetailsResponse) {
                AppointmentDetailsResponse appointmentDetailsResponse = (AppointmentDetailsResponse) result;

                updateResultsToUI(appointmentDetailsResponse);
            }
        } else if (id == getLoaderHelper().getLoaderId(CANCEL_BOOKING)) {
            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponse = (BaseResponse) result;
                if (!TextUtils.isEmpty(baseResponse.getMessageCaps())) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(baseResponse.getMessageCaps());
                            mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });
                } else if (!TextUtils.isEmpty(baseResponse.getMessage())) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(baseResponse.getMessage());
                            mActivity.onBackPressed();
//                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    });
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cancelCallBackListener.onCancelSuccess();
                    }
                },500);
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(GET_APPOINTMENT_DETAILS)) {
            showToast(getString(R.string.error_loading_appointment_details));
        } else if (id == getLoaderHelper().getLoaderId(CANCEL_BOOKING)) {
            showToast(getString(R.string.error_cancelling_appointment));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_print_option, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelBookingButton:
                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);
                    CancelBookingLoader cancelBookingLoader = new CancelBookingLoader(mActivity, mActiveUser.getToken(),
                            (appointmentDetailsData != null) ? (!TextUtils.isEmpty(appointmentDetailsData.getApptId()) ? appointmentDetailsData.getApptId() : "") : "",
                            (appointmentDetailsData != null) ? (!TextUtils.isEmpty(appointmentDetailsData.getTicketNumber()) ? appointmentDetailsData.getTicketNumber() : "0") : "0");
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CANCEL_BOOKING), cancelBookingLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            default:
                break;
        }
    }

    private void updateResultsToUI(AppointmentDetailsResponse appointmentDetailsResponse) {
        if (appointmentDetailsResponse.getAppointmentDetailsDataAL() != null &&
                appointmentDetailsResponse.getAppointmentDetailsDataAL().size() > 0) {

            appointmentDetailsData = appointmentDetailsResponse.getAppointmentDetailsDataAL().get(0);
            if (appointmentDetailsData != null) {
                appointmentaWithtTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getClinicianName()) ? appointmentDetailsData.getClinicianName() : "");

                specialityTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getSpeciality()) ? appointmentDetailsData.getSpeciality() : "");

                dateTimeTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getStartdatetime()) ? appointmentDetailsData.getStartdatetime() : "");

                consultationModeTextView.setText("");

                feeTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getFee()) ? appointmentDetailsData.getFee() : "");

                clinicTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getClinicName()) ? appointmentDetailsData.getClinicName() : "");

                addressTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getAddress()) ? appointmentDetailsData.getAddress() : "");

                emailTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getEmail()) ? appointmentDetailsData.getEmail() : "");

                contactTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getPhone()) ? appointmentDetailsData.getPhone() : "");

                visitUsTextView.setText(!TextUtils.isEmpty(appointmentDetailsData.getURL()) ? appointmentDetailsData.getURL() : "");

                //bookingStatusTextView.setVisibility(View.GONE);
            }
        }
    }


    public interface CancelCallBackListener {
        public void onCancelSuccess();
    }
}
