package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.PhysicalActivity;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;

public class AddActivityListPost extends AbsRequestPost<PhysicalActivity.PhysicalActivityList,
        ResponseList<PhysicalActivity>> {


    @Override
    protected String getUrl() {
        return Urls.get(Urls.ACTIVITY_LIST);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }


}
