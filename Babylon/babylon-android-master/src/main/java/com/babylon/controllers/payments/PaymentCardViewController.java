package com.babylon.controllers.payments;

import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.R;
import com.babylon.enums.PaymentCardType;
import com.babylon.model.payments.PaymentCard;

import javax.annotation.Nullable;

public class PaymentCardViewController {


    public
    @Nullable
    String removeWhitespacesFromCreditCard(String creditCardNumber) {
        String result = null;

        if (!TextUtils.isEmpty(creditCardNumber)) {
            result = creditCardNumber.trim().replace(" ", "");
        }

        return result;
    }

    public PaymentCard getNewCreditCard() {
        final PaymentCard creditCard = new PaymentCard();

        creditCard.setCardType(App.getInstance().getString(R.string.list_item_add_card));

        return creditCard;
    }

    public String getCardViewNumber(PaymentCard paymentCard) {
        String number = paymentCard.getMaskedNumber();
        String subNumber = number.substring(number.length() - 8, number.length());
        String subNumberWithSpace = new StringBuilder(subNumber).insert(4, " ").toString();

        return subNumberWithSpace;
    }

    public
    @DrawableRes
    int findImageId(String creditType) {
        final String creditCardTypeLowerCase = TextUtils.isEmpty(creditType) ? "" : creditType.toLowerCase();
        switch (creditCardTypeLowerCase) {
            case PaymentCardType.VISA_CARD_TYPE:
                return R.drawable.ic_card_visa;
            case PaymentCardType.MASTER_CARD_TYPE:
                return R.drawable.ic_card_mastercard;
            case PaymentCardType.AMERICA_EXPRESS_CARD_TYPE:
                return R.drawable.ic_card_amex;
            default:
                return R.drawable.ic_new_card;
        }
    }
}
