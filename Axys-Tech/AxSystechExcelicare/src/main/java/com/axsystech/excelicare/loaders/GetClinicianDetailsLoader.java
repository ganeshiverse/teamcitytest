package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class GetClinicianDetailsLoader extends DataAsyncTaskLibLoader<ClinicianDetailsResponse> {

    private String cliniciaId;
    private String token;

    public GetClinicianDetailsLoader(Context context, String cliniciaId, String token) {
        super(context);
        this.cliniciaId = cliniciaId;
        this.token = token;
    }

    @Override
    protected ClinicianDetailsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getClinicianDetails(cliniciaId, token);
    }
}
