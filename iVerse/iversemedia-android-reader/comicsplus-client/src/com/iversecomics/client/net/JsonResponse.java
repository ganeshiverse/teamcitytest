package com.iversecomics.client.net;

import com.iversecomics.json.JSONObject;

public interface JsonResponse {
    void setData(JSONObject json);
}
