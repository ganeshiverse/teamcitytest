package com.babylon.test.activity;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;
import android.text.TextUtils;

import com.babylon.R;
import com.babylon.activity.SignInActivity;
import com.babylon.model.Patient;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
public class SignInActivityTest extends ActivityInstrumentationTestCase2<SignInActivity> {


    public SignInActivityTest() {
        super(SignInActivity.class);
    }

    private final String email = "patient@babylon.com";
    private final String password = "password";

    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
        SyncUtils.signOut();
        onView(withId(R.id.emailEdt)).perform(clearText(), scrollTo(), typeText(email));
        onView(withId(R.id.passwordEdt)).perform(scrollTo(), typeText(password));
        onView(withId(R.id.signInBtn)).perform(scrollTo(), click());
    }

    public void test_shouldBeLoggedIn() {
        assertTrue(SyncUtils.isLoggedIn());
    }

    public void test_wsseShouldBeStorred() {
        Wsse wsse = SyncUtils.getWsse();
        assertTrue(wsse != null);
        assertFalse(TextUtils.isEmpty(wsse.getToken()));
        assertFalse(TextUtils.isEmpty(wsse.getId()));
    }

    public void test_accountShouldBeStored() {
        Patient patient = SyncUtils.getPatientWithCreds();
        assertTrue(patient != null);
        assertTrue(patient.getEmail().equals(email));
        assertTrue(patient.getPassword().equals(password));
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
