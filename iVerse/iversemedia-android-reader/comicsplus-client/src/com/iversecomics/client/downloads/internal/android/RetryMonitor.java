package com.iversecomics.client.downloads.internal.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.iversecomics.client.downloads.DownloaderConstants;
import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class RetryMonitor implements DownloadMonitor {
    private static final Logger LOG    = LoggerFactory.getLogger(RetryMonitor.class);
    private static final long   SECOND = 1000;
    private Context             context;
    private AlarmManager        alarms;
    private long                wakeUp;

    public RetryMonitor(Context context) {
        this.context = context;
        this.wakeUp = SECOND * 10;
        this.alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public PendingIntent createPendingWakeup() {
        Intent i = new Intent();
        i.setAction(DownloaderConstants.ACTION_DOWNLOAD_WAKEUP);
        return PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_ONE_SHOT);
    }

    @Override
    public void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        /* ignore */
    }

    @Override
    public void downloadFailure(Uri uri, DownloadException failure) {
    	/*
        if ((failure instanceof DownloadFailedException) || failure.getStatus().isPermanentFailure()) {
            // No retry possible.
            LOG.warn("No retry possible: %s", failure.getStatus());
            return;
        }
        */

        LOG.debug("%s, Issuing Retry/Wakeup in %d ms: %s", failure.getClass().getName(), wakeUp, failure.getMessage());

        PendingIntent pendingIntent = createPendingWakeup();

        alarms.cancel(pendingIntent);
        alarms.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + wakeUp, pendingIntent);
    }

    @Override
    public void downloadProgress(Uri uri, long currentByte) {
        /* ignore */
    }

    @Override
    public void downloadState(Uri uri, DownloadStatus state, String message) {
        /* ignore */
    }
}
