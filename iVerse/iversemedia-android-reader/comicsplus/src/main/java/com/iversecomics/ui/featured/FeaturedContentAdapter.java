package com.iversecomics.ui.featured;

import com.iversecomics.otto.event.FeaturedComics;
import com.iversecomics.otto.event.FeaturedSlots;
import com.squareup.otto.Subscribe;

/**
 */
public class FeaturedContentAdapter extends AbstractFeaturedContentAdapter {

    @Subscribe
    public void updateFeaturedComics(FeaturedComics fc){
       setFeaturedComics(fc.getComics());
    }

    @Subscribe
    public void updateFeaturedSlots(FeaturedSlots fc){
        setSlots(fc.getSlots());
    }

}
