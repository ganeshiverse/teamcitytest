package com.babylon.utils.activity;

import android.content.ContentResolver;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.L;

public class ActivitySaver {


    private static final String TAG =ActivitySaver.class.getSimpleName() ;

    public static boolean rewrite(ContentResolver contentResolver, PhysicalActivity walkingActivity) {
        boolean wasUpdated = true;

        PhysicalActivity prevActivity = DatabaseOperationUtils.getThisDatePhysicalActivity
                (walkingActivity.getStartDate(), walkingActivity.getSource(), contentResolver);

        if (prevActivity != null) {
            wasUpdated = !prevActivity.getSteps().equals(walkingActivity.getSteps());

            if(wasUpdated) {
                setSyncWithServerType(walkingActivity, prevActivity);
                walkingActivity.setId(prevActivity.getId());
                DatabaseOperationUtils.getInstance().updatePhysicalActivity(walkingActivity, contentResolver);
            }
        } else {
            contentResolver.insert(PhysicalActivitySchema.CONTENT_URI,
                    walkingActivity.toContentValues());
        }

        return wasUpdated;
    }

    public static void update(ContentResolver contentResolver, PhysicalActivity walkingActivity) {
        PhysicalActivity prevActivity = DatabaseOperationUtils.getInstance().getPhysicalActivityFromDay
                (walkingActivity.getStartDate(), walkingActivity.getSource(), contentResolver);

        if (prevActivity != null) {
            mergeActivities(walkingActivity, prevActivity);
            DatabaseOperationUtils.getInstance().updatePhysicalActivity(walkingActivity, contentResolver);
        } else {
            contentResolver.insert(PhysicalActivitySchema.CONTENT_URI,
                    walkingActivity.toContentValues());
        }
    }

    private static void mergeActivities(PhysicalActivity walkingActivity, PhysicalActivity prevActivity) {
        if (prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADDED
                || prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_PATCH) {
            walkingActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_PATCH);
        } else if (prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADD) {
            walkingActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
        }

        walkingActivity.setCalories(walkingActivity.getCalories() + prevActivity.getCalories());
        walkingActivity.setSteps(walkingActivity.getSteps() + prevActivity.getSteps());
        walkingActivity.setId(prevActivity.getId());
    }

    private static void setSyncWithServerType(PhysicalActivity walkingActivity, PhysicalActivity prevActivity) {
        if (prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADDED
                || prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_PATCH) {
            walkingActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_PATCH);
        } else if (prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADD) {
            walkingActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
        }
    }
}
