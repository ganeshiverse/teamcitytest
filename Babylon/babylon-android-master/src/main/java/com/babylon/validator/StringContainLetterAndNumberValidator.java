package com.babylon.validator;

public class StringContainLetterAndNumberValidator extends BaseValidator {

    public StringContainLetterAndNumberValidator(BaseValidator nextValidator, String errorMessage) {
        super(nextValidator, errorMessage);
    }

    @Override
    public String validate(String string) {
        boolean containLetter = false;
        boolean containNumber = false;
        char[] charArray = string.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (Character.isLetter(charArray[i])) {
                containLetter = true;
            } else if (Character.isDigit(charArray[i])) {
                containNumber = true;
            }
            if (containLetter && containNumber) {
                break;
            }
        }
        return (containLetter && containNumber) ? null : errorMessage;
    }
}
