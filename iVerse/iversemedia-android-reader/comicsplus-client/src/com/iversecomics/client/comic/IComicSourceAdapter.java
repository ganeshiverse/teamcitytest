package com.iversecomics.client.comic;

import android.graphics.Bitmap;

import java.net.URI;

public interface IComicSourceAdapter {

    public int getImageCount();

    public URI getImageURI(int index);

    public void setComicSourceListener(IComicSourceListener listener);

    public ComicMode getComicMode();

    public void setComicMode(ComicMode mode);

    public Bitmap loadComicPage(int index);
}
