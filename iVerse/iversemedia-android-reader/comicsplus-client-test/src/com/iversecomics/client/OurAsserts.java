package com.iversecomics.client;

import junit.framework.Assert;

public final class OurAsserts {
    public static void assertAtLeast(String msg, int expectedMinimum, int actualCount) {
        Assert.assertTrue(msg + " actual [" + actualCount + "] >= expected [" + expectedMinimum + "]",
                actualCount >= expectedMinimum);
    }

    public static void assertGreaterThan(String msg, int expectedMinimum, int actualCount) {
        Assert.assertTrue(msg + " - actual [" + actualCount + "] > expected [" + expectedMinimum + "]",
                actualCount > expectedMinimum);
    }
}
