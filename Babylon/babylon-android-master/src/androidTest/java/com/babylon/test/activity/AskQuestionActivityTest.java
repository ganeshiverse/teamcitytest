package com.babylon.test.activity;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import com.babylon.R;
import com.babylon.activity.AskQuestionActivity;

import org.junit.After;
import org.junit.Before;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@LargeTest
public class AskQuestionActivityTest
        extends ActivityInstrumentationTestCase2<AskQuestionActivity> {


    public AskQuestionActivityTest() {
        super(AskQuestionActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    public void testMessageSend() {
        getActivity();
        String askMessage = "Doctor, I have eaten 200 burgers, please safe my life";

        onView(withId(R.id.edit_text_message)).perform(clearText(), typeText(askMessage),
                closeSoftKeyboard());
        onView(withId(R.id.btn_send_question)).perform(click());
        onView(withId(R.id.text_view_message)).check(matches(withText((askMessage))));
    }

    @After
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
