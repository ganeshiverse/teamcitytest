package com.babylon.model;

public class Specialist {
    private Integer id;
    private String name;
    private String specialism;
    private Boolean isSelected;
    // non json field
    private Boolean isCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialism() {
        return specialism;
    }

    public void setSpecialism(String specialism) {
        this.specialism = specialism;
    }

    public Boolean isSelected() {
        return isSelected;
    }

    public void setSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Boolean isCategory() {
        return isCategory;
    }

    public void setCategory(Boolean isCategory) {
        this.isCategory = isCategory;
    }
}