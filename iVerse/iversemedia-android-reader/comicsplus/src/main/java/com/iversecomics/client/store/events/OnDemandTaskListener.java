package com.iversecomics.client.store.events;

import com.iversecomics.client.store.ComicStoreTask;

public interface OnDemandTaskListener {
    /**
     * Notification that a specific ComicStoreTask is being requested by some sub-process.
     * <p/>
     * Usually seen as part of an adapter that detects the need for another subtask to be run.
     *
     * @param task the task that wants to be run.
     */
    void onDemandTaskRequest(ComicStoreTask task);
}
