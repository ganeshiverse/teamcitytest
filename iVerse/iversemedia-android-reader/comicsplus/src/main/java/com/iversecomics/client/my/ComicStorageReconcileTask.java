package com.iversecomics.client.my;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;

import com.iversecomics.archie.android.R;
import com.iversecomics.bundle.ComicBundle;
import com.iversecomics.bundle.ComicContent;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.bundle.Ownership;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.Preferences;
import com.iversecomics.client.Storage;
import com.iversecomics.client.bitmap.BitmapUtil;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.my.db.MyComicsTable;
import com.iversecomics.client.refresh.Task;
import com.iversecomics.client.store.tasks.ServerConfigTask;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.ComicReconciledEvent;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Future;

public class ComicStorageReconcileTask extends Task {
    //private static final float  COVER_WIDTH_DIP  = 94.0f;
    //private static final float  COVER_HEIGHT_DIP = 141.0f;


    private final float COVER_WIDTH_DIP;
    private final float COVER_HEIGHT_DIP;

    public static final String ACTION_COMIC_RECONCILED = "com.iversecomics.database.actions.RECONCILED";

    private static final Logger LOG = LoggerFactory.getLogger(ComicStorageReconcileTask.class);
    private Context activity;
    private Ownership ownership;
    private MyComicsModel model;
    private int widthPixels;
    private int heightPixels;
    private boolean refreshAll = false;
    Future<?> serverConfigFuture;

    public ComicStorageReconcileTask(Context activity, Ownership ownership) {
        super();
        this.activity = activity;
        this.model = new MyComicsModel(activity);
        this.ownership = ownership;

        COVER_WIDTH_DIP = activity.getResources().getDimension(R.dimen.mycomics_cell_cover_width);
        COVER_HEIGHT_DIP = activity.getResources().getDimension(R.dimen.mycomics_cell_cover_height);

        float scale = activity.getResources().getDisplayMetrics().density;

        // Technically this is not necessary, but in practice it creates a much higher quality image
        scale = scale * 2;

        widthPixels = (int) (COVER_WIDTH_DIP * scale + 0.5f);
        heightPixels = (int) (COVER_HEIGHT_DIP * scale + 0.5f);
    }

    public void assertSdCardPresent() throws SDStateException {
        // TODO Auto-generated method stub

    }

    @Override
    public void execTask() {
        Long comicsize;
        MyComic comic;

        try {
            Storage storage = new Storage();

            for (File file : storage.listAssets()) {
                comicsize = getComicSizeFromDB(file.getName());

                comic = parseComic(file);

                if (comic.getStatus() == ComicMetadata.STATUS_BAD) {
                    continue;
                } else if (comicsize == null) {
                    // Not in database. parse it
                    model.insert(comic);
                    if(comic.getOnDemand()){
                        final IverseApplication app = IverseApplication.getApplication();
                        final Task task = new ServerConfigTask(app, app.getComicStore());
                        serverConfigFuture = app.getTaskPool().submit(task);
                    }
                    //Util.exportDB();
                } else if (refreshAll || hasComicChanged(file, comicsize)) {
                    // Changed file. reparse.
                    model.updateByAssetName(file.getName(), comic);
                    //Util.exportDB();
                }

                OttoBusProvider.getInstance().post(new ComicReconciledEvent(comic.getComicId()));
//                final Intent intent = new Intent(ACTION_COMIC_RECONCILED);
//                intent.putExtra("name", comic.getComicId());
//                LocalBroadcastManager.getInstance(IverseApplication.getApplication().getApplicationContext()).sendBroadcast(intent);

            }
        } catch (IOException e) {
            LOG.warn(e, "Unable to process Storage Directory");
        }
    }

    //This is an alternate mechanism for bitmap scaling we might look at if we have problems with the other.
    //It comes from http://stackoverflow.com/questions/4231817/quality-problems-when-resizing-an-image-at-runtime
    /*
    private void scaleBitmap(String sourceBitmapFile, String destBitmapFile, float widthPixels, int heightPixels)
    {
    	// Get the source image's dimensions
    	BitmapFactory.Options options = new BitmapFactory.Options();
    	options.inJustDecodeBounds = true;
    	try
    	{
        	BitmapFactory.decodeFile(sourceBitmapFile, options);

        	int srcWidth = options.outWidth;
        	int srcHeight = options.outHeight;

        	// Only scale if the source is big enough. This code is just trying to fit a image into a certain width.
        	if(widthPixels > srcWidth)
        		widthPixels = srcWidth;


        	// Calculate the correct inSampleSize/scale value. This helps reduce memory use. It should be a power of 2
        	// from: http://stackoverflow.com/questions/477572/android-strange-out-of-memory-issue/823966#823966
        	int inSampleSize = 1;
        	while(srcWidth / 2 > widthPixels){
        	    srcWidth /= 2;
        	    srcHeight /= 2;
        	    inSampleSize *= 2;
        	}

        	float desiredScale = (float) widthPixels / srcWidth;

        	// Decode with inSampleSize
        	options.inJustDecodeBounds = false;
        	options.inDither = false;
        	options.inSampleSize = inSampleSize;
        	options.inScaled = false;
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        	Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(sourceBitmapFile, options);

        	// Resize
        	Matrix matrix = new Matrix();
        	matrix.postScale(desiredScale, desiredScale);
        	Bitmap scaledBitmap = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
        	sampledSrcBitmap = null;

        	// Save
        	FileOutputStream out = new FileOutputStream(destBitmapFile);
        	scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        	scaledBitmap = null;    	
    	}
    	catch (FileNotFoundException e)
    	{
    		LOG.debug("Failed to resize bitmap %s",	sourceBitmapFile);
    	}
    }
    */

    private Uri extractCover(ComicContent content, File coverImage) {
        // if (content.getStatus() != ComicMetadata.STATUS_OK) {
        // LOG.warn("Comic is not STATUS_OK, not extracting Cover from " + content.getFile().getName());
        // return null;
        // }

        Uri uri = Uri.fromFile(coverImage);

        // Perform fast save to storage dir
        try {
            content.open();
            IOUtil.copy(content.getCoverStream(), coverImage);
            LOG.info(String.format("Saved cover image %s (%,d bytes)", coverImage, coverImage.length()));
        } catch (IOException e) {
            LOG.warn(e, "Unable to save fullsize cover: %s", coverImage.getName());
            return uri;
        } finally {
            IOUtil.close(content);
        }

        if (!coverImage.exists()) {
            return uri;
        }

        try {
            // Load fullsize bitmap
            Bitmap fullsizeBitmap = BitmapUtil.load(coverImage);
            if (fullsizeBitmap == null) {
                LOG.warn("No fullsize bitmap!?");
                if (coverImage.exists()) {
                    // Its a bad image file
                    if (!coverImage.delete()) {
                        LOG.warn("Unable to delete bad image: " + coverImage);
                    }
                }
                return null;
            }

            // Resize to appropriate size
            Bitmap coverBitmap = Bitmap.createScaledBitmap(fullsizeBitmap, widthPixels, heightPixels, true);
            // Save
            BitmapUtil.save(coverBitmap, coverImage);
            LOG.info(String.format("Saved scaled cover image %s (%,d bytes)", coverImage, coverImage.length()));

        } catch (IOException e) {
            LOG.warn(e, "IO Error during extractCover - %s", coverImage);
        } finally {
            IOUtil.close(content);
        }

        return uri;
    }


    private Long getComicSizeFromDB(String assetFilename) {
        Cursor c = null;
        try {
            c = model.getCursorMyComicByAssetFilename(assetFilename);
            Long size = null;
            if ((c != null) && c.moveToFirst()) {
                size = c.getLong(c.getColumnIndexOrThrow(MyComicsTable.ASSET_FILESIZE));
            }
            return size;
        } finally {
            DBUtil.close(c);
        }
    }

    private boolean hasComicChanged(File file, Long comicsize) {
        if (comicsize == null) {
            return true; // Assume changed
        }

        return (file.length() != comicsize);
    }

    public boolean isRefreshAll() {
        return refreshAll;
    }

    private MyComic parseComic(File file) {
        ComicBundle bundle = new ComicBundle();
        bundle.setOwnership(ownership);

        MyComic comic = new MyComic();
        comic.setAssetFilename(file.getName());
        comic.setAssetFilesize(file.length());

        try {
            bundle.parse(file);
            ComicMetadata metadata = bundle.getMetadata();

            comic.setComicId(metadata.getId());
            comic.setName(metadata.getName());
            comic.setAssetFilename(metadata.getFilename());
            comic.setAssetFilesize(metadata.getFilesize());
            comic.setStatus(metadata.getStatus());
            comic.setSeriesId(metadata.getSeriesId());
            comic.setPublisherId(metadata.getPublisherId());
            comic.setDescription(metadata.getDescription());
            comic.setSynopsis(metadata.getSynopsis());
            comic.setType(metadata.getProductType());

            comic.setGenres(metadata.getCategories());
            // VerifyPurchaseTask.isOnDemand ==true - means, it is a OnDemand(subscribed product) type purchase
            // else it is a free/paid type purchase
            // add SP condition
            SharedPreferences preferences = IverseApplication.getApplication().getSharedPreferences(Preferences.NAME, 0);
            comic.setOnDemand(preferences.getBoolean(metadata.getId(),false));


            File coversDir = activity.getDir("covers", Context.MODE_WORLD_READABLE);
            File coverImage = new File(coversDir, metadata.getId() + ".png");

            if (comic.getStatus() == ComicMetadata.STATUS_OK) {
                if (coverImage.exists()) {
                    //Cover image already exists, no need to create it
                    comic.setUriCover(Uri.fromFile(coverImage));
                } else {
                    //Cover image does not exist, we must extract and create it
                    Uri uriCover = extractCover(bundle.getContent(), coverImage);
                    comic.setUriCover(uriCover);
                }
            }

            return comic;
            //TODO - this won't catch a BundleReadException that's thrown from parse
            //BundleReadException is the more specific exp though and should replace Exception
        } catch (Exception e) {
            LOG.warn("Bad Comic - %s: %s", file.getName(), e);
            comic.setStatus(ComicMetadata.STATUS_BAD);
        }

        return comic;
    }

    public void setRefreshAll(boolean refreshAll) {
        this.refreshAll = refreshAll;
    }
}
