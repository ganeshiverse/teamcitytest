package com.babylon.model;

import com.google.gson.annotations.SerializedName;

public class StressQuestion extends BaseModel {
    @SerializedName("name")
    private String question;
    private Integer id;
    private String isChecked;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getIsChecked() {
        return getBoolean(isChecked);
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public int hashCode() {
        return id;
    }

    public boolean equals(Object obj) {
        if ( this == obj ) return true;

        if ( !(obj instanceof StressQuestion) ) return false;

        StressQuestion that = (StressQuestion)obj;

        return  that.id.equals(id);
    }
}
