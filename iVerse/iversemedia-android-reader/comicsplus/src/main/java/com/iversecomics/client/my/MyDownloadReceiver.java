package com.iversecomics.client.my;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;

import com.iversecomics.archie.android.R;
import com.iversecomics.bundle.ComicBundle;
import com.iversecomics.bundle.parse.BundleAccess;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.downloads.DownloaderConstants;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.event.DownloadCompleteEvent;
import com.squareup.otto.Subscribe;

import org.acra.ErrorReporter;

import java.io.File;
import java.io.IOException;

public class MyDownloadReceiver {
    private static class DownloadVerificationTask extends AsyncTask<DownloadCompleteEvent, Void, Object> {
        private static final Logger LOG = LoggerFactory.getLogger(DownloadVerificationTask.class);
        private DownloadCompleteEvent event;
        private String failureReason;

        protected Object doInBackground(DownloadCompleteEvent... events) {
            //doInBackground's parameter list is a var args, but the completion broadcast will
            //only ever send a single intent at a time - Log and error though if this changes.
            if (events.length != 1)
                LOG.error("DownloadVerificationTask got more that one comic complete");

            //for (Intent intent : intents) {
            try {
                event = events[0];
                if (verifyDownload(event)) {
                    IverseApplication.getApplication().comicStorageReconcileTask();
                    return true;
                }
            } catch (Throwable t) {
                ErrorReporter.getInstance().handleSilentException(t);
            }
            //}
            return false;
        }

        protected void onPostExecute(final Object success) {
            final Context context = IverseApplication.getApplication().getApplicationContext();

            //Clean up the database
            Intent dbCleanupIntent = new Intent();
            dbCleanupIntent.setAction(DownloaderConstants.ACTION_CLEANUP);
            IverseApplication.getApplication().getApplicationContext().sendBroadcast(dbCleanupIntent);
            //LocalBroadcastManager.getInstance(IverseApplication.getApplication().getApplicationContext()).sendBroadcast(dbCleanupIntent);

            if (!((Boolean) success)) {
                //safest way to message the user is to display a notification and
                //let them interact with it when they're ready
                final String ns = Context.NOTIFICATION_SERVICE;

                //this is optional - on touching the notification go back to the purchases list
                final Intent notificationIntent = null;

                // TODO: joliver: PreviousPurchasesActivity is dead, this will need to be migrated to the comicsplus project,
                // change PreviousPurchasesActivity to MainActivity
//                final Intent notificationIntent = new Intent(context, PreviousPurchasesActivity.class);
//                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);

                final Resources res = context.getResources();
                final Notification notification =
                        new Notification(R.drawable.icon,
                                res.getString(R.string.notification_download_failed_title) + " " + this.failureReason,
                                System.currentTimeMillis());
                if(context !=null && notificationIntent !=null) {
                    final PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    //final String comicTitle = intent.getStringExtra("filename") == null ? "" : intent.getStringExtra("filename");
                    final String comicTitle = res.getString(R.string.notification_download_failed_title);
                    notification.setLatestEventInfo(context, comicTitle, res.getString(R.string.notification_download_failed_msg) + "\n" + this.failureReason, contentIntent);
                    mNotificationManager.notify(0, notification);
                }
            }
        }

        private boolean verifyDownload(DownloadCompleteEvent event) {
            LOG.debug("Download Complete: " + event.toString());

            if (!"SUCCESS".equals(event.getStatus())) {
                StringBuilder err = new StringBuilder();
                err.append("Download Failure [").append(event.getStatus()).append("] ");
                err.append("\nReason: ").append(event.getReason());
                err.append("\nFilename: ").append(event.getFilename());
                ErrorReporter.getInstance().handleSilentException(new IOException(err.toString()));
                return false;
            }

            /*
             * At this point, this file is typically found on the storage directory under a temporary filename that is
             * used for downloading, (so as to not make the file visible to MyComics, yet)
             */
            File downloadFile = new File(event.getFilename());


            ComicBundle cb = new ComicBundle();
            try {
                cb.setContent(downloadFile);
                cb.checksumIsValid();
            } catch (Exception e) {
                //Log the exception
                LOG.error("The file %s failed to download successfully (failed checksum checks).  %s", event.getFilename(), e.getMessage());

                //Set the failure reason
                this.failureReason = e.getMessage();

                //Delete the bad file
                downloadFile.delete();
                return false;
            }


            File destFile = new File(event.getFilename().replaceFirst("\\.dl$", ""));

            try {

                //Lets add the email ownership to the file
                BundleAccess downloadFileBundle = new BundleAccess(downloadFile, "r");
                BundleAccess destFileBundle = new BundleAccess(destFile, "rw");

                destFileBundle.copyHeadersAndAddEmail(downloadFileBundle);
                destFileBundle.close();

                downloadFile.delete();


            } catch (Exception e) {
                //Log the exception
                LOG.error("Failed to copy and sign the file from %s to %s.  %s", downloadFile.toString(), destFile.toString(), e.getMessage());

                //Set the failure reason
                this.failureReason = e.getMessage();

                //Delete the bad file
                downloadFile.delete();
                destFile.delete();
                return false;
            }


            ComicBundle comicBundle = new ComicBundle();
            try {
                comicBundle.setOwnership(IverseApplication.getApplication().getOwnership());
                comicBundle.setContent(destFile);
                comicBundle.setHash();
                comicBundle.parse(destFile);
                comicBundle.signBundle();
            } catch (Exception e) {
                //Log the exception
                LOG.error("The file failed to download successfully.  %s", e.getMessage());

                //Set the failure reason
                this.failureReason = e.getMessage();

                //Delete the bad file
                destFile.delete();
                return false;
            }

            return true;
        }
    }

    @Subscribe
    public void onDownloadComplete(DownloadCompleteEvent event) {
        new DownloadVerificationTask().execute(event);
    }
}
