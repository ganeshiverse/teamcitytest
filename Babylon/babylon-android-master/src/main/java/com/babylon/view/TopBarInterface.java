package com.babylon.view;

import android.view.View;
import com.babylon.R;

/**
 * Shows action items on top bar
 * For correct work include view_topbar.xml
 * in your activity of fragment layout
 */
public interface TopBarInterface {

    int GREEN_THEME = R.color.green_ask_text;
    int BLUE_THEME = R.color.blue_top_bar;
    int ORANGE_THEME = R.color.orange_bright;
    int PURPLE_THEME = R.color.purple_top_bar;
    int PINK_THEME = R.color.pink_top_bar;

    void setTitle(String title);
    void setBackButtonVisible(boolean isVisible);
    public void setProgressBarVisible(boolean isVisible);
    public void setRightImageButtonVisible(boolean isVisible);
    public void setSecondRightImageButtonVisible(boolean isVisible);
    void setRightImageButton(View.OnClickListener buttonRunnable, int iconResourceId);
    void setSecondRightImageButton(View.OnClickListener buttonRunnable, int iconResourceId);
    void setRightButton(final View.OnClickListener onClickListener, int stringResourceId);
    /**
     * @return color theme constant from @see com.babylon.view.TopBarInterface
     */
    abstract int getColorTheme();
}
