package com.babylon.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.babylon.App;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.triage.TriageExportGet;
import com.babylon.api.request.triage.TriageUpdateGet;
import com.babylon.sync.TriageSync;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;

public class TriageSyncService extends IntentService {

    public static final String TAG = TriageSyncService.class.getSimpleName();

    public TriageSyncService() {
        super(TAG);
    }

    public static void startFirstSync() {
        boolean isExportDone = PreferencesManager.getInstance().getBoolean(
                TriageSync.KEY_EXPORT_DONE, false);
        boolean isExportInProgress = PreferencesManager.getInstance().getBoolean(
                TriageSync.KEY_EXPORT_IN_PROGRESS, false);
        if (!isExportDone && !isExportInProgress) {
            startService();
        }
    }

    public static void startSync() {
        boolean isExportInProgress = PreferencesManager.getInstance().getBoolean(
                TriageSync.KEY_EXPORT_IN_PROGRESS, false);
        if (!isExportInProgress) {
            startService();
        }
    }

    private static void startService() {
        Intent intent = new Intent(App.getInstance(), TriageSyncService.class);
        App.getInstance().startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (App.getInstance().isConnection()) {
            syncTriageDatabase();
        }
    }

    private void syncTriageDatabase() {
        PreferencesManager preferencesManager = PreferencesManager.getInstance();
        boolean isExportDone = preferencesManager.getBoolean(TriageSync.KEY_EXPORT_DONE, false);
        PreferencesManager.getInstance().setBoolean(TriageSync.KEY_EXPORT_IN_PROGRESS, true);
        Response<Void> response;

        notifyExportStatus(true, null);

        if (isExportDone) {
            long lastSyncTime = preferencesManager.getLong(TriageSync.KEY_LAST_SYNC_DATE, 0);
            response = new TriageUpdateGet(App.getInstance().getContentResolver(), lastSyncTime)
                    .execute();
        } else {
            long startTime = System.currentTimeMillis();
            L.d(TAG, "START TIRAGE EXPORT");
            response = new TriageExportGet(App.getInstance().getContentResolver()).execute();
            if (response.isSuccess()) {
                L.e(TAG, "END TIRAGE EXPORT! This take: " + (System.currentTimeMillis() - startTime)
                        / DateUtils.SECOND_IN_MILLS + " sec");
                PreferencesManager.getInstance().setBoolean(TriageSync.KEY_EXPORT_DONE, true);
            }
        }

        if (response.isSuccess()) {
            notifyExportStatus(false, null);
        } else {
            notifyExportStatus(true, response.getUserResponseError());
            L.e(TAG, "END TIRAGE EXPORT! FAILED!");
        }

        PreferencesManager.getInstance().setBoolean(TriageSync.KEY_EXPORT_IN_PROGRESS, false);
    }

    private void notifyExportStatus(boolean inProgress, String error) {
        final Context context = App.getInstance();
        Intent intent = new Intent();
        intent.setAction(TriageSync.ACTION_EXPORT_IN_PROGRESS);
        intent.putExtra(TriageSync.KEY_EXPORT_IN_PROGRESS, inProgress);
        intent.putExtra(TriageSync.KEY_ERROR, error);
        context.sendBroadcast(intent);
    }
}
