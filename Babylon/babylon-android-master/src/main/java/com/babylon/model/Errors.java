package com.babylon.model;

import java.util.List;

/**
 * Facebook server response errors
 */
public class Errors {
    private List<String> errors;

    public List<String> getErrors() {
        return errors;
    }
}
