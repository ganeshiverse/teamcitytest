package com.babylon.test;

import android.test.AndroidTestCase;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.enums.SmokeStatus;
import com.babylon.view.SmokerView;

public class SmokerViewTestCase extends AndroidTestCase {
    public void testSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus("smoker");
        assertTrue(smokerView.getNewSmokerStatus() == SmokeStatus.SMOKER);
    }

    public void testExSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus("ex_smoker");
        assertTrue(smokerView.getNewSmokerStatus() == SmokeStatus.EX_SMOKER);
    }

    public void testNonSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus("non_smoker");
        assertTrue(smokerView.getNewSmokerStatus() == SmokeStatus.NON_SMOKER);
    }

    public void testDefaultSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus(null);
        assertTrue(smokerView.getNewSmokerStatus() == SmokeStatus.NON_SMOKER);
    }

    public void testShowSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus("smoker");
        TextView textView = (TextView)smokerView.findViewById(R.id.text_view_smoker);

        assertTrue(textView.getText().toString().equals("Smoker"));
    }

    public void testShowNonSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus("non_smoker");
        TextView textView = (TextView)smokerView.findViewById(R.id.text_view_smoker);

        assertTrue(textView.getText().toString().equals("Non smoker"));
    }

    public void testShowExSmokerStatus() {
        SmokerView smokerView = new SmokerView(getContext());
        smokerView.setSmokerStatus("ex_smoker");
        TextView textView = (TextView)smokerView.findViewById(R.id.text_view_smoker);

        assertTrue(textView.getText().toString().equals("Ex smoker"));
    }
}
