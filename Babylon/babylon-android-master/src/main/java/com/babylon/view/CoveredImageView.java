package com.babylon.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Slowly reveals an image by changing the clip bounds. See {@link #reveal(int)}
 * .
 */
public class CoveredImageView extends ImageView {

    private Rect mRect = new Rect();

    private float visiblePart;

    public CoveredImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CoveredImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CoveredImageView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.getClipBounds(mRect);
        int alpha = Math.round(mRect.width() * visiblePart);
        mRect.right = mRect.left + alpha;
        canvas.clipRect(mRect);

        // Draw current state
        super.onDraw(canvas);
    }

    public void setVisiblePart(float visiblePart) {
        this.visiblePart = visiblePart;
        invalidate();
    }

}