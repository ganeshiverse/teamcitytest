package com.axsystech.excelicare.network.exceptions;

/**
 * Date: 07.05.14
 * Time: 13:47
 *
 * @author SomeswarReddy
 */
public class InvalidSiteUrlException extends Exception {

    private static final long serialVersionUID = -5520989932270955482L;

    public InvalidSiteUrlException() {
    }

    public InvalidSiteUrlException(final String detailMessage) {
        super(detailMessage);
    }

    public InvalidSiteUrlException(final String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InvalidSiteUrlException(final Throwable throwable) {
        super(throwable);
    }
}