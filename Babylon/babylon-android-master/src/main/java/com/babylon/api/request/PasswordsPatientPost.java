package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequest;
import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.utils.L;
import com.google.gson.JsonObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicHeader;

public class PasswordsPatientPost extends AbsRequestPost<Patient, Void> {

    private static final String PATIENT = "patient";

    @Override
    protected BasicHeader getAuthHeader() {
        return null;
    }

    @Override
    public AbsRequestPost<Patient, Void> withEntry(Patient entry) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(PATIENT, getGson().toJsonTree(entry));
        jsonBody = jsonObject.toString();
        return this;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.PASSWORDS);
    }

    @Override
    protected AbsResponseProcessor<Void> getResponseProcessor() {
        return new ResponseProcessor();
    }

    @Override
    protected Response<Void> processResponse(HttpResponse httpResponse) {
        Response<Void> response = super.processResponse(httpResponse);
        return response;
    }

    class ResponseProcessor extends AbsResponseProcessor<Void> {

        public ResponseProcessor() {
            super(Void.TYPE);
        }

        @Override
        public Response<Void> getResponse(HttpResponse httpResponse) {
            Response response = new Response();
            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine != null) {
                L.d(AbsRequest.TAG, getStringResponse(httpResponse));
                if (statusLine.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                    response.setStatus(Response.RESULT_OK);
                }
            }
            return response;
        }
    }
}
