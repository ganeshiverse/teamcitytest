package com.iversecomics.ui.menu.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.iversecomics.archie.android.R;

/**
 * Created by jeffaoliver on 11/15/13.
 */
public class LicenseFragment extends Fragment {

    public static final String ARG_URL = "com.iverse.arg_url";

    public static LicenseFragment newInstance(String licenseUrl) {
        Bundle args = new Bundle();
        args.putString(ARG_URL, licenseUrl);
        LicenseFragment frag = new LicenseFragment();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_license, container, false);

        rootView.findViewById(R.id.action_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        WebView webView = (WebView) rootView.findViewById(R.id.web_view);

        Bundle args = getArguments();
        if (args != null)
            webView.loadUrl(args.getString(ARG_URL));

        return rootView;
    }
}
