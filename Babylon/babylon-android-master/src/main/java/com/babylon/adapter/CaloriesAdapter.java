package com.babylon.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.database.schema.FoodSchema;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.model.PhysicalActivity;

import java.util.concurrent.TimeUnit;

public class CaloriesAdapter extends CursorAdapter {

    public enum CaloriesAdapterType {
        CALORIES_INPUT,
        CALORIES_BURNT
    }

    private CaloriesAdapterType caloriesAdapterType;

    public CaloriesAdapter(Activity activity, Cursor cursor, CaloriesAdapterType caloriesAdapterType) {
        super(activity, cursor, false);
        this.caloriesAdapterType = caloriesAdapterType;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        String title = null;
        String quantity = null;
        String amount = null;

        switch (caloriesAdapterType) {
            case CALORIES_INPUT:
                title = cursor.getString(FoodSchema.Query.TITLE);
                amount = String.valueOf(cursor.getInt(FoodSchema.Query.CALORIES)
                        * cursor.getInt(FoodSchema.Query.COUNT));
                break;

            case CALORIES_BURNT:
                title = cursor.getString(PhysicalActivitySchema.Query.TITLE);
                amount = cursor.getString(PhysicalActivitySchema.Query.CALORIES);

                quantity = getDuration(cursor);

                final String intensity = cursor.getString(PhysicalActivitySchema.Query.INTENSITY);
                if (!TextUtils.isEmpty(intensity)) {
                    quantity += " - " + intensity;
                }

                setImage(cursor, viewHolder.imageView);
                break;
            default:
                break;
        }

        viewHolder.titleTextView.setText(title);
        viewHolder.amountTextView.setText(amount);
        showQuantity(viewHolder, quantity);
    }

    private void showQuantity(ViewHolder viewHolder, String quantity) {
        if(TextUtils.isEmpty(quantity)) {
            viewHolder.quantityTextView.setVisibility(View.GONE);
        } else {
            viewHolder.quantityTextView.setText(quantity);
            viewHolder.quantityTextView.setVisibility(View.VISIBLE);
        }
    }

    private void setImage(Cursor cursor, ImageView imageView) {
        final String source = cursor.getString(PhysicalActivitySchema.Query.SOURCE);

        if (PhysicalActivity.Source.isValidic(source)) {
            imageView.setImageResource(R.drawable.ic_validic_list_item);
        } else {
            imageView.setImageResource(R.drawable.ic_device_list_item);
        }
    }

    private String getDuration(Cursor c) {
        final String source = c.getString(PhysicalActivitySchema.Query.SOURCE);

        String durationFormatted;

        if (PhysicalActivity.Source.isAuto(source)) {
            durationFormatted = App.getInstance().getString(R.string.location_tracking_daily_activity);
        } else {
            int durationInSeconds = c.getInt(PhysicalActivitySchema.Query.DURATION);
            long durationInMinutes = TimeUnit.SECONDS.toMinutes(durationInSeconds);
            String durationStr = String.valueOf(durationInMinutes);
            durationFormatted = String.format(App.getInstance().getString(R.string.activity_duration), durationStr);
        }

        return durationFormatted;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = View.inflate(context, R.layout.list_item_calories, null);
        final ViewHolder holder = new ViewHolder();
        holder.titleTextView = (TextView) view.findViewById(R.id.list_item_calories_title_text_view);
        holder.quantityTextView = (TextView) view.findViewById(R.id.list_item_calories_quantity_text_view);
        holder.amountTextView = (TextView) view.findViewById(R.id.list_item_calories_amount_text_view);
        holder.imageView = (ImageView) view.findViewById(R.id.image_view);
        view.setTag(holder);
        return view;
    }

    private static class ViewHolder {
        TextView titleTextView;
        TextView quantityTextView;
        TextView amountTextView;
        ImageView imageView;
    }
}
