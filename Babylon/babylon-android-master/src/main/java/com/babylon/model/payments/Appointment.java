package com.babylon.model.payments;

import com.babylon.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Appointment extends BaseModel  implements Serializable {

    @BaseModel.SkipFieldInContentValues
    private static final long serialVersionUID = 4725L;

    private Integer id;
    private Float price;
    @SerializedName("consultant_id")
    private Integer consultantId;
    @SerializedName("video_session_id")
    private Integer videoSessionId;
    @SerializedName("time")
    private String startTime;
    @SerializedName("slot_size")
    private Long slotSize;
    @SerializedName("consultant_name")
    private String consultName;
    @SerializedName("patient_id")
    private String patientId;
    @SerializedName("gp_consent")
    private Boolean gpConsent;
    private String state;
    @SerializedName("consultant_role")
    private String consultantRole;
    private Boolean is_using_promotion;

    public Integer videoSessionId() {
        return videoSessionId;
    }

    public Integer getId() {
        return id;
    }

    public Float getPrice() {
        return price == null ? 0 : price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Integer consultantId) {
        this.consultantId = consultantId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Long getSlotSize() {
        return slotSize;
    }

    public void setSlotSize(Long slotSize) {
        this.slotSize = slotSize;
    }

    public String getConsultName() {
        return consultName;
    }

    public void setConsultName(String consultName) {
        this.consultName = consultName;
    }

    public Boolean getGpConsent() {
        return gpConsent;
    }

    public void setGpConsent(Boolean gpConsent) {
        this.gpConsent = gpConsent;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean getIsPaid() {
        return state.equals("paid");
    }

    public String getConsultantRole() {
        return consultantRole;
    }

    public void setConsultantRole(String consultantRole) {
        this.consultantRole = consultantRole;
    }

    public boolean getIsSpecialist() {
        return consultantRole.equals("Specialist");
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public boolean getIsFree() {
        return getPrice().doubleValue() == 0.0;
    }

    public boolean getIsPromotion() {
        return is_using_promotion;
    }
    public void setIsPromotion(Boolean is_using_promotion) {
        this.is_using_promotion = is_using_promotion;
    }
}
