package com.iversecomics.client.comic.viewer.example;

import android.os.Bundle;

import com.iversecomics.client.comic.viewer.AbstractInformationActivity;
import com.iversecomics.client.comic.viewer.error.ErrorReporter;

public class ComicInformationActivity extends AbstractInformationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
        } catch (Throwable t) {
            ErrorReporter.reportException(this, t);
        }
    }
}
