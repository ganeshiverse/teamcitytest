package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.RegionsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.images.FileCache;
import com.babylon.images.ImageLoader;
import com.babylon.model.FileEntry;
import com.babylon.model.Message;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.sync.MessagesSync;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.SelectImageManager;
import com.babylon.utils.UIUtils;
import com.babylon.view.TopBarInterface;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

public class AskQuestionActivity extends BaseActivity {
    private static final int ON_ATTACH_ACTIVITY_RESULT = 1;
    private static final int ON_SUCCESS_SAVING_PROFILE_RESULT = 2;
    private TextView mOutputBox;

    private Message message;
    private Toast toast;
    private ImageButton sendImageButton;
    private ImageButton attachButton;

    private boolean wasMessageAddedForSync = false;
    private byte[] audioByteArrayForPlay;
    private boolean isActivityStartFromSkin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);

        initViews();

        Intent intent = getIntent();
        if (intent != null) {
            int photoAction = intent.getIntExtra(Keys.PHOTO_ACTION, 0);
            if (photoAction != 0) {
                isActivityStartFromSkin = true;
                onAttachClick(photoAction);
            }
        }

        initMessage();

        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PATIENT,
                SyncAdapter.SYNC_PULL);

       /* JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(AskQuestionActivity.this,props);
        helper.sendEvent(MixPanelEventConstants.USER_OPENS_ASK);*/
    }

    @Override
    protected void onResume() {
        attachButton.setEnabled(true);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (!wasMessageAddedForSync) {
            addTextToMessage();
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.GREEN_THEME;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sendImageButton.setEnabled(true);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ON_ATTACH_ACTIVITY_RESULT:
                    isActivityStartFromSkin = false;
                    boolean isAudioResult = data.getBooleanExtra(Keys.AUDIO_RESULT, false);
                    if (isAudioResult) {
                        boolean isDeleteAudio = data.getBooleanExtra(Keys.AUDIO_DELETE_RESULT, false);
                        if (isDeleteAudio) {
                            message.removeExistingFileType(FileEntry.FileType.AUDIO);
                            setVisibleAudioView(View.INVISIBLE);
                        } else {
                            byte[] audioByteArray = data.getByteArrayExtra(Keys.AUDIO_BYTE_ARRAY);
                            if (audioByteArray != null) {
                                audioByteArrayForPlay = audioByteArray;
                                message.addFile(audioByteArray, FileEntry.FileType.AUDIO);
                                setVisibleAudioView(View.VISIBLE, audioByteArray);
                                setVisibilityOfAttachmentView();
                            }
                        }
                    } else {
                        final String photoPath = data.getStringExtra(QuestionAttachmentsActivity.EXTRA_IMAGE_PATH);
                        if (!TextUtils.isEmpty(photoPath)) {
                            sendImageButton.setEnabled(false);
                            processPhoto(photoPath);
                        }
                    }
                    break;
                case ON_SUCCESS_SAVING_PROFILE_RESULT:
                    sendMessage();
                    break;
                default:
                    break;
            }
        } else if (isActivityStartFromSkin && requestCode == ON_ATTACH_ACTIVITY_RESULT) {
            finish();
        }
    }

    private void setVisibilityOfAttachmentView(int visibility) {
        final View view = findViewById(R.id.container_attachments);
        final int curVisibility = view.getVisibility();

        if (curVisibility != visibility) {
            view.setVisibility(visibility);
        }
    }

    private void setVisibilityOfAttachmentView() {
        final View view = findViewById(R.id.container_attachments);
        final int visibility = view.getVisibility();

        final boolean hasFiles = message.hasAudio() || message.hasPhoto();
        if (hasFiles) {
            if (visibility != View.VISIBLE)
                view.setVisibility(View.VISIBLE);
        } else {
            if (view.getVisibility() != View.GONE) {
                view.setVisibility(View.GONE);
            }
            this.getString(R.string.check_connection_problem);
        }
    }

    private void sendMessage() {
        saveMessage();

        if (App.getInstance().isConnection()) {
            sendMessageAsyncTask.execute();
        } else if (message != null && !TextUtils.isEmpty(message.getId())) {
            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MESSAGES, SyncAdapter.SYNC_PUSH);
            onSuccessfulMessageSent(message.getId());
        }
    }

    private void onSuccessfulMessageSent(String questionId) {
        AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.QUESTION_ASKED_EVENT);

        int questionCount = PreferencesManager.getInstance().getQuestionsAskedCount();
        String lastQuestionAsked = PreferencesManager.getInstance().getLastQuestionAskedDate();

        //Send Event to MixPanel
        try {
            JSONObject props = new JSONObject();
            //props.put("Number of Questions Asked", questionCount);
            props.put("Question Asked Date", lastQuestionAsked);


            Bundle fb_parameters = new Bundle();
            fb_parameters.putInt("Number of Questions Asked", questionCount);
            fb_parameters.putString("Question Asked Date", lastQuestionAsked);


            MixPanelHelper helper = new MixPanelHelper(AskQuestionActivity.this, props, fb_parameters);
            helper.sendEvent(MixPanelEventConstants.USER_ASKS_QUESTION);
            helper.incrementProperty("Number of Questions Asked");

            PreferencesManager.getInstance().setLastQuestionAskedDate();
            PreferencesManager.getInstance().incrementQuestionsAskedCount();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        startQuestionActivity(questionId);
        finish();
    }

    public void onSendMessageClick(View view) {
        sendImageButton.setEnabled(false);
        addTextToMessage();
        verifyMessage();
    }

    public void onHowToAskClick(View view) {
        Intent intent = new Intent(this, HowToAskActivity.class);
        startActivity(intent);
    }

    public void onAttachClick(int photoAction) {
        Intent i = new Intent(AskQuestionActivity.this, QuestionAttachmentsActivity.class);
        if (photoAction != 0) {
            i.putExtra(Keys.PHOTO_ACTION, photoAction);
        }

        attachButton.setEnabled(false);
        startActivityForResult(i, ON_ATTACH_ACTIVITY_RESULT);
    }

    public void onAttachClick(View view) {
        onAttachClick(0);
    }

    private void initViews() {
        initializeTopBar();

        sendImageButton = (ImageButton) findViewById(R.id.btn_send_question);
        attachButton = (ImageButton) findViewById(R.id.btn_attach);
        TextView howToAskTextView = (TextView) findViewById(R.id.how_to_ask_text_view);

        setTitle(getString(R.string.ask_question_title));

        howToAskTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onHowToAskClick(view);
            }
        });

        final EditText messageEditTest = (EditText) findViewById(R.id.edit_text_message);
        messageEditTest.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //  mOutputBox.setText(180-count);
            }
        });
    }

    private void showMessage(String message) {
        final EditText messageEditTest = (EditText) findViewById(R.id.edit_text_message);
        messageEditTest.setText(message);
        messageEditTest.setSelection(message.length());

    }

    private void startQuestionActivity(String questionId) {
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(Keys.QUESTION_ID, questionId);
        if (audioByteArrayForPlay != null) {
            intent.putExtra(Keys.AUDIO_BYTE_ARRAY, audioByteArrayForPlay);
        }
        startActivity(intent);
    }

    private void initMessage() {
        message = Message.createNewMessage();
    }

    private void restoreImage(FileEntry fileEntry) {
        final String filePath = fileEntry.getFilePath();
        final Bitmap bitmap = ImageLoader.getInstance().loadBitmap(filePath);

        if (bitmap != null) {
            final byte[] data = ImageLoader.getInstance().convertBitmapToByteArray(bitmap);

            fileEntry.setData(data);
            message.addFile(fileEntry, FileEntry.FileType.PHOTO);

            showPhoto(bitmap, filePath);
            setVisibilityOfAttachmentView();
        }
    }

    private void restoreAudio(FileEntry fileEntry) {
        final String filePath = fileEntry.getFilePath();
        final byte[] audioByteArray = FileCache.getInstance().readFile(filePath);
        fileEntry.setData(audioByteArray);
        message.addFile(fileEntry, FileEntry.FileType.AUDIO);

        audioByteArrayForPlay = audioByteArray;
        setVisibleAudioView(View.VISIBLE, audioByteArray);
        setVisibilityOfAttachmentView();
    }

    private void processPhoto(String photoPath) {
        setProgressBarVisible(true);
        final Bitmap bitmap = ImageLoader.getInstance().getBitmapByPath(photoPath, ImageLoader.BIG_IMAGE);

        if (bitmap != null) {
            showPhoto(bitmap, photoPath);
            addFileToMessage(bitmap);
            setVisibilityOfAttachmentView(View.VISIBLE);
        }
    }

    /**
     * Set the enabled state of this view.
     *
     * @param visibility One of View.VISIBLE, View.INVISIBLE or View.GONE
     */
    private void setVisibleAudioView(int visibility) {
        setVisibleAudioView(visibility, null);
    }

    private void setVisibleAudioView(int visibility, final byte[] audioByteArray) {
        final View audioView = findViewById(R.id.layout_audio);
        audioView.setVisibility(visibility);
        setVisibilityOfAttachmentView();

        if (audioByteArray != null) {
            audioView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AskQuestionActivity.this, RecordAudioActivity.class);
                    intent.putExtra(Keys.AUDIO_BYTE_ARRAY, audioByteArray);
                    startActivity(intent);
                }
            });
        }
    }

    private void addFileToMessage(final Bitmap b) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                byte[] photoByteArray = SelectImageManager.getInstance().getCompressedByByteSize(b);
                message.addFile(photoByteArray, FileEntry.FileType.PHOTO);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                setProgressBarVisible(false);
                sendImageButton.setEnabled(true);
            }
        }.execute();
    }

    private void setImageLayoutVisibility(int visibility) {
        final View photoContainer = findViewById(R.id.layout_photo);
        photoContainer.setVisibility(visibility);
    }

    private void showPhoto(final Bitmap bitmap, final String imagePath) {
        if (bitmap == null) {
            setImageLayoutVisibility(View.INVISIBLE);
        } else {
            setImageLayoutVisibility(View.VISIBLE);

            final ImageView photoImageView = (ImageView) findViewById(R.id.img_photo);
            photoImageView.setImageBitmap(bitmap);

            photoImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AskQuestionActivity.this, ImageActivity.class);
                    intent.putExtra(Keys.KEY_PHOTO_PATH, imagePath);
                    startActivity(intent);
                }
            });

        }

        setVisibilityOfAttachmentView();
    }

    private void addTextToMessage() {
        final EditText messageEditTest = (EditText) findViewById(R.id.edit_text_message);
        final String text = messageEditTest.getText().toString().trim();
        message.setText(text);
    }

    private void verifyMessage() {
        final ValidationInfo validationInfo = verifyMessage(message);
        if (validationInfo.isValid) {
            UIUtils.hideSoftKeyboard(this, (EditText) findViewById(R.id.edit_text_message));
            Patient patient = App.getInstance().getPatient();
            if (patient.isInfoFilled()) {
                sendMessage();
            } else if (PreferencesManager.getInstance().getRegions() != null) {
                final boolean isProfileValid = AskQuestionProfileActivity.isProfileValid();
                if (!isProfileValid) {
                    showProfileActivity();
                } else {
                    sendMessage();
                }
            } else {
                loadRegions();
            }
        } else {
            showErrorMessage(validationInfo);
            sendImageButton.setEnabled(true);
        }
    }

    private void loadRegions() {
        setProgressBarVisible(true);
        RegionsGet regionsGet = new RegionsGet();
        regionsGet.execute(this, new RequestListener<Region[]>() {
            @Override
            public void onRequestComplete(Response<Region[]> response) {
                setProgressBarVisible(false);
                if (response.getStatus() == HttpStatus.SC_OK) {
                    Region[] regions = response.getData();
                    if (regions != null && regions.length != 0) {
                        PreferencesManager.getInstance().setRegions(regions);
                        showProfileActivity();
                    } else {
                        showConnectionErrorMessage();
                    }
                } else {
                    showConnectionErrorMessage();
                }
            }
        });
    }

    private void showConnectionErrorMessage() {
        sendImageButton.setEnabled(true);
        App.getInstance().showWaitingMessage(R.string.offline_error);
    }

    private void showProfileActivity() {
        Intent i = new Intent(this, AskQuestionProfileActivity.class);
        startActivityForResult(i, ON_SUCCESS_SAVING_PROFILE_RESULT);
    }

    private void showErrorMessage(ValidationInfo validationInfo) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(this, validationInfo.errorMessage, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void saveMessage() {
        UIUtils.hideSoftKeyboard(this, (EditText) findViewById(R.id.edit_text_message));
        DatabaseOperationUtils.getInstance().saveMessage(message, getContentResolver());
        wasMessageAddedForSync = true;
    }

    private ValidationInfo verifyMessage(Message message) {
        final ValidationInfo validationInfo = new ValidationInfo(false);

        if (TextUtils.isEmpty(message.getText())) {
            final boolean hasAudio = message.hasAudio();
            if (hasAudio) {
                validationInfo.isValid = true;
            } else {
                validationInfo.errorMessage = getString(R.string.message_is_required);
            }
        } else {
            validationInfo.isValid = true;
        }

        return validationInfo;
    }

    class ValidationInfo {
        boolean isValid;
        String errorMessage;

        public ValidationInfo(boolean isValid) {
            this.isValid = isValid;
        }
    }

    private final AsyncTask<Void, Void, Response<Message.Question>> sendMessageAsyncTask = new AsyncTask<Void, Void,
            Response<Message.Question>>() {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setProgressBarVisible(true);
            sendImageButton.setEnabled(false);
        }

        @Override
        protected Response<Message.Question> doInBackground(Void... params) {
            return new MessagesSync().syncWithServer(getContentResolver
                    (), message);
        }

        @Override
        protected void onPostExecute(Response<Message.Question> response) {
            if (!AskQuestionActivity.this.isFinishing()) {
                setProgressBarVisible(false);

                if (response != null && response.isSuccess() && response.getData() != null
                        && response.getData().getQuestion() != null
                        && !TextUtils.isEmpty(response.getData().getQuestion().getId())) {
                    onSuccessfulMessageSent(response.getData().getQuestion().getId());
                } else {
                    //TODO: remove if condition
                    if (response.getStatus() == MessagesSync.SERVER_RESPONSE_LIMIT_REACHED) {
                        onFailMessageSent(response);
                        sendImageButton.setEnabled(true);
                    } else {
                        //TODO: remove
                        onSuccessfulMessageSent(message.getId());
                    }
                }
            }
        }
    };


    private void onFailMessageSent(Response<Message.Question> response) {
        String text = response.getUserResponseError();

        if (TextUtils.isEmpty(text)) {
            text = getString(R.string.some_error);
        }

        if (response.getStatus() == MessagesSync.SERVER_RESPONSE_LIMIT_REACHED) {
            onLimitReachedError(text);
        } else {
            if (response.getStatus() != HttpStatus.SC_UNAUTHORIZED)
                Toast.makeText(App.getInstance(), text, Toast.LENGTH_LONG).show();
        }

        finish();
    }

    private void onLimitReachedError(String text) {
        final String message = text + " " + getString(R.string.message_was_discarded);

        if (App.getInstance().isAppOnForeground()) {
            Toast.makeText(App.getInstance(), message, Toast.LENGTH_LONG).show();
        }
    }
}
