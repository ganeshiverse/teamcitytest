package com.babylon.api.request.base;

import android.text.TextUtils;
import com.babylon.App;
import com.babylon.R;
import org.apache.http.HttpStatus;

public abstract class RequestListenerErrorProcessed<O> implements RequestListener<O> {
    @Override
    public void onRequestComplete(Response<O> response) {
        String errorMessage = null;

        boolean isResponseOk = response.getStatus() == HttpStatus.SC_OK || response.getStatus() == HttpStatus
                .SC_NO_CONTENT;

        if (response != null && !isResponseOk) {
            errorMessage = App.getInstance().getString(R.string.some_error);
            if(!TextUtils.isEmpty(response.getUserResponseError())) {
                errorMessage = response.getUserResponseError();
            } else if(!TextUtils.isEmpty(response.getResponseError())) {
                errorMessage = response.getResponseError();
            }
        }


        onRequestComplete(response, errorMessage);
    }

    public abstract void onRequestComplete(Response<O> response, String errorMessage);
}
