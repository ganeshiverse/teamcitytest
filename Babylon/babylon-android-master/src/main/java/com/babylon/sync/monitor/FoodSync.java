package com.babylon.sync.monitor;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.babylon.api.request.AddNutrientListPost;
import com.babylon.api.request.NutrientListDelete;
import com.babylon.api.request.NutrientListGet;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.Table;
import com.babylon.database.schema.FoodSchema;
import com.babylon.model.Food;
import com.babylon.model.Nutrient;
import com.babylon.sync.AbsSyncStrategy;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class FoodSync extends AbsSyncStrategy {

    Set<Long> updatedDates = new HashSet<Long>();

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        updatedDates.clear();

        Cursor notSyncActivityCursor = getUpdateCursor(contentResolver, Table.FOOD,
                FoodSchema.CONTENT_URI, null);
        List<Food> addServerFoodList = new ArrayList<Food>();
        List<Food> addFoodList = new ArrayList<Food>();
        List<String> deleteIds = new ArrayList<String>();
        while (notSyncActivityCursor.moveToNext()) {
            Food food = new Food();
            food.createFromCursor(notSyncActivityCursor);

            switch (food.getSyncWithServer()) {
                case SyncUtils.SYNC_TYPE_ADD:
                    addServerFoodList.add(food.getServerModel());
                    addFoodList.add(food);
                    updatedDates.add(DateUtils.getDayStartMills(food.getDate().getTime()));
                    break;
                case SyncUtils.SYNC_TYPE_DELETE:
                    deleteIds.add(String.valueOf(food.getId()));
                    updatedDates.add(DateUtils.getDayStartMills(food.getDate().getTime()));
                    break;
                default:
                    break;
            }
        }
        notSyncActivityCursor.close();

        if (!addServerFoodList.isEmpty()) {
            final ResponseList<Nutrient> response = postAddFood(addServerFoodList);
            if (response != null) {
                final List<Nutrient> result = response.getList();

                if (result != null) {
                    updateAddedFood(contentResolver, addFoodList, result);
                }
            }
        }

        if (!deleteIds.isEmpty()) {
            NutrientListDelete foodListDelete = NutrientListDelete.newFoodListDelete(deleteIds);
            Response deleteResponse = foodListDelete.execute();
            if (deleteResponse != null && deleteResponse.isSuccess()) {
                deleteFood(contentResolver, deleteIds);
            }
        }

        pullParams(contentResolver);
    }

    private void pullParams(ContentResolver provide) {
        for (Iterator<Long> it = updatedDates.iterator(); it.hasNext(); ) {
            Long dayInMillis = it.next();
            Bundle b = new Bundle();
            b.putLong(Constants.EXTRA_MIN_DATE, dayInMillis);
            b.putLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(dayInMillis, 1));
            new MonitorMeParamSync().pull(provide, b);
        }
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        long minDate = bundle.getLong(Constants.EXTRA_MIN_DATE, SyncUtils.getMinDateSyncInMillis());
        long maxDate = bundle.getLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(new Date(), 1));

        Response<ResponseList<Food>> response = NutrientListGet
                .newNutrientGet(minDate, maxDate).execute();
        saveResults(contentResolver, response, minDate, maxDate);
    }

    private
    @Nullable
    ResponseList<Nutrient> postAddFood(List<Food> foodList) {
        AddNutrientListPost nutrientListPost = new AddNutrientListPost();
        Food.FoodList serverFoodList = new Food.FoodList();
        serverFoodList.setNutrients(foodList);
        nutrientListPost.withEntry(serverFoodList);

        Response<ResponseList<Nutrient>> response = nutrientListPost.execute();

        return response.isSuccess() ? response.getData() : null;
    }

    private void updateAddedFood(ContentResolver contentResolver,
                                 List<Food> foodList,
                                 List<Nutrient> responseNutrients) {
        if (foodList.size() == responseNutrients.size()) {
            List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
            int count = 0;
            for (Food food : foodList) {
                Integer id = responseNutrients.get(count++).getId();
                int oldId = food.getId();
                if (id != null) {
                    food.setId(id);
                    food.setSyncWithServer(SyncUtils.SYNC_TYPE_ADDED);
                    batch.add(ContentProviderOperation.newUpdate(FoodSchema.CONTENT_URI)
                            .withValues(food
                                    .toContentValues())
                            .withSelection(FoodSchema.Columns.ID.getName() + " = ?",
                                    new String[]{String.valueOf(oldId)})
                            .build());
                }
            }
            DatabaseOperationUtils.applyBatch(contentResolver, batch);
        }
    }

    private void deleteFood(ContentResolver contentResolver, List<String> deleteIds) {
        List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        for (String id : deleteIds) {
            batch.add(ContentProviderOperation.newDelete(FoodSchema.CONTENT_URI)
                    .withSelection(FoodSchema.Columns.ID.getName() + "= ?",
                            new String[]{id})
                    .build());
        }
        DatabaseOperationUtils.applyBatch(contentResolver, batch);
    }

    public static void saveResults(ContentResolver contentResolver,
                                   Response<ResponseList<Food>> response, long minDate, long maxDate) {
        ResponseList<Food> data = response.getData();
        if (response.isSuccess() && data != null) {
            List<Food> nutrientList = data.getList();
            List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

            StringBuilder stringNutrientIdBuilder = new StringBuilder();
            stringNutrientIdBuilder.append("(");
            for (Food nutrientServer : nutrientList) {
                stringNutrientIdBuilder.append("'")
                        .append(nutrientServer.getId()).append("'").append(",");

                nutrientServer.setSyncWithServer(SyncUtils.SYNC_TYPE_ADDED);
                batch.add(ContentProviderOperation.newInsert(FoodSchema.CONTENT_URI)
                        .withValues(nutrientServer
                                .toContentValues()).build());

            }
            DatabaseOperationUtils.applyBatch(contentResolver, batch);
            stringNutrientIdBuilder.deleteCharAt(stringNutrientIdBuilder.length() - 1).append(")");
            if (stringNutrientIdBuilder.toString().length() > 1) {
                contentResolver.delete(FoodSchema.CONTENT_URI,
                        FoodSchema.Query.SELECTION_DELETE + stringNutrientIdBuilder.toString()
                                + " AND " +
                                FoodSchema.Columns.DATE.getName() + " >= " + String.valueOf(minDate)
                                + " AND " +
                                FoodSchema.Columns.DATE.getName() + " < " + String.valueOf(maxDate)
                                + " AND " +
                                FoodSchema.Columns.SYNC_WITH_SERVER + " != " + SyncUtils.SYNC_TYPE_ADD,
                        null);
            }
        }
    }
}