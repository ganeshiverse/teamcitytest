package com.babylon.utils;

import android.util.Log;

import com.babylon.BaseConfig;

/**
 * Class for logging
 */

public class L {
    protected L() {
        // not called
    }

    public static void i(String tag, String string) {
        if (BaseConfig.LOG_ENABLED && tag != null && string != null) {
            android.util.Log.i(tag, string);
        }
    }

    public static void e(String tag, String string) {
        if (BaseConfig.LOG_ENABLED && tag != null && string != null) {
            android.util.Log.e(tag, string);
        }
    }

    public static void e(String tag, String string, Throwable e) {
        if (BaseConfig.LOG_ENABLED && tag != null && string != null) {
            android.util.Log.e(tag, string, e);
        }
    }

    public static void d(String tag, String string) {
        if (BaseConfig.LOG_ENABLED && tag != null && string != null) {
            int maxLogSize = 2000;
            for (int i = 0; i <= string.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > string.length() ? string.length() : end;
                Log.d(tag, string.substring(start, end));
            }
        }
    }

    public static void v(String tag, String string) {
        if (BaseConfig.LOG_ENABLED && tag != null && string != null) {
            android.util.Log.v(tag, string);
        }
    }

    public static void w(String tag, String string) {
        if (BaseConfig.LOG_ENABLED && tag != null && string != null) {
            android.util.Log.w(tag, string);
        }
    }
}
