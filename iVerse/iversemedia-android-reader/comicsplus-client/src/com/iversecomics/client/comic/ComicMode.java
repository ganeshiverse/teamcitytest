package com.iversecomics.client.comic;

import com.iversecomics.client.util.Dim;

public enum ComicMode {
    PAGE(Dim.PAGE), PANEL(Dim.PANEL), PREVIEW(Dim.PAGE), STREAMING(Dim.PAGE);

    public static ComicMode fromType(String type) {
        if (type == null) {
            return null;
        }
        if ("page".equalsIgnoreCase(type)) {
            return PAGE;
        }
        if ("panel".equalsIgnoreCase(type)) {
            return PANEL;
        }
        if("preview".equalsIgnoreCase(type)) {
        	return PREVIEW;
        }
        if("streaming".equalsIgnoreCase(type)) {
        	return STREAMING;
        }
        return null;
    }


    public static String toString(final ComicMode mode)
    {
    	if(mode == PAGE)
    		return "page";
    	else if(mode == PANEL)
    		return "panel";
    	else if(mode == PREVIEW)
    		return "preview";
        else if(mode == STREAMING)
    		return "streaming";
    	return null;
    }
    private Dim dim;

    private ComicMode(Dim dim) {
        this.dim = dim;
    }

    public Dim getDim() {
        return dim;
    }
}
