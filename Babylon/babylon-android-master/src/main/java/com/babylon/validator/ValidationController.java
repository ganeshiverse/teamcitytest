package com.babylon.validator;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Pair;

import com.babylon.App;
import com.babylon.R;
import com.babylon.view.CustomEditText;

import java.util.ArrayList;
import java.util.List;

public class ValidationController implements TextWatcher {
    private List<Pair<CustomEditText, BaseValidator>> validations;
    private List<String> errors = new ArrayList<String>();
    private EmptyValidator emptyValidator;
    private String emptyErrorMessageDefault;
    boolean formIsFilled = false;
    private FormFilledChangedListenerInterface formFilledListener;

    public ValidationController() {
        validations = new ArrayList<Pair<CustomEditText, BaseValidator>>();
        emptyErrorMessageDefault = App.getInstance().getString(R.string.validator_field_is_empty_default);
        emptyValidator = new EmptyValidator(null, App.getInstance().getString(R.string.validator_field_is_empty));
    }

    public void addValidation(CustomEditText editText, BaseValidator validator) {
        Pair<CustomEditText, BaseValidator> validation = new Pair<CustomEditText, BaseValidator>(editText, validator);
        validations.add(validation);

        /*if (editText.isMandatory()) {
            editText.getEditText().addTextChangedListener(this);
        }*/
    }

    public void removeLastValidation(CustomEditText editText) {
        int indexToRemove = -1;
        for (int i = validations.size() - 1; i >= 0; i--) {
            Pair<CustomEditText, BaseValidator> validation = validations.get(i);
            if (validation.first.getEditText().getId() == editText.getEditText().getId()) {
                indexToRemove = i;
                break;
            }
        }
        if (indexToRemove != -1) {
            validations.remove(indexToRemove);
        }
    }

    public boolean performValidationEditText(CustomEditText customEditText) {
        boolean success = true;
        errors.clear();

        for (Pair<CustomEditText, BaseValidator> validation : validations) {
            if (validation.first.getId() == customEditText.getId()) {
                success = performValidation(validation);
                if (!success) {
                    break;
                }
            }
        }

        return success;
    }

    public boolean performValidation() {
        boolean success = true;
        errors.clear();

        for (Pair<CustomEditText, BaseValidator> validation : validations) {
            success = performValidation(validation);
            if (!success) {
                break;
            }
        }

        return success;
    }

    public boolean performValidation(Pair<CustomEditText, BaseValidator> validation) {
        boolean success = true;

        String error = null;
        validation.first.setDefaultStatus();

        if (validation.first.isMandatory()) {
            error = emptyValidator.validate(validation.first.getText());
            if (error != null) {
                if (validation.first.getEmptyValidationErrorText() != null) {
                    error = validation.first.getEmptyValidationErrorText();
                } else if (validation.first.getTitleText() != null) {
                    error = String.format(error, validation.first.getTitleText());
                } else {
                    error = emptyErrorMessageDefault;
                }
            }
        } else {
            if (TextUtils.isEmpty(validation.first.getText())) {
                return true;
            }
        }

        if (error == null && validation.second != null) {
            error = validation.second.performValidation(validation.first.getText());
        }

        if (error != null) {
            success = false;
            validation.first.setErrorStatus(error);
            errors.add(error);
        } else {
            validation.first.format();
        }

        return success;
    }

    public String getFirstError() {
        String error = null;
        if (errors.size() > 0) {
            error = errors.get(0);
        }
        return error;
    }

    public void setFormFilledListener(FormFilledChangedListenerInterface formFilledListener) {
        this.formFilledListener = formFilledListener;
    }

    private void checkIfFormIsFilled() {
        if (formFilledListener != null) {
            boolean filled = true;

            for (Pair<CustomEditText, BaseValidator> validation : validations) {
                if (validation.first.isMandatory() && TextUtils.isEmpty(validation.first.getText())) {
                    filled = false;
                    break;
                }
            }

            if (this.formIsFilled != filled) {
                this.formIsFilled = filled;
                formFilledListener.onFormFilledChange(filled);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        checkIfFormIsFilled();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
}
