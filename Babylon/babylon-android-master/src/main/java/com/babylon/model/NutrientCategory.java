package com.babylon.model;

import com.babylon.App;
import com.babylon.R;

public enum NutrientCategory {
    BREAKFAST(1), LUNCH(2), DINNER(3), SNACKS(4);

    private int value;

    NutrientCategory(int value) {
        this.value = value;
    }

    public String getTitle() {
        String result = "";

        switch (this) {
            case BREAKFAST:
                result = App.getInstance().getString(R.string.category_breakfast);
                break;
            case LUNCH:
                result = App.getInstance().getString(R.string.category_lunch);
                break;
            case DINNER:
                result = App.getInstance().getString(R.string.category_dinner);
                break;
            case SNACKS:
                result = App.getInstance().getString(R.string.category_snacks);
                break;
        }

        return result;
    }

    public int getValue() {
        return value;
    }
}
