package com.babylon.utils;

import android.content.Context;
import android.os.Bundle;
import com.babylon.App;
import com.facebook.AppEventsLogger;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import org.json.JSONObject;

import java.util.Map;

public class MixPanelHelper
{
    private Context mContext;
    private JSONObject mProps;
    private Bundle mFbParametrs;
    private MixpanelAPI mMixpanel;

    public MixPanelHelper(Context context, JSONObject props,Bundle fbParametrs)
    {
        mContext = context;
        mProps = props;
        mFbParametrs=fbParametrs;
    }

    public void sendEvent(MixPanelEventConstants eventConstant)
    {
        getInstance();
        mMixpanel.getPeople().identify(App.getInstance().getPatient().getId());
/*      mixpanel.getPeople().set("first_name", App.getInstance().getPatient().getFirstName());
        mixpanel.getPeople().set("last_name",App.getInstance().getPatient().getLastName());*/
        mMixpanel.getPeople().set("email",App.getInstance().getPatient().getEmail());
        mMixpanel.identify(App.getInstance().getPatient().getId());
        mMixpanel.track(eventConstant.getEventName(), mProps);
        //FB EVENTS
        AppEventsLogger logger = AppEventsLogger.newLogger(mContext);
        if(mFbParametrs!=null)
        {
            logger.logEvent(eventConstant.getEventName(), mFbParametrs);
        }
        else
        {
            logger.logEvent(eventConstant.getEventName());
        }
     //   Bundle parameters = new Bundle();
    }

    public void getInstance()
    {
        mMixpanel = MixpanelAPI.getInstance(mContext,Constants.MIX_PANEL_TOKEN);
    }

    public void incrementProperty(String props)
    {
        mMixpanel.getPeople().increment(props,1);
    }
}
