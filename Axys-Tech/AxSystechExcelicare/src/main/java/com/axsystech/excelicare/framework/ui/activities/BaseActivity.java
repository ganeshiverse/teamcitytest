package com.axsystech.excelicare.framework.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.app.CustomDialog;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.SiteListResponse;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.loaders.SaveSiteDetailsLoader;
import com.axsystech.excelicare.network.exceptions.InternalServerException;
import com.axsystech.excelicare.network.exceptions.InvalidSiteUrlException;
import com.axsystech.excelicare.network.exceptions.NoMessageShowInBaseActivityException;
import com.axsystech.excelicare.network.exceptions.TokenExpiredException;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.Trace;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.Injector;
import com.mig35.loaderlib.exceptions.NoNetworkException;
import com.mig35.loaderlib.utils.ActivityLoaderHelper;
import com.mig35.loaderlib.utils.ActivityLoaderListener;
import com.mig35.loaderlib.utils.FragmentToActivityLoaderTaskListener;
import com.mig35.loaderlib.utils.LoaderHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 06.05.14
 * Time: 13:37
 *
 * @author SomeswarReddy
 */
public abstract class BaseActivity extends ActionBarActivity implements ActivityLoaderListener {

    private String TAG = BaseActivity.class.getSimpleName();

//    protected static final String GET_SITE_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.SplashActivity.GET_SITE_LIST_LOADER";
//    protected static final String SAVE_SITE_LIST_LOADER = "com.axsystech.excelicare.framework.ui.activities.SplashActivity.SAVE_SITE_LIST_LOADER";
    protected static final String SYSTEM_PREFERENCES_LOADER = "com.axsystech.excelicare.framework.ui.activities.SplashActivity.SYSTEM_PREFERENCES_LOADER";

    private Injector mInjector;

    private CustomDialog mDialog;

    @InjectSavedState
    private ActivityLoaderHelper mActivityLoaderHelper;

    private IntentFilter mLogoutIntentFilter;

    /**
     * This method is abstract and will be implemented by the sub classes which extends BaseActivity.
     * We will update the actionbar title and also based on LeftNav enable/disable flag that we receive in fragment we will
     * display leftnav menu or back button in the actionbar.
     */
    protected abstract void initActionBar();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        mInjector = Injector.init(this);

        mInjector.applyOnActivityCreate(this, savedInstanceState);

        if (null == mActivityLoaderHelper) {
            mActivityLoaderHelper = new ActivityLoaderHelper();
        }

        super.onCreate(savedInstanceState);

        mActivityLoaderHelper.onCreate(this);

        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        mLogoutIntentFilter = new IntentFilter();
        mLogoutIntentFilter.addAction(AxSysConstants.LOGOUT_INTENT_FILTER);
    }

    @Override
    public void setContentView(final int layoutResID) {
        super.setContentView(layoutResID);

        mInjector.applyOnActivityContentChange(this);
    }

    @Override
    public void setContentView(final View view) {
        super.setContentView(view);

        mInjector.applyOnActivityContentChange(this);
    }

    @Override
    public void setContentView(final View view, final ViewGroup.LayoutParams params) {
        super.setContentView(view, params);

        mInjector.applyOnActivityContentChange(this);
    }

    @Override
    public void addContentView(final View view, final ViewGroup.LayoutParams params) {
        super.addContentView(view, params);

        mInjector.applyOnActivityContentChange(this);
    }

    @Override
    protected void onStart() {
        mActivityLoaderHelper.onStart();

        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLogoutIntentFilter != null && mLogoutReceiver != null) {
            registerReceiver(mLogoutReceiver, mLogoutIntentFilter);
        }
        initActionBar();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        mActivityLoaderHelper.onResumeFragments();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mActivityLoaderHelper.onDestroy();
        mInjector.applyOnActivityDestroy(this);

        if (mLogoutReceiver != null) {
            unregisterReceiver(mLogoutReceiver);
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        mActivityLoaderHelper.onSaveInstanceState();
        mInjector.applyOnActivitySaveInstanceState(this, outState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mActivityLoaderHelper.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mActivityLoaderHelper.onStop();
    }

    @Override
    public void showProgress(final boolean hasRunningLoaders) {
        setSupportProgressBarIndeterminateVisibility(hasRunningLoaders);
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        hideProgressLoader();
        getLoaderHelper().destroyAsyncLoader(id);

        /*if (id == getLoaderHelper().getLoaderId(GET_SITE_LIST_LOADER)) {
            // Cache the Site List Details in Global Object
            if (result != null && result instanceof SiteListResponse) {
                SiteListResponse response = (SiteListResponse) result;
                if (response.getSiteDetailsModelArrayList() != null && response.getSiteDetailsModelArrayList().size() > 0) {
                    GlobalDataModel.getInstance().setSiteList(response.getSiteDetailsModelArrayList());

                    // Save SiteDetails in Database
                    final SaveSiteDetailsLoader saveSiteDetailsLoader = new SaveSiteDetailsLoader(this, response.getSiteDetailsModelArrayList());
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_SITE_LIST_LOADER), saveSiteDetailsLoader);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_SITE_LIST_LOADER)) {
            Trace.d(TAG, "SUCCESS: Saving Site Details in DB...");
        } else*/ if(id == getLoaderHelper().getLoaderId(SYSTEM_PREFERENCES_LOADER)) {
            if(result != null && result instanceof List) {
                ArrayList<SystemPreferenceDBModel> systemPreferenceDBModelAL = (ArrayList<SystemPreferenceDBModel>) result;

                // Cache SystemPreference data in Global Object
                GlobalDataModel.getInstance().setSystemPreferencesAL(systemPreferenceDBModelAL);
            }
        }
    }

    @Override
    public void onLoaderError(final int id, final Exception exception) {
        hideProgressLoader();
        getLoaderHelper().destroyAsyncLoader(id);

        // general base activity exception handle
        if (exception instanceof NoNetworkException) {
            showToast(R.string.error_no_network);
        } else if (exception instanceof InternalServerException) {
            if (!TextUtils.isEmpty(exception.getMessage())) {
                showToast(exception.getMessage());
            }
        } else if (exception instanceof InvalidSiteUrlException) {
            final Intent i = new Intent(this, AppWelcomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        } else if (exception instanceof TokenExpiredException) {
            final Intent i = new Intent(this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        } else if (!(exception instanceof NoMessageShowInBaseActivityException)) {
//            showToast(R.string.error_unknown_problem);
        } /*else if (id == getLoaderHelper().getLoaderId(SAVE_SITE_LIST_LOADER)) {
            Trace.d(TAG, "FAILURE: Saving Site Details in DB...");
        }*/ else if (exception instanceof Exception) {
            showToast(exception.getMessage());
        }
    }

    @Override
    public LoaderHelper getLoaderHelper() {
        return mActivityLoaderHelper;
    }

    @Override
    public void addLoaderListener(final FragmentToActivityLoaderTaskListener loaderTaskListener) {
        mActivityLoaderHelper.addLoaderListener(loaderTaskListener);
    }

    @Override
    public void removeLoaderListener(final FragmentToActivityLoaderTaskListener loaderFragment) {
        mActivityLoaderHelper.removeLoaderListener(loaderFragment);
    }

    public void showToast(final int toastMessage) {
        showToast(getString(toastMessage));
    }

    public void showToast(final String message) {
        AxSysTechApplication.getAppContext().showToast(message);
    }

    public void showDialog(final DialogFragment dialogFragment) {
        showDialog(dialogFragment, null);
    }

    public void showDialog(final DialogFragment dialogFragment, final String tag) {
        showDialog(dialogFragment, tag, true);
    }

    public void showDialog(final DialogFragment dialogFragment, final String tag, final boolean allowStateLoss) {
        if (isFinishing()) {
            return;
        }
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(dialogFragment, tag);
        if (allowStateLoss) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }
    }

    public String getStringTextView(final TextView textView) {
        return textView != null && textView.getText() != null ? textView.getText().toString().trim() : "";
    }

    public void displayProgressLoader(boolean isCancelable) {
        if (mDialog == null) {
            mDialog = new CustomDialog(this);
        }
        mDialog.setCancelable(isCancelable);

        if (!mDialog.isShowing()) {
            mDialog.show();
        }
    }

    public void hideProgressLoader() {
        if (mDialog == null) {
            return;
        }

        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    private BroadcastReceiver mLogoutReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    /**
     * This method is used to clear all the backstack fragments added to given activity.
     * @param activity
     */
    public void clearFragmentsBackStackInActivity(final BaseActivity activity) {
        // Run backstack clearing task in background so that we cannot get performance issues with Navigation Drawer
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                FragmentManager fm = activity.getSupportFragmentManager();

                List<Fragment> fragmentsList = fm.getFragments();
                try {
                    Log.d("BACK_STACK", "fragmentsList # Count::" + (fragmentsList != null ? fragmentsList.size() : 0));
                    if (fragmentsList != null && fragmentsList.size() > 0) {
                        for (Fragment fragment : fragmentsList) {
                            if (fm.beginTransaction() != null && fragment != null) {
                                fm.beginTransaction().remove(fragment).commit();
                            }
                        }
                    }

                    int bsCount = fm.getBackStackEntryCount();
                    Log.d("BACK_STACK", "BACK_STACK # Count::" + bsCount);
                    for (int i = 0; i < bsCount; ++i) {
                        fm.popBackStack();
                        Log.d("BACK_STACK", "BACK_STACK # Poping Fragment from BackStack...");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
