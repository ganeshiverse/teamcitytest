package com.iversecomics.client.util;

import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.util.text.StringUtils;

public final class DBUtil {
    private static final Logger LOG = LoggerFactory.getLogger(DBUtil.class);

    public static String[] appendColumn(String[] columns, String extraColumn) {
        String ret[] = new String[columns.length + 1];
        System.arraycopy(columns, 0, ret, 0, columns.length);
        ret[columns.length] = extraColumn;
        return ret;
    }

    public static String asAlias(String table, String id, String alias) {
        return String.format("%s.%s AS %s", table, id, alias);
    }

    public static void close(Cursor c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        } catch (Throwable t) {
            LOG.warn(t, "while closing cursor: " + c);
        }
    }

    public static void debugQuery(Uri uri, String tables, String[] projection, CharSequence where, String[] whereArgs,
            String orderBy) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Query URI: %s", uri);
            LOG.debug("  tables = %s", tables);
            LOG.debug("  projection = %s", StringUtils.join(projection, ", "));
            LOG.debug("  where = %s", where);
            LOG.debug("  whereArgs = %s", StringUtils.join(whereArgs, ", "));
            LOG.debug("  orderBy = %s", orderBy);
        }
    }

    public static boolean hasSelectionFor(String selection, String table) {
        if (TextUtils.isEmpty(selection)) {
            return false;
        }
        return selection.contains(table + ".");
    }

    public static String toOrderBy(String sortOrder, String defaultSortOrder) {
        if (TextUtils.isEmpty(sortOrder)) {
            return defaultSortOrder;
        }
        return sortOrder;
    }

    public static String toPrependedWhere(String original, String extra, Object... args) {
        StringBuilder where = new StringBuilder();
        where.append(String.format(extra, args));
        if (!TextUtils.isEmpty(original)) {
            where.append(" AND ( ").append(original).append(" ) ");
        }
        return where.toString();
    }

    public static String[] toPrependedWhereArgs(String[] originalArgs, String... extraArgs) {
        String whereArgs[];

        if (originalArgs == null) {
            whereArgs = new String[extraArgs.length];
        } else {
            whereArgs = new String[originalArgs.length + extraArgs.length];
            System.arraycopy(originalArgs, 0, whereArgs, extraArgs.length, originalArgs.length);
        }
        System.arraycopy(extraArgs, 0, whereArgs, 0, extraArgs.length);
        return whereArgs;
    }
}
