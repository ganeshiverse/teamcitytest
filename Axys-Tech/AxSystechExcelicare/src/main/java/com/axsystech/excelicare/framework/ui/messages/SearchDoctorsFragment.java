package com.axsystech.excelicare.framework.ui.messages;

import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.CityOrLocationDetailsResponse;
import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.data.responses.LookupsListObject;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.loaders.CityOrLocationsLoader;
import com.axsystech.excelicare.loaders.GetLookUpDetailsLoader;
import com.axsystech.excelicare.loaders.SearchProvidersLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SearchDoctorsFragment extends BaseFragment implements View.OnClickListener,
        SearchSpecialityFragment.SpeclialitySelectionListener, SearchLocationFragment.LocationSelectionListener {

    private String TAG = SearchDoctorsFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private SearchDoctorsFragment mFragment;

    private static final String SEARCH_DOCTORS_LOADER = "com.axsystech.excelicare.framework.ui.messages.SearchDoctorsFragment.SEARCH_DOCTORS_LOADER ";
    // Find Speciality list
    private static final String FIND_SPECIALIST_LOADER = "com.axsystech.excelicare.framework.ui.messages.SearchDoctorsFragment.FIND_SPECIALIST_LOADER";
    //Find Location tag
    private static final String FIND_LOCATIONS_LOADER = "com.axsystech.excelicare.framework.ui.messages.SearchDoctorsFragment.FIND_LOCATIONS_LOADER";

    private boolean isHomeLocationSelected;
    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mProviderSource = AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS;

    @InjectView(R.id.specialityContainer)
    private LinearLayout specialityContainer;

    @InjectView(R.id.locationContainer)
    private LinearLayout locationContainer;

   // @InjectView(R.id.doctorNameEditText)
    private EditText doctorNameEditText;

   // @InjectView(R.id.specialityTextView)
    private TextView specialityTextView;

   // @InjectView(R.id.hospitalNameEditText)
    private EditText hospitalNameEditText;

    @InjectView(R.id.checkboxSearchFavOnly)
    private CheckBox mCheckboxSearchFavOnly;

   // @InjectView(R.id.locationTextView)
    private TextView locationTextView;

    //@InjectView(R.id.searchdoctorclearButton)
    private TextView searchdoctorclearButton;

    @InjectView(R.id.searchButton)
    private TextView searchButton;

    JSONObject searchCriteriaJson = new JSONObject();
   // private CustomTextView searchfavourite;

     @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(),"Find Doctor / HCP"));
         mActivity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.changepasswordactionbar_color)));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_search_doctor, container, false);

        doctorNameEditText = (EditText)view.findViewById(R.id.doctorNameEditText);
        specialityTextView = (TextView)view.findViewById(R.id.specialityTextView);
        hospitalNameEditText = (EditText)view.findViewById(R.id.hospitalNameEditText);
        locationTextView = (TextView)view.findViewById(R.id.locationTextView);
        searchdoctorclearButton = (TextView)view.findViewById(R.id.searchdoctorclearButton);
        searchButton = (TextView)view.findViewById(R.id.searchButton);

        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

        doctorNameEditText.setTypeface(regular);
        specialityTextView.setTypeface(regular);
        hospitalNameEditText.setTypeface(regular);
        locationTextView.setTypeface(regular);
        searchdoctorclearButton.setTypeface(bold);
        searchButton.setTypeface(bold);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        initViews();
        readIntentArgs();
        addEventListeners();
    }

    private void initViews() {

        specialityTextView.setFocusable(false);
        specialityTextView.setClickable(true);
        locationTextView.setFocusable(false);
        locationTextView.setClickable(true);
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_PROVIDER_SOURCE)) {
                mProviderSource = bundle.getString(AxSysConstants.EXTRA_PROVIDER_SOURCE, AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS);
            }
        }
    }

    private void addEventListeners() {
        specialityContainer.setOnClickListener(this);
        specialityTextView.setOnClickListener(this);
        locationTextView.setOnClickListener(this);
        locationContainer.setOnClickListener(this);
        searchdoctorclearButton.setOnClickListener(this);
        searchButton.setOnClickListener(this);

        mCheckboxSearchFavOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mCheckboxSearchFavOnly.isChecked()) {
                    mCheckboxSearchFavOnly.setButtonDrawable(R.drawable.checked_appointments);
                } else {
                    mCheckboxSearchFavOnly.setButtonDrawable(R.drawable.unchecked_appointments);
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
    }


    @Override
    public void onLoaderError(final int id, final Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(FIND_SPECIALIST_LOADER)) {
            showToast(R.string.no_speciality_list_found);
        } else if (id == getLoaderHelper().getLoaderId(FIND_LOCATIONS_LOADER)) {
            showToast(R.string.no_locations_list_found);
        } else if (id == getLoaderHelper().getLoaderId(SEARCH_DOCTORS_LOADER)) {
            showToast(R.string.no_doctors_list_found);
        }
    }

    @Override
    public void onLoaderResult(final int id, final Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(FIND_SPECIALIST_LOADER)) {
            if (result != null && result instanceof LookUpDetailsResponse) {
                LookUpDetailsResponse lookUpDetailsResponse = (LookUpDetailsResponse) result;
                final ArrayList<LookupsListObject> specialityLookupsList = lookUpDetailsResponse.getSystemLookupsList();

                if (specialityLookupsList != null && specialityLookupsList.size() > 0) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                            FragmentTransaction ft = activityFragmentManager.beginTransaction();

                            SearchSpecialityFragment searchSpecialityFragment = new SearchSpecialityFragment();
                            searchSpecialityFragment.setSpeclialitySelectionListener(mFragment);

                            Bundle bundle = new Bundle();
                            bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                            bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Specialty");
                            bundle.putSerializable(AxSysConstants.EXTRA_SPECIALITY_LOOKUPS_LIST, specialityLookupsList);
                            searchSpecialityFragment.setArguments(bundle);
                            ft.add(R.id.container_layout, searchSpecialityFragment);
                            ft.addToBackStack(SearchSpecialityFragment.class.getSimpleName());
                            ft.commit();
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(FIND_LOCATIONS_LOADER)) {
            if (result != null && result instanceof CityOrLocationDetailsResponse) {
                CityOrLocationDetailsResponse cityOrLocationDetailsResponse = (CityOrLocationDetailsResponse) result;

                if (cityOrLocationDetailsResponse.getLocationListResponses() != null) {
                    final ArrayList<CityOrLocationDetailsResponse.LocationDetails> locationsList = cityOrLocationDetailsResponse.getLocationListResponses();

                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            if (locationsList != null && locationsList.size() > 0) {
                                FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                                FragmentTransaction ft1 = fragmentManager.beginTransaction();

                                SearchLocationFragment searchLocationFragment = new SearchLocationFragment();
                                searchLocationFragment.setLocationSelectionListener(mFragment);

                                Bundle bundle1 = new Bundle();
                                bundle1.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                                bundle1.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_location));
                                bundle1.putSerializable(AxSysConstants.EXTRA_LOCATION_LOOKUPS_LIST, locationsList);
                                searchLocationFragment.setArguments(bundle1);
                                ft1.add(R.id.container_layout, searchLocationFragment);
                                ft1.addToBackStack(SearchLocationFragment.class.getSimpleName());
                                ft1.commit();
                            }
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SEARCH_DOCTORS_LOADER)) {
            if (result != null && result instanceof SearchDoctorsResponse) {
                final SearchDoctorsResponse searchDoctorsResponse = (SearchDoctorsResponse) result;

                String locationId = "0";
                String specialityId = "0";

                if (locationTextView.getTag(R.id.selected_location) != null &&
                        locationTextView.getTag(R.id.selected_location) instanceof CityOrLocationDetailsResponse.LocationDetails) {
                    CityOrLocationDetailsResponse.LocationDetails selectedLocation = (CityOrLocationDetailsResponse.LocationDetails) locationTextView.getTag(R.id.selected_location);
                    locationId = selectedLocation.getLocid();
                }

                if (specialityTextView.getTag(R.id.selected_speciality) != null &&
                        specialityTextView.getTag(R.id.selected_speciality) instanceof LookupsListObject) {
                    LookupsListObject selectedSpeciality = (LookupsListObject) specialityTextView.getTag(R.id.selected_speciality);
                    specialityId = selectedSpeciality.getId() + "";
                }

                if (searchDoctorsResponse.getDoctorDetailsAL() != null && searchDoctorsResponse.getDoctorDetailsAL().size() > 0) {
                    final String finalLocationId = locationId;
                    final String finalSpecialityId = specialityId;

                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                            FragmentTransaction ft1 = fragmentManager.beginTransaction();

                            DoctorsSearchResultsFragments doctorsSearchResultsFragments = new DoctorsSearchResultsFragments();
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                            bundle1.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.title_search_results));
                            bundle1.putString(AxSysConstants.EXTRA_SEARCH_LOCATION_ID, finalLocationId);
                            bundle1.putString(AxSysConstants.EXTRA_SEARCH_SPECIALITY_ID, finalSpecialityId);
                            bundle1.putSerializable(AxSysConstants.EXTRA_DOCTORS_SEARCH_RESULTS_LIST, searchDoctorsResponse.getDoctorDetailsAL());
                            bundle1.putString(AxSysConstants.EXTRA_PROVIDER_SOURCE, mProviderSource);
                            doctorsSearchResultsFragments.setArguments(bundle1);
                            ft1.add(R.id.container_layout, doctorsSearchResultsFragments, DoctorsSearchResultsFragments.class.getSimpleName());
                            ft1.addToBackStack(DoctorsSearchResultsFragments.class.getSimpleName());
                            ft1.commit();
                        }
                    });
                } else {
                    showToast(R.string.empty_doctors_found);
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
//        inflater.inflate(R.menu.message_find_doctors, menu);
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            case R.id.action_menu_search:
                searchProvider();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.specialityTextView:
                // Get Speciality lists from server and then pass results list to speciality fragment
                if (CommonUtils.hasInternet()) {
                    // Reset global data here
                    displayProgressLoader(false);

                    GetLookUpDetailsLoader specialityLookupLoader = new GetLookUpDetailsLoader(mActivity, "", AxSysConstants.LOOKUP_SPECIALITIES, AxSysConstants.LOOKUP_USERLOOKUPS);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(FIND_SPECIALIST_LOADER), specialityLookupLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;
            case  R.id.specialityContainer:
                // Get Speciality lists from server and then pass results list to speciality fragment
                if (CommonUtils.hasInternet()) {
                    // Reset global data here
                    displayProgressLoader(false);

                    GetLookUpDetailsLoader specialityLookupLoader = new GetLookUpDetailsLoader(mActivity, "", AxSysConstants.LOOKUP_SPECIALITIES, AxSysConstants.LOOKUP_USERLOOKUPS);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(FIND_SPECIALIST_LOADER), specialityLookupLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            case R.id.locationContainer:
                // get Location lists from server and then pass results to Location fragment
                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);

                    String dictionaryId = "7000015";
                    String searchString = "";

                    CityOrLocationsLoader cityDetailsLoader = new CityOrLocationsLoader(mActivity, mActiveUser.getToken(), dictionaryId, searchString);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(FIND_LOCATIONS_LOADER), cityDetailsLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;
            case R.id.locationTextView:
                // get Location lists from server and then pass results to Location fragment
                if (CommonUtils.hasInternet()) {
                    displayProgressLoader(false);

                    String dictionaryId = "7000015";
                    String searchString = "";

                    CityOrLocationsLoader cityDetailsLoader = new CityOrLocationsLoader(mActivity, mActiveUser.getToken(), dictionaryId, searchString);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(FIND_LOCATIONS_LOADER), cityDetailsLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
                break;

            case R.id.searchButton:

                searchProvider();
                doctorNameEditText.setText("");
                hospitalNameEditText.setText("");
                mCheckboxSearchFavOnly.setChecked(false);
                mCheckboxSearchFavOnly.setButtonDrawable(R.drawable.unchecked_appointments);
                specialityTextView.setText("");
                locationTextView.setText("");
                try {
                    searchCriteriaJson.put("LocationId", "0");
                    searchCriteriaJson.put("StrSearchText","");
                    searchCriteriaJson.put("ClinicName", "0");
                    searchCriteriaJson.put("SpecialityId", "0");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;

            case R.id.searchdoctorclearButton:
                doctorNameEditText.setText("");
                hospitalNameEditText.setText("");
                mCheckboxSearchFavOnly.setButtonDrawable(R.drawable.unchecked_appointments);
                specialityTextView.setText("");
                locationTextView.setText("");
                try {
                    searchCriteriaJson.put("LocationId", "0");
                    searchCriteriaJson.put("StrSearchText","");
                    searchCriteriaJson.put("ClinicName", "0");
                    searchCriteriaJson.put("SpecialityId", "0");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

            default:
                break;
        }
    }

    private void searchProvider() {

        if (CommonUtils.hasInternet()) {
            displayProgressLoader(false);
            String pageNumber = "1";
            String pageSize = "10";
            String isFavorite = "" ;


            try {

                if (locationTextView.getTag(R.id.selected_location) != null &&
                        locationTextView.getTag(R.id.selected_location) instanceof CityOrLocationDetailsResponse.LocationDetails) {
                    CityOrLocationDetailsResponse.LocationDetails selectedLocation = (CityOrLocationDetailsResponse.LocationDetails) locationTextView.getTag(R.id.selected_location);
                    if (isHomeLocationSelected) {
                        searchCriteriaJson.put("LocationId", -1);
                    } else {
                        searchCriteriaJson.put("LocationId", selectedLocation.getLocid());
                    }
                } else {

                    searchCriteriaJson.put("LocationId", "0");
                }

                if(doctorNameEditText.getText()!=null){
                    searchCriteriaJson.put("StrSearchText",doctorNameEditText.getText().toString());
                }
                else {
                    searchCriteriaJson.put("StrSearchText","");
                }
                if(hospitalNameEditText.getText()!=null){
                    searchCriteriaJson.put("ClinicName",hospitalNameEditText.getText().toString());
                }
                else {
                    searchCriteriaJson.put("ClinicName", "0");
                }

                if (specialityTextView.getTag(R.id.selected_speciality) != null &&
                        specialityTextView.getTag(R.id.selected_speciality) instanceof LookupsListObject) {
                    LookupsListObject selectedSpeciality = (LookupsListObject) specialityTextView.getTag(R.id.selected_speciality);
                    searchCriteriaJson.put("SpecialityId", selectedSpeciality.getId());
                } else {
                    searchCriteriaJson.put("SpecialityId", "0");
                }
                if(mCheckboxSearchFavOnly.isChecked()){
                    isFavorite = "1";
                }
                else {
                    isFavorite = "";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (getView() != null && isAdded()) {
                SearchProvidersLoader searchProvidersLoader = null;
                try {
                    searchProvidersLoader = new SearchProvidersLoader(mActivity, mActiveUser.getToken(),
                            URLEncoder.encode(searchCriteriaJson.toString(), "UTF-8"), pageNumber, pageSize,isFavorite);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SEARCH_DOCTORS_LOADER), searchProvidersLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    @Override
    public void onSpecialitySelected(LookupsListObject selectedSpecialityObject) {
        if (specialityTextView != null) {
            specialityTextView.setTag(R.id.selected_speciality, selectedSpecialityObject);
            specialityTextView.setText(!TextUtils.isEmpty(selectedSpecialityObject.getValue()) ? selectedSpecialityObject.getValue() : "");
        }
    }

    @Override
    public void onLocationSelected(CityOrLocationDetailsResponse.LocationDetails selectedLocationObject, boolean isHomeSelected) {
        if (locationTextView != null) {
            locationTextView.setTag(R.id.selected_location, selectedLocationObject);
            locationTextView.setText(!TextUtils.isEmpty(selectedLocationObject.getCity()) ? selectedLocationObject.getCity() : "");
        }
        isHomeLocationSelected = isHomeSelected;
    }
}
