package com.babylon.api.request;

import android.text.TextUtils;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Doctor;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import org.apache.http.message.BasicHeader;

public class DoctorBiographyGet extends AbsRequestGet<Doctor> {

    private int doctorId;

    public static DoctorBiographyGet getAlliantsDoctorBiography(int doctorId) {
        DoctorBiographyGet alliantsDoctorBiography = new DoctorBiographyGet();
        alliantsDoctorBiography.doctorId = doctorId;
        return alliantsDoctorBiography;
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        String url = Urls.getRuby(Urls.DOCTOR_BIO);
        return TextUtils.concat(url, "/", String.valueOf(doctorId)).toString();
    }

    @Override
    protected AbsResponseProcessor<Doctor> getResponseProcessor() {
        return new RubyResponseProcessor<Doctor>(getOutputType());
    }

}
