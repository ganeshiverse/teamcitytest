package com.babylon.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.model.Consultant;
import com.babylon.model.Specialist;
import com.babylon.utils.FontUtils;

import java.util.List;

public class ConsultantAdapter extends BaseAdapter {
    public static final int TEXT_SIZE = 16;

    private Context context;
    private List<Consultant> consultants;

    public ConsultantAdapter(Context context, List<Consultant> consultants) {
        this.context = context;
        this.consultants = consultants;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Consultant getItem(int position) {
        return consultants.get(position);
    }

    @Override
    public int getCount() {
        return consultants.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Consultant consultant = consultants.get(position);

        if (convertView == null) {
            convertView = new TextView(context);
        }

        TextView textView = (TextView) convertView;
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE);
        int margin = context.getResources().getDimensionPixelSize(R.dimen.activity_margin);
        textView.setPadding(margin, margin, margin, margin);
        textView.setText(consultant.getName());
        textView.setTypeface(FontUtils.getTypeface(FontUtils.Font.RobotoLight));
        textView.setBackgroundColor(context.getResources().getColor(consultant.isSelected() ?
                R.color.blue_top_bar : R.color.white));

        return convertView;
    }
}
