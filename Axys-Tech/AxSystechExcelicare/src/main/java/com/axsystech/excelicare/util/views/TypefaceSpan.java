package com.axsystech.excelicare.util.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.LruCache;

import java.util.Hashtable;

/**
 * Style a {@link android.text.Spannable} with a custom {@link Typeface}.
 *
 * @author Tristan Waddington
 */
public class TypefaceSpan extends MetricAffectingSpan {
    /** An <code>LruCache</code> for previously loaded typefaces. */
    //private static LruCache<String, Typeface> sTypefaceCache =
    //        new LruCache<String, Typeface>(12);

    private Typeface mTypeface;
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    /**
     * Load the {@link Typeface} and apply to a {@link android.text.Spannable}.
     */
    public TypefaceSpan(Context context, String typefaceName) {
        //mTypeface = sTypefaceCache.get(typefaceName);
        if(!cache.contains(typefaceName)) {
            mTypeface = Typeface.createFromAsset(context.getApplicationContext()
                    .getAssets(), String.format("fonts/%s", typefaceName));
                cache.put(typefaceName, mTypeface);
        } else {
            mTypeface = cache.get(typefaceName);
        }
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        p.setTypeface(mTypeface);

        // Note: This flag is required for proper typeface rendering
        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setTypeface(mTypeface);

        // Note: This flag is required for proper typeface rendering
        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }


    public static SpannableString setCustomActionBarTypeface(Context context, String title) {
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan(context, "if_std_bolditalic.ttf"), 0, s.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

// Update the action bar title with the TypefaceSpan instance

        return s;
    }

}