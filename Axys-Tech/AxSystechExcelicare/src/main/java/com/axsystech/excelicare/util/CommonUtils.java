package com.axsystech.excelicare.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.framework.ui.activities.BaseActivity;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.exceptions.NoNetworkException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 12:08
 *
 * @author SomeswarReddy
 */
public final class CommonUtils {

    private static String TAG = CommonUtils.class.getSimpleName();
    public static HashMap<String, Integer> mAlignmentHashmap = new HashMap<String, Integer>();

    static

    {
        mAlignmentHashmap.put("left", Gravity.LEFT);
        mAlignmentHashmap.put("right", Gravity.RIGHT);
        mAlignmentHashmap.put("center", Gravity.CENTER);
        mAlignmentHashmap.put("bottom", Gravity.BOTTOM);
    }

    private CommonUtils() {
    }

    public static void hideSoftKeyboard(final Activity activity) {
        if (activity == null) {
            return;
        }
        hideSoftKeyboard(activity, false, activity.getWindow().getDecorView());
    }

    public static void hideSoftKeyboard(final Context context, final View... views) {
        hideSoftKeyboard(context, true, views);
    }

    public static void hideSoftKeyboard(final Context context, final boolean clearFocus, final View... views) {
        if (null == views || null == context) {
            return;
        }
        final InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        for (final View currentView : views) {
            if (null == currentView) {
                continue;
            }
            if (clearFocus) {
                currentView.clearFocus();
            }
            manager.hideSoftInputFromWindow(currentView.getWindowToken(), 0);
            manager.hideSoftInputFromWindow(currentView.getApplicationWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(final Context context, final View view) {
        if (view == null) {
            return;
        }
        final InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        manager.showSoftInput(view, 0);
    }

    public static File getCacheFileDir() {
        File rootCacheDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            rootCacheDir = AxSysTechApplication.getAppContext().getExternalCacheDir();
        }
        if (checkAndCreateDirIfNotExists(rootCacheDir)) {
            return rootCacheDir;
        }
        rootCacheDir = getInternalCacheDir();
        if (checkAndCreateDirIfNotExists(rootCacheDir)) {
            return rootCacheDir;
        }

        return null;
    }

    public static File getInternalCacheDir() {
        final File rootCacheDir = AxSysTechApplication.getAppContext().getCacheDir();

        if (checkAndCreateDirIfNotExists(rootCacheDir)) {
            return rootCacheDir;
        }

        return null;
    }

    public static File getInternalFilesDir() {
        final File rootFilesDir = AxSysTechApplication.getAppContext().getFilesDir();

        if (checkAndCreateDirIfNotExists(rootFilesDir)) {
            return rootFilesDir;
        }

        return null;
    }

    public static boolean checkAndCreateDirIfNotExists(final File rootCacheDir) {
        if (null != rootCacheDir) {
            if (!rootCacheDir.exists() && rootCacheDir.mkdirs()) {
                return true;
            }

            if (rootCacheDir.exists()) {
                return true;
            }
        }
        return false;
    }

    public static String readStreamAsString(final InputStream inputStream, final String encoding) throws IOException {
        return new String(readStreamAsByteArray(inputStream), encoding);
    }

    public static byte[] readStreamAsByteArray(final InputStream inputStream) throws IOException {
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(new FlushedInputStream(inputStream));

        final byte[] buffer = new byte[2048];
        int read;
        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        while ((read = bufferedInputStream.read(buffer)) != -1) {
            os.write(buffer, 0, read);
        }

        return os.toByteArray();
    }

    public static void checkInternet() throws NoNetworkException {
        final ConnectivityManager cm = (ConnectivityManager) AxSysTechApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            throw new NoNetworkException();
        }
    }

    public static boolean hasInternet() {
        final ConnectivityManager cm = (ConnectivityManager) AxSysTechApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return !(netInfo == null || !netInfo.isConnectedOrConnecting());
    }

    public static boolean checkFormatDouble(final String rating) {
        return rating != null && (rating.length() == 0 || Integer.valueOf(rating) <= 10);
    }

    public static String getDeviceId(final Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static int getColor(final float current, final float max) {
        final float color = 100f * (1 - current / max);
        final float[] hsvColor = {color, 1, 1};
        return Color.HSVToColor(hsvColor);
    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void quitApplication(Context context) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(AxSysConstants.LOGOUT_INTENT_FILTER);
        context.sendBroadcast(broadcastIntent);
    }

    /**
     * Get Device Resolutions at runtime
     */
    @SuppressWarnings("deprecation")
    public static String getDeviceResolution(ActionBarActivity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point dimns = new Point();
        String deviceRes;
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            // getSize method avilable only above API Level 13
            display.getSize(dimns);
            deviceRes = dimns.x + "X" + dimns.y;
        } else {
            deviceRes = display.getWidth() + "X" + display.getHeight();
        }
        return deviceRes;
    }

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    public static String getMediaGroupDateString(long dateInMills) {
        if (dateInMills != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_ONLY_DATE);
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(dateInMills);

                Date mediaDate = sdf.parse(sdf.format(calendar.getTime()));

                Calendar yesterdayCalendar = Calendar.getInstance(); // today
                yesterdayCalendar.add(Calendar.DAY_OF_YEAR, -1); // yesterday
                Date yesterdayDate = sdf.parse(sdf.format(yesterdayCalendar.getTime()));

                 if (mediaDate.compareTo(yesterdayDate) == 0) {
                    return "Yesterday";
                } else if (mediaDate.compareTo(sdf.parse(sdf.format(new Date()))) == 0) {


                    return "Today";
                } else if (isDateInCurrentWeek(mediaDate)) {
                    return "This Week";
                } else if (isDateInLastWeek(mediaDate)) {
                    return "Last Week";
                } else if(isDateInThisMonth(mediaDate)) {
                    return "This Month";
                } else if(isDateInLastMonth(mediaDate)) {
                    return "Last Month";
                } else if(isDateInLastYear(mediaDate)) {
                    return "Last Year";
                } else {
                    return sdf.format(mediaDate);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return "Today";
    }

    public static boolean isDateInCurrentWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);

        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);

        return week == targetWeek && year == targetYear;
    }

    public static boolean isDateInLastWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);

        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR) + 1;
        int targetYear = targetCalendar.get(Calendar.YEAR);

        return week == targetWeek && year == targetYear;
    }

    public static boolean isDateInThisMonth(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int month = currentCalendar.get(Calendar.MONTH);

        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetMonth = targetCalendar.get(Calendar.MONTH);

        return month == targetMonth;
    }

    public static boolean isDateInLastMonth(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int month = currentCalendar.get(Calendar.MONTH);

        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetMonth = targetCalendar.get(Calendar.MONTH) + 1;

        return month == targetMonth;
    }

    public static boolean isDateInLastYear(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int year = currentCalendar.get(Calendar.YEAR);

        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetYear = targetCalendar.get(Calendar.YEAR) + 1;

        return year == targetYear;
    }

    public static long getMediaDateInMills(String dateFormat, String selectedDateString) {
        Calendar calendar = Calendar.getInstance();

        if (!TextUtils.isEmpty(selectedDateString)) {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

            try {
                return sdf.parse(selectedDateString).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return calendar.getTime().getTime();
    }

    public static String getSelectedMediaDateString(Date selectedDate) {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_DATE_TIME);

        if (selectedDate != null) {
            return sdf.format(selectedDate);
        }

        return sdf.format(calendar);
    }

    /****/public static String getSelectedMediaTimeString(Date selectedTime) {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_ONLY_TIME);

        if (selectedTime!= null) {
            return sdf.format(selectedTime);
        }

        return sdf.format(calendar);
    }


    /*public static long getMediaCreatedDateWithoutTime(long createdDateMills) {

        SimpleDateFormat sdf = new SimpleDateFormat(AxSysConstants.DF_MEDIA_DETAILS_ONLY_DATE);

        if (createdDateMills > 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(createdDateMills);

            String parsedDate = sdf.format(calendar.getTime());

            try {
                Date convertedDate = sdf.parse(parsedDate);

                return convertedDate.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return Calendar.getInstance().getTime().getTime();
    }*/

    public static String getMediaServerDateFormat(long createdDateMills) {
        String serverDateFormat = "";
        SimpleDateFormat sdf = new SimpleDateFormat(AxSysConstants.DF_MEDIA_ITEM_SERVER_DATE_FORMAT);

        Calendar calendar = Calendar.getInstance();

        if (createdDateMills > 0) {
            calendar.setTimeInMillis(createdDateMills);

            serverDateFormat = sdf.format(calendar.getTime());
        } else {
            serverDateFormat = sdf.format(calendar.getTime());
        }

        return serverDateFormat;
    }

    public static boolean setGridViewHeightBasedOnItems(GridView listView) {
        ListAdapter listAdapter = listView.getAdapter();

        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = /*listView.getDividerHeight() * (numberOfItems - 1)*/ 0;

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }
    }

    public static File createMediaFolder() {
        // here PictureFolder is the folder name you can change it offcourse
        // External sdcard location
        File mediaStorageDir = new File(MemoryUtil.getAppDirectoryPath(AxSysTechApplication.getAppContext()), AxSysConstants.MEDIA_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Trace.d(TAG, "Oops! Failed to create " + AxSysConstants.MEDIA_DIRECTORY_NAME + " directory!");

                return mediaStorageDir;
            }
        }

        return mediaStorageDir;
    }

    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = AxSysTechApplication.getAppContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(int px) {
        DisplayMetrics displayMetrics = AxSysTechApplication.getAppContext().getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static float pixelsToSp(float px) {
        float scaledDensity = AxSysTechApplication.getAppContext().getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static long getUploadFileSizeInMB(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_UPLOAD_SIZE_ID)) {
                    if(!TextUtils.isEmpty(prefDataObj.getValue()) && prefDataObj.getValue().trim().length() > 0) {
                        return Long.parseLong(prefDataObj.getValue());
                    }
                }
            }
        }

        return 10;
    }

    public static String getIdentification1Pattern(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IDENFICATION1_PATTERN_ID)) {
                    return prefDataObj.getDescription();
                }
            }
        }

        return "";
    }

    public static String getIdentification1HintValue(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IDENFICATION1_PATTERN_ID)) {
                    return prefDataObj.getValue();
                }
            }
        }

        return "";
    }

    public static String getIdentification2Pattern(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IDENFICATION2_PATTERN_ID)) {
                    return prefDataObj.getDescription();
                }
            }
        }

        return "";
    }

    public static String getIdentification2HintValue(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IDENFICATION2_PATTERN_ID)) {
                    return prefDataObj.getValue();
                }
            }
        }

        return "";
    }

    public static boolean isSignUpTypeFull(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_SIGNUP_TYPE_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean shouldDisplaySecurityQuestions(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_TOGGLE_SEQURITY_QUESTIONS_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean shouldDisplayPrivacyPolicy(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_TOGGLE_PRIVACY_POLICY_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean shouldDisplayMobileNumber(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_TOGGLE_MOBILE_NO_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean isMobileNumberManadatory(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IS_MOBILE_NO_MANDATORY)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean shouldDisplayPrimaryIdentifier(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_TOGGLE_PRIMARY_IDENTIFIER_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean isPrimaryIdentifierManadatory(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IS_PRIMARY_IDENTIFIER_MANDATORY)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean shouldDisplaySecondaryIdentifier(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_TOGGLE_SECONDARY_IDENTIFIER_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean isSecondaryIdentifierManadatory(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IS_SECONDARY_IDENTIFIER_MANDATORY)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean shouldDisplayAddressFields(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_TOGGLE_ADDRESS_FILEDS_ID)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean isAddressFieldsManadatory(ArrayList<SystemPreferenceDBModel> systemPreferencesAL) {
        if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
            for (SystemPreferenceDBModel prefDataObj : systemPreferencesAL) {
                if (prefDataObj != null && prefDataObj.getId() != null &&
                        prefDataObj.getId().trim().equals(AxSysConstants.REG_IS_ADDRESS_FILEDS_MANDATORY)) {
                    if (!TextUtils.isEmpty(prefDataObj.getValue())) {
                        if (prefDataObj.getValue().trim().equals("1")) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static void displayWebViewInDialog(BaseActivity mActivity, String cardUrl, String cardName) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_dialog);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        TextView cardTitleTextView = (TextView) dialog.findViewById(R.id.cardTitleTextView);
        if (!TextUtils.isEmpty(cardName)) {
            cardTitleTextView.setText(cardName);
        } else {
            cardTitleTextView.setText("URL Card");
        }

        ImageView closeIV = (ImageView) dialog.findViewById(R.id.closeImageView);

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // Creating webview container
        WebView webView = (WebView) dialog.findViewById(R.id.webView);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= 11) {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            fixNewAndroid(webView);
        }

        if (!TextUtils.isEmpty(cardUrl)) {
            if (cardUrl.contains("pdf") || cardUrl.contains("PDF")) {
                String googleDocs = "https://docs.google.com/gview?embedded=true&url=";
//                String googleDocs = "https://docs.google.com/viewer?url=";
                webView.loadUrl(googleDocs + cardUrl);
            } else {
                webView.loadUrl(cardUrl);
            }
        } else {
            webView.loadUrl("");
        }

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @SuppressLint("NewApi")
    private static void fixNewAndroid(WebView webView) {
        try {
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        } catch (NullPointerException e) {
        }
    }

    public static List<MediaFoldersModel> getFolderIdFromDB(final String mediaType) {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(AxSysTechApplication.getAppContext(), DatabaseHelper.class);
            final Dao<MediaFoldersModel, Long> mediaFolderDao = helper.getDao(MediaFoldersModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<MediaFoldersModel>>() {
                @Override
                public List<MediaFoldersModel> call() throws Exception {
                    List<MediaFoldersModel> queriedFolder = mediaFolderDao.queryBuilder().where().eq(MediaFoldersModel.MEDIA_FOLDER_NAME, mediaType).query();

                    return queriedFolder;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return null;
    }

    public static List<MediaFoldersModel> getFolderNameForId(final int folderId) {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(AxSysTechApplication.getAppContext(), DatabaseHelper.class);
            final Dao<MediaFoldersModel, Long> mediaFolderDao = helper.getDao(MediaFoldersModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<MediaFoldersModel>>() {
                @Override
                public List<MediaFoldersModel> call() throws Exception {
                    List<MediaFoldersModel> queriedFolder = mediaFolderDao.queryBuilder().where().eq(MediaFoldersModel.MEDIA_FOLDER_ID, folderId).query();

                    return queriedFolder;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            OpenHelperManager.releaseHelper();
        }

        return null;
    }
}
