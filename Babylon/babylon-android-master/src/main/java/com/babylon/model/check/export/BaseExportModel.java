package com.babylon.model.check.export;

import com.babylon.model.ServerModel;

public class BaseExportModel extends ServerModel {
    @SkipFieldInContentValues
    public static final int STATE_NEW = 1;
    @SkipFieldInContentValues
    public static final int STATE_UPDATE = 2;
    @SkipFieldInContentValues
    public static final int STATE_DELETE = 3;

    @SkipFieldInContentValues
    int state;
    int id;

    public int getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
