package com.babylon.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A request for retrieving a {@link org.json.JSONObject} response body at a given URL, allowing for an
 * optional {@link org.json.JSONObject} to be passed in as part of the request body.
 */
public class SecureJsonArrayRequest extends JsonArrayRequest
{
    Context mContext;

    String authorization;
    String mRequestAuthURL;



    public SecureJsonArrayRequest(String url, Context context, Listener<JSONArray> listener, ErrorListener errorListener)
    {
        super(url,listener,errorListener);
        mContext = context;
    }

   /* public SecureJsonObjectRequest(String url,Context context,JSONObject jsonRequest,String requestAuthURL,Listener<JSONObject> listener, ErrorListener errorListener)
    {
        this(jsonRequest == null ? Method.GET : Method.POST, url,context,jsonRequest,requestAuthURL,listener,errorListener);
        mContext = context;
    }*/

    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response)
    {

       // Log.d("[Secure JSON Object Request]", "Received JSON: " + new String(response.data));

        return super.parseNetworkResponse(response);
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError
    {
        HashMap<String, String> headers = new HashMap<String, String>();
        if(CommonHelper.getAuthHeader()!= null) {
            headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
        }
        headers.put("Content-Type", "application/json");

        return headers;
    }

}