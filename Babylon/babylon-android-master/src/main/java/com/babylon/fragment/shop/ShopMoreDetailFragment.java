package com.babylon.fragment.shop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.babylon.R;
import com.babylon.fragment.BaseFragment;
import com.babylon.view.TopBarInterface;

public class ShopMoreDetailFragment extends BaseFragment {

    public static final String TAG = ShopMoreDetailFragment.class.getSimpleName();
    private static String DETAIL_BUNDLE_KEY="detail";
    private static String TITLE_BUNDLE_KEY="title";
    private WebView wbVwDetails;
    public static ShopMoreDetailFragment newInstance(String detail) {
        ShopMoreDetailFragment fragment = new ShopMoreDetailFragment();
        Bundle args = new Bundle();
         args.putString(DETAIL_BUNDLE_KEY, detail);
         fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shop_more_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getString(R.string.kit_list_fragment_title));
        wbVwDetails=(WebView)view.findViewById(R.id.webVwDetail);
        wbVwDetails.setBackgroundColor(0x00000000);

        Bundle arguments = getArguments();
        if (arguments != null) {
            String title;
            String details = getArguments().getString(DETAIL_BUNDLE_KEY);
            String[] split=details.split("::");
            if(split.length>0) {
                title = split[0];
                setTitle(title);
                details = split[1];
            }


                details= "<html>"  +   "<body><font color=\"#ffffff\">"+details+"</font></body></html>";
               wbVwDetails.loadDataWithBaseURL(null, details, "text/html", "UTF-8", null);

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.PINK_THEME;
    }

   /* public void OnWebViewTask(final String question,final WebView webView) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                // use data here
                wbVwDetails.loadDataWithBaseURL(null, details, "text/html", "utf-8", null);
            }
        });
    }*/

}






