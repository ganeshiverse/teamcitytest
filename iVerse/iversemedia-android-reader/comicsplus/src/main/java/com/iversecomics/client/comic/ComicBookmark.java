package com.iversecomics.client.comic;

import java.net.URI;

public class ComicBookmark {

    URI imageUri;
    ComicMode mode;
    Integer index;

    public static ComicBookmark fromModeAndIndex(final ComicMode mode, final int index) {
        return new ComicBookmark(mode, index);
    }

    public static ComicBookmark fromURI(final URI bookmark) {
        return new ComicBookmark(bookmark);
    }

    public static ComicBookmark fromURIString(final String bookmark) {
        return fromURI(URI.create(bookmark));
    }

    boolean isValid() {
        return imageUri != null && mode != null && index != null;
    }

    ComicBookmark(final URI bookmark) {
        final String ssp = bookmark.getSchemeSpecificPart();
        int idx = ssp.indexOf('/');
        final String modeStr = ssp.substring(0, idx);
        mode = ComicMode.fromType(modeStr);

        final String indexStr = ssp.substring(idx + 1);
        index = Integer.valueOf(indexStr);

        imageUri = bookmark;
    }

    ComicBookmark(final ComicMode mode, final int index) {
        this.mode = mode;
        this.index = index;
        String uri = String.format("comic:%s/%d", mode.name(), index);
        imageUri = URI.create(uri);
    }

    public String asBookmarkString() {
        return imageUri.toASCIIString();
    }

    public ComicMode getMode() {
        return mode;
    }

    public void setMode(final ComicMode mode) {
        this.mode = mode;
        final String uri = String.format("comic:%s/%d", mode.name(), index);
        imageUri = URI.create(uri);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(final int index) {
        this.index = index;
        final String uri = String.format("comic:%s/%d", mode.name(), index);
        imageUri = URI.create(uri);
    }
}
