package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.SaveMyCircleResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class SaveCircleDetailsLoader  extends DataAsyncTaskLibLoader<BaseResponse> {

    private String requestBody;

    public SaveCircleDetailsLoader(Context context, String requestBody) {
        super(context);
        this.requestBody = requestBody;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().saveMyCircleDetails(requestBody);
    }
}
