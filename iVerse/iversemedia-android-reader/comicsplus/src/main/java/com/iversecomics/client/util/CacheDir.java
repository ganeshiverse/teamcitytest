package com.iversecomics.client.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.iversecomics.io.Digest;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class CacheDir {
    private static final Logger LOG = LoggerFactory.getLogger(CacheDir.class);
    private static final int QUALITY = 100;
    private File cacheDir;

    public CacheDir(Context context) {
        this.cacheDir = context.getCacheDir();
    }

    public boolean addBitmap(URI uri, Bitmap bitmap) {
        if (bitmap == null) {
            return false; // No bitmap? no caching.
        }
        File cachedFile = getFileRef(uri);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(cachedFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, QUALITY, out);
            return true;
        } catch (Exception e) {
            LOG.warn(e, "Unable to add Bitmap (%s): %s", uri.toASCIIString(), cachedFile.getAbsolutePath());
            return false;
        } finally {
            IOUtil.close(out);
        }
    }

    public Bitmap getBitmap(Uri uri) {
        if (uri == null) {
            throw new IllegalArgumentException("Uri cannot be null");
        }
        File cachedFile = getFileRef(uri);
        return getCachedBitmap(uri.toString(), cachedFile);
    }

    public Bitmap getBitmap(URI uri) {
        if (uri == null) {
            throw new IllegalArgumentException("URI cannot be null");
        }
        File cachedFile = getFileRef(uri);
        return getCachedBitmap(uri.toASCIIString(), cachedFile);
    }

    public boolean hasCachedBitmap(URI uri) {
        return (uri != null && getFileRef(uri).exists());
    }

    private Bitmap getCachedBitmap(String uriStr, File cachedFile) {
        if (!cachedFile.exists()) {
            return null;
        }

        InputStream in = null;
        try {
            in = new FileInputStream(cachedFile);
            return BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            LOG.warn(e, "Unable to get Bitmap (%s): %s", uriStr, cachedFile.getAbsolutePath());
            return null;
        } catch (OutOfMemoryError e) {
            // Fatal!
            LOG.error("Deleting Invalid Bitmap: " + cachedFile);
            if (!cachedFile.delete()) {
                LOG.warn("Unable to delete: " + cachedFile);
            }
            return null;
        } finally {
            IOUtil.close(in);
        }
    }

    /**
     * Internal method to obtain a filename reference from a {@link android.net.Uri}
     */
    private File getFileRef(Uri uri) {
        String filename = safeFilename(uri.toString()) + ".dat";
        return new File(cacheDir, filename);
    }

    /**
     * Internal method to obtain a filename reference from a {@link java.net.URI}
     */
    private File getFileRef(URI uri) {
        String filename = safeFilename(uri.toASCIIString()) + ".dat";
        return new File(cacheDir, filename);
    }

    private String safeFilename(String string) {
        try {
            return Digest.md5(string);
        } catch (IOException e) {
            return Long.toString(string.hashCode(), 16);
        }
    }
}
