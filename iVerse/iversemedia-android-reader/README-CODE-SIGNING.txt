
CODE SIGNING SETUP
------------------

Code signing is done with the maven-jarsigner-plugin as part of the build.
To setup the configuration for iverse do the following ...


1) Checkout a copy of the android-signing-keys.

   $ git clone git@https://bitbucket.org/iversemedia/android-reader/

   i). In that repo you will found keystore folder.

2) Add gradle.properties file in root of the project.  The file should look like follows
   replacing /path/to/keystore/iverse.keystore with the correct path.

   releaseKeystore=/path/to/keystore/iverse.keystore
   releaseStorePassword=dr01d_fr34k
   releaseKeyAlias=iverse
   releaseKeyPassword=dr01d_fr34k


3) Now you can build.

   $ ./gradlew build 

