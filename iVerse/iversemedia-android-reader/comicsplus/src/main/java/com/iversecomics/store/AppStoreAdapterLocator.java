package com.iversecomics.store;

import com.iversecomics.client.iab.AbstractAppStoreAdapter;

/**
 * Created by Gabriel Dogaru.
 */
public interface AppStoreAdapterLocator {
    public AbstractAppStoreAdapter getAppStoreAdapter();
}
