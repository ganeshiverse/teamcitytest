package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.network.ServerMethods;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/15/2015.
 */
public class CircleMemberDetailsDbLoader extends DataAsyncTaskLibLoader<ArrayList<CirclesMemberDetailsDbModel>> {

    private ArrayList<CirclesMemberDetailsDbModel> circleMemberDetailsDbModels;
    private String TAG = CircleMemberListDbLoader.class.getSimpleName();

    public CircleMemberDetailsDbLoader(final Context context, final ArrayList<CirclesMemberDetailsDbModel> mediaItems) {
        super(context, true);
        this.circleMemberDetailsDbModels = mediaItems;
    }

    @Override
    protected ArrayList<CirclesMemberDetailsDbModel> performLoad() throws Exception {
        if (circleMemberDetailsDbModels == null)
            throw new SQLException("Unable to cache circle member details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (circleMemberDetailsDbModels != null && circleMemberDetailsDbModels.size() > 0) {

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<CirclesMemberDetailsDbModel>>() {
                    @Override
                    public ArrayList<CirclesMemberDetailsDbModel> call() throws Exception {
                        for (final CirclesMemberDetailsDbModel dbModel : circleMemberDetailsDbModels) {
                            final Dao<CirclesMemberDetailsDbModel, Long> memberDetailsDbModelLongDao = helper.getDao(CirclesMemberDetailsDbModel.class);

                            memberDetailsDbModelLongDao.createOrUpdate(dbModel);
                            Trace.d(TAG, "Saved circles members details in DB...");
                        }

                        return circleMemberDetailsDbModels;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return circleMemberDetailsDbModels;
    }
}