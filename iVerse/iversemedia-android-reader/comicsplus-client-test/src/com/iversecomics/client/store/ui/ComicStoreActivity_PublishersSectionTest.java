package com.iversecomics.client.store.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.LargeTest;

import com.iversecomics.client.OurAsserts;
import com.iversecomics.client.store.ui.adapters.PublisherListAdapter;

public class ComicStoreActivity_PublishersSectionTest extends AbstractComicStoreActivityTestCase {
    public ComicStoreActivity_PublishersSectionTest() {
        super();

        Intent intent = new Intent();
        intent.putExtra("testing", true);
        intent.putExtra(ComicStoreActivity.BUNDLE_ACTIVE_PAGE, PublisherListFragment.TAG);
        setActivityIntent(intent);
    }

    @LargeTest
    public void testOnResumeLoadListFromServer() throws Throwable {
        final ComicStoreActivity activity = getActivity();
        assertNotNull("Activity should have launched", activity);

        waitForRefreshTasks(activity);

        Fragment fragment = activity.getActiveFragment();
        MoreAsserts.assertAssignableFrom(PublisherListFragment.class, fragment);

        PublisherListFragment publishersFrag = (PublisherListFragment) fragment;
        PublisherListAdapter listadapter = publishersFrag.getAdapter();
        assertNotNull("PublisherListAdapter should not be null", listadapter);

        refresh(listadapter);

        OurAsserts.assertAtLeast("PublisherListAdapter.size", 5, listadapter.getCount());
    }
}
