package com.iversecomics.client.account;

public class StoreLocation {

    public String lat;
    public String lng;
    public String name;
    public String address;
    public String address2;
    public String city;
    public String state;
    public String zip;
    public String phone;

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public void setAddress2(final String address) {
        this.address2 = address;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public void setLat(final String lat) {
        this.lat = lat;
    }

    public void setLng(final String lng) {
        this.lng = lng;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}