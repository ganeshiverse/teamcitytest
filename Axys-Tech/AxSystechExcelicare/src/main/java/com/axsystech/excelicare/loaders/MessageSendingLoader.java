package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.MessageSendingResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by G ajaykumar on 08/07/2015.
 */
public class MessageSendingLoader extends DataAsyncTaskLibLoader<MessageSendingResponse> {

    private String postData;

    public MessageSendingLoader(Context context, String postData) {
        super(context);
        this.postData = postData;
    }

    @Override
    protected MessageSendingResponse performLoad() throws Exception {
        return ServerMethods.getInstance().sendMessage(postData);
    }
}
