package com.iversecomics.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.db.ComicStoreDatabaseHelper;
import com.iversecomics.client.store.tasks.ServerConfigTask;
import com.iversecomics.client.util.Time;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.ServerConfigErrorEvent;
import com.iversecomics.ui.main.MainActivity;
import com.iversecomics.util.Util;
import com.iversecomics.util.crashreporter.PostMortemReportExceptionHandler;
import com.squareup.otto.Subscribe;

import java.util.concurrent.Future;

/**
 */
public class SplaschScreenActivity extends Activity {
    private final static Logger LOG = LoggerFactory.getLogger(SplaschScreenActivity.class);
    static InitAppAsyncTask initAppTask;
    final Handler handler = new Handler();
    ComicApp comicApp;
    boolean resumed = false;
    boolean offlineOneTimeFlag = true;
    // Crash Reporter- asks user to send the crash report, if the application crashes.
    protected PostMortemReportExceptionHandler mDamageReport = new PostMortemReportExceptionHandler(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        //TapjoyConnect.requestTapjoyConnect(this, AppConstants.TAPJOY_APPID, AppConstants.TAPJOY_SECRET);
        if(AppConstants.ENABLE_CRASH_REPORT) {
            mDamageReport.initialize();
        }
        // This is used to fet the SHA keys for Facebook
        Util.getSha1(SplaschScreenActivity.this);
        comicApp = (ComicApp) getApplication();
        if (savedInstanceState == null) {
            initAppTask = new InitAppAsyncTask(this);
            initAppTask.execute();
        } else {
            resumed = true;
        }
    }

    @Override
    protected void onDestroy() {
        mDamageReport.restoreOriginalHandler();
        mDamageReport=null;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OttoBusProvider.getInstance().register(this);
        if(AppConstants.ENABLE_CRASH_REPORT) {
            mDamageReport.initialize();
        }

    }

    /**
     * We do  not want to recreate this on screen rotation yet.
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        OttoBusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void serverConfigAvailable(ServerConfig config) {
        if (config.isConfigured() && initAppTask != null && initAppTask.getStatus() == AsyncTask.Status.FINISHED) {
            if (!isAppFresh()) {
                resetDbCache();
            }
            startMainActivity();
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Subscribe
    public void serverConfigError(ServerConfigErrorEvent errorEvent) {

        /**
         * offlineOneTimeFlag - to call the "Main activity" only once. even though ServerConfigErrorEvent called multiple times.
         */
        if(offlineOneTimeFlag) {
            startMainActivity();
            offlineOneTimeFlag= false;
        }
        /**
         * Commented for Offline Functionality.
         *
         */

//        if (ServerConfig.getDefault().isConfigured() && ConnectionHelper.isConnectedToInternet(this)) {
//            startMainActivity();
//        } else {
//            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            String errorMessage = ConnectionHelper.isConnectedToInternet(this) ? errorEvent.getErrorMessage() : getString(R.string.error_try_soon);
//            builder.setTitle(com.iversecomics.archie.android.R.string.error_title)
//                    .setMessage(errorMessage)
//                    .setCancelable(true)
//                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface arg0, int arg1) {
//                            finish();
//                        }
//                    })
//                    .create().show();
//        }
    }

    private boolean isAppFresh() {
        Freshness freshness = comicApp.getFreshness();
        return freshness.isFresh(ComicApp.FRESHNESS_ID);
    }

    private void resetDbCache() {
        // check to see if the database cache is still fresh
        // will stay fresh for one week
        LOG.warn("Database cache freshness expired. Clearing out the cache");
        // Clear out the database cache only
        ComicStoreDatabaseHelper helper = ComicStoreDatabaseHelper.getInstance(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        helper.rebuild(db); // force rebuild
        Freshness freshness = comicApp.getFreshness();
        freshness.setExpiresOn(ComicApp.FRESHNESS_ID, System.currentTimeMillis() + Time.WEEK);
    }


    private class InitAppAsyncTask extends AsyncTask<Object, Integer, Integer> {

        private final Context context;
        ComicApp comicApp;

        private InitAppAsyncTask(Activity activity) {
            this.context = activity.getApplicationContext();
            comicApp = ComicApp.getInstance();
        }

        @Override
        protected Integer doInBackground(Object... params) {
            checkServerConfigAvailability();
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
        }


        private void checkServerConfigAvailability() {
            // Make sure we have a fresh ServerConfig object
            final ServerConfigTask task = new ServerConfigTask(context, ComicApp.getInstance().getComicStore());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Future<?> future = ComicApp.getInstance().getTaskPool().submit(task);
                }
            });
        }
    }
}
