package com.babylon.database;

import com.babylon.database.schema.CheckResultSchema;
import com.babylon.database.schema.CommentSchema;
import com.babylon.database.schema.FileEntrySchema;
import com.babylon.database.schema.FoodSchema;
import com.babylon.database.schema.MessageSchema;
import com.babylon.database.schema.MonitorCategorySchema;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.database.schema.MonitorMeReportSchema;
import com.babylon.database.schema.PaymentCardScheme;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.database.schema.StressQuizSchema;

public enum Table implements ITable {
    MESSAGE("message", MessageSchema.class),
    FILE_ENTRY("file_entry", FileEntrySchema.class),
    COMMENT("comment", CommentSchema.class),
    MONITOR_ME_PARAM("monitor_me_param", MonitorMeParamSchema.class),
    PHYSICAL_ACTIVITY("physical_activity", PhysicalActivitySchema.class),
    FOOD("food", FoodSchema.class),
    MONITOR_ME("monitor_me", MonitorMeReportSchema.class),
    STRESS_QUIZ("stress_quiz", StressQuizSchema.class),
    MONITOR_CATEGORY("monitor_category", MonitorCategorySchema.class),
    CHECK_RESULT("check_result", CheckResultSchema.class),
    CREDIT_CARD("credit_card", PaymentCardScheme.class);

    private String name;
    private Class schema;

    private Table(String name, Class schema) {
        this.name = name;
        this.schema = schema;
    }

    public String getName() {
        return name;
    }

    public Class getSchema() {
        return schema;
    }

    public String toString() {
        return name;
    }
}
