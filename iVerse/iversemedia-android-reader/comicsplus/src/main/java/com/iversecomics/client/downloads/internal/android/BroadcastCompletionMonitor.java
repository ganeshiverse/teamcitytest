package com.iversecomics.client.downloads.internal.android;

import android.content.Context;
import android.net.Uri;

import com.iversecomics.client.downloads.internal.DownloadException;
import com.iversecomics.client.downloads.internal.DownloadFailedException;
import com.iversecomics.client.downloads.internal.data.DownloadData;
import com.iversecomics.client.downloads.internal.data.DownloadStatus;
import com.iversecomics.client.downloads.internal.net.DownloadMonitor;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.DownloadCompleteEvent;
import com.iversecomics.otto.event.DownloadProgressChangeEvent;

public class BroadcastCompletionMonitor implements DownloadMonitor {
    private Context context;
    private long id;
    private String filename;
    private String refContentUri;
    private long totalBytes;

    public BroadcastCompletionMonitor(Context context, DownloadData download) {
        this.context = context;
        this.id = download.getId();
        if (download.getLocalFile() != null) {
            this.filename = download.getLocalFile().getAbsolutePath();
        }
        if (download.getReferenceContentUri() != null) {
            this.refContentUri = download.getReferenceContentUri().toString();
        }
    }

    @Override
    public void downloadConnected(Uri uri, String title, long totalBytes, String etag) {
        this.totalBytes = totalBytes;
    }

    @Override
    public void downloadFailure(Uri uri, DownloadException failure) {
        if (failure instanceof DownloadFailedException) {
            // Notify of fatal failure.
            sendCompleteBroadcast(failure.getStatus(), failure.getMessage());
        }
    }

    @Override
    public void downloadProgress(Uri uri, long currentByte/*, long totalBytes*/) {
//        final Intent intent = new Intent(DownloaderConstants.ACTION_DOWNLOAD_PROGRESS);
//        intent.putExtra("uri", uri.toString());
//        intent.putExtra("currentByte", currentByte);
//        intent.putExtra("totalBytes", totalBytes);
//        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        DownloadProgressChangeEvent event = new DownloadProgressChangeEvent();
        event.setUri(uri.toString());
        event.setCurrentByte(currentByte);
        event.setTotalBytes(totalBytes);
        OttoBusProvider.getInstance().post(event);
    }

    @Override
    public void downloadState(Uri uri, DownloadStatus state, String message) {
        if (state.isSuccess()) {
            sendCompleteBroadcast(state, message);
        }
    }

    private void sendCompleteBroadcast(DownloadStatus state, String message) {
//todo        Intent intent = new Intent(DownloaderConstants.ACTION_DOWNLOAD_COMPLETE);
//        intent.putExtra("status", state.toString());
//        intent.putExtra("reason", message);
//        intent.putExtra("id", id);
//        intent.putExtra("filename", filename);
//        intent.putExtra("referenceContentUri", refContentUri);
//        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        DownloadCompleteEvent event = new DownloadCompleteEvent();
        event.setId(id);
        event.setStatus(state.toString());
        event.setReason(message);
        event.setFilename(filename);
        event.setReferenceContentUri(refContentUri);
        OttoBusProvider.getInstance().post(event);
    }
}
