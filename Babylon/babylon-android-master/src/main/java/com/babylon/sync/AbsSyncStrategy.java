package com.babylon.sync;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.babylon.database.Table;
import com.babylon.database.schema.MessageSchema;

public abstract class AbsSyncStrategy {
    private static final String TAG = AbsSyncStrategy.class.getSimpleName();

    abstract public void push(ContentResolver contentResolver, Bundle bundle);

    abstract public void pull(ContentResolver contentResolver, Bundle bundle);

    public static Cursor getUpdateCursor(ContentResolver contentResolver, Table table, Uri uri,
                                     String[] projection) {
        String selection = TextUtils.concat(table.getName(),
                ".",
                MessageSchema.Columns.SYNC_WITH_SERVER.getName(),
                "!=?").toString();
        String[] selectionArgs = {String.valueOf(SyncUtils.SYNC_TYPE_ADDED)};
        return contentResolver.query(uri, projection, selection, selectionArgs, null);
    }

}
