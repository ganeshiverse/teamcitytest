package com.iversecomics.client.comic.viewer;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public abstract class AbstractComicDialog extends Dialog {
    private boolean canceled = false;
    private final Thread uiThread;
    private final Handler handler = new Handler();

    public AbstractComicDialog(final Context context) {
        // super(context, R.style.Theme_Light_Dialog);
        super(context);
        this.uiThread = Thread.currentThread();
    }

    public SharedPreferences getSharedPreferences(String name, int mode) {
        return getContext().getSharedPreferences(name, mode);
    }

    @Override
    public void cancel() {
        super.cancel();
        setCanceled(true);
    }

    public Resources getResources() {
        return getContext().getResources();
    }

    public boolean isCanceled() {
        return canceled;
    }

    protected boolean isUiThread() {
        return (Thread.currentThread() == uiThread);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    protected void runOnUiThread(final Runnable action) {
        if (isUiThread()) {
            action.run();
        } else {
            handler.post(action);
        }
    }

    public void setCanceled(final boolean canceled) {
        this.canceled = canceled;
    }

}
