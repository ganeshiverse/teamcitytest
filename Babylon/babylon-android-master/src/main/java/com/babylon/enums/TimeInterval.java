package com.babylon.enums;

public enum TimeInterval {
    ONE_WEEK("0"),
    ONE_MONTH("1"),
    ONE_YEAR("2");

    private String id;

    TimeInterval(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
