package com.iversecomics.otto.event;

import com.iversecomics.client.store.model.FeaturedSlot;

import java.util.List;

/**
 */
public class FeaturedSlots {

    List<FeaturedSlot> slots;

    public FeaturedSlots(List<FeaturedSlot> comics) {
        this.slots = comics;
    }

    public List<FeaturedSlot> getSlots() {
        return slots;
    }
}
