package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.Urls;

public class ValidicSubscribeUrlGet extends AbsRequestGet<ValidicSubscribeUrlGet.SubscribeUrl> {

    @Override
    protected String getUrl() {
        return Urls.get(Urls.VALIDIC_SUBSCRIBE_URL_GET);
    }

    public static class SubscribeUrl {
        private String url;

        public String getUrl() {
            return url;
        }
    }

}
