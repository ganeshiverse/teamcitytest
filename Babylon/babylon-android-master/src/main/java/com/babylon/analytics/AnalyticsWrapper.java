package com.babylon.analytics;

import com.babylon.App;
import com.babylon.Config;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class AnalyticsWrapper {

    public static final class Events {
        public static final String APP_OPEN_EVENT = "App Open";
        public static final String LOGIN_EVENT = "Login";
        public static final String QUESTION_ASKED_EVENT = "Question Asked";
        public static final String ANSWER_READ_EVENT = "Answer Read";
        public static final String REGISTRATION_STARTED_EVENT = "Registration Started";
        public static final String REGISTRATION_COMPLETED_EVENT = "Registration Completed";
        public static final String ASK_OPEN_EVENT = "Ask Open";
        public static final String ON_FACEBOOK_REGISTERED = "onFacebookRegistered";
        public static final String ON_BABYLON_REGISTERED = "onBabylonRegistered";
        public static final String ON_ASK_CLICK = "onAskClick";
        public static final String ON_MONITOR_CLICK = "onMonitorClick";
        public static final String ON_CONSULTANT_CLICK = "onConsultantClick";
        public static final String ON_RECORDS_CLICK = "onRecordsClick";
        public static final String ON_SHOP_CLICK = "onShopClick";
        public static final String ON_CHECK_CLICK = "onCheckClick";
        public static final String ON_SETTINGS_CLICK = "onSettingsClick";
        public static final String ON_NOTIFICATIONS_CLICK = "onNotificationsClick";
        public static final String OPENED_RECORD = "Opened Record";
        public static final String OPENED_SETTINGS = "Opened Settings";
        public static final String REFERRAL_RECEIVED = "Referral Received";
        public static final String REFERRAL_OPENED = "Referral Opened";
        public static final String PRESCRIPTION_RECEIVED = "Prescription Received";
        public static final String PRESCRIPTION_OPENED = "Prescription Opened";
        public static final String APPOINTMENT_TRIED = "Appointment Tried";
        public static final String APPOINTMENT_BOOKED = "Appointment Booked";
    }

    private static AnalyticsWrapper instance;

    private MixpanelAPI mixpanelAPI;

    public static synchronized AnalyticsWrapper getInstance() {
        if (instance == null) {
            instance = new AnalyticsWrapper();
            instance.initialise();
        }
        return instance;
    }

    public static void sendEvent(String eventName) {
        getInstance().mixpanelAPI.track(eventName, null);
    }

    public static void flush() {
        getInstance().mixpanelAPI.flush();
    }

    private void initialise() {
        mixpanelAPI = MixpanelAPI.getInstance(App.getInstance(), Config.MIXPANEL_TOKEN);
    }

}
