package com.iversecomics.client.ui.widget;

import android.content.Context;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.VideoView;

import com.iversecomics.client.R;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class SlotView extends ComicStoreImageView {
    private static final Logger LOG = LoggerFactory.getLogger(SlotView.class);
    private CoverSize           slotSize;
    private VideoView videoView = null;
    private Context context;
    
    public SlotView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public SlotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public SlotView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init() {
    	setImageResource(R.drawable.cover_placeholder);
        
        NinePatchDrawable npd = (NinePatchDrawable)context.getResources().getDrawable(R.drawable.comicborder);
        this.setBackgroundDrawable(npd);
        int padding = ComicStoreImageView.dpToPx(PADDING_DP, getContext());
        this.setPadding(padding, padding, padding, padding);
    }

    public void setSlot(FeaturedSlot slot, BitmapManager bitmapManager) {
        LOG.debug("setSlot(%s, bitmapManager)", slot.getSlotID());
        
        if(slot.getSlotType() == FeaturedSlot.FeaturedSlotType.FeaturedSlotTypeMovie)
        {
        	//videoView = new VideoView(context);
        }
        else if(slot.getImageURL() != null)
        {
        	Uri serverUri = Uri.parse(slot.getImageURL());
        	setImageViaUri(serverUri, bitmapManager);
        }
    }
}
