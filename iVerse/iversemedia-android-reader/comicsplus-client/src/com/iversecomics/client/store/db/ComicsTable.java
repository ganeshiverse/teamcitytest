package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.client.store.model.Comic;

public class ComicsTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri    CONTENT_URI            = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "comics");

    public static final Uri    CONTENT_URI_BY_COMICID = Uri.withAppendedPath(CONTENT_URI, "byid");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of my comics.
     */
    public static final String CONTENT_TYPE           = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                              + "/vnd.iversecomics.store.comics";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single user.
     */
    public static final String CONTENT_ITEM_TYPE      = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                              + "/vnd.iversecomics.store.comics";

    public static final String DEFAULT_SORT_ORDER     = "name ASC";
    public static final String TABLE                  = "comics";

    public static final String COMICID                = "comicId";
    public static final String NAME                   = "name";
    public static final String COMIC_BUNDLE_NAME      = "comicBundleName";
    public static final String AVG_RATING             = "avgRating";
    public static final String DESCRIPTION            = "description";
    public static final String IMAGE_FILE_NAME        = "imageFileName";
    public static final String LEGAL                  = "legal";
    public static final String NUMBER                 = "number";
    public static final String AMOUNT_USD             = "amountUSD";
    public static final String PRICING_TIER           = "pricingTier";
    public static final String PRODUCT_TYPE           = "productType";
    public static final String RAW_NAME               = "rawName";
    public static final String SERIES_BUNDLE_NAME     = "seriesBundleName";
    // TODO: remove? probably not needed in android side (iOS only?)
    public static final String SKU                    = "sku";
    public static final String PUBLISHERID            = "publisherId";
    public static final String PUBLISHER_LEGACYID     = "publisherLegacyId";
    public static final String PUBLISHER_NAME         = "publisherName";
    public static final String ASSET_TYPES            = "assetTypes";
    
    public static final String SUBSCRIPTION			  = "subscription";
    public static final String TAPJOY				  = "tapjoy";

    public static final String PROJECTION_FULL[]      = new String[] {
            TABLE + '.' + _ID,
            TABLE + '.' + COMICID,
            NAME,
            COMIC_BUNDLE_NAME,
            AVG_RATING,
            DESCRIPTION,
            IMAGE_FILE_NAME,
            LEGAL,
            NUMBER,
            PRICING_TIER,
            AMOUNT_USD,
            RAW_NAME,
            SERIES_BUNDLE_NAME,
            SKU,
            PRODUCT_TYPE,
            PUBLISHERID,
            PUBLISHER_LEGACYID,
            PUBLISHER_NAME,
            ASSET_TYPES,
            SUBSCRIPTION,
            TAPJOY
                                                      };

    public static final String PROJECTION_BRIEF[]     = new String[] {
            TABLE + '.' + _ID,
            TABLE + '.' + COMICID,
            COMIC_BUNDLE_NAME,
            NAME,
            RAW_NAME,
            AVG_RATING,
            NUMBER,
            IMAGE_FILE_NAME,
            PRODUCT_TYPE,
            PRICING_TIER,
            AMOUNT_USD,
            PUBLISHER_NAME,
            SUBSCRIPTION,
            TAPJOY
                                                      };

    public static ContentValues asValues(Comic comic) {
        ContentValues values = new ContentValues();
        if (comic.getDbId() >= 0) {
            values.put(_ID, comic.getDbId());
        }

        values.put(AMOUNT_USD, comic.getAmountUSD());
        values.put(COMICID, comic.getComicId());
        values.put(NAME, comic.getName());
        values.put(COMIC_BUNDLE_NAME, comic.getComicBundleName());
        values.put(AVG_RATING, comic.getAvgRating());
        values.put(DESCRIPTION, comic.getDescription());
        values.put(IMAGE_FILE_NAME, comic.getImageFileName());
        values.put(LEGAL, comic.getLegal());
        values.put(NUMBER, comic.getNumber());
        values.put(PRICING_TIER, comic.getPricingTier());
        values.put(PRODUCT_TYPE, comic.getProductType());
        values.put(RAW_NAME, comic.getRawName());
        values.put(SERIES_BUNDLE_NAME, comic.getSeriesBundleName());
        values.put(SKU, comic.getSku());
        values.put(PUBLISHERID, comic.getPublisherId());
        values.put(PUBLISHER_LEGACYID, comic.getPublisherLegacyId());
        values.put(PUBLISHER_NAME, comic.getPublisherName());
        values.put(SUBSCRIPTION, comic.getSubscription() ? 1 : 0);
        values.put(TAPJOY, comic.isTapjoy() ? 1 : 0);
        setStringList(values, ASSET_TYPES, comic.getAssetTypes());

        return values;
    }

    public static Comic fromCursor(Cursor c) {
        Comic comic = new Comic();

        comic.setDbId(getLong(c, _ID));
        comic.setComicId(getString(c, COMICID));
        comic.setName(getString(c, NAME));
        
        comic.setAmountUSD(getOptionalString(c, AMOUNT_USD));
        comic.setComicBundleName(getOptionalString(c, COMIC_BUNDLE_NAME));
        comic.setAvgRating(getDouble(c, AVG_RATING));
        comic.setDescription(getOptionalString(c, DESCRIPTION));
        comic.setImageFileName(getString(c, IMAGE_FILE_NAME));
        comic.setLegal(getOptionalString(c, LEGAL));
        comic.setNumber(getInteger(c, NUMBER));
        comic.setPricingTier(getInteger(c, PRICING_TIER));
        comic.setProductType(getString(c, PRODUCT_TYPE));
        comic.setRawName(getString(c, RAW_NAME));
        comic.setSeriesBundleName(getOptionalString(c, SERIES_BUNDLE_NAME));
        comic.setSku(getOptionalString(c, SKU));
        comic.setPublisherId(getOptionalString(c, PUBLISHERID));
        comic.setPublisherLegacyId(getOptionalString(c, PUBLISHER_LEGACYID));
        comic.setPublisherName(getOptionalString(c, PUBLISHER_NAME));
        comic.setSubscription(getInteger(c, SUBSCRIPTION) == 1 ? true : false);
        comic.setTapjoy(getInteger(c, TAPJOY) == 1 ? true : false);
        
        comic.setAssetTypes(getStringList(c, ASSET_TYPES));

        return comic;
    }
}
