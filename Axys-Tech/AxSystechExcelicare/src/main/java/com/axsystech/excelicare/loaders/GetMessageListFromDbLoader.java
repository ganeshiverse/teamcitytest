package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.db.models.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by SomeswarReddy on 9/10/2015.
 */
public class GetMessageListFromDbLoader extends DataAsyncTaskLibLoader<List<MessageListItemDBModel>> {

    private String panelId;
    private String ecUserId;

    public GetMessageListFromDbLoader(final Context context, String panelId, long ecUserId) {
        super(context, true);
        this.panelId = panelId;
        this.ecUserId = ecUserId+"";
    }

    @Override
    protected List<MessageListItemDBModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<MessageListItemDBModel, Long> messageListItemDBModelDao = helper.getDao(MessageListItemDBModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<MessageListItemDBModel>>() {
                @Override
                public List<MessageListItemDBModel> call() throws Exception {

                    return messageListItemDBModelDao.queryBuilder().orderBy(MessageListItemDBModel.MSG_RECEIVED_DATE,true).where().eq(MessageListItemDBModel.PANEL_ID, panelId).and().
                            eq(MessageListItemDBModel.EC_USER_ID, ecUserId).query();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
