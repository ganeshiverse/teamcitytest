package com.iversecomics.otto.event;

/**
 */
public class DownloadCompleteEvent {
    private long id;
    private String status;
    private String reason;
    private String filename;
    private String referenceContentUri;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public void setReferenceContentUri(String referenceContentUri) {
        this.referenceContentUri = referenceContentUri;
    }

    public String getReferenceContentUri() {
        return referenceContentUri;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DownloadCompleteEvent{");
        sb.append("id=").append(id);
        sb.append(", status='").append(status).append('\'');
        sb.append(", reason='").append(reason).append('\'');
        sb.append(", filename='").append(filename).append('\'');
        sb.append(", referenceContentUri='").append(referenceContentUri).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
