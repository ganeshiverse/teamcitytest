package com.iversecomics.client.store.tasks;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchComicTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(FetchComicTask.class);
    private final String comicBundleName;
    private final String freshnessKey;

    public FetchComicTask(ComicStore comicStore, String comicBundleName) {
        super(comicStore);
        this.comicBundleName = comicBundleName;
        this.freshnessKey = "single-comic_" + comicBundleName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchComicTask other = (FetchComicTask) obj;
        if (freshnessKey == null) {
            if (other.freshnessKey != null) {
                return false;
            }
        } else if (!freshnessKey.equals(other.freshnessKey)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        LOG.debug("execTask() - comicBundleName=%s", comicBundleName);
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getComic(comicBundleName);
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            ResponseParser parser = new ResponseParser();
            parser.parseComics(json, updater);
            server.updateFreshness(freshnessKey, Time.HOUR * 3);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update comic: " + comicBundleName);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((freshnessKey == null) ? 0 : freshnessKey.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(freshnessKey);
    }
}
