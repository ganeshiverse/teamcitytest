package com.axsystech.excelicare.framework.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.axsystech.excelicare.data.responses.GetDashBoardCardsResponse;
import com.mig35.injectorlib.utils.inject.InjectSavedState;


/**
 * Date: 17.06.14
 * Time: 22:31
 *
 * @author SomeswarReddy
 */
public class ConfirmationDialogForCardsFragment extends BaseDialogFragment {

    public static final String MESSAGE_KEY = "com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment.MESSAGE_KEY";
    public static final String DIALOG_ACTION_KEY = "com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment.DIALOG_ACTION_KEY";
    public static final String DIALOG_CARD_INDEX_KEY = "com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment.DIALOG_CARD_INDEX_KEY";
    public static final String DIALOG_CONTROL_OBJ_KEY = "com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment.DIALOG_CONTROL_OBJ_KEY";
    public static final String DIALOG_ID_KEY = "com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment.DIALOG_ID_KEY";
    public static final String SHOULD_DISPLAY_CANCEL_KEY = "com.axsystech.excelicare.framework.ui.dialogs.ConfirmationDialogFragment.SHOULD_DISPLAY_CANCEL_KEY";

    public interface OnConfirmListener {
        void onConfirm(int dialogId, int cardIndex, String action, GetDashBoardCardsResponse.Control controlObj);

        void onCancel(int dialogId);
    }

    @InjectSavedState
    private String mMessage;

    @InjectSavedState
    private int mDialogId;

    @InjectSavedState
    private boolean mShouldDisplayCancelButton;

    @InjectSavedState
    private int cardIndex;

    @InjectSavedState
    private String action;

    private GetDashBoardCardsResponse.Control controlObj;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null == getArguments() || TextUtils.isEmpty(getArguments().getString(MESSAGE_KEY))) {
            dismiss();
        } else {
            mMessage = getArguments().getString(MESSAGE_KEY);
        }

        if (getArguments() != null && getArguments().containsKey(DIALOG_ID_KEY)) {
            mDialogId = getArguments().getInt(DIALOG_ID_KEY);
        }

        if (getArguments() != null && getArguments().containsKey(SHOULD_DISPLAY_CANCEL_KEY)) {
            mShouldDisplayCancelButton = getArguments().getBoolean(SHOULD_DISPLAY_CANCEL_KEY, false);
        }

        if (getArguments() != null && getArguments().containsKey(DIALOG_CARD_INDEX_KEY)) {
            cardIndex = getArguments().getInt(DIALOG_CARD_INDEX_KEY);
        }

        if (getArguments() != null && getArguments().containsKey(DIALOG_ACTION_KEY)) {
            action = getArguments().getString(DIALOG_ACTION_KEY, "DeleteNavigate");
        }

        if (getArguments() != null && getArguments().containsKey(DIALOG_CONTROL_OBJ_KEY)) {
            controlObj = (GetDashBoardCardsResponse.Control) getArguments().getSerializable(DIALOG_CONTROL_OBJ_KEY);
        }
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        setCancelable(true);

        if (null != mMessage) {
            builder.setMessage(mMessage);
        }
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                final Fragment targetFragment = getTargetFragment();
                final Activity activity = getActivity();
                OnConfirmListener onConfirmListener = null;
                if (targetFragment instanceof OnConfirmListener) {
                    onConfirmListener = (OnConfirmListener) targetFragment;
                } else if (activity instanceof OnConfirmListener) {
                    onConfirmListener = (OnConfirmListener) activity;
                }

                if (onConfirmListener != null) {
                    onConfirmListener.onConfirm(mDialogId, cardIndex, action, controlObj);
                }
            }
        });

        if (mShouldDisplayCancelButton) {
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    final Fragment targetFragment = getTargetFragment();
                    final Activity activity = getActivity();
                    OnConfirmListener onConfirmListener = null;
                    if (targetFragment instanceof OnConfirmListener) {
                        onConfirmListener = (OnConfirmListener) targetFragment;
                    } else if (activity instanceof OnConfirmListener) {
                        onConfirmListener = (OnConfirmListener) activity;
                    }

                    if (onConfirmListener != null) {
                        onConfirmListener.onCancel(mDialogId);
                    }
                }
            });
        }

        return builder.create();
    }

}