package com.iversecomics.client.store.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.AbstractStubActivityTestCase;

public class BatchInsertsTest extends AbstractStubActivityTestCase {
    private void assertBatchInsertCount(String msg, int expectedCount) {
        startStubActivity();
        SQLiteDatabase db = getEmptyDB();
        BatchInserts batch = new BatchInserts(db);
        for (int i = 0; i < expectedCount; i++) {
            ContentValues values = new ContentValues();
            values.put(GenresTable.GENREID, "genre." + i);
            values.put(GenresTable.NAME, "Genre #" + i);
            batch.insert(GenresTable.TABLE, values);
        }
        batch.end();
        assertFalse("DB should not be locked by current thread", db.isDbLockedByCurrentThread());
        assertFalse("DB should not be locked by other threads", db.isDbLockedByOtherThreads());
        assertRowCount(msg, db, GenresTable.TABLE, expectedCount);
    }

    private void assertRowCount(String type, SQLiteDatabase db, String tableName, int expectedCount) {
        assertEquals(type + " - row count in table=" + tableName, expectedCount, getRowCount(db, tableName));
    }

    protected SQLiteDatabase getEmptyDB() {
        ComicStoreDatabaseHelper helper = ComicStoreDatabaseHelper.getInstance(getActivity());
        SQLiteDatabase db = helper.getWritableDatabase();
        helper.rebuild(db); // force rebuild
        return db;
    }

    @MediumTest
    public void testInsert0001() {
        int expectedCount = 1;
        assertBatchInsertCount("1 item", expectedCount);
    }

    @MediumTest
    public void testInsert0005() {
        int expectedCount = 5;
        assertBatchInsertCount("5 items", expectedCount);
    }

    @MediumTest
    public void testInsert0050() {
        int expectedCount = 50;
        assertBatchInsertCount("50 items", expectedCount);
    }

    @MediumTest
    public void testInsert0500() {
        int expectedCount = 500;
        assertBatchInsertCount("500 items", expectedCount);
    }

    @MediumTest
    public void testInsert3000() {
        int expectedCount = 3000;
        assertBatchInsertCount("3000 items", expectedCount);
    }
}
