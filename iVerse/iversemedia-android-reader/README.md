# iVerse ComicsPlus


## Develop
This project is developed using Android Studio.


## Build
Builds are done using Gradle.

    ./gradlew clean build

There are several build variants, to see all available build tasks use:

    ./gradlew tasks

## Build Variants
There are 3 flavor groups and 4 product flavors used to produce builds for the Samsung App Store and Google Play for both the Archie ComicsPlus and OVNI brands.
  See comicsplus/build.gradle

The only difference between the Samsung and Google variants is in which store facilitates In-App Purchases.

Other than branding, the primary difference between the Archie and ComicsPlus variants is in the data shown in the side menu.  The fourth section of the side menu lists comic titles in the Archie Variant. In the ComicsPlus variant it lists publisher names.


## Developer Orientation
Singletons are typically accessed via IverseApplication.
The most important one is *ComicStore*, it provides access to the local sqlite databases.

### ServerConfig
Most configuration data is fetched from the server and stored in ServerConfig.

### AppConstants
Constants that differ among build flavors are stored in AppConstants.

### Local SQLite Databases and Content Providers
SQLite Databases are created by *ComicStoreDatabaseHelper.java*

Tables column names are defined in the following classes:

- ComicsTable
- FeaturedSlotTable
- FreshnessTable
- GenresTable
- GroupsTable
- OrderingTable
- PromotionTable
- ServerConfigTable

### ComicBundle
Not to be confused with the standard Android notion of a Bundle.  ComicBundle is a proprietary format for storing comic images and meta data.

### On Demand / Unlimited
On Demand or Unlimited comics require a subscription.  In contrast to ComicBundles and  Previews, the images(pages) for these comics are downloaded from the server one page at a time as needed.  

### Image URIs
Images URIs are typically constructed dynamically based upon the server fileName and the size needed.  Images are stored on S3, for example:
    http://s3.amazonaws.com/iverse_public/store/publisher/logo/publisher_sixtyeight.png
See *ServerConfig.java* for fileName to URI getters
#### CoverSize.java
    ServerConfig.getDefault().getiPadLargeImageUri(fileName);
    ServerConfig.getDefault().getLargeImageUri(fileName);
    ServerConfig.getDefault().getSmallImageUri(fileName);
    ServerConfig.getDefault().getiPadMediumImageUri(fileName);
    ServerConfig.getDefault().getMediumImageUri(fileName);
#### Publisher Image
	ServerConfig.getDefault().getPublisherLogoImageUri(fileName);

### Web Services
JSON Parsing: ResponseParser.java


