package com.babylon.model;

import com.babylon.utils.GsonWrapper;

public class Wsse {
    public final String role = "ROLE_PATIENT";
    private String id;
    private String token;

    public Wsse(String id, String token) {
        this.id = id;
        this.token = token;
    }

    @Override
    public String toString() {
        return GsonWrapper.getGson().toJson(this);
    }

    public String getToken() {
        return token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
