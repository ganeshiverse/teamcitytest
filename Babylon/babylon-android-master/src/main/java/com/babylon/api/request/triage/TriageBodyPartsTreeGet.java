package com.babylon.api.request.triage;

import android.text.TextUtils;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseTree;
import com.babylon.api.request.base.Urls;
import com.babylon.enums.Gender;
import com.babylon.model.check.export.BodyPart;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class TriageBodyPartsTreeGet extends AbsRequestGet<ResponseTree<BodyPart>> {
    private static final String PARAM_GENDER = "gender";

    @Override
    protected String getUrl() {
        final String urlParams = getUrlParams(params);
        String url = Urls.get(Urls.TRIAGE_BODY_PARTS_TREE_GET);
        String endURL = TextUtils.concat(url, urlParams).toString();
        return endURL;
    }

    public static TriageBodyPartsTreeGet newCheckBodyPartsGet(Gender gender, ParentBodyPart parentId) {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        if (gender != null) {
            params.add(new BasicNameValuePair(PARAM_GENDER, String.valueOf(gender.ordinal())));
        }
        if (parentId != null) {
            if (parentId.parentId != -1) {
                params.add(new BasicNameValuePair(parentId.keyForApi, String.valueOf(parentId.parentId)));
            }
        }
        return (TriageBodyPartsTreeGet) new TriageBodyPartsTreeGet().withParams(params);
    }

    public static class ParentBodyPart {
        private final int parentId;
        private final String keyForApi = "parentId";

        public ParentBodyPart(int parentId) {
            this.parentId = parentId;
        }
    }

}