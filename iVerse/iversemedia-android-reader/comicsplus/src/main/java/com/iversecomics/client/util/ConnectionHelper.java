package com.iversecomics.client.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;

/**
 * Created by Gabriel on 10/29/13.
 */
public class ConnectionHelper {

    public static boolean isConnectedToInternet(Context context) {
        if(context !=null) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks weather internet connection is over wifi.
     *
     * @param context context
     * @return true if internet connected
     */
    private static boolean isConectedOverWifi(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
    }

    /**
     * Show a Toast to user when internet not available
     * "No Internet Connection"
     */

    public static void showNoInternetMessage() {
        Toast.makeText(IverseApplication.getApplication().getApplicationContext(),
                IverseApplication.getApplication().getApplicationContext().getResources().getString(R.string.info_no_network),
                Toast.LENGTH_LONG).show();
        return;
    }

    /**
     * This Describes the status of a network interface of a given type (currently either Mobile or Wifi).
     * @param state
     * @return stateString returns the state of the current Network.
     * */
    public static String getNetworkState(NetworkInfo.State state) {
        String stateString = "Unknown";
        switch (state) {
            case CONNECTED:
                stateString = "Connected";
                break;
            case CONNECTING:
                stateString = "Connecting";
                break;
            case DISCONNECTED:
                stateString = "Disconnected";
                break;
            case DISCONNECTING:
                stateString = "Disconnecting";
                break;
            case SUSPENDED:
                stateString = "Suspended";
                break;
            default:
                stateString = "Unknown";
                break;
        }
        return stateString;
    } //getNetworkStateString()
}
