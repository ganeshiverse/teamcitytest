package com.babylon.test;

import android.test.AndroidTestCase;
import com.babylon.R;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.enums.PaymentCardType;
import com.babylon.model.payments.PaymentCard;

public class PaymentCardViewControllerTestCase extends AndroidTestCase {

    public void testIsNewCreditCard() {
        PaymentCard paymentCard = new PaymentCardViewController().getNewCreditCard();
        assertFalse(paymentCard.isCreated());
    }

    public void testGetCardViewNumber() {
        PaymentCard paymentCard = new PaymentCard();
        paymentCard.setMaskedNumber("1111222233334444");

        String cardViewNumber = new PaymentCardViewController().getCardViewNumber(paymentCard);
        assertTrue(cardViewNumber.equals("3333 4444"));
    }

    public void testTransferCreditCardToNumber() {
        String creditCardNumberStr = "1111 2222 3333 4444";
        String creditCardNumber = new PaymentCardViewController().removeWhitespacesFromCreditCard(creditCardNumberStr);

        assertTrue(creditCardNumber != null);
        assertTrue(creditCardNumber.equals("1111222233334444"));
    }

    public void testVisaImage() {
        PaymentCardViewController paymentCardViewController = new PaymentCardViewController();
        int imageId = paymentCardViewController.findImageId(PaymentCardType.VISA_CARD_TYPE);

        assertTrue(imageId == R.drawable.ic_card_visa);
    }

    public void testMasterImage() {
        PaymentCardViewController paymentCardViewController = new PaymentCardViewController();
        int imageId = paymentCardViewController.findImageId(PaymentCardType.MASTER_CARD_TYPE);

        assertTrue(imageId == R.drawable.ic_card_mastercard);
    }

    public void testAmericanExpressImage() {
        PaymentCardViewController paymentCardViewController = new PaymentCardViewController();
        int imageId = paymentCardViewController.findImageId(PaymentCardType.AMERICA_EXPRESS_CARD_TYPE);

        assertTrue(imageId == R.drawable.ic_card_amex);
    }
}
