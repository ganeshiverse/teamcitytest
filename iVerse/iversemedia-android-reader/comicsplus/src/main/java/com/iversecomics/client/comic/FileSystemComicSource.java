package com.iversecomics.client.comic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iversecomics.client.util.PatternFileFilter;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileSystemComicSource implements IMyComicSourceAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(FileSystemComicSource.class);
    private File baseDir;
    private Map<String, URI> filemap;
    private int pageCount;
    private int panelCount;
    private ComicMode mode;
    private IComicSourceListener comicSourceListener;

    public FileSystemComicSource(File comicDir) {
        this.baseDir = comicDir;
        this.filemap = new HashMap<String, URI>();
        this.pageCount = 0;
        this.panelCount = 0;
        this.mode = ComicMode.PAGE;
        this.comicSourceListener = new LogComicSourceListener();
    }

    @Override
    public void close() throws IOException {
        /* nothing to do */
    }

    @Override
    public ComicMode getComicMode() {
        return mode;
    }

    @Override
    public int getImageCount() {
        switch (mode) {
            case PAGE:
                return pageCount;
            default:
            case PANEL:
                return panelCount;
        }
    }

    @Override
    public URI getImageURI(int index) {
        switch (mode) {
            case PAGE:
                return filemap.get("page-" + index);
            default:
            case PANEL:
                return filemap.get("panel-" + index);
        }
    }

    @Override
    public void open() throws IOException {
        filemap.clear();
        pageCount = 0;
        panelCount = 0;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        opts.inPurgeable = true;

        Pattern pat = Pattern.compile("^(Page|Panel)_([0-9]+)\\.png*", Pattern.CASE_INSENSITIVE);
        PatternFileFilter filefilter = new PatternFileFilter(pat);

        Matcher mat;
        String type, key;
        int index;
        for (File image : baseDir.listFiles(filefilter)) {
            mat = pat.matcher(image.getName());
            if (!mat.matches()) {
                continue; // skip, no match
            }
            try {
                type = mat.group(1);
                index = Integer.parseInt(mat.group(2));
                if ("Page".equalsIgnoreCase(type)) {
                    pageCount++;
                } else if ("Panel".equalsIgnoreCase(type)) {
                    panelCount++;
                }
                key = type.toLowerCase() + "-" + index;
                filemap.put(key, image.toURI());
                // Log.d(TAG, "Adding filemap entry [" + key + "] -> " + image);
            } catch (NumberFormatException e) {
                // Bad number format??
            }
        }
        LOG.debug("Found " + pageCount + " pages, " + panelCount + " panels");
        comicSourceListener.onComicSourceUpdated(this);
    }

    @Override
    public void setComicMode(ComicMode mode) {
        if (mode != this.mode) {
            this.mode = mode;
            comicSourceListener.onComicSourceUpdated(this);
        }
    }

    @Override
    public Bitmap loadComicPage(int index) {
        // NA ?
        return null;
    }


    @Override
    public void setComicSourceListener(IComicSourceListener listener) {
        this.comicSourceListener = listener;
        if (comicSourceListener == null) {
            comicSourceListener = new LogComicSourceListener();
        }
    }

    @Override
    public void onOpenFinished(boolean bSuccess) {
        // TODO Auto-generated method stub

    }
}
