package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;
import com.babylon.model.NutrientCategory;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.DateUtils;

public interface FoodSchema {

    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri BASE_CONTENT_URI = Uri.parse("content://" + DatabaseHelper.CONTENT_AUTHORITY);
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.FOOD.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.INT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        DATE("date", DBType.INT),
        TITLE("title", DBType.TEXT),
        CALORIES("calories", DBType.INT),
        PORTION_WEIGHT("portionWeight", DBType.FLOAT),
        ADD_PORTION_WEIGHT("addPortionWeight", DBType.FLOAT),
        PORTION_DETAILS("portionDetails", DBType.FLOAT),
        COUNT("count", DBType.INT),
        NUTRIENT_ID("nutrientId", DBType.INT),
        CATEGORY("category", DBType.TEXT);

        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public static class CategoriesQuery {

        public static Uri getByCategoryUri(long dateFrom) {
            long dateTo = DateUtils.getEndOfTheDay(dateFrom);
            return DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendEncodedPath("nutrient_categories")
                    .appendPath(String.valueOf(dateFrom))
                    .appendPath(String.valueOf(dateTo))
                    .build();
        }

        public static String getDateFrom(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static String getDateTo(Uri uri) {
            return uri.getPathSegments().get(2);
        }

        public static String getByCategorySelection(Uri uri) {
            final String selection = "select " + FoodSchema.Columns._ID.getName() + ", " +
                    getSumCaloriesByCategorySelection(NutrientCategory.BREAKFAST, uri) + "," +
                    getSumCaloriesByCategorySelection(NutrientCategory.LUNCH, uri) + "," +
                    getSumCaloriesByCategorySelection(NutrientCategory.DINNER, uri) + "," +
                    getSumCaloriesByCategorySelection(NutrientCategory.SNACKS, uri) +
                    " FROM " + Table.FOOD.getName() + " GROUP BY  " + FoodSchema.Columns.CALORIES.getName();

            return selection;
        }

        private static String getSumCaloriesByCategorySelection(NutrientCategory category, Uri uri) {
            final String daySelection = getDaySelection(uri, category);
            return "(select sum("
                    + FoodSchema.Columns.CALORIES.getName() +" * "
                    + Columns.COUNT.getName()
                    +") from " + Table.FOOD.getName() +
                    " where " + daySelection + ") as " + category.getTitle();
        }

        private static String getDaySelection(Uri uri, NutrientCategory category) {
            final String dateFrom = getDateFrom(uri);
            final String dateTo = getDateTo(uri);

            return Query.DAY_SELECTION_BY_CATEGORY.replaceFirst("[?]", dateFrom).replaceFirst("[?]", dateTo)
                    .replaceFirst("[?]", String.valueOf(category.getValue()));
        }
    }

    public final static class Query extends BaseQuery {
        public static final int CURSOR_LOAD_TODAY_FOOD = 6487;
        public static final int CURSOR_LOAD_TODAY_CATEGORY = 6488;

        public static final String DAY_SELECTION_BY_CATEGORY = Columns.DATE.getName() + ">= ? AND "
                + Columns.DATE.getName() + "< ? AND " + Columns.SYNC_WITH_SERVER + " != "
                + String.valueOf(SyncUtils.SYNC_TYPE_DELETE) + " AND " + Columns.CATEGORY.getName() + "= ?";

        public static final String SORT = Columns.TITLE.getName() + " ASC";
        public static final String SELECTION_SYNC_NEEDED = Columns.SYNC_WITH_SERVER.getName() + " != " + SyncUtils
                .SYNC_TYPE_ADD;

        public static final String SELECTION_DELETE  = SELECTION_SYNC_NEEDED + " AND " + Columns.ID.getName() + " NOT" +
                " IN ";

        public static final int _ID = 0;
        public static final int ID = 1;
        public static final int SYNC_WITH_SERVER = 2;
        public static final int DATE = 3;
        public static final int TITLE = 4;
        public static final int CALORIES = 5;
        public static final int PORTION_WEIGHT = 6;
        public static final int ADD_PORTION_WEIGHT = 7;
        public static final int PORTION_DETAILS = 8;
        public static final int COUNT = 9;
        public static final int NUTRIENT_ID = 10;
        public static final int CATEGORY = 11;
    }
}

