package com.iversecomics.client.refresh;

import android.os.AsyncTask;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class TaskExecutor extends AsyncTask<Task, String, Void> {
    private static final Logger LOG = LoggerFactory.getLogger(TaskExecutor.class);

    @Override
    protected Void doInBackground(Task... tasks) {
        int count = 0;
        for (Task task : tasks) {
            LOG.debug("Starting Task [%s]", task.getClass().getSimpleName());
            task.execTask();
            count++;
        }
        LOG.debug("%d tasks completed", count);
        return null;
    }
}
