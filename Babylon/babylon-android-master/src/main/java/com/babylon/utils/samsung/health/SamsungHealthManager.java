package com.babylon.utils.samsung.health;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.enums.SamsungHealthPermission;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.samsung.android.sdk.SsdkVendorCheck;
import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import com.samsung.android.sdk.healthdata.HealthDataService;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Singleton manager, which help initialize, authorize and sync samsung health data
 */
public final class SamsungHealthManager {

    public interface SamsungHealthListener {
        void onHealthStoreSuccessConnected();

        void onHealthStoreFailConnected(HealthConnectionErrorResult error);

        void onHealthStorePermissionFailed();

        void onRequestSynchronization(boolean isPermissionApproved);

        void onNoPermissionsSelected();

        void onDisconnected();
    }

    private static SamsungHealthManager instance;

    public static final String TAG = SamsungHealthManager.class.getSimpleName();
    private PreferencesManager preferencesManager = PreferencesManager.getInstance();
    public SamsungHealthListener activityHealthListener;
    private HealthDataStore store;
    private Map<String, BaseReporter> dataReporters = new HashMap<>();
    private boolean shouldAskPermissions;
    private Activity activity;

    private SamsungHealthManager() {
        //singleton
    }

    public static synchronized SamsungHealthManager getInstance() {
        if (instance == null) {
            instance = new SamsungHealthManager();

        }
        return instance;
    }

    public void onCreate(@NonNull Activity activity,
                         @NonNull SamsungHealthListener samsungHealthListener,
                         boolean shouldAskPermissions) {
        this.activity = activity;
        this.activityHealthListener = samsungHealthListener;
        this.shouldAskPermissions = shouldAskPermissions;


        HealthDataService healthDataService = new HealthDataService();
        try {
            healthDataService.initialize(activity);
        } catch (IllegalArgumentException e) {
            L.e(TAG, e.getMessage(), e);
        }

        store = new HealthDataStore(activity, connectionListener);
        try {
            store.connectService();
        } catch (IllegalStateException e) {
            L.e(TAG, e.getMessage(), e);
            connectionListener.onConnectionFailed(null);
        }
    }

    public void onDestroy() {
        for (BaseReporter reporter : dataReporters.values()) {
            reporter.stop();
        }
        store.disconnectService();
        activity = null;
    }

    private final HealthDataStore.ConnectionListener connectionListener =
            new HealthDataStore.ConnectionListener() {

                @Override
                public void onConnected() {
                    L.d(TAG, "Health data service is connected.");

                    HealthPermissionManager permissionManager = new HealthPermissionManager(store);

                    try {
                        // check whether the permissions that this application needs are acquired
                        Set<HealthPermissionManager.PermissionKey> permissionKeySet =
                                SamsungHealthPermission.getPermissionKeySet();
                        Map<HealthPermissionManager.PermissionKey, Boolean> permissionMap =
                                permissionManager.isPermissionAcquired(permissionKeySet);
                        
                        boolean isPermissionApproved = isEvenOnePermissionEnabled(permissionMap);

                        if (!shouldAskPermissions && !isPermissionApproved) {
                            activityHealthListener.onNoPermissionsSelected();
                        } else if (shouldAskPermissions) {
                            askPermissions();
                        } else if (isFirstConnection() && isPermissionApproved) {
                            setFirstConnection(false);
                            activityHealthListener.onRequestSynchronization(false);
                            Toast.makeText(activity, R.string.samsung_health_success, Toast.LENGTH_LONG).show();
                        } else if (isPermissionApproved) {
                            startSynchronization(permissionMap);
                        } else {
                            store.disconnectService();
                        }

                        setActivated(isPermissionApproved);

                    } catch (Exception e) {
                        L.e(TAG, e.getClass().getName() + " - " + e.getMessage(), e);
                    }
                }

                @Override
                public void onConnectionFailed(HealthConnectionErrorResult error) {
                    if (error != null) {
                        L.d(TAG, "Error code: " + error.getErrorCode());
                    }

                    L.d(TAG, "Health data service is not available.");

                    activityHealthListener.onHealthStoreFailConnected(error);
                    setActivated(false);
                    onDestroy();
                }

                @Override
                public void onDisconnected() {
                    L.d(TAG, "Health data service is disconnected.");
                    activityHealthListener.onDisconnected();
                    setActivated(false);
                    onDestroy();
                }
            };

    /**
     * Call this method if storage is connect only
     */
    public void askPermissions() {
        HealthPermissionManager permissionManager = new HealthPermissionManager(store);
        Set<HealthPermissionManager.PermissionKey> permissionKeySet =
                SamsungHealthPermission.getPermissionKeySet();

        permissionManager.requestPermissions(permissionKeySet)
                .setResultListener(permissionListener);
    }

    private final HealthResultHolder.ResultListener<HealthPermissionManager.PermissionResult>
            permissionListener = new HealthResultHolder.ResultListener<
            HealthPermissionManager.PermissionResult>() {

        @Override
        public void onResult(HealthPermissionManager.PermissionResult result) {
            L.d(TAG, "Permission callback received.");
            Map<HealthPermissionManager.PermissionKey, Boolean> permissionMap = result.getResultMap();

            boolean isPermissionApproved = isEvenOnePermissionEnabled(permissionMap);

            if (isPermissionApproved) {
                startSynchronization(permissionMap);
            } else {
                activityHealthListener.onHealthStorePermissionFailed();
                setActivated(false);
            }
        }
    };

    private boolean isEvenOnePermissionEnabled(Map<HealthPermissionManager.PermissionKey,
            Boolean> permissionMap) {
        boolean isEvenOnePermissionEnabled = false;
        for (Map.Entry<HealthPermissionManager.PermissionKey, Boolean> permission
                : permissionMap.entrySet()) {
            if (permission.getValue()) {
                isEvenOnePermissionEnabled = true;
                break;
            }
        }
        return isEvenOnePermissionEnabled;
    }

    private void startSynchronization(Map<HealthPermissionManager.PermissionKey,
            Boolean> permissionMap) {
        if (!isActivated()) {
            activityHealthListener.onHealthStoreSuccessConnected();
        }
        setActivated(true);

        startDataTypeReport(permissionMap, SamsungHealthPermission.WEIGHT_READ,
                WeightReporter.class);

        startDataTypeReport(permissionMap, SamsungHealthPermission.STEP_READ,
                StepCountReporter.class);
    }

    public boolean isFirstConnection() {
        return preferencesManager.getBoolean(Keys.IS_FIRST_SAMSUNG_HEALTH_CONNECTION, true);
    }

    public void setFirstConnection(boolean isFirstConnection) {
        preferencesManager.setBoolean(Keys.IS_FIRST_SAMSUNG_HEALTH_CONNECTION,
                isFirstConnection);
    }

    public boolean isAvailable() {
        boolean isSamsungDevice = SsdkVendorCheck.isSamsungDevice();
        L.d(TAG, "Is samsung device? " + isSamsungDevice);

        return isSamsungDevice
                && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public boolean isActivated() {
        return preferencesManager.getBoolean(Keys.IS_SAMSUNG_HEALTH_ACTIVATED, false);
    }

    public void setActivated(boolean isActivated) {
        preferencesManager.setBoolean(Keys.IS_SAMSUNG_HEALTH_ACTIVATED,
                isActivated);
    }

    private BaseReporter getReporter(String dataTypeName, Class<? extends BaseReporter> clazz) {
        BaseReporter reporter = dataReporters.get(dataTypeName);
        if (reporter == null) {
            try {
                reporter = clazz.newInstance();
                dataReporters.put(dataTypeName, reporter);
            } catch (InstantiationException | IllegalAccessException e) {
                L.e(TAG, e.getMessage(), e);
            }
        }

        return reporter;
    }

    private void startDataTypeReport(Map<HealthPermissionManager.PermissionKey,
            Boolean> permissionMap,
                                     SamsungHealthPermission permission,
                                     Class<? extends BaseReporter> clazz) {
        if (permissionMap.get(permission.getKey())) {
            BaseReporter reporter = getReporter(permission.getName(), clazz);
            reporter.start(store);
        }
    }

}
