package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.ListMediaItemThumbNailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by hkyerra on 7/9/2015.
 */
public class MediaItemsThumbnailsListLoader extends DataAsyncTaskLibLoader<ListMediaItemThumbNailsResponse> {
    private String criteria;
    private String token;
    private String patientId;
    private int pageNo;
    private int pageSize;

    public MediaItemsThumbnailsListLoader(Context context, String criteria, String token, String patientId, int pageNo, int pageSize) {
        super(context);
        this.criteria = criteria;
        this.token = token;
        this.patientId = patientId;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    @Override
    protected ListMediaItemThumbNailsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().listMediaItemThumbnails(criteria, token, patientId, pageNo, pageSize);
    }
}
