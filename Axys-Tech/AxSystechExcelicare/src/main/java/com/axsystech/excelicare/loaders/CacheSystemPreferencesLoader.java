package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.data.responses.SystemPreferencesData;
import com.axsystech.excelicare.db.ConverterResponseToDbModel;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.DeviceUsersDbModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class CacheSystemPreferencesLoader extends DataAsyncTaskLibLoader<ArrayList<SystemPreferenceDBModel>> {

    private String TAG = CacheSystemPreferencesLoader.class.getSimpleName();
    private final ArrayList<SystemPreferencesData> systemPreferencesAL;
    private ArrayList<SystemPreferenceDBModel> systemPreferenceDBModelAL;

    public CacheSystemPreferencesLoader(final Context context, final ArrayList<SystemPreferencesData> systemPreferencesAL) {
        super(context, true);
        this.systemPreferencesAL = systemPreferencesAL;
    }

    @Override
    protected ArrayList<SystemPreferenceDBModel> performLoad() throws Exception {
        if (systemPreferencesAL == null) throw new SQLException("Unable to cache system config details in DB");

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (systemPreferencesAL != null && systemPreferencesAL.size() > 0) {
                systemPreferenceDBModelAL = new ArrayList<SystemPreferenceDBModel>();

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ArrayList<SystemPreferenceDBModel>>() {
                    @Override
                    public ArrayList<SystemPreferenceDBModel> call() throws Exception {
                        for (final SystemPreferencesData responseModel : systemPreferencesAL) {
                            SystemPreferenceDBModel dbModel = ConverterResponseToDbModel.getSystemPreferenceDBModel(responseModel);

                            if (dbModel != null) {
                                final Dao<SystemPreferenceDBModel, Long> systemPrefDao = helper.getDao(SystemPreferenceDBModel.class);

                                systemPrefDao.createOrUpdate(dbModel);

                                systemPreferenceDBModelAL.add(dbModel);
                                Trace.d(TAG, "Saved System Preference Details in DB...");
                            }
                        }

                        return systemPreferenceDBModelAL;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return systemPreferenceDBModelAL;
    }
}
