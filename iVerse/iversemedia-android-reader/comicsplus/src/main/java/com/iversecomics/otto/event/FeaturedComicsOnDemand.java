package com.iversecomics.otto.event;

import com.iversecomics.client.store.model.Comic;

import java.util.List;

/**
 */
public class FeaturedComicsOnDemand {

    List<Comic> comics;

    public FeaturedComicsOnDemand(List<Comic> comics) {
        this.comics = comics;
    }

    public List<Comic> getComics() {
        return comics;
    }
}
