package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.AudioRecordingInfo;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class AudioRecordingGet extends AbsRequestGet<AudioRecordingInfo> {
    private String mRequestUrl;

    public AudioRecordingGet(String requestUrl) {
        mRequestUrl = requestUrl;
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if(wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(mRequestUrl);
    }

    @Override
    protected AbsResponseProcessor<AudioRecordingInfo> getResponseProcessor() {
        return new RubyResponseProcessor<AudioRecordingInfo>(getOutputType());
    }
}
