package com.axsystech.excelicare.framework.ui.fragments.media;

import com.axsystech.excelicare.db.models.MediaFoldersModel;
import com.axsystech.excelicare.util.CommonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by someswarreddy on 12/07/15.
 */
public enum FilterType {

    FILTER_ALL(-1, "All"/*, -1*/),
    FILTER_PHOTOS(0, "Images"/*, 1*/),
    FILTER_VIDEOS(1, "Videos"/*, 4*/),
    FILTER_AUDIOS(2, "Audios"/*, 5*/);

    private final int filterId;
    private final String filterName;
//    private final int folderId;

    private FilterType(int fid, String fName/*, int folderId*/) {
        this.filterId = fid;
        this.filterName = fName;
//        this.folderId = folderId;
    }

    public boolean equalsFilter(String otherName) {
        return (otherName == null) ? false : filterName.equals(otherName);
    }

    public int getFilterId() {
        return this.filterId;
    }

    public String getFilterName() {
        return this.filterName;
    }

//    public int getFolderId() {
//        return folderId;
//    }

    public static FilterType getFilterTypeForFolderId(int folderId) {
        List<MediaFoldersModel> queriedFolder = CommonUtils.getFolderNameForId(folderId);

        FilterType filterType = FILTER_PHOTOS;
        if(queriedFolder != null && queriedFolder.size() > 0) {
            String folderName = queriedFolder.get(0).getMediaFolderName();

            filterType = getFilterTypeForName(folderName);
        }

        return filterType;
    }

    public static FilterType getFilterTypeForName(String filterName) {
        FilterType filterType = FILTER_ALL;

        if (filterName.equalsIgnoreCase(FILTER_ALL.getFilterName())) {
            return FILTER_ALL;
        } else if (filterName.equalsIgnoreCase(FILTER_PHOTOS.getFilterName())) {
            return FILTER_PHOTOS;
        } else if (filterName.equalsIgnoreCase(FILTER_VIDEOS.getFilterName())) {
            return FILTER_VIDEOS;
        } else if (filterName.equalsIgnoreCase(FILTER_AUDIOS.getFilterName())) {
            return FILTER_AUDIOS;
        }

        return filterType;
    }

    public static ArrayList<FilterType> getAllFilters(FilterType selectedFilterType) {
        ArrayList<FilterType> filters = new ArrayList<FilterType>();

        if (selectedFilterType.getFilterId() != FILTER_ALL.getFilterId()) filters.add(FILTER_ALL);
        if (selectedFilterType.getFilterId() != FILTER_PHOTOS.getFilterId())
            filters.add(FILTER_PHOTOS);
        if (selectedFilterType.getFilterId() != FILTER_VIDEOS.getFilterId())
            filters.add(FILTER_VIDEOS);
        if (selectedFilterType.getFilterId() != FILTER_AUDIOS.getFilterId())
            filters.add(FILTER_AUDIOS);

        return filters;
    }

    public static ArrayList<FilterType> getAllFilters(String selectedFilterTypeName) {
        ArrayList<FilterType> filters = new ArrayList<FilterType>();

        if (!selectedFilterTypeName.equalsIgnoreCase(FILTER_ALL.getFilterName())) {
            filters.add(FILTER_ALL);
        }

        if (!selectedFilterTypeName.equalsIgnoreCase(FILTER_PHOTOS.getFilterName())) {
            filters.add(FILTER_PHOTOS);
        }

        if (!selectedFilterTypeName.equalsIgnoreCase(FILTER_VIDEOS.getFilterName())) {
            filters.add(FILTER_VIDEOS);
        }

        if (!selectedFilterTypeName.equalsIgnoreCase(FILTER_AUDIOS.getFilterName())) {
            filters.add(FILTER_AUDIOS);
        }

        return filters;
    }

    public static String[] getAllFilters(FilterType selectedFilterType, ArrayList<FilterType> filtersList,
                                         int imagesCount, int videosCount, int audiosCount) {
        String[] filtersArray = null;

        if (filtersList != null && filtersList.size() > 0) {
            filtersArray = new String[filtersList.size()];
            int idx = -1;

            for (FilterType fType : filtersList) {
                if (fType.getFilterId() != selectedFilterType.getFilterId()) {
                    if(fType == FILTER_ALL) {
                        filtersArray[++idx] = fType.getFilterName() + " (" + (imagesCount + videosCount + audiosCount) + ")";
                    } else if(fType == FILTER_PHOTOS) {
                        filtersArray[++idx] = fType.getFilterName() + " (" + (imagesCount) + ")";
                    } else if(fType == FILTER_VIDEOS) {
                        filtersArray[++idx] = fType.getFilterName() + " (" + (videosCount) + ")";
                    } else if(fType == FILTER_AUDIOS) {
                        filtersArray[++idx] = fType.getFilterName() + " (" + (audiosCount) + ")";
                    }
                }
            }
        }

        return filtersArray;
    }

    @Override
    public String toString() {
        return this.filterName;
    }
}
