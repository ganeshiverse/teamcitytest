package com.babylon.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.MonitorMeParamsLoaderCallback;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.BaseModel;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.Patient;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.UIUtils;
import com.babylon.validator.ValidationController;
import com.babylon.validator.ValueBetweenValidator;
import com.babylon.view.CustomEditText;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Fragment {@link android.support.v4.app.Fragment} for viewing and changing user parameters
 */
public class TotalCholesterolParametersFragment extends BaseMonitorMeParamsLoaderFragment implements View.OnClickListener {

    private Patient originalPatient;

    private CustomEditText cholesterolEditText;
    private CustomEditText hdlCholesterolEditText;
    private CustomEditText triglycerideEditText;

    private DailyFeatureMap dailyFeatureMap;
    private List<MonitorMeParam> featureList;

    public static TotalCholesterolParametersFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        TotalCholesterolParametersFragment fragment = new TotalCholesterolParametersFragment();
        fragment.setArguments(b);
        return fragment;
    }

    private ValidationController validationController = new ValidationController();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        originalPatient = App.getInstance().getPatient();
        featureList = initMonitorMeParams();

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_total_cholesterol_parameters, container, false);
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(final long dateFrom) {
        final long endOfTheDay = DateUtils.getDayStartMills(dateInMillis, 1);
        MonitorMeParamsGet.newMonitorMeParamsGet(dateInMillis, endOfTheDay,
                MonitorMeParam.getParameterList(featureList)).execute(getActivity(),
                new RequestListener<ResponseList<MonitorMeFeatures>>() {
                    @Override
                    public void onRequestComplete(Response<ResponseList<MonitorMeFeatures>> response) {
                        if (getActivity() != null) {
                            MonitorMeParamSync.saveMonitorMeParams(getActivity().getContentResolver(), response,
                                    dateInMillis, endOfTheDay);
                            startLoaderManager();
                            onLoadedMonitorMeParamsFromServer();
                            setProgressBarVisible(false);
                        }
                    }
                }
        );
    }

    @Override
    protected void startLoaderManager() {
        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_USER_PARAMS,
                MonitorMeParamsLoaderCallback.createBundle(dateInMillis),
                new ParamsLoader());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.weight_parameters_input_data);

        if (getArguments() != null) {
            dateInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
        }

        String dateString = new MonitorMeDateViewController().getFormattedDay(dateInMillis);

        ((TextView) view.findViewById(R.id.dateTextView)).setText(dateString);
        view.findViewById(R.id.acceptButton).setOnClickListener(this);

        cholesterolEditText = (CustomEditText) getView().findViewById(R.id.edit_text_cholesterol);
        cholesterolEditText.getEditText().setKeyListener(DigitsKeyListener.getInstance(false, true));
        hdlCholesterolEditText = (CustomEditText) getView().findViewById(R.id
                .edit_text_hdl_cholesterol);
        hdlCholesterolEditText.getEditText().setKeyListener(DigitsKeyListener.getInstance(false, true));
        triglycerideEditText = (CustomEditText) getView().findViewById(R.id.edit_text_triglyceride);
        triglycerideEditText.getEditText().setKeyListener(DigitsKeyListener.getInstance(true, true));

        cholesterolEditText.setEmptyValidationErrorText(getString(R.string.cholesterol_validation));
        hdlCholesterolEditText.setEmptyValidationErrorText(getString(R.string.hdl_cholesterol_validation));
        triglycerideEditText.setEmptyValidationErrorText(getString(R.string.triglyceride_validation));

        cholesterolEditText.validationAfterTextChanged(validationController);
        hdlCholesterolEditText.validationAfterTextChanged(validationController);
        triglycerideEditText.validationAfterTextChanged(validationController);

        ValueBetweenValidator cholesterolValueValidator = new ValueBetweenValidator(null,
                getActivity().getString(R.string.validator_field_value_between, 0.01,
                        50), 0.01f, 50
        );

        validationController.addValidation(cholesterolEditText, cholesterolValueValidator);
        validationController.addValidation(hdlCholesterolEditText, cholesterolValueValidator);
        validationController.addValidation(triglycerideEditText, cholesterolValueValidator);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptButton:
                saveNewData();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isBackAllowed() {
        UIUtils.hideSoftKeyboard(getActivity(), cholesterolEditText.getEditText());
        return true;
    }

    public void saveNewData() {
        if (validationController.performValidation()) {
            if (wasDataUpdated()) {
                putMonitorMeParam(MonitorMeParam.CHOLESTEROL_ID, cholesterolEditText.getEditText());
                putMonitorMeParam(MonitorMeParam.CHOLESTEROL_HDL_ID, hdlCholesterolEditText.getEditText());
                putMonitorMeParam(MonitorMeParam.TRIGLYCERIDE_ID, triglycerideEditText.getEditText());

                DatabaseOperationUtils.getInstance().addMonitorMeFeatures(dailyFeatureMap.getFeatureMap(),
                        getActivity().getContentResolver());
                SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);
            }

            super.onTopBarButtonPressed();
        }
    }

    private void putMonitorMeParam(MonitorMeParam monitorMeParam, EditText editText) {
        final String value = String.valueOf(getFloatValue(editText));
        dailyFeatureMap.setValue(monitorMeParam, value);
    }

    /**
     * Define if patient personal data or daily report was updated
     *
     * @return true if patient personal data or daily report was updated
     * false if patient personal data and daily report wasn't updated
     */
    private boolean wasDataUpdated() {
        boolean cholesterol = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.CHOLESTEROL_ID))
                == getFloatValue(cholesterolEditText.getEditText());
        boolean hdlCholesterol = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.CHOLESTEROL_HDL_ID))
                == getFloatValue(hdlCholesterolEditText.getEditText());
        boolean triglyceride = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.TRIGLYCERIDE_ID))

                == getFloatValue(triglycerideEditText.getEditText());

        return !(cholesterol && hdlCholesterol && triglyceride);
    }

    /**
     * Convert input number in {@param editText} in float format
     *
     * @param editText view {@link android.view.View} for input
     * @return input number in float format
     */
    private float getFloatValue(EditText editText) {
        return BaseModel.getFloat(editText.getText().toString());
    }

    private class ParamsLoader extends MonitorMeParamsLoaderCallback {

        @Override
        public Context getContext() {
            return getActivity();
        }

        @Override
        public List<MonitorMeParam> getMonitorMeParamList() {
            return featureList;
        }

        @Override
        public void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> featureMap) {
            dailyFeatureMap = new DailyFeatureMap(featureMap);

            if (!featureMap.isEmpty()) {
                showNotZeroValue(cholesterolEditText.getEditText(), MonitorMeParam.CHOLESTEROL_ID);
                showNotZeroValue(hdlCholesterolEditText.getEditText(), MonitorMeParam.CHOLESTEROL_HDL_ID);
                showNotZeroValue(triglycerideEditText.getEditText(), MonitorMeParam.TRIGLYCERIDE_ID);
            }
        }
    }

    private List<MonitorMeParam> initMonitorMeParams() {
        List<MonitorMeParam> featureList =
                new LinkedList<MonitorMeParam>();
        featureList.add(MonitorMeParam.CHOLESTEROL_ID);
        featureList.add(MonitorMeParam.CHOLESTEROL_HDL_ID);
        featureList.add(MonitorMeParam.SYSTOLIC_BP_ID);
        featureList.add(MonitorMeParam.DIASTOLIC_BP_ID);
        featureList.add(MonitorMeParam.DIABETIC_ID);
        featureList.add(MonitorMeParam.CHOLESTEROL_LDL_ID);
        featureList.add(MonitorMeParam.TRIGLYCERIDE_ID);
        featureList.add(MonitorMeParam.HEART_TREATMENT_ID);
        return featureList;
    }

    private void showNotZeroValue(TextView textView, MonitorMeParam monitorMeParam) {
        final MonitorMeFeatures.Feature feature = dailyFeatureMap
                .getFeature(monitorMeParam);
        final String value = feature.getValue();

        String result = TextUtils.isEmpty(textView.getText().toString()) ? "" : textView.getText().toString();

        if (!TextUtils.isEmpty(value) && Float.valueOf(value) != 0) {
            result = value;
        }

        textView.setText(result);
    }
}