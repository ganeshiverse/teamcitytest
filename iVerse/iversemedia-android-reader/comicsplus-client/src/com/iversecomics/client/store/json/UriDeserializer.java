package com.iversecomics.client.store.json;

import java.lang.reflect.Type;

import android.net.Uri;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

//Gson gson = new GsonBuilder()
//.registerTypeAdapter(UriDeserializer.class, new UriDeserializer())
//.create();

public class UriDeserializer implements JsonDeserializer<Uri> {
  public Uri deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
	  
    return Uri.parse(json.getAsJsonPrimitive().getAsString());
  }
}