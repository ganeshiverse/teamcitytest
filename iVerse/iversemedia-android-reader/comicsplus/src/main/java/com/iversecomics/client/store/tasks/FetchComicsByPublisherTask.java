package com.iversecomics.client.store.tasks;

import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStore.ServerApi;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.ComicStoreTask;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.db.OrderingUpdater;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class FetchComicsByPublisherTask extends ComicStoreTask {
    private static final Logger LOG = LoggerFactory.getLogger(FetchComicsByPublisherTask.class);
    private final String publisherId;
    private final String listId;

    public FetchComicsByPublisherTask(ComicStore comicStore, String publisherId) {
        super(comicStore);
        this.publisherId = publisherId;
        this.listId = ListID.getComicsByPublisher(publisherId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FetchComicsByPublisherTask other = (FetchComicsByPublisherTask) obj;
        if (listId == null) {
            if (other.listId != null) {
                return false;
            }
        } else if (!listId.equals(other.listId)) {
            return false;
        }
        return true;
    }

    @Override
    public void execTask() {
        try {
            ServerApi server = comicStore.getServerApi();

            JSONObject json = server.getComicsByPublisher(publisherId);
            ComicStoreDBUpdater updater = comicStore.newDBUpdater();
            updater.addComicDBSubUpdater(new OrderingUpdater(listId));

            ResponseParser parser = new ResponseParser();
            parser.parseComics(json, updater);
            server.updateFreshness(listId, Time.HOUR * 3);
        } catch (ComicStoreException e) {
            LOG.warn(e, "Unable to update comics list by-publisher: " + publisherId);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listId == null) ? 0 : listId.hashCode());
        return result;
    }

    @Override
    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(listId);
    }
}
