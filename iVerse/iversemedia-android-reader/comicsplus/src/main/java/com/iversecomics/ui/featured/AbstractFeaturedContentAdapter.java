package com.iversecomics.ui.featured;

import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;

import java.util.List;

/**
 */
public abstract class AbstractFeaturedContentAdapter {

    List<Comic> featuredComics;
    List<FeaturedSlot> slots;
    Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public List<Comic> getFeaturedComics() {
        return featuredComics;
    }

    public void setFeaturedComics(List<Comic> featuredComics) {
        this.featuredComics = featuredComics;
        if (listener != null && featuredComics != null) {
            listener.onFeaturedUpdated(featuredComics);
        }
    }

    public List<FeaturedSlot> getSlots() {
        return slots;
    }

    public void setSlots(List<FeaturedSlot> slots) {
        this.slots = slots;
        if (listener != null && slots != null) {
            listener.onSlotsUpdated(slots);
        }
    }

    public static interface Listener {
        void onSlotsUpdated(List<FeaturedSlot> comics);

        void onFeaturedUpdated(List<Comic> comics);
    }

}
