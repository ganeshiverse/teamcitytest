package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class FreshnessTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "freshness");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of freshness.
     */
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/vnd.iversecomics.store.freshness";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single freshness.
     */
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/vnd.iversecomics.store.freshness";

    public static final String DEFAULT_SORT_ORDER = "key ASC";
    public static final String TABLE = "freshness";

    public static final String KEY = "key";
    public static final String TIMESTAMP_CREATED = "timestampCreated";
    public static final String TIMESTAMP_EXPIRED = "timestampExpired";

    public static final String[] FULL_PROJECTION = new String[]{
            _ID, KEY, TIMESTAMP_CREATED, TIMESTAMP_EXPIRED
    };
}
