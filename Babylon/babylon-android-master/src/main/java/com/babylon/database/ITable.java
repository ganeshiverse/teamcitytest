package com.babylon.database;

public interface ITable {
    public String getName();

    public Class getSchema();

}
