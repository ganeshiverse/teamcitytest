package com.babylon.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Babylon on 06/05/2015.
 */
public class Password {

    @Expose
    private String password;

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}