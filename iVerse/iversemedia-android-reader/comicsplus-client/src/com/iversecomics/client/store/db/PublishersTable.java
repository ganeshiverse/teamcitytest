package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.client.store.model.Publisher;

public class PublishersTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri      CONTENT_URI                = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI,
                                                                    "publishers");

    public static final Uri      CONTENT_URI_BY_PUBLISHERID = Uri.withAppendedPath(CONTENT_URI, "byid");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of publisher.
     */
    public static final String   CONTENT_TYPE               = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                                    + "/vnd.iversecomics.store.publishers";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single user.
     */
    public static final String   CONTENT_ITEM_TYPE          = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                                    + "/vnd.iversecomics.store.publishers";

    public static final String   DEFAULT_SORT_ORDER         = "name ASC";
    public static final String   TABLE                      = "publishers";

    // server id for publisher
    public static final String   PUBLISHERID                = "publisherId";
    // human readable name
    public static final String   NAME                       = "name";
    // flag if featured
    public static final String   FEATURED                   = "featured";
    public static final String   BANNER_IMAGE_FILE_NAME     = "bannerImageFileName";
    public static final String   LOGO_IMAGE_FILE_NAME       = "logoImageFileName";
    public static final String   IMAGE_URL                  = "imageUrl";

    public static final String[] FULL_PROJECTION            = new String[] {
            _ID, PUBLISHERID, NAME, FEATURED, BANNER_IMAGE_FILE_NAME, LOGO_IMAGE_FILE_NAME, IMAGE_URL
                                                            };

    public static ContentValues asValues(Publisher publisher) {
        ContentValues values = new ContentValues();
        if (publisher.getDbId() >= 0) {
            values.put(_ID, publisher.getDbId());
        }

        values.put(PUBLISHERID, publisher.getPublisherId());
        values.put(NAME, publisher.getName());
        setBoolean(values, FEATURED, publisher.isFeatured());
        values.put(BANNER_IMAGE_FILE_NAME, publisher.getBannerImageFileName());
        values.put(LOGO_IMAGE_FILE_NAME, publisher.getLogoImageFileName());
        values.put(IMAGE_URL, publisher.getImageUrl());

        return values;
    }

    public static Publisher fromCursor(Cursor c) {
        Publisher publisher = new Publisher();

        publisher.setDbId(getLong(c, _ID));
        publisher.setPublisherId(getString(c, PUBLISHERID));
        publisher.setName(getString(c, NAME));
        publisher.setFeatured(getBoolean(c, FEATURED));
        publisher.setBannerImageFileName(getString(c, BANNER_IMAGE_FILE_NAME));
        publisher.setLogoImageFileName(getString(c, LOGO_IMAGE_FILE_NAME));
        publisher.setImageUrl(getString(c, IMAGE_URL));

        return publisher;
    }
}
