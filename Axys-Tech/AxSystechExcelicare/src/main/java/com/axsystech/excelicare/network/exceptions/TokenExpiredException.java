package com.axsystech.excelicare.network.exceptions;

/**
 * Date: 14.10.2014
 * Time: 11:08
 *
 * @author SomeswarReddy
 */
public class TokenExpiredException extends Exception {

    private static final long serialVersionUID = 5529655471231758629L;

    public TokenExpiredException() {
    }

    public TokenExpiredException(final String detailMessage) {
        super(detailMessage);
    }

    public TokenExpiredException(final String detailMessage, final Throwable throwable) {
        super(detailMessage, throwable);
    }

    public TokenExpiredException(final Throwable throwable) {
        super(throwable);
    }
}
