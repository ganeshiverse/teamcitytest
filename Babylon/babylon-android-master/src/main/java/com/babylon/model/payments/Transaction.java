package com.babylon.model.payments;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Transaction implements Comparable<Transaction> {
    private String id;
    private Double price;
    private String currency;
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("purchase_type")
    private String purchaseType;

    private Appointment purchase;
    @SerializedName("credit_card")
    private PaymentCard creditCard;

    public Double getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public Appointment getPurchase() {
        return purchase;
    }

    public PaymentCard getCreditCard() {
        return creditCard;
    }

    @Override
    public int compareTo(Transaction t) {
        return createdAt.compareTo(t.createdAt);
    }
}
