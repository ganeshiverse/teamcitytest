package com.axsystech.excelicare.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Utility method for shared preferences.
 *
 * @author SomeswarReddy
 */
public class SharedPreferenceUtil {
    private static final String TAG = "SharedPreferenceUtil";

    private static final String SP_NAME = "SHARED_PREF_AXSYS";

    public static final String SP_MEDIA_LOCAL_ID_KEY = "MEDIA_LOCAL_ID";
    public static final String SP_CLIENT_URL_KEY = "CLIENT_URL";

    /**
     * Method to get the value for the specified key
     *
     * @param context The context
     * @param key     Key for the preference.
     * @return The Value for specified key, "" by default.
     */
    public static String getStringFromSP(Context context, String key) {
        return getStringFromSP(context, key, "");
    }


    /**
     * Retrieve boolean value from SharedPreference for the given key
     */
    public static void saveBooleanInSP(Context context, String key, boolean value) {
        Trace.i(TAG, "Save boolean to SP");
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Retrieve boolean value from SharedPreference for the given key
     */
    public static boolean getBooleanFromSP(Context context, String key, boolean defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        return preferences.getBoolean(key, defaultValue);
    }


    /**
     * Retrieve string value from SharedPreference for the given key
     *
     * @param defaultvalue Value to return if this preference does not exist.
     */
    public static String getStringFromSP(Context context, String key, String defaultvalue) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        return preferences.getString(key, defaultvalue);
    }

    /**
     * Save String value from SharedPreference for the given key with default value
     */
    public static void saveStringInSP(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Retrieve int value from SharedPreference for the given key with default value
     */
    public static int getIntFromSP(Context context, String key, int defaultvalue) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        return preferences.getInt(key, defaultvalue);
    }

    /**
     * Save int value from SharedPreference for the given key with default value
     */
    public static void saveIntInSP(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * Delete key from SharedPreference for the given key
     */
    public static void deleteKeyFromSP(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Retrieve long value from SharedPreference for the given key
     *
     * @param defaultvalue Value to return if this preference does not exist.
     */
    public static long getLongFromSP(Context context, String key, long defaultvalue) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        return preferences.getLong(key, defaultvalue);
    }

    /**
     * Save long value from SharedPreference for the given key with default value
     */
    public static void saveLongInSP(Context context, String key, long value) {
        SharedPreferences preferences = context.getSharedPreferences(
                SP_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }
}