package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.babylon.Keys;
import com.babylon.R;
import com.babylon.images.FileCache;
import com.babylon.utils.SelectImageManager;

public class QuestionAttachmentsActivity extends Activity {

    public static final String EXTRA_IMAGE_PATH = "EXTRA_IMAGE_PATH";
    public static final int AUDIO_RESULT = 3214;
    public static final String ENABLE_AUDIO_EXTRA = "ENABLE_AUDIO";
    private boolean isActivityStartFromSkin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_attachments);

        Intent intent = getIntent();
        if (intent != null) {
            boolean enableAudio = intent.getBooleanExtra(ENABLE_AUDIO_EXTRA, true);
            int photoAction = intent.getIntExtra(Keys.PHOTO_ACTION, 0);

            if (!enableAudio) {
                View audioButton = findViewById(R.id.btnRecordAudio);
                audioButton.setVisibility(View.GONE);
            }

            if (photoAction != 0) {
                isActivityStartFromSkin = true;
                switch (photoAction) {
                    case SelectImageManager.CAMERA:
                        SelectImageManager.getInstance().startCamera(this);
                        break;
                    case SelectImageManager.GALLERY:
                        SelectImageManager.getInstance().startGallery(this);
                        break;
                    default:
                        break;
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AUDIO_RESULT) {
                setResult(Activity.RESULT_OK, data);
                finish();
            } else {
                SelectImageManager.getInstance().onActivityResult(requestCode, resultCode, data,
                        onImageCompleteListener);
            }
        } else if (isActivityStartFromSkin) {
            setResult(resultCode);
            finish();
        }
    }

    public void onExistingPhotoClick(View view) {
        SelectImageManager.getInstance().startGallery(this);
    }

    public void onTakePhotoClick(View view) {
        SelectImageManager.getInstance().startCamera(this);
    }

    public void onRecordAudioClick(View view) {
        Intent intent = new Intent(this, RecordAudioActivity.class);
        startActivityForResult(intent, AUDIO_RESULT);
    }

    private void onFileReceived() {
        finish();
    }

    private final SelectImageManager.OnImageCompleteListener onImageCompleteListener = new SelectImageManager
            .OnImageCompleteListener() {

        @Override
        public void onImageComplete(Bitmap bitmap) {
            setImageResult(bitmap);
            onFileReceived();
        }
    };

    private void setImageResult(Bitmap bitmap) {
        final FileCache cache = FileCache.getInstance();
        final String path = cache.save(bitmap);

        final Intent i = new Intent();
        i.putExtra(EXTRA_IMAGE_PATH, path);

        setResult(Activity.RESULT_OK, i);
    }
}
