package com.babylon.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import com.babylon.R;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.model.payments.PaymentCard;
import com.babylon.view.TypefaceTextView;

public class PaymentCardsCursorAdapter extends CursorAdapter {

    private OnCreditCardDeleteListener onCreditCardDeleteListener;
    private boolean mHideArrow=false;
    public PaymentCardsCursorAdapter(Context context, OnCreditCardDeleteListener onCreditCardDeleteListener, boolean
            pHideArrow) {
        super(context, null, true);
        this.onCreditCardDeleteListener = onCreditCardDeleteListener;
        mHideArrow=pHideArrow;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return View.inflate(context, R.layout.list_item_payment_card, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final PaymentCard paymentCard = new PaymentCard();
        paymentCard.createFromCursor(cursor);

        final PaymentCardViewController paymentCardViewController = new PaymentCardViewController();
        paymentCard.setImageResId(paymentCardViewController.findImageId(paymentCard.getCardType()));

        final String type = paymentCard.getCardType();
        final TypefaceTextView typeTextView = (TypefaceTextView) view.findViewById(R.id.credit_card_type_text_view);
        typeTextView.setText(type);

        final ImageView arrowView=(ImageView)view.findViewById(R.id.next_image_view);
        if(mHideArrow) {
            arrowView.setVisibility(View.GONE);
        }

        final TypefaceTextView maskedNumberTextView = (TypefaceTextView) view.findViewById(R.id
                .credit_card_number_text_view);
        maskedNumberTextView.setText(paymentCardViewController.getCardViewNumber(paymentCard));

        final ImageView cardTypeImageView = (ImageView)view.findViewById(R.id.credit_card_image_view);
        cardTypeImageView.setImageResource(paymentCard.getImageResId());

        final View deleteCardImageView = view.findViewById(R.id.credit_card_delete_button);
        deleteCardImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreditCardDeleteListener.onCreditCardDelete(paymentCard.getId());
            }
        });

        deleteCardImageView.setVisibility(onCreditCardDeleteListener.isEditable() ? View.VISIBLE : View.GONE);
    }

    @Override
    public Object getItem(int position) {
        if (getCursor() != null && getCursor().moveToPosition(position)) {
            PaymentCard creditCard = new PaymentCard();
            creditCard.createFromCursor(getCursor());
            return creditCard;
        } else {
            return super.getItem(position);
        }
    }

    public interface OnCreditCardDeleteListener {
        public void onCreditCardDelete(int id);
        public boolean isEditable();
    }
}
