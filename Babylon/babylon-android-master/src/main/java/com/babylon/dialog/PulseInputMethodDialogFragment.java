package com.babylon.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.babylon.R;
import com.babylon.fragment.MeasuringPulseFragment;
import com.babylon.utils.Constants;

public class PulseInputMethodDialogFragment extends DialogFragment {

    public static final String TAG = PulseInputMethodDialogFragment.class.getSimpleName();

    public static PulseInputMethodDialogFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        PulseInputMethodDialogFragment fragment = new PulseInputMethodDialogFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new AlertDialog.Builder(getActivity())
                .setCancelable(true)
                .setView(getContentView())
                .create();

        return dialog;
    }

    private View getContentView() {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_pulse_input_type, null, false);

        final Button manuallyButton = (Button) view.findViewById(R.id.btn_manually);
        manuallyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PulseInputManuallyDialogFragment pulseInputManuallyDialogFragment =
                        PulseInputManuallyDialogFragment.getInstance(getArguments().getLong(Constants.DATE_IN_MILLIS));
                pulseInputManuallyDialogFragment.show(getFragmentManager(), PulseInputManuallyDialogFragment.TAG);

                dismiss();
            }
        });

        final Button measureButton = (Button) view.findViewById(R.id.btn_measure);
        measureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content,
                        MeasuringPulseFragment.getInstance(getArguments().getLong(Constants.DATE_IN_MILLIS))).addToBackStack(null)
                        .commit();
                dismiss();
            }
        });

        final Button closeButton = (Button) view.findViewById(R.id.btn_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

}
