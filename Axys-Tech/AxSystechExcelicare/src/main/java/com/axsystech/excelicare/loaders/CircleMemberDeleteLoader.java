package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/2/2015.
 */
public class CircleMemberDeleteLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String userCircleMemberId;
    private String stopReasonSLU;
    private String stopReasonDescription;


    public CircleMemberDeleteLoader(Context context, String token, String userCircleMemberId, String stopReasonSLU, String stopReasonDescription) {
        super(context);
        this.token = token;
        this.userCircleMemberId = userCircleMemberId;
        this.stopReasonSLU = stopReasonSLU;
        this.stopReasonDescription = stopReasonDescription;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCircleMemberRemove(token, userCircleMemberId, stopReasonSLU, stopReasonDescription);
    }
}

