package com.babylon.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.babylon.PushBroadcastReceiver;
import com.babylon.R;
import com.babylon.model.push.NotifAskConsultant;
import com.babylon.utils.GsonWrapper;
import com.babylonpartners.babylon.HomePageActivity;

/**
 * Fragment {@link android.support.v4.app.Fragment} for viewing and changing user parameters
 */
public class OrderTempFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_temp, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.order_title);

        view.findViewById(R.id.acceptButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptButton:
                getActivity().finish();
                sendSuggestConsultationBroadcast();
                break;
            default:
                break;
        }
    }

    private void sendSuggestConsultationBroadcast() {
        NotifAskConsultant parseData = new NotifAskConsultant();
        parseData.setAction(PushBroadcastReceiver.PushAction.SUGGEST_CONSULTATION_AFTER_ARCHIVING.toString());

        Intent intent = new Intent(getActivity(), HomePageActivity.class);
        intent.putExtra(PushBroadcastReceiver.KEY_PARSE_DATA, GsonWrapper.getGson().toJson(parseData));

        startActivity(intent);
    }
}