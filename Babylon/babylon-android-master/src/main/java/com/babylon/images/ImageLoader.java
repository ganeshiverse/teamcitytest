package com.babylon.images;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Region;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.babylon.App;
import com.babylon.R;
import com.babylon.model.FileEntry;
import com.babylon.sync.FileDownloader;
import com.babylon.utils.L;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Nullable;

public class ImageLoader {
    private static final String TAG = "ImageLoader";
    private static final int STAGE_STEP = 20;
    private static final int PADDING = 80;
    private static ImageLoader instance = null;
    private MemoryCache memoryCache = new MemoryCache();
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private ExecutorService executorService;
    public static final int BIG_IMAGE = 0;
    public static final int NORMAL_IMAGE = 1;
    public static final int SMALL_IMAGE = 2;

    private static final int BIG_SIZE = 400;
    private static final int NORMAL_SIZE = 200;
    private static final int SMALL_SIZE = 100;

    private static final int BIG_SIZE_VAR = (int) App.getInstance().getResources().getDimension(R.dimen.big_image_size);
    private static final int NORMAL_SIZE_VAR = (int) App.getInstance().getResources().getDimension(R.dimen.normal_image_size);
    private static final int SMALL_SIZE_VAR = (int) App.getInstance().getResources().getDimension(R.dimen.small_image_size);
    private ProgressBar progressBar;
    private FileEntry fileEntry;

    static interface OnFetchCompleteListener {
        void onFetchComplete(Bitmap result);
    }

    public static synchronized ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public ImageLoader() {
        // LogUtils.e(TAG, "new ImageLoader");
        executorService = Executors.newFixedThreadPool(5);
    }

    private final int loadingBig = R.drawable.loading_big;
    private final int loadingNormal = R.drawable.loading_normal;
    private final int loadingSmall = R.drawable.loading_small;
    private final int noImageId = R.drawable.no_image;

    public void displayImage(String url, ImageView imageView, int sizeType) {
        displayImage(url, imageView, sizeType, false, false, 0, true);
    }

    public void displayImageNoCache(String url, ImageView imageView, int sizeType) {
        displayImage(url, imageView, sizeType, false, false, 0, false);
    }

    public void displayImage(String url, ImageView imageView, int sizeType, int noImageRes) {
        displayImage(url, imageView, sizeType, false, false, noImageRes, true);
    }

    public void displayImage(String url, ImageView imageView, int sizeType, boolean needGone) {
        displayImage(url, imageView, sizeType, false, needGone, 0, true);
    }

    public void displayImage(ImageView imageView, ProgressBar progressbar, int sizeType, boolean needGone,
                             boolean needAnimation, FileEntry fileEntry) {
        progressBar = progressbar;
        this.fileEntry = fileEntry;
        displayImage(fileEntry.getFilePath(), imageView, sizeType, needAnimation, needGone, 0, true);
    }

    public void displayImage(ImageView imageView, int sizeType, boolean needGone,
                             boolean needAnimation, String filePath) {
        displayImage(filePath, imageView, sizeType, needAnimation, needGone, 0, true);
    }

    public void displayImage(String url, ImageView imageView, int sizeType, boolean needAnimation, boolean needGone,
                             int noImageRes, boolean fromMemory) {
        if (url != null && !url.equals("")) {

            imageViews.put(imageView, url);
            Bitmap bitmap = null;
            if (fromMemory) {
                bitmap = memoryCache.get(url);
            }
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            } else {
                if (progressBar == null) {
                    if (sizeType == 0 || sizeType == 3) {
                        imageView.setImageResource(loadingBig);
                    } else if (sizeType == 1 || sizeType == 4) {
                        imageView.setImageResource(loadingNormal);
                    } else {
                        imageView.setImageResource(loadingSmall);
                    }
                }
                queuePhoto(url, imageView, sizeType, needAnimation, needGone);
            }
        } else {
            if (needGone) {
                imageView.setVisibility(View.GONE);
            } else {
                imageView.setVisibility(View.VISIBLE);
                if (noImageRes != 0) {
                    imageView.setImageResource(noImageRes);
                } else {
                    imageView.setImageResource(noImageId);
                }
            }
        }
    }

    public byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight());
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, buffer);
        return buffer.toByteArray();
    }

    public @Nullable Bitmap loadBitmap(String filePath) {
        Bitmap b = null;
        try {
            FileInputStream fi = new FileInputStream(filePath);
             b = BitmapFactory.decodeStream(fi);
        } catch (FileNotFoundException e) {
            L.e(TAG, e.getMessage(), e);
        }
        return b;
    }

    public void loadBitmap(String url, OnFetchCompleteListener onFetchCompleteListener) {
        loadBitmap(url, NORMAL_IMAGE, onFetchCompleteListener);
    }

    public void loadBitmap(String url, int sizeType, OnFetchCompleteListener onFetchCompleteListener) {
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null) {
            onFetchCompleteListener.onFetchComplete(bitmap);
        } else {
            executorService.submit(new BitmapLoader(url, sizeType, onFetchCompleteListener));
        }
    }

    private void queuePhoto(String url, ImageView imageView, int sizeType, boolean needAnimation, boolean needGone) {
        PhotoToLoad p = new PhotoToLoad(url, imageView, sizeType, needAnimation, needGone);
        executorService.submit(new PhotosLoader(p));
    }

    public Bitmap getBitmap(String url, ImageView imageView, int sizeType) {
        File f = getFileByUrl(url);

        Bitmap b = decodeFile(f, sizeType);
        if (imageView != null) {
            imageView.setTag(f.getAbsolutePath());
        }
        return b;

    }

    public File getFileByUrl(String url) {
        if (fileEntry == null) {
            try {
                return loadFile(url);
            } catch (IOException e) {
                L.e(TAG, e.getMessage(), e);
                return null;
            }
        } else {
            return loadFile(fileEntry);
        }

    }

    private File loadFile(FileEntry fileEntry) {
        File f = FileCache.getInstance().getFileByPath(fileEntry.getFilePath());
        if (!f.exists()) {
            FileDownloader.pullFileEntry(fileEntry);
            f = FileCache.getInstance().getFileByPath(fileEntry.getFilePath());
        }
        return f;
    }

    private File loadFile(String url) throws IOException {
        File f = FileCache.getInstance().getFileByUrl(url);

        URL imageUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
        conn.setConnectTimeout(30000);
        conn.setReadTimeout(30000);
        conn.setInstanceFollowRedirects(true);
        InputStream is = conn.getInputStream();
        OutputStream os = new FileOutputStream(f);
        Utils.copyStream(is, os);
        os.close();

        return f;
    }

    public Bitmap getStageBitmap(String[] urlList, int sizeType) {
        int imageCount = urlList.length;
        List<Bitmap> bitmapList = new ArrayList<Bitmap>();

        for (int i = 0; i < imageCount; i++) {
            Bitmap bitmap = getBitmap(urlList[i], null, sizeType);
            bitmapList.add(bitmap);
        }
        return getStageBitmap(bitmapList, sizeType);
    }

    public Bitmap getStageBitmap(List<Bitmap> bitmapList, int sizeType) {
        Bitmap resultBitmap = null;

        switch (sizeType) {
            case 0:
                resultBitmap = Bitmap.createBitmap(BIG_SIZE, BIG_SIZE, Config.ARGB_8888);
                break;
            case 1:
                resultBitmap = Bitmap.createBitmap(NORMAL_SIZE, NORMAL_SIZE, Config.ARGB_8888);
                break;
            case 2:
                resultBitmap = Bitmap.createBitmap(SMALL_SIZE, SMALL_SIZE, Config.ARGB_8888);
                break;
            case 3:
                resultBitmap = Bitmap.createBitmap(BIG_SIZE_VAR, BIG_SIZE_VAR, Config.ARGB_8888);
                break;
            case 4:
                resultBitmap = Bitmap.createBitmap(NORMAL_SIZE_VAR, NORMAL_SIZE_VAR, Config.ARGB_8888);
                break;
            case 5:
                resultBitmap = Bitmap.createBitmap(SMALL_SIZE_VAR, SMALL_SIZE_VAR, Config.ARGB_8888);
                break;
            default:
                break;
        }

        Canvas canvas = new Canvas(resultBitmap);
        int stage = 0;
        for (Bitmap bitmap : bitmapList) {
            Bitmap bitmapToCanvas = Bitmap.createScaledBitmap(bitmap,
                    (int) (resultBitmap.getWidth() - (bitmapList.size() - 1) * STAGE_STEP),
                    (int) (resultBitmap.getHeight() - (bitmapList.size() - 1) * STAGE_STEP), false);
            canvas.drawBitmap(bitmapToCanvas, 0 + stage, 0 + stage, null);
            stage = stage + STAGE_STEP;
        }
        return resultBitmap;
    }

    public Bitmap getCollageBitmap(String[] urlList, int resultBitmapSizeType) {
        int imageCount = urlList.length;
        int simpleBitmapSizeType = NORMAL_SIZE;
        List<Bitmap> bitmapList = new ArrayList<Bitmap>();

        for (int i = 0; i < imageCount; i++) {

            if (imageCount == 1) {
                simpleBitmapSizeType = BIG_IMAGE;
            } else if (imageCount == 2) {
                simpleBitmapSizeType = NORMAL_SIZE;
            } else if (imageCount == 3) {
                if (i == 0) {
                    simpleBitmapSizeType = NORMAL_SIZE;
                } else {
                    simpleBitmapSizeType = NORMAL_SIZE;
                }
            } else if (imageCount == 4) {
                simpleBitmapSizeType = SMALL_SIZE;
            } else if (imageCount == 5) {
                if (i == 0) {
                    simpleBitmapSizeType = NORMAL_SIZE;
                } else {
                    simpleBitmapSizeType = SMALL_SIZE;
                }
            } else if (imageCount == 6) {
                if (i == 0 || i == 5) {
                    simpleBitmapSizeType = NORMAL_SIZE;
                } else {
                    simpleBitmapSizeType = SMALL_SIZE;
                }
            } else if (imageCount == 7) {
                if (i == 1) {
                    simpleBitmapSizeType = NORMAL_SIZE;
                } else {
                    simpleBitmapSizeType = SMALL_SIZE;
                }
            } else {
                simpleBitmapSizeType = SMALL_SIZE;
            }

            Bitmap bitmap = getBitmap(urlList[i], null, simpleBitmapSizeType);
            bitmapList.add(bitmap);
        }
        return getCollageBitmap(bitmapList, resultBitmapSizeType);
    }

    public Bitmap getCollageBitmap(List<Bitmap> bitmapList, int resultBitmapSizeType) {
        int maxImageInLine = 4;
        int maxImageInCollage = maxImageInLine * 2;
        int imageCount = bitmapList.size();
        int resultBitmapWidth = NORMAL_SIZE * maxImageInLine;
        int resultBitmapHeight = NORMAL_SIZE * 2;
        Bitmap resultBitmap = null;

        switch (resultBitmapSizeType) {
            case 0:
                resultBitmapWidth = BIG_SIZE * maxImageInLine;
                resultBitmapHeight = BIG_SIZE * 2;
                break;
            case 1:
                resultBitmapWidth = NORMAL_SIZE * maxImageInLine;
                resultBitmapHeight = NORMAL_SIZE * 2;
                break;
            case 2:
                resultBitmapWidth = SMALL_SIZE * maxImageInLine;
                resultBitmapHeight = SMALL_SIZE * 2;
                break;
            case 3:
                resultBitmapWidth = BIG_SIZE_VAR * maxImageInLine;
                resultBitmapHeight = BIG_SIZE_VAR * 2;
                break;
            case 4:
                resultBitmapWidth = NORMAL_SIZE_VAR * maxImageInLine;
                resultBitmapHeight = NORMAL_SIZE_VAR * 2;
                break;
            case 5:
                resultBitmapWidth = SMALL_SIZE_VAR * maxImageInLine;
                resultBitmapHeight = SMALL_SIZE_VAR * 2;
                break;
            default:
                break;
        }

        if (imageCount == 1) {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth, resultBitmapHeight, Config.ARGB_8888);
        } else if (imageCount == 2) {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + PADDING, resultBitmapHeight, Config.ARGB_8888);
        } else if (imageCount == 3) {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + PADDING * 3, resultBitmapHeight + PADDING * 2,
                    Config.ARGB_8888);
        } else if (imageCount == 4) {
            resultBitmapHeight = resultBitmapHeight / 2;
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + PADDING * 3, resultBitmapHeight, Config.ARGB_8888);
        } else if (imageCount == 5) {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + PADDING * 3, resultBitmapHeight + PADDING,
                    Config.ARGB_8888);
        } else if (imageCount == 6) {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + PADDING * 3, resultBitmapHeight + PADDING,
                    Config.ARGB_8888);
        } else if (imageCount == 7) {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + PADDING * 3, resultBitmapHeight + PADDING,
                    Config.ARGB_8888);
        } else {
            resultBitmap = Bitmap.createBitmap(resultBitmapWidth + (maxImageInLine - 1) * PADDING, resultBitmapHeight
                    + PADDING, Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(resultBitmap);

        for (int i = 0; i < imageCount; i++) {
            Bitmap sourceBitmap = bitmapList.get(i);
            Bitmap bitmapToCanvas = null;

            if (imageCount == 1) {
                bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth, resultBitmapHeight);
            } else if (imageCount == 2) {
                bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 2d, resultBitmapHeight);
            } else if (imageCount == 3) {
                if (i == 0) {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap,
                            (double) resultBitmapWidth / 2d + (double) PADDING,
                            (double) resultBitmapHeight + (double) PADDING);
                } else {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 2d + PADDING,
                            resultBitmapHeight / 2d);
                }
            } else if (imageCount == 4) {
                bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 4d, resultBitmapHeight);
            } else if (imageCount == 5) {
                if (i == 0) {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 2d + PADDING, resultBitmapHeight
                            + PADDING);
                } else {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 4d, resultBitmapHeight / 2d);
                }
            } else if (imageCount == 6) {
                if (i == 0 || i == 5) {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 2d + PADDING,
                            resultBitmapHeight / 2d);
                } else {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 4d, resultBitmapHeight / 2d);
                }
            } else if (imageCount == 7) {
                if (i == 1) {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 2d + PADDING,
                            resultBitmapHeight / 2d);
                } else {
                    bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 4d, resultBitmapHeight / 2d);
                }
            } else {
                bitmapToCanvas = getCroppedBitmap(sourceBitmap, resultBitmapWidth / 4d, resultBitmapHeight / 2d);
            }

            if (imageCount == 1) {
                canvas.drawBitmap(bitmapToCanvas, 0, 0, null);
            } else if (imageCount == 2) {
                canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 * i + PADDING * i, 0, null);
            } else if (imageCount == 3) {
                if (i == 0) {
                    canvas.drawBitmap(bitmapToCanvas, 0, 0, null);
                } else if (i == 1) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + PADDING * 2, 0, null);
                } else {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + PADDING * 2, resultBitmapHeight / 2
                            + PADDING, null);
                }
            } else if (imageCount == 4) {
                canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 4 * i + PADDING * i, 0, null);
            } else if (imageCount == 5) {
                if (i == 0) {
                    canvas.drawBitmap(bitmapToCanvas, 0, 0, null);
                } else if (i == 1 || i == 2) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + resultBitmapWidth / 4 * (i - 1) + PADDING
                            * (i + 1), 0, null);
                } else if (i == 3 || i == 4) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + resultBitmapWidth / 4 * (i - 3) + PADDING
                            * (i - 1), resultBitmapHeight / 2 + PADDING, null);
                }
            } else if (imageCount == 6) {
                if (i == 0) {
                    canvas.drawBitmap(bitmapToCanvas, 0, 0, null);
                } else if (i == 1 || i == 2) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + resultBitmapWidth / 4 * (i - 1) + PADDING
                            * (i + 1), 0, null);
                } else if (i == 3 || i == 4) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 4 * (i - 3) + PADDING * (i - 3),
                            resultBitmapHeight / 2 + PADDING, null);
                } else if (i == 5) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + PADDING * 2, resultBitmapHeight / 2
                            + PADDING, null);
                }
            } else if (imageCount == 7) {
                if (i == 0) {
                    canvas.drawBitmap(bitmapToCanvas, 0, 0, null);
                } else if (i == 1) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 4 + PADDING, 0, null);
                } else if (i == 2) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 2 + resultBitmapWidth / 4 + PADDING * 3, 0,
                            null);
                } else {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 4 * (i - 3) + PADDING * (i - 3),
                            resultBitmapHeight / 2 + PADDING, null);
                }
            } else {
                if (i < maxImageInLine) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 4 * i + PADDING * i, 0, null);
                } else if (i >= maxImageInLine && i < maxImageInCollage) {
                    canvas.drawBitmap(bitmapToCanvas, resultBitmapWidth / 4 * (i - maxImageInLine) + PADDING
                            * (i - maxImageInLine), resultBitmapHeight / 2 + PADDING, null);
                }
            }
        }

        resultBitmap = Bitmap.createScaledBitmap(resultBitmap, resultBitmapWidth, resultBitmapHeight, false);
        return resultBitmap;
    }

    public static Bitmap getCroppedBitmap(Bitmap source, double resultWidth, double resultHeight) {
        double coefficient;
        double sourceWidth = source.getWidth();
        double sourceHeight = source.getHeight();

        if (sourceHeight - resultHeight > sourceWidth - resultWidth) {
            coefficient = resultWidth / sourceWidth;
        } else {
            coefficient = resultHeight / sourceHeight;
        }

        sourceWidth = (coefficient * sourceWidth);
        sourceHeight = (coefficient * sourceHeight);

        int left = (int) -(sourceWidth - resultWidth) / 2;
        int top = (int) -(sourceHeight - resultHeight) / 2;

        Bitmap newSourceBitmap = Bitmap.createScaledBitmap(source, (int) sourceWidth, (int) sourceHeight, false);
        Bitmap bitmapToCanvas = Bitmap.createBitmap((int) resultWidth, (int) resultHeight, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapToCanvas);
        canvas.drawBitmap(newSourceBitmap, left, top, null);
        return bitmapToCanvas;
    }

    public static Bitmap getCircleBitmap(Bitmap source, double resultWidth, double resultHeight) {

        Bitmap resultBitmap = getCroppedBitmap(source.copy(Bitmap.Config.ARGB_8888, true), resultWidth, resultHeight);
        Canvas c = new Canvas(resultBitmap);
        Path p = new Path();
        p.addCircle(resultBitmap.getWidth() / 2F, resultBitmap.getHeight() / 2F, resultBitmap.getWidth() / 2F, Path.Direction.CW);
        c.clipPath(p, Region.Op.DIFFERENCE);
        c.drawColor(0x00000000, PorterDuff.Mode.CLEAR);

        return resultBitmap;
    }

    // decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f, int sizeType) {
        int bitmapSize = NORMAL_SIZE;
        int scale = 1;
        switch (sizeType) {
            case 0:
                bitmapSize = BIG_SIZE;
                break;
            case 1:
                bitmapSize = NORMAL_SIZE;
                break;
            case 2:
                bitmapSize = SMALL_SIZE;
                break;
            case 3:
                bitmapSize = BIG_SIZE_VAR;
                break;
            case 4:
                bitmapSize = NORMAL_SIZE_VAR;
                break;
            case 5:
                bitmapSize = SMALL_SIZE_VAR;
                break;
            default:
                break;
        }

        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(f.getAbsolutePath(), o);

            // Find the correct scale value. It should be the power of 2.
            int widthTmp = o.outWidth, heightTmp = o.outHeight;

            while (true) {
                if (widthTmp / 2 < bitmapSize || heightTmp / 2 < bitmapSize) {
                    break;
                }
                widthTmp /= 2;
                heightTmp /= 2;
                scale *= 2;
            }
            // LogUtils.e(TAG,
            // "BIG_SIZE_VAR: "+BIG_SIZE_VAR+" NORMAL_SIZE_VAR: "+NORMAL_SIZE_VAR+" SMALL_SIZE_VAR: "+SMALL_SIZE_VAR);
            // LogUtils.e(TAG, "width_tmp: "+width_tmp+" height_tmp: "+height_tmp+" scale: "+scale);
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(f.getAbsolutePath(), o2);
        } catch (Exception e) {
            L.e(TAG, "OutOfMemory", e);
        }
        return null;
    }

    public Bitmap decodeBitmap(Bitmap b, int sizeType) {
        int bitmapSize = NORMAL_SIZE;
        int scale = 1;
        switch (sizeType) {
            case 0:
                bitmapSize = BIG_SIZE;
                break;
            case 1:
                bitmapSize = NORMAL_SIZE;
                break;
            case 2:
                bitmapSize = SMALL_SIZE;
                break;
            case 3:
                bitmapSize = BIG_SIZE_VAR;
                break;
            case 4:
                bitmapSize = NORMAL_SIZE_VAR;
                break;
            case 5:
                bitmapSize = SMALL_SIZE_VAR;
                break;
            default:
                break;
        }

        try {

            // Find the correct scale value. It should be the power of 2.
            int widthTmp = b.getWidth(), heightTmp = b.getHeight();
            while (true) {
                if (widthTmp < bitmapSize || heightTmp < bitmapSize) {
                    break;
                }
                widthTmp /= 2;
                heightTmp /= 2;
                scale *= 2;
            }

            return Bitmap.createScaledBitmap(b, b.getWidth() / scale, b.getHeight() / scale, true);
        } catch (Exception e) {
            L.e(TAG, "OutOfMemory", e);
        }
        return null;
    }

    // Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;
        public boolean anim;
        public boolean gone;
        public int sizeType;

        public PhotoToLoad(String u, ImageView i, int s, boolean a, boolean g) {
            this.url = u;
            this.imageView = i;
            this.anim = a;
            this.gone = g;
            this.sizeType = s;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad)) {
                return;
            }
            Bitmap bmp = null;
            String[] urlList = photoToLoad.url.split(",,");
            if (urlList.length > 1) {
                bmp = getStageBitmap(urlList, photoToLoad.sizeType);
            } else {
                bmp = getBitmap(photoToLoad.url, photoToLoad.imageView, photoToLoad.sizeType);
            }

            memoryCache.put(photoToLoad.url, bmp);

            if (imageViewReused(photoToLoad)) {
                return;
            }
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    class BitmapLoader implements Runnable {
        private String url;
        private OnFetchCompleteListener listener;
        private int sizeType;

        BitmapLoader(String url, int sizeType, OnFetchCompleteListener listener) {
            this.url = url;
            this.listener = listener;
            this.sizeType = sizeType;
        }

        @Override
        public void run() {
            Bitmap bmp = getBitmap(url, null, sizeType);
            memoryCache.put(url, bmp);

            if (listener != null) {
                listener.onFetchComplete(bmp);
            }

        }

    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url)) {
            return true;
        }
        return false;
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad)) {
                return;
            }
            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
                photoToLoad.imageView.setVisibility(View.VISIBLE);
                if (photoToLoad.anim) {
                    Animation fadeIn = new AlphaAnimation(0.2f, 1f);
                    fadeIn.setInterpolator(new DecelerateInterpolator());
                    fadeIn.setDuration(800);
                    photoToLoad.imageView.setAnimation(fadeIn);
                }
            } else {
                if (photoToLoad.gone) {
                    photoToLoad.imageView.setVisibility(View.GONE);
                }
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        FileCache.getInstance().clear();
    }

    public File getRawRsourceAsFile(Activity activity, int resId) {
        File f = FileCache.getInstance().getFileByPath(String.valueOf(resId));

        if (f.exists()) {
            return f;
        }

        InputStream is;
        OutputStream os;
        try {
            is = activity.getResources().openRawResource(resId);
            os = new FileOutputStream(f);
            Utils.copyStream(is, os);
            os.close();
        } catch (Exception e) {
            L.e(TAG, "error copyRsourceToFile", e);
        }

        return f;
    }

    public Bitmap getBitmapByPath(String path, int sizeType) {
        if (!TextUtils.isEmpty(path)) {
            return decodeFile(new File(path), sizeType);
        } else {
            return BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.no_widget_photo);
        }
    }
}
