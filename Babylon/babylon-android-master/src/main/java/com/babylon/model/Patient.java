package com.babylon.model;

import android.text.TextUtils;
import android.text.format.DateUtils;
import com.babylon.enums.Gender;
import com.babylon.model.payments.*;
import com.babylon.utils.L;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Patient extends BaseModel {
    private static final String TAG = Patient.class.getSimpleName();
    @Expose
    private String id;
    @Expose
    private String email;

    @SerializedName("email_confirmation")
    @Expose
    private String emailConfirmation;

    /**
     * Field is obtained from header. It's expired in 30 min.
     * {@link #setToken }
     */
    @Expose
    private String token;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @Expose
    private String password;
    @Expose
    private String gender;
    @SerializedName("agreed_terms_and_conditions")
    @Expose
    private Boolean agreedTermsAndConditions;
    @SerializedName("date_of_birth")
    private Date birthday;
    @SerializedName("smoking_status")
    @Expose
    private String smokingStatus;
    @Expose
    private String height;
    @Expose
    private String weight;

    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("installation_id")
    @Expose
    private String installationId;
    @SkipFieldInContentValues
    @Expose
    private boolean isUpdated = false;
    @SerializedName("is_active")
    @Expose
    private boolean isActive;
    @SerializedName("facebook_access_token")
    @Expose
    private String facebookAccessToken;
    @SerializedName("facebook_user_id")
    @Expose
    private String facebookUserId;
    @SerializedName("region_id")
    @Expose
    private Integer regionId;
    @SerializedName("practice_id")
    @Expose
    private Integer practiceId;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("phone_country_code")
    @Expose
    private String countryCode;
    @Expose
    private Subscription subscription;
    @Expose
    private Promotion promotion;
    @SerializedName("redemptions_attributes")
    @Expose
    private List<PromoCode> redemptionAttributes;
    @Expose
    private String avatar;
    @SerializedName("is_minor")
    @Expose
    private Boolean isMinor;
    @SerializedName("submitted_at")
    @Expose
    private String submittedAt;
    @Expose
    private int switchedToFamilyAccountId;
    @SerializedName("last_used_credit_card")
    @Expose
    private PaymentCard lastUsedCreditCard;
    @SerializedName("insurance_company_id")
    private int insuranceCompanyId;
    @SerializedName("insurance_membership_id")
    private int insuranceMembershipId;
    @SerializedName("insurance_membership_number")
    private String insuranceMembershipNumber;
    @SerializedName("address_post_code")
    private  String addressPostCode;
    @SerializedName("account_state")
    private  String accountState;

    public int getAge() {
        long birthdayInMillis = getBirthdayTime();

        long diff = System.currentTimeMillis() - birthdayInMillis;
        int result = (int) (diff / DateUtils.YEAR_IN_MILLIS);

        return result;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setIsUpdated(boolean isUpdated) {
        if (this.isUpdated != isUpdated) {
            this.isUpdated = isUpdated;
        }

        if (isUpdated) {
            submittedAt = com.babylon.utils.DateUtils.getIso8601DateFormat(new Date());
        }
    }

    public boolean isUpdated() {
        return isUpdated;
    }

    public String getInstallationId() {
        return installationId;
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        L.d(TAG, "setToken(); token = " + token);
    }

    public String getFirstName()  {

        return  firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAgreedTermsAndConditions() {
        return agreedTermsAndConditions;
    }

    public void setAgreedTermsAndConditions(Boolean agreedTermsAndConditions) {
        this.agreedTermsAndConditions = agreedTermsAndConditions;
    }

    public Date getBirthday() {
        return birthday;
    }

    public long getBirthdayTime() {
        long result;

        if (birthday == null) {
            result = new Date().getTime();
        } else {
            result = birthday.getTime();
        }

        return result;
    }

    public void setBirthday(Date newBirthday) {
        this.birthday = newBirthday;
    }

    public String getSmokingStatus() {
        return getNotNullString(smokingStatus);
    }

    public void setSmokingStatus(String smokingStatus) {
        this.smokingStatus = smokingStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Float getHeight() {
        return getFloat(height);
    }

    public void setHeight(Float height) {
        if (height > 0) {
            this.height = String.valueOf(height);
        } else {
            this.height = "";
        }
    }

    public Float getWeight() {
        return getFloat(weight);
    }

    public void setWeight(Float weight) {
        if (weight > 0) {
            this.weight = String.valueOf(weight);
        } else {
            this.weight = "";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "Patient{"
                + "firstName='" + firstName + '\''
                + ", email='" + email + '\''
                + ", smoker='" + smokingStatus + '\''
                + ", gender='" + gender + '\''
                + ", birthday=" + birthday
                + ", isUpdated=" + isUpdated
                + '}';
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean hasPhoneNumber() {
        return !TextUtils.isEmpty(phoneNumber);
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getEmailConfirmation() {
        return emailConfirmation;
    }

    public void setEmailConfirmation(String emailConfirmation) {
        this.emailConfirmation = emailConfirmation;
    }

    public boolean isInfoFilled() {
        return Gender.isGenderFilled(gender) && birthday != null
                && getHeight() != 0f && getWeight() != 0f;
    }

    public String getFacebookAccessToken() {
        return facebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public Integer getRegionId() {
        return getInt(regionId);
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public Integer getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Integer practiceId) {
        this.practiceId = practiceId;
    }

    public Plan getPlan() {
        return subscription.getPlan();
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public List<PromoCode> getRedemptionAttributes() {
        return redemptionAttributes;
    }

    public void setRedemptionAttributes(List<PromoCode> redemptionAttributes) {
        this.redemptionAttributes = redemptionAttributes;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getIsMinor() {
        return isMinor;
    }

    public void setIsMinor(Boolean isMinor) {
        this.isMinor = isMinor;
    }

    public int getSwitchedToFamilyAccountId() {
        return switchedToFamilyAccountId;
    }

    public void setSwitchedToFamilyAccountId(int switchedToFamilyAccountId) {
        this.switchedToFamilyAccountId = switchedToFamilyAccountId;
    }

    public String getSubmittedAt() {
        return submittedAt;
    }

    public PaymentCard getLastUsedCreditCard() {
        return lastUsedCreditCard;
    }

    public void setLastUsedCreditCard(PaymentCard lastUsedCreditCard) {
        this.lastUsedCreditCard = lastUsedCreditCard;
    }

    public void setInsuranceMembershipNumber(String insuranceMembershipNumber) {
        this.insuranceMembershipNumber = insuranceMembershipNumber;
    }

    public String getInsuranceMembershipNumber() {
        return insuranceMembershipNumber;
    }

    public void setInsuranceMembershipId(int insuranceMembershipId) {
        this.insuranceMembershipId = insuranceMembershipId;
    }

    public int getInsuranceMembershipId() {
        return insuranceMembershipId;
    }

    public void setInsuranceCompanyId(int insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }

    public int getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setAddressPostCode(String addressPostCode) {
        this.addressPostCode = addressPostCode;
    }

    public String getAddressPostCode() {
        return addressPostCode;
    }
    public void setAccountState(String state){
        this.accountState=state;
    }
 public String getAccountState(){
    return accountState;
}




}
