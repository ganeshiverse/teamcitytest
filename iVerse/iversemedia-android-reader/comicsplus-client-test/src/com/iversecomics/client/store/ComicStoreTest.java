package com.iversecomics.client.store;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import com.iversecomics.client.AbstractStubActivityTestCase;
import com.iversecomics.client.store.db.ComicStoreDBUpdater;
import com.iversecomics.client.store.db.ComicsTable;
import com.iversecomics.client.store.db.GenresTable;
import com.iversecomics.client.store.db.ListID;
import com.iversecomics.client.store.db.OrderingUpdater;
import com.iversecomics.client.store.db.PublishersTable;
import com.iversecomics.client.store.json.ResponseParser;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.util.DBUtil;
import com.iversecomics.io.IOUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.util.text.StringUtils;

public class ComicStoreTest extends AbstractStubActivityTestCase {
    private static boolean dbInitialized = false;
    private String         sciFiGenreId;
    private String         boomPublisherId;
    private String         seriesId;

    private void addTestData() throws ParseException, IOException, JSONException {
        seriesId = "com.iversecomics.boom.studios.twenty.eight.days.later.series";
        sciFiGenreId = "9";
        boomPublisherId = "boom.studios";

        if (dbInitialized) { // don't bother reinitializing DB.
            return;
        }

        SQLiteDatabase db = getEmptyDB(getActivity());
        ContentResolver resolver = getActivity().getContentResolver();

        ResponseParser parser = new ResponseParser();
        ComicStoreDBUpdater updater;
        JSONObject json;

        // Add New Releases
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.NEW_RELEASES));
        json = loadAssetJsonObject("api2-products-new-releases.json");
        parser.parseComics(json, updater);

        // Add Top Free
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.TOP_FREE));
        json = loadAssetJsonObject("api2-products-top-free.json");
        parser.parseComics(json, updater);

        // Add Top Paid
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.TOP_PAID));
        json = loadAssetJsonObject("api2-products-top-paid.json");
        parser.parseComics(json, updater);

        // Add Comics By Featured
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.FEATURED_COMICS));
        json = loadAssetJsonObject("api2-products-by-featured.json");
        parser.parseComics(json, updater);

        // Add Comics By Series
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.getComicsBySeries(seriesId)));
        json = loadAssetJsonObject("api2-products-by-series-28days.json");
        parser.parseComics(json, updater);

        // Add Comics By Genre
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.getComicsByGenre(sciFiGenreId)));
        json = loadAssetJsonObject("api2-products-by-genre-scifi.json");
        parser.parseComics(json, updater);

        // Add Comics By Publisher
        updater = new ComicStoreDBUpdater(db, resolver);
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.getComicsByPublisher(boomPublisherId)));
        json = loadAssetJsonObject("api2-products-by-publisher-boom.json");
        parser.parseComics(json, updater);

        // Add Genres
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.TOP_PAID));
        json = loadAssetJsonObject("api2-categories-list.json");
        parser.parseGenres(json, updater);

        // Add Publishers
        updater.addComicDBSubUpdater(new OrderingUpdater(ListID.TOP_PAID));
        json = loadAssetJsonObject("api2-supplier-list.json");
        parser.parsePublishers(json, updater);

        dbInitialized = true;
    }

    private void assertExpectedStrings(String[] expectedNames, String[] actualNames) {
        boolean sizeMismatch = (expectedNames.length != actualNames.length);

        int mismatchIdx = -1;
        int len = Math.min(expectedNames.length, actualNames.length);
        for (int i = 0; i < len; i++) {
            if (!expectedNames[i].equals(actualNames[i])) {
                mismatchIdx = i;
                break;
            }
        }

        if ((mismatchIdx >= 0) || (sizeMismatch)) {
            dumpIdList("Expected Names", expectedNames);
            dumpIdList("Actual Names", actualNames);
            if (sizeMismatch && (mismatchIdx < 0)) {
                mismatchIdx = len;
            }
            StringBuilder err = new StringBuilder();
            err.append("Mismatch in lists encountered. [at index ").append(mismatchIdx);
            err.append("]");
            if (sizeMismatch) {
                err.append(" (expected.length=").append(expectedNames.length);
                err.append(", actual.length=").append(actualNames.length);
                err.append(")");
            }
            assertTrue(err.toString(), mismatchIdx < 0);
        }
    }

    private String[] collectComicNames(Cursor c) {
        int rowcount = c.getCount();
        String actualNames[] = new String[rowcount];
        for (int i = 0; i < rowcount; i++) {
            Comic actualComic = ComicsTable.fromCursor(c);
            actualNames[i] = actualComic.getName();
            c.moveToNext();
        }
        return actualNames;
    }

    private String[] collectGenreNames(Cursor c) {
        int rowcount = c.getCount();
        String actualNames[] = new String[rowcount];
        for (int i = 0; i < rowcount; i++) {
            Genre actualGenre = GenresTable.fromCursor(c);
            actualNames[i] = actualGenre.getName();
            c.moveToNext();
        }
        return actualNames;
    }

    private String[] collectPublisherNames(Cursor c) {
        int rowcount = c.getCount();
        String actualNames[] = new String[rowcount];
        for (int i = 0; i < rowcount; i++) {
            Publisher actualPublisher = PublishersTable.fromCursor(c);
            actualNames[i] = actualPublisher.getName();
            c.moveToNext();
        }
        return actualNames;
    }

    private void dumpIdList(String header, String[] ids) {
        String tag = "TEST";
        Log.e(tag, header);
        if (ids == null) {
            Log.e(tag, "  <NULL>");
        } else {
            Log.e(tag, " count[" + ids.length + "]");
            Log.e(tag, "--------------------------");
            for (int i = 0; i < ids.length; i++) {
                Log.e(tag, "[" + i + "] \"" + ids[i] + "\"");
            }
        }
    }

    private String[] loadExpectedStrings(String filename) throws IOException {
        List<String> strlist = new ArrayList<String>();
        AssetManager am = getInstrumentation().getContext().getAssets();
        InputStream stream = null;
        InputStreamReader reader = null;
        BufferedReader buf = null;
        try {
            stream = am.open(filename);
            reader = new InputStreamReader(stream);
            buf = new BufferedReader(reader);
            String line;
            ;
            while ((line = buf.readLine()) != null) {
                if (StringUtils.isBlank(line)) {
                    continue; // skip empty
                }
                strlist.add(line);
            }
            int len = strlist.size();
            return strlist.toArray(new String[len]);
        } finally {
            IOUtil.close(reader);
            IOUtil.close(stream);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicByBundleName() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Comic comic;

        // Fetch bad id
        comic = comicStore.getDatabaseApi().getComicByBundleName("foo.bar");
        assertNull("Bad comicId should result in a null comic", comic);

        // Fetch known good id (that arrived via new releases)
        comic = comicStore.getDatabaseApi().getComicByBundleName(
                "com.iversecomics.archie.comics.betty.one.hundred.three.07132011");
        assertNotNull("comicId should result in a comic", comic);
        assertEquals("comic.name", "Betty #103", comic.getName());
    }

    @MediumTest
    public void testDatabaseApiGetComicById() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Comic comic;

        // Fetch bad id
        comic = comicStore.getDatabaseApi().getComicById("999999999");
        assertNull("Bad comicId should result in a null comic", comic);

        // Fetch known good id
        comic = comicStore.getDatabaseApi().getComicById("529");
        assertNotNull("comicId should result in a comic", comic);
        assertEquals("comic.name", "Betty #103", comic.getName());
    }

    @MediumTest
    public void testDatabaseApiGetComicsByGenre() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsByGenre(sciFiGenreId);
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-by-genre-scifi.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicsByPublisher() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsByPublisher(boomPublisherId);
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-by-publisher-boom.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicsBySeries() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsBySeries(seriesId);
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-by-series-28days.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicsFeatured() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsFeatured(4);
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-by-featured.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicsNew() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsNew();
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-new-releases.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicsTopFree() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsTopFree();
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-top-free.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetComicsTopPaid() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorComicsTopPaid();
            assertTrue("Comics List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectComicNames(c);
            String expectedNames[] = loadExpectedStrings("expected-products-top-paid.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetGenresAll() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorGenresAll();
            assertTrue("Genres List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectGenreNames(c);
            String expectedNames[] = loadExpectedStrings("expected-categories.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetPublishersAll() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorPublishersAll();
            assertTrue("Publishers List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectPublisherNames(c);
            String expectedNames[] = loadExpectedStrings("expected-suppliers-list.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

    @MediumTest
    public void testDatabaseApiGetPublishersFeatured() throws Exception {
        startStubActivity();
        addTestData();

        ComicStore comicStore = getComicStore();
        Cursor c = null;
        try {
            c = comicStore.getDatabaseApi().getCursorPublishersFeatured();
            assertTrue("Publishers List (should be able to move cursor to first row)", c.moveToFirst());

            String actualNames[] = collectPublisherNames(c);
            String expectedNames[] = loadExpectedStrings("expected-suppliers-featured.txt");
            assertExpectedStrings(expectedNames, actualNames);
        } finally {
            DBUtil.close(c);
        }
    }

}
