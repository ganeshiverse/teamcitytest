package com.axsystech.excelicare.data.responses;

import android.text.TextUtils;

import com.axsystech.excelicare.util.AxSysConstants;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Date: 18.06.14
 * Time: 13:19
 *
 * @author SomeswarReddy
 */
public class RegisterResponse extends BaseResponse implements Serializable {

    @SerializedName("ClientURL")
    private String clientUrl;

    @SerializedName("data")
    private DataObject dataObject;

    public String getClientUrl() {
        return clientUrl;
    }

    public DataObject getDataObject() {
        return dataObject;
    }

    public class DataObject implements Serializable {
        @SerializedName("CurrentUser")
        private CurrentUserObject currentUser;

        @SerializedName("DeviceUsers")
        private DeviceUsersObject deviceUser;

        @SerializedName("SystemPreferences")
        private SystemPreferencesObject systemPreferences;

        public CurrentUserObject getCurrentUser() {
            return currentUser;
        }

        public DeviceUsersObject getDeviceUser() {
            return deviceUser;
        }

        public SystemPreferencesObject getSystemPreferences() {
            return systemPreferences;
        }
    }

    public class CurrentUserObject implements Serializable {
        @SerializedName("data")
        private CurrentUserData currentUserData;

        public CurrentUserData getCurrentUserData() {
            return currentUserData;
        }
    }

    public class CurrentUserData implements Serializable {
        @SerializedName("Forename")
        private String forename;

        @SerializedName("NavigateTo")
        private String navigateTo;

        @SerializedName("Surname")
        private String surname;

        @SerializedName("token")
        private String token;

        public String getForename() {
            return forename;
        }

        public String getNavigateTo() {
            return navigateTo;
        }

        public String getSurname() {
            return surname;
        }

        public String getToken() {
            return token;
        }
    }

    public class DeviceUsersObject implements Serializable {

        @SerializedName("data")
        private ArrayList<DeviceUserDataObject> deviceUserDataObjectsAL;

        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public ArrayList<DeviceUserDataObject> getDeviceUserDataObjectsAL() {
            return deviceUserDataObjectsAL;
        }
    }

    public class DeviceUserDataObject implements Serializable {
        @SerializedName("Address")
        private String address;

        @SerializedName("DOB")
        private String dob;

        @SerializedName("DeviceID")
        private String deviceID;

        @SerializedName("EcUserID")
        private String ecUserID;

        @SerializedName("Forename")
        private String forename;

        @SerializedName("loginName")
        private String loginName;

        @SerializedName("MobileNo")
        private String mobileNo;

        @SerializedName("Primary_Identifier")
        private String primary_Identifier;

        @SerializedName("Sex")
        private String sex;

        @SerializedName("SiteID")
        private String siteID;

        @SerializedName("Surname")
        private String surname;

        @SerializedName("token")
        private String token;

        @SerializedName("UserDeviceID")
        private String userDeviceID;

        public String getAddress() {
            return address;
        }

        public String getDob() {
            return dob;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public String getEcUserID() {
            return ecUserID;
        }

        public String getForename() {
            return forename;
        }

        public String getLoginName() {
            return loginName;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public String getPrimary_Identifier() {
            return primary_Identifier;
        }

        public String getSex() {
            return sex;
        }

        public String getSiteID() {
            return siteID;
        }

        public String getSurname() {
            return surname;
        }

        public String getToken() {
            return token;
        }

        public String getUserDeviceID() {
            return userDeviceID;
        }
    }
}
