package com.babylon.images;

import android.graphics.Bitmap;

import com.babylon.App;
import com.babylon.BaseConfig;
import com.babylon.utils.L;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCache {

    private static final String TAG = FileCache.class.getSimpleName();

    private File cacheDir;

    private static FileCache sFileCache;

    public static FileCache getInstance() {
        if( sFileCache==null ) {
            sFileCache = new FileCache();
        }
        return sFileCache;
    }

    private FileCache() {
        // Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir = new File(BaseConfig.CACHE_DIR, "images");
        } else {
            cacheDir = App.getInstance().getCacheDir();
        }
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
    }

    private String generateFileName() {
        String curDate = String.valueOf(System.currentTimeMillis());
        String path = cacheDir.getPath() + "/" + curDate + ".jpg";
        return path;
    }

    public String save(Bitmap b) {
        final String filename = generateFileName();

        save(b, filename);

        return filename;
    }

    public void save(Bitmap b, String filename) {
        try {
            final FileOutputStream stream = new FileOutputStream(filename, false);
            b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        } catch (FileNotFoundException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }

    public File getFileByPath(String path) {
        File f = new File(path);
        if (!f.exists()) {
            String filename = String.valueOf(path.hashCode());
            f = new File(cacheDir, filename);
        }
        return f;

    }

    public File getFileByUrl(String url) {
        String filename = String.valueOf(url.hashCode());
        File f = new File(cacheDir, filename);

        return f;
    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null) {
            return;
        }
        for (File f : files) {
            f.delete();
        }
    }

    public byte[] readFile(String path) {
        FileInputStream fileInputStream=null;

        File file = new File(path);

        byte[] bFile = new byte[(int) file.length()];

        try {
            //convert file into array of bytes
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);

            for (int i = 0; i < bFile.length; i++) {
                System.out.print((char)bFile[i]);
            }

            System.out.println("Done");
        }catch(Exception e){
            L.e(TAG, e.toString(), e);
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        }

        return bFile;
    }
}
