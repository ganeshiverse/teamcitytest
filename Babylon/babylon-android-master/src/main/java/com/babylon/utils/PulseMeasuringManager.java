package com.babylon.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.SurfaceHolder;

import com.babylon.App;
import com.babylon.R;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Singleton manager, which help measure pulse with flashlight and camera
 */
public final class PulseMeasuringManager {

    public enum PULSE_BEAT_TYPE {
        GREEN, RED
    }

    private static PulseMeasuringManager instance;

    public static final String EXTRA_PART = "EXTRA_PART";
    public static final String EXTRA_AMOUNT = "EXTRA_AMOUNT";
    public static final int PULSE_MEASURE_UPDATE = 4573;
    public static final int PULSE_MEASURE_STOP = 4574;
    public static final int PULSE_MEASURE_CLEAN = 4575;

    public static final float FINGER_RED_AVERAGE_START_COLOR = 178.5f;
    public static final int BEAT_DELAY = 10;
    public static final float TOTAL_TIME_MS = 12000f;
    public static final int COLOR_MAX_VALUE = 255;
    public static final double SECOND_IN_MS = 1000d;
    public static final double SECONDS_IN_MIN = 60d;

    private Timer timer;
    private Handler handler;
    private SurfaceHolder surfaceHolder;
    private Camera camera = null;
    private PowerManager.WakeLock wakeLock = null;

    private boolean isMeasuringFinished;
    private boolean isFingerOnCamera;
    private AtomicBoolean processing = new AtomicBoolean(false);

    private PULSE_BEAT_TYPE currentType = PULSE_BEAT_TYPE.GREEN;
    private int averageIndex = 0;
    private final int averageArraySize = 4;
    private int[] averageArray = new int[averageArraySize];
    private int beatsIndex = 0;
    private final int beatsArraySize = 3;
    private int[] beatsArray = new int[beatsArraySize];
    private double beats = 0;
    private long totalTime;
    private int pulseAmount;

    private PulseMeasuringManager() {
        //singleton
    }

    public static synchronized PulseMeasuringManager getInstance() {
        return getInstance(false);
    }

    public static synchronized PulseMeasuringManager getInstance(boolean recreateObject) {
        if (instance == null || recreateObject) {
            instance = new PulseMeasuringManager();
        }
        return instance;
    }

    public boolean isMeasuringFinished() {
        return isMeasuringFinished;
    }

    public int getPulseAmount() {
        return pulseAmount;
    }

    public void initialize(Activity activity, SurfaceHolder surfaceHolder,
                           Handler handler) {
        if (isCameraFeaturesAvailable()) {
            this.surfaceHolder = surfaceHolder;
            PowerManager powerManager = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
            wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
            wakeLock.acquire();
            if (camera == null) {
                camera = Camera.open();
            }
            if (surfaceHolder != null) {
                surfaceHolder.addCallback(surfaceCallback);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            }
            this.handler = handler;
        }
    }

    public void onActivityPause() {
        saveViewsState();

        if (isCameraFeaturesAvailable()) {
            if (processing.get()) {
                stopMeasuringPulse(0);
            }
            wakeLock.release();
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    private void saveViewsState() {
        if (!isMeasuringFinished) {
            if (pulseAmount == 0) {
                cleanMeasureView();
            } else {
                stopMeasuringPulse(pulseAmount);
            }
        }
    }

    public boolean startMeasuringPulse(Activity activity) {
        if (isCameraFeaturesAvailable()) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(parameters);
            camera.setPreviewCallback(previewCallback);
            camera.startPreview();
            startHeartBeatTimer();
            isMeasuringFinished = false;
        } else {
            AlertDialogHelper.getInstance().showMessage(activity, R.string.resting_pulse_fragment_error);
        }
        return isCameraFeaturesAvailable();
    }

    private void stopMeasuringPulse(int pulseAmount) {
        isMeasuringFinished = true;
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera.setParameters(parameters);
        camera.stopPreview();
        camera.setPreviewCallback(null);

        beats = 0;
        averageIndex = 0;
        currentType = PULSE_BEAT_TYPE.GREEN;
        beatsIndex = 0;
        averageArray = new int[averageArraySize];
        beatsArray = new int[beatsArraySize];
        totalTime = 0;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        this.pulseAmount = pulseAmount;

        if (handler != null) {
            Message message = new Message();
            message.what = PULSE_MEASURE_STOP;
            Bundle extras = new Bundle();
            extras.putInt(EXTRA_AMOUNT, pulseAmount);
            message.setData(extras);
            handler.sendMessage(message);
        }
    }

    private void cleanMeasureView() {
        if (camera != null) {
            isMeasuringFinished = true;
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            camera.stopPreview();
            camera.setPreviewCallback(null);

            beats = 0;
            averageIndex = 0;
            currentType = PULSE_BEAT_TYPE.GREEN;
            beatsIndex = 0;
            averageArray = new int[averageArraySize];
            beatsArray = new int[beatsArraySize];
            totalTime = 0;
            if (timer != null) {
                timer.cancel();
                timer = null;
            }

            this.pulseAmount = 0;

            if (handler != null) {
                Message message = new Message();
                message.what = PULSE_MEASURE_CLEAN;
                Bundle extras = new Bundle();
                extras.putInt(EXTRA_AMOUNT, pulseAmount);
                message.setData(extras);
                handler.sendMessage(message);
            }
        }
    }


    private void startHeartBeatTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isFingerOnCamera) {
                    totalTime += BEAT_DELAY;
                    float part = totalTime / TOTAL_TIME_MS;
                    if (handler != null) {
                        Message message = new Message();
                        message.what = PULSE_MEASURE_UPDATE;
                        Bundle extras = new Bundle();
                        extras.putFloat(EXTRA_PART, part);
                        message.setData(extras);
                        handler.sendMessage(message);
                    }
                }
            }
        }, 0, BEAT_DELAY);
    }

    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data != null) {
                Camera.Size size = cam.getParameters().getPreviewSize();
                if (size != null) {
                    if (!processing.compareAndSet(false, true)) {
                        return;
                    }

                    int width = size.width;
                    int height = size.height;

                    int imgAvg = HeartRateImageProcessingUtil.decodeYUV420SPtoRedAvg(data.clone(), height, width);
                    isFingerOnCamera = !(imgAvg < FINGER_RED_AVERAGE_START_COLOR || imgAvg == COLOR_MAX_VALUE);
                    if (!isFingerOnCamera) {
                        processing.set(false);
                        return;
                    }

                    int averageArrayAvg = 0;
                    int averageArrayCnt = 0;
                    for (int anAverageArray : averageArray) {
                        if (anAverageArray > 0) {
                            averageArrayAvg += anAverageArray;
                            averageArrayCnt++;
                        }
                    }

                    int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
                    PULSE_BEAT_TYPE newType = currentType;
                    if (imgAvg < rollingAverage) {
                        newType = PULSE_BEAT_TYPE.RED;
                        if (newType != currentType) {
                            beats++;
                        }
                    } else if (imgAvg > rollingAverage) {
                        newType = PULSE_BEAT_TYPE.GREEN;
                    }

                    if (averageIndex == averageArraySize) {
                        averageIndex = 0;
                    }
                    averageArray[averageIndex] = imgAvg;
                    averageIndex++;

                    // Transitioned from one state to another to the same
                    if (newType != currentType) {
                        currentType = newType;
                    }

                    if (totalTime > TOTAL_TIME_MS) {
                        double bps = (beats / (totalTime / SECOND_IN_MS));
                        int dpm = (int) (bps * SECONDS_IN_MIN);
                        if (beatsIndex == beatsArraySize) {
                            beatsIndex = 0;
                        }
                        beatsArray[beatsIndex] = dpm;
                        beatsIndex++;

                        int beatsArrayAvg = 0;
                        int beatsArrayCnt = 0;
                        for (int aBeatsArray : beatsArray) {
                            if (aBeatsArray > 0) {
                                beatsArrayAvg += aBeatsArray;
                                beatsArrayCnt++;
                            }
                        }

                        int beatsAvg = beatsArrayCnt == 0 ? 0 : (beatsArrayAvg / beatsArrayCnt);
                        stopMeasuringPulse(beatsAvg);
                    }
                    processing.set(false);
                }
            }
        }
    };

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                if (camera != null && surfaceHolder != null) {
                    camera.setPreviewDisplay(surfaceHolder);
                }
            } catch (IOException e) {
                Log.i("PreviewDemo-surfaceCallback", "Exception in setPreviewDisplay()", e);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (camera != null) {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = getSmallestPreviewSize(width, height, parameters);
                if (size != null) {
                    parameters.setPreviewSize(size.width, size.height);
                }
                camera.setParameters(parameters);
                camera.startPreview();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;
        List<Camera.Size> cameraSizes = parameters.getSupportedPreviewSizes();
        if (cameraSizes != null) {
            for (Camera.Size size : cameraSizes) {
                if (size.width <= width && size.height <= height) {
                    if (result == null) {
                        result = size;
                    } else {
                        int resultArea = result.width * result.height;
                        int newArea = size.width * size.height;
                        if (newArea < resultArea) {
                            result = size;
                        }
                    }
                }
            }
        }
        return result;
    }

    private boolean isCameraFeaturesAvailable() {
        PackageManager packageManager = App.getInstance().getPackageManager();
        return packageManager != null
                && packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
                && packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
}
