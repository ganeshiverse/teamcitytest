package com.iversecomics.util.text;

import java.util.ArrayList;
import java.util.List;

public final class WordUtil {
    public static List<String> wrap(String str, int wrapLength, boolean wrapLongWords) {
        if (str == null) {
            return null;
        }
        int inputLineLength = str.length();
        int offset = 0;

        List<String> lines = new ArrayList<String>();

        while ((inputLineLength - offset) > wrapLength) {
            if (str.charAt(offset) == ' ') {
                offset++;
                continue;
            }
            int spaceToWrapAt = str.lastIndexOf(' ', wrapLength + offset);

            if (spaceToWrapAt >= offset) {
                // normal case
                lines.add(str.substring(offset, spaceToWrapAt));
                offset = spaceToWrapAt + 1;

            } else {
                // really long word or URL
                if (wrapLongWords) {
                    // wrap really long word one line at a time
                    lines.add(str.substring(offset, wrapLength + offset));
                    offset += wrapLength;
                } else {
                    // do not wrap really long word, just extend beyond limit
                    spaceToWrapAt = str.indexOf(' ', wrapLength + offset);
                    if (spaceToWrapAt >= 0) {
                        lines.add(str.substring(offset, spaceToWrapAt));
                        offset = spaceToWrapAt + 1;
                    } else {
                        lines.add(str.substring(offset));
                        offset = inputLineLength;
                    }
                }
            }
        }

        // Whatever is left in line is short enough to just pass through
        lines.add(str.substring(offset));

        return lines;
    }

    private WordUtil() {
        /* prevent instantiation */
    }
}
