package com.iversecomics.client.store.events;

public interface IGenreChooser {
    OnGenreClickListener getOnGenreClickListener();

    void setOnGenreClickListener(OnGenreClickListener listener);
}
