package com.babylon.api.request;

import android.text.TextUtils;

import com.babylon.App;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class PatientGet extends AbsRequestGet<Patient> {

    private String patientId;

    public PatientGet(String patientId) {
        this.patientId = patientId;
    }

    public PatientGet() {
        patientId = App.getInstance().getPatient().getId();
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        String url = Urls.getRuby(Urls.PATIENTS);
        return TextUtils.concat(url, "/", patientId).toString();
    }

    @Override
    protected AbsResponseProcessor<Patient> getResponseProcessor() {
        return new RubyResponseProcessor<Patient>(getOutputType());
    }
}
