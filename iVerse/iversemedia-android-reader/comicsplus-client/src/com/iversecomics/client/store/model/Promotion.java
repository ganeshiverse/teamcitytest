package com.iversecomics.client.store.model;

public class Promotion {
	
	int promotionID;
	String name;
	String imageURLString;
	String imageURLPhoneString;
	String targetURLString;
	boolean active;
	
	
	public Promotion()
	{
		
	}

	public int getPromotionID() {
		return promotionID;
	}

	public void setPromotionID(int promotionID) {
		this.promotionID = promotionID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageURLString() {
		return imageURLString;
	}

	public void setImageURLString(String imageURLString) {
		this.imageURLString = imageURLString;
	}

	public String getImageURLPhoneString() {
		return imageURLPhoneString;
	}

	public void setImageURLPhoneString(String imageURLPhoneString) {
		this.imageURLPhoneString = imageURLPhoneString;
	}

	public String getTargetURLString() {
		return targetURLString;
	}

	public void setTargetURLString(String targetURLString) {
		this.targetURLString = targetURLString;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
