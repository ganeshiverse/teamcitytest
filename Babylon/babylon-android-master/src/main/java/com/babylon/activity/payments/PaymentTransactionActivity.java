package com.babylon.activity.payments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.activity.BaseActivity;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.AddingCreditCardPost;
import com.babylon.api.request.payments.CreditCardsListGet;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.fragment.BaseFragment;
import com.babylon.fragment.payments.AddingPaymentCardFragment;
import com.babylon.fragment.payments.CreditCardPromotionConfirmationFragment;
import com.babylon.fragment.payments.OrderDetailsListFragment;
import com.babylon.fragment.payments.PaymentCardsListFragment;
import com.babylon.model.Patient;
import com.babylon.model.payments.NewPaymentCard;
import com.babylon.model.payments.OrderDetail;
import com.babylon.model.payments.PaymentCard;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.HandlerLoaderListener;

import java.util.List;

public abstract class PaymentTransactionActivity extends BaseActivity implements AddingPaymentCardFragment
        .OnCreatingPaymentCardClickListener, OrderDetailsListFragment.OnPayClickListener,
        OrderDetailsListFragment.DataProtocol, OrderDetailsListFragment.OnCardListRequestClickLister,
        PaymentCardsListFragment.OnPaymentCardInteractionListener,OrderDetailsListFragment.OrderAddressFragmentListener {

    public static final String PATIENT_EXTRA = "PATIENT_ID_EXTRA";

    private PaymentCard selectedPaymentCard;
    protected List<OrderDetail> orderDetails;
    protected boolean isPaymentInProgress = false;
    protected Patient patient;

    private HandlerLoaderListener handlerLoaderListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handlerLoaderListener = new HandlerLoaderListener(this);
        patient = (Patient)getIntent().getSerializableExtra(PATIENT_EXTRA);
        if (patient == null)
        {
            patient = App.getInstance().getPatient();
        }
    }

    /**
     * Start payment transaction when you need
     * not start from OnCreate by inheritance reason
     */
    public void startPaymentTransaction()
    {
        startFragment(new OrderDetailsListFragment(), OrderDetailsListFragment.TAG);
    }

    @Override
    public void onCreatingPaymentCardClick(NewPaymentCard paymentCard) {
        setProgressBarVisible(true);

        new AddingCreditCardPost().withEntry(paymentCard).execute(PaymentTransactionActivity.this,
                onCreatingPaymentCardRequestListener);
    }

    @Override
    public void onNewPaymentCardClick() {
        startFragment(new AddingPaymentCardFragment(), AddingPaymentCardFragment.TAG);
    }

    @Override
    public void onPaymentCardSelect(PaymentCard paymentCard) {
        this.selectedPaymentCard = paymentCard;

        App.getInstance().getPatient().setLastUsedCreditCard(this.selectedPaymentCard);

        onBackPressed();
    }

    @Override
    public void onPaymentCardDelete(int id) {
        if (selectedPaymentCard.getId().intValue() == id) {
            selectedPaymentCard = new PaymentCardViewController().getNewCreditCard();
        }
    }

    @Override
    public PaymentCard getPaymentCard() {

        if (selectedPaymentCard != null)
            return selectedPaymentCard;

        PaymentCard card = App.getInstance().getPatient().getLastUsedCreditCard();

        if (card != null)
            selectedPaymentCard = card;
        else
            selectedPaymentCard = new PaymentCardViewController().getNewCreditCard();

        return selectedPaymentCard;
    }

    private final RequestListener<PaymentCard[]> cardsListRequestListener = new
            RequestListenerErrorProcessed<PaymentCard[]>() {
                @Override
                public void onRequestComplete(Response<PaymentCard[]> response, String errorMessage) {
                    if (!PaymentTransactionActivity.this.isFinishing()) {
                        if (!TextUtils.isEmpty(errorMessage)) {
                            Toast.makeText(PaymentTransactionActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        } else {
                            processPaymentCardsResponse(response);
                        }
                    }
                }

                private void processPaymentCardsResponse(Response<PaymentCard[]> response) {
                    if (response.getData().length >= 1) {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag(PaymentCardsListFragment.TAG);
                        if (fragment == null) {
                            fragment = new PaymentCardsListFragment();
                        } else {
                            ((BaseFragment) fragment).setProgressBarVisible(false);
                        }

                        startFragment(fragment, PaymentCardsListFragment.TAG);
                    } else {
                        startFragment(new AddingPaymentCardFragment(), AddingPaymentCardFragment.TAG);
                    }
                }
            };

    private final RequestListener<PaymentCard> onCreatingPaymentCardRequestListener = new
            RequestListenerErrorProcessed<PaymentCard>() {
                @Override
                public void onRequestComplete(Response<PaymentCard> response, String errorMessage) {
                    if (!PaymentTransactionActivity.this.isFinishing()) {
                        setProgressBarVisible(false);
                        if (response.isSuccess()) {
                            selectedPaymentCard = response.getData();

                            App.getInstance().getPatient().setLastUsedCreditCard(selectedPaymentCard);

                            //handlerLoaderListener.sendEmptyMessage(HandlerLoaderListener.POP_BACK_HANDLER_CODE);
                            handlerLoaderListener.sendEmptyMessage(HandlerLoaderListener.BACK_PRESS_HANDLER_CODE);
                            
                        } else {
                            AlertDialogHelper.getInstance().showMessage(PaymentTransactionActivity.this, response.getUserResponseError());
                        }
                    }
                }
            };

    @Override
    public void onCardListRequestClickLister() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(PaymentCardsListFragment.TAG);
        if (fragment != null) {
            ((BaseFragment) fragment).setProgressBarVisible(true);
        }

        new CreditCardsListGet().execute(this, cardsListRequestListener);
    }


    @Override
    public void onAddressClicked() {

    }


}
