package com.babylon.model.payments;

import com.babylon.model.ServerModel;

public class Order extends ServerModel {
    @SkipFieldInContentValues
    private static final long serialVersionUID = 979345683647343L;

    private String id;
    private String title;
    private String description;
    private Double price;
    private Integer qty;
    private String currency;

    public Order(String title, String description, Double price, String currency){
        this.title = title;
        this.description = description;
        this.price = price;
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
