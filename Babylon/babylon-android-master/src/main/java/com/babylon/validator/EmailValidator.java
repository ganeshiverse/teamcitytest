package com.babylon.validator;

public class EmailValidator extends BaseValidator {

    public EmailValidator(BaseValidator nextValidator, String errorMessage) {
        super(nextValidator, errorMessage);
    }

    @Override
    protected String validate(String string) {
        String error = null;

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(string).matches()) {
            error = errorMessage;
        }

        return error;
    }

}
