package com.iversecomics.logging;

public interface ILoggerFactory {
    Logger getLogger(String logname);
}
