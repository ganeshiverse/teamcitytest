package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @Expose
    private Integer id;
    @Expose
    private String name;
    @Expose
    private String price;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("long_description")
    @Expose
    private String longDescription;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getPrice() {
        return price;
    }


    public void setPrice(String price) {
        this.price = price;
    }


    public String getShortDescription() {
        return shortDescription;
    }


    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }


    public String getImageUrl() {
        return imageUrl;
    }


    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public Integer getCategoryId() {
        return categoryId;
    }


    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }


    public String getLongDescription() {
        return longDescription;
    }


    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }



}