package com.babylon.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.babylon.R;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.utils.PulseMeasuringManager;
import com.babylon.view.CoveredImageView;

public class MeasuringPulseFragment extends BaseFragment
        implements View.OnClickListener {

    private TextView pulseTextView;
    private TextView tipsTextView;
    private TextView infoTextView;
    private Button retakeButton;
    private Button processButton;
    private FrameLayout heartBeatLinearLayout;
    private CoveredImageView diagramImageView;
    private LinearLayout dividerLinearLayout;
    private LinearLayout beforeDividerLayout;
    private LinearLayout afterDividerLayout;
    private SurfaceView surfaceView;
    private PulseMeasuringManager pulseManager;

    public static MeasuringPulseFragment getInstance(long beginOfTheDayInMillis) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);

        MeasuringPulseFragment fragment = new MeasuringPulseFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_measuring_pulse, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pulseTextView = (TextView) view.findViewById(R.id.pulse_fragment_amount_text_view);
        infoTextView = (TextView) view.findViewById(R.id.info_text_view);
        tipsTextView = (TextView) view.findViewById(R.id.pulse_fragment_tips_text_view);
        retakeButton = (Button) view.findViewById(R.id.pulse_fragment_retake_button);
        processButton = (Button) view.findViewById(R.id.pulse_fragment_process_button);
        heartBeatLinearLayout = (FrameLayout) view.findViewById(R.id.pulse_fragment_heart_beat_linear_layout);
        diagramImageView = (CoveredImageView) view.findViewById(R.id.pulse_fragment_diagram_image_view);
        beforeDividerLayout = (LinearLayout) view.findViewById(R.id.pulse_fragment_diagram_before_layout);
        afterDividerLayout = (LinearLayout) view.findViewById(R.id.pulse_fragment_diagram_after_layout);
        dividerLinearLayout = (LinearLayout) view.findViewById(R.id.pulse_fragment_div_layout);
        surfaceView = (SurfaceView) view.findViewById(R.id.pulse_fragment_surface);

        retakeButton.setOnClickListener(this);
        processButton.setOnClickListener(this);

        setTitle(getString(R.string.resting_pulse_fragment_title));

        setBackButtonVisible(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        pulseManager = PulseMeasuringManager.getInstance(true);
        if (surfaceView != null) {
            pulseManager.initialize(getActivity(), surfaceView.getHolder(), new Handler() {
                @Override
                public void handleMessage(Message message) {
                    Bundle extras = message.getData();
                    if (isVisible() && extras != null) {
                        switch (message.what) {
                            case PulseMeasuringManager.PULSE_MEASURE_UPDATE:
                                float part = extras.getFloat(PulseMeasuringManager.EXTRA_PART);
                                setDividerProgress(part);
                                diagramImageView.setVisiblePart(part);
                                break;
                            case PulseMeasuringManager.PULSE_MEASURE_STOP:
                                int pulseAmount = extras.getInt(PulseMeasuringManager.EXTRA_AMOUNT);
                                processButton.setVisibility(View.VISIBLE);
                                diagramImageView.setVisibility(ImageView.INVISIBLE);
                                retakeButton.setVisibility(View.VISIBLE);
                                infoTextView.setVisibility(View.GONE);
                                heartBeatLinearLayout.setVisibility(View.INVISIBLE);
                                dividerLinearLayout.setVisibility(View.INVISIBLE);
                                setBackButtonVisible(true);
                                tipsTextView.setText(R.string.resting_pulse_fragment_done);
                                processButton.setText(R.string.resting_pulse_fragment_save);
                                pulseTextView.setText(String.valueOf(pulseAmount));
                                break;
                            case PulseMeasuringManager.PULSE_MEASURE_CLEAN:
                                setMeasuringPulseViewVisibility(false);
                                break;
                            default:
                                break;
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        pulseManager.onActivityPause();
    }

    /* Listeners */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pulse_fragment_process_button:
                onClickProcessButton();
                break;
            case R.id.pulse_fragment_retake_button:
                startMeasuringPulse();
                break;
            default:
                break;
        }

    }

    @Override
    protected void onTopBarButtonPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    /* Methods */

    private void onClickProcessButton() {
        if (!pulseManager.isMeasuringFinished()) {
            startMeasuringPulse();
        } else {
            savePulse();
        }
    }

    private void savePulse() {
        long dayInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
        MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature(
                MonitorMeParam.RESTING_PULSE_ID.getId(), String.valueOf(pulseManager.getPulseAmount()), dayInMillis);

        DatabaseOperationUtils.getInstance().updateMonitorMeFeatureOperation(feature, getActivity()
                .getContentResolver());

        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);

        getActivity().onBackPressed();
    }

    private void startMeasuringPulse() {
        boolean isMeasuringPulseStarted = pulseManager.startMeasuringPulse(getActivity());

        if (isMeasuringPulseStarted) {
            setMeasuringPulseViewVisibility(true);
        }
    }

    private void setMeasuringPulseViewVisibility(boolean isMeasuringStarted) {
        infoTextView.setVisibility(View.VISIBLE);
        diagramImageView.setVisibility(ImageView.VISIBLE);
        retakeButton.setVisibility(View.GONE);
        heartBeatLinearLayout.setVisibility(View.VISIBLE);
        dividerLinearLayout.setVisibility(View.VISIBLE);
        setDividerProgress(0);

        processButton.setVisibility(isMeasuringStarted ? View.INVISIBLE : View.VISIBLE);
        setBackButtonVisible(!isMeasuringStarted);
        tipsTextView.setText(isMeasuringStarted ?
                R.string.resting_pulse_fragment_measuring
                : R.string.resting_pulse_fragment_place_finger);

        if (!isMeasuringStarted) {
            pulseTextView.setText(R.string.resting_pulse_fragment_default_pulse);
            processButton.setText(R.string.resting_pulse_fragment_start);
            diagramImageView.setVisiblePart(0);

        }
    }

    /**
     * @param progress 0 - 1
     */
    private void setDividerProgress(float progress) {
        progress = progress > 1 ? 1 : progress;
        LinearLayout.LayoutParams beforeDividerLayoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        beforeDividerLayoutParams.weight = progress;
        beforeDividerLayout.setLayoutParams(beforeDividerLayoutParams);

        LinearLayout.LayoutParams afterDividerLayoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        afterDividerLayoutParams.weight = 1 - progress;
        afterDividerLayout.setLayoutParams(afterDividerLayoutParams);
    }
}
