package com.babylon.model;

import android.text.TextUtils;

import com.babylon.enums.MonitorMeParam;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.DateUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DailyFeatureMap {

    public static final String EMPTY_VALUE = "0";

    private Map<String, MonitorMeFeatures.Feature> featureMap;

    public DailyFeatureMap(Map<String, MonitorMeFeatures.Feature> featureMap) {
        this.featureMap = new HashMap<String, MonitorMeFeatures.Feature>(featureMap);
    }

    public Map<String, MonitorMeFeatures.Feature> getFeatureMap() {
        return featureMap;
    }

    public void setFeatureMap(Map<String, MonitorMeFeatures.Feature> featureMap) {
        this.featureMap = featureMap;
    }

    /**
     *
     * @param monitorMeParam
     * @param value
     * @return true if value successfully saved, else - false
     *
     */
    public boolean setValue(MonitorMeParam monitorMeParam, String value) {
        boolean result = false;

        if(featureMap.containsKey(monitorMeParam.getId())) {
            result = true;
            MonitorMeFeatures.Feature feature = featureMap.get(monitorMeParam.getId());

            final Date savingDate = DateUtils.findOutSavingDate(feature.getDate());
            feature.setDate(savingDate);
            feature.setValue(value);
            feature.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
        }

        return result;
    }

    public boolean setValue(MonitorMeParam monitorMeParam, float value) {
        return setValue(monitorMeParam, String.valueOf(value));
    }

    public MonitorMeFeatures.Feature getFeature(MonitorMeParam monitorMeParam) {
        String featureId = monitorMeParam.getId();
        MonitorMeFeatures.Feature feature;
        if (featureMap.containsKey(featureId)) {
            feature = featureMap.get(featureId);
        } else {
            feature = new MonitorMeFeatures.Feature(featureId, EMPTY_VALUE);
        }
        return feature;
    }

    public String getFeatureValue(MonitorMeParam monitorMeParam) {
        final MonitorMeFeatures.Feature feature = getFeature(monitorMeParam);
        final String value = TextUtils.isEmpty(feature.getValue()) ? "0" : feature.getValue();
        return value;
    }

    public void put(MonitorMeFeatures.Feature feature) {
        featureMap.put(feature.getId(), feature);
    }


}
