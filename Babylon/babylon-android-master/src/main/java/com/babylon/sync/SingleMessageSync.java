package com.babylon.sync;

import android.content.ContentResolver;
import android.os.Bundle;
import android.text.TextUtils;
import com.babylon.Keys;
import com.babylon.api.request.QuestionGet;
import com.babylon.api.request.base.Response;
import com.babylon.model.Message;

import java.util.ArrayList;
import java.util.List;

public class SingleMessageSync extends MessagesSync {
    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
    }

    @Override
    protected List<Message> pullMessages(Bundle bundle) {
        String questionId = bundle.getString(Keys.QUESTION_ID);
        List<Message> messages = null;
        if (!TextUtils.isEmpty(questionId)) {
            Response<Message.Question> response = QuestionGet.newQuestionGet(questionId).execute();
            if (response != null && response.isSuccess()) {
                messages = new ArrayList<Message>(1);
                messages.add(response.getData().getQuestion());
            }
        }
        return messages;
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        super.pull(contentResolver, bundle);
    }
}
