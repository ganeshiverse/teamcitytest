package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.ClientUrlDbModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.util.Trace;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class SaveClientUrlAndSiteDetailsDBLoader extends DataAsyncTaskLibLoader<ClientUrlDbModel> {

    private String TAG = SaveClientUrlAndSiteDetailsDBLoader.class.getSimpleName();
    private ChangePasswordResponse changePasswordResponse;

    public SaveClientUrlAndSiteDetailsDBLoader(final Context context, ChangePasswordResponse changePasswordResponse) {
        super(context, true);
        this.changePasswordResponse = changePasswordResponse;
    }

    @Override
    protected ClientUrlDbModel performLoad() throws Exception {
        if (changePasswordResponse == null)
            throw new SQLException("Unable to cache verified user details in device database...");

        final ClientUrlDbModel clientUrlDbModel = new ClientUrlDbModel();

        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            if (changePasswordResponse != null && changePasswordResponse.getChangePasswordDataObject() != null) {
                // Create client url and site details db model and insert in DB

                clientUrlDbModel.setSiteIdAndUserId(changePasswordResponse.getChangePasswordDataObject().getSiteID() + "|" +
                        changePasswordResponse.getChangePasswordDataObject().getEcUserID());
                clientUrlDbModel.setSiteId(changePasswordResponse.getChangePasswordDataObject().getSiteID());
                clientUrlDbModel.setSiteName(changePasswordResponse.getChangePasswordDataObject().getSiteName());
                clientUrlDbModel.setEcUserId(changePasswordResponse.getChangePasswordDataObject().getEcUserID());
                clientUrlDbModel.setUserEmailId(changePasswordResponse.getChangePasswordDataObject().getLoginName());
                clientUrlDbModel.setClientURL(changePasswordResponse.getClientURL());

                TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<ClientUrlDbModel>() {
                    @Override
                    public ClientUrlDbModel call() throws Exception {
                        final Dao<ClientUrlDbModel, Long> clientUrlDbModelDao = helper.getDao(ClientUrlDbModel.class);

                        clientUrlDbModelDao.createOrUpdate(clientUrlDbModel);
                        Trace.d(TAG, "Saved client url and user details in DB...");

                        return clientUrlDbModel;
                    }
                });
            }

        } finally {
            OpenHelperManager.releaseHelper();
        }

        return clientUrlDbModel;
    }
}
