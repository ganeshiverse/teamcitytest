package com.iversecomics.otto;

import com.squareup.otto.Bus;


// Provided by Square under the Apache License
public final class OttoBusProvider {
    private static final Bus BUS = new MainThreadBus();

    public static Bus getInstance() {
        return BUS;
    }

    private OttoBusProvider() {
        // No instances.
    }
}