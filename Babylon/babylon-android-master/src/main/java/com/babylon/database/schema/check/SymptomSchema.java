package com.babylon.database.schema.check;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.enums.CheckGender;

import static com.babylon.database.check.CheckTables.SYMPTOM;

public interface SymptomSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.ID.getName() + ") ON CONFLICT REPLACE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("check_entity").appendPath(SYMPTOM.getName()).build();
    Uri CONTENT_URI_WITH_CHILDREN_COUNT = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath(SYMPTOM.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        TYPE("type", DBType.INT),
        NAME("name", DBType.TEXT),
        GENDER("gender", DBType.TEXT),
        PARENT_ID("parentId", DBType.TEXT),
        SELECT_BOX("selectBox", DBType.TEXT),
        SORT("sort", DBType.TEXT),
        BODY_PART_ID("bodyPartId", DBType.INT_DEF),
        CHILDREN_COUNT("childrenCount", DBType.INT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public class SymptomQuery {
        //select id, name, (select count(*) from symptom where parentId=main.id) as a from symptom as main where gender=? AND bodyPartId=? AND parentId=?;
        public static String PARENT_SYMPTOM_TABLE = "parent_symptom_table";

        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int TYPE = 3;
        int NAME = 4;
        int GENDER = 5;
        int PARENT_ID = 6;
        int SELECT_BOX = 7;
        int SORT = 8;
        int BODY_PART_ID = 9;
        int CHILDREN_COUNT = 10;

        public static String[] PROJECTION = {
                PARENT_SYMPTOM_TABLE + "." +
                        Columns._ID.getName(),
                Columns.ID.getName(),
                Columns.SYNC_WITH_SERVER.getName(),
                Columns.TYPE.getName(),
                Columns.NAME.getName(),
                Columns.GENDER.getName(),
                Columns.PARENT_ID.getName(),
                Columns.SELECT_BOX.getName(),
                Columns.SORT.getName(),
                Columns.BODY_PART_ID.getName(),
                "(SELECT COUNT(*) FROM " + SYMPTOM +
                        " WHERE " + Columns.PARENT_ID + "=" + PARENT_SYMPTOM_TABLE + "." + Columns.ID + ")" +
                        " AS " + Columns.CHILDREN_COUNT
        };

        public static final String SELECTION =
                "(" + Columns.GENDER + "=" + String.valueOf(CheckGender.ALL.checkValue) + " OR " +
                        Columns.GENDER + "=?) AND " +
                        Columns.PARENT_ID + "=?";

        public static final String SELECTION_WITH_BODY_PART =
                SELECTION + " AND " +
                        Columns.BODY_PART_ID + "=?";

        public static final String SORT_ASC = Columns.SORT + " ASC";
    }
}