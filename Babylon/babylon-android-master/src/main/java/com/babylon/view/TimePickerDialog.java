package com.babylon.view;

import android.app.Activity;
import android.app.Dialog;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import com.babylon.R;
import com.babylon.utils.L;

public class TimePickerDialog extends Dialog implements View.OnClickListener{

    private static final String TAG = TimePickerDialog.class.getSimpleName();
    private final android.app.TimePickerDialog.OnTimeSetListener onTimeSetListener;
    private TimePicker timePicker;
    private int hours;
    private int minutes;

    public TimePickerDialog(Activity a, android.app.TimePickerDialog.OnTimeSetListener onTimeSetListener, int hours,
                            int minutes) {
        super(a);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_time_picker);
        getWindow().setGravity(Gravity.CENTER);
        getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        this.hours = hours;
        this.minutes = minutes;
        this.onTimeSetListener = onTimeSetListener;

        initViews();
    }

    private void initViews() {
        timePicker = (TimePicker)findViewById(R.id.time_picker);
        timePicker.setCurrentHour(hours);
        timePicker.setCurrentMinute(minutes);
        timePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

        findViewById(R.id.okButton).setOnClickListener(this);
        findViewById(R.id.cancelButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.okButton:
                onTimeSetListener.onTimeSet(timePicker, timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                dismiss();
                break;

            case R.id.cancelButton:
                dismiss();
                break;
            default:
                L.e(TAG, "Unknown View id");
        }
    }
}
