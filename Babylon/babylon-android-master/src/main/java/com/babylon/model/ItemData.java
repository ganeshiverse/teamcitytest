package com.babylon.model;

/**
 * Created by Babylon on 01/06/2015.
 */
public class ItemData {


    private String title;
    private String imageUrl;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ItemData(String title,String imageUrl){

        this.title = title;
        this.imageUrl = imageUrl;
    }
    // getters & setters
}