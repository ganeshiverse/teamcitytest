package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.AppointmentListResponse;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleDetailsEditResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.db.models.CircleDetailsDbModel;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.loaders.CircleDetailsDbLoader;
import com.axsystech.excelicare.loaders.CircleDetailsLoader;
import com.axsystech.excelicare.loaders.CircleListSearchLoader;
import com.axsystech.excelicare.loaders.CircleMemberDeleteLoader;
import com.axsystech.excelicare.loaders.CircleMemberDetailsDbLoader;
import com.axsystech.excelicare.loaders.CircleMemberListDbLoader;
import com.axsystech.excelicare.loaders.CircleMembersDetailsLoader;
import com.axsystech.excelicare.loaders.CircleMembersListSearchLoader;
import com.axsystech.excelicare.loaders.GetCircleMemberListFromDbLoader;
import com.axsystech.excelicare.loaders.GetCirclesListFromDbLoader;
import com.axsystech.excelicare.loaders.ListMembersInCircleLoader;
import com.axsystech.excelicare.loaders.ListMyCirclesLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class CircleMembersListFragment extends BaseFragment implements View.OnClickListener, TextWatcher {

    private String TAG = CircleMembersListFragment.class.getSimpleName();
    private CircleMembersListFragment mFragment;
    private MainContentActivity mActivity;

    private LayoutInflater mLayoutInflater;

    @InjectView(R.id.circlesListView)
    private ListView mCircleMembersListView;

    //@InjectView(R.id.textViewinviteDoctor)
    private TextView textViewInviteDoctor;

    //@InjectView(R.id.textViewinviteCareGiver)
    private TextView textViewInviteCareGiver;

    @InjectView(R.id.relativeLayoutFooter)
    private RelativeLayout relativeLayoutFooter;

    @InjectView(R.id.relativeLayoutFooter)
    private RelativeLayout mRelativeLayoutFooter;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mCircleId;

    @InjectSavedState
    private String mCircleName;

    @InjectSavedState
    private String mCircleType;

    View view;

    CircleDetailsDbModel circleDetailsDbModel;

    private ListMyCirclesResponse.CircleDetails mSelectedCircleObject;
    private ArrayList<ListMembersInCircleResponse.CircleMemberDetails> mMembersInSelectedCircle;
    private ArrayList<CircleMemberListDbModel> circleMemberListDbModels;
    CircleMemberListDbModel circleMemberListDbModel = new CircleMemberListDbModel();

    private ArrayList<CirclesMemberDetailsDbModel> dbCirclesMemberDetailsDbModels;
    private ArrayList<CircleDetailsDbModel> dbCircleDetailsDbModels;

    private CircleMembersAdapter mCircleMembersAdapter;

    private static final String LIST_MEMBERS_IN_CIRCLE_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersListFragment.LIST_MEMBERS_IN_CIRCLE_LOADER";
    private static final String CIRCLE_MEMBERS_DETAIL = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesMemberListFragment.CIRCLE_MEMBERS_DETAIL";
    private static final String CIRCLE_DETAILS = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesMemberListFragment.CIRCLE_DETAILS";
    private static final String MEMBER_DELETE = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesMemberListFragment.MEMBER_DELETE";
    private static final String GET_MEMBER_LIST_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesMemberListFragment.GET_MEMBER_LIST_DB_LOADER";
    private static final String SAVE_MEMBER_DETAILS_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesMemberListFragment.SAVE_MEMBER_DETAILS_DB_LOADER";
    private static final String SAVE_CIRCLE_DETAILS_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CirclesMemberListFragment.SAVE_CIRCLE_DETAILS_DB_LOADER";
    private static final String SAVE_CIRCLE_MEMBER_LIST_LOADER = "com.axsystech.excelicare.framework.ui.messages.CircleMembersListFragment.SAVE_CIRCLE_MEMBER_LIST_LOADER";
    private static final String CIRCLE_MEMBER_LIST_SEARCH_LOADER = "com.axsystech.excelicare.framework.ui.messages.CircleMembersListFragment.CIRCLE_MEMBER_LIST_SEARCH_LOADER";

    boolean menu = true;
    boolean sendMessage = true;
    int pageCount = 0;
    int pageIndex;
    int PAGE_SIZE = 10;
    ArrayList<CircleMemberListDbModel> completeCircleMemberListDbModels = new ArrayList<CircleMemberListDbModel>();

    @Override
    protected void initActionBar() {

        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, mCircleName));


        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_circles_list, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        preloadData();
        readIntentArgs();
        getCircleMembersListFromDb(true);
        addEventClickListener();
    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_members));

        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Add Content")) {
                    if (m.getValue() == 1) {
                        menu = true;
                    } else if (m.getValue() == 0) {
                        menu = false;
                    }
                }
                if (m.getKey().equals("Message Providers")) {
                    if (m.getValue() == 1) {
                        sendMessage = true;
                    } else if (m.getValue() == 0) {
                        sendMessage = false;
                    }
                }
            }
        }
    }

    private void getCircleMembersListFromDb(boolean showProgress) {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        if (CommonUtils.hasInternet()) {
            if (showProgress)
                displayProgressLoader(false);
            pageIndex = pageIndex + 1;
            ListMembersInCircleLoader listMembersInCircleLoader = new ListMembersInCircleLoader(mActivity, mActiveUser.getToken(),
                    mCircleId, "", pageIndex, PAGE_SIZE, "");
            if (mFragment.isAdded() && mFragment.getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MEMBERS_IN_CIRCLE_LOADER), listMembersInCircleLoader);
            }
        } else {
            final GetCircleMemberListFromDbLoader getCircleMemberListFromDbLoader = new GetCircleMemberListFromDbLoader(mActivity, mCircleId);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_MEMBER_LIST_DB_LOADER), getCircleMemberListFromDbLoader);
            }
        }
    }

    private void readIntentArgs() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CIRCLEID)) {
                mCircleId = bundle.getString(AxSysConstants.EXTRA_SELECTED_CIRCLEID);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CIRCLENAME)) {
                mCircleName = bundle.getString(AxSysConstants.EXTRA_SELECTED_CIRCLENAME);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CIRCLENAME)) {
                mCircleType = bundle.getString(AxSysConstants.EXTRA_SELECTED_CIRCLETYPE);
            }
        }

    }

    private void addEventClickListener() {

        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

        textViewInviteDoctor = (TextView) view.findViewById(R.id.textViewinviteDoctor);
        textViewInviteCareGiver = (TextView) view.findViewById(R.id.textViewinviteCareGiver);

        textViewInviteDoctor.setTypeface(bold);
        textViewInviteCareGiver.setTypeface(bold);

        mRelativeLayoutFooter.setVisibility(View.VISIBLE);
        textViewInviteDoctor.setOnClickListener(this);
        textViewInviteCareGiver.setOnClickListener(this);

        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        if (menu) {
            relativeLayoutFooter.setVisibility(View.VISIBLE);
        } else {
            relativeLayoutFooter.setVisibility(View.GONE);
        }


        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String searchEdit = null;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        try {
                            searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        pageCount = pageCount + 1;
                        CircleMembersListSearchLoader circleMembersListSearchLoader = null;
                        circleMembersListSearchLoader = new CircleMembersListSearchLoader(mActivity, searchEdit, pageCount, "2", mActiveUser.getToken(), mCircleId);
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_MEMBER_LIST_SEARCH_LOADER), circleMembersListSearchLoader);
                        }
                    } else {
                        searchEditText.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                                // When user changed the Text
                                String text = searchEditText.getText().toString().toLowerCase(Locale.getDefault());
                            }

                            @Override
                            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                          int arg3) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void afterTextChanged(Editable arg0) {
                                // TODO Auto-generated method stub
                            }
                        });
                    }
                }
                return false;
            }
        });

        mCircleMembersListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount)) {

                    if (circleMemberListDbModels != null && circleMemberListDbModels.size() != 0) {
                        getCircleMembersListFromDb(false);
                    }
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        if (mCircleType.equals(AxSysConstants.EXTRA_SELECTED_CIRCLE_SYSTEMCIRCLE)) {
            inflater.inflate(R.menu.menu_empty, menu);
        } else if (mCircleType.equals(AxSysConstants.EXTRA_SELECTED_CIRCLE_CARECIRCLE)) {
            inflater.inflate(R.menu.menu_circle_members, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            case R.id.action_edit_circle:
                if (menu) {
                    if (dbCircleDetailsDbModels != null && dbCircleDetailsDbModels.size() > 0) {
                        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                        CircleDetailEditFragment circleDetailEditFragment = new CircleDetailEditFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID, circleDetailsDbModel.getUserType());
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, circleDetailsDbModel.getUserCircleId());
                        circleDetailEditFragment.setArguments(bundle);
                        ft.replace(R.id.container_layout, circleDetailEditFragment, CircleDetailEditFragment.class.getSimpleName());
                        ft.addToBackStack(CircleDetailEditFragment.class.getSimpleName());
                        ft.commit();
                    } else {
                        if (CommonUtils.hasInternet()) {
                            // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                            displayProgressLoader(false);

                            CircleDetailsLoader circleDetailsLoader = new CircleDetailsLoader(mActivity, mActiveUser.getToken(),
                                    mCircleId);
                            if (mFragment.isAdded() && mFragment.getView() != null) {
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_DETAILS), circleDetailsLoader);
                            }
                        } else {
                            showToast(R.string.error_no_network);
                        }
                    }
                } else {
                    showToast("No permissions to edit circle");
                }

                break;

            default:
                break;
        }
        return super.
                onOptionsItemSelected(item);
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {


    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(CIRCLE_DETAILS)) {
            if (result != null && result instanceof CircleDetailsEditResponse) {
                final CircleDetailsEditResponse circleDetailsEditResponse = (CircleDetailsEditResponse) result;

                dbCircleDetailsDbModels = new ArrayList<CircleDetailsDbModel>();

                if (circleDetailsEditResponse != null) {

                    circleDetailsDbModel = new CircleDetailsDbModel();
                    try {
                        circleDetailsDbModel.setCircleName(circleDetailsEditResponse.getDataObject().getCircleName());
                        circleDetailsDbModel.setComments(circleDetailsEditResponse.getDataObject().getDescription());
                        circleDetailsDbModel.setStopReasonDesc(circleDetailsEditResponse.getDataObject().getStopReasonDesc());
                        circleDetailsDbModel.setStartDate(circleDetailsEditResponse.getDataObject().getStartDate());
                        circleDetailsDbModel.setUserType(circleDetailsEditResponse.getDataObject().getUserType());
                        circleDetailsDbModel.setUserCircleId(circleDetailsEditResponse.getDataObject().getUserCircle_ID());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dbCircleDetailsDbModels.add(circleDetailsDbModel);

                }
                final CircleDetailsDbLoader circleDetailsDbLoader = new CircleDetailsDbLoader(mActivity, dbCircleDetailsDbModels);
                if (isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_CIRCLE_DETAILS_DB_LOADER), circleDetailsDbLoader);
                } else {
                    showToast(R.string.circles_no_circle_details_found);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(MEMBER_DELETE)) {
            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponseRemove = (BaseResponse) result;

                if (baseResponseRemove.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            completeCircleMemberListDbModels.remove(circleMemberListDbModel);
                            circleMemberListDbModel = null;
                            mCircleMembersAdapter.notifyDataSetChanged();
                            if (CommonUtils.hasInternet()) {
                                displayProgressLoader(false);
                                ListMembersInCircleLoader listMembersInCircleLoader = new ListMembersInCircleLoader(mActivity, mActiveUser.getToken(),
                                        mCircleId, "", pageIndex, PAGE_SIZE, "");
                                if (mFragment.isAdded() && mFragment.getView() != null) {
                                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(LIST_MEMBERS_IN_CIRCLE_LOADER), listMembersInCircleLoader);
                                }
                            } else {
                                showToast(R.string.error_no_network);
                            }
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_MEMBER_LIST_DB_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                circleMemberListDbModels = (ArrayList<CircleMemberListDbModel>) result;

                if (circleMemberListDbModels != null) {

                    // Display members in ListView
                    mCircleMembersAdapter = new CircleMembersAdapter(circleMemberListDbModels);
                    mCircleMembersListView.setAdapter(mCircleMembersAdapter);
                } else {
                    showToast(R.string.no_circle_members_found);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CIRCLE_MEMBER_LIST_SEARCH_LOADER)) {
            if (result != null && result instanceof CircleMemberListDbModel) {
                CircleMemberListDbModel circleMemberListDbModel = (CircleMemberListDbModel) result;
                ArrayList<CircleMemberListDbModel> searchResultsAL = circleMemberListDbModels;

                if (circleMemberListDbModel != null) {
                    mCircleMembersAdapter.updateData(searchResultsAL);
                } else {
                    mCircleMembersAdapter = new CircleMembersAdapter(searchResultsAL);
                    mCircleMembersListView.setAdapter(mCircleMembersAdapter);
                }

                if (searchResultsAL != null && searchResultsAL.size() > 0) {

                } else {
                    showToast(R.string.no_search_results_found);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(LIST_MEMBERS_IN_CIRCLE_LOADER)) {
            if (result != null && result instanceof ListMembersInCircleResponse) {
                final ListMembersInCircleResponse listMembersInCircleResponse = (ListMembersInCircleResponse) result;

                if (listMembersInCircleResponse.getCircleMembersDataObject() != null &&
                        listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList() != null &&
                        listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList().size() > 0) {

                    ArrayList<ListMembersInCircleResponse.CircleMemberDetails> circleMemberDetailses = listMembersInCircleResponse.getCircleMembersDataObject().getCircleMemberDetailsArrayList();

                    circleMemberListDbModels = new ArrayList<CircleMemberListDbModel>();

                    for (int i = 0; i < circleMemberDetailses.size(); i++) {

                        ListMembersInCircleResponse.CircleMemberDetails indexedObj = circleMemberDetailses.get(i);
                        CircleMemberListDbModel circleMemberListDbModel = new CircleMemberListDbModel();

                        try {
                            circleMemberListDbModel.setUserId(indexedObj.getUserID());
                            circleMemberListDbModel.setForeName(indexedObj.getForeName());
                            circleMemberListDbModel.setSurName(indexedObj.getSurName());
                            circleMemberListDbModel.setUserCircleId(indexedObj.getUserCircle_ID());
                            circleMemberListDbModel.setUserMemberMail(indexedObj.getUserCircleMemberEmail_ID());
                            circleMemberListDbModel.setUserMemberId(indexedObj.getUserCircleMember_ID());
                            circleMemberListDbModel.setUserType(indexedObj.getUserType());
                            circleMemberListDbModel.setUserUrl(indexedObj.getURL());
                            circleMemberListDbModel.setUserMemberSpeciality(indexedObj.getSpeciality());
                            circleMemberListDbModel.setMemberRoleTypeSlu(indexedObj.getRoleType_SLU());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        circleMemberListDbModels.add(circleMemberListDbModel);
                    }
                    final CircleMemberListDbLoader circleMemberListDBLoader = new CircleMemberListDbLoader(mActivity, circleMemberListDbModels);
                    if (isAdded() && getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_CIRCLE_MEMBER_LIST_LOADER), circleMemberListDBLoader);
                    }
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_CIRCLE_MEMBER_LIST_LOADER)) {
            circleMemberListDbModels = (ArrayList<CircleMemberListDbModel>) result;
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (circleMemberListDbModels != null) {
                            //TODO: add global list for this.
                            if (completeCircleMemberListDbModels == null) {
                                completeCircleMemberListDbModels = new ArrayList<CircleMemberListDbModel>();
                            }
                            completeCircleMemberListDbModels.addAll(circleMemberListDbModels);

                            // Display members in ListView
                            mCircleMembersAdapter = new CircleMembersAdapter(completeCircleMemberListDbModels);
                            mCircleMembersListView.setAdapter(mCircleMembersAdapter);
                        } else {
                            showToast(R.string.no_circle_members_found);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

        } else if (id == getLoaderHelper().getLoaderId(SAVE_CIRCLE_DETAILS_DB_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                dbCircleDetailsDbModels = (ArrayList<CircleDetailsDbModel>) result;
                if (dbCircleDetailsDbModels != null && dbCircleDetailsDbModels.size() > 0) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {

                            final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                            final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                            CircleDetailEditFragment circleDetailEditFragment = new CircleDetailEditFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID, circleDetailsDbModel.getUserType());
                            bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, circleDetailsDbModel.getUserCircleId());
                            circleDetailEditFragment.setArguments(bundle);
                            ft.replace(R.id.container_layout, circleDetailEditFragment, CircleDetailEditFragment.class.getSimpleName());
                            ft.addToBackStack(CircleDetailEditFragment.class.getSimpleName());
                            ft.commit();
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    private class CircleMembersAdapter extends BaseAdapter {

        private ArrayList<CircleMemberListDbModel> membersInSelectedCircle;

        public CircleMembersAdapter(ArrayList<CircleMemberListDbModel> membersInSelectedCircle) {
            this.membersInSelectedCircle = membersInSelectedCircle;
        }

        public void updateData(ArrayList<CircleMemberListDbModel> membersInSelectedCircle) {
            this.membersInSelectedCircle = membersInSelectedCircle;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return membersInSelectedCircle != null && membersInSelectedCircle.size() > 0 ? membersInSelectedCircle.size() : 0;
        }

        @Override
        public CircleMemberListDbModel getItem(int position) {
            return membersInSelectedCircle.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final CircleMemberListDbModel rowObject = getItem(position);
            String imagePath = null;

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.circles_members_row, viewGroup, false);
            }

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");
            Typeface boldItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            final ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImageView);
            TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberNameTextView);
            TextView memberRoleTypeTextView = (TextView) convertView.findViewById(R.id.memberRoleTypeTextView);
            TextView specializationTextView = (TextView) convertView.findViewById(R.id.specializationTextView);
            TextView sendMessageTextView = (TextView) convertView.findViewById(R.id.sendMessageTextView);

            memberNameTextView.setTypeface(bold);
            memberRoleTypeTextView.setTypeface(regular);
            specializationTextView.setTypeface(regular);
            sendMessageTextView.setTypeface(bold);

            //String htmlString = "<u>Send Message</u>";
            sendMessageTextView.setText("Send Message");

            if (sendMessage) {
                sendMessageTextView.setVisibility(View.VISIBLE);
            } else {
                sendMessageTextView.setVisibility(View.GONE);
            }

            try {
                imagePath = rowObject.getUserUrl();
                if (!TextUtils.isEmpty(imagePath)) {
                    if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                        imagePath = "file://" + imagePath;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!TextUtils.isEmpty(imagePath)) {
               /* Picasso.with(mActivity)
                        .load(imagePath)
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .into(memberImageView);*/

                Picasso.with(getActivity()).load(imagePath).transform(new CircleTransform()).into(memberImageView);

            }
            try {

                memberNameTextView.setText(rowObject.getForeName() + " " + rowObject.getSurName());

                if (!TextUtils.isEmpty(rowObject.getUserMemberSpeciality())) {
                    specializationTextView.setVisibility(View.VISIBLE);
                    specializationTextView.setText(!TextUtils.isEmpty(rowObject.getUserMemberSpeciality()) ? rowObject.getUserMemberSpeciality() : "");
                } else {
                    specializationTextView.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(rowObject.getMemberRoleTypeSlu())) {
                    memberRoleTypeTextView.setVisibility(View.VISIBLE);
                    memberRoleTypeTextView.setText(!TextUtils.isEmpty(rowObject.getMemberRoleTypeSlu()) ? rowObject.getMemberRoleTypeSlu() : "");
                } else {
                    memberRoleTypeTextView.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            sendMessageTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (CommonUtils.hasInternet()) {
                            final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                            final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                            MessageComposeFragment messageComposeFragment = new MessageComposeFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Compose Message");
                            bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID, rowObject.getUserMemberMail());
                            bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_ID, rowObject.getUserId());
                            messageComposeFragment.setArguments(bundle);
                            ft.replace(R.id.container_layout, messageComposeFragment, MessageComposeFragment.class.getSimpleName());
                            ft.addToBackStack(MessageComposeFragment.class.getSimpleName());
                            ft.commit();
                            pageIndex = 0;
                            completeCircleMemberListDbModels.clear();
                            if(circleMemberListDbModels!=null)
                            circleMemberListDbModels.clear();
                            mCircleMembersAdapter = new CircleMembersAdapter(completeCircleMemberListDbModels);
                        } else {
                            showToast(R.string.error_no_network);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                        final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                        CircleMembersDetailsFragments circleMembersDetailsFragments = new CircleMembersDetailsFragments();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLEID, rowObject.getUserCircleId());
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_ID, rowObject.getUserMemberId());
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_USER_ID, rowObject.getUserId());
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_MAIL, rowObject.getUserMemberMail());
                        bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_NAME, rowObject.getForeName() + rowObject.getSurName());
                        circleMembersDetailsFragments.setArguments(bundle);
                        ft.replace(R.id.container_layout, circleMembersDetailsFragments, CircleMembersDetailsFragments.class.getSimpleName());
                        ft.addToBackStack(CircleMembersDetailsFragments.class.getSimpleName());
                        ft.commit();
                        pageIndex = 0;
                        completeCircleMemberListDbModels.clear();
                        if(circleMemberListDbModels!=null)
                        circleMemberListDbModels.clear();
                        mCircleMembersAdapter = new CircleMembersAdapter(completeCircleMemberListDbModels);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());
                    alert.setTitle(getString(R.string.text_alert));
                    alert.setMessage(getString(R.string.confirmation_to_delete_member));
                    alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {

                                if (CommonUtils.hasInternet()) {
                                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                                    circleMemberListDbModel = rowObject;

                                    displayProgressLoader(false);
                                    CircleMemberDeleteLoader circleMemberDeleteLoader = new CircleMemberDeleteLoader(mActivity, mActiveUser.getToken(),
                                            rowObject.getUserMemberId(), "", "");
                                    if (mFragment.isAdded() && mFragment.getView() != null) {
                                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MEMBER_DELETE), circleMemberDeleteLoader);
                                    }
                                } else {
                                    showToast(R.string.error_no_network);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

                    alert.show();
                    return false;
                }
            });

            return convertView;
        }

    }

    @Override
    public void onClick(View view) {
        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
        final FragmentTransaction ft = activityFragmentManager.beginTransaction();
        AddCircleMemberFragment addCirclememberFragment = new AddCircleMemberFragment();
        Bundle bundle = new Bundle();

        switch (view.getId()) {
            case R.id.textViewinviteDoctor:

                if (CommonUtils.hasInternet()) {
                    pageIndex = 0;
                    completeCircleMemberListDbModels.clear();
                    if(circleMemberListDbModels!=null)
                    circleMemberListDbModels.clear();
                    mCircleMembersAdapter = new CircleMembersAdapter(completeCircleMemberListDbModels);
                    //bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_CIRCLE_OBJECT, mSelectedCircleObject);
                    bundle.putString(AxSysConstants.EXTRA_CIRCLES_INVITE_TARGET, AxSysConstants.INVITE_CIRCLE_DOCTOR);
                    bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLEID, mCircleId);
                    bundle.putString(AxSysConstants.EXTRA_CIRCLE_MEMBER_TARGET, AddCircleMemberFragment.class.getSimpleName());
                    addCirclememberFragment.setArguments(bundle);
                    ft.replace(R.id.container_layout, addCirclememberFragment, AddCircleMemberFragment.class.getSimpleName());
                    ft.addToBackStack(AddCircleMemberFragment.class.getSimpleName());
                    ft.commit();
                } else {
                    showToast(R.string.error_no_network);
                }

                break;

            case R.id.textViewinviteCareGiver:

                if (CommonUtils.hasInternet()) {
                    pageIndex = 0;
                    completeCircleMemberListDbModels.clear();
                    if (circleMemberListDbModels != null)
                        circleMemberListDbModels.clear();
                    mCircleMembersAdapter = new CircleMembersAdapter(completeCircleMemberListDbModels);
                    bundle.putString(AxSysConstants.EXTRA_SELECTED_CIRCLEID, mCircleId);
                    bundle.putString(AxSysConstants.EXTRA_CIRCLES_INVITE_TARGET, AxSysConstants.INVITE_CIRCLE_MEMBER);
                    addCirclememberFragment.setArguments(bundle);
                    ft.replace(R.id.container_layout, addCirclememberFragment, AddCircleMemberFragment.class.getSimpleName());
                    ft.addToBackStack(AddCircleMemberFragment.class.getSimpleName());
                    ft.commit();
                } else {
                    showToast(R.string.error_no_network);
                }

                break;
        }

    }
}
