package com.babylon.test;

import android.test.AndroidTestCase;

import com.babylon.enums.PhysicalActivityType;

public class ActivityCaloriesTestCase extends AndroidTestCase {

    public static final String CYCLE = "Cycle";
    public static final String RUN = "Run";
    public static final String WALK = "Walk";
    public static final String EASY = "easy";
    public static final String MEDIUM = "medium";
    public static final String HARD = "hard";

    public void testCalculateWalkingEasyActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                WALK, EASY);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(69, 1000) == 4140);
    }

    public void testCalculateWalkingMediumActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                WALK, MEDIUM);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(150.7f, 11) == (int) 116.03);
    }

    public void testCalculateWalkingHardActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                WALK, HARD);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(77.7f, 25) == (int) 155.4);
    }

    public void testCalculateCyclingEasyActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                CYCLE, EASY);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(72, 30) == 216);
    }

    public void testCalculateCyclingMediumActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                CYCLE, MEDIUM);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(102, 15) == (int) 229.5);
    }

    public void testCalculateCyclingHardActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                CYCLE, HARD);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(52, 45) == (int) 491.4);
    }

    public void testCalculateRunningEasyActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                RUN, EASY);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(96.5f, 60) == (int) 694.8);
    }

    public void testCalculateRunningMediumActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                RUN, MEDIUM);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(111.1f, 53) == (int) 1048.2);
    }
    public void testCalculateRunningHardActivity() {
        PhysicalActivityType physicalActivityType = PhysicalActivityType.getActivityType(
                RUN, HARD);
        assertTrue(physicalActivityType.calculateBurntKiloCalories(42, 214) == (int) 2157.12);
    }
}
