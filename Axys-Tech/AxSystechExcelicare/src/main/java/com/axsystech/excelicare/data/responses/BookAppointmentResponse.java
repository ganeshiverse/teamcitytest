package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 26/08/15.
 */
public class BookAppointmentResponse extends BaseResponse implements Serializable {

    /* {
        BookAppointmentResult: {
            Status: "Success"
            data: "3091"
        }
    }
    */

    @SerializedName("BookAppointmentResult")
    private BookAppointmentResult bookAppointmentResult;

    public BookAppointmentResult getBookAppointmentResult() {
        return bookAppointmentResult;
    }

    public class BookAppointmentResult extends BaseResponse implements Serializable {

        @SerializedName("data")
        private String data;

        public String getData() {
            return data;
        }
    }
}
