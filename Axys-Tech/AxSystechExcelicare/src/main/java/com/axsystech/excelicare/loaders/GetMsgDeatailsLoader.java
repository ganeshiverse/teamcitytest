package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.MessageDetailsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by bhargav on 7/29/2015.
 */
public class GetMsgDeatailsLoader extends DataAsyncTaskLibLoader<MessageDetailsResponse> {

   private String token;
private String Id;


    public GetMsgDeatailsLoader(Context context, String token, String ID) {
        super(context);
        this.token = token;
        this.Id = ID;
    }

    @Override
    protected MessageDetailsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMessagedetailsListContent(token, Id);
    }
}
