package com.babylon.model.check;


import com.babylon.model.ServerModel;
import com.babylon.model.check.export.Outcome;

public class CheckOutcome extends ServerModel {
    private CheckOutcomeResult result;
    public CheckOutcomeResult getResult() {
        return result;
    }

    public class CheckOutcomeResult {
        private int id;
        private CheckPatient patient;
        private long date;
        private Outcome outcome;
        private int bodyPartId;
        public int getId() {
            return id;
        }
        public CheckPatient getPatient() {
            return patient;
        }
        public long getDate() {
            return date;
        }
        public Outcome getOutcome() {
            return outcome;
        }
        public int getBodyPartId() {
            return bodyPartId;
        }
        public class CheckPatient {
            private int id;

            public int getId() {
                return id;
            }
        }
    }
}