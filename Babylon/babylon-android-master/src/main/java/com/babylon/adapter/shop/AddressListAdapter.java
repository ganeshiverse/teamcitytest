package com.babylon.adapter.shop;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.model.Kit.Address;

import java.util.List;

public class AddressListAdapter extends ArrayAdapter<Address> {

    public interface OnPropertyItemClickListener {
        View.OnClickListener getOnPropertyClickListener();
        View.OnClickListener getOnMoreDetailClickListener();

    }

    private static final String TAG = AddressListAdapter.class.getSimpleName();

    public AddressListAdapter(Context context, List<Address> categories) {
        super(context, 0, categories);

    }

    private class ViewHolder
    {
        TextView addressView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = View.inflate(getContext(), R.layout.list_item_address_details, null);
            viewHolder.addressView=(TextView)convertView.findViewById(R.id.address_details_name);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

       Address address= getItem(position);

        viewHolder.addressView.setText(address.getFirstLine()+", "+address.getSecondLine()+"\n"+address.getThirdLine()
                +", "+address.getPostCode());



        return convertView;
    }



}
