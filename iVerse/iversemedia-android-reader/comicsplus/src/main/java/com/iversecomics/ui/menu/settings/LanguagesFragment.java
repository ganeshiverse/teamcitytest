package com.iversecomics.ui.menu.settings;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.store.ServerConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class LanguagesFragment extends Fragment {

    private ViewGroup mRootView;
    private LanguagesAdapter mMenuAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMenuAdapter = new LanguagesAdapter(getActivity());

        // This should have already been done, but just in case.
        ComicPreferences.initialize(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        mMenuAdapter.setData(new LanguagesDataProvider().getAllData());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_settings, container, false);

        mRootView.findViewById(R.id.action_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        StickyListHeadersListView listView = (StickyListHeadersListView) mRootView.findViewById(android.R.id.list);
        listView.setAdapter(mMenuAdapter);

        //Remove line separator in list. I tried to do this in XML, but was getting errors setting dividerHeight to 0dp.
        listView.setDivider(null);
        listView.setDividerHeight(0);

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // NA
            }
        });

        return mRootView;
    }


    /**
     * ***************************  Settings Menu Adapter  ************************************
     */


    class LanguagesAdapter extends ArrayAdapter<SettingsMenuItem> implements StickyListHeadersAdapter {
        private List<SettingsMenuSection> mSections;
        /**
         * Array keeping the first index of the section. Indexes correspond to mSections.
         */
        private int[] mSectionHeads;

        private LayoutInflater mInflater;

        public LanguagesAdapter(Context context) {
            super(context, 0);
            mInflater = LayoutInflater.from(context);
        }

        public void setData(List<SettingsMenuSection> data) {
            clear();
            if (data == null) {
                mSections = Collections.emptyList();
            } else {
                mSections = data;
            }
            int numSections = mSections.size();
            mSectionHeads = new int[numSections];

            int sectionHead = 0;

            for (int i = 0; i < numSections; ++i) {
                mSectionHeads[i] = sectionHead;

                SettingsMenuSection section = mSections.get(i);

                int sectionSize = section.size();
                List<SettingsMenuItem> menuItems = section.menuItems;

                for (int j = 0; j < sectionSize; ++j)
                    add(menuItems.get(j));

                sectionHead += sectionSize;
            }
            notifyDataSetChanged();
        }


        public View getView(int position, View view, ViewGroup parent) {

            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_language, parent, false);
            }

            final SettingsMenuItem menuItem = getItem(position);

            TextView titleView = (TextView) view.findViewById(R.id.title);
            ToggleButton toggle = (ToggleButton) view.findViewById(R.id.toggle);

            titleView.setText(menuItem.title);

            toggle.setVisibility(View.VISIBLE);
            // remove previous listener so that it doesn't trigger when we set the new checked value
            toggle.setOnCheckedChangeListener(null);
            toggle.setChecked(ComicPreferences.getInstance().isSet(menuItem.languagePreferenceKey, false));
            toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ComicPreferences.getInstance().setPreferredLanguage(menuItem.languagePreferenceKey, b);
                }
            });

            return view;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            int sectionIndex = getSectionIndex(position);

            if (convertView == null)
                convertView = mInflater.inflate(R.layout.menu_item_header, parent, false);

            SettingsMenuSection section = mSections.get(sectionIndex);
            ((TextView) convertView).setText(section.title);

            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            return getSectionIndex(position);
        }

        private int getSectionIndex(int position) {
            int numSections = mSectionHeads.length;
            int sectionIndex = 0;

            for (; sectionIndex < numSections; ++sectionIndex) {
                //This should be the only check. We've reached the section head that is above the position.
                //It accounts for empty sections (e.g. mSectionHeads = [0, 2, 2, 8] and for
                //position 2, we want to return index 2, not 1).
                if (position < mSectionHeads[sectionIndex]) {
                    break;
                }
            }

            return sectionIndex - 1;
        }
    }


    /**
     * Provides data for the list of items and section headers in the Settings->Languages Sub-Menu.
     */
    class LanguagesDataProvider {

        public List<SettingsMenuSection> getAllData() {
            List<SettingsMenuSection> res = new ArrayList<SettingsMenuSection>();

            for (int i = 0; i < SECTIONS.length; i++)
                res.add(getSection(i));

            return res;
        }

        public final String[] SECTIONS = {getResources().getString(R.string.language)};

        public SettingsMenuSection getSection(int index) {
            String title;
            ArrayList<SettingsMenuItem> resultList = new ArrayList<SettingsMenuItem>();

            // we only have one section at the moment
            switch (index) {
                case 0:
                    // Display Product Languages
                    for (String langKey : ServerConfig.getDefault().getAvailableLanguages()) {
                        String displayValue = new Locale(langKey).getDisplayName(Locale.getDefault());
                        if (TextUtils.isEmpty(displayValue))
                            displayValue = langKey;
                        resultList.add(new SettingsMenuItem(displayValue, SettingsMenuItem.SettingsMenuItemType.Languages, langKey));
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Segment must be == 0, but was " + index);
            }
            return new SettingsMenuSection(SECTIONS[index], resultList);
        }
    }

}
