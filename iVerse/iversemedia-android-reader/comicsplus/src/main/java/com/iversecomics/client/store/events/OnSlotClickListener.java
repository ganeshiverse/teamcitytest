package com.iversecomics.client.store.events;

import android.support.v4.app.Fragment;

import com.iversecomics.client.store.model.FeaturedSlot;

public interface OnSlotClickListener {
    void onSlotClick(Fragment fragment, FeaturedSlot slot);
}