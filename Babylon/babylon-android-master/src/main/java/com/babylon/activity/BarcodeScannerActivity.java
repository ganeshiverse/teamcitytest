package com.babylon.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;

import com.babylon.utils.Constants;
import com.google.zxing.Result;
import com.google.zxing.client.android.CaptureActivity;

public class BarcodeScannerActivity extends CaptureActivity {

    @Override
    public void handleDecode(Result rawResult, Bitmap barcodeBitmap, float scaleFactor) {
        final String barcode = rawResult.getText(); //5010511470914

        final Intent i = new Intent();
        i.putExtra(Constants.EXTRA_BARCODE, barcode);

        setResult(Activity.RESULT_OK, i);

        finish();
    }

}
