package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.SiteListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 30/06/15.
 */
public class GetSiteListLoader extends DataAsyncTaskLibLoader<SiteListResponse> {

    private String mUserId;

    public GetSiteListLoader(Context context) {
        super(context);
    }

    public GetSiteListLoader(Context context, String userId) {
        super(context);
        this.mUserId = userId;
    }

    @Override
    protected SiteListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getSiteList(mUserId);
    }

}
