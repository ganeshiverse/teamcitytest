package com.babylon.database.check;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.babylon.database.BaseSQLiteOpenHelper;
import com.babylon.database.Table;

public class CheckDatabaseHelper extends BaseSQLiteOpenHelper {

    public static final String TAG = CheckDatabaseHelper.class.getSimpleName();
    public static final String DATABASE_NAME = "com.babylon.check";
    private static final int DATABASE_VERSION = 1;
    private static CheckDatabaseHelper instance;

    public static CheckDatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new CheckDatabaseHelper(context);
        }
        return instance;
    }

    public CheckDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder(1024);
        for (CheckTables table : CheckTables.values()) {
            if (appendCreateTableSQL(sql, table)) continue;
            db.execSQL(sql.toString());
            sql.setLength(0);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(CheckDatabaseHelper.class.getSimpleName(), "Upgrading database from v." + oldVersion + " to v." + newVersion);
        /* drop all tables */
        for (Table table : Table.values()) {
            db.execSQL("DROP TABLE IF EXISTS " + table);
        }
        onCreate(db);
    }

    public static void deleteDataBase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }
}