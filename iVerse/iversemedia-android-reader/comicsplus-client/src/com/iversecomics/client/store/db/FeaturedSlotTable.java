package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.iversecomics.client.store.model.FeaturedSlot;

public class FeaturedSlotTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri    CONTENT_URI            = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "featuredslots");

    public static final Uri    CONTENT_URI_BY_SLOTID = Uri.withAppendedPath(CONTENT_URI, "byid");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of my comics.
     */
    public static final String CONTENT_TYPE           = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                              + "/vnd.iversecomics.store.featuredslots";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single user.
     */
    public static final String CONTENT_ITEM_TYPE      = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                              + "/vnd.iversecomics.store.featuredslots";

    public static final String DEFAULT_SORT_ORDER     = "slotid ASC";
    public static final String TABLE                  = "featuredslot";

    public static final String SLOTID                 = "slotid";
    public static final String SLOT                   = "slotposition";
    public static final String SLOTTYPE               = "slottype";
    public static final String TITLE	              = "title";
    public static final String IMAGEURL	              = "imageurl";
    public static final String TARGETURL        	  = "targeturl";
    public static final String SUBSCRIPTION           = "subscription";
    public static final String LANGUAGECODE           = "languagecode";
    public static final String PRODUCTID              = "productid";
    public static final String PROMOTIONID            = "promotionid";
    public static final String SUPPLIERID             = "supplierid";
    public static final String GROUPID                = "groupid";
    
    public static final String PROJECTION_FULL[]      = new String[] {
            TABLE + '.' + _ID,
            TABLE + '.' + SLOTID,
            SLOT,
            SLOTTYPE,
            TITLE,
            IMAGEURL,
            TARGETURL,
            SUBSCRIPTION,
            LANGUAGECODE,
            PRODUCTID,
            PROMOTIONID,
            SUPPLIERID,
            GROUPID };

    public static final String PROJECTION_BRIEF[]     = new String[] {
            TABLE + '.' + _ID,
            TABLE + '.' + SLOTID,
            SLOT,
            SLOTTYPE,
            TITLE,
            IMAGEURL,
            SUBSCRIPTION,
            LANGUAGECODE};

    public static ContentValues asValues(FeaturedSlot slot) {
        ContentValues values = new ContentValues();
        if (slot.getSlotID() >= 0) {
            values.put(_ID, slot.getSlotID());
        }
        
        values.put(SLOTID, slot.getSlotID());
        values.put(SLOT, slot.getSlot().ordinal());
        values.put(SLOTTYPE, slot.getSlotType().ordinal());
        values.put(TITLE, slot.getTitle());
        values.put(IMAGEURL, slot.getImageURL());
        values.put(TARGETURL, slot.getTargetURL());
        values.put(SUBSCRIPTION, slot.getSubscription());
        values.put(LANGUAGECODE, slot.getLanguageCode());
        values.put(PRODUCTID, slot.getProductID());
        values.put(PROMOTIONID, slot.getPromotionID());
        values.put(SUPPLIERID, slot.getSupplierID());
        values.put(GROUPID, slot.getGroupID());
        
        return values;
    }

    public static FeaturedSlot fromCursor(Cursor c) {
    	FeaturedSlot slot = new FeaturedSlot();
    	
    	slot.setSlotID(getInteger(c, _ID));
    	slot.setSlot(getInteger(c, SLOT));
    	slot.setSlotType(getInteger(c, SLOTTYPE));
    	slot.setTitle(getString(c, TITLE));
    	slot.setImageURL(getString(c, IMAGEURL));
    	slot.setTargetURL(getString(c, TARGETURL));
    	slot.setSubscription(getBoolean(c, SUBSCRIPTION));
    	slot.setLanguageCode(getString(c, LANGUAGECODE));
    	slot.setProductID(getInteger(c, PRODUCTID));
    	slot.setPromotionID(getInteger(c, PROMOTIONID));
    	slot.setSupplierID(getInteger(c, SUPPLIERID));
    	slot.setGroupID(getInteger(c, GROUPID));
    	
        return slot;
    }
}
