package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.HelpResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/14/2015.
 */
public class HelpLoader extends DataAsyncTaskLibLoader<HelpResponse> {

    private String token;
    private String moduleId;


    public HelpLoader(Context context, String moduleId, String token) {
            super(context);
        this.token = token;
        this.moduleId = moduleId;
    }

    @Override
    protected HelpResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getHelpInfo(moduleId, token);
    }
}
