package com.iversecomics.client.my.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import com.iversecomics.client.my.MyComicsModel;

public abstract class AbstractMyComicsProviderTest extends ProviderTestCase2<MyComicsProvider> {
    private Context               context;
    protected MockContentResolver resolver;

    public AbstractMyComicsProviderTest() {
        super(MyComicsProvider.class, MyComicsDB.AUTHORITY);
    }

    protected void assertDatabaseRowCount(String msg, Cursor c, int expectedCount) {
        assertNotNull(msg + " (cursor should not be null", c);
        assertTrue(msg + " (move to first, we are expecting at least 1 row)", c.moveToFirst());
        assertEquals(msg + " (row count)", expectedCount, c.getCount());
    }

    protected void assertDatabaseUriHasDBId(String type, Uri uri) {
        getDatabaseId(type, uri);
    }

    protected void freshDB() {
        MyComicsDatabaseHelper helper = MyComicsDatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        helper.rebuild(db); // force rebuild
    }

    protected long getDatabaseId(String type, Uri uri) {
        assertNotNull(type + " should have had a uri", uri);
        long dbId = Long.parseLong(uri.getLastPathSegment());
        assertTrue(type + " DB Id [" + dbId + "] should be >= 0", dbId >= 0);
        return dbId;
    }

    protected MyComicsModel getMyComicsModel() {
        return new MyComicsModel(context);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        this.context = getMockContext();
        this.resolver = getMockContentResolver();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
