package com.babylon.chart;

import android.net.http.AndroidHttpClient;

import com.babylon.App;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import javax.net.ssl.HostnameVerifier;

/**
 * This class manages connection to server and have methods to communicate with
 * it via POST, GET and DELETE methods. Also there are methods that parse response from
 * server.
 */
public class HttpConnector {
    private static final String TAG = HttpConnector.class.getSimpleName();
    private static final String USER_AGENT = "android";

    //private final String SUB_URL = "/ribs/";

    private static HttpConnector instance = null;
    private AndroidHttpClient httpClient;

    private HttpConnector() {
        httpClient = getAndroidHttpClient();
    }

    public static HttpConnector getInstance() {
        if (instance == null)
            instance = new HttpConnector();
        return instance;
    }

    protected AndroidHttpClient getAndroidHttpClient() {
        if (httpClient != null) {
            httpClient.close();
        }

        httpClient = AndroidHttpClient.newInstance(USER_AGENT, App.getInstance());
        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);

        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", socketFactory, 443));

        return httpClient;
    }

    public String sendGETRequest(final String url) throws URISyntaxException, IOException {
        String result = null;

        L.i(TAG, url);

        Wsse wsse = SyncUtils.getWsse();
        String token = wsse != null ? wsse.toString() : "";

        final HttpGet get = new HttpGet(url);
        get.setHeader("content-type", "text/html;charset=utf-8");
        get.setHeader(Constants.Header_RequestKeys.HEADER_WSSE_KEY, token);
        get.addHeader("timezone", DateUtils.getDefaultTimezoneId());
        try {
            result = executeRequest(get);
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }
        L.i(TAG, "Result json" + result);
        return result;
    }

    private String executeRequest(HttpGet get) throws IOException {
        String result = null;

        try {
            final HttpResponse response = httpClient.execute(get);
            result = readHttpResponse(response);
        } catch (Exception e){
            L.e(TAG, e.toString(), e);
        }
        return result;
    }

    private String readHttpResponse(final HttpResponse response) {
        BufferedReader reader = null;
        final StringBuffer sb = new StringBuffer("");

        try {
            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

        } catch (final IOException e) {
            L.e(TAG, e.toString(), e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                L.e(TAG, e.toString(), e);
            }
        }

        return sb.toString();
    }
}
