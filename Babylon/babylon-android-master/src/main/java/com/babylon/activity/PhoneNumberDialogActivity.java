package com.babylon.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.PatientPatch;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.model.Patient;
import com.babylon.utils.AlertDialogHelper;
import org.apache.http.HttpStatus;

public class PhoneNumberDialogActivity extends FragmentActivity {

    EditText phoneNumberField;
    EditText countryCodeField;
    ProgressBar progressBar;
    ImageButton okButton;
    ImageButton cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_dialog);

        phoneNumberField = (EditText)findViewById(R.id.phone_number_dlg_phone_edit_field);
        countryCodeField = (EditText)findViewById(R.id.phone_number_dlg_country_edit_field);
        progressBar = (ProgressBar)findViewById(R.id.phone_number_dlg_progress);
        okButton = (ImageButton)findViewById(R.id.phone_number_dlg_ok);
        cancelButton = (ImageButton)findViewById(R.id.phone_number_dlg_cancel);

        phoneNumberField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId ==
                        EditorInfo.IME_ACTION_DONE)) {
                    Log.i("", "Enter pressed");
                    submitPhoneNumber();
                }
                return false;
            }
        });
        if(App.getInstance().getCurrentRegion()!=null) {
            countryCodeField.setHint(App.getInstance().getCurrentRegion().getDefaultPhoneCountryCode());
        }
        else{
            countryCodeField.setHint("+44");
        }
        countryCodeField.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void onOkPressed(View view) {
        submitPhoneNumber();
    }

    public void onCancelPressed(View view) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private boolean checkPhoneNumberValidation() {
        if (TextUtils.isEmpty(phoneNumberField.getText())) {
            AlertDialogHelper.getInstance().showMessage(this, R.string.phone_number_dialog_incorrect_number, null);
            return true;
        } else if (TextUtils.isEmpty(countryCodeField.getText()) && TextUtils.isEmpty(countryCodeField.getHint())) {
            AlertDialogHelper.getInstance().showMessage(this, R.string.phone_number_dialog_incorrect_country_code, null);
            return true;
        }
        return false;
    }

    private void submitPhoneNumber() {
        if (checkPhoneNumberValidation()) return;

        progressBar.setVisibility(View.VISIBLE);
        okButton.setEnabled(false);

        Patient patient = new Patient();
        patient.setId(App.getInstance().getPatient().getId());
        patient.setPhoneNumber(phoneNumberField.getText().toString());
        String countryCode = (!TextUtils.isEmpty(countryCodeField.getText()))? countryCodeField.getText().toString() : countryCodeField.getHint().toString();
        patient.setCountryCode(countryCode);
        new PatientPatch().withEntry(patient, new String[] {"phone_number", "phone_country_code"}).execute(this, new RequestListener<Patient>() {
            @Override
            public void onRequestComplete(Response<Patient> response) {
                progressBar.setVisibility(View.INVISIBLE);
                okButton.setEnabled(true);
                if (response.getStatus() == HttpStatus.SC_OK) {
                    //Success
                    App.getInstance().setPatient(response.getData());
                    setResult(Activity.RESULT_OK);
                    finish();
                } else if (response.getStatus() == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                    //Incorrect format
                    AlertDialogHelper.getInstance().showMessage(PhoneNumberDialogActivity.this, R.string.phone_number_dialog_incorrect_number);
                } else {
                    //Other error
                    AlertDialogHelper.getInstance().showMessage(PhoneNumberDialogActivity.this, R.string.phone_number_dialog_request_error);
                }
            }
        });
    }
}
