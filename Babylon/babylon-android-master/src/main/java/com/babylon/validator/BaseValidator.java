package com.babylon.validator;

public abstract class BaseValidator {
    protected BaseValidator nextValidator;
    protected String errorMessage;

    public BaseValidator(BaseValidator nextValidator, String errorMessage) {
        this.nextValidator = nextValidator;
        this.errorMessage = errorMessage;
    }

    public String performValidation(String string) {
        String error = validate(string);
        if (error == null && nextValidator != null) {
            error = nextValidator.validate(string);
        }

        return error;
    }

    protected abstract String validate(String string);
}
