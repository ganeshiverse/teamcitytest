package com.iversecomics.util.text;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {
    @Test
    public void testIsBlank() {
        Assert.assertTrue("isBlank(null)", StringUtils.isBlank(null));
        Assert.assertTrue("isBlank(\"\")", StringUtils.isBlank(""));
        Assert.assertTrue("isBlank(\" \")", StringUtils.isBlank(" "));
        Assert.assertTrue("isBlank(\"\\t\")", StringUtils.isBlank("\t"));
        Assert.assertFalse("isBlank(\"abc\")", StringUtils.isBlank("abc"));
        Assert.assertFalse("isBlank(\" . \")", StringUtils.isBlank(" . "));
    }

    @Test
    public void testIsNoBlank() {
        Assert.assertFalse("isNoBlank(null)", StringUtils.isNoBlank(null));
        Assert.assertFalse("isNoBlank(\"\")", StringUtils.isNoBlank(""));
        Assert.assertFalse("isNoBlank(\" \")", StringUtils.isNoBlank(" "));
        Assert.assertFalse("isNoBlank(\"\\t\")", StringUtils.isNoBlank("\t"));
    }

    @Test
    public void testJoinStringArray() {
        Assert.assertEquals("", StringUtils.join(new String[]{}, ","));
        
        String items[] = new String[] { "A", "B", "C" };
        Assert.assertEquals("ABC", StringUtils.join(items,""));
        Assert.assertEquals("A,B,C", StringUtils.join(items,","));
        Assert.assertEquals("A, B, C", StringUtils.join(items,", "));
    }

}
