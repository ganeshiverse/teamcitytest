package com.iversecomics.ui.main.event;

import com.iversecomics.client.store.model.Group;

/**
 */
public class ShowGroupEvent {
    public final Group group;

    public ShowGroupEvent(Group  group) {
        this.group = group;
    }
}
