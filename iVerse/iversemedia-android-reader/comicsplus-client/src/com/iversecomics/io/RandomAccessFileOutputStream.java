package com.iversecomics.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class RandomAccessFileOutputStream extends OutputStream {

    private RandomAccessFile file;

    public RandomAccessFileOutputStream(RandomAccessFile file) {
        super();
        this.file = file;
    }

    @Override
    public void write(byte[] b) throws IOException {
        this.file.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        this.file.write(b, off, len);
    }

    @Override
    public void write(int b) throws IOException {
        this.file.write(b);
    }
}
