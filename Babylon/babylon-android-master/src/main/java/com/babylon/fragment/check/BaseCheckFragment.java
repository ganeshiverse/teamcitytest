package com.babylon.fragment.check;

import android.app.Activity;

import com.babylon.activity.check.CheckActivity;
import com.babylon.fragment.BaseFragment;

public class BaseCheckFragment extends BaseFragment {

    @Override
    public void onStart() {
        super.onStart();
        ((CheckActivity) getActivity()).updateActivity();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof CheckActivity)) {
            throw new RuntimeException("You should use only CheckActivity with this fragment");
        }
    }
}
