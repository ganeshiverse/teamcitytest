package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 15/09/15.
 */
public class StateListResponse extends BaseResponse implements Serializable {

    @SerializedName("SysLookupNames")
    private ArrayList<SystemLookupNames> systemLookupNamesAL;

    @SerializedName("UserLookups")
    private ArrayList<UserLookups> userLookupsAL;

    @SerializedName("SystemLookups")
    private ArrayList<SystemLookups> systemLookupsAL;

    public ArrayList<SystemLookupNames> getSystemLookupNamesAL() {
        return systemLookupNamesAL;
    }

    public ArrayList<UserLookups> getUserLookupsAL() {
        return userLookupsAL;
    }

    public ArrayList<SystemLookups> getSystemLookupsAL() {
        return systemLookupsAL;
    }

    public class SystemLookups implements Serializable {
        @SerializedName("Id")
        private String id;

        @SerializedName("list")
        private ArrayList<StateDetails> stateDetailsAL;

        public String getId() {
            return id;
        }

        public ArrayList<StateDetails> getStateDetailsAL() {
            return stateDetailsAL;
        }
    }

    public class StateDetails implements Serializable {
        @SerializedName("Id")
        private String id;

        @SerializedName("Value")
        private String value;

        public String getId() {
            return id;
        }

        public String getValue() {
            return value;
        }
    }

    public class SystemLookupNames implements Serializable {}

    public class UserLookups implements Serializable {}
}
