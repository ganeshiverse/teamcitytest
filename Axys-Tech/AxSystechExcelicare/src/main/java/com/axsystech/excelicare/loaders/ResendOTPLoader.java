package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class ResendOTPLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private final String userName;
    private final String siteID;
    private final String deviceId;

    public ResendOTPLoader(final Context context, String userName, String siteId, String deviceId) {
        super(context);
        this.userName = userName;
        this.siteID = siteId;
        this.deviceId = deviceId;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().generateNewOTP(userName, siteID, deviceId);
    }
}
