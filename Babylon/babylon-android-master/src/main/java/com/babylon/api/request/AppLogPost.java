package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.Urls;
import com.babylon.model.AppLog;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class AppLogPost extends AbsRequestPost<AppLog, String> {

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.APP_LOG);
    }

}
