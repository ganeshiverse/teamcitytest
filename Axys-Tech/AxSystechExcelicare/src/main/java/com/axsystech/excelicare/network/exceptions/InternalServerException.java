package com.axsystech.excelicare.network.exceptions;

/**
 * Date: 07.05.14
 * Time: 13:47
 *
 * @author SomeswarReddy
 */
public class InternalServerException extends Exception {

    private static final long serialVersionUID = 7760827596787602278L;

    public InternalServerException() {
    }

    public InternalServerException(String detailMessage) {
        super(detailMessage);
    }

    public InternalServerException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InternalServerException(Throwable throwable) {
        super(throwable);
    }
}