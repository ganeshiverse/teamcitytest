package com.babylonpartners.babylon;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;

public class VideoSessionActivity extends Activity implements VideoSession.Listener {

    public static final String INTENT_EXTRA_SESSION_ID = "sessionId";
    public static final String INTENT_EXTRA_TOKEN = "token";
    public static final String INTENT_EXTRA_IDENTIFIER = "identifier";
    public static final String INTENT_EXTRA_CONSULTANT_NAME = "consultantName";
    public static final String INTENT_EXTRA_CONSULTANT_AVATAR_URL = "consultantAvatarUrl";
    public static final String INTENT_EXTRA_ENABLE_VOICE_CALLS = "enableVoiceCalls";

    private String sessionId;
    private String token;
    private String consultantName;
    private String consultantAvatarUrl;

    private VideoSession video;
    private Button endVideoButton;
    private Button switchCameraButton;
    private Button pleaseCallMeButton;
    private ViewGroup targetView;
    private TextView textViewStatus;
    private AudioManager audioManager;
    private TextView consultantNameTextView;
    private ImageView consultantAvatarView;
    private boolean enableVoiceCalls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadIntentExtras();
        setupViews();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        consultantNameTextView.setText(consultantName);
        prepareAvatar();
    }

    private void prepareAvatar() {
        Picasso.with(this).load(consultantAvatarUrl).transform(new CircleTransform()).into(consultantAvatarView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (video != null) {
            video.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (video != null) {
            video.onResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (video != null) {
            video.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        if (video != null) {
            video.onDestroy();
        }

        audioManager.setSpeakerphoneOn(false);
        super.onDestroy();
    }

    private void setupViews() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video_session);

        targetView = (ViewGroup) findViewById(R.id.videoView);
        textViewStatus = (TextView) findViewById(R.id.textViewStatus);
        endVideoButton = (Button) findViewById(R.id.endVideoButton);
        endVideoButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    endConsultationButtonPressed();
                    return true;
                }
                return false;
            }
        });

        switchCameraButton = (Button) findViewById(R.id.switchCameraButton);
        switchCameraButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    switchCameraButtonPressed();
                    return true;
                }
                return false;
            }
        });

        pleaseCallMeButton = (Button) findViewById(R.id.pleaseCallMeButton);
        pleaseCallMeButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    pleaseCallMeButtonPressed();
                    return true;
                }
                return false;
            }
        });

        consultantNameTextView = (TextView) findViewById(R.id.consultantName);
        consultantAvatarView = (ImageView) findViewById(R.id.consultantAvatar);

        animateStatusTextView();
    }

    @Override
    public void onBackPressed() {
        endConsultationButtonPressed();
    }

    private void loadIntentExtras() {
        sessionId = getIntent().getExtras().getString(INTENT_EXTRA_SESSION_ID);
        token = getIntent().getExtras().getString(INTENT_EXTRA_TOKEN);
        consultantName = getIntent().getExtras().getString(INTENT_EXTRA_CONSULTANT_NAME);
        consultantAvatarUrl = getIntent().getExtras().getString(INTENT_EXTRA_CONSULTANT_AVATAR_URL);
        enableVoiceCalls = getIntent().getExtras().getBoolean(INTENT_EXTRA_ENABLE_VOICE_CALLS);
    }

    private void startVideo() {
        video = new VideoSession(this, this, sessionId, token, this.targetView);
    }

    private void endConsultationButtonPressed() {
        this.video.finishVideoConfirmation();
    }

    private void pleaseCallMeButtonPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Babylon")
                .setMessage("Do you wish to receive a call from the practitioner?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pleaseCallMeButton.setVisibility(View.GONE);
                        video.finishVideo();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private void switchCameraButtonPressed() {
        this.video.switchCamera();
    }

    @Override
    public void sessionDroppedStream() {
        Log.v("VideoSessionActivity", "sessionDroppedStream()");
        //TODO: ajax to show label depending on status
        runOnUiThread(new Runnable() {
            public void run() {
                textViewStatus.setText("Consultant has ended your session.");
            }
        });

    }

    @Override
    public void videoSessionEnded()
    {

        Intent resultIntent = new Intent();
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void subscriberConnected() {
        if (enableVoiceCalls) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Button pleaseCallMeButton = (Button) findViewById(R.id.pleaseCallMeButton);
                    pleaseCallMeButton.setVisibility(View.VISIBLE);
                }
            }, 15000);
        }
    }

    private void animateStatusTextView() {
        final View textViewStatus = this.textViewStatus;

        final AlphaAnimation animation1 = new AlphaAnimation(0.4f, 1.0f);
        animation1.setDuration(600);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.4f);
        animation2.setDuration(600);

        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                textViewStatus.startAnimation(animation2);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });


        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                textViewStatus.startAnimation(animation1);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        textViewStatus.startAnimation(animation1);
    }

}
