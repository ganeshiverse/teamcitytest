package com.iversecomics.client.store.events;

import com.iversecomics.client.store.model.FeaturedSlot;

import android.support.v4.app.Fragment;

public interface OnSlotClickListener {
    void onSlotClick(Fragment fragment, FeaturedSlot slot);
}