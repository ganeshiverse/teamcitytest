package com.babylon.sync.monitor;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import com.babylon.api.request.ActivityListDelete;
import com.babylon.api.request.ActivityListGet;
import com.babylon.api.request.AddActivityListPost;
import com.babylon.api.request.EditActivityPatch;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.Table;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.AbsSyncStrategy;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.AutoTrackedActivity;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class PhysicalActivitySync extends AbsSyncStrategy {

    @Override
    public void push(ContentResolver contentResolver, Bundle bundle) {
        AutoTrackedActivity.save(contentResolver);

        Cursor notSyncActivityCursor = getUpdateCursor(contentResolver, Table.PHYSICAL_ACTIVITY,
                PhysicalActivitySchema.CONTENT_URI, null);
        List<PhysicalActivity> addActivityList = new ArrayList<>();
        List<String> deleteIds = new ArrayList<>();
        List<PhysicalActivity> patchActivities = new ArrayList<>();

        while (notSyncActivityCursor.moveToNext()) {
            PhysicalActivity physicalActivity = new PhysicalActivity();
            physicalActivity.createFromCursor(notSyncActivityCursor);

            switch (physicalActivity.getSyncWithServer()) {
                case SyncUtils.SYNC_TYPE_ADD:
                    addActivityList.add(physicalActivity);
                    break;
                case SyncUtils.SYNC_TYPE_DELETE:
                    deleteIds.add(physicalActivity.getId());
                    break;
                case SyncUtils.SYNC_TYPE_PATCH:
                    patchActivities.add(physicalActivity);
                    break;
                default:
                    break;
            }

            if ((notSyncActivityCursor.getPosition() + 1) % SyncUtils.MAX_SYNC_OBJECTS_NUMBER == 0 ||
                    notSyncActivityCursor.isLast()) {
                sync(contentResolver, addActivityList, deleteIds, patchActivities);
                addActivityList.clear();
                deleteIds.clear();
                patchActivities.clear();
            }
        }

        notSyncActivityCursor.close();
    }

    private void sync(ContentResolver contentResolver, List<PhysicalActivity> addActivityList, List<String>
            deleteIds, List<PhysicalActivity> patchActivities) {
        if (!addActivityList.isEmpty()) {
            processNewActivities(contentResolver, addActivityList);
        }

        if (!deleteIds.isEmpty()) {
            ActivityListDelete activityListDelete = ActivityListDelete.newActivityListDelete(deleteIds);
            Response deleteResponse = activityListDelete.execute();
            if (deleteResponse != null && deleteResponse.isSuccess()) {
                deleteActivities(contentResolver, deleteIds);
            }
        }

        if (!patchActivities.isEmpty()) {
            patchActivities(contentResolver, patchActivities);
        }
    }

    private void patchActivities(ContentResolver contentResolver, List<PhysicalActivity> patchActivities) {
        final int patchActivitiesSize = patchActivities.size();
        for (int i = 0; i < patchActivitiesSize; i++) {
            PhysicalActivity curActivity = patchActivities.get(i);

            PhysicalActivity.PatchPhysicalActivity newActivity = new PhysicalActivity.PatchPhysicalActivity
                    (patchActivities.get(i));

            EditActivityPatch patch = EditActivityPatch.getEditActivityPatch(newActivity, curActivity.getId());
            Response<PhysicalActivity.PatchPhysicalActivity> responseActivity = patch.execute();

            processPatchResponse(contentResolver, patchActivities, responseActivity);
        }
    }

    private void processNewActivities(ContentResolver contentResolver, List<PhysicalActivity> addActivityList) {
        List<PhysicalActivity> serverPhysicalActivities = getServerModelForPost
                (addActivityList);

        List<PhysicalActivity> result = postAddActivities(serverPhysicalActivities);

        if (!result.isEmpty()) {
            updateAddedActivities(contentResolver, result, addActivityList);
        }
    }

    private List<PhysicalActivity> getServerModelForPost(List<PhysicalActivity> addActivityList) {
        List<PhysicalActivity> serverPhysicalActivities = new ArrayList<PhysicalActivity>();

        final int addActivityListSize = addActivityList.size();
        for (int i = 0; i < addActivityListSize; i++) {
            PhysicalActivity newActivity = addActivityList.get(i).getServerValues();

            serverPhysicalActivities.add(newActivity);
        }

        return serverPhysicalActivities;
    }

    private void processPatchResponse(ContentResolver contentResolver, List<PhysicalActivity> patchActivities,
                                      Response<PhysicalActivity.PatchPhysicalActivity> responseActivity) {
        if (responseActivity != null && responseActivity.isSuccess()
                && responseActivity.getData() != null) {
            PhysicalActivity.PatchPhysicalActivity patchActivity = responseActivity.getData();
            PhysicalActivity newActivity = patchActivity.getActivity();

            List<PhysicalActivity> list = new ArrayList<PhysicalActivity>();
            list.add(newActivity);

            updateAddedActivities(contentResolver, list, patchActivities);
        }
    }

    @Override
    public void pull(ContentResolver contentResolver, Bundle bundle) {
        long minDate = bundle.getLong(Constants.EXTRA_MIN_DATE, SyncUtils.getMinDateSyncInMillis());
        long maxDate = bundle.getLong(Constants.EXTRA_MAX_DATE, DateUtils.getDayStartMills(new Date(), 1));

        Response<ResponseList<PhysicalActivity>> response = ActivityListGet
                .newActivityListGet(minDate, maxDate).execute();
        processResponse(contentResolver, response, minDate, maxDate);
    }

    public static void processResponse(ContentResolver contentResolver, Response<ResponseList<PhysicalActivity>>
            response, long minDate, long maxDate) {
        ResponseList<PhysicalActivity> data = response.getData();
        if (response.isSuccess() && data != null) {
            List<PhysicalActivity> list = data.getList();
            saveResults(contentResolver, list, minDate, maxDate);
        }
    }

    private List<PhysicalActivity> postAddActivities(List<PhysicalActivity> physicalActivities) {
        AddActivityListPost addActivityListPost = new AddActivityListPost();
        PhysicalActivity.PhysicalActivityList physicalActivityList = new PhysicalActivity.PhysicalActivityList();
        physicalActivityList.setActivities(new ArrayList<>(physicalActivities));
        addActivityListPost.withEntry(physicalActivityList);

        Response<ResponseList<PhysicalActivity>> response = addActivityListPost.execute();

        List<PhysicalActivity> result = new ArrayList<>();
        if (response != null && response.isSuccess() && response.getData() != null &&
                response.getData().getList() != null) {
            result = response.getData().getList();
        }

        return result;
    }

    private void updateAddedActivities(ContentResolver contentResolver,
                                       List<PhysicalActivity> responsepActivities,
                                       List<PhysicalActivity> oldActivities) {


        List<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        final int responseActivitiesSize = responsepActivities.size();
        if (responseActivitiesSize == oldActivities.size()) {
            for (int i = 0; i < responseActivitiesSize; i++) {
                ContentProviderOperation operation = getUpdateOperation(responsepActivities.get(i),
                        oldActivities.get(i));
                batch.add(operation);
            }
        }

        DatabaseOperationUtils.applyBatch(contentResolver, batch);

    }

    private ContentProviderOperation getUpdateOperation(PhysicalActivity newPhysicalActivity,
                                                        PhysicalActivity oldPhysicalActivity) {
        String id = oldPhysicalActivity.getId();

        newPhysicalActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_ADDED);
        ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(PhysicalActivitySchema
                .CONTENT_URI)
                .withValues(newPhysicalActivity
                        .toContentValues())
                .withSelection(PhysicalActivitySchema.Columns.ID.getName() + " = ?",
                        new String[]{id});

        return builder.build();
    }

    private void deleteActivities(ContentResolver contentResolver, List<String> deleteIds) {
        List<ContentProviderOperation> batch = new LinkedList<>();

        for (String id : deleteIds) {
            batch.add(ContentProviderOperation.newDelete(PhysicalActivitySchema.CONTENT_URI)
                    .withSelection(PhysicalActivitySchema.Columns.ID.getName() + " = ?",
                            new String[]{id})
                    .build());
        }
        DatabaseOperationUtils.applyBatch(contentResolver, batch);
    }

    public static void saveResults(ContentResolver contentResolver,
                                   List<PhysicalActivity> activities, long minDate, long maxDate) {
        List<ContentProviderOperation> batch = new LinkedList<>();
        StringBuilder stringActivityIdBuilder = new StringBuilder();
        stringActivityIdBuilder.append("(");
        for (PhysicalActivity physicalActivity : activities) {
            stringActivityIdBuilder.append("'")
                    .append(physicalActivity.getId()).append("'").append(",");
            physicalActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_ADDED);
            batch.add(ContentProviderOperation.newInsert(PhysicalActivitySchema.CONTENT_URI)
                    .withValues(physicalActivity
                            .toContentValues()).build());
        }

        DatabaseOperationUtils.applyBatch(contentResolver, batch);
        stringActivityIdBuilder.deleteCharAt(stringActivityIdBuilder.length() - 1).append(")");

        deleteActivities(contentResolver, minDate, maxDate, stringActivityIdBuilder);
    }

    private static void deleteActivities(ContentResolver contentResolver, long minDate, long maxDate,
                                         StringBuilder stringActivityIdBuilder) {
        String query = "";

        if (stringActivityIdBuilder.toString().length() > 1) {
            query = PhysicalActivitySchema.Query.SELECTION_DELETE + stringActivityIdBuilder.toString()
                    + " AND ";
        }

        contentResolver.delete(PhysicalActivitySchema.CONTENT_URI,
                query +
                        PhysicalActivitySchema.Columns.START_DATE.getName() + " >= " + String.valueOf(minDate)
                        + " AND " +
                        PhysicalActivitySchema.Columns.START_DATE.getName() + " < " + String.valueOf(maxDate)
                        + " AND " +
                        PhysicalActivitySchema.Columns.SYNC_WITH_SERVER + " != " + SyncUtils.SYNC_TYPE_ADD,
                null);
    }
}
