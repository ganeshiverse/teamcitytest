package com.iversecomics.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.my.db.MyComicsDB;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Util {

    public static float getRandom() {
        double d = Math.random();
        return (float) ((d * 10 + (Time.getCurrentTimeInMilliseconds() % 10)) / 20);
    }

    public static void exportDB() {


        File dbFile =
                new File(Environment.getDataDirectory() + "/data/" + IverseApplication.getApplication().getPackageName() + "/databases/" + MyComicsDB.DB_NAME);

        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }
        File file = new File(exportDir, dbFile.getName());

        try {
            file.createNewFile();
            copyFile(dbFile, file);

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    static void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }

    /**
     * Send the given feedback to email app.
     * @return Returns true if the email app was launched regardless if the email was sent.
     */
    public static boolean sendFeedBack(Activity activity) {
        final String MSG_SENDTO = "support@iversemedia.com";
        final String MSG_SUBJECT = String.format(activity.getResources().getString(R.string.app_feedback_subject), getAppNameandVersion(activity));
        final String MSG_BODY = String.format(activity.getResources().getString(R.string.app_feedback_body), getDeviceName());

            Intent theIntent = new Intent(Intent.ACTION_SEND);
            String theBody = "\n"+"\n"+"\n"+"\n"+MSG_BODY+ "\n";
            theIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{MSG_SENDTO});
            theIntent.putExtra(Intent.EXTRA_TEXT, theBody);
            theIntent.putExtra(Intent.EXTRA_SUBJECT, MSG_SUBJECT);
            theIntent.setType("plain/text");
            Boolean hasSendRecipients = (activity.getPackageManager().queryIntentActivities(theIntent, 0).size() > 0);
            if (hasSendRecipients) {
                activity.startActivity(Intent.createChooser(theIntent, ""));
                return true;
            } else {
                new AlertDialog.Builder(activity).setTitle(activity.getString(R.string.feedback_no_action)).setPositiveButton(activity.getResources().getString(R.string.status_ok), null).create().show();
                return false;
            }
    }

    /**
     * Return the application's friendly name and Version number.
     * @return Returns the application name as defined by the android:name attribute && android:versionName.
     */
    public static String getAppNameandVersion(Activity activity) {
        PackageManager pm = activity.getPackageManager();
        PackageInfo pi;
        try {
            pi = pm.getPackageInfo(activity.getPackageName(), 0);
            return (pi.applicationInfo.loadLabel(pm) +" "+ pi.versionName).toString();

        } catch (PackageManager.NameNotFoundException nnfe) {
            //doubt this will ever run since we want info about our own package
            return activity.getPackageName();
        }
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }
    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static void getSha1(Activity activity){

        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

}