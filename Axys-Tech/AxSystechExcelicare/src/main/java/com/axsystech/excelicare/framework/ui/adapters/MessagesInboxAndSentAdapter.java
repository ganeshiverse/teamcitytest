package com.axsystech.excelicare.framework.ui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.db.models.MessageListItemDBModel;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageInboxFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageSentFragment;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.google.common.io.Resources;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MessagesInboxAndSentAdapter extends BaseAdapter {

    private MainContentActivity activity;
    private Fragment mCurrentFragment;

    private ArrayList<MessageListItemDBModel> msgDataObjectsAL;
    private boolean isInbox;

    private boolean delete;

    private LayoutInflater inflater;

    public MessagesInboxAndSentAdapter(MainContentActivity activity, Fragment fragment,
                                       ArrayList<MessageListItemDBModel> msgDataObjectsAL, boolean isInbox, boolean delete) {
        this.activity = activity;
        this.mCurrentFragment = fragment;
        this.msgDataObjectsAL = msgDataObjectsAL;
        this.isInbox = isInbox;
        this.delete = delete;

        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void updateData(ArrayList<MessageListItemDBModel> msgDataObjectsAL) {
        this.msgDataObjectsAL = msgDataObjectsAL;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return msgDataObjectsAL != null && msgDataObjectsAL.size() > 0 ? msgDataObjectsAL.size() : 0;
    }

    @Override
    public MessageListItemDBModel getItem(int position) {
        return msgDataObjectsAL.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MessageListItemDBModel rowObject = getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.msg_inbox_row, parent, false);
        }
        Typeface regular = Typeface.createFromAsset(activity.getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(activity.getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(activity.getAssets(), "fonts/if_std_bold.ttf");


        CheckBox readStatusCheckBox = (CheckBox) convertView.findViewById(R.id.readStatusCheckBox);
        final CheckBox readStatusCheckBoxuncheckedmessage = (CheckBox) convertView.findViewById(R.id.readStatusCheckBoxuncheckedmessage);
        final ImageView userImageView = (ImageView) convertView.findViewById(R.id.userImageView);
        TextView senderNameTextView = (TextView) convertView.findViewById(R.id.senderNameTextView);
        TextView dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
        TextView specialityTextView = (TextView) convertView.findViewById(R.id.specialityTextView);
        TextView subjectTextView = (TextView) convertView.findViewById(R.id.subjectTextView);
        ImageView attachmentImageView = (ImageView) convertView.findViewById(R.id.attachmentImageView);
        final CheckBox priorityCheckBox = (CheckBox) convertView.findViewById(R.id.priorityCheckBox);
        TextView messageTextView = (TextView) convertView.findViewById(R.id.messageTextView);

        senderNameTextView.setTypeface(bold);
        dateTextView.setTypeface(regular);
        specialityTextView.setTypeface(regular);
        subjectTextView.setTypeface(regular);
        messageTextView.setTypeface(regular);

        // Assign values to UI elements - setChecked - true (Read) / false (Un-Read)
        if (!TextUtils.isEmpty(rowObject.getReadStatus()) && rowObject.getReadStatus().equalsIgnoreCase("Un-Read")) {
            readStatusCheckBox.setChecked(false); // means Un-Read the message
        } else {
            readStatusCheckBox.setChecked(true); // means Read the message
        }

        if (!TextUtils.isEmpty(rowObject.getPhotoUrl())) {
            String imagePath = rowObject.getPhotoUrl();
            if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                imagePath = "file://" + imagePath;
            }

            /*Picasso.with(activity)
                    .load(imagePath)
                    .placeholder(R.drawable.user_icon)
                    .error(R.drawable.user_icon)
                    .into(userImageView);*/

            Picasso.with(activity).load(imagePath).transform(new CircleTransform()).into(userImageView);
        }

        if (delete) {
            readStatusCheckBox.setButtonDrawable(convertView.getResources().getDrawable(R.drawable.checkbox_unselected));
        }

        readStatusCheckBoxuncheckedmessage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    readStatusCheckBoxuncheckedmessage.setButtonDrawable(buttonView.getResources().getDrawable(R.drawable.checkbox_selected));
                } else {

                }
            }
        });

        if (isInbox) {
            if (!TextUtils.isEmpty(rowObject.getSender())) {
                senderNameTextView.setText(rowObject.getSender());
                senderNameTextView.setVisibility(View.VISIBLE);
            } else {
                senderNameTextView.setVisibility(View.INVISIBLE);
            }
        } else {
            if (!TextUtils.isEmpty(rowObject.getSentTo())) {
                senderNameTextView.setText(rowObject.getSentTo());
                senderNameTextView.setVisibility(View.VISIBLE);
            } else {
                senderNameTextView.setVisibility(View.INVISIBLE);
            }
        }

        if (isInbox) {
            if (!TextUtils.isEmpty(rowObject.getReceivedDate())) {
                dateTextView.setVisibility(View.VISIBLE);
                /*String formateddate = parseDateToddMMyyyy(rowObject.getReceivedDate());
                dateTextView.setText(formateddate);*/
                dateTextView.setText(rowObject.getReceivedDate());

            } else {
                dateTextView.setVisibility(View.INVISIBLE);
            }
        } else {
            if (!TextUtils.isEmpty(rowObject.getSentDate())) {
                dateTextView.setText(rowObject.getSentDate());
                dateTextView.setVisibility(View.VISIBLE);
            } else {
                dateTextView.setVisibility(View.INVISIBLE);
            }
        }

        if (!TextUtils.isEmpty(rowObject.getSpeciality())) {
            specialityTextView.setText(rowObject.getSpeciality());
            specialityTextView.setVisibility(View.VISIBLE);
        } else {
            specialityTextView.setVisibility(View.GONE);
        }

        if (isInbox) {
            if (!TextUtils.isEmpty(rowObject.getSubject())) {
                subjectTextView.setText(rowObject.getSubject());
                subjectTextView.setVisibility(View.VISIBLE);
            } else {
                subjectTextView.setVisibility(View.INVISIBLE);
            }
        } else {
            if (!TextUtils.isEmpty(rowObject.getHeader())) {
                subjectTextView.setText(rowObject.getHeader());
                subjectTextView.setVisibility(View.VISIBLE);
            } else {
                subjectTextView.setVisibility(View.INVISIBLE);
            }
        }

        if (!TextUtils.isEmpty(rowObject.getAttachments())) {
            attachmentImageView.setVisibility(View.VISIBLE);
        } else {
            attachmentImageView.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(rowObject.getPriority())) {
            if (rowObject.getPriority().equalsIgnoreCase("High")) {
                priorityCheckBox.setChecked(true);
                priorityCheckBox.setButtonDrawable(R.drawable.priority_high);
            } else {
                priorityCheckBox.setChecked(false);
                priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
            }
        } else {
            priorityCheckBox.setChecked(false);
            priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
        }

        if (!TextUtils.isEmpty(rowObject.getMessageBody())) {
            messageTextView.setText(rowObject.getMessageBody());
            messageTextView.setVisibility(View.VISIBLE);
        } else {
            messageTextView.setVisibility(View.INVISIBLE);
        }

        priorityCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rowObject.setPriority(priorityCheckBox.isChecked() ? "High" : "Medium");
                if (rowObject.getPriority().equalsIgnoreCase("High")) {
                    priorityCheckBox.setChecked(true);
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_high);
                } else {
                    priorityCheckBox.setChecked(false);
                    priorityCheckBox.setButtonDrawable(R.drawable.priority_medium);
                }

                if (mCurrentFragment != null) {
                    if (mCurrentFragment instanceof MessageInboxFragment) {
                        ((MessageInboxFragment) mCurrentFragment).onMessagePriorityChanged(rowObject.getMessageId(), priorityCheckBox.isChecked());

                    } else if (mCurrentFragment instanceof MessageSentFragment) {
                        ((MessageSentFragment) mCurrentFragment).onMessagePriorityChanged(rowObject.getMessageId(), priorityCheckBox.isChecked());
                    }
                }
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final FragmentManager activityFragmentManager = activity.getSupportFragmentManager();
                final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                final MessageDetailsFragment detaislFragment = new MessageDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("Attachments", rowObject.getAttachments());
                bundle.putString(AxSysConstants.EXTRA_MESSAGE_ID, rowObject.getMessageId());
                bundle.putString(AxSysConstants.EXTRA_MESSAGE_PHOTO_URL, rowObject.getPhotoUrl());
                bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, activity.getResources().getString(R.string.title_massage_details));
                detaislFragment.setArguments(bundle);
                ft.replace(R.id.container_layout, detaislFragment, MessageDetailsFragment.class.getSimpleName());
                ft.addToBackStack(MessageDetailsFragment.class.getSimpleName());
                ft.commit();
            }
        });

        return convertView;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd-mm-yyyy hh:mm";
        String outputPattern = "mm-dd hh:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}