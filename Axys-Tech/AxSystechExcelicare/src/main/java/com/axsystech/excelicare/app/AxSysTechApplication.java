package com.axsystech.excelicare.app;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;

/**
 * This class will hold data and methods which are common to components in entire application.
 * Date: 06.05.14
 * Time: 13:24
 *
 * @author SomeswarReddy
 */
public class AxSysTechApplication extends MultiDexApplication {

    private static AxSysTechApplication mAppContext;

    private Toast mOldToast;
    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppContext = this;
        mHandler = new Handler();
    }

    public static AxSysTechApplication getAppContext() {
        return mAppContext;
    }

    /**
     * This method will return the password to be used for Database security.
     *
     * @return
     */
    public static String getPassword() {
        final String password = "Excelic@r5DbP@55w0rd";//TODO: here must be a real password
        byte[] data;
        try {
            data = password.getBytes(HTTP.UTF_8);
        } catch (UnsupportedEncodingException e) {
            data = password.getBytes();
            Log.e(getAppContext().getClass().getSimpleName(), "Unsupported character set");
        }

        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    /**
     * This method will display the toast messages as requested.
     *
     * @param message
     */
    public void showToast(final String message) {
        if (getMainLooper() == Looper.myLooper()) {
            if (null != mOldToast) {
                mOldToast.cancel();
            }
            mOldToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
            mOldToast.setGravity(Gravity.CENTER, 0, 0);
            mOldToast.show();
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    showToast(message);
                }
            });
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
