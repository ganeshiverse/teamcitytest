package com.axsystech.excelicare.framework.ui.fragments.appointments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.AddCircleMemberFragment;
import com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersListFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class ClinicianDetailsFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = ClinicianDetailsFragment.class.getSimpleName();
    private MainContentActivity mActivity;
    private ClinicianDetailsFragment mFragment;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private String mProviderSource = AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectView(R.id.isFavoriteCheckBox)
    private CheckBox isFavoriteCheckBox;

    @InjectView(R.id.appointmentUserImageView)
    private ImageView appointmentUserImageView;

    //@InjectView(R.id.clinicianNameTextView)
    private TextView clinicianNameTextView;

    //@InjectView(R.id.qualificationTextView)
    private TextView qualificationTextView;

    //@InjectView(R.id.experienceTextView)
    private TextView experienceTextView;

    //@InjectView(R.id.specializationTextView)
    private TextView specializationTextView;

    @InjectView(R.id.registerNumberTextView)
    private TextView registerNumberTextView;

    @InjectView(R.id.doctorRatingBar)
    private RatingBar doctorRatingBar;

    //@InjectView(R.id.likeTextView)
    private TextView likeTextView;
    //**************************************
    //@InjectView(R.id.backgroundInfoButton)
    private Button backgroundInfoButton;

    //@InjectView(R.id.clinicalDetailsButton)
    private Button clinicalDetailsButton;
    //**************************************
    @InjectView(R.id.backgroundInfoLayout)
    private ScrollView backgroundInfoLayout;

    //@InjectView(R.id.doctorMessageTextView)
    private TextView doctorMessageTextView;

    //@InjectView(R.id.clinicianBackgroundTextView)
    private TextView clinicianBackgroundTextView;

    @InjectView(R.id.clinicDetailsListView)
    private ListView clinicDetailsListView;


    private SearchDoctorsResponse.DoctorDetails mSelectedClinicianObject;
    private ClinicianDetailsResponse.ClinicianDetailsData mClinicianDetailsData;
    private ClinicDetailsAdapter mClinicDetailsAdapter;


    private TextView expTextView;
    private TextView regdTextView;
    private TextView doctorsmessagetextview;
    private TextView backgroundtextview;


    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.doctor_details, container, false);


        clinicianNameTextView = (TextView) view.findViewById(R.id.clinicianNameTextView);
        qualificationTextView = (TextView) view.findViewById(R.id.qualificationTextView);
        specializationTextView = (TextView) view.findViewById(R.id.specializationTextView);
        expTextView = (TextView) view.findViewById(R.id.expTextView);
        regdTextView = (TextView) view.findViewById(R.id.regdTextView);
        backgroundInfoButton = (Button) view.findViewById(R.id.backgroundInfoButton);
        clinicalDetailsButton = (Button) view.findViewById(R.id.clinicalDetailsButton);
        doctorsmessagetextview = (TextView) view.findViewById(R.id.doctorsmessagetextview);
        backgroundtextview = (TextView) view.findViewById(R.id.backgroundtextview);
        doctorMessageTextView = (TextView) view.findViewById(R.id.doctorMessageTextView);
        clinicianBackgroundTextView = (TextView) view.findViewById(R.id.clinicianBackgroundTextView);
        experienceTextView = (TextView) view.findViewById(R.id.experienceTextView);
        likeTextView = (TextView) view.findViewById(R.id.likeTextView);

        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface boldItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

        clinicianNameTextView.setTypeface(bold);
        qualificationTextView.setTypeface(regular);
        specializationTextView.setTypeface(regular);
        backgroundInfoButton.setTypeface(regular);
        clinicalDetailsButton.setTypeface(regular);
        doctorsmessagetextview.setTypeface(bold);
        backgroundtextview.setTypeface(bold);
        doctorMessageTextView.setTypeface(boldItalic);
        clinicianBackgroundTextView.setTypeface(regular);
        experienceTextView.setTypeface(bold);
        likeTextView.setTypeface(bold);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;

        readIntentArgs();
        addEventListeners();

        loadHeaderData();
        //Sravan commented the next 3 lines.
//        clinicalDetailsButton.setSelected(false);
//        backgroundInfoButton.setSelected(true);
//        loadTabsData();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT)) {
                mSelectedClinicianObject = (SearchDoctorsResponse.DoctorDetails) bundle.getSerializable(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_CLINICIAN_DETAILS)) {
                mClinicianDetailsData = (ClinicianDetailsResponse.ClinicianDetailsData) bundle.getSerializable(AxSysConstants.EXTRA_CLINICIAN_DETAILS);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_PROVIDER_SOURCE)) {
                mProviderSource = bundle.getString(AxSysConstants.EXTRA_PROVIDER_SOURCE, AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS);
            }
            //Sravan added if condition for the newly included key
            if (bundle.containsKey("showTabAt")) {
                int showTabAt = bundle.getInt("showTabAt");
                Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
                Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
                switch (showTabAt) {
                    case 0:

                        backgroundInfoButton.setSelected(true);
                        backgroundInfoButton.setTypeface(bold);
                        clinicalDetailsButton.setSelected(false);
                        clinicalDetailsButton.setTypeface(regular);

                        //backgroundInfoButton.setTextColor(getResources().getColor(R.color.textcolor22d2c3));
                        //clinicalDetailsButton.setTextColor(getResources().getColor(R.color.textcolor373d52));

                        loadTabsData();
                        break;
                    case 1:

                        backgroundInfoButton.setSelected(false);
                        backgroundInfoButton.setTypeface(regular);
                        clinicalDetailsButton.setSelected(true);
                        clinicalDetailsButton.setTypeface(bold);

                        //backgroundInfoButton.setTextColor(getResources().getColor(R.color.textcolor373d52));
                        //clinicalDetailsButton.setTextColor(getResources().getColor(R.color.textcolor22d2c3));

                        loadTabsData();
                        break;
                }
            }
        }

        if (mClinicianDetailsData != null && !TextUtils.isEmpty(mClinicianDetailsData.getClinicianName())) {
            mActivity.getSupportActionBar().setTitle(/*"Dr " +*/ mClinicianDetailsData.getClinicianName());
        }
    }

    private void addEventListeners() {
        // isFavoriteCheckBox.setOnClickListener(mFragment);
        backgroundInfoButton.setOnClickListener(mFragment);
        clinicalDetailsButton.setOnClickListener(mFragment);
        isFavoriteCheckBox.setVisibility(View.VISIBLE);
    }

    private void loadHeaderData() {
        if (mClinicianDetailsData != null) {
            // Update Clinician header details

            if (!TextUtils.isEmpty(mClinicianDetailsData.getIsFavorite())) {
                if (mClinicianDetailsData.getIsFavorite().trim().equalsIgnoreCase("True")) {
                    isFavoriteCheckBox.setChecked(true);
                    isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
                } else {
                    isFavoriteCheckBox.setChecked(false);
                    isFavoriteCheckBox.setButtonDrawable(R.drawable.message_bluestar);
                }
            } else if (mSelectedClinicianObject != null && !TextUtils.isEmpty(mSelectedClinicianObject.getIsFavorite())) {
                if (mSelectedClinicianObject.getIsFavorite().trim().equalsIgnoreCase("True")) {
                    isFavoriteCheckBox.setChecked(true);
                    isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
                } else {
                    isFavoriteCheckBox.setChecked(false);
                    isFavoriteCheckBox.setButtonDrawable(R.drawable.message_bluestar);
                }
            }
            isFavoriteCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!TextUtils.isEmpty(mClinicianDetailsData.getIsFavorite())) {
                        if (mClinicianDetailsData.getIsFavorite().trim().equalsIgnoreCase("True")) {
                            isFavoriteCheckBox.setChecked(true);
                            isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
                        } else {
                            isFavoriteCheckBox.setChecked(false);
                            isFavoriteCheckBox.setButtonDrawable(R.drawable.message_bluestar);
                        }
                    } else if (mSelectedClinicianObject != null && !TextUtils.isEmpty(mSelectedClinicianObject.getIsFavorite())) {
                        if (mSelectedClinicianObject.getIsFavorite().trim().equalsIgnoreCase("True")) {
                            isFavoriteCheckBox.setChecked(true);
                            isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
                        } else {
                            isFavoriteCheckBox.setChecked(false);
                            isFavoriteCheckBox.setButtonDrawable(R.drawable.message_bluestar);
                        }
                    }
                    if (mSelectedClinicianObject != null) {
                        SaveFavoriteProviderTask saveFavoriteProviderTask = new SaveFavoriteProviderTask(mSelectedClinicianObject.getClinicianId(),
                                isFavoriteCheckBox.isChecked(), mActiveUser.getToken(), new SaveFavoriteProviderTask.SaveFavoriteProviderListener() {
                            @Override
                            public void onFavoriteProviderSaved() {
                                mSelectedClinicianObject.setIsFavorite(isFavoriteCheckBox.isChecked() ? "False" : "True");
                            }

                            @Override
                            public void onFavoriteProviderFailed() {
                                showToast(R.string.error_save_favorite);
                            }
                        });

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            saveFavoriteProviderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            saveFavoriteProviderTask.execute();
                        }
                    }

                }
            });
          /*  isFavoriteCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectedClinicianObject != null) {
                        SaveFavoriteProviderTask saveFavoriteProviderTask = new SaveFavoriteProviderTask(mSelectedClinicianObject.getClinicianId(),
                                isFavoriteCheckBox.isChecked(), mActiveUser.getToken(), new SaveFavoriteProviderTask.SaveFavoriteProviderListener() {
                            @Override
                            public void onFavoriteProviderSaved() {
                                mSelectedClinicianObject.setIsFavorite(isFavoriteCheckBox.isChecked() ? "True" : "False");
                            }

                            @Override
                            public void onFavoriteProviderFailed() {
                                showToast(R.string.error_save_favorite);
                            }
                        });

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            saveFavoriteProviderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            saveFavoriteProviderTask.execute();
                        }
                    }
                }
            });
*/
            if (!TextUtils.isEmpty(mClinicianDetailsData.getPhoto())) {
                String imagePath = mClinicianDetailsData.getPhoto();
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                /*Picasso.with(mActivity)
                        .load(imagePath)
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .into(appointmentUserImageView);*/

                Picasso.with(getActivity()).load(imagePath).transform(new CircleTransform()).into(appointmentUserImageView);
            }

            clinicianNameTextView.setText(!TextUtils.isEmpty(mClinicianDetailsData.getClinicianName()) ? /*"Dr " +*/ mClinicianDetailsData.getClinicianName() : "");
            qualificationTextView.setText(!TextUtils.isEmpty(mClinicianDetailsData.getQualification()) ? mClinicianDetailsData.getQualification() : "");
            experienceTextView.setText(!TextUtils.isEmpty(mClinicianDetailsData.getExperience()) ? mClinicianDetailsData.getExperience() : "");
            specializationTextView.setText(!TextUtils.isEmpty(mClinicianDetailsData.getSpeciality()) ? mClinicianDetailsData.getSpeciality() : "");
            registerNumberTextView.setText("");
            if (!TextUtils.isEmpty(mClinicianDetailsData.getRating())) {
                float rating = Float.parseFloat(mClinicianDetailsData.getRating());
                doctorRatingBar.setRating(rating);
            } else {
                doctorRatingBar.setRating(0.0f);
            }
            if (!TextUtils.isEmpty(mClinicianDetailsData.getLikes())) {
                likeTextView.setText(mClinicianDetailsData.getLikes());
            } else if (mSelectedClinicianObject != null && !TextUtils.isEmpty(mSelectedClinicianObject.getLikes())) {
                likeTextView.setText(mSelectedClinicianObject.getLikes());
            }
        }
    }

    private void loadTabsData() {
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        if (backgroundInfoButton.isSelected()) {
            backgroundInfoButton.setTypeface(bold);
            clinicalDetailsButton.setTypeface(regular);
            backgroundInfoButton.setTextColor(getResources().getColor(R.color.sidemenu_selectaccount_color));
            clinicalDetailsButton.setTextColor(getResources().getColor(R.color.circles_text_color));
            backgroundInfoLayout.setVisibility(View.VISIBLE);
            clinicDetailsListView.setVisibility(View.GONE);
            doctorMessageTextView.setText(mClinicianDetailsData.getMessage());
            clinicianBackgroundTextView.setText(mClinicianDetailsData.getAdditionalInfo());

        } else if (clinicalDetailsButton.isSelected()) {
            backgroundInfoButton.setTypeface(regular);
            backgroundInfoButton.setTextColor(getResources().getColor(R.color.circles_text_color));
            clinicalDetailsButton.setTextColor(getResources().getColor(R.color.sidemenu_selectaccount_color));
            clinicalDetailsButton.setTypeface(bold);
            backgroundInfoLayout.setVisibility(View.GONE);
            clinicDetailsListView.setVisibility(View.VISIBLE);

            if (mClinicDetailsAdapter == null) {
                mClinicDetailsAdapter = new ClinicDetailsAdapter(mClinicianDetailsData.getClinicDetailsArrayList());
            } else {
                mClinicDetailsAdapter.updateData(mClinicianDetailsData.getClinicDetailsArrayList());
            }
            clinicDetailsListView.setAdapter(mClinicDetailsAdapter);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        if (!TextUtils.isEmpty(mProviderSource)) {
            if (mProviderSource.equalsIgnoreCase(AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS)) {
                inflater.inflate(R.menu.menu_empty, menu);
            } else if (mProviderSource.equalsIgnoreCase(AxSysConstants.PROVIDER_SOURCE_COMPOSE)) {
                inflater.inflate(R.menu.menu_clinician_details, menu);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            case R.id.actionSelect:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                int stackCount = fm.getBackStackEntryCount();
                for (int i = 0; i < stackCount; ++i) {
                    int backStackId = fm.getBackStackEntryAt(i).getId();
                    String backStackName = fm.getBackStackEntryAt(i).getName();

                    if (!TextUtils.isEmpty(backStackName)) {
                        if (!backStackName.equalsIgnoreCase(MessageComposeFragment.class.getSimpleName()) &&
                                !backStackName.equalsIgnoreCase(MessageDetailsFragment.class.getSimpleName()) &&
                                !backStackName.equalsIgnoreCase(AddCircleMemberFragment.class.getSimpleName()) &&
                                !backStackName.equalsIgnoreCase(CircleMembersListFragment.class.getSimpleName())) {

                            fm.popBackStack(backStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        } else if (backStackName.equalsIgnoreCase(MessageComposeFragment.class.getSimpleName())) {

                            Fragment fragment = fm.findFragmentByTag(MessageComposeFragment.class.getSimpleName());
                            if (fragment != null && fragment instanceof MessageComposeFragment) {
                                MessageComposeFragment messageComposeFragment = (MessageComposeFragment) fragment;
                                messageComposeFragment.onProviderSelected(mSelectedClinicianObject, mClinicianDetailsData);
                            }
                        } else if (backStackName.equalsIgnoreCase(AddCircleMemberFragment.class.getSimpleName())) {

                            Fragment fragment = fm.findFragmentByTag(AddCircleMemberFragment.class.getSimpleName());
                            if (fragment != null && fragment instanceof AddCircleMemberFragment) {
                                AddCircleMemberFragment addCircleMemberFragment = (AddCircleMemberFragment) fragment;
                                addCircleMemberFragment.onProviderSelected(mSelectedClinicianObject, mClinicianDetailsData);
                            }
                        }
                    }
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.isFavoriteCheckBox:
                if (isFavoriteCheckBox.isChecked()) {
                    isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
                }

                break;

            case R.id.backgroundInfoButton:
                backgroundInfoButton.setSelected(true);
                clinicalDetailsButton.setSelected(false);

                loadTabsData();
                break;

            case R.id.clinicalDetailsButton:
                backgroundInfoButton.setSelected(false);
                clinicalDetailsButton.setSelected(true);

                loadTabsData();
                break;

            default:
                break;
        }
    }

    private class ClinicDetailsAdapter extends BaseAdapter {

        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");


        private ArrayList<ClinicianDetailsResponse.ClinicDetails> clinicDetailsArrayList;

        public ClinicDetailsAdapter(ArrayList<ClinicianDetailsResponse.ClinicDetails> clinicDetailsArrayList) {
            this.clinicDetailsArrayList = clinicDetailsArrayList;
        }

        public void updateData(ArrayList<ClinicianDetailsResponse.ClinicDetails> clinicDetailsArrayList) {
            this.clinicDetailsArrayList = clinicDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (clinicDetailsArrayList != null && clinicDetailsArrayList.size() > 0) ? clinicDetailsArrayList.size() : 0;
        }

        @Override
        public ClinicianDetailsResponse.ClinicDetails getItem(int position) {
            return clinicDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ClinicianDetailsResponse.ClinicDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.clinic_details_list_row, parent, false);
            }

            if (position % 2 == 0) {//EAFBFA
                convertView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#EAFBFA"));
            }

            final TextView clinicNameTextView = (TextView) convertView.findViewById(R.id.clinicNameTextView);
            clinicNameTextView.setTypeface(bold);

            TextView detailsTextView = (TextView) convertView.findViewById(R.id.detailsTextView);
            detailsTextView.setTypeface(bold);
            TextView feeTextView = (TextView) convertView.findViewById(R.id.feeTextView);
            feeTextView.setTypeface(bold);


            TextView feeValueTextView = (TextView) convertView.findViewById(R.id.feeValueTextView);
            feeValueTextView.setTypeface(bold);

            TextView timeSlotsTextView = (TextView) convertView.findViewById(R.id.timeSlotsTextView);
            timeSlotsTextView.setTypeface(regular);


            TextView locationDetailsTextView = (TextView) convertView.findViewById(R.id.locationDetailsTextView);
            locationDetailsTextView.setTypeface(regular);


            TextView contactDetailsTextView = (TextView) convertView.findViewById(R.id.contactDetailsTextView);
            contactDetailsTextView.setTypeface(regular);


            TextView emailDetailsTextView = (TextView) convertView.findViewById(R.id.emailDetailsTextView);
            emailDetailsTextView.setTypeface(regular);


            TextView websiteDetailsTextView = (TextView) convertView.findViewById(R.id.websiteDetailsTextView);
            websiteDetailsTextView.setTypeface(regular);

            Button bookAppointmentButton = (Button) convertView.findViewById(R.id.bookAppointmentButton);

            clinicNameTextView.setText(rowObject.getName());

            feeValueTextView.setText(rowObject.getFee());

            if (rowObject.getTimingsArrayList() != null && rowObject.getTimingsArrayList().size() > 0) {
                StringBuffer strBuffer = new StringBuffer();
                for (int idx = 0; idx < rowObject.getTimingsArrayList().size(); idx++) {
                    ClinicianDetailsResponse.Timings timeSlot = rowObject.getTimingsArrayList().get(idx);

                    if (!TextUtils.isEmpty(timeSlot.getTimingSlots())) {
                        strBuffer.append(timeSlot.getTimingSlots());
                    }

                    if (idx < rowObject.getTimingsArrayList().size() - 1) {
                        strBuffer.append(", ");
                    }
                }
                timeSlotsTextView.setText(strBuffer.toString());
            }

            locationDetailsTextView.setText(rowObject.getAddress1() +
                    (!TextUtils.isEmpty(rowObject.getAddress2()) ? ", " + rowObject.getAddress2() : "") +
                    (!TextUtils.isEmpty(rowObject.getAddress3()) ? ", " + rowObject.getAddress3() : "") +
                    (!TextUtils.isEmpty(rowObject.getState()) ? ", " + rowObject.getState() : "") +
                    (!TextUtils.isEmpty(rowObject.getPostcode()) ? ", " + rowObject.getPostcode() : ""));

            contactDetailsTextView.setText("Contact: " + (!TextUtils.isEmpty(rowObject.getPhone()) ? rowObject.getPhone() : ""));

            emailDetailsTextView.setText("Email: " + (!TextUtils.isEmpty(rowObject.getEmail()) ? rowObject.getEmail() : ""));

            websiteDetailsTextView.setText(!TextUtils.isEmpty(rowObject.getURL()) ? rowObject.getURL() : "");

            if (!TextUtils.isEmpty(mProviderSource)) {
                if (mProviderSource.equalsIgnoreCase(AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS)) {
                    bookAppointmentButton.setVisibility(View.VISIBLE);
                } else if (mProviderSource.equalsIgnoreCase(AxSysConstants.PROVIDER_SOURCE_COMPOSE)) {
                    bookAppointmentButton.setVisibility(View.GONE);
                }
            }

            bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                    FragmentTransaction ft1 = fragmentManager.beginTransaction();

                    BookAppointmentFragment bookAppointmentFragment = new BookAppointmentFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                    bundle1.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Appointment Data & Time");
                    bundle1.putString(AxSysConstants.EXTRA_CLINIC_TITLE, clinicNameTextView.getText().toString());
                    bundle1.putSerializable(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT, mSelectedClinicianObject);
                    bundle1.putSerializable(AxSysConstants.EXTRA_CLINICIAN_DETAILS, mClinicianDetailsData);
                    bundle1.putInt(AxSysConstants.EXTRA_SELECTED_CLINIC_POSITION, position);
                    bookAppointmentFragment.setArguments(bundle1);
                    ft1.add(R.id.container_layout, bookAppointmentFragment);
                    ft1.addToBackStack(BookAppointmentFragment.class.getSimpleName());
                    ft1.commit();
                }
            });

            return convertView;
        }
    }
}
