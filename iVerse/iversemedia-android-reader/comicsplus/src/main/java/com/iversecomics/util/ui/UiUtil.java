package com.iversecomics.util.ui;

import android.app.Activity;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class UiUtil {

    @SuppressWarnings("deprecation")
    public static boolean isLandscapeOrientation(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        if (display.getWidth() < display.getHeight()) {
            return false;
        } else {
            return true;
        }
    }

    @SuppressWarnings("deprecation")
    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        return display.getWidth();
    }

    @SuppressWarnings("deprecation")
    public static int getScreenHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        return display.getHeight();
    }

    public static void applyButtonEffect(final View view) {
        final ColorFilter filter = new LightingColorFilter(0xA0A0A0A0, 0);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Drawable background = v.getBackground();
                    if (background != null) {
                        background.setColorFilter(filter);
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    Drawable background = v.getBackground();
                    if (background != null) {
                        background.setColorFilter(null);
                    }
                }
                v.onTouchEvent(event);
                return true;
            }
        });
    }

    public static void applyImageButtonEffect(final ImageView view) {
        final ColorFilter filter = new LightingColorFilter(0xA0A0A0A0, 0);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setColorFilter(filter);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    view.setColorFilter(null);
                }
                return false;
            }
        });
    }
}
