package com.babylon.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.model.payments.Order;

import java.util.List;
import java.util.Locale;

public class OrderAdapter extends BaseAdapter {
    private final List<Order> orders;
    private final Context context;
    private TextWatcher textWatcher;

    public OrderAdapter(Context context, List<Order> orders, TextWatcher textWatcher) {
        this.context = context;
        this.orders = orders;
        this.textWatcher = textWatcher;

    }

    private class ViewHolder {
        TextView title;
        TextView description;
        TextView price;
        EditText qty;
        View itemDivider;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Order getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();

            convertView = View.inflate(context, R.layout.list_item_order, null);

            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.description);
            viewHolder.price = (TextView) convertView.findViewById(R.id.price);
            viewHolder.qty = (EditText) convertView.findViewById(R.id.qty);
            viewHolder.itemDivider = (View) convertView.findViewById(R.id.itemDivider);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Order order = getItem(position);

        viewHolder.title.setText(order.getTitle());

        if (TextUtils.isEmpty(order.getDescription())) {
            viewHolder.description.setVisibility(View.GONE);
        } else {
            viewHolder.description.setVisibility(View.VISIBLE);
            viewHolder.description.setText(String.valueOf(order.getDescription()));
        }

        viewHolder.price.setText(App.getInstance().getCurrentRegion().getFormattedPriceString(order.getPrice()));

        viewHolder.qty.setTag(R.id.position, position);
        viewHolder.qty.removeTextChangedListener(textWatcher);
        if (order.getQty() != null) {
            viewHolder.qty.setText(String.valueOf(order.getQty()));
        } else {
            viewHolder.qty.setText("");
        }
        viewHolder.qty.addTextChangedListener(textWatcher);

        if (position + 1 == getCount()) {
            viewHolder.itemDivider.setVisibility(View.GONE);
        } else {
            viewHolder.itemDivider.setVisibility(View.VISIBLE);
        }
        //validationController.addValidation(viewHolder.qty, validator);


        return convertView;
    }
}
