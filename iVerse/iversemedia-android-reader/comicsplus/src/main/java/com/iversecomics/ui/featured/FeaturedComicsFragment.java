package com.iversecomics.ui.featured;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.store.CoverSize;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.store.AppStoreAdapterLocator;
import com.iversecomics.store.ComicBuyer;
import com.iversecomics.ui.ComicListFragment;
import com.iversecomics.ui.PublishersFragment;
import com.iversecomics.ui.main.MainActivity;
import com.iversecomics.ui.widget.ComicSlot;
import com.iversecomics.ui.widget.NavigationBar;
import com.iversecomics.util.Util;
import com.iversecomics.util.ui.UiUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.squareup.otto.Bus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FeaturedComicsFragment extends Fragment implements AbstractFeaturedContentAdapter.Listener {

    public static final String TAG = FeaturedComicsFragment.class.getSimpleName();
    public static final String ARG_ON_DEMAND = "com.iverse.ON_DEMAND";
    static final int NUM_SLOTS = 6;
    ComicSlot listViewSlots[] = new ComicSlot[NUM_SLOTS];
    static final int NUM_OFFERS = 3;
    ComicSlot listViewOffers[] = new ComicSlot[NUM_OFFERS];
    static final int listSlotIds[] = new int[]{R.id.slot0, R.id.slot1, R.id.slot2, R.id.slot3, R.id.slot4, R.id.slot5};
    static final int listOfferIds[] = new int[]{R.id.offer0, R.id.offer1, R.id.offer2};
    private final static Logger LOG = LoggerFactory.getLogger(FeaturedComicsFragment.class);

    private ComicBuyer mComicBuyer;
    private AbstractAppStoreAdapter appStoreAdapter;
    private NavigationBar navBar;
    private static FeaturedSlot.SlotPositionType[] TYPE = {
            FeaturedSlot.SlotPositionType.FeaturedSlotLeftTop
            , FeaturedSlot.SlotPositionType.FeaturedSlotLeftMiddle
            , FeaturedSlot.SlotPositionType.FeaturedSlotLeftBottom
            , FeaturedSlot.SlotPositionType.FeaturedSlotRightTop
            , FeaturedSlot.SlotPositionType.FeaturedSlotRightMiddle
            , FeaturedSlot.SlotPositionType.FeaturedSlotRightBottom
            , FeaturedSlot.SlotPositionType.FeaturedSlotBottomLeft
            , FeaturedSlot.SlotPositionType.FeaturedSlotBottomMiddle
            , FeaturedSlot.SlotPositionType.FeaturedSlotBottomRight
    };

    ViewGroup rootView;
    ViewGroup featureFrame;
    ViewPager featurePager;
    View processingOverlay;
    LinearLayout mainView;
    View offlineMessageOverlay;
    AbstractFeaturedContentAdapter featureContent;
    Bus bus = OttoBusProvider.getInstance();

    private DateFormat mDateFormat = SimpleDateFormat.getDateTimeInstance();

    /**
     * Is this the on demand version of the featured page?  The on demand version looks like this:
     * <a href="https://s3.amazonaws.com/uploads.hipchat.com/36372/253810/ruqh2fnj6j33gmf/upload.png" />.
     * It's triggered by the "Unlimited!" link in the side menu.
     *
     * @param onDemand
     */
    public static final FeaturedComicsFragment newInstance(boolean onDemand) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_ON_DEMAND, onDemand);
        FeaturedComicsFragment frag = new FeaturedComicsFragment();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        boolean flagOnDemand = false;
        if (args != null) {
            flagOnDemand = args.getBoolean(ARG_ON_DEMAND, false);
        }
        featureContent = flagOnDemand ? new OnDemandFeaturedContentAdapter() : new FeaturedContentAdapter();
        featureContent.setListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_featured_comics, null, false);

        processingOverlay = rootView.findViewById(R.id.processing_overlay);
        mainView = (LinearLayout)(rootView.findViewById(R.id.container_view));
        offlineMessageOverlay = rootView.findViewById(R.id.offline_message_overlay);

        navBar = (NavigationBar) rootView.findViewById(R.id.navbar);
        navBar.setNavigationListener((NavigationBar.NavigationListener) getActivity());
        navBar.setBackButtonVisibility(View.GONE);

        initSlots();
        if (getActivity() instanceof AppStoreAdapterLocator) {
            appStoreAdapter = ((AppStoreAdapterLocator) getActivity()).getAppStoreAdapter();
        } else {
            throw new IllegalArgumentException("Invalid activity. Must implement AppStoreAdapterLocator.");
        }
        mComicBuyer = new ComicBuyer(getActivity(), appStoreAdapter);

        return rootView;
    }

    //changes for Offline support.
    public void initSlots() {
        if(!ConnectionHelper.isConnectedToInternet(getActivity())) {
            mainView.setVisibility(View.INVISIBLE);
            offlineMessageOverlay.setVisibility(View.INVISIBLE);
            navBar.setMenuButtonVisibility(View.INVISIBLE);
        }
        featurePager = (ViewPager) (rootView.findViewById(R.id.feature));
        featureFrame = (ViewGroup) (rootView.findViewById(R.id.feature_frame));

        for (int i = 0; i < NUM_OFFERS; i++) {
            listViewOffers[i] = (ComicSlot) (rootView.findViewById(listOfferIds[i]));
            // set click listeners for navigating to list view for the given FeaturedSlot type
            final int slotIndex = NUM_SLOTS + i;
            listViewOffers[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FeaturedSlot slot = getSlotByPosition(slotIndex);
                    showComicListForSlot(slot);
                }
            });
        }

        for (int i = 0; i < NUM_SLOTS; i++) {
            listViewSlots[i] = (ComicSlot) (rootView.findViewById(listSlotIds[i]));
            // set click listeners for navigating to list view for the given FeaturedSlot type
            final int slotIndex = i;
            listViewSlots[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FeaturedSlot slot = getSlotByPosition(slotIndex);
                    showComicListForSlot(slot);
                }
            });
        }
    }

    private void refreshSlots() {
        if (featurePager != null) {
            hideSlots();
            ImageLoader imageLoader = ImageLoader.getInstance();

            for (int i = 0; i < NUM_OFFERS; i++) {
                FeaturedSlot slot = getSlotByPosition(NUM_SLOTS + i);
                if (slot != null) {
                    imageLoader.displayImage(slot.getImageUrl2x(), listViewOffers[i].getImageView(), new FeaturedImageListener(listViewOffers[i]));
                }
            }
            for (int i = 0; i < NUM_SLOTS; i++) {
                FeaturedSlot slot = getSlotByPosition(i);
                if (slot != null) {
                    imageLoader.displayImage(slot.getImageUrl2x(), listViewSlots[i].getImageView(), new FeaturedImageListener(listViewSlots[i]));
                }
            }
        }
    }

    private void showComicListForSlot(FeaturedSlot slot) {
        if (slot == null) {
            LOG.warn("null slot passed to showComicListForSlot");
            return;
        }

        Fragment frag = null;

        switch (slot.getSlotType()) {
            case FeaturedSlotTypePromotion:
                frag = ComicListFragment.newInstanceForPromotionId(slot.getPromotionID(), slot.getTitle());
                break;
            case FeaturedSlotTypePublisher:
                frag = ComicListFragment.newInstanceForSupplierId(slot.getSupplierID(), slot.getTitle());
                break;
            case FeaturedSlotTypeGroup:
                if (slot.getGroupID() != 0) {
                    frag = ComicListFragment.newInstanceForGroupId(slot.getGroupID(), slot.getTitle());
                } else if (slot.getSupplierID() != 0) {
                    frag = ComicListFragment.newInstanceForSupplierId(slot.getSupplierID(), slot.getTitle());
                }
                break;
            case FeaturedSlotTypeJustAdded:
                frag = ComicListFragment.newInstanceNewReleases();
                break;
            case FeaturedSlotTypeTopFree:
                frag = ComicListFragment.newInstanceTopFree();
                break;
            case FeaturedSlotTypeTopPaid:
                frag = ComicListFragment.newInstanceTopPaid();
                break;
            case FeaturedSlotTypeURL:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slot.getTargetURL()));
                startActivity(browserIntent);
                break;
            case FeaturedSlotTypePublisherList:
                frag = PublishersFragment.newInstance();
                break;
            case FeaturedSlotTypeOnDemand:
                //((MainActivity) getActivity()).onDemandClick();
                ServerConfig serverConfig = ServerConfig.getDefault();
                    if (serverConfig.isSubscribed()) {
                        // validate that the subscription end date has not passed
                        Date current = new Date();
                        if (serverConfig.getSubscriptionEndDate().after(current)) {
                            // show my alert
                            Date endDate = serverConfig.getSubscriptionEndDate();
                            String endDateString = "Unknown";
                            if (endDate != null)
                                endDateString = mDateFormat.format(endDate);
                            showsubScriptionDetailsDialog(getActivity().getString(R.string.subscription_activation_titile), getResources().getString(R.string.subscription_current) + endDateString + getResources().getString(R.string.subscription_cancel), false);
                        } else {
                            // subscription may have expired or been canceled, we need to check the
                            // subscription status against the app store and then update the iverse server
                            // with the new subscription information.
                            showsubScriptionDetailsDialog(getActivity().getString(R.string.subscription_expired_title), getActivity().getString(R.string.re_subscribe), true);
                        }
                    } else {
                        mComicBuyer.purchaseSubscription();
                    }
                    //TODO Will implent this functionality in future
//                frag = SubscriptionFragment.newInstance();
                break;
            // TODO: FeaturedSLotTypeOnDemandSolicitation
        }
        if (frag != null)
            ((MainActivity) getActivity()).showTopFragment(frag);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Save Offline change
        if(ConnectionHelper.isConnectedToInternet(getActivity()))
            showLoadingDialog();
        hideSlots();
        bus.register(featureContent);
    }

    @Override
    public void onPause() {
        bus.unregister(featureContent);
        super.onPause();
    }

    private void hideSlots() {
        featureFrame.setVisibility(View.INVISIBLE);
        for (int i = 0; i < NUM_SLOTS; i++) {
            listViewSlots[i].setVisibility(View.INVISIBLE);
        }
        for (int i = 0; i < NUM_OFFERS; i++) {
            listViewOffers[i].setVisibility(View.INVISIBLE);
        }
    }

    private void animateSlot(View slot, boolean flagFeature) {

        int durationTranslate = (int) (300 + 200 * Util.getRandom());
        int durationRotate1 = durationTranslate + 300;
        int durationRotate2 = 50;

        TranslateAnimation translation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, -(2.3f + 1.7f * Util.getRandom())
                , TranslateAnimation.RELATIVE_TO_SELF, 0.0f);
        translation.setDuration(durationTranslate);
        translation.setRepeatCount(0);
        translation.setFillAfter(true);

        if (flagFeature) {
            translation.setDuration(durationTranslate * 2);
            slot.startAnimation(translation);
        } else {
            float degree = 10 * (Util.getRandom() + 0.2f);
            float rx = 1.0f;
            if ((int) (Util.getRandom() * 10000) % 2 == 0) {
                degree = -degree;
                rx = 0.0f;
            }
            RotateAnimation rotation1 = new RotateAnimation(
                    degree, degree
                    , RotateAnimation.RELATIVE_TO_SELF, rx
                    , RotateAnimation.RELATIVE_TO_SELF, 1.0f);
            rotation1.setDuration(durationRotate1);
            rotation1.setRepeatCount(0);
            rotation1.setFillAfter(true);
            RotateAnimation rotation2 = new RotateAnimation(
                    0, -degree
                    , RotateAnimation.RELATIVE_TO_SELF, rx
                    , RotateAnimation.RELATIVE_TO_SELF, 1.0f);
            rotation2.setDuration(durationRotate2);
            rotation2.setStartOffset(durationRotate1);
            rotation2.setFillAfter(true);

            AnimationSet set = new AnimationSet(true);
            set.addAnimation(rotation1);
            set.addAnimation(rotation2);
            set.addAnimation(translation);

            set.setDuration(durationRotate1 + durationRotate2);
            set.setFillAfter(true);

            slot.startAnimation(set);
            slot.setVisibility(View.VISIBLE);
        }
    }

    public void showLoadingDialog() {
        processingOverlay.setVisibility(View.VISIBLE);
    }

    private void hideLoadingDialog() {
        processingOverlay.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSlotsUpdated(List<FeaturedSlot> slots) {
        refreshSlots();
    }

    @Override
    public void onFeaturedUpdated(List<Comic> comics) {
        featurePager.setAdapter(new ComicPagerAdapter(comics));
    }

    public FeaturedSlot getSlotByPosition(int pos) {
        FeaturedSlot.SlotPositionType type = TYPE[pos];
        for (FeaturedSlot slot : featureContent.getSlots()) {
            if (slot.getSlot() == type) {
                return slot;
            }
        }
        return null;
    }

    class ComicPagerAdapter extends PagerAdapter {

        private final List<Comic> comics;

        ComicPagerAdapter(List<Comic> comics) {
            this.comics = comics;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            final View view = inflater.inflate(
                    R.layout.fragment_comic_cover, container, false);

            ImageView cover = (ImageView) view.findViewById(R.id.cover);
            ImageView ondeamand = (ImageView) view.findViewById(R.id.ondeamand);
            Comic comic = comics.get(position);
            view.setTag(comic);
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).onComicClicked((Comic) v.getTag());
                }
            });
            if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                int onDemandVisibility = comic.getSubscription() ? View.VISIBLE : View.GONE;
                ondeamand.setVisibility(onDemandVisibility);
            }
            int height = UiUtil.getScreenHeight(getActivity());
            CoverSize coverSize = CoverSize.bestFit(height / 2);
            Uri coverImageUri = coverSize.getServerUri(comic.getImageFileName());

            ImageLoader.getInstance().displayImage(coverImageUri.toString(), cover, new MainImageListener());

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            int size = comics.size();
            Log.i(TAG, "featureContent size: " + size);
            return size;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private class MainImageListener implements ImageLoadingListener {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                animate();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                animate();
            }

            private void animate() {
                if (featureFrame.getVisibility() == View.INVISIBLE) {
                    hideLoadingDialog();
                    animateSlot(featureFrame, true);
                    featureFrame.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }
    }

    private class FeaturedImageListener implements ImageLoadingListener {
        View animatingView;

        private FeaturedImageListener(View animatingView) {
            this.animatingView = animatingView;
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {

        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            showSlot();
        }

        private void showSlot() {
            hideLoadingDialog();
            animateSlot(animatingView, false);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            showSlot();
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }
    }
    /**
     * Displays a subscription expired dialog and gives a chance to user to buy a new subscription.
     * @param title - title of the dialog
     * @param message - message to be displayed
     * @param buyNewSubscription - yes/no
     */
    private void showsubScriptionDetailsDialog(String title, String message, final boolean buyNewSubscription) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(buyNewSubscription){
                            mComicBuyer.purchaseSubscription();
                        }
                    }
                });
        if(buyNewSubscription) {
            builder.setNegativeButton(com.iversecomics.archie.android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                }
            });
        }
        builder.create().show();

    }

}
