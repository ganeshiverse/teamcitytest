package com.babylon.api.request.base;

public interface RequestListener<O> {
    public void onRequestComplete(Response<O> response);
}