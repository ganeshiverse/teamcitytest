package com.babylon.api.request.payments;

import android.text.TextUtils;

import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.RubyResponseProcessor;
import com.babylon.api.request.base.Response;
import com.babylon.model.payments.PaymentError;
import com.babylon.utils.GsonWrapper;
import com.babylon.utils.L;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Response processor for handle and parse facebook alliants requests
 * Such request not follow standard of alliants response structure, especially errors
 */
public class PaymentResponseProcessor<O> extends RubyResponseProcessor<O> {

    public static final String TAG = PaymentResponseProcessor.class.getSimpleName();

    public PaymentResponseProcessor(Type outputType) {
        super(outputType);
    }

    @Override
    protected void addParsedErrors(Response<O> result, String jsonErrors) {
        try {
            PaymentError paymentError = GsonWrapper.getGson().fromJson(jsonErrors, PaymentError.class);
            if (paymentError != null) {
                String userError = paymentError.getUserMessage();
                if (!TextUtils.isEmpty(userError)) {
                    addError(result, userError);
                } else if (paymentError.getError() != null
                        && paymentError.getError().getBaseErrors() != null
                        && !paymentError.getError().getBaseErrors().isEmpty()) {
                    List<String> errors = paymentError.getError().getBaseErrors();
                    for (String error : errors) {
                        if (!TextUtils.isEmpty(error)) {
                            addError(result, error);
                        }
                    }
                } else {
                    addError(result, App.getInstance().getString(R.string.some_error));
                }
            }
        } catch (JsonSyntaxException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }
}
