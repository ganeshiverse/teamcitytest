package com.iversecomics.client.comic.viewer.error;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.iversecomics.archie.android.R;
import com.iversecomics.io.IOUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ErrorReporter {
    private static final String TAG = "ErrorReporter";

    public static void reportException(final Context context, final Throwable t) {
        Log.w(TAG, "Reporing error", t);

        AlertDialog.Builder dlg = new AlertDialog.Builder(context);
        dlg.setTitle(R.string.error_title);
        dlg.setMessage(R.string.error_occured);
        dlg.setCancelable(false);
        dlg.setPositiveButton(R.string.error_report_email, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                String[] mailto = new String[]{"iversecomics@gmail.com"};
                String subject = "[iverse-debug] emailed trace";

                StringBuffer body = new StringBuffer();

                body.append("Feel free to review this email to be sure that there is no personal information present.");

                body.append("Auto generated email body from app\n");
                try {
                    PackageManager pm = context.getPackageManager();
                    PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
                    body.append(info.packageName).append(" v").append(info.versionName);
                    body.append(" (").append(info.versionCode).append(")\n");
                } catch (NameNotFoundException e) {
                    body.append(context.getPackageName()).append(" (v)\n");
                }

                body.append("\n\n");
                body.append("\nBelow here lies cryptic symbols and chants for the developer of this application.");
                body.append("\nYou are not expected to understand what this means, however, "
                        + "you do have the right to see it.");
                body.append("\nIt is safe to ignore this section, but please do not delete it, as it is of "
                        + "value in diagnosing the error condition that you encountered.");
                body.append(".\\ Trace \\.____________________________________________\n");
                body.append("\n\nTrace\n");
                body.append(asString(t));

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, mailto);
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, body.toString());
                email.setType("text/plain");

                context.startActivity(Intent.createChooser(email, "IverseSendEmail"));
                if (Activity.class.isAssignableFrom(context.getClass())) {
                    ((Activity) context).finish();
                }
            }
        });
        dlg.setNegativeButton(R.string.error_close_app, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (Activity.class.isAssignableFrom(context.getClass())) {
                    ((Activity) context).finish();
                }
            }
        });
        dlg.show();
    }

    private static String asString(Throwable t) {
        StringWriter writer = null;
        try {
            writer = new StringWriter();

            t.printStackTrace(new PrintWriter(writer));
            return writer.toString();
        } finally {
            IOUtil.close(writer);
        }
    }
}
