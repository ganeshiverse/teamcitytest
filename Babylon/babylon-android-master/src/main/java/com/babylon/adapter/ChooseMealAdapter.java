package com.babylon.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.model.Nutrient;

import java.util.List;

public class ChooseMealAdapter extends BaseAdapter {

    private final List<Nutrient> foodList;
    private final Context context;

    public ChooseMealAdapter(Context context, List<Nutrient> foodList) {
        this.context = context;
        this.foodList = foodList;
    }

    @Override
    public int getCount() {
        return foodList.size();
    }

    @Override
    public Nutrient getItem(int position) {
        return foodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.list_item_choose_meal, null);

            viewHolder.mealTitleTextView = (TextView) convertView.findViewById(R.id.list_item_meal_title_text_view);
            viewHolder.mealCaloriesTextView = (TextView) convertView.findViewById(R.id.list_item_meal_calories_text_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Nutrient nutrient = getItem(position);
        showNutrientData(viewHolder, nutrient);

        return convertView;
    }

    private void showNutrientData(ViewHolder viewHolder, Nutrient food) {
        if (food.getName() != null) {
            viewHolder.mealTitleTextView.setText(food.getName());
        }

        if (food.getKilocalories() != null) {
            final String kcal = String.valueOf(food.getKilocalories());
            final String subTitle = String.format(App.getInstance().getString(R.string.kcalories_pattern), kcal);

            viewHolder.mealCaloriesTextView.setText(subTitle);
        }
    }

    private static class ViewHolder {
        TextView mealTitleTextView;
        TextView mealCaloriesTextView;
    }
}
