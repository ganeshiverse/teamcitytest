package com.babylon.utils.samsung.health;

import android.database.Cursor;
import android.os.AsyncTask;

import com.babylon.database.DatabaseOperationUtils;
import com.babylon.enums.MonitorMeParam;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataResolver;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class WeightProcessor extends AsyncTask<HealthDataResolver.ReadResult, Void, Void> {
    private static String TAG = WeightProcessor.class.getSimpleName();

    @Override
    protected Void doInBackground(HealthDataResolver.ReadResult... params) {
        Cursor cursor = null;

        try {
            cursor = params[0].getResultCursor();
            if (cursor != null && cursor.moveToFirst()) {

                List<MonitorMeFeatures.Feature> features = new LinkedList<>();

                do {
                    MonitorMeFeatures.Feature feature = new MonitorMeFeatures.Feature();
                    feature.setId(MonitorMeParam.WEIGHT_ID.getId());
                    feature.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);

                    float weight = cursor.getFloat(cursor.getColumnIndex(
                            HealthConstants.Weight.WEIGHT));
                    long startTime = cursor.getLong(cursor.getColumnIndex(
                            HealthConstants.Weight.START_TIME));

                    L.d(TAG, "weight = " + weight);
                    L.d(TAG, "startTime = " + DateUtils.longToString(startTime));

                    feature.setValue(String.valueOf(weight));
                    feature.setDate(new Date(startTime));

                    features.add(feature);

                } while (cursor.moveToNext());

                DatabaseOperationUtils.getInstance().saveMonitorMeFeatures(features);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        SyncAdapter.performSyncAndPullReport(
                Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);
    }
}
