package com.iversecomics.client.reader;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.MediumTest;

public class DrawUtilTest extends AndroidTestCase {
    private void assertDelta(String msg, float actualDelta, float expectedDelta) {
        assertEquals(msg, expectedDelta, actualDelta, 0.0001f);
    }

    @MediumTest
    public void testDrawingLargerAtEdgeViewA() {
        float gapA = 0f;
        float gapB = -10f;
        float drawSize = 30f;
        float viewSize = 20f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta nas nothing to do", delta, 0f);
    }

    @MediumTest
    public void testDrawingLargerAtEdgeViewB() {
        float gapA = -10f;
        float gapB = 0f;
        float drawSize = 30f;
        float viewSize = 20f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta has nothing to do", delta, 0f);
    }

    @MediumTest
    public void testDrawingLargerInsideEdgeViewA() {
        float gapA = 5f;
        float gapB = -15f;
        float drawSize = 30f;
        float viewSize = 20f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta has nothing to do", delta, -5f);
    }

    @MediumTest
    public void testDrawingLargerInsideEdgeViewB() {
        float gapA = -10f;
        float gapB = 5f;
        float drawSize = 30f;
        float viewSize = 20f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta has nothing to do", delta, 5f);
    }

    @MediumTest
    public void testDrawingSameCenteredAlready() {
        float gapA = 0f;
        float gapB = 0f;
        float drawSize = 30f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta should do nothing (already centered)", delta, -0f);
    }

    @MediumTest
    public void testDrawingSameOffEdgeViewA() {
        float gapA = -10f;
        float gapB = 10f;
        float drawSize = 30f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta wants to center", delta, 10f);
    }

    @MediumTest
    public void testDrawingSameOffEdgeViewB() {
        float gapA = 10f;
        float gapB = -10f;
        float drawSize = 30f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta wants to center", delta, -10f);
    }

    @MediumTest
    public void testDrawingSmallerAtEdgeViewA() {
        float gapA = 0f;
        float gapB = 10f;
        float drawSize = 20f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta wants to center", delta, 5f);
    }

    @MediumTest
    public void testDrawingSmallerAtEdgeViewB() {
        float gapA = 10f;
        float gapB = 0f;
        float drawSize = 20f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta wants to center", delta, -5f);
    }

    @MediumTest
    public void testDrawingSmallerCenteredAlready() {
        float gapA = 5f;
        float gapB = 5f;
        float drawSize = 20f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta should do nothing (already centered)", delta, 0f);
    }

    @MediumTest
    public void testDrawingSmallerOffEdgeViewA() {
        float gapA = -5f;
        float gapB = 15f;
        float drawSize = 20f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta wants to center", delta, 10f);
    }

    @MediumTest
    public void testDrawingSmallerOffEdgeViewB() {
        float gapA = 15f;
        float gapB = -5f;
        float drawSize = 20f;
        float viewSize = 30f;
        float delta = DrawUtil.reduceGap(gapA, gapB, drawSize, viewSize);
        assertDelta("delta wants to center", delta, -10f);
    }
}
