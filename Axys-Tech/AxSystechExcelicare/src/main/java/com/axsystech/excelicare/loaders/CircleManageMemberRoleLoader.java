package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/9/2015.
 */
public class CircleManageMemberRoleLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String userCircleId;
    private String status;
    private String userCircleMemberId;


    public CircleManageMemberRoleLoader(Context context, String token, String userCircleId, String userCircleMemberId, String status) {
        super(context);
        this.token = token;
        this.userCircleId = userCircleId;
        this.userCircleMemberId = userCircleMemberId;
        this.status = status;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getMemberManageRole(token, userCircleId, userCircleMemberId, status);
    }
}
