package com.iversecomics.client.store.ui.sidemenu;

import android.app.Activity;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.ui.widget.ComicStoreImageView;

import java.util.List;

public class SideMenuListViewAdapter extends SectionListViewAdapter {

    List<SideMenuSection> all;

    Activity context;
    BitmapManager bitmapManager;

    public SideMenuListViewAdapter(Activity ctx) {
        context = ctx;
        reloadData();
    }

    public void reloadData() {
        all = SideMenuDataProvider.getInstance(context).getAllData();
    }

    public BitmapManager getBitmapManager() {
        return bitmapManager;
    }

    public void setBitmapManager(BitmapManager bitmapManager) {
        this.bitmapManager = bitmapManager;
    }

    @Override
    public int getCount() {
        int res = 0;
        for (int i = 0; i < all.size(); i++) {
            res += all.get(i).second.size();
        }
        return res;
    }

    @Override
    public SideMenuItemInfo getItem(int position) {

        int c = 0;
        for (int i = 0; i < all.size(); i++) {
            if (position >= c && position < c + all.get(i).second.size()) {
                return all.get(i).second.get(position - c);
            }
            c += all.get(i).second.size();
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    protected void bindSectionHeader(View view, int position,
                                     boolean displaySectionHeader) {
        if (displaySectionHeader) {
            view.findViewById(R.id.drawer_item_header).setVisibility(View.VISIBLE);
            TextView lSectionTitle = (TextView) view.findViewById(R.id.drawer_item_header);
            lSectionTitle.setText(getSections()[getSectionForPosition(position)]);
        } else {
            view.findViewById(R.id.drawer_item_header).setVisibility(View.GONE);
        }
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        View res = convertView;
        if (res == null) res = context.getLayoutInflater().inflate(R.layout.drawer_list_item, null);

        ComicStoreImageView image = (ComicStoreImageView) res.findViewById(R.id.itemImage);
        TextView title = (TextView) res.findViewById(R.id.itemTitle);

        SideMenuItemInfo info = getItem(position);
        if (info.getImageURL() == null || info.getImageURL().length() == 0) {
            image.setVisibility(View.GONE);
        } else {
            image.setVisibility(View.VISIBLE);
            image.setImageViaUri(Uri.parse(info.getImageURL()), getBitmapManager());
        }

        title.setText(info.getTitle());

        res.setTag(Integer.valueOf(position));
        return res;
    }

    @Override
    public void configurePinnedHeader(View header, int position, int alpha) {
        TextView lSectionHeader = (TextView) header;
        lSectionHeader.setText(getSections()[getSectionForPosition(position)]);
        lSectionHeader.setBackgroundColor(alpha << 24 | (0xbbffbb));
        lSectionHeader.setTextColor(alpha << 24 | (0x000000));
    }

    @Override
    public int getPositionForSection(int section) {

        if (section < 0) section = 0;
        if (section >= all.size()) section = all.size() - 1;
        int c = 0;
        for (int i = 0; i < all.size(); i++) {
            if (section == i) {
                return c;
            }
            c += all.get(i).second.size();
        }
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {

        int c = 0;
        for (int i = 0; i < all.size(); i++) {
            if (position >= c && position < c + all.get(i).second.size()) {
                return i;
            }
            c += all.get(i).second.size();
        }
        return -1;
    }

    @Override
    public String[] getSections() {
        String[] res = new String[all.size()];
        for (int i = 0; i < all.size(); i++) {
            res[i] = all.get(i).first;
        }
        return res;
    }


    String searchTerm;

    public void setSearchTerm(String term) {
        searchTerm = term;
    }
}
