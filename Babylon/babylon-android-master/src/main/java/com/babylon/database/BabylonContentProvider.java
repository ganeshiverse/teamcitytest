package com.babylon.database;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.babylon.database.check.CheckDatabaseHelper;
import com.babylon.database.check.CheckTables;
import com.babylon.database.schema.CommentSchema;
import com.babylon.database.schema.FileEntrySchema;
import com.babylon.database.schema.FoodSchema;
import com.babylon.database.schema.MessageSchema;
import com.babylon.database.schema.check.BodyPartColorSchema;
import com.babylon.database.schema.check.BodyPartSchema;
import com.babylon.database.schema.check.OutcomeSchema;
import com.babylon.database.schema.check.SymptomHashSchema;
import com.babylon.database.schema.check.SymptomSchema;
import com.babylon.utils.DateUtils;
import com.babylon.utils.L;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BabylonContentProvider extends ContentProvider {

    private static final String TAG = BabylonContentProvider.class.getSimpleName();

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    public static final int ENTITIES = 100;
    public static final int QUESTIONS_WITH_FILES = MessageSchema.QUESTIONS_WITH_FILES;
    private static final int PENDING_QUESTIONS = 200;
    public static final int STROKE_RISK_POINTS = 300;
    private static final int NUTRIENTS_CATEGORIES = 400;
    private static final int SYMPTOM = 500;
    private static final int SYMPTOM_HASH_OUTCOME = 600;
    private static final int BODY_PART_BY_COLOR = 700;
    private static final int CHECK_ENTITIES = 800;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DatabaseHelper.CONTENT_AUTHORITY;

        matcher.addURI(authority, "entity/" + Table.MESSAGE + QUESTIONS_WITH_FILES, QUESTIONS_WITH_FILES);
        matcher.addURI(authority, "entity/*", ENTITIES);
        matcher.addURI(authority, "pending_question", PENDING_QUESTIONS);
        matcher.addURI(authority, "nutrient_categories/#/#", NUTRIENTS_CATEGORIES);

        matcher.addURI(authority, "check_entity/*", CHECK_ENTITIES);
        matcher.addURI(authority, "symptom", SYMPTOM);
        matcher.addURI(authority, "symptom_hash_outcome", SYMPTOM_HASH_OUTCOME);
        matcher.addURI(authority, "body_part_by_color", BODY_PART_BY_COLOR);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    private BaseSQLiteOpenHelper getDbHelper(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case CHECK_ENTITIES:
            case SYMPTOM:
            case SYMPTOM_HASH_OUTCOME:
            case BODY_PART_BY_COLOR:
                return CheckDatabaseHelper.getInstance(getContext());
            default:
                return DatabaseHelper.getInstance(getContext());
        }
    }

    private SQLiteDatabase getReadableDatabase(Uri uri) {
        return getDbHelper(uri).getReadableDatabase();
    }

    private SQLiteDatabase getWritableDatabase(Uri uri) {
        return getDbHelper(uri).getWritableDatabase();
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = getWritableDatabase(uri);
        final SelectionBuilder builder = buildSimpleSelection(uri);

        int result = builder.where(selection, selectionArgs).delete(db);

        if (getContext() != null && result != 0) {
            getContext().getContentResolver().notifyChange(uri, null, false);
            notifyLinkedUri(uri);
        }

        return result;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = getWritableDatabase(uri);
        if (db != null) {
            db.insertOrThrow(getTable(uri), null, values);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null, false);
            notifyLinkedUri(uri, values);
        }
        return uri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = getReadableDatabase(uri);

        Cursor cursor = null;
        SQLiteQueryBuilder builder = buildExpandedSelection(uri);

        switch (sUriMatcher.match(uri)) {
            case ENTITIES:
            case CHECK_ENTITIES:
            case QUESTIONS_WITH_FILES:
            case PENDING_QUESTIONS:
            case STROKE_RISK_POINTS:
            case SYMPTOM:
            case SYMPTOM_HASH_OUTCOME:
            case BODY_PART_BY_COLOR:
                cursor = builder.query(db, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case NUTRIENTS_CATEGORIES:
                cursor = db.rawQuery(FoodSchema.CategoriesQuery.getByCategorySelection(uri), null);
                break;
        }
        if (cursor != null && getContext() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }


    private SQLiteQueryBuilder buildExpandedSelection(Uri uri) {
        final SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(getTable(uri));
        return builder;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = getWritableDatabase(uri);
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int result = builder
                .where(selection, selectionArgs)
                .update(db, values);
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null, false);
            notifyLinkedUri(uri, values);
        }
        return result;
    }

    private SelectionBuilder buildSimpleSelection(Uri uri) {
        final SelectionBuilder builder = new SelectionBuilder();
        return builder.table(getTable(uri));
    }

    public static String getTable(final Uri uri) {
        String tableName = null;

        switch (sUriMatcher.match(uri)) {
            case CHECK_ENTITIES:
            case ENTITIES:
                List<String> segments = uri.getPathSegments();
                tableName = segments.get(segments.size() - 1);
                break;

            case QUESTIONS_WITH_FILES:
                tableName = Table.MESSAGE.getName() + " LEFT JOIN " + Table.FILE_ENTRY.getName() + " ON "
                        + Table.MESSAGE.getName() + "." + MessageSchema.Columns.ID.getName() + "="
                        + Table.FILE_ENTRY.getName() + "." + FileEntrySchema.Columns.QUESTION_ID.getName()
                        + " LEFT JOIN " + Table.COMMENT.getName() + " ON " + Table.MESSAGE.getName() + "."
                        + MessageSchema.Columns.ID.getName() + "=" + Table.COMMENT.getName()
                        + "." + CommentSchema.Columns.QUESTION_ID.getName();
                break;
            case PENDING_QUESTIONS:
                tableName = Table.MESSAGE.getName() + " LEFT JOIN " + Table.COMMENT.getName() + " ON "
                        + Table.COMMENT.getName() + "." + CommentSchema.Columns.QUESTION_ID.getName() + "="
                        + Table.MESSAGE.getName() + "." + MessageSchema.Columns.ID.getName();
                break;
            case NUTRIENTS_CATEGORIES:
                tableName = Table.FOOD.getName();
                break;
            case SYMPTOM:
                tableName = CheckTables.SYMPTOM + " AS " + SymptomSchema.SymptomQuery.PARENT_SYMPTOM_TABLE;
                break;
            case SYMPTOM_HASH_OUTCOME:
                tableName = CheckTables.SYMPTOM_HASHES + " LEFT JOIN " + CheckTables.OUTCOME + " ON " +
                        SymptomHashSchema.Columns.OUTCOME_ID + "=" + CheckTables.OUTCOME + "." + OutcomeSchema.Columns.ID;
                break;
            case BODY_PART_BY_COLOR:
                tableName = CheckTables.BODY_PART_COLOR + " LEFT JOIN " + CheckTables.BODY_PART + " ON " +
                        BodyPartColorSchema.Columns.BODY_PART_ID + "=" + CheckTables.BODY_PART + "." + BodyPartSchema.Columns.ID;
                break;
            default:
                throw new UnsupportedOperationException("Unknown query uri: " + uri);
        }
        return tableName;
    }

    @Override
    public String getType(Uri uri) {
        String type = null;
        String tableName = getTable(uri);
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ENTITIES:
                type = "vnd.android.cursor.dir/vnd.babylon.entity." + tableName;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return type;
    }

    @Override
    public int bulkInsert(Uri uri, @NonNull ContentValues[] values) {
        SQLiteDatabase writableDatabase = getWritableDatabase(uri);
        writableDatabase.beginTransaction();
        for (int i = 0; i < values.length; i++) {
            ContentValues value = values[i];
            writableDatabase.insert(getTable(uri), null, value);
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return values.length;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws
            OperationApplicationException {
        SQLiteDatabase checkWritableDatabase = CheckDatabaseHelper.getInstance(getContext()).getWritableDatabase();
        SQLiteDatabase writableDatabase = DatabaseHelper.getInstance(getContext()).getWritableDatabase();
        ContentProviderResult[] contentProviderResults = new ContentProviderResult[]{};
        writableDatabase.beginTransaction();
        checkWritableDatabase.beginTransaction();
        try {
            contentProviderResults = super.applyBatch(operations);
            if (writableDatabase.isOpen()) {
                writableDatabase.setTransactionSuccessful();
            }
            if (checkWritableDatabase.isOpen()) {
                checkWritableDatabase.setTransactionSuccessful();
            }
        } catch (SQLiteException e) {
            L.e(TAG, e.toString());
        } finally {
            if (writableDatabase.isOpen()) {
                writableDatabase.endTransaction();
            }
            if (checkWritableDatabase.isOpen()) {
                checkWritableDatabase.endTransaction();
            }
        }
        return contentProviderResults;
    }

    private void notifyLinkedUri(Uri changedUri) {
        notifyLinkedUri(changedUri, null);
    }

    private void notifyLinkedUri(Uri changedUri, ContentValues values) {
        if (changedUri.equals(CommentSchema.CONTENT_URI)) {
            getContext().getContentResolver().notifyChange(
                    MessageSchema.CONTENT_URI_QUESTIONS_WITH_FILES_URI,
                    null, false);
        } else if (changedUri.equals(FoodSchema.CONTENT_URI)) {
            notifyFoodByCategory(values);
        }
    }

    private void notifyFoodByCategory(ContentValues values) {
        if (values != null) {
            long date = values.getAsLong(FoodSchema.Columns.DATE.getName());
            long startDate = DateUtils.getStartDate(new Date(date)).getTime();
            getContext().getContentResolver().notifyChange(FoodSchema.CategoriesQuery.getByCategoryUri(startDate)
                    , null, false);

        }
    }
}

