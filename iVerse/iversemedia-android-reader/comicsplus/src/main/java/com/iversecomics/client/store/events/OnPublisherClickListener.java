package com.iversecomics.client.store.events;

import android.support.v4.app.Fragment;

import com.iversecomics.client.store.model.Publisher;

public interface OnPublisherClickListener {
    void onPublisherClick(Fragment fragment, Publisher publisher);
}
