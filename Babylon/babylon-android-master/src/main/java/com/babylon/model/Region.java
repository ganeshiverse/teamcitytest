package com.babylon.model;

import android.text.TextUtils;

import com.babylon.api.request.payments.Currency;
import com.babylon.utils.Constants;
import com.google.gson.annotations.SerializedName;

import java.text.NumberFormat;

public class Region {
    private static final int MAX_CURRENCY_FRACTION_DIGITS = 2;
    public static final int JERSEY_REGION_ID = 2;

    private Integer id;
    private String name;
    @SerializedName("age_of_consent")
    private Integer minimumAge = Constants.REGION_MIN_AGE_DEFAULT;
    @SerializedName("iso_code")
    private String countryCode;
    @SerializedName("enable_ask")
    private boolean enableAsk;
    @SerializedName("enable_consult")
    private boolean enableConsult;
    @SerializedName("enable_monitor_me")
    private boolean enableMonitorMe;
    @SerializedName("support_number")
    private String supportNumber;
    @SerializedName("support_email")
    private String supportEmail;
    private Currency currency;
    @SerializedName("default_phone_country_code")
    private String defaultPhoneCountryCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinimumAge() {
        return minimumAge;
    }

    public void setMinimumAge(Integer minimumAge) {
        this.minimumAge = minimumAge;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

    public boolean getEnableAsk() {
        return enableAsk;
    }

    public void setEnableAsk(boolean enableAsk) {
        this.enableAsk = enableAsk;
    }

    public boolean isEnableConsult() {
        return enableConsult;
    }

    public void setEnableConsult(boolean enableConsult) {
        this.enableConsult = enableConsult;
    }

    public boolean getEnableMonitorMe() {
        return enableMonitorMe;
    }

    public void setEnableMonitorMe(boolean enableMonitorMe) {
        this.enableMonitorMe = enableMonitorMe;
    }

    @Override
    public String toString() {
        return !TextUtils.isEmpty(name)? name : "";
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getFormattedPriceString(Double price) {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        format.setMaximumFractionDigits(MAX_CURRENCY_FRACTION_DIGITS);
        format.setCurrency(java.util.Currency.getInstance(this.currency.getIsoCode()));
        return format.format(price);
    }

    public String getDefaultPhoneCountryCode() {
        return defaultPhoneCountryCode;
    }

    public void setDefaultPhoneCountryCode(String defaultPhoneCountryCode) {
        this.defaultPhoneCountryCode = defaultPhoneCountryCode;
    }

    public String getSupportNumber() {
        return supportNumber;
    }

    public void setSupportNumber(String supportNumber) {
        this.supportNumber = supportNumber;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

}