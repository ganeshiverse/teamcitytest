package com.iversecomics.client.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.iversecomics.io.IOUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.json.JSONTokener;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class JsonRest {
    private static final Logger LOG = LoggerFactory.getLogger(JsonRest.class);
    private HTTPClient          http;

    public JsonRest(HTTPClient http) {
        this.http = http;
    }

    public JSONObject get(URI uri) throws IOException, JSONException {
        HttpGet httpget = new HttpGet(uri);
        http.setUserAgent(httpget);
        http.enableGzip(httpget);
        HttpEntity entity = null;
        try {
            HttpResponse response = http.execute(httpget);
            entity = response.getEntity();
            StatusLine statusline = response.getStatusLine();

            if (statusline.getStatusCode() != HttpStatus.SC_OK) {
                StringBuilder err = new StringBuilder();
                err.append("GET not successful: ");
                err.append(statusline.getStatusCode()).append(" ");
                err.append(statusline.getReasonPhrase()).append('\n');
                err.append(uri.toASCIIString());
                LOG.warn(err.toString());
                throw new IOException(err.toString());
            }

            // http.logResponseHeaders(response);
            http.logContentLength(uri, response);

            // The response entity holds the data
            return readJSONObject(entity);
        } finally {
            http.close(entity);
        }
    }

    public JSONObject post(URI uri, JSONObject payload) throws IOException, JSONException {
        HttpEntity entity = null;
        try {
            HttpPost httppost = new HttpPost(uri);
            http.setUserAgent(httppost);
            StringEntity postentity = new StringEntity(payload.toString());
            postentity.setContentType("application/json");
            httppost.setEntity(postentity);

            HttpResponse response = http.execute(httppost);
            entity = response.getEntity();
            StatusLine statusline = response.getStatusLine();

            if (statusline.getStatusCode() != HttpStatus.SC_OK) {
                StringBuilder err = new StringBuilder();
                err.append("POST not successful: ");
                err.append(statusline.getStatusCode()).append(" ");
                err.append(statusline.getReasonPhrase()).append('\n');
                err.append(uri.toASCIIString());
                LOG.warn(err.toString());
                throw new IOException(err.toString());
            }

            http.logResponseHeaders(response);
            http.logContentLength(uri, response);

            // The response entity holds the data
            return readJSONObject(entity);
        } finally {
            http.close(entity);
        }
    }
    
    public String postForm(final URI uri, final List<NameValuePair> nameValuePairs) throws IOException, JSONException {
        HttpEntity entity = null;
        try {
        	HttpPost httppost = new HttpPost(uri);
            http.setUserAgent(httppost);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = http.execute(httppost);
            entity = response.getEntity();
            StatusLine statusline = response.getStatusLine();

            if (statusline.getStatusCode() != HttpStatus.SC_OK) {
                StringBuilder err = new StringBuilder();
                err.append("POST not successful: ");
                err.append(statusline.getStatusCode()).append(" ");
                err.append(statusline.getReasonPhrase()).append('\n');
                err.append(uri.toASCIIString());
                LOG.warn(err.toString());
                throw new IOException(err.toString());
            }

            http.logResponseHeaders(response);
            http.logContentLength(uri, response);
            
            return streamToString(http.getUngzippedContent(entity));

        } finally {
            http.close(entity);
        }
    }
    
    static String streamToString(final InputStream in)
    {
    	final InputStreamReader reader = new InputStreamReader(in);
    	BufferedReader r = new BufferedReader(reader);
    	StringBuilder response = new StringBuilder();
    	String line;
    	try {
			while ((line = r.readLine()) != null) {
				response.append(line);
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return response.toString();
    }

    private JSONObject readJSONObject(HttpEntity entity) throws IOException, JSONException {
        if (entity == null) {
            throw new JSONException("No response content found");
        }
        InputStream in = null;
        GZIPInputStream gzipin = null;
        InputStreamReader reader = null;
        try {
            in = http.getUngzippedContent(entity);
            reader = new InputStreamReader(in);
            JSONTokener jsontokener = new JSONTokener(reader);
            return new JSONObject(jsontokener);
        } finally {
            IOUtil.close(gzipin);
            IOUtil.close(reader);
            IOUtil.close(in);
        }
    }
}
