package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vvydesai on 9/4/2015.
 */
public class GetRoleDataResponse extends BaseResponse implements Serializable {


    @SerializedName("SysLookupNames")
    private ArrayList<SysLookupNames> sysLookupNames;

    public ArrayList<SysLookupNames> getSysLookupNames() {
        return sysLookupNames;
    }

    public class SysLookupNames implements Serializable {

        @SerializedName("Id")
        private String Id;

        @SerializedName("list")
        private ArrayList<list> lists;

        public String getId() {
            return Id;
        }

        public ArrayList<list> getLists() {
            return lists;
        }
    }

    public class list implements Serializable {

        @SerializedName("Id")
        private String Id;

        @SerializedName("Value")
        private String Value;

        public String getId() {
            return Id;
        }

        public String getValue() {
            return Value;
        }
    }

}
