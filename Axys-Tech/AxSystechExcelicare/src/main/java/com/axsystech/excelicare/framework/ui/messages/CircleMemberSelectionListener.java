package com.axsystech.excelicare.framework.ui.messages;

import com.axsystech.excelicare.data.responses.ListMembersInCircleResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;

/**
 * Created by someswarreddy on 02/09/15.
 */
public interface CircleMemberSelectionListener {

    public void onCircleMemberSelected(ListMembersInCircleResponse.CircleMemberDetails selectedCircleMemberObject);
}
