package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;

public interface CommentSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.DATE.getName() + ") ON CONFLICT IGNORE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.COMMENT.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        CONSULTANT_ID("consultantId", DBType.INT),
        DATE("date", DBType.INT),
        PATIENT_ID("patientId", DBType.INT),
        QUESTION_ID("questionId", DBType.TEXT),
        TEXT("text", DBType.TEXT),
        IS_READ("isRead", DBType.INT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return columnName;
        }
    }

    public interface Query {
        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int CONSULTANT_ID = 3;
        int DATE = 4;
        int PATIENT_ID = 5;
        int QUESTION_ID = 6;
        int TEXT = 7;
        int IS_READ = 8;
    }
}