package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/15/2015.
 */
public class SaveCirclePermissionsLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String requestBody;

    public SaveCirclePermissionsLoader(Context context, String requestBody) {
        super(context);
        this.requestBody = requestBody;
    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().saveCirclePermissions(requestBody);
    }
}
