package com.iversecomics.client.downloads.internal.data;

import android.net.Uri;

import com.iversecomics.app.AppConstants;

public final class DownloadsDB {
    // Content Provider Authority
    public static final String AUTHORITY = AppConstants.SKU_PACKAGE_NAME + ".downloader";
    /**
     * The content:// style URL for this database
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    // Sqlite Database Details
    public static final String DB_NAME = "downloads.db";

    /**
     * 11/12/13 ver 3, joliver, added coverImageFileName
     */
    public static final int DB_VERSION = 3;

}
