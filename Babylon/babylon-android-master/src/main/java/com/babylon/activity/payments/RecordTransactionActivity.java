package com.babylon.activity.payments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.family.FamilyPatientsListGet;
import com.babylon.api.request.payments.SubscribePatch;
import com.babylon.fragment.payments.BillingOverviewFragment;
import com.babylon.fragment.payments.CurrentSubscriptionFragment;
import com.babylon.fragment.payments.OrderDetailsListFragment;
import com.babylon.fragment.payments.PaymentConfirmationFragment;
import com.babylon.model.Patient;
import com.babylon.model.payments.OrderDetail;
import com.babylon.model.payments.Plan;
import com.babylon.model.payments.Subscription;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class RecordTransactionActivity extends PaymentTransactionActivity implements
        CurrentSubscriptionFragment.DataProtocol,
        CurrentSubscriptionFragment.OnChangeSubscriptionClickListener,
        BillingOverviewFragment.OnBillingOverviewClickListener,
        PaymentConfirmationFragment.OnPaymentConfirmationListener, PaymentConfirmationFragment
                .DataProtocol,OrderDetailsListFragment.OrderAddressFragmentListener {

    private final int SELECT_SUBSCRIPTION_FOR_PENDING_STATE_RESULT = 9;

    private List<Patient> additionalPatients = new ArrayList<Patient>();
    private CurrentSubscriptionFragment currentSubscriptionFragment;
    private Plan plan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViewById(R.id.base_layout).setBackgroundColor(getResources().getColor(R.color.orange_bright));

        new PatientGet().execute(this, mainPatientRequestListener);

        // fetch family member only if we should display the subscription screen
        if (fragmentTag() != null && fragmentTag().equals(Constants.SUBSCRIPTIONS_FRAGMENT_TAG))
        {
            new FamilyPatientsListGet().execute(this, familyListRequestListener);
        }
    }

    protected String fragmentTag()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            return extras.getString(Constants.EXTRA_FRAGMENT_TYPE);

        return null;
    }

 /*   @Override
     protected void onResume() {
        super.onResume();
        new FamilyPatientsListGet().execute(this, familyListRequestListener);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SelectSubscriptionTransactionActivity.NEW_SUBSCRIPTION_SELECTED_RESULT) {

                if (data.getExtras() != null) {
                    Patient updatedPatient = (Patient) data.getExtras().get(PaymentTransactionActivity.PATIENT_EXTRA);
                    insertPatient(updatedPatient);

                    if (currentSubscriptionFragment != null)
                    {
                        new FamilyPatientsListGet().execute(this, familyListRequestListener);

                        Intent intent = new Intent(this, RecordTransactionActivity.class);
                        intent.putExtra(Constants.EXTRA_FRAGMENT_TYPE, Constants.SUBSCRIPTIONS_FRAGMENT_TAG);
                        startActivity(intent);
                        this.finish();
                       // new FamilyPatientsListGet().execute(this, familyListRequestListener);
                      //  currentSubscriptionFragment.refreshPatients();
                         //currentSubscriptionFragment = new CurrentSubscriptionFragment();
                       // startFragment(currentSubscriptionFragment, false, PaymentConfirmationFragment.TAG);
                    }


                }

         //********   new PatientGet().execute(this, mainPatientRequestListener);*************8/

        } else if (resultCode == RESULT_OK && requestCode == SELECT_SUBSCRIPTION_FOR_PENDING_STATE_RESULT) {
            startActivity(new Intent(this, RecordTransactionActivity.class));
            finish();
        } else if (resultCode == RESULT_CANCELED && requestCode == SELECT_SUBSCRIPTION_FOR_PENDING_STATE_RESULT) {
            finish();
        }

    }

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    @Override
    public void onConfirmChangeSubscriptionListener(Patient patient) {
        this.patient = patient;

        SelectSubscriptionTransactionActivity.startActivityForResult(this, null, patient, SelectSubscriptionTransactionActivity.NEW_SUBSCRIPTION_SELECTED_RESULT);
    }

    @Override
    public List<OrderDetail> getOrderDetails() {
        orderDetails = new LinkedList<>();

        final OrderDetail orderDetail = new OrderDetail(App.getInstance().getPatient().getPlan().getTitle(),
                App.getInstance().getPatient().getSubscription().getPrice());
        this.orderDetails.add(orderDetail);

        return orderDetails;
    }

    @Override
    public String getOrderDetailsSubTitle() {
        return getString(R.string.order_details_billed_monthly);
    }

    @Override
    public void onPayClick()
    {
        //Mix Panel event
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(RecordTransactionActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.PAYMENT_REQUESTED);

        if (!isPaymentInProgress) {
            setProgressBarVisible(true);
            isPaymentInProgress = true;

            new SubscribePatch().withEntry(new SubscribePatch.BodyParams(getPaymentCard().getId(), Integer.parseInt(patient.getId()))).execute(this, onPayRequestListener);
        }
    }

    @Override
    public boolean IsSpecialist() {
        return false;
    }

    @Override
    public void onBackPressed() {
        //I know It's awkward. But we need allow user to select new subscription if he has already pending one
        final Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);

        if (fragment != null && fragment instanceof OrderDetailsListFragment) {
            startActivityForResult(new Intent(this, SelectSubscriptionTransactionActivity.class),
                    SELECT_SUBSCRIPTION_FOR_PENDING_STATE_RESULT);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPaymentDone() {
        startFragment(new CurrentSubscriptionFragment(), false, CurrentSubscriptionFragment.TAG);
    }

    @Override
    public Patient getPatient() {
        return patient;
    }

    @Override
    public void onAddFamilyToPlanClick() {
        //not implemented yet
    }

    @Override
    public Double getSubscriptionPrice() {
        return plan.getPriceForPatient(patient);
    }

    private final RequestListenerErrorProcessed<Subscription> onPayRequestListener = new
            RequestListenerErrorProcessed<Subscription>() {
                @Override
                public void onRequestComplete(Response<Subscription> response, String errorMessage) {
                    if (!RecordTransactionActivity.this.isFinishing()) {
                        isPaymentInProgress = false;

                        setProgressBarVisible(false);

                        if (TextUtils.isEmpty(errorMessage)) {
                            setProgressBarVisible(true);
                            new PatientGet().execute(RecordTransactionActivity.this, payPatientRequestListener);
                        } else {
                            Toast.makeText(RecordTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

    private final RequestListener<Patient> mainPatientRequestListener = new RequestListener<Patient>() {

        @Override
        public void onRequestComplete(Response<Patient> response) {
            patient = response.getData();

            if (patient == null) {
                startActivityForResult(new Intent(RecordTransactionActivity.this,
                        SelectSubscriptionTransactionActivity.class), SelectSubscriptionTransactionActivity
                        .NEW_SUBSCRIPTION_SELECTED_RESULT);

            } else {

                App.getInstance().setPatient(patient);

                    if (patient.getSubscription().isPending()) {
                        AlertDialogHelper.getInstance().showSimpleConfirmationDialog(RecordTransactionActivity.this, getString(R.string.pending_subscription_dialog_title), getString(R.string.pending_subscription_dialog_message) , null, getString(android.R.string.ok) , null);
                        startFragment(new OrderDetailsListFragment(), false, OrderDetailsListFragment.TAG);
                    } else {

                        switch (fragmentTag()) {
                            case Constants.SUBSCRIPTIONS_FRAGMENT_TAG:
                                currentSubscriptionFragment = new CurrentSubscriptionFragment();
                                startFragment(currentSubscriptionFragment, false, PaymentConfirmationFragment.TAG);
                                break;

                            case Constants.BILLINGS_FRAGMENT_TAG:
                                startFragment(new BillingOverviewFragment(), false, PaymentConfirmationFragment.TAG);
                                break;
                        }


                    }

            }
        }
    };

    private final RequestListener<Patient[]> familyListRequestListener = new
            RequestListenerErrorProcessed<Patient[]>() {
                @Override
                public void onRequestComplete(Response<Patient[]> response, String errorMessage) {
                    if (!isFinishing()) {
                        if (response.isSuccess()) {
                            final Patient[] patients = response.getData();
                            setFamilyPatients(patients);
                        } else {
                            final String userResponseError = response.getUserResponseError();
                            if (!TextUtils.isEmpty(userResponseError)) {
                                AlertDialogHelper.getInstance().showMessage(RecordTransactionActivity.this, userResponseError);
                            }
                        }
                    }
                }
            };

    private void setFamilyPatients(Patient[] patients) {
        this.additionalPatients.clear();
        this.additionalPatients.addAll(Arrays.asList(patients));

        if (currentSubscriptionFragment != null)
            currentSubscriptionFragment.refreshPatients();
    }

    protected void insertPatient(Patient updatedPatient)
    {
        List<Patient> allPatients = new ArrayList<>();
        allPatients.add(patient);
        allPatients.addAll(additionalPatients);

        for (Patient p : allPatients)
        {
            if (p.getId().equals(updatedPatient.getId()))
            {
                p.setSubscription(updatedPatient.getSubscription());
            }
        }
    }

    private final RequestListener<Patient> payPatientRequestListener = new RequestListener<Patient>() {

        @Override
        public void onRequestComplete(Response<Patient> response) {
            if (!RecordTransactionActivity.this.isFinishing()) {
                App.getInstance().setPatient(response.getData());
                plan = App.getInstance().getPatient().getPlan();
                if (plan == null)
                {
                    startActivityForResult(new Intent(RecordTransactionActivity.this,
                            SelectSubscriptionTransactionActivity.class), SelectSubscriptionTransactionActivity
                            .NEW_SUBSCRIPTION_SELECTED_RESULT);
                }
                else
                {
                    //Mix Panel event
                    JSONObject props = new JSONObject();
                    MixPanelHelper helper = new MixPanelHelper(RecordTransactionActivity.this,props,null);
                    helper.sendEvent(MixPanelEventConstants.PAYMENT_SUCCESS);

                    startFragment(new PaymentConfirmationFragment(), false, PaymentConfirmationFragment.TAG);
                }
            }
        }

    };

    @Override
    public Patient getMainPatient() {
        return patient;
    }

    @Override
    public List<Patient> getAdditionalPatients() {
        return additionalPatients;
    }

    @Override
    public void onOverviewDone() {

    }

    @Override
    public String getOrderTotal() {
        //total is not needed
        return null;
    }

    @Override
    public void onAddressClicked() {


    }
}
