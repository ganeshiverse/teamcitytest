package com.babylon.test;

import android.test.AndroidTestCase;
import android.widget.TextView;
import com.babylon.R;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.model.payments.PaymentCard;
import com.babylon.view.PaymentCardView;


public class PaymentCardViewTestCase extends AndroidTestCase {

    public void testMaskedNumber() {
        PaymentCard paymentCard = new PaymentCard();
        paymentCard.setMaskedNumber("********4521");

        PaymentCardView paymentCardView = new PaymentCardView(getContext());
        paymentCardView.showPaymentCard(new PaymentCardViewController(), paymentCard, null);

        String viewedMaskedNumber = ((TextView) paymentCardView.findViewById(R.id.credit_card_number_text_view))
                .getText().toString();
        assertTrue(viewedMaskedNumber.equals("**** 4521"));
    }

}
