package com.axsystech.excelicare.db;

/**
 * This will hold all database table names and column names in each table.
 * Created by someswarreddy on 07/07/15.
 */
public class DBUtils {

    public static final String TABLE_NAME_USER = "user";
    public static final String TABLE_NAME_DEVICE_USERS = "DeviceUsers";
    public static final String TABLE_NAME_SYSTEM_PREFERENCES = "SystemPreferences";
    public static final String TABLE_NAME_SITE_DETAILS = "SiteDetails";
    public static final String TABLE_NAME_MEDIA_FOLDERS = "MediaFolders";
    public static final String TABLE_NAME_MEDIA_FILES = "MediaFiles";
    public static final String TABLE_NAME_CIRCLES_LIST = "CirclesList";
    public static final String TABLE_NAME_CIRCLES_INVITATION_RECEIVED = "CirclesInvitationReceived";
    public static final String TABLE_NAME_CIRCLES_INVITATION_SENT = "CirclesInvitationSent";
    public static final String TABLE_NAME_CIRCLES_MEMBER_LIST = "CirclesMemberList";
    public static final String TABLE_NAME_CIRCLES_MEMBER_DETAILS = "CirclesMemberDetails";
    public static final String TABLE_NAME_CIRCLE_DETAILS = "CircleDetails";
    public static final String TABLE_NAME_CLIENT_URL_DETAILS = "ClientUrlAndSiteDetails";
    public static final String TABLE_NAME_CLIENT_URL_USER_DETAILS = "ClientUrlAndUserDetails";
    public static final String TABLE_NAME_MESSAGES_LIST = "MessagesList";
    //*****************************************************

    //*****************************************************
    public static final String MESSAGE_ID = "MessageId";
    public static final String PANEL_ID = "PanelId";
    public static final String MSG_RECEIVED_DATE = "MsgReceivedDate";
    public static final String MSG_SENT_DATE = "MsgSentDate";
    public static final String MESSAGE_BODY = "MessageBody";
    public static final String USER_TYPE = "UserType";
    public static final String PHOTO_URL = "PhotoUrl";
    public static final String MSG_PRIORITY = "MsgPriority";
    public static final String READ_STATUS = "MsgReadStatus";
    public static final String MSG_SENDER = "MsgSender";
    public static final String MSG_SENT_TO = "MsgSentTo";
    public static final String MSG_SUBJECT = "MsgSubject";
    public static final String MSG_ATTACHMENTS = "MsgAttachments";
    public static final String MSG_HEADER = "MsgHeader";
    public static final String SPECIALITY = "Speciality";
    //*****************************************************

    //*****************************************************
    public static final String ADDRESS = "Address";
    public static final String PRIMARY_IDENTIFIER = "PrimaryIdentifier";
    public static final String LOGIN_NAME = "LoginName";
    //*****************************************************

    //*****************************************************
    public static final String DESCRIPTION = "Description";
    public static final String VALUE = "Value";
    //*****************************************************

    //*****************************************************
    public static final String _ID = "ID";
    public static final String EC_USER_ID = "EcUserID";
    public static final String FORENAME = "Forename";
    public static final String PATIENT_ID = "patientID";
    public static final String PHOTO = "Photo";
    public static final String COMPOSITE_PK = "SiteIdAndUserId";
    public static final String COMPOSITE_KEY = "UserNameAndSiteId";
    public static final String SITE_ID = "SiteID";
    public static final String CLIENT_URL = "ClientUrl";
    public static final String SITE_NAME = "SiteName";
    public static final String SITE_PHOTO_URL = "SitePhotoUrl";
    public static final String SITE_DESCRIPTION = "SiteDescription";
    public static final String SITE_INFO_LINK = "SiteInfoLink";
    public static final String USER_EMAIL_ID = "UserEmailId";
    public static final String SURNAME = "Surname";
    public static final String AGE = "Age";
    public static final String TOKEN = "token";
    public static final String USER_DEVICE_ID = "UserDeviceID";
    public static final String DEVICE_ID = "DeviceID";
    public static final String EMAIL = "Email";
    public static final String PASSWORD = "Password";
    public static final String DOB = "DOB";
    public static final String MOBILE_NO = "MobileNo";
    public static final String GENDER = "Gender";
    public static final String GENDER_VALUE = "GenderValue";
    //*****************************************************

    //*****************************************************
    public static final String MEDIA_FOLDER_PK = "MediaFolderPrimaryKey";
    public static final String MEDIA_FOLDER_ID = "MediaFolderId";
    public static final String MEDIA_SUB_FOLDER_ID = "MediaSubFolderId";
    public static final String MEDIA_FOLDER_NAME = "MediaFolderName";
    public static final String MEDIA_SUB_FOLDER_NAME = "MediaSubFolderName";
    //*****************************************************

    //*****************************************************
    public static final String MEDIA_ITEM_PK = "MediaItemPrimaryKey";
    public static final String MEDIA_ITEM_ID = "MediaItemId";
    public static final String MEDIA_ITEM_IS_UPLOADED = "MediaItemIsUploaded";
    public static final String MEDIA_ITEM_IS_EDITED = "MediaItemIsEdited";
    public static final String MEDIA_ITEM_IS_DELETED = "MediaItemIsDeleted";
    public static final String MEDIA_ITEM_IS_UPLOADING = "MediaItemIsUploading";
    public static final String MEDIA_ITEM_NAME = "MediaItemName";
    public static final String MEDIA_FILE_NAME = "FileName";
    public static final String MEDIA_FILE_DURATION = "FileDuration";
    public static final String MEDIA_ITEM_CREATED_DATE = "MediaItemCreatedDate";
    public static final String MEDIA_ITEM_LMD = "MediaItemLMD";
    public static final String MEDIA_ITEM_COMMENTS = "MediaItemComments";
    public static final String MEDIA_ITEM_ANNOTATION_MEDIA = "MediaItemAnnotationMedia";
    public static final String MEDIA_ITEM_ORIGINAL_MEDIA = "MediaItemOriginalMedia";
    public static final String MEDIA_ITEM_PREVIEW_MEDIA = "MediaItemPreviewMedia";
    //*****************************************************

    //*****************************************************
    public static final String CIRCLES_IS_SYSTEM_CIRCLE = "CirclesIsSystemCircle";
    public static final String CIRCLES_MEMBER_COUNT = "CirclesMemberCount";
    public static final String CIRCLES_NAME = "CirclesName";
    public static final String CIRCLES_IS_PROTECTED = "CirclesIsProtected";
    public static final String CIRCLES_SITEID = "CirclesSiteId";
    public static final String CIRCLES_STARTDATE = "CirclesStartDate";
    public static final String CIRCLES_STOPDATE = "CirclesStopDate";
    public static final String CIRCLES_STOPREASONDESC = "CirclesStopreasonDesc";
    public static final String CIRCLES_STOPREASONDSLU = "CirclesStopReasonSlu";
    public static final String CIRCLES_SYSTEM_CIRCLE_ID = "CirclesSystemCircleId";
    public static final String CIRCLES_USER_CIRCLE_ID = "CirclesUserCircleId";
    public static final String CIRCLES_USER_ID = "CieclesUserId";
    public static final String CIRCLES_USER_TYPE = "CirclesUserType";
    //*****************************************************

    //*****************************************************
    public static final String CIRCLES_INVITATION_RECEIVED_INVITEID = "CirclesInvitationReceivedInviteId";
    public static final String CIRCLES_INVITATION_MESSAGE_RECEIVED_STATUS_SLU = "CirclesInvitationReceivedMessageReceivedStatusSlu";
    public static final String CIRCLES_INVITATION_MESSAGE_SENT_STATUS_SLU = "CirclesInvitationReceivedMessageSentStatusSlu";
    public static final String CIRCLES_INVITATION_RECEIVED_MESSAGE_TEXT = "CirclesInvitationReceivedMessageText";
    public static final String CIRCLES_INVITATION_RECEIVED_RECIPIENT_MAILADDRESS = "CirclesInvitationReceivedRecipientMailAddress";
    public static final String CIRCLES_INVITATION_RECEIVED_RECIPIENT_FORENAME = "CirclesInvitationReceivedRecipientForename";
    public static final String CIRCLES_INVITATION_RECEIVED_RECIPIENT_SURNAME = "CirclesInvitationReceivedRecipientSurname";
    public static final String CIRCLES_INVITATION_RECEIVED_ROLESLU = "CirclesInvitationReceivedRoleSlu";
    public static final String CIRCLES_INVITATION_RECEIVED_SENDDATE = "CirclesInvitationReceivedSendDate";
    public static final String CIRCLES_INVITATION_RECEIVED_SENDER_FORENAME = "CirclesInvitationReceivedSenderForeName";
    public static final String CIRCLES_INVITATION_RECEIVED_SENDER_NAME = "CirclesInvitationReceivedSenderName";
    public static final String CIRCLES_INVITATION_RECEIVED_SENDER_SURNAME = "CirclesInvitationReceivedSenderSurName";
    public static final String CIRCLES_INVITATION_RECEIVED_SENDER_USERID = "CirclesInvitationReceivedSenderUserId";
    public static final String CIRCLES_INVITATION_RECEIVED_SITEID = "CirclesInvitationReceivedSiteId";
    public static final String CIRCLES_INVITATION_STATUS_UPDATED_DATE = "CirclesInvitationStatusUpdatedDate";
    public static final String CIRCLES_INVITATION_RECEIVED_URL = "CirclesInvitationReceivedUrl";
    public static final String CIRCLES_INVITATION_RECEIVED_USERCIRCLEID = "CirclesInvitationReceivedUserCircleId";
    public static final String CIRCLES_INVITATION_RECEIVED_USERID = "CirclesInvitationReceivedUserId";
    //*****************************************************

    //*****************************************************
    public static final String CIRCLES_INVITATION_SENT_INVITEID = "CirclesInvitationSentInviteId";
    public static final String CIRCLES_INVITATION_SENT_MESSAGE_RECEIVED_STATUS_SLU = "CirclesInvitationSentMessageReceivedStatusSlu";
    public static final String CIRCLES_INVITATION_SENT_MESSAGE_SENT_STATUS_SLU = "CirclesInvitationSentMessageSentStatusSlu";
    public static final String CIRCLES_INVITATION_SENT_MESSAGE_TEXT = "CirclesInvitationSentMessageText";
    public static final String CIRCLES_INVITATION_SENT_RECIPIENT_MAILADDRESS = "CirclesInvitationSentRecipientMailAddress";
    public static final String CIRCLES_INVITATION_SENT_RECIPIENT_FORENAME = "CirclesInvitationSentRecipientForename";
    public static final String CIRCLES_INVITATION_SENT_RECIPIENT_SURNAME = "CirclesInvitationSentRecipientSurname";
    public static final String CIRCLES_INVITATION_SENT_ROLESLU = "CirclesInvitationSentRoleSlu";
    public static final String CIRCLES_INVITATION_SENT_SENDDATE = "CirclesInvitationSentSendDate";
    public static final String CIRCLES_INVITATION_SENT_SENDER_FORENAME = "CirclesInvitationSentSenderForeName";
    public static final String CIRCLES_INVITATION_SENT_SENDER_NAME = "CirclesInvitationSentSenderName";
    public static final String CIRCLES_INVITATION_SENT_SENDER_SURNAME = "CirclesInvitationSentSenderSurName";
    public static final String CIRCLES_INVITATION_SENT_SENDER_USERID = "CirclesInvitationSentSenderUserId";
    public static final String CIRCLES_INVITATION_SENT_SITEID = "CirclesInvitationSentSiteId";
    public static final String CIRCLES_INVITATION_SENT_STATUS_UPDATED_DATE = "CirclesInvitationStatusUpdatedDate";
    public static final String CIRCLES_INVITATION_SENT_RECEIVED_URL = "CirclesInvitationSentUrl";
    public static final String CIRCLES_INVITATION_SENT_RECEIVED_USERCIRCLEID = "CirclesInvitationSentUserCircleId";
    public static final String CIRCLES_INVITATION_SENT_USERID = "CirclesInvitationSentUserId";
    //*****************************************************

    //*****************************************************
    public static final String CIRCLE_USERID = "CircleUserId";
    public static final String CIRCLE_MEMBER_FORENAME = "CircleMemberForename";
    public static final String CIRCLE_MEMBER_SURNAME = "CircleMemberSurname";
    public static final String CIRCLE_USER_MEMBER_CIRCLEID = "CircleUserMemberCircleId";
    public static final String CIRCLE_USER_MEMBER_MAIl = "CircleUserMemberMail";
    public static final String CIRCLE_USER_MEMBER_ID = "CircleUserMemberId";
    public static final String CIRCLE_USER_TYPE = "CircleUserType";
    public static final String CIRCLE_MEMBER_SPECIALITY = "CircleMemberSpecuality";
    public static final String CIRCLE_URL = "CircleUrl";
    public static final String CIRCLE_MEMBER_ROLETYPE_SLU = "CircleMemberRoleTypeSlu";
    //*****************************************************

    //*****************************************************
    public static final String CIRCLE_MEMBER_DETAILS_ISACTIVE = "CircleMemberDetailsIsActive";
    public static final String CIRCLE_MEMBER_DETAILS_ISCIRCLEMANAGER = "CircleMemberDetailsIsCircleManager";
    public static final String CIRCLE_MEMBER_DETAILS_ADDRESS1 = "CircleMemberDetailsAddress1";
    public static final String CIRCLE_MEMBER_DETAILS_ADDRESS2 = "CircleMemberDetailsAddress2";
    public static final String CIRCLE_MEMBER_DETAILS_ADDRESS3 = "CircleMemberDetailsAddress3";
    public static final String CIRCLE_MEMBER_DETAILS_EMAIL = "CircleMemberDetailsEmail";
    public static final String CIRCLE_MEMBER_DETAILS_ENDREASONCOMMENT = "CircleMemberDetailsEndReasonComment";
    public static final String CIRCLE_MEMBER_DETAILS_ENDREASONLU = "CircleMemberDetailsEndreasonLu";
    public static final String CIRCLE_MEMBER_DETAILS_FORENAME = "CircleMemberDetailsForeName";
    public static final String CIRCLE_MEMBER_DETAILS_HOMEPHONE = "CircleMemberDetailsHomePhone";
    public static final String CIRCLE_MEMBER_DETAILS_LASTMODIFIEDDATE = "CircleMemberDetailsLastModifiedDate";
    public static final String CIRCLE_MEMBER_DETAILS_LASTMODIFIEDUSERID = "CircleMemberDetailsLastModifiedUserId";
    public static final String CIRCLE_MEMBER_DETAILS_LMD = "CircleMemberDetailsLmd";
    public static final String CIRCLE_MEMBER_DETAILS_MOBILEPHONE = "CircleMemberDetailsMobilePhone";
    public static final String CIRCLE_MEMBER_DETAILS_PHONE = "CircleMemberDetailsPhone";
    public static final String CIRCLE_MEMBER_DETAILS_PHOTOURL = "CircleMemberDetailsPhotoUrl";
    public static final String CIRCLE_MEMBER_DETAILS_ROLESLU = "CircleMemberDetailsRoleSlu";
    public static final String CIRCLE_MEMBER_DETAILS_ROLETYPESLU = "CircleMemberDetailsRoleTypeSlu";
    public static final String CIRCLE_MEMBER_DETAILS_SITEID = "CircleMemberDetailsSiteId";
    public static final String CIRCLE_MEMBER_DETAILS_SPECIALITY = "CircleMemberDetailsSpeciality";
    public static final String CIRCLE_MEMBER_DETAILS_STARTDATE = "CircleMemberDetailsIsStartDate";
    public static final String CIRCLE_MEMBER_DETAILS_STOPDATE = "CircleMemberDetailsStopdate";
    public static final String CIRCLE_MEMBER_DETAILS_SURNAME = "CircleMemberDetailsSurname";
    public static final String CIRCLE_MEMBER_DETAILS_TITLE = "CircleMemberDetailsTitle";
    public static final String CIRCLE_MEMBER_DETAILS_USERAUTHORITYTYPESLU = "CircleMemberDetailsUserAuthorityTypeSlu";
    public static final String CIRCLE_MEMBER_DETAILS_USERCIRCLEID = "CircleMemberDetailsUserCircleId";
    public static final String CIRCLE_MEMBER_DETAILS_USERCIRCLEMEMBERMAILID = "CircleMemberDetailsUserCircleMemberMailId";
    public static final String CIRCLE_MEMBER_DETAILS_USERCIRCLEMEMBERID = "CircleMemberDetailsUserCircleMemberId";
    public static final String CIRCLE_MEMBER_DETAILS_USERID = "CircleMemberDetailsUserId";
    //*****************************************************

    //*****************************************************
    public static final String CIRCLE_NAME = "CircleName";
    public static final String CIRCLE_COMMENTS = "CircleComments";
    public static final String CIRCLE_ISPROTECTED = "CircleIsProtected";
    public static final String CIRCLE_LMD = "CircleLmd";
    public static final String CIRCLE_START_DATE = "CircleStartDate";
    public static final String CIRCLE_SITE_ID = "CircleSiteId";
    public static final String CIRCLE_STOP_DATE = "CircleStopDate";
    public static final String CIRCLE_STOP_REASON_DESC = "CircleStopReasonDesc";
    public static final String CIRCLE_STOP_REASON_SLU = "CircleStopReasonSlu";
    public static final String CIRCLE_SYSTEM_CIRCLE_ID = "CircleSystemCircleId";
    public static final String CIRCLE_USER_CIRCLE_ID = "CircleUserCircleId";
    public static final String CIRCLE_USER_ID = "CircleUserId";
    public static final String CIRCLE_DETAILS_USER_TYPE = "CircleUserType";
    //*****************************************************
}
