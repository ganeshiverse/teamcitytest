package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bhargav.kadi on 7/29/2015.
 */
public class GetMessageComposeResponse extends BaseResponse implements Serializable {


    @SerializedName("data")
    private DataObject dataObject;


    public DataObject getDataObject() {
        return dataObject;
    }

    public class DataObject implements Serializable {

        @SerializedName("Details")
        private ArrayList<DetailList> dataMailList;

        @SerializedName("LMD")
        private int lmd;

        public ArrayList<DetailList> getDataMailList() {
            return dataMailList;
        }

        public int getLmd() {
            return lmd;
        }
    }

    public class DetailList implements Serializable {
        @SerializedName("UserCircleMemberEmail_ID")
        private String userCircleMemberEmail_ID;

        public String getUserCircleMemberEmail_ID() {

            return userCircleMemberEmail_ID;
        }
    }

}
