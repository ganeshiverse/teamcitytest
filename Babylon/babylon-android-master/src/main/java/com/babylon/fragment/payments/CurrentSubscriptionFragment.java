package com.babylon.fragment.payments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.SubscriptionCurrentArrayAdapter;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.SubscriptionDelete;
import com.babylon.api.request.payments.SubscriptionGetCancelText;
import com.babylon.dialog.OkCancelDialogFragment;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Patient;
import com.babylon.model.payments.SubscriptionCancelText;
import com.babylon.model.payments.SubscriptionDeleteConfirmation;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link onOkCancelSubscriptionClickListener} interface
 * to handle interaction events.
 */
public class CurrentSubscriptionFragment extends BaseFragment implements
        AdapterView.OnItemClickListener
{

    public static final String TAG = CurrentSubscriptionFragment.class.getSimpleName();

    private OnChangeSubscriptionClickListener onChangeSubscriptionListener;
    private DataProtocol dataProtocol;

    private ListView additionalPatientsListView;
    private SubscriptionCurrentArrayAdapter adapter;
    private Patient currentPatient = null;

    private boolean isListItemsEditable = false;
    private boolean isCancelingInProgress = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_current_subscription, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(R.string.current_subscription_title);


        additionalPatientsListView = (ListView)view.findViewById(R.id.subscriptions_current_list_view);

        adapter = new SubscriptionCurrentArrayAdapter(getActivity(), dataProtocol.getMainPatient(), dataProtocol.getAdditionalPatients(), onSubscriptionCancelListener);
        additionalPatientsListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        additionalPatientsListView.setOnItemClickListener(this);

        setRightButton(onEditClickListener, R.string.payments_list_edit);
    }

   /* @Override
    public void onResume() {
        super.onResume();
        adapter = new SubscriptionCurrentArrayAdapter(getActivity(), dataProtocol.getMainPatient(), dataProtocol.getAdditionalPatients(), onSubscriptionCancelListener);
        additionalPatientsListView.setAdapter(adapter);
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onChangeSubscriptionListener = (OnChangeSubscriptionClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCurrentSubscriptionInteractionListener");
        }
        try {
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onChangeSubscriptionListener = null;
    }

    @Override
    public int getColorTheme() {
        return ORANGE_THEME;
    }

    private final View.OnClickListener onEditClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setListEditable(!isListItemsEditable);
        }
    };

    private void setListEditable(Boolean editable)
    {
        isListItemsEditable = editable;

        if(adapter != null) {
            adapter.notifyDataSetChanged();
        }

        setRightButtonText(isListItemsEditable ? getString(R.string.payments_list_done) : getString(R.string.payments_list_edit));
    }

    private final View.OnClickListener onChangeSubscriptionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(App.getInstance().getPatient().getPromotion() == null) {
                onChangeSubscriptionListener.onConfirmChangeSubscriptionListener(currentPatient);
            } else {
                final OkCancelDialogFragment okCancelDialog = OkCancelDialogFragment.getInstance(R.string
                        .current_subscription_change_alert);
                okCancelDialog.setOnOkClickListener(onOkChangeSubscriptionClickListener);
                okCancelDialog.show(getFragmentManager(),
                        OkCancelDialogFragment.TAG);
            }
        }
    };

    private final DialogInterface.OnClickListener onOkChangeSubscriptionClickListener = new DialogInterface
            .OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            onChangeSubscriptionListener.onConfirmChangeSubscriptionListener(currentPatient);


        }
    };

    private final RequestListener<SubscriptionDeleteConfirmation> cancelationConfirmationRequestListener = new
            RequestListenerErrorProcessed<SubscriptionDeleteConfirmation>() {
                @Override
                public void onRequestComplete(Response<SubscriptionDeleteConfirmation> response, String errorMessage) {
                    setProgressBarVisible(false);
                    if (!getActivity().isFinishing()) {
                        if (response.isSuccess()) {
                            final SubscriptionDeleteConfirmation cancelationConfirmation = response.getData();
                            setListEditable(false);
                            isCancelingInProgress = false;

                            currentPatient.setSubscription(cancelationConfirmation.getSubscription());
                            currentPatient.setPromotion(cancelationConfirmation.getPromotion());
                            adapter.notifyDataSetChanged();

                            AlertDialogHelper.getInstance().showSimpleConfirmationDialog(getActivity(), cancelationConfirmation.getTitle(), cancelationConfirmation.getText(), null, cancelationConfirmation.getButtonText(), null);

                        } else {
                            final String userResponseError = response.getUserResponseError();
                            if (!TextUtils.isEmpty(userResponseError)) {
                                AlertDialogHelper.getInstance().showMessage(getActivity(), userResponseError);
                            }
                        }
                    }
                }
            };

    private final SubscriptionCurrentArrayAdapter.OnSubscriptionCancelListener onSubscriptionCancelListener = new

            SubscriptionCurrentArrayAdapter.OnSubscriptionCancelListener() {
                @Override
                public void onCancelSubscription(final Patient patient) {
                    if (!isCancelingInProgress) {
                        setProgressBarVisible(true);
                        isCancelingInProgress = true;

                        currentPatient = patient;

                        SubscriptionGetCancelText getRequest = new SubscriptionGetCancelText();
                        getRequest.execute(getActivity(), new RequestListener<SubscriptionCancelText>() {
                            @Override
                            public void onRequestComplete(Response<SubscriptionCancelText> response) {
                                setProgressBarVisible(false);
                                final SubscriptionCancelText cancelTexts = response.getData();

                                AlertDialogHelper.getInstance().showSimpleConfirmationDialog(getActivity(), cancelTexts.getTitle(), cancelTexts.getText(), new AlertDialogHelper
                                        .ConfirmationDialogListener() {
                                    @Override
                                    public void confirm()
                                    {
                                        setProgressBarVisible(true);

                                        //Subscription MixPanel Event
                                        JSONObject props = new JSONObject();
                                        try
                                        {
                                            props.put("Date", PreferencesManager.getInstance().getDateString());
                                        }
                                        catch (JSONException e)
                                        {
                                            e.printStackTrace();
                                        }


                                        MixPanelHelper helper = new MixPanelHelper(getActivity(),props,null);
                                        helper.sendEvent(MixPanelEventConstants.SUBSCRIPTION_CANCELLED);

                                        new SubscriptionDelete().withEntry(patient.getSubscription()).execute(getActivity(), cancelationConfirmationRequestListener);
                                    }

                                    @Override
                                    public void cancel() {
                                        isCancelingInProgress = false;
                                    }
                                }, cancelTexts.getCancelButtonText(), cancelTexts.getPerformButtonText());


                            }
                        });


                    }
                }

                @Override
                public boolean isEditable() {
                    return isListItemsEditable;
                }
            };


    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (onChangeSubscriptionListener != null && isListItemsEditable) {
            onChangeSubscriptionListener.onConfirmChangeSubscriptionListener(adapter.getPatientAtPosition(position));
        }
    }

    public void refreshPatients()
    {
        if(getActivity()!= null)
        {
            if (adapter != null)
            {
                adapter.notifyDataSetChanged();
            }
            setListEditable(false);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnChangeSubscriptionClickListener {
        public void onConfirmChangeSubscriptionListener(Patient patient);
    };

    public interface DataProtocol {
        public Patient getMainPatient();
        public List<Patient> getAdditionalPatients();
    }
}
