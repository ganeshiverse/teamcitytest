package com.iversecomics.util.text;

import org.junit.Assert;
import org.junit.Test;

public class HexTest {
    private void assertAsHex(String expected, int... input) {
        StringBuilder msg = new StringBuilder();
        msg.append("asHex(");
        boolean delim = false;
        int len = input.length;
        byte b[] = new byte[input.length];
        for (int i = 0; i < len; i++) {
            if (delim) {
                msg.append(",");
            }
            b[i] = (byte) (input[i] & 0xFF);
            msg.append("0x").append(Integer.toHexString(b[i]));
            delim = true;
        }
        msg.append(")");
        Assert.assertEquals(msg.toString(), expected, Hex.asHex(b));
    }
    
    private static void assertEquals(byte[] expected, byte[] actual) {
        assertEquals(null, expected, actual);
    }
    
    private static void assertEquals(
            String message, byte[] expected, byte[] actual) {
        if (expected.length != actual.length) {
            failWrongLength(message, expected.length, actual.length);
        }
        for (int i = 0; i < expected.length; i++) {
            if (expected[i] != actual[i]) {
                failWrongElement(message, i, expected[i], actual[i]);
            }
        }
    }
    
    private static void failWrongLength(
            String message, int expected, int actual) {
        failWithMessage(message, "expected array length:<" + expected
                + "> but was:<" + actual + '>');
    }

    private static void failWrongElement(
            String message, int index, Object expected, Object actual) {
        failWithMessage(message, "expected array element[" + index + "]:<"
                + expected + "> but was:<" + actual + '>');
    }
    
    private static void failWithMessage(String userMessage, String ourMessage) {
        Assert.fail((userMessage == null)
                ? ourMessage
                : userMessage + ' ' + ourMessage);
    }

    private void assertByteArrayEquals(int size, String rawid, byte expected[]) {
        byte actual[] = Hex.asByteArray(rawid, size);
        assertEquals(expected, actual);
    }

    @Test
    public void testAESKey() {
        assertByteArrayEquals(16, "298fe3b6c954ac02dd99ab449b9ac954", new byte[] { 0x29, (byte) 0x8F, (byte) 0xE3,
                (byte) 0xB6, (byte) 0xC9, 0x54, (byte) 0xAC, 0x02, (byte) 0xDD, (byte) 0x99, (byte) 0xAB, 0x44,
                (byte) 0x9B, (byte) 0x9A, (byte) 0xC9, 0x54 });
    }

    @Test
    public void testAsByteArray() {
        String uniqueid = "deadbeef00111100";
        byte expected[] = { (byte) 0xde, (byte) 0xad, (byte) 0xbe, (byte) 0xef, 0x00, 0x11, 0x11, 0x00 };
        byte actual[] = Hex.asByteArray(uniqueid, 8);

        assertEquals(expected, actual);
    }

    @Test
    public void testAsByteArrayFull() {
        assertByteArrayEquals(8, "6c0e26961d884b6b", new byte[] { 0x6c, 0x0e, 0x26, (byte) 0x96, 0x1d, (byte) 0x88,
                0x4b, 0x6b });
    }

    @Test
    public void testAsByteArrayRealWorld() {
        assertByteArrayEquals(8, "c0e26961dbb4b6b", new byte[] { 0x0c, 0x0e, 0x26, (byte) 0x96, 0x1d, (byte) 0xbb,
                0x4b, 0x6b });
    }

    @Test
    public void testAsByteArrayShort() {
        assertByteArrayEquals(8, "deadbeef", new byte[] { 0x00, 0x00, 0x00, (byte) 0x00, (byte) 0xde, (byte) 0xad,
                (byte) 0xbe, (byte) 0xef });
    }

    @Test(expected=IllegalArgumentException.class)
    public void testAsByteTooBig() {
        String rawid = "56c0e26961d884b6b";
        Hex.asByteArray(rawid, 8);
    }

    @Test
    public void testAsHex() {
        assertAsHex("0110", 0x01, 0x10);
        assertAsHex("deadbeef", 0xDE, 0xAD, 0xBE, 0xEF);
    }
}
