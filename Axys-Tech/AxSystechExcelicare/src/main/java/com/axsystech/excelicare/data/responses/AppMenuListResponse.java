package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 08/07/15.
 */
public class AppMenuListResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<MenuListData> menuListDataAL;

    public ArrayList<MenuListData> getMenuListDataAL() {
        return menuListDataAL;
    }

    public class MenuListData implements Serializable {
        @SerializedName("Action")
        private String action;

        @SerializedName("Alias")
        private String alias;

        @SerializedName("ID")
        private int id;

        @SerializedName("Icon")
        private String icon;

        @SerializedName("Text")
        private String text;

        @SerializedName("Type")
        private String type;

        @SerializedName("Width")
        private String width;

        @SerializedName("Items")
        private ArrayList<Items> itemsAL;

        @SerializedName("Params")
        private Params params;

        public String getAction() {
            return action;
        }

        public int getId() {
            return id;
        }

        public String getIcon() {
            return icon;
        }

        public String getText() {
            return text;
        }

        public String getType() {
            return type;
        }

        public String getWidth() {
            return width;
        }

        public ArrayList<Items> getItemsAL() {
            return itemsAL;
        }

        public Params getParams() {
            return params;
        }
    }

    public class Params implements Serializable {

        @SerializedName("ModuleID")
        private String moduleID;

        @SerializedName("Value")
        private String value; // it will be used as 'DashboardID'

        @SerializedName("RecordId")
        private String recordId;

        @SerializedName("OutputType")
        private String outputType;

        public String getOutputType() {
            return outputType;
        }

        public String getRecordId() {
            return recordId;
        }

        public String getModuleID() {
            return moduleID;
        }

        public String getValue() {
            return value;
        }

    }

    public class Items implements Serializable {
        @SerializedName("Action")
        private String action;

        @SerializedName("Alias")
        private String alias;

        @SerializedName("ID")
        private int id;

        @SerializedName("Icon")
        private String icon;

        @SerializedName("Text")
        private String text;

        @SerializedName("Params")
        private Params params;

        public String getAction() {
            return action;
        }

        public String getAlias() {
            return alias;
        }

        public int getId() {
            return id;
        }

        public String getIcon() {
            return icon;
        }

        public String getText() {
            return text;
        }

        public Params getParams() {
            return params;
        }
    }
}
