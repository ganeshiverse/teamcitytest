package com.iversecomics.store;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.store.StoreConstants;
import com.iversecomics.store.vo.PurchaseVO;

/**
 * Created by Gabriel Dogaru.
 */
public class AppStoreAdapter extends AbstractAppStoreAdapter implements SamsungIapHelper.OnInitIapListener,
        SamsungIapHelper.OnPurchaseVerified{
    /**
     * Standard activity result: operation canceled.
     */
    public static final int RESULT_CANCELED = 0;
    /**
     * Standard activity result: operation succeeded.
     */
    public static final int RESULT_OK = -1;
    /**
     * Start of user-defined activity results.
     */
    public static final int RESULT_FIRST_USER = 1;
    public static final String TAG = "Samsung_IAP_Adapter";
    // ※ CAUTION
    // SamsungIapHelper.IAP_MODE_TEST_SUCCESS mode is test mode that does not
    // occur actual billing.
    // When you release the SamsungIapHelper.IAP_MODE_COMMERCIAL mode
    // must be set.
    // In SamsungIapHelper.IAP_MODE_COMMERCIAL mode only
    // actual billing is occurred.
    // ========================================================================
    private static final int IAP_MODE = SamsungIapHelper.IAP_MODE_TEST_SUCCESS;
    Activity activity;
    PurchaseObserver purchaseObserver;
    // Communication Helper between IAPService and 3rd Party Application
    // ========================================================================
    private SamsungIapHelper mSamsungIapHelper = null;
    private Comic comicToPurchase;

    @Override
    public void onCreate(Bundle savedInstanceState, Activity activity) {
        this.activity = activity;
        initSamsungIap();
    }

    private boolean isIAPInitialized() {
        return mSamsungIapHelper != null;
    }

    private void initSamsungIap() {

        // 2. create SamsungIapHelper Instance
        //
        //    Billing does not want to set the test mode,
        //    SamsungIapHelper.IAP_MODE_TEST_SUCCESS use.
        // ====================================================================
        mSamsungIapHelper = SamsungIapHelper.getInstance(activity, IAP_MODE);

        // For test...
        /*mSamsungIapHelper = new SamsungIapHelper( this ,
                                    SamsungIapHelper.IAP_MODE_TEST_SUCCESS );*/
        // ====================================================================

        // 3. OnInitIapListener 등록
        //    Register OnInitIapListener
        // ====================================================================
        mSamsungIapHelper.setOnInitIapListener(this);
        // ====================================================================

        // 4. Register OnGetItemListListener
        // ====================================================================
//        mSamsungIapHelper.setOnGetItemListListener(this);
        // ====================================================================

        // 5. If IAP Package is installed in your device
        // ====================================================================
        if (true == mSamsungIapHelper.isInstalledIapPackage(activity)) {
            // 1) If IAP package installed in your device is valid
            // ================================================================
            if (true == mSamsungIapHelper.isValidIapPackage(activity)) {
                // show ProgressDialog
                // ------------------------------------------------------------
                mSamsungIapHelper.showProgressDialog(activity);
                // ------------------------------------------------------------
                // process SamsungAccount authentication
                // ------------------------------------------------------------
                mSamsungIapHelper.startAccountActivity(activity);
                // ------------------------------------------------------------
            }
            // ================================================================
            // 2)  If IAP package installed in your device is not valid
            // ================================================================
            else {
                // show alert dialog for invalid IAP Package
                // ------------------------------------------------------------
                mSamsungIapHelper.showIapDialog(activity,
                        activity.getString(R.string.in_app_purchase),
                        activity.getString(R.string.invalid_iap_package),
                        true, null);
                // ------------------------------------------------------------
            }
            // ================================================================
        }
        // 6.  If IAP Package is not installed in your device
        // ====================================================================
        else {
            mSamsungIapHelper.installIapPackage(activity);
        }
        // ====================================================================
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {
        mSamsungIapHelper = SamsungIapHelper.getInstance(activity, IAP_MODE);

        if (null != mSamsungIapHelper) {
            mSamsungIapHelper.stopRunningTask();
            mSamsungIapHelper.dispose();
        }
        activity = null;
    }

    @Override
    public void purchaseComic(Comic comic) {
        comicToPurchase = comic;

        mSamsungIapHelper.startPurchase(activity, SamsungIapHelper.REQUEST_CODE_IS_IAP_PAYMENT,
                StoreConstants.SAMSUNG_ITEM_GROUP_ID, StoreConstants.SAMSUNG_TEST_ITEM);

    }

    @Override
    public void purchaseSubscription(final String subscriptionSku) {
        mSamsungIapHelper.startPurchase(activity, SamsungIapHelper.ITEM_TYPE_SUBSCRIPTION,
                StoreConstants.SAMSUNG_ITEM_GROUP_ID, subscriptionSku);
    }

    @Override
    public void onSucceedInitIap() {

        // dismiss ProgressDialog
        // ====================================================================
        mSamsungIapHelper.dismissProgressDialog();

//        // ====================================================================
//        // call PurchaseMethodListActivity of IAP
//        // ====================================================================
        if (comicToPurchase != null) {
            mSamsungIapHelper.startPurchase(activity,
                    SamsungIapHelper.REQUEST_CODE_IS_IAP_PAYMENT, StoreConstants.SAMSUNG_ITEM_GROUP_ID,
                    StoreConstants.SAMSUNG_TEST_ITEM);
        }
//        // ====================================================================
    }

    @Override
    public boolean canHandle(int requestCode) {
        return requestCode == SamsungIapHelper.REQUEST_CODE_IS_IAP_PAYMENT
                || requestCode == SamsungIapHelper.REQUEST_CODE_IS_ACCOUNT_CERTIFICATION;
    }

    @Override
    public void handleActivityResult(int _requestCode, int _resultCode, Intent _intent) {
        switch (_requestCode) {
            // 1. treat result of IAPService
            // ================================================================
            case SamsungIapHelper.REQUEST_CODE_IS_IAP_PAYMENT: {
                if (null == _intent) {
                    break;
                }

                Bundle extras = _intent.getExtras();

                String itemId = "";
                String thirdPartyName = "";

                // payment success   : 0
                // payment cancelled : 1
                // ============================================================
                int statusCode = 1;
                // ============================================================

                String errorString = "";
                PurchaseVO purchaseVO = null;

                // 1) If there is bundle passed from IAP
                // ------------------------------------------------------------
                if (null != extras) {
                    thirdPartyName = extras.getString(SamsungIapHelper.KEY_NAME_THIRD_PARTY_NAME);
                    statusCode = extras.getInt(SamsungIapHelper.KEY_NAME_STATUS_CODE);
                    errorString = extras.getString(SamsungIapHelper.KEY_NAME_ERROR_STRING);
                    itemId = extras.getString(SamsungIapHelper.KEY_NAME_ITEM_ID); //store item id

                    // print log : Please remove before release
                    // --------------------------------------------------------
                    Log.i(TAG, "3rdParty Name : " + thirdPartyName + "\n" +
                            "ItemId        : " + itemId + "\n" +
                            "StatusCode    : " + statusCode + "\n" +
                            "errorString   : " + errorString);
                    // --------------------------------------------------------
                }
                // ------------------------------------------------------------
                // 2) If there is no bundle passed from IAP
                // ------------------------------------------------------------
                else {
                    mSamsungIapHelper.showIapDialog(activity,
                            activity.getString(R.string.dlg_title_payment_error),
                            activity.getString(R.string.msg_payment_was_not_processed_successfully),
                            true, null);

//                    purchase
                }
                // ------------------------------------------------------------

                // 3) If payment was not cancelled
                // ------------------------------------------------------------
                if (RESULT_OK == _resultCode) {
                    // a. IAP 에서 넘어온 결제 결과가 성공인 경우 verifyurl 과
                    //    purchaseId 값으로 서버에 해당 결제가 유효한 지 확인한다.
                    //    if Payment succeed
                    // --------------------------------------------------------
                    if (statusCode == SamsungIapHelper.IAP_ERROR_NONE) {
                        // make PurcahseVO
                        // ----------------------------------------------------
                        purchaseVO = new PurchaseVO(extras.getString(
                                SamsungIapHelper.KEY_NAME_RESULT_OBJECT));
                        // ----------------------------------------------------

                        // verify payment result
                        // ----------------------------------------------------
                        mSamsungIapHelper.verifyPurchaseResult(activity, purchaseVO);
                        // ----------------------------------------------------
                    }
                    // --------------------------------------------------------
                    // b. Payment failed
                    // --------------------------------------------------------
                    else {
                        mSamsungIapHelper.showIapDialog(activity,
                                activity.getString(R.string.dlg_title_payment_error),
                                errorString, true, null);
                    }
                    // --------------------------------------------------------
                }
                // ------------------------------------------------------------
                // 4) If payment was cancelled
                // ------------------------------------------------------------
                else if (RESULT_CANCELED == _resultCode) {
                    //payment cancelled, nothing to do
//                    mSamsungIapHelper.showIapDialog(activity,
//                            activity.getString(R.string.dlg_title_payment_cancelled),
//                            activity.getString(R.string.dlg_msg_payment_cancelled),
//                            true, null);
                }
                // ------------------------------------------------------------

                break;
            }
            // ================================================================

            // 2. treat result of SamsungAccount authentication
            // ================================================================
            case SamsungIapHelper.REQUEST_CODE_IS_ACCOUNT_CERTIFICATION: {
                // 2) If SamsungAccount authentication is succeed
                // ------------------------------------------------------------
                if (RESULT_OK == _resultCode) {
                    // IAP Service 바인드 및 초기화 진행
                    // start binding and initialization for IAPService
                    // --------------------------------------------------------
                    bindIapService();
                    // --------------------------------------------------------
                }
                // ------------------------------------------------------------
                // 3) If SamsungAccount authentication is cancelled
                // ------------------------------------------------------------
                else if (RESULT_CANCELED == _resultCode) {
                    // dismiss ProgressDialog for SamsungAccount Authentication
                    // --------------------------------------------------------
                    mSamsungIapHelper.dismissProgressDialog();
                    // --------------------------------------------------------

                    mSamsungIapHelper.showIapDialog(activity,
                            activity.getString(R.string.dlg_title_samsungaccount_authentication),
                            activity.getString(R.string.msg_authentication_has_been_cancelled),
                            true, null);
                }
                // ------------------------------------------------------------

                break;
            }
            // ================================================================
        }
    }

    /**
     * bind IAPService. If IAPService properly bound,
     * initIAP() method is called to initialize IAPService.
     */
    public void bindIapService() {
        // 1. bind to IAPService
        // ====================================================================
        mSamsungIapHelper.bindIapService(
                new SamsungIapHelper.OnIapBindListener() {
                    @Override
                    public void onBindIapFinished(int result) {
                        // 1) If successfully bound IAPService
                        // ============================================================
                        if (result == SamsungIapHelper.IAP_RESPONSE_RESULT_OK) {
                            // initialize IAPService.
                            // PurchaseMethodListActivity is called after IAPService
                            // is initialized
                            // --------------------------------------------------------
                            mSamsungIapHelper.safeInitIap(activity);
                            // --------------------------------------------------------
                        }
                        // ============================================================
                        // 2) If IAPService is not bound correctly
                        // ============================================================
                        else {
                            // dismiss ProgressDialog
                            // --------------------------------------------------------
                            mSamsungIapHelper.dismissProgressDialog();
                            // --------------------------------------------------------

                            // show alert dialog for bind failure
                            // --------------------------------------------------------
                            mSamsungIapHelper.showIapDialog(activity,
                                    activity.getString(R.string.in_app_purchase),
                                    activity.getString(R.string.msg_iap_service_bind_failed),
                                    true, null);
                            // --------------------------------------------------------
                        }
                        // ============================================================
                    }
                });
        // ====================================================================
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void restoreTransactions() {

    }

    @Override
    public void onPurchaseVerified(String id) {
        //todo
    }

    private class ComicPurchaseObserver extends PurchaseObserver {

        public ComicPurchaseObserver(Activity activity, Handler handler) {
            super(activity, handler);
        }

        @Override
        public void onBillingSupported(boolean supported) {

        }

        @Override
        public void onPurchaseStateChange(PurchaseState purchaseState, String itemId, int quantity, long purchaseTime, String receiptData, String developerPayload) {

        }
    }
}
