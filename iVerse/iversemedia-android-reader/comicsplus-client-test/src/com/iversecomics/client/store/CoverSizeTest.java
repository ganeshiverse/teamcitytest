package com.iversecomics.client.store;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

public class CoverSizeTest extends AndroidTestCase {

    @SmallTest
    public void testBestFitLarge() {
        // Exact Fit
        Assert.assertEquals(CoverSize.LARGE, CoverSize.bestFit(320, 480));

        // Slightly Smaller
        Assert.assertEquals(CoverSize.LARGE, CoverSize.bestFit(315, 475));

        // Slightly Larger
        Assert.assertEquals(CoverSize.LARGE, CoverSize.bestFit(325, 490));

        // *WAY* Larger
        Assert.assertEquals(CoverSize.LARGE, CoverSize.bestFit(1024, 768));
    }

    @SmallTest
    public void testBestFitMedium() {
        // Exact Fit
        Assert.assertEquals(CoverSize.MEDIUM, CoverSize.bestFit(58, 87));

        // Slightly Smaller
        Assert.assertEquals(CoverSize.MEDIUM, CoverSize.bestFit(55, 85));

        // Slightly Larger
        Assert.assertEquals(CoverSize.MEDIUM, CoverSize.bestFit(61, 91));
    }

    @SmallTest
    public void testBestFitSmall() {
        // Exact Fit
        Assert.assertEquals(CoverSize.SMALL, CoverSize.bestFit(28, 42));

        // *WAY* Smaller
        Assert.assertEquals(CoverSize.SMALL, CoverSize.bestFit(5, 5));

        // Slightly Smaller
        Assert.assertEquals(CoverSize.SMALL, CoverSize.bestFit(20, 40));

        // Slightly Larger
        Assert.assertEquals(CoverSize.SMALL, CoverSize.bestFit(30, 45));
    }

    @SmallTest
    public void testBestFitTabletMedium() {
        // Exact Fit
        Assert.assertEquals(CoverSize.TABLET_MEDIUM, CoverSize.bestFit(150, 225));

        // Slightly Smaller
        Assert.assertEquals(CoverSize.TABLET_MEDIUM, CoverSize.bestFit(145, 220));

        // Slightly Larger
        Assert.assertEquals(CoverSize.TABLET_MEDIUM, CoverSize.bestFit(155, 230));
    }
}
