package com.babylon.model.Kit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Category {

    @Expose
    private Integer id;
    @Expose
    private String name;
    @Expose
    private List<Product> products = new ArrayList<Product>();
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("info_button_text")
    @Expose
    private String infoButtonText;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public List<Product> getProducts() {
        return products;
    }


    public void setProducts(List<Product> products) {
        this.products = products;
    }


    public String getShortDescription() {
        return shortDescription;
    }


    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }


    public String getImageUrl() {
        return imageUrl;
    }


    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getInfoButtonText() {
        return infoButtonText;
    }


    public void setInfoButtonText(String infoButtonText) {
        this.infoButtonText = infoButtonText;
    }

}