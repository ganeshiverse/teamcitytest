package com.iversecomics.client.comic;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iversecomics.bundle.ComicBundle;
import com.iversecomics.bundle.ComicContent;
import com.iversecomics.bundle.ComicContent.Nav;
import com.iversecomics.bundle.ComicMetadata;
import com.iversecomics.bundle.Ownership;
import com.iversecomics.io.IOUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.ui.ComicReaderPhotoViewActivity;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class ComicBundleSourceAdapter implements IMyComicSourceAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(ComicBundleSourceAdapter.class);
    private File comicFile;
    private ComicBundle cb;
    private ComicContent content;
    private ComicMode mode;
    private IComicSourceListener comicSourceListener;
    final ImageLoader mImageLoader = ImageLoader.getInstance();

    public ComicBundleSourceAdapter(File comicFile) {
        this.comicFile = comicFile;
        this.cb = new ComicBundle();
        this.mode = getMetadata().isWaidFormat()?ComicMode.PANEL : ComicMode.PAGE;

        this.comicSourceListener = new LogComicSourceListener();
    }

    @Override
    public void close() throws IOException {
        try {
        this.content.close();
        }catch(Exception e ){
            LOG.error("error closing mComicSource: %s", e.getLocalizedMessage());
        }
    }

    public File getComicFile() {
        return comicFile;
    }

    public ComicMetadata getMetadata() {
        return cb.getMetadata();
    }

    @Override
    public ComicMode getComicMode() {
        return mode;
    }

    @Override
    public int getImageCount() {
        if (content != null && content.getPageNav() != null) {
            switch (getComicMode()) {
                case PAGE:
                    return content.getPageNav().size();
                default:
                case PANEL:
                    return content.getPanelNav().size();
            }
        } else {
            return 0;
        }
    }

    @Override
    public URI getImageURI(int index) {
        try {
            // LOG.debug("getImageURI(%d) [%s]", index, mode.name());
            switch (getComicMode()) {
                case PAGE:
                    return toImageSegment(content.getPageNav(), index);
                default:
                case PANEL:
                    return toImageSegment(content.getPanelNav(), index);
            }
        } catch (IOException e) {
            LOG.warn(e, "Unable to get image uri (%d)", index);
            return null;
        }
    }


    @Override
    public Bitmap loadComicPage(int pageIndex) {
        URI bitmapUri = getImageURI(pageIndex);

        String ssp = bitmapUri.getSchemeSpecificPart();
        int idx = ssp.indexOf('/');
        String modeStr = ssp.substring(0, idx);
        String indexStr = ssp.substring(idx + 1);

        ComicMode cmode = ComicMode.fromType(modeStr);
        int index = Integer.parseInt(indexStr);
        // LOG.debug("loadBitmap %s -> %s/%d", bitmapUri.toASCIIString(), cmode.name(), index);

        InputStream in = null;
        Bitmap bitmap = null;
        try {
            switch (cmode) {
                case PAGE:
                    in = content.getPageNav().getImageStream(index);
                    break;
                case PANEL:
                    in = content.getPanelNav().getImageStream(index);
                    break;
            }
            if (in == null) {
                throw new IOException("Invalid InputStream for " + bitmapUri.toASCIIString());
            }

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            try {
                if (bitmap != null) {
                    bitmap.recycle();
                }
                bitmap = BitmapFactory.decodeStream(in, null, opts);
            }catch(Exception e){
                LOG.error("OutOfMemoryError: " + e.getLocalizedMessage());
            }
            if (comicSourceListener != null) {
                comicSourceListener.onBitmapLoaded(pageIndex, bitmap);
            }
        } catch (IOException e) {
            LOG.error("loadComicPage exception: " + e.getLocalizedMessage());
        } finally {
            IOUtil.close(in);
        }
        return bitmap;
    }


    @Override
    public void open() throws IOException {
        cb.parse(comicFile);
        content = cb.getContent();
        ComicMetadata  metaData= cb.getMetadata();
        this.mode = metaData.isWaidFormat()?ComicMode.PANEL : ComicMode.PAGE;
        ComicReaderPhotoViewActivity.isWaidForamt = metaData.isWaidFormat();
        content.open();

        comicSourceListener.onComicSourceUpdated(this);
    }

    @Override
    public void setComicMode(ComicMode mode) {
        LOG.debug("Setting ComicMode to %s", mode.name());
        if (mode != this.mode) {
            this.mode = mode;
            comicSourceListener.onComicSourceUpdated(this);
        }
    }


    @Override
    public void setComicSourceListener(IComicSourceListener listener) {
        this.comicSourceListener = listener;
        if (comicSourceListener == null) {
            comicSourceListener = new LogComicSourceListener();
        }
    }

    public void setOwnership(Ownership ownership) {
        this.cb.setOwnership(ownership);
    }

    private URI toImageSegment(Nav nav, int index) throws IOException {
        String uri = String.format("comic:%s/%d", mode.name(), index);
        return URI.create(uri);
    }

    @Override
    public void onOpenFinished(boolean bSuccess) {
        if (bSuccess)
            this.comicSourceListener.onComicSourceUpdated(this);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
