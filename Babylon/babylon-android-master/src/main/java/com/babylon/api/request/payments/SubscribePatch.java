package com.babylon.api.request.payments;

import com.babylon.api.request.base.AbsRequestPatch;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Wsse;
import com.babylon.model.payments.Subscription;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;
import com.google.gson.JsonObject;
import org.apache.http.message.BasicHeader;

public class SubscribePatch extends AbsRequestPatch<SubscribePatch.BodyParams, Subscription> {

    public static class BodyParams {
        public Integer creditCardId;
        public Integer patientId;
        public BodyParams(Integer creditCardId, Integer patientId) {
            this.creditCardId = creditCardId;
            this.patientId = patientId;
        }
    }

    @Override
    public AbsRequestPatch<BodyParams, Subscription> withEntry(BodyParams params) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("credit_card_id", params.creditCardId);
        jsonObject.addProperty("patient_id", params.patientId);
        jsonBody = jsonObject.toString();
        return this;
    }
    
    @Override
    protected String getUrl() {
        return Urls.getRuby(Urls.SUBSCRIBE_POST);
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    @Override
    protected AbsResponseProcessor<Subscription> getResponseProcessor() {
        return new PaymentResponseProcessor<>(getOutputType());
    }
}
