package com.babylon.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.DataCleaner;

public class DataClearActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SyncUtils.signOut();

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void[] params) {
                new DataCleaner().clearAll(DataClearActivity.this);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                finish();
            }
        }.execute();

    }
}
