package com.axsystech.excelicare.framework.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.ForgotPasswordResponse;
import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.data.responses.LookupsListObject;
import com.axsystech.excelicare.data.responses.PasswordRulesResponse;
import com.axsystech.excelicare.data.responses.UserSecurityQuestionsResponse;
import com.axsystech.excelicare.loaders.ForgotPasswordLoader;
import com.axsystech.excelicare.loaders.GetLookUpDetailsLoader;
import com.axsystech.excelicare.loaders.UserSecurityQuestionsLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.NavigationUtils;
import com.axsystech.excelicare.util.views.FloatLabeledEditText;
import com.axsystech.excelicare.util.views.FloatLabeledSpinner;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectView;

import java.util.ArrayList;

/**
 * Created by someswar on 6/30/2015.
 */
public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    private static final String SECURITY_QUESTIONS_LOADER = "com.axsystech.excelicare.framework.ui.activities.ForgotPasswordActivity.SECURITY_QUESTIONS_LOADER";
    private static final String GET_USER_SECURITY_QUESTIONS_LOADER = "com.axsystech.excelicare.framework.ui.activities.ForgotPasswordActivity.GET_USER_SECURITY_QUESTIONS_LOADER";
    private static final String FORGOT_PASSWORD_LOADER = "com.axsystech.excelicare.framework.ui.activities.ForgotPasswordActivity.FORGOT_PASSWORD_LOADER";

    public static String seedValue = "AXSYS";

    private ForgotPasswordActivity mActivity;

    @InjectView(R.id.emailIdEditTextContainer)
    private FloatLabeledEditText emailIdEditTextContainer;

    @InjectView(R.id.securityQuestion1SpinnerTopContainer)
    private LinearLayout securityQuestion1SpinnerTopContainer;

    @InjectView(R.id.securityQuestion1SpinnerContainer)
    private LinearLayout securityQuestion1SpinnerContainer;

    @InjectView(R.id.answer1EditTextContainer)
    private FloatLabeledEditText answer1EditTextContainer;

    @InjectView(R.id.securityQuestion2SpinnerTopContainer)
    private LinearLayout securityQuestion2SpinnerTopContainer;

    @InjectView(R.id.securityQuestion2SpinnerContainer)
    private LinearLayout securityQuestion2SpinnerContainer;

    @InjectView(R.id.answer2EditTextContainer)
    private FloatLabeledEditText answer2EditTextContainer;

    @InjectView(R.id.clearBtn)
    private TextView clearBtn;

    @InjectView(R.id.securityQuestion1Spinner)
    private  TextView securityQuestion1Spinner;

    @InjectView(R.id.securityQuestion2Spinner)
    private TextView securityQuestion2Spinner;

    @InjectView(R.id.submitBtn)
    private TextView submitBtn;

    private String mInputEmailID = "";

    private String selectedSecurityQues1Id = "";
    private String selectedSecurityQues2Id = "";

    private ArrayList<LookupsListObject> securityQuesList;

    private String selectedSiteId = AxSysConstants.DEFAULT_SITE_ID;

    @Override
    protected void initActionBar() {
        getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, getString(R.string.btn_forgot_password)));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_color)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mActivity = this;
        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getAssets(), "fonts/if_std_bold.ttf");

        submitBtn.setTypeface(bold);
        clearBtn.setTypeface(bold);

        readIntentData();
        preloadData();
        addEventListeners();
    }

    private void readIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_INPUT_EMAIL)) {
                mInputEmailID = bundle.getString(AxSysConstants.EXTRA_INPUT_EMAIL, "");
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_SITE_ID)) {
                selectedSiteId = bundle.getString(AxSysConstants.EXTRA_SELECTED_SITE_ID);
            }
        }
    }

    private void preloadData() {
        emailIdEditTextContainer.getEditText().setText(mInputEmailID);

        // Check for Security Questions visibility flag in SystemPreferences Data
        if (GlobalDataModel.getInstance().getSystemPreferencesAL() != null &&
                !CommonUtils.shouldDisplaySecurityQuestions(GlobalDataModel.getInstance().getSystemPreferencesAL())) {
            // Hide if not required and do not call 'GET_LOOKUP_DETAILS_LOADER' API to retrieve Security Questions from server
            securityQuestion1SpinnerTopContainer.setVisibility(View.GONE);
            securityQuestion1SpinnerContainer.setVisibility(View.GONE);
            answer1EditTextContainer.setVisibility(View.GONE);
            securityQuestion2SpinnerTopContainer.setVisibility(View.GONE);
            securityQuestion2SpinnerContainer.setVisibility(View.GONE);
            answer2EditTextContainer.setVisibility(View.GONE);
        } else {
            // IF we display Security Question options, get Security Question from server by calling "GET_LOOKUP_DETAILS_LOADER" api
            if (CommonUtils.hasInternet()) {
                if (!isFinishing()) {
                    displayProgressLoader(false);
                    // Need to remove hard coded SystemLookUp & UserLookUp values
                    final GetLookUpDetailsLoader securityQuesLoader = new GetLookUpDetailsLoader(this, "", AxSysConstants.LOOKUP_SEQURITY_QUESTIONS, AxSysConstants.LOOKUP_USERLOOKUPS);
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SECURITY_QUESTIONS_LOADER), securityQuesLoader);
                }
            } else {
                showToast(R.string.error_no_network);
            }
        }
    }

    private void addEventListeners() {
        emailIdEditTextContainer.setOnClickListener(mActivity);
        answer1EditTextContainer.setOnClickListener(mActivity);
        answer2EditTextContainer.setOnClickListener(mActivity);

        submitBtn.setOnClickListener(mActivity);
        clearBtn.setOnClickListener(mActivity);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);

        if (id == getLoaderHelper().getLoaderId(FORGOT_PASSWORD_LOADER)) {
            if (result != null && result instanceof BaseResponse) {
                ForgotPasswordResponse response = (ForgotPasswordResponse) result;

                decideNavigation(response);
            }
        } else if (id == getLoaderHelper().getLoaderId(SECURITY_QUESTIONS_LOADER)) {

            if (result != null && result instanceof LookUpDetailsResponse) {
                LookUpDetailsResponse lookUpDetailsResponse = (LookUpDetailsResponse) result;

                securityQuesList = lookUpDetailsResponse.getSystemLookupsList();

                // Get user selected Security Questions from server
                if (CommonUtils.hasInternet()) {
                    if (!isFinishing()) {
                        displayProgressLoader(false);
                        UserSecurityQuestionsLoader userSecurityQuestionsLoader = new UserSecurityQuestionsLoader(mActivity, mInputEmailID);
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_USER_SECURITY_QUESTIONS_LOADER), userSecurityQuestionsLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_USER_SECURITY_QUESTIONS_LOADER)) {
            if (result != null && result instanceof UserSecurityQuestionsResponse) {
                UserSecurityQuestionsResponse userSecurityQuestionsResponse = (UserSecurityQuestionsResponse) result;

                if (userSecurityQuestionsResponse != null && userSecurityQuestionsResponse.getDataObject() != null) {

                    String securityQuest1Id = userSecurityQuestionsResponse.getDataObject().getQuestion1Text();
                    String securityQuest2Id = userSecurityQuestionsResponse.getDataObject().getQuestion2Text();

                    securityQuestion1Spinner.setText(securityQuest1Id);
                    securityQuestion2Spinner.setText(securityQuest2Id);

                   /* if (!TextUtils.isEmpty(securityQuest1Id) && !TextUtils.isEmpty(securityQuest2Id)) {
                        LookupsListObject securityQues1Obj = null;
                        LookupsListObject securityQues2Obj = null;

                        if (securityQuesList != null && securityQuesList.size() > 0) {
                            for (int idx = 0; idx < securityQuesList.size(); idx++) {
                                if (securityQuest1Id.trim().equalsIgnoreCase(securityQuesList.get(idx).getId() + "")) {
                                    securityQues1Obj = securityQuesList.get(idx);
                                } else if (securityQuest2Id.trim().equalsIgnoreCase(securityQuesList.get(idx).getId() + "")) {
                                    securityQues2Obj = securityQuesList.get(idx);
                                }
                            }

                            if (securityQues1Obj != null && securityQues2Obj != null) {
                                ArrayList<LookupsListObject> securityQues1List = new ArrayList<LookupsListObject>();
                                securityQues1List.add(securityQues1Obj);

                                ArrayList<LookupsListObject> securityQues2List = new ArrayList<LookupsListObject>();
                                securityQues2List.add(securityQues2Obj);


                                ArrayAdapter<LookupsListObject> securityQuestion1Adapter =
                                        new ArrayAdapter<LookupsListObject>(mActivity, android.R.layout.simple_spinner_item, securityQues1List);
                                securityQuestion1Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                ArrayAdapter<LookupsListObject> securityQuestion2Adapter =
                                        new ArrayAdapter<LookupsListObject>(mActivity, android.R.layout.simple_spinner_item, securityQues2List);
                                securityQuestion2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                securityQuestion1SpinnerContainer.getSpinner().setAdapter(securityQuestion1Adapter);
                                securityQuestion2SpinnerContainer.getSpinner().setAdapter(securityQuestion2Adapter);

                                securityQuestion1SpinnerContainer.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        selectedSecurityQues1Id = "" + ((LookupsListObject) securityQuestion1SpinnerContainer.getSpinner().getAdapter().
                                                getItem(securityQuestion1SpinnerContainer.getSpinner().getSelectedItemPosition())).getId();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                securityQuestion2SpinnerContainer.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        selectedSecurityQues2Id = "" + ((LookupsListObject) securityQuestion2SpinnerContainer.getSpinner().getAdapter().
                                                getItem(securityQuestion2SpinnerContainer.getSpinner().getSelectedItemPosition())).getId();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }*/
                }
                else {
                    showToast("This user not registered security Questions");
                }
            }
        }
    }



    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
    }


    private void decideNavigation(ForgotPasswordResponse forgotPasswordResponse) {
        try {
            if (forgotPasswordResponse != null) {
                if (!TextUtils.isEmpty(forgotPasswordResponse.getMessage())) {
                    if (NavigationUtils.LOGIN.containsString(forgotPasswordResponse.getMessage())) {
                        // Navigate user to LOGIN screen
                        Intent loginIntent = new Intent(mActivity, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        startActivity(loginIntent);
                        finish();

                    } else if (NavigationUtils.CHANGE_PASSWORD.containsString(forgotPasswordResponse.getMessage())) {
                        // Navigate user to CHANGE PASSWORD screen
                        Intent changePwdIntent = new Intent(mActivity, ChangePasswordActivity.class);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_SELECTED_SITE_ID, selectedSiteId);
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_ACTIONBAR_TITLE, getString(R.string.btn_change_password));
                        changePwdIntent.putExtra(AxSysConstants.EXTRA_INPUT_EMAIL, mInputEmailID);
                        startActivity(changePwdIntent);

                    } else if (!TextUtils.isEmpty(forgotPasswordResponse.getMessage())) {
                        showToast(forgotPasswordResponse.getMessage());
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.emailIdEditTextContainer:
            case R.id.answer1EditTextContainer:
            case R.id.answer2EditTextContainer:
                if (view instanceof FloatLabeledEditText) {
                    ((FloatLabeledEditText) view).getEditText().requestFocus();
                    CommonUtils.showSoftKeyboard(mActivity, ((FloatLabeledEditText) view).getEditText());
                }
                break;

            case R.id.submitBtn:
                if (validateUserInputs()) {
                    if (!isFinishing()) {
                        displayProgressLoader(false);
                        final ForgotPasswordLoader verifyOtpLoader = new ForgotPasswordLoader(mActivity,
                                emailIdEditTextContainer.getEditText().getText().toString().trim(),
                                selectedSecurityQues1Id, answer1EditTextContainer.getEditText().getText().toString().trim(),
                                selectedSecurityQues2Id, answer2EditTextContainer.getEditText().getText().toString().trim());
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(FORGOT_PASSWORD_LOADER), verifyOtpLoader);
                    }
                }
                break;

            case R.id.clearBtn:
                emailIdEditTextContainer.getEditText().getText().clear();
                answer1EditTextContainer.getEditText().getText().clear();
                answer2EditTextContainer.getEditText().getText().clear();
                break;
        }
    }

    private boolean validateUserInputs() {
        if (TextUtils.isEmpty(emailIdEditTextContainer.getEditText().getText().toString().trim()) ||
                !CommonUtils.isValidEmail(emailIdEditTextContainer.getEditText().getText().toString().trim())) {
            showToast(R.string.input_valid_email);
            emailIdEditTextContainer.getEditText().requestFocus();
            return false;
        }

        if (answer1EditTextContainer.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(answer1EditTextContainer.getEditText().getText().toString().trim())) {
                showToast(R.string.input_answer1);
                answer1EditTextContainer.getEditText().requestFocus();
                return false;
            }
        }

        if (answer2EditTextContainer.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(answer2EditTextContainer.getEditText().getText().toString().trim())) {
                showToast(R.string.input_answer2);
                answer2EditTextContainer.getEditText().requestFocus();
                return false;
            }
        }

        return true;
    }

}