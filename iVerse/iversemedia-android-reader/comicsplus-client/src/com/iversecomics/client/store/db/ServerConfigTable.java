package com.iversecomics.client.store.db;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class ServerConfigTable extends AbstractTable implements BaseColumns {
    /**
     * The content:// style URL for this table
     */
    public static final Uri      CONTENT_URI        = Uri.withAppendedPath(ComicStoreDB.CONTENT_URI, "config");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a list of config values.
     */
    public static final String   CONTENT_TYPE       = ContentResolver.CURSOR_DIR_BASE_TYPE
                                                            + "/vnd.iversecomics.store.config";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single config value.
     */
    public static final String   CONTENT_ITEM_TYPE  = ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                            + "/vnd.iversecomics.store.config";

    public static final String   DEFAULT_SORT_ORDER = "key ASC";
    public static final String   TABLE              = "config";

    public static final String   KEY                = "key";
    public static final String   VALUE              = "value";

    public static final String[] FULL_PROJECTION    = new String[] {
            _ID, KEY, VALUE
                                                    };
}
