package com.babylonpartners.babylon;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;

import com.babylon.utils.L;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

public class Base64ImagePicker extends Activity {

    public static final String TAG = Base64ImagePicker.class.getSimpleName();
    private static final int IMAGE_PICKER_GALLERY_RESULT_CODE = 1;
    private static final int IMAGE_PICKER_CAMERA_RESULT_CODE = 2;

    public static final String TYPE_IDENTIFIER = "type";
    public static final String IMAGE_DATA = "image";
    private static final String TYPE_CAMERA = "camera";
    private static final String TYPE_PHOTO_LIBRARY = "photo";

    private int maximumDimension = 1600;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String type = getIntent().getStringExtra(TYPE_IDENTIFIER);
        if (type.equals(TYPE_CAMERA)) {
            startCamera();
        }
        if (type.equals(TYPE_PHOTO_LIBRARY)) {
            startPhotoLibrary();
        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
        Uri imgUri = Uri.fromFile(file);
        this.imagePath = file.getAbsolutePath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);     // set the image file name
        startActivityForResult(intent, IMAGE_PICKER_CAMERA_RESULT_CODE);
    }

    private void startPhotoLibrary() {
        Intent chooserIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(chooserIntent, IMAGE_PICKER_GALLERY_RESULT_CODE);
    }

    public Bitmap rotateByExif(String imagePath, Bitmap bitmap) {
        try {
            ExifInterface oldExif = new ExifInterface(imagePath);
            int index = Integer.valueOf(oldExif.getAttribute(ExifInterface.TAG_ORIENTATION));
            int degrees = 0;
            switch(index) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degrees = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degrees = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degrees = 90;
                    break;
            }

            Matrix matrix = new Matrix();
            matrix.postRotate(degrees);

            if (degrees == 0) {
                return bitmap;
            } else {
                return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
        } catch (Exception e) {
            L.e(TAG, e.toString(), e);
        }

        return bitmap;

    }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            int requiredSize = this.maximumDimension;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= requiredSize || o.outHeight / scale / 2 >= requiredSize) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bmp = BitmapFactory.decodeFile(path, o2);
            bmp = rotateByExif(path, bmp);
            return bmp;
        } catch (OutOfMemoryError e) {
            L.e(TAG, e.toString(), e);
        }
        return null;

    }

    private void sendCancelResponse() {
        Intent resultIntent = new Intent();
        setResult(RESULT_CANCELED, resultIntent);
        finish();
    }

    private void sendImageResponse(String image) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(IMAGE_DATA, image);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private Bitmap scaleBitmap(Bitmap bitmap) {
        int maxDimension = this.maximumDimension;
        int w = maximumDimension;
        int h = maximumDimension;

        if (bitmap.getHeight() > maxDimension || bitmap.getWidth() > maxDimension) { // Portrait
            if (bitmap.getHeight() > bitmap.getWidth()) {
                w = (int) Math.round(((double) maxDimension / (double) bitmap.getHeight()) * bitmap.getWidth());
            } else { // Landscape
                h = (int) Math.round(((double) maxDimension / (double) bitmap.getWidth()) * bitmap.getHeight());
            }
            try {
                bitmap = Bitmap.createScaledBitmap(bitmap, w, h, false);
            } catch (OutOfMemoryError e) {
                L.e(TAG, e.getMessage(), e);
            }
        }
        return bitmap;
    }

    private Bitmap decodeImageFromIntent(Intent i) {
        Uri selectedImage = i.getData();
        InputStream imageStream = null;
        try {
            imageStream = getContentResolver().openInputStream(selectedImage);
        } catch (FileNotFoundException e) {
            L.e(TAG, e.toString(), e);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        return BitmapFactory.decodeStream(imageStream, null, options);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == RESULT_CANCELED) {
            sendCancelResponse();
        }
        if (resultCode != RESULT_OK) {
            return;
        }

        Bitmap bitmap = null;

        switch (requestCode) {
            case IMAGE_PICKER_GALLERY_RESULT_CODE:
                bitmap = decodeImageFromIntent(intent);
                break;
            case IMAGE_PICKER_CAMERA_RESULT_CODE:
                bitmap = decodeFile(this.imagePath);
                break;
            default:
                break;
        }

        if (bitmap == null) {
            sendCancelResponse();
            return;
        }

        bitmap = scaleBitmap(bitmap);
        // compress the image
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 30, stream);
        bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(stream.toByteArray()));

        String base64 = base64EncodedImage(bitmap);
        bitmap.recycle();
        sendImageResponse(base64);
    }

    private String base64EncodedImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

}
