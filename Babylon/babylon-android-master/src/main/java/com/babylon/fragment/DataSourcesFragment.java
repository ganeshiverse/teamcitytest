package com.babylon.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.MonitorMeActivity;
import com.babylon.service.TrackingService;
import com.babylon.utils.L;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.samsung.health.SamsungHealthManager;
import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;

public class DataSourcesFragment extends BaseFragment implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {

    public static final String TAG = DataSourcesFragment.class.getSimpleName();
    private CheckBox phoneCheckBox;
    private CheckBox samsungHeathCheckBox;
    private boolean isPhoneRunning;
    private SamsungHealthManager.SamsungHealthListener samsungHealthListener;
    private final SamsungHealthManager samsungHealthManager = SamsungHealthManager.getInstance();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            samsungHealthListener = ((MonitorMeActivity) activity)
                    .new MonitorMeSamsungHealthListener() {
                @Override
                public void onHealthStoreSuccessConnected() {
                    super.onHealthStoreSuccessConnected();
                    enableHealthCheckBox(true);
                }

                @Override
                public void onHealthStorePermissionFailed() {
                   super.onHealthStorePermissionFailed();
                    enableHealthCheckBox(false);
                }

                @Override
                public void onHealthStoreFailConnected(HealthConnectionErrorResult error) {
                    error.resolve(getActivity());
                    enableHealthCheckBox(false);
                }

                @Override
                public void onDisconnected() {
                    super.onDisconnected();
                    enableHealthCheckBox(false);
                }

            };
        } catch (ClassCastException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_data_sources, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        phoneCheckBox = (CheckBox) view.findViewById(R.id.phoneCheckBox);
        samsungHeathCheckBox = (CheckBox) view.findViewById(R.id.samsungHealthCheckBox);

        setTitle(getString(R.string.data_source_title));
        setProgressBarVisible(false);

        phoneCheckBox.setOnCheckedChangeListener(this);
        view.findViewById(R.id.appsButton).setOnClickListener(this);

        if (samsungHealthManager.isAvailable()) {
            TextView samsungHeathTextView = (TextView) view.findViewById(R.id.samsungHealthTextView);

            samsungHealthManager.activityHealthListener = samsungHealthListener;

            samsungHeathCheckBox.setVisibility(View.VISIBLE);
            samsungHeathTextView.setVisibility(View.VISIBLE);

            samsungHeathCheckBox.setChecked(samsungHealthManager.isActivated());
            samsungHeathCheckBox.setOnClickListener(this);
        }
    }

    @Override
    public void onResume() {
		super.onResume();

        isPhoneRunning = TrackingService.isRunning();
        phoneCheckBox.setChecked(isPhoneRunning);

        if (samsungHealthManager.isAvailable()) {
            samsungHealthManager.activityHealthListener = samsungHealthListener;
            samsungHeathCheckBox.setChecked(samsungHealthManager.isActivated());
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.phoneCheckBox:
                if (isChecked && !isPhoneRunning) {
                    startTrackingStepService();
                } else if (!isChecked && isPhoneRunning) {
                    stopTrackingTrackingStepService();
                }
                PreferencesManager.getInstance().setTrackingActivity(isChecked);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.samsungHealthCheckBox:
                if(samsungHealthManager.isActivated()) {
                    onDestroy();
                }
                samsungHealthManager.onCreate(getActivity(), samsungHealthListener, true);
                break;
            case R.id.appsButton:
                PreferencesManager preferencesManager = PreferencesManager.getInstance();
                if (preferencesManager.isFirstLaunchValidic()) {
                    preferencesManager.setIsFirstLaunchValidic(false);
                    new AlertDialog.Builder(getActivity()).setMessage(R.string.data_source_validic_dialog_message).
                            setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    startFragment(new ValidicSubscribeFragment());
                                }
                            }).create().show();
                } else {
                    startFragment(new ValidicSubscribeFragment());
                }
                break;
            default:
                break;
        }
    }

    private void enableHealthCheckBox(boolean isChecked) {
        samsungHeathCheckBox.setEnabled(true);
        samsungHeathCheckBox.setChecked(isChecked);
    }


    private void startTrackingStepService() {
        isPhoneRunning = true;
        getActivity().startService(new Intent(App.getInstance(), TrackingService.class));
    }

    private void stopTrackingTrackingStepService() {
        getActivity().stopService(new Intent(App.getInstance(), TrackingService.class));
        isPhoneRunning = false;
    }

}
