package com.iversecomics.client.store.model;

public class Group {
    private long    dbId = -1;

    private String  groupId;
    private String  name;
    private boolean featured;
    private String  logoImageFileName;
    private String  bannerImageFileName;
    private String	imageUrl;
    private String  parentId;
    private String  publisherId;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Group other = (Group) obj;
        if (groupId == null) {
            if (other.groupId != null) {
                return false;
            }
        } else if (!groupId.equals(other.groupId)) {
            return false;
        }
        return true;
    }

    public String getBannerImageFileName() {
        return bannerImageFileName;
    }

    public long getDbId() {
        return dbId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getLogoImageFileName() {
        return logoImageFileName;
    }

    public String getName() {
        return name;
    }

    public String getParentId() {
        return parentId;
    }

    public String getPublisherId() {
        return publisherId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        return result;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setBannerImageFileName(String bannerImageFileName) {
        this.bannerImageFileName = bannerImageFileName;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setLogoImageFileName(String logoImageFileName) {
        this.logoImageFileName = logoImageFileName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
