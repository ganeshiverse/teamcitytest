#!/usr/bin/ruby -w
# vim: ts=2 sw=2:

require 'rubygems'
require 'json'

unless ARGV.length == 1
  puts "ERROR: Must provide a json file argument"
  puts "Usage: dump-json-result-names.rb [jsonfile]"
  exit(1)
end

jsonfile = ARGV[0]

if jsonfile.nil?
  puts "ERROR: Json file argument is empty"
  exit(1)
end

unless FileTest.exists?(jsonfile)
  puts "ERROR: No such file #{$jsonfile.inspect}"
  exit(1)
end

# Read JSON
rawjson = File.read(jsonfile)
json = JSON.parse(rawjson)
results = json['results']

results.each do |result|
rName = result['name']
puts "#{rName}"
end


