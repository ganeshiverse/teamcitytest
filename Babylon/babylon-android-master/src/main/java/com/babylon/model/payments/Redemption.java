package com.babylon.model.payments;

import com.babylon.model.BaseModel;

import java.io.Serializable;
import java.util.List;

public class Redemption implements Serializable {

    @BaseModel.SkipFieldInContentValues
    private static final long serialVersionUID = 23463663567L;

    Integer id;
    Promotion promotion;
    List<Subscription> subscriptions;
    List<Appointment> appointments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }
}
