package com.iversecomics.client.store.model;

public class FeaturedSlot {
    public enum SlotPositionType {
        FeaturedSlotBackground,
        FeaturedSlotLeftTop,
        FeaturedSlotLeftMiddle,
        FeaturedSlotLeftBottom,
        FeaturedSlotRightTop,
        FeaturedSlotRightMiddle,
        FeaturedSlotRightBottom,
        FeaturedSlotBottomLeft,
        FeaturedSlotBottomMiddle,
        FeaturedSlotBottomRight
    }

    public enum FeaturedSlotType {
        FeaturedSlotTypeBackgroundImage,
        FeaturedSlotTypePromotion,
        FeaturedSlotTypePublisher,
        FeaturedSlotTypeGroup,
        FeaturedSlotTypeURL,
        FeaturedSlotTypeJustAdded,
        FeaturedSlotTypeTopPaid,
        FeaturedSlotTypeTopFree,
        FeaturedSlotTypePublisherList,
        FeaturedSlotTypeMovie,
        FeaturedSlotTypeOnDemand
    }

    int slotID;
    SlotPositionType slot;
    FeaturedSlotType slotType;
    String title;
    String imageURL;
    String targetURL;
    boolean subscription;
    String languageCode;
    int productID;
    int promotionID;
    int supplierID;
    int groupID;

    public FeaturedSlot() {

    }

    public SlotPositionType getSlot() {
        return slot;
    }

    public void setSlot(SlotPositionType slotNew) {
        slot = slotNew;
    }

    public void setSlot(int slotNew) {
        if (slotNew >= 0 && slotNew < SlotPositionType.values().length)
            slot = SlotPositionType.values()[slotNew];
    }


    public FeaturedSlotType getSlotType() {
        return slotType;
    }

    public void setSlotType(FeaturedSlotType slotNew) {
        slotType = slotNew;
    }

    public void setSlotType(int slotNew) {
        if (slotNew >= 0 && slotNew < FeaturedSlotType.values().length)
            slotType = FeaturedSlotType.values()[slotNew];
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String newTitle) {
        title = newTitle;
    }


    public String getImageURL() {
        return imageURL;
    }

    public String getImageUrl2x() {
        return imageURL.replace(".jpg", "@2x.jpg");
    }

    public void setImageURL(String newImageURL) {
        imageURL = newImageURL;
    }

    public String getTargetURL() {
        return targetURL;
    }

    public void setTargetURL(String newTargetURL) {
        targetURL = newTargetURL;
    }

    public boolean getSubscription() {
        return subscription;
    }

    public void setSubscription(boolean newSubscription) {
        subscription = newSubscription;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getPromotionID() {
        return promotionID;
    }

    public void setPromotionID(int promotionID) {
        this.promotionID = promotionID;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }
}
