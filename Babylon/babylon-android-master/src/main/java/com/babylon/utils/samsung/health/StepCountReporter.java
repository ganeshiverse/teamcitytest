package com.babylon.utils.samsung.health;

import android.support.annotation.Nullable;
import com.babylon.enums.SamsungHealthPermission;
import com.babylon.utils.DateUtils;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataResolver.Filter;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadRequest;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import java.util.Date;

public class StepCountReporter extends BaseReporter {

    @Override
    protected String getDataType() {
        return SamsungHealthPermission.STEP_READ.getName();
    }

    @Override
    protected void readData(@Nullable String dataTypeName) {
        final long createdDateFromInMillis = getSyncDateFromInMillis();

        Filter beforeTodayStepsFilter = getBeforeTodaysStepsFilter(createdDateFromInMillis);
        readNewStepData(resultListener, beforeTodayStepsFilter);

        Filter todaysStepsFilter = getTodaysStepsFilter();
        readNewStepData(resultListener, todaysStepsFilter);
    }

    private Filter getBeforeTodaysStepsFilter(long createdDateFromInMillis) {
        long minDayStartMills = DateUtils.getDayStartMills(new Date(), -DateUtils.DAYS_IN_YEAR);

        return Filter.and(Filter.greaterThanEquals(HealthConstants.StepCount.START_TIME, minDayStartMills),
                Filter.lessThan(HealthConstants.StepCount.START_TIME, DateUtils.getDayStartMills(System.currentTimeMillis())),
                Filter.greaterThanEquals(HealthConstants.StepCount.CREATE_TIME, createdDateFromInMillis),
                Filter.lessThan(HealthConstants.StepCount.CREATE_TIME, currentSyncDateInMillis)
        );
    }

    private Filter getTodaysStepsFilter() {
        return Filter.and(Filter.greaterThanEquals(HealthConstants.StepCount.START_TIME,
                        DateUtils.getDayStartMills(0)),
                Filter.lessThan(HealthConstants.StepCount.START_TIME, DateUtils.getDayStartMills(1))
        );
    }

    private void readNewStepData(HealthResultHolder.ResultListener<ReadResult> listener, Filter filter) {
        HealthDataResolver.ReadRequest request = new ReadRequest.Builder()
                .setDataType(getDataType())
                .setProperties(new String[]{
                        HealthConstants.StepCount.COUNT,
                        HealthConstants.StepCount.CALORIE,
                        HealthConstants.StepCount.START_TIME,
                        HealthConstants.StepCount.END_TIME,
                        HealthConstants.StepCount.DISTANCE})
                .setFilter(filter)
                .setSort(HealthConstants.StepCount.START_TIME, HealthDataResolver.SortOrder.DESC)
                .build();

        requestData(request, resultListener);
    }

    private final HealthResultHolder.ResultListener<ReadResult> resultListener = new HealthResultHolder
            .ResultListener<ReadResult>() {
        @Override
        public void onResult(final ReadResult result) {
            new StepsProcessor().execute(result);
        }
    };

}
