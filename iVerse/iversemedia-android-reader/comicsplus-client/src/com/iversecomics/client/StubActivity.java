package com.iversecomics.client;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.iversecomics.client.drawable.ComicPlusSplashDrawable;

/**
 * Stub Activity, used as placeholder for target-less activities, anonymous intents, pending intents, instrumentation,
 * and test cases.
 */
public class StubActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int width = ViewGroup.LayoutParams.MATCH_PARENT; //.FILL_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT; //.FILL_PARENT;
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(width, height);
        View v = new View(this);
        v.setBackgroundDrawable(ComicPlusSplashDrawable.makeWithResource(getResources(), false, true));
        v.setLayoutParams(lp);
        setContentView(v);
    }
}
