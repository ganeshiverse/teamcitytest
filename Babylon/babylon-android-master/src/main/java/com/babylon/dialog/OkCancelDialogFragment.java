package com.babylon.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.babylon.App;
import com.babylon.Keys;

public class OkCancelDialogFragment extends OkDialogFragment {

    public static String TAG = OkCancelDialogFragment.class.getSimpleName();

    public static OkCancelDialogFragment getInstance(String message) {
        OkCancelDialogFragment fragment = new OkCancelDialogFragment();

        final Bundle b = new Bundle();
        b.putString(Keys.MESSAGE, message);

        fragment.setArguments(b);

        return fragment;
    }

    public static OkCancelDialogFragment getInstance(int messageId) {
        final String message = App.getInstance().getString(messageId);
        return getInstance(message);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setView(getContentView())
                .setPositiveButton(android.R.string.ok, onOkClickListener)
                .setNegativeButton(android.R.string.cancel, onCancelClickListener)
                .create();
    }

    public void setOnOkClickListener(DialogInterface.OnClickListener onOkClickListener) {
        this.onOkClickListener = onOkClickListener;
    }

    private DialogInterface.OnClickListener onOkClickListener;


}
