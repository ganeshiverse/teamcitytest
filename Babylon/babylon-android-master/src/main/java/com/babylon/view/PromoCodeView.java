package com.babylon.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import com.babylon.R;

public class PromoCodeView extends RelativeLayout {
    public PromoCodeView(Context context) {
        super(context);
        initView();
    }

    public PromoCodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PromoCodeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_promo_code, this);
    }
}
