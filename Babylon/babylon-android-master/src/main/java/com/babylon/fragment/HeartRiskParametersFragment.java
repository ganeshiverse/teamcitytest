package com.babylon.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.MonitorMeParamsGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.base.ResponseList;
import com.babylon.controllers.MonitorMeDateViewController;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.MonitorMeParamsLoaderCallback;
import com.babylon.database.schema.MonitorMeParamSchema;
import com.babylon.enums.MonitorMeParam;
import com.babylon.enums.SmokeStatus;
import com.babylon.model.BaseModel;
import com.babylon.model.DailyFeatureMap;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.model.Patient;
import com.babylon.sync.monitor.MonitorMeParamSync;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.Constants;
import com.babylon.utils.DateUtils;
import com.babylon.utils.UIUtils;
import com.babylon.validator.ValidationController;
import com.babylon.view.CustomEditText;
import com.babylon.view.SmokerView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Fragment {@link android.support.v4.app.Fragment} for viewing and changing user parameters
 */
public class HeartRiskParametersFragment extends BaseMonitorMeParamsLoaderFragment implements View.OnClickListener {

    private TextView diabeticTextView;
    private TextView heartTreatmentTextView;
    private SmokerView smokerView;

    private CustomEditText systolicBPEditText;
    private CustomEditText diastolicBPEditText;

    private boolean isDiabeticNew;
    private boolean hasHeartTreatmentNew;
    private DailyFeatureMap dailyFeatureMap;
    private List<MonitorMeParam> featureList;

    private ValidationController validationController = new ValidationController();

    public static HeartRiskParametersFragment getInstance(long beginOfTheDayInMillis,
                                                          boolean isMedicalKitExist) {
        Bundle b = new Bundle();
        b.putLong(Constants.DATE_IN_MILLIS, beginOfTheDayInMillis);
        b.putBoolean(Constants.IS_KIT_BUTTON_EXIST, isMedicalKitExist);

        HeartRiskParametersFragment fragment = new HeartRiskParametersFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        featureList = initMonitorMeParams();

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_heart_risk_parameters, container, false);
    }

    @Override
    protected void onStartLoadingMonitorMeParamsFromServer(final long dateFrom) {
        final long endOfTheDay = DateUtils.getDayStartMills(dateInMillis, 1);
        MonitorMeParamsGet.newMonitorMeParamsGet(dateInMillis, endOfTheDay,
                MonitorMeParam.getParameterList(featureList)).execute(getActivity(),
                new RequestListener<ResponseList<MonitorMeFeatures>>() {
                    @Override
                    public void onRequestComplete(Response<ResponseList<MonitorMeFeatures>> response) {
                        if (getActivity() != null) {
                            MonitorMeParamSync.saveMonitorMeParams(getActivity().getContentResolver(), response,
                                    dateInMillis, endOfTheDay);
                            startLoaderManager();
                            onLoadedMonitorMeParamsFromServer();
                            setProgressBarVisible(false);
                        }
                    }
                }
        );
    }

    @Override
    protected void startLoaderManager() {

        getLoaderManager().initLoader(MonitorMeParamSchema.Query.CURSOR_LOAD_USER_PARAMS,
                MonitorMeParamsLoaderCallback.createBundle(dateInMillis),
                new ParamsLoader());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.heartRisk);

        boolean isMedicalKitExist = false;
        if (getArguments() != null) {
            dateInMillis = getArguments().getLong(Constants.DATE_IN_MILLIS);
            isMedicalKitExist = getArguments().getBoolean(Constants.IS_KIT_BUTTON_EXIST);
        }

        String dateString = new MonitorMeDateViewController().getFormattedDay(new Date(dateInMillis));

        ((TextView) view.findViewById(R.id.dateTextView)).setText(dateString);
        if (isMedicalKitExist && onMedicalKitClickListener != null) {
            setRightImageButton(onMedicalKitClickListener.getOnKitClickListener(),
                    R.drawable.ic_menu_kit);
        }

        smokerView = (SmokerView) view.findViewById(R.id.view_smoker);
        diabeticTextView = (TextView) getView().findViewById(R.id.text_view_diabetic);
        heartTreatmentTextView = (TextView) getView().findViewById(R.id.text_view_heart_treatment);

        diabeticTextView.setOnClickListener(this);
        heartTreatmentTextView.setOnClickListener(this);
        view.findViewById(R.id.acceptButton).setOnClickListener(this);

        systolicBPEditText = (CustomEditText) getView().findViewById(R.id.edit_text_systolic_bp);
        diastolicBPEditText = (CustomEditText) getView().findViewById(R.id.edit_text_diastolic_bp);

        BloodPressureParametersFragment.initValidation(systolicBPEditText, diastolicBPEditText, validationController,
                getActivity());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_view_diabetic:
                showDiabeticDialog();
                break;
            case R.id.text_view_heart_treatment:
                showHeartTreatment();
                break;
            case R.id.acceptButton:
                saveNewData();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PATIENT, new Bundle(), SyncAdapter.SYNC_PULL);
    }

    @Override
    protected void onTopBarButtonPressed() {
        UIUtils.hideSoftKeyboard(getActivity(), systolicBPEditText.getEditText());
        super.onTopBarButtonPressed();
    }

    private void showHeartTreatment() {
        final MonitorMeFeatures.Feature feature = dailyFeatureMap.getFeature(MonitorMeParam
                .HEART_TREATMENT_ID);
        final boolean wasHeartTreated = getBooleanValue(feature.getValue());
        final int activePosition = wasHeartTreated ? 1 : 0;
        final String[] diabeticValue = getResources().getStringArray(R.array.no_yes_value);
        AlertDialogHelper.getInstance().showListSingleChoiceAlert(getActivity(), activePosition,
                diabeticValue, onHeartTreatmentSelectedListener);
    }

    public void saveNewData() {
        if (validationController.performValidation()) {

            if (wasSmokerStatusUpdated()) {
                updatePatient();
            }

            if (wasDataUpdated()) {
                dailyFeatureMap.setValue(MonitorMeParam.DIABETIC_ID,
                        String.valueOf(isDiabeticNew ? 1 : 0));
                dailyFeatureMap.setValue(MonitorMeParam.HEART_TREATMENT_ID,
                        String.valueOf(hasHeartTreatmentNew ? 1 : 0));

                int newSmokerStatus = smokerView.getNewSmokerStatus().ordinal();
                dailyFeatureMap.setValue(MonitorMeParam.SMOKER_ID,
                        newSmokerStatus);

                putMonitorMeParamNotZero(MonitorMeParam.SYSTOLIC_BP_ID, systolicBPEditText.getEditText(),
                        dailyFeatureMap);
                putMonitorMeParamNotZero(MonitorMeParam.DIASTOLIC_BP_ID, diastolicBPEditText.getEditText(),
                        dailyFeatureMap);

                DatabaseOperationUtils.getInstance().addMonitorMeFeatures(dailyFeatureMap.getFeatureMap(),
                        getActivity().getContentResolver());
                SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MONITOR_ME_PARAMS, SyncAdapter.SYNC_PUSH);
            }

            UIUtils.hideSoftKeyboard(getActivity(), systolicBPEditText.getEditText());
            super.onTopBarButtonPressed();
        }
    }

    private void updatePatient() {
        Patient patient = App.getInstance().getPatient();
        patient.setSmokingStatus(smokerView.getNewServerSmokerStatus());
        patient.setIsUpdated(true);
        App.getInstance().setPatient(patient);
    }

    /**
     * Define if patient personal data or daily report was updated
     *
     * @return true if patient personal data or daily report was updated
     * false if patient personal data and daily report wasn't updated
     */
    private boolean wasDataUpdated() {
        boolean wasSmokerStatusUpdated = wasSmokerStatusUpdated();

        final String oldHeartTreatmentStr = dailyFeatureMap.getFeature(MonitorMeParam.HEART_TREATMENT_ID).getValue();
        final String curHeartTreatmentStr = String.valueOf(hasHeartTreatmentNew ? 1 : 0);

        boolean heartTreatment = curHeartTreatmentStr.equals(oldHeartTreatmentStr);

        final String oldDiabeticStr = dailyFeatureMap.getFeature(MonitorMeParam.DIABETIC_ID).getValue();
        final String curDiabeticStr = String.valueOf(isDiabeticNew ? 1 : 0);

        boolean diabetic = curDiabeticStr.equals(oldDiabeticStr);

        return wasSmokerStatusUpdated || !(diabetic && heartTreatment) || wasBloodPressureUpdated();
    }

    private boolean wasSmokerStatusUpdated() {
        final int newSmokerStatus = smokerView.getNewSmokerStatus().ordinal();
        final MonitorMeFeatures.Feature feature = dailyFeatureMap.getFeature(MonitorMeParam.SMOKER_ID);

        int oldSmokerStatus = -1;

        if (!TextUtils.isEmpty(feature.getValue())) {
            oldSmokerStatus = feature.getFloatValue().intValue();
        }

        return newSmokerStatus != oldSmokerStatus;
    }

    private boolean wasBloodPressureUpdated() {
        boolean systolicBP = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.SYSTOLIC_BP_ID))
                .compareTo(UIUtils.getFloat(systolicBPEditText)) == 0;

        boolean diastolicBP = BaseModel.getFloat(dailyFeatureMap
                .getFeatureValue(MonitorMeParam.DIASTOLIC_BP_ID))
                .compareTo(UIUtils.getFloat(diastolicBPEditText)) == 0;

        return !(systolicBP && diastolicBP);
    }

    private void showDiabeticDialog() {
        final int activePosition = isDiabeticNew ? 1 : 0;
        final String[] diabeticValue = getResources().getStringArray(R.array.no_yes_value);
        AlertDialogHelper.getInstance().showListSingleChoiceAlert(getActivity(), activePosition,
                diabeticValue, onDiabeticValueSelectedListener);
    }

    private String getDisplayedBoolean(boolean value) {
        int diabetic = value ? 1 : 0;
        String diabeticSelectedValue = getResources().getStringArray(R.array.no_yes_value)[diabetic];
        return diabeticSelectedValue;
    }

    private class ParamsLoader extends MonitorMeParamsLoaderCallback {

        @Override
        public Context getContext() {
            return getActivity();
        }

        @Override
        public List<MonitorMeParam> getMonitorMeParamList() {
            return featureList;
        }

        @Override
        public void onParamsLoaded(Map<String, MonitorMeFeatures.Feature> featureMap) {
            dailyFeatureMap = new DailyFeatureMap(featureMap);

            if (!featureMap.isEmpty()) {
                MonitorMeFeatures.Feature diabeticFeature = dailyFeatureMap
                        .getFeature(MonitorMeParam.DIABETIC_ID);
                isDiabeticNew = getBooleanValue(diabeticFeature.getValue());
                showBooleanValue(isDiabeticNew, diabeticTextView);

                MonitorMeFeatures.Feature heartTreatmentFeature = dailyFeatureMap
                        .getFeature(MonitorMeParam.HEART_TREATMENT_ID);

                hasHeartTreatmentNew = getBooleanValue(heartTreatmentFeature.getValue());
                showBooleanValue(hasHeartTreatmentNew, heartTreatmentTextView);

                loadSmokeStatus();

                showNotZeroValue(systolicBPEditText.getEditText(), MonitorMeParam.SYSTOLIC_BP_ID, dailyFeatureMap);
                showNotZeroValue(diastolicBPEditText.getEditText(), MonitorMeParam.DIASTOLIC_BP_ID, dailyFeatureMap);
            }
        }
    }

    private void loadSmokeStatus() {
        MonitorMeFeatures.Feature smokerFeature = dailyFeatureMap
                .getFeature(MonitorMeParam.SMOKER_ID);
        SmokeStatus smokerStatus = SmokeStatus.getSmokerStatus(smokerFeature);
        smokerView.setSmokerStatus(smokerStatus.getSmokerText());
    }

    private void showBooleanValue(boolean value, TextView textView) {
        String diabeticDisplayValue = getDisplayedBoolean(value);
        textView.setText(diabeticDisplayValue);
    }

    private List<MonitorMeParam> initMonitorMeParams() {
        List<MonitorMeParam> featureList =
                new LinkedList<MonitorMeParam>();
        featureList.add(MonitorMeParam.SYSTOLIC_BP_ID);
        featureList.add(MonitorMeParam.DIASTOLIC_BP_ID);
        featureList.add(MonitorMeParam.DIABETIC_ID);
        featureList.add(MonitorMeParam.HEART_TREATMENT_ID);
        featureList.add(MonitorMeParam.SMOKER_ID);
        return featureList;
    }

    private final DialogInterface.OnClickListener onDiabeticValueSelectedListener = new DialogInterface
            .OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            final int selectPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();

            isDiabeticNew = selectPosition != 0;

            String diabeticSelectedValue = getResources().getStringArray(R.array.no_yes_value)[selectPosition];
            diabeticTextView.setText(diabeticSelectedValue);
        }
    };

    private final DialogInterface.OnClickListener onHeartTreatmentSelectedListener = new DialogInterface
            .OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            final int selectPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
            hasHeartTreatmentNew = selectPosition != 0;

            String heartTreatmentSelectedValue = getResources().getStringArray(R.array.no_yes_value)[selectPosition];
            heartTreatmentTextView.setText(heartTreatmentSelectedValue);
        }
    };
}