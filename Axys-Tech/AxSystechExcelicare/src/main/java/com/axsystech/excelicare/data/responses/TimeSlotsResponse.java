package com.axsystech.excelicare.data.responses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class TimeSlotsResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private TimeSlotsData timeSlotsData;

    public TimeSlotsData getTimeSlotsData() {
        return timeSlotsData;
    }

    public class TimeSlotsData implements Serializable {

        @SerializedName("TimeSlotDetails")
        private ArrayList<TimeSlotDetails> timeSlotDetailsArrayList;

        public ArrayList<TimeSlotDetails> getTimeSlotDetailsArrayList() {
            return timeSlotDetailsArrayList;
        }
    }

    public class TimeSlotDetails implements Serializable {

        @SerializedName("IsBooked")
        private String isBooked;

        @SerializedName("TimeSlot")
        private String timeSlot;

        @SerializedName("SlotLength")
        private String slotLength;

        public String getSlotLength() {
            return slotLength;
        }

        public String getIsBooked() {
            return isBooked;
        }

        public boolean isSlotAvailable() {
            return (!TextUtils.isEmpty(isBooked) && isBooked.trim().equalsIgnoreCase("Available")) ? true : false;
        }

        public String getTimeSlot() {
            return timeSlot;
        }
    }

}
