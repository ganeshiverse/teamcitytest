package com.iversecomics.client.refresh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

public class TaskPoolTest extends AndroidTestCase {
    private static final Logger LOG = LoggerFactory.getLogger(TaskPoolTest.class);

    private void assertMessages(String[] expectedMsgs, List<String> actualMsgs) {
        assertEquals("Expected Messages.length", expectedMsgs.length, actualMsgs.size());
        int len = expectedMsgs.length;
        for (int i = 0; i < len; i++) {
            assertEquals("Message[" + i + "]", expectedMsgs[i], actualMsgs.get(i));
        }
    }

    @MediumTest
    public void testDuplicateTasks() throws Exception {
        List<String> msgs = Collections.synchronizedList(new ArrayList<String>());

        TaskPool tasks = new TaskPool();
        List<Future<?>> futures = new ArrayList<Future<?>>();

        String strs[] = {
                "it's",
                "a",
                "big",
                "enough",
                "umbrella",
                "but",
                "it's",
                "always",
                "me",
                "that",
                "ends",
                "up",
                "getting",
                "wet"
        };

        for (String str : strs) {
            MessageTask task = new MessageTask(str);
            task.setOut(msgs);
            futures.add(tasks.submit(task));
        }

        // Makes sure we get all of the responses
        for (Future<?> fut : futures) {
            if (fut == null) {
                continue; // skip null futures.
            }
            if (fut.isCancelled()) {
                continue; // skip cancelled futures
            }
            LOG.debug("Future.get() on %s", fut);
            fut.get(5, TimeUnit.SECONDS);
        }

        // The expected list.
        String expectedStrs[] = {
                "it's", "a", "big", "enough", "umbrella", "but", "always", "me", "that", "ends", "up", "getting", "wet"
        };
        // Note: we removed the second "it's"

        assertMessages(expectedStrs, msgs);
    }

    @MediumTest
    public void testMultipleTasks() throws Exception {
        List<String> msgs = Collections.synchronizedList(new ArrayList<String>());

        TaskPool tasks = new TaskPool();
        List<Future<?>> futures = new ArrayList<Future<?>>();

        String strs[] = {
                "Sun is shining, the weather is sweet",
                "Make you want to move your dancing feet",
                "To the rescue, here I am",
                "Want you to know, y'all, where I stand"
        };

        for (String str : strs) {
            MessageTask task = new MessageTask(str);
            task.setOut(msgs);
            futures.add(tasks.submit(task));
        }

        // Makes sure we get all of the responses
        for (Future<?> fut : futures) {
            if (!fut.isCancelled()) {
                fut.get(5, TimeUnit.SECONDS);
            }
        }

        assertMessages(strs, msgs);
    }

    @SmallTest
    public void testQueueContains() {
        LinkedBlockingQueue<Task> queue = new LinkedBlockingQueue<Task>();
        queue.add(new MessageTask("by order of the prophet"));
        queue.add(new MessageTask("we ban that boogie sound"));
        queue.add(new MessageTask("degenerate the faithful"));
        queue.add(new MessageTask("with that crazy casbah sound"));

        String msg = "we ban that boogie sound";
        assertTrue("Should contain message [" + msg + "]", queue.contains(new MessageTask(msg)));

        msg = "rock the casbah";
        assertFalse("Should not contain message [" + msg + "]", queue.contains(new MessageTask(msg)));
    }

    @MediumTest
    public void testSingleTask() throws Exception {
        List<String> msgs = Collections.synchronizedList(new ArrayList<String>());

        TaskPool tasks = new TaskPool();
        MessageTask task = new MessageTask("Single");
        task.setOut(msgs);
        Future<?> fut = tasks.submit(task);
        fut.get(10, TimeUnit.SECONDS);

        assertMessages(new String[] {
            "Single"
        }, msgs);
    }
}
