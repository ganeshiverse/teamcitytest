package com.iversecomics.client.account;

import android.content.ContentResolver;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.bitmap.BitmapManager;

import java.util.ArrayList;

public class ComicStoreAdaptor extends BaseAdapter {

    final Context context;
    final int viewResourceId;
    final ArrayList<StoreLocation> storeLocations;
    final ContentResolver contentResolver;
    final BitmapManager bitmapManager;

    ComicStoreAdaptor(final Context context, final int viewResourceId, final ArrayList<StoreLocation> storeLocations) {
        this.context = context;
        this.viewResourceId = viewResourceId;
        this.storeLocations = storeLocations;
        contentResolver = context.getContentResolver();
        IverseApplication app = (IverseApplication) context.getApplicationContext();
        bitmapManager = app.createBitmapManager();
    }

    @Override
    public int getCount() {
        return storeLocations.size();
    }

    @Override
    public StoreLocation getItem(final int position) {
        return storeLocations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(viewResourceId, parent, false);
        }

        return makeView(position, convertView);
    }

    View makeView(final int position, final View view) {
        setTextOrHide(view, R.id.store_name, storeLocations.get(position).getName());
        setTextOrHide(view, R.id.store_address_1, storeLocations.get(position).getAddress());
        setTextOrHide(view, R.id.store_address_2, storeLocations.get(position).getAddress2());

        final String cityStateZip = storeLocations.get(position).getCity() + ", " +
                storeLocations.get(position).getState() + " " +
                storeLocations.get(position).getZip();

        setTextOrHide(view, R.id.city_state_zip, cityStateZip);
        setTextOrHide(view, R.id.phone, storeLocations.get(position).getPhone());
        return view;
    }

    void setTextOrHide(final View view, final int id, final String text) {
        final TextView tv = (TextView) view.findViewById(id);
        if (text == null || text.trim().equals("") || text.trim().equals("null")) {
            tv.setVisibility(View.GONE);
        } else {
            tv.setVisibility(View.VISIBLE);
            tv.setText(Html.fromHtml(text));
        }
    }
}
