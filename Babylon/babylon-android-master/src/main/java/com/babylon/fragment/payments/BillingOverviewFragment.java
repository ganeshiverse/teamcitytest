package com.babylon.fragment.payments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.PaymentHistoryAdapter;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.TransactionListGet;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.model.payments.Transaction;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.view.TopBarInterface;

import java.text.SimpleDateFormat;
import java.util.*;

public class BillingOverviewFragment extends BaseFragment implements View.OnClickListener {
    private CurrentSubscriptionFragment.DataProtocol dataProtocol;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    public interface OnBillingOverviewClickListener {
        public void onOverviewDone();
    }

    private OnBillingOverviewClickListener onBillingOverviewListener;
    private PaymentHistoryAdapter adapter;
    private List<Transaction> transactions = new ArrayList<Transaction>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {

        return inflater.inflate(R.layout.fragment_billing_overview, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onBillingOverviewListener = (OnBillingOverviewClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBillingOverviewListener");
        }
        try {
            dataProtocol = (CurrentSubscriptionFragment.DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onBillingOverviewListener = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setProgressBarVisible(true);
        new TransactionListGet().execute(getActivity(), transactionListRequestListener);

        ListView listView = (ListView) view.findViewById(android.R.id.list);
        setTitle(getString(R.string.billing_title));
        //setRightButton(this, R.string.done);

        showHeader(listView);

        adapter = new PaymentHistoryAdapter(getActivity(), transactions);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (onBillingOverviewListener != null) {
            switch (view.getId()) {
                case R.id.topbar_right_text_button:
                    onBillingOverviewListener.onOverviewDone();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    private void showHeader(ListView listView) {
        View headerView = View.inflate(getActivity(), R.layout.list_header_billing_overview, null);

        TextView planTextView = (TextView) headerView.findViewById(R.id.plan_data_text_view);
        TextView paymentTextView = (TextView) headerView.findViewById(R.id.payment_data_text_view);
        TextView dueTextView = (TextView) headerView.findViewById(R.id.due_data_text_view);
        TextView amountTextView = (TextView) headerView.findViewById(R.id.amount_data_text_view);
        TextView codeTitleTextView = (TextView) headerView.findViewById(R.id.code_text_view);
        TextView codeTextView = (TextView) headerView.findViewById(R.id.code_data_text_view);

        Patient patient = dataProtocol.getMainPatient();

        // PLAN
        String ageText = patient.getIsMinor() ? "Child" : "Adult"; // TODO: Get these strings from the server side!?
        String planText = String.format("%s - %s", ageText, patient.getPlan().getTitle());
        planTextView.setText(planText);

        if (patient.getPlan().isDefault()) // Pay As You Go
        {
            paymentTextView.setText("-");
            dueTextView.setText("-");
            amountTextView.setText("-");
        }
        else { // Subscription

            // Payment
            if (patient.getSubscription() != null) {
                String paymentText = (patient.getSubscription().getCreditCard() == null) ? "N/A" : patient.getSubscription().getCreditCard().getDisplayName();
                paymentTextView.setText(paymentText);

                // Next Due
                if (patient.getSubscription().getNextBillingDate() != null) {
                    String nextDueDate = simpleDateFormat.format(patient.getSubscription().getNextBillingDate());
                    dueTextView.setText(nextDueDate);
                } else {
                    dueTextView.setText("N/A");
                }

                // Amount
                Region region = App.getInstance().getCurrentRegion();
                Double price = (patient.getPromotion() != null) ? patient.getSubscription().getPrice() : patient.getPlan().getPriceForPatient(patient);
                String amountText = region.getFormattedPriceString(price);
                amountTextView.setText(amountText);
            }
        }

        // Promotion Code
        if (patient.getPromotion() != null) {
            codeTextView.setText(patient.getPromotion().getName());
            codeTextView.setVisibility(View.VISIBLE);
            codeTitleTextView.setVisibility(View.VISIBLE);
        } else {
            codeTextView.setVisibility(View.GONE);
            codeTitleTextView.setVisibility(View.GONE);
        }

        listView.addHeaderView(headerView, null, false);
    }


    private final RequestListener<Transaction[]> transactionListRequestListener = new
            RequestListenerErrorProcessed<Transaction[]>() {
                @Override
                public void onRequestComplete(Response<Transaction[]> response, String errorMessage) {
                    setProgressBarVisible(false);
                    if (!getActivity().isFinishing()) {
                        if (response.isSuccess()) {
                            final Transaction[] newTransactions = response.getData();
                            // sort by date
                            Arrays.sort(newTransactions, Collections.reverseOrder());
                            transactions.clear();
                            transactions.addAll(Arrays.asList(newTransactions));
                            adapter.notifyDataSetChanged();
                        } else {
                            final String userResponseError = response.getUserResponseError();
                            if (!TextUtils.isEmpty(userResponseError)) {
                                AlertDialogHelper.getInstance().showMessage(getActivity(), userResponseError);
                            }
                        }
                    }
                }
            };
}
