package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.InvitationReceivedDataResponse;
import com.axsystech.excelicare.data.responses.InvitationReceivedDetails;
import com.axsystech.excelicare.db.models.CircleInvitationSentDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageDetailsFragment;
import com.axsystech.excelicare.loaders.CircleInvitationSentDbLoader;
import com.axsystech.excelicare.loaders.GetCircleInvitationSentFromDbLoader;
import com.axsystech.excelicare.loaders.InvitationSentLoader;
import com.axsystech.excelicare.loaders.InvitationSentSearchLoader;
import com.axsystech.excelicare.loaders.RemoveInvitationLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.DateFormatUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by vvydesai on 8/31/2015.
 */
public class InvitationSentFragment extends BaseFragment implements View.OnClickListener, TextWatcher {

    private String TAG = InvitationReceivedFragment.class.getSimpleName();
    private InvitationSentFragment mFragment;
    private MainContentActivity mActivity;

    private LayoutInflater mLayoutInflater;

    @InjectView(R.id.mediaListView)
    private ListView mInvitationsReceivedListView;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    @InjectView(R.id.filter_btn)
    private ImageButton filterBtn;

    @InjectView(R.id.help_btn)
    private ImageButton helpBtn;

    //CircleInvitationSentDbModel rowObject;

    //private InvitationReceivedDetails mInvitationDetails;
    private ArrayList<InvitationReceivedDetails> mMembersInvitationSent;
    private CircleInvitationSentDbModel mCirclesInvitationSentDbModel;

    private CircleInvitationReceivedAdapter circleInvitationReceivedAdapter;

//    private ArrayList<InvitationReceivedDetails> mMembersInvitationSentSearch = new ArrayList<InvitationReceivedDetails>();
//    private ArrayList<InvitationReceivedDetails> mMembersInvitationSentPending = new ArrayList<InvitationReceivedDetails>();
//    private ArrayList<InvitationReceivedDetails> mMembersInvitationSentAccepted = new ArrayList<InvitationReceivedDetails>();
//    private ArrayList<InvitationReceivedDetails> mMembersInvitationSentDeclined = new ArrayList<InvitationReceivedDetails>();

    private ArrayList<CircleInvitationSentDbModel> mDbMembersInvitationSentPending = new ArrayList<CircleInvitationSentDbModel>();
    private ArrayList<CircleInvitationSentDbModel> mDbMembersInvitationSentAccepted = new ArrayList<CircleInvitationSentDbModel>();
    private ArrayList<CircleInvitationSentDbModel> mDbMembersInvitationSentDeclined = new ArrayList<CircleInvitationSentDbModel>();

    //private ArrayList<CircleInvitationSentDbModel> completeDbMembersInvitationSentPending = new ArrayList<CircleInvitationSentDbModel>();
    //private ArrayList<CircleInvitationSentDbModel> completeDbMembersInvitationSentAccepted = new ArrayList<CircleInvitationSentDbModel>();
    //private ArrayList<CircleInvitationSentDbModel> completeDbMembersInvitationSentDeclined = new ArrayList<CircleInvitationSentDbModel>();

    public static ArrayList<CircleInvitationSentDbModel> dbCirclesInvitationSentDbModel;

    private static final String INVITATION_SENT = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.INVITATION_SENT";
    private static final String INVITATION_DELETE = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationReceivedFragment.INVITATION_DELETE";
    private static final String INVITATION_SENT_SEARCH = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationSentFragment.INVITATION_SENT_SEARCH";
    private static final String SAVE_INVITATION_SENT_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationSentFragment.SAVE_INVITATION_SENT_DB_LOADER";
    private static final String GET_INVITATION_SENT_FROM_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.InvitationSentFragment.GET_INVITATION_SENT_FROM_DB_LOADER";
    private int pageIndex = 0;
    private int PAGE_SIZE = 10;


    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), getString(R.string.title_invitations_sent_to)));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_invitation_received, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        readIntentArgs();
        getCirclesListFromServer(true);
        preloadData();
        addEventListener();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    private void addEventListener() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);
        filterBtn.setOnClickListener(this);
        helpBtn.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String searchEdit = null;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        try {
                            searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        InvitationSentSearchLoader msgSentLoader = null;
                        msgSentLoader = new InvitationSentSearchLoader(mActivity, mActiveUser.getToken(),
                                searchEdit);
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_SENT_SEARCH), msgSentLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
                return false;
            }
        });

        mInvitationsReceivedListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount)) {

                    if (dbCirclesInvitationSentDbModel != null && dbCirclesInvitationSentDbModel.size() != 0) {
                        getCirclesListFromServer(false);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
        }
    }

    private void getCirclesListFromServer(boolean showProgress) {
        if (CommonUtils.hasInternet()) {
            if (showProgress)
                displayProgressLoader(false);
            pageIndex = pageIndex + 1;
            InvitationSentLoader invitationSentLoader = new InvitationSentLoader(mActivity, mActiveUser.getToken(), "", "0", "", pageIndex, PAGE_SIZE, "");
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_SENT), invitationSentLoader);
            }
        } else {

            final GetCircleInvitationSentFromDbLoader getCircleInvitationSentFromDbLoader = new GetCircleInvitationSentFromDbLoader(mActivity);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_INVITATION_SENT_FROM_DB_LOADER), getCircleInvitationSentFromDbLoader);
            }
        }
    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_members));
        circleInvitationReceivedAdapter = new CircleInvitationReceivedAdapter(null);
        mInvitationsReceivedListView.setAdapter(circleInvitationReceivedAdapter);
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
        synchronized (this) {
            if (id == getLoaderHelper().getLoaderId(INVITATION_SENT)) {
                if (result != null && result instanceof InvitationReceivedDataResponse) {
                    final InvitationReceivedDataResponse invitationReceivedDataResponse = (InvitationReceivedDataResponse) result;

                    if (invitationReceivedDataResponse.getDataObject() != null) {
                        mMembersInvitationSent = invitationReceivedDataResponse.getDataObject().getDataDetailslist();

                        // separate Pending & Accepted invitations from total invitations list
                        if (mMembersInvitationSent != null && mMembersInvitationSent.size() > 0) {

                            dbCirclesInvitationSentDbModel = new ArrayList<CircleInvitationSentDbModel>();

                            for (int i = 0; i < mMembersInvitationSent.size(); i++) {
                                InvitationReceivedDetails indexedObj = mMembersInvitationSent.get(i);
                                CircleInvitationSentDbModel circleInvitationSentDbModel = new CircleInvitationSentDbModel();

                                try {
                                    circleInvitationSentDbModel.setInvioteId(indexedObj.getInvite_Id());
                                    circleInvitationSentDbModel.setMessageReceivedStatusSlu(indexedObj.getMessageReceivedStatus_SLU());
                                    circleInvitationSentDbModel.setSenderName(indexedObj.getSenderName());
                                    circleInvitationSentDbModel.setSenderForeName(indexedObj.getSenderForeName());
                                    circleInvitationSentDbModel.setSenderSurName(indexedObj.getSenderSurName());
                                    circleInvitationSentDbModel.setSendDate(indexedObj.getSendDate());
                                    circleInvitationSentDbModel.setRoleSlu(indexedObj.getRole_SLU());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                dbCirclesInvitationSentDbModel.add(circleInvitationSentDbModel);
                            }
                            final CircleInvitationSentDbLoader circleInvitationSentDbLoader = new CircleInvitationSentDbLoader(mActivity, dbCirclesInvitationSentDbModel);
                            if (isAdded() && getView() != null) {
                                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_INVITATION_SENT_DB_LOADER), circleInvitationSentDbLoader);
                            }
                        }

                        // By default display Pending invitations list
//                    if (circleInvitationReceivedAdapter != null) {
//                        circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentPending);
//                    }

                    } else {
                        showToast(R.string.circles_no_invitations_sent);
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(INVITATION_DELETE)) {
                if (result != null && result instanceof BaseResponse) {
                    final BaseResponse baseResponseRemove = (BaseResponse) result;

                    if (baseResponseRemove.isSuccess()) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
//                                if (completeDbMembersInvitationSentDeclined.contains(mCirclesInvitationSentDbModel)) {
//                                    completeDbMembersInvitationSentDeclined.remove(mCirclesInvitationSentDbModel);
//                                    mDbMembersInvitationSentDeclined.remove(mCirclesInvitationSentDbModel);
//                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedDeclined);
//                                } else if (completeDbMembersInvitationSentAccepted.contains(mCirclesInvitationSentDbModel)) {
//                                    completeDbMembersInvitationSentAccepted.remove(mCirclesInvitationSentDbModel);
//                                    mDbMembersInvitationSentAccepted.remove(mCirclesInvitationSentDbModel);
//                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedAccepted);
//                                } else if (completeDbMembersInvitationSentPending.contains(mCirclesInvitationSentDbModel)) {
//                                    completeDbMembersInvitationSentPending.remove(mCirclesInvitationSentDbModel);
//                                    mDbMembersInvitationSentPending.remove(mCirclesInvitationSentDbModel);
//                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedPending);
//                                }

                                if (mDbMembersInvitationSentDeclined.contains(mCirclesInvitationSentDbModel)) {
                                    mDbMembersInvitationSentDeclined.remove(mCirclesInvitationSentDbModel);
                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedDeclined);
                                } else if (mDbMembersInvitationSentAccepted.contains(mCirclesInvitationSentDbModel)) {
                                    mDbMembersInvitationSentAccepted.remove(mCirclesInvitationSentDbModel);
                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedAccepted);
                                } else if (mDbMembersInvitationSentPending.contains(mCirclesInvitationSentDbModel)) {
                                    mDbMembersInvitationSentPending.remove(mCirclesInvitationSentDbModel);
                                    //circleInvitationReceivedAdapter.updateData(completeDbCirclesInvitationReceivedPending);
                                }
                                mCirclesInvitationSentDbModel = null;
                                dbCirclesInvitationSentDbModel.clear();
                                mDbMembersInvitationSentAccepted.clear();
                                mDbMembersInvitationSentPending.clear();
                                mDbMembersInvitationSentDeclined.clear();
                                pageIndex = 0;
//                                completeDbMembersInvitationSentDeclined.clear();
//                                completeDbMembersInvitationSentAccepted.clear();
//                                completeDbMembersInvitationSentPending.clear();
                                //completeDbCirclesInvitationReceivedDbModel.remove(mCirclesInvitationReceivedDbModel);
                                circleInvitationReceivedAdapter.notifyDataSetChanged();

                                showToast(baseResponseRemove.getMessage());
                                if (CommonUtils.hasInternet()) {
                                    displayProgressLoader(false);
                                    InvitationSentLoader invitationSentLoader = new InvitationSentLoader(mActivity, mActiveUser.getToken(), "", "0", "", pageIndex, PAGE_SIZE, "");
                                    if (isAdded() && getView() != null) {
                                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_SENT), invitationSentLoader);
                                    }
                                } else {
                                    showToast(R.string.error_no_network);
                                }
                            }
                        });
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(INVITATION_SENT_SEARCH)) {
                if (result != null && result instanceof InvitationReceivedDataResponse) {
                    InvitationReceivedDataResponse mMsgSentResponse = (InvitationReceivedDataResponse) result;

                    if (mMsgSentResponse.getDataObject() != null) {
                        final ArrayList<InvitationReceivedDetails> searchResultsDataObjectsAL = mMsgSentResponse.getDataObject().getDataDetailslist();
                        if (searchResultsDataObjectsAL != null && searchResultsDataObjectsAL.size() > 0) {
                            // update to list adapter with search items
                            if (circleInvitationReceivedAdapter != null) {
                                circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentPending);
                            }
                        } else {
                            showToast(R.string.no_search_results_found);
                        }
                    }
                }
            } else if (id == getLoaderHelper().getLoaderId(SAVE_INVITATION_SENT_DB_LOADER)) {
                if (result != null && result instanceof ArrayList) {
                    dbCirclesInvitationSentDbModel = (ArrayList<CircleInvitationSentDbModel>) result;
                    if (dbCirclesInvitationSentDbModel != null && dbCirclesInvitationSentDbModel.size() > 0) {
                        for (int j = 0; j < dbCirclesInvitationSentDbModel.size(); j++) {
                            CircleInvitationSentDbModel indexedObj = dbCirclesInvitationSentDbModel.get(j);
                            if (mMembersInvitationSent.size() > 0)
                                if (!TextUtils.isEmpty(mMembersInvitationSent.get(j).getMessageReceivedStatus_SLU())) {
                                    if (mMembersInvitationSent.get(j).getMessageReceivedStatus_SLU().
                                            trim().equalsIgnoreCase(AxSysConstants.PENDING)) {
                                        mDbMembersInvitationSentPending.add(indexedObj);
                                        //completeDbMembersInvitationSentPending.addAll(mDbMembersInvitationSentPending);
                                    } else if (mMembersInvitationSent.get(j).getMessageReceivedStatus_SLU().
                                            trim().equalsIgnoreCase(AxSysConstants.ACCEPTED)) {
                                        mDbMembersInvitationSentAccepted.add(indexedObj);
                                        //completeDbMembersInvitationSentAccepted.addAll(mDbMembersInvitationSentAccepted);
                                    } else if (mMembersInvitationSent.get(j).getMessageReceivedStatus_SLU().
                                            trim().equalsIgnoreCase(AxSysConstants.DECLINED)) {
                                        mDbMembersInvitationSentDeclined.add(indexedObj);
                                        //completeDbMembersInvitationSentDeclined.addAll(mDbMembersInvitationSentDeclined);
                                    }
                                }
                        }
                        // By default display Pending invitations list
                        if (circleInvitationReceivedAdapter != null) {
                            circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentPending);
                        }
                    }

                }
            } else if (id == getLoaderHelper().getLoaderId(GET_INVITATION_SENT_FROM_DB_LOADER)) {
                if (result != null && result instanceof ArrayList) {
                    dbCirclesInvitationSentDbModel = (ArrayList<CircleInvitationSentDbModel>) result;
                    // By default display Pending invitations list
                    if (dbCirclesInvitationSentDbModel != null && dbCirclesInvitationSentDbModel.size() > 0) {
                        for (int j = 0; j < dbCirclesInvitationSentDbModel.size(); j++) {
                            CircleInvitationSentDbModel indexedObj = dbCirclesInvitationSentDbModel.get(j);
                            try {
                                if (!TextUtils.isEmpty(dbCirclesInvitationSentDbModel.get(j).getMessageReceivedStatusSlu())) {
                                    if (dbCirclesInvitationSentDbModel.get(j).getMessageReceivedStatusSlu().
                                            trim().equalsIgnoreCase(AxSysConstants.PENDING)) {
                                        mDbMembersInvitationSentPending.add(indexedObj);
                                    } else if (dbCirclesInvitationSentDbModel.get(j).getMessageReceivedStatusSlu().
                                            trim().equalsIgnoreCase(AxSysConstants.ACCEPTED)) {
                                        mDbMembersInvitationSentAccepted.add(indexedObj);
                                    } else if (dbCirclesInvitationSentDbModel.get(j).getMessageReceivedStatusSlu().
                                            trim().equalsIgnoreCase(AxSysConstants.DECLINED)) {
                                        mDbMembersInvitationSentDeclined.add(indexedObj);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        // By default display Pending invitations list
                        if (circleInvitationReceivedAdapter != null) {
                            circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentPending);
                        }
                    } else {
                        showToast(R.string.no_data);
                    }

                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        if(dbCirclesInvitationSentDbModel!=null)
//        dbCirclesInvitationSentDbModel.clear();
//        mDbMembersInvitationSentAccepted.clear();
//        mDbMembersInvitationSentPending.clear();
//        mDbMembersInvitationSentDeclined.clear();
//        completeDbMembersInvitationSentDeclined.clear();
//        completeDbMembersInvitationSentAccepted.clear();
//        completeDbMembersInvitationSentPending.clear();
//        pageIndex = 0;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        getCirclesListFromServer();
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                break;

            case R.id.filter_btn:
                displayStatusDialog();
                break;

            case R.id.help_btn:
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    private void displayStatusDialog() {
        final CharSequence[] items = {
                getString(R.string.dialog_text_pending),
                getString(R.string.dialog_text_accepted), getString(R.string.dialog_text_declined)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.picker_title_filter_by));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(getString(R.string.dialog_text_pending))) {
                    circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentPending);
                } else if (items[item].equals(getString(R.string.dialog_text_accepted))) {
                    circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentAccepted);
                } else if (items[item].equals(getString(R.string.dialog_text_declined))) {
                    circleInvitationReceivedAdapter.updateData(mDbMembersInvitationSentDeclined);
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private class CircleInvitationReceivedAdapter extends BaseAdapter {

        private ArrayList<CircleInvitationSentDbModel> membersInvitationsSent;

        public CircleInvitationReceivedAdapter(ArrayList<CircleInvitationSentDbModel> membersInvitationsCircle) {
            this.membersInvitationsSent = membersInvitationsCircle;
        }

        @Override
        public int getCount() {
            return membersInvitationsSent != null && membersInvitationsSent.size() > 0 ? membersInvitationsSent.size() : 0;
        }

        @Override
        public CircleInvitationSentDbModel getItem(int position) {
            return membersInvitationsSent.get(position);
        }

        public void updateData(ArrayList<CircleInvitationSentDbModel> membersInvitationsCircle) {
            this.membersInvitationsSent = membersInvitationsCircle;
            notifyDataSetChanged();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final CircleInvitationSentDbModel rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.fragment_invitation_received_row, viewGroup, false);
            }

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
            Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

            ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImageView);
            Picasso.with(getActivity()).load(R.drawable.user_icon).transform(new CircleTransform()).into(memberImageView);
            TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberNameTextView);
            TextView memberRoleTypeTextView = (TextView) convertView.findViewById(R.id.memberTextView);
            TextView dateTypeTextView = (TextView) convertView.findViewById(R.id.dateTextView);
            final TextView statusTextView = (TextView) convertView.findViewById(R.id.statusTextView);

            memberNameTextView.setTypeface(bold);
            memberRoleTypeTextView.setTypeface(regular);
            dateTypeTextView.setTypeface(bold);
            statusTextView.setTypeface(bold);

            try {
                memberNameTextView.setText(rowObject.getSenderForeName() + " " + rowObject.getSenderSurName());
                memberRoleTypeTextView.setText(rowObject.getRoleSlu());
                String displayDate = DateFormatUtils.formatDefaultDate(rowObject.getSendDate(), "dd/MM/yyyy");
               /* //Split Date
                String string = displayDate;
                String[] parts = string.split("-");
                String part1 = parts[0]; // month
                String part2 = parts[1]; // date
                String part3 = parts[2]; // year
                dateTypeTextView.setText(part1 + " " + part2 + "," + " " + part3);*/
                dateTypeTextView.setText(displayDate);
                //dateTypeTextView.setText(rowObject.getSendDate());
                if (rowObject.getRoleSlu() != null) {
                    if (rowObject.getMessageReceivedStatusSlu().equals(AxSysConstants.PENDING)) {
                        statusTextView.setText("Pending");
                    } else if (rowObject.getMessageReceivedStatusSlu().equals(AxSysConstants.ACCEPTED)) {
                        statusTextView.setText("Accepted");
                    } else if (rowObject.getMessageReceivedStatusSlu().equals(AxSysConstants.DECLINED)) {
                        statusTextView.setText("Declined");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                        final FragmentTransaction ft = activityFragmentManager.beginTransaction();
//                        pageIndex=0;
//                        completeDbMembersInvitationSentDeclined.clear();
//                        completeDbMembersInvitationSentAccepted.clear();
//                        completeDbMembersInvitationSentPending.clear();
//                        dbCirclesInvitationSentDbModel.clear();
//                        circleInvitationReceivedAdapter = new CircleInvitationReceivedAdapter(completeDbMembersInvitationSentPending);
                        final InvitationDetailScreen invitationDetailScreen = new InvitationDetailScreen();
                        Bundle bundle = new Bundle();
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_MEMBER_NAME, rowObject.getSenderForeName() + " " + rowObject.getSenderSurName());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_DATE, DateFormatUtils.formatDefaultDate(rowObject.getSendDate(), "dd/MM/yyyy"));
                        //bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_TEXT, rowObject.getMessageText());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_MESSAGE_STATUS, statusTextView.getText().toString());
                        bundle.putString(AxSysConstants.EXTRA_BOOK_INVITATION_SENT, "Invitation Sent");
                        bundle.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                        bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Invitation Details");
                        invitationDetailScreen.setArguments(bundle);
                        ft.add(R.id.container_layout, invitationDetailScreen, "InvitationDetailScreen");
                        ft.addToBackStack(MessageDetailsFragment.class.getSimpleName());
                        ft.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());
                    alert.setTitle(getString(R.string.text_alert));
                    alert.setMessage(getString(R.string.confirmation_to_delete_invitation));
                    alert.setPositiveButton(getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            pageIndex=0;
//                            completeDbMembersInvitationSentDeclined.clear();
//                            completeDbMembersInvitationSentAccepted.clear();
//                            completeDbMembersInvitationSentPending.clear();
//                            mMembersInvitationSentPending.clear();
//                            mMembersInvitationSentAccepted.clear();
//                            mMembersInvitationSentDeclined.clear();
//                            dbCirclesInvitationSentDbModel.clear();
//                            //TODO: set adapter accordingly . ..
//                            circleInvitationReceivedAdapter.notifyDataSetChanged();
                            try {
                                if (CommonUtils.hasInternet()) {
                                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                                    mCirclesInvitationSentDbModel = rowObject;
                                    displayProgressLoader(false);
                                    RemoveInvitationLoader removeInvitationLoader = new RemoveInvitationLoader(mActivity, mActiveUser.getToken(),
                                            rowObject.getInvioteId());
                                    if (mFragment.isAdded() && mFragment.getView() != null) {
                                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(INVITATION_DELETE), removeInvitationLoader);
                                    }
                                } else {
                                    showToast(R.string.error_no_network);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alert.setNegativeButton(getString(R.string.text_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                    return false;
                }
            });
            return convertView;
        }
    }
}