package com.iversecomics.client.util;

public class Position {
    private int current = 0;
    private int size = 0;

    public Position(int size) {
        this.current = 0;
        this.size = size;
    }

    public void doGoto(int index) {
        if (index >= size) {
            current = this.size - 1;
        } else {
            current = index;
        }
    }

    public int doNext() {
        if (hasNext()) {
            current++;
        }
        return current;
    }

    public int doPrev() {
        if (hasPrev()) {
            current--;
        }
        return current;
    }

    public int getCurrent() {
        return current;
    }

    public int getNext() {
        if (hasNext()) {
            return current + 1;
        }
        throw new ArrayIndexOutOfBoundsException("Cannot get next, current is at end");
    }

    public int getPrev() {
        if (hasPrev()) {
            return current - 1;
        }
        throw new ArrayIndexOutOfBoundsException("Cannot get previous, current is at start");
    }

    public boolean hasNext() {
        return (current < this.size);
    }

    public boolean hasPrev() {
        return (current > 0);
    }

    public void reset(int size) {
        this.size = size;
        if (this.current > size) {
            this.current = size;
        }
    }
}
