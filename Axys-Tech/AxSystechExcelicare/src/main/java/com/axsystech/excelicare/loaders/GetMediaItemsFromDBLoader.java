package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetMediaItemsFromDBLoader extends DataAsyncTaskLibLoader<List<MediaItemDbModel>> {

    public GetMediaItemsFromDBLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<MediaItemDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<MediaItemDbModel, Long> mediaItemDao = helper.getDao(MediaItemDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<MediaItemDbModel>>() {
                @Override
                public List<MediaItemDbModel> call() throws Exception {
                    return mediaItemDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
