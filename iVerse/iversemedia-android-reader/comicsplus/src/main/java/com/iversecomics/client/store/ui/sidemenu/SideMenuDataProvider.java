package com.iversecomics.client.store.ui.sidemenu;

import android.content.Context;
import android.database.Cursor;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.db.GroupsTable;
import com.iversecomics.client.store.db.PromotionTable;
import com.iversecomics.client.store.db.PublishersTable;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.store.ui.sidemenu.SideMenuItemInfo.ItemType;

import java.util.ArrayList;
import java.util.List;

public class SideMenuDataProvider {

    public static final String TAG = SideMenuDataProvider.class.getSimpleName();
    public static final int SECTIONS = 4;

    public static SideMenuDataProvider g_instance;
    public Context context;
    private boolean mPublishersInsteadOfTitlesInSideMenu = false;

    public static SideMenuDataProvider initialize(Context ctx, boolean publishersInsteadOfTitlesInSideMenu) {
        if (g_instance == null)
            g_instance = new SideMenuDataProvider();

        if (ctx != null)
            g_instance.context = ctx;

        g_instance.setPublishersInsteadOfTitlesInSideMenu(publishersInsteadOfTitlesInSideMenu);
        return g_instance;
    }

    public static SideMenuDataProvider getInstance(Context ctx) {
        if (g_instance == null) {
            initialize(ctx, false);
        }
        return g_instance;
    }

    public void setPublishersInsteadOfTitlesInSideMenu(boolean publishersInsteadOfTitlesInSideMenu) {
        this.mPublishersInsteadOfTitlesInSideMenu = publishersInsteadOfTitlesInSideMenu;
    }

    public List<SideMenuSection> getAllData() {
        List<SideMenuSection> res = new ArrayList<SideMenuSection>();

        for (int i = 0; i < SECTIONS; i++) {
            res.add(getOneSection(i));
        }

        return res;
    }

    /**
     * Gets data for a "Digests" sub-menu.
     *
     * @param parentGroupId
     * @return List<SideMenuSection>
     */
    public List<SideMenuSection> getDataForParentGroup(String parentGroupId) {

        String title = context.getString(R.string.titles_header);
        ArrayList<SideMenuItemInfo> resultList = new ArrayList<SideMenuItemInfo>();
        ComicStore comicStore = IverseApplication.getApplication().getComicStore();
        Cursor cursor = comicStore.getDatabaseApi().getCursorGroupsByParentGroupID(parentGroupId);
        while (cursor.moveToNext()) {
            Group group = GroupsTable.fromCursor(cursor);
            SideMenuItemInfo sideMenuItemInfo = new SideMenuItemInfo(ItemType.ItemTitles, group.getName(), group.getImageUrl(), (Object) group);
            resultList.add(sideMenuItemInfo);
        }
        cursor.close();

        SideMenuSection section = new SideMenuSection(title, resultList);

        // create a single section for our group sub-menu
        List<SideMenuSection> res = new ArrayList<SideMenuSection>();
        res.add(section);
        return res;
    }


    /**
     * Gets data for a publisher's sub-menu.
     *
     * @param publisherId
     * @return List<SideMenuSection>
     */
    public List<SideMenuSection> getDataForPublisherGroup(String publisherId) {

        String title = context.getString(R.string.titles_header);
        ArrayList<SideMenuItemInfo> resultList = new ArrayList<SideMenuItemInfo>();
        ComicStore comicStore = IverseApplication.getApplication().getComicStore();
        Cursor cursor = comicStore.getDatabaseApi().getCursorGroupsByPublisher(publisherId);
        while (cursor.moveToNext()) {
            Group group = GroupsTable.fromCursor(cursor);
            SideMenuItemInfo sideMenuItemInfo = new SideMenuItemInfo(ItemType.ItemTitles, group.getName(), group.getImageUrl(), (Object) group);
            resultList.add(sideMenuItemInfo);
        }
        cursor.close();

        SideMenuSection section = new SideMenuSection(title, resultList);

        // create a single section for our group sub-menu
        List<SideMenuSection> res = new ArrayList<SideMenuSection>();
        res.add(section);
        return res;
    }


    public List<SideMenuItemInfo> getFlattenedData() {
        List<SideMenuItemInfo> res = new ArrayList<SideMenuItemInfo>();

        for (int i = 0; i < SECTIONS; i++) {
            res.addAll(getOneSection(i).second);
        }

        return res;
    }

    public static final String OP_LOGIN = "login";
    public static final String OP_SIGNUP = "signup";
    public static final String OP_FEEDBACK = "feedback";

    public static final String OP_LOGOUT = "logout";
    public static final String OP_MYCOMICS = "mycomics";
    public static final String OP_MYPURCHASES = "mypurchases";
    public static final String OP_MYDOWNLOADS = "mydownloads";
    public static final String OP_XPOINTS = "points";

    public static final String OP_FEATURED = "featured";
    public static final String OP_DEMAND = "ondemand";
    public static final String OP_TOPPAID = "toppaid";
    public static final String OP_TOPFREE = "topfree";
    public static final String OP_NEWRELEASES = "newreleases";
    public static final String OP_SETTINGS = "settings";


    public SideMenuSection getOneSection(int index) {
        String title;
        ArrayList<SideMenuItemInfo> resultList = new ArrayList<SideMenuItemInfo>();

        switch (index) {

            case 0: {
                String emailAddress = IverseApplication.getApplication().getOwnership().getEmailAddress();
                title = context.getString(R.string.my_account_header);

                if (emailAddress == null || emailAddress.length() == 0) {
                    //not logged in
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.log_in_button), null, OP_LOGIN));
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.sign_up_positive_button), null, OP_SIGNUP));
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.app_feedback_button), null, OP_FEEDBACK));
                } else {
                    //logged in
                    resultList.add(new SideMenuItemInfo(ItemType.Account, emailAddress, null, OP_LOGOUT));
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.my_comics), null, OP_MYCOMICS));
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.my_purchases), null, OP_MYPURCHASES, true));
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.download), null, OP_MYDOWNLOADS, true));
                    resultList.add(new SideMenuItemInfo(ItemType.Account, context.getString(R.string.app_feedback_button), null, OP_FEEDBACK));

                    int userPoints = IverseApplication.getApplication().getOwnership().getUserPoints();

                    if(AppConstants.ENABLE_TAPJOY) {
                        String points = userPoints + context.getResources().getString(R.string.points);
                        resultList.add(new SideMenuItemInfo(ItemType.Account, points, null, OP_XPOINTS, true));
                    }
                }
                break;
            }

            // promotions
            case 1: {
                title = context.getString(R.string.promotions_header);
                ComicStore comicStore = IverseApplication.getApplication().getComicStore();
                Cursor cursor = comicStore.getDatabaseApi().getCursorPromotion();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Promotion promotion = PromotionTable.fromCursor(cursor);
                    resultList.add(new SideMenuItemInfo(ItemType.Promotions, promotion.getName(), null, (Object) promotion));
                    cursor.moveToNext();
                }
                cursor.close();
                break;
            }

            // navigation
            case 2: {
                title = context.getString(R.string.navigation_header);
                resultList.add(new SideMenuItemInfo(ItemType.Navigation, context.getString(R.string.featured), null, OP_FEATURED));
                //For Archie - ondemand service enabled for Comic Plus - ondemand disabled.
                if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                    resultList.add(new SideMenuItemInfo(ItemType.Navigation, context.getString(R.string.ondemand), null, OP_DEMAND));
                }
                resultList.add(new SideMenuItemInfo(ItemType.Navigation, context.getString(R.string.top_paid), null, OP_TOPPAID));
                resultList.add(new SideMenuItemInfo(ItemType.Navigation, context.getString(R.string.top_free), null, OP_TOPFREE));
                resultList.add(new SideMenuItemInfo(ItemType.Navigation, context.getString(R.string.new_releases), null, OP_NEWRELEASES));
                resultList.add(new SideMenuItemInfo(ItemType.Navigation, context.getString(R.string.settings), null, OP_SETTINGS, true));
                break;
            }

            // titles(Archie variant) OR publishers (ComicsPlus variant)
            case 3: {
                ComicStore comicStore = IverseApplication.getApplication().getComicStore();

                // publishers
                if (mPublishersInsteadOfTitlesInSideMenu) {
                    title = context.getString(R.string.publishers_header);
                    Cursor cursor = comicStore.getDatabaseApi().getCursorPublishersAll();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        Publisher publisher = PublishersTable.fromCursor(cursor);

                        SideMenuItemInfo sideMenuItemInfo = new SideMenuItemInfo(ItemType.ItemTitles, publisher.getName(), publisher.getImageUrl(), (Object) publisher);

                        // Determine if publishers has sub-groups
                        // have sub-groups can be drilled into via the side menu.
                        Cursor subGroupCursor = comicStore.getDatabaseApi().getCursorGroupsByPublisher(publisher.getPublisherId());
                        if (subGroupCursor.getCount() > 0)
                            sideMenuItemInfo.setHasSubMenu(true);
                        subGroupCursor.close();

                        resultList.add(sideMenuItemInfo);
                        cursor.moveToNext();
                    }
                    cursor.close();
                }
                // titles
                else {
                    title = context.getString(R.string.titles_header);
                    Cursor cursor = comicStore.getDatabaseApi().getCursorGroupsAll();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        Group group = GroupsTable.fromCursor(cursor);

                        SideMenuItemInfo sideMenuItemInfo = new SideMenuItemInfo(ItemType.ItemTitles, group.getName(), group.getImageUrl(), (Object) group);

                        // Determine if group has sub-groups, for example: "Digests" and "Double Digests"
                        // have sub-groups that can be drilled into via the side menu.
                        Cursor subGroupCursor = comicStore.getDatabaseApi().getCursorGroupsByParentGroupID(group.getGroupId());
                        if (subGroupCursor.getCount() > 0)
                            sideMenuItemInfo.setHasSubMenu(true);
                        subGroupCursor.close();

                        resultList.add(sideMenuItemInfo);
                        cursor.moveToNext();
                    }
                    cursor.close();

                }

                break;
            }
            default:
                throw new IllegalArgumentException("Segment must be >= 0 and < 3, but was " + index);
        }
        return new SideMenuSection(title, resultList);
    }

}
