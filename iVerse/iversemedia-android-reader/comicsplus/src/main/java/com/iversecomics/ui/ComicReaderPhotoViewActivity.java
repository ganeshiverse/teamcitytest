package com.iversecomics.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.ShareManager;
import com.iversecomics.client.Storage;
import com.iversecomics.client.comic.AsyncMyComicSourceOpenTask;
import com.iversecomics.client.comic.AsyncMyComicSourceOpenTask.OnAsyncMyComicSourceOpenTaskListener;
import com.iversecomics.client.comic.ComicBookmark;
import com.iversecomics.client.comic.ComicBundlePreviewAdapter;
import com.iversecomics.client.comic.ComicBundleSourceAdapter;
import com.iversecomics.client.comic.ComicMode;
import com.iversecomics.client.comic.IComicSourceAdapter;
import com.iversecomics.client.comic.IComicSourceListener;
import com.iversecomics.client.comic.IMyComicSourceAdapter;
import com.iversecomics.client.comic.viewer.ComicBundleOnDemandAdapter;
import com.iversecomics.client.comic.viewer.ComicNav;
import com.iversecomics.client.comic.viewer.example.ComicInformationActivity;
import com.iversecomics.client.comic.viewer.example.ComicPreferencesActivity;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.ComicStoreException;
import com.iversecomics.client.store.OnDemandOfflineQueue;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.client.util.UIUtil;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.util.crashreporter.PostMortemReportExceptionHandler;
import com.iversecomics.util.ui.ABaseTransformer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.Future;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ComicReaderPhotoViewActivity extends FragmentActivity implements
        OnAsyncMyComicSourceOpenTaskListener, IComicSourceListener {

    public static final String EXTRA_COMIC_BUNDLE_NAME = "com.iverse.COMIC_BUNDLE_NAME";
    public static final String EXTRA_PREVIEW = "PREVIEW";
    public static final String EXTRA_ONDEMAND_PAGES = "EXTRA_ONDEMAND_PAGES";

    private static final String CURRENT_PAGE = "com.iverse.CURRENT_PAGE";

    private static final Logger LOG = LoggerFactory.getLogger(ComicReaderPhotoViewActivity.class);

    private static final int INFORMATION = 1;
    private static final int NAVIGATION = 2;
    private static final int PREFERENCES = 3;

    IverseApplication mIverseApp;

    private ViewPager mViewPager;
    private View mLoadingOverlay;
    private SeekBar mSeekBar;
    private RelativeLayout mNavOverlay;
    private TextView mCurrentPageTextView;

    private IMyComicSourceAdapter mComicSource;

    private MyComic mMyComic;
    private Comic mComic;
    private ComicBookmark mBookmark;
    boolean previewMode;
    boolean streamingMode;
    private AsyncMyComicSourceOpenTask previewDownloadTask;

    RelativeLayout viewInfoBackContainer = null;
    View viewInfoBack = null;
    Vector<View> listInfoViewsPortrait = null;
    Vector<View> listInfoViewsLandscape = null;
    boolean bShowInfo = false;
    public static boolean isWaidForamt = false;
    protected PostMortemReportExceptionHandler mDamageReport = new PostMortemReportExceptionHandler(this);
    Future<?> serverConfigFuture;
    IverseApplication mApp;
    private ComicReaderPhotoViewActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(AppConstants.ENABLE_CRASH_REPORT) {
            mDamageReport.initialize();
        }
        setContentView(R.layout.activity_comic_reader_photo_view);
        mActivity= this;
        final Intent intent = getIntent();
        mApp = IverseApplication.getApplication();
        final String comicBundleName = intent.getStringExtra(EXTRA_COMIC_BUNDLE_NAME);

        previewMode = intent.getBooleanExtra(EXTRA_PREVIEW, false);
        streamingMode = intent.hasExtra(EXTRA_ONDEMAND_PAGES);

        mIverseApp = (IverseApplication) getApplication();

        mLoadingOverlay = findViewById(R.id.overlay_loading);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);

        String comicName;
        int pageIdx = 0;

        if (previewMode) {
            mComic = mIverseApp.getComicStore().getDatabaseApi().getComicByBundleName(comicBundleName);

            if (mComic == null) {
                Toast.makeText(this, getResources().getString(R.string.could_not_find_comic) + comicBundleName, Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            mComicSource = new ComicBundlePreviewAdapter(mComic);
            mBookmark = ComicBookmark.fromModeAndIndex(ComicMode.PREVIEW, 0);
            comicName = mComic.getName();
        } else if (streamingMode) {
            ArrayList<String> pages = intent.getStringArrayListExtra(EXTRA_ONDEMAND_PAGES);
            mComicSource = new ComicBundleOnDemandAdapter(pages);
            mComic = mIverseApp.getComicStore().getDatabaseApi().getComicByBundleName(comicBundleName);

            if (mComic == null) {
                Toast.makeText(this, getResources().getString(R.string.could_not_find_comic) + comicBundleName, Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            comicName = mComic.getName();
            // note: onDemand comics do not have bookmarks
        } else {
            mComic = mIverseApp.getComicStore().getDatabaseApi().getComicByBundleName(comicBundleName);
            MyComicsModel myComicsModel = new MyComicsModel(this);
            mMyComic = myComicsModel.getComicByBundleName(comicBundleName);

            // user does not own or has not downloaded this mComic
            if (mMyComic == null) {
                Toast.makeText(this,  getResources().getString(R.string.could_not_find_comic_name)+ comicBundleName, Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            comicName = mMyComic.getName();
            File comicFile = null;
            try {
                comicFile = new Storage().getFile(mMyComic.getAssetFilename());
            } catch (IOException e1) {
                Toast.makeText(this, getResources().getString(R.string.unable_to_open_sdcard),
                        Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            final Uri comicUri = Uri.fromFile(comicFile);

            if (!"file".equals(comicUri.getScheme())) {
                Toast.makeText(this, getResources().getString(R.string.error_cant_read_comic),
                        Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            ComicBundleSourceAdapter myComicSource = new ComicBundleSourceAdapter(comicFile);
            myComicSource.setOwnership(mIverseApp.getOwnership());

            if (mMyComic.getBookMark() != null)
                mBookmark = ComicBookmark.fromURIString(mMyComic.getBookMark());
            else
                mBookmark = ComicBookmark.fromModeAndIndex(ComicMode.PAGE, 0);

            if(myComicSource.getMetadata().isWaidFormat()) {
                myComicSource.setComicMode(ComicMode.PANEL);
            }else {
                myComicSource.setComicMode(mBookmark.getMode());
            }

            // Receive callback when mComic source is updated
            // This must happen before mComicSource.open so that
            // onComicSourceUpdated will be called.
            myComicSource.setComicSourceListener(this);
            mComicSource = myComicSource;

            pageIdx = mBookmark.getIndex();
        }

        setupNavigationLayer(comicName);

        // open is only necessary for mComic bundles that have been downloaded to the filesystem
        if (!previewMode && !streamingMode) {
            try {
                mComicSource.open();
            } catch (Exception e) {
                LOG.error("error opening mComic bundle for reading: %s", e.getLocalizedMessage());
                showFailDialog();
            }
        }
        final ComicPagerAdapter mAdapter = new ComicPagerAdapter();
        mViewPager.setAdapter(mAdapter);

        // means it is a waid format book and not in streaming mode.
        if(isWaidForamt && !previewMode ) {

            // This is for apply more gradual fade animation to WAID format books.
            try {
                Field mScroller;
                mScroller = ViewPager.class.getDeclaredField("mScroller");
                mScroller.setAccessible(true);
                FixedSpeedScroller scroller = new FixedSpeedScroller(mViewPager.getContext());
                mScroller.set(mViewPager, scroller);
            } catch (NoSuchFieldException e) {
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            }
            //Applying the fadeIn/FadeOut animation to the WAID format book.
            mViewPager.setPageTransformer(true, new ZoomOutTranformer());
        }
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                // NA

            }

            @Override
            public void onPageSelected(int i) {
                updateSeekBar(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                // NA
            }
        });

        // restore position from mBookmark
        if (mBookmark != null)
            mViewPager.setCurrentItem(mBookmark.getIndex());

        updateSeekBar(pageIdx);

        if (previewMode) {
            previewDownloadTask = new AsyncMyComicSourceOpenTask(this, mComicSource);
            previewDownloadTask.execute();
            mLoadingOverlay.setVisibility(View.VISIBLE);
        } else {
            mLoadingOverlay.setVisibility(View.GONE);
            setSliderMax();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(CURRENT_PAGE, mViewPager.getCurrentItem());
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int currentPage = savedInstanceState.getInt(CURRENT_PAGE, mViewPager.getCurrentItem());
        if (currentPage != mViewPager.getCurrentItem())
            mViewPager.setCurrentItem(currentPage);
    }


    private void setupNavigationLayer(String comicTitle) {
        // setup and hide the navigation layer
        mNavOverlay = (RelativeLayout) (findViewById(R.id.navPage));

        mNavOverlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                alpha.setDuration(300);
                alpha.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mNavOverlay.setVisibility(View.GONE);
                    }
                });
                mNavOverlay.startAnimation(alpha);
            }
        });

        ((TextView) findViewById(R.id.txtTitle)).setText(comicTitle);

        mCurrentPageTextView = (TextView) (findViewById(R.id.txtPage));

        mSeekBar = (SeekBar) (findViewById(R.id.sliderPage));
        mSeekBar.setMax(100);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                float progress = mSeekBar.getProgress() / 10.0f;
                int page = Math.round(progress);
                mSeekBar.setProgress(page * 10);
                mViewPager.setCurrentItem(page);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }
        });

        findViewById(R.id.btnNavLeft).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(isWaidForamt) {
                    applyFadeIn(mViewPager);
                }
                if (mViewPager.getCurrentItem() > 0)
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            }
        });
        UIUtil.makeButton(findViewById(R.id.btnNavLeft));
        findViewById(R.id.btnNavRight).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(isWaidForamt) {
                    applyFadeIn(mViewPager);
                }
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            }
        });
        UIUtil.makeButton(findViewById(R.id.btnNavRight));

        findViewById(R.id.btnDone).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
        UIUtil.makeButton(findViewById(R.id.btnDone));

        findViewById(R.id.btnInfo).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bShowInfo) {
                    bShowInfo = false;
                    hideInfo();
                } else {
                    bShowInfo = true;
                    showInfo();
                }
            }
        });

        /** As per the Bug #5306: Comic Reader > Remove Overlay HUD text and arrows for localizations
        If the localization is in English, please display the "i" button in reader HUD and text.
        If the localization is in an other language, please hide the "i" button in reader HUD and the text it would display.
        - Enabled for only English*/
        if( AndroidInfo.getLanguage(ComicReaderPhotoViewActivity.this).equalsIgnoreCase("en")) {
            findViewById(R.id.btnInfo).setVisibility(View.VISIBLE);
            UIUtil.makeButton(findViewById(R.id.btnInfo));
        }

        findViewById(R.id.btnFacebook).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(mComic !=null)
                    mIverseApp.getShareManager().shareComicVia(ComicReaderPhotoViewActivity.this, ShareManager.METHOD_FACEBOOK, mComic, null);
            }
        });

        findViewById(R.id.btnTwitter).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(mComic !=null)
                    mIverseApp.getShareManager().shareComicVia(ComicReaderPhotoViewActivity.this, ShareManager.METHOD_TWITTER, mComic, null);
            }
        });

        if(!previewMode && !streamingMode) {
            if(ConnectionHelper.isConnectedToInternet(mActivity)) {
                findViewById(R.id.btndelete).setVisibility(View.VISIBLE);
                findViewById(R.id.btndelete).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        showDeleteComicDialog(mMyComic);
                    }
                });
            }
        }

        UIUtil.makeButton(findViewById(R.id.btnFacebook));
        UIUtil.makeButton(findViewById(R.id.btnTwitter));

        configureInstructionsOverlay();

        removeInfo();
        
        mNavOverlay.setVisibility(View.GONE);
    }

    private void configureInstructionsOverlay() {
        Typeface fontInfo = Typeface.createFromAsset(getAssets(), "font/ChalkDust.ttf");
        if (fontInfo == null)
            return;
        viewInfoBack = findViewById(R.id.info_back);
        viewInfoBackContainer = (RelativeLayout) (findViewById(R.id.info_back_container));

        bShowInfo = false;
        listInfoViewsPortrait = new Vector<View>();
        listInfoViewsLandscape = new Vector<View>();
        int ids[] = {R.id.info1, R.id.info2, R.id.info3, R.id.info4, R.id.info5, R.id.info6, R.id.info7, R.id.info8, R.id.info9, R.id.info10, R.id.info11, R.id.info12, R.id.info13, R.id.info14,R.id.info15,R.id.info16};
        int wids[] = {R.id.winfo1, R.id.winfo2, R.id.winfo3, R.id.winfo4, R.id.winfo5, R.id.winfo6, R.id.winfo7, R.id.winfo8, R.id.winfo9, R.id.winfo10, R.id.winfo12, R.id.winfo13, R.id.winfo14,R.id.winfo15,R.id.winfo16};

        for (int i = 0; i < ids.length; i++) {
            View view = findViewById(ids[i]);
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(fontInfo);
            }
            listInfoViewsPortrait.add(view);
        }
        for (int i = 0; i < wids.length; i++) {
            View view = findViewById(wids[i]);
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(fontInfo);
            }
            listInfoViewsLandscape.add(view);
        }
    }

    private void setSliderMax() {
        mSeekBar.setMax((mComicSource.getImageCount() - 1) * 10);
    }

//    private void onPageChange(final int page) {
//		runOnUiThread(new Runnable() {
//            public void run() {
//                mCurrentPageTextView.setText("Current Page: " + (page + 1));
//                mSeekBar.setProgress(page * 10);
//            }
//        });
//	}

    private void updateSeekBar(int page) {
        mCurrentPageTextView.setText(getResources().getString(R.string.current_pages) + (page + 1));
        mSeekBar.setProgress(page * 10);
    }

    private void showNavigationLayer() {
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        mNavOverlay.setVisibility(View.VISIBLE);
        alpha.setDuration(300);
        mNavOverlay.startAnimation(alpha);
    }

    private void showInfo() {
        viewInfoBack.setVisibility(View.VISIBLE);

        applyFadeIn(viewInfoBack);

        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            for (View view : listInfoViewsPortrait) {
                view.setVisibility(View.VISIBLE);
                applyFadeIn(view);
            }
        } else {
            for (View view : listInfoViewsLandscape) {
                view.setVisibility(View.VISIBLE);
                applyFadeIn(view);
            }
        }
    }

    private void removeInfo() {
        viewInfoBack.setVisibility(View.GONE);

        for (View view : listInfoViewsPortrait)
            view.setVisibility(View.GONE);
        for (View view : listInfoViewsLandscape)
            view.setVisibility(View.GONE);
    }

    private void hideInfo() {
        applyFadeOut(viewInfoBack, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                removeInfo();
            }
        });
        for (View view : listInfoViewsPortrait) {
            applyFadeOut(view, null);
        }
        for (View view : listInfoViewsLandscape) {
            applyFadeOut(view, null);
        }
    }

    private void applyNewFadeIn(View view) {
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        alpha.setDuration(2000);
        view.startAnimation(alpha);
    }
    private void applyFadeIn(View view) {
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        alpha.setDuration(300);
        view.startAnimation(alpha);
    }

    private void applyFadeOut(View view, Animation.AnimationListener listener) {
        AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
        alpha.setDuration(300);
        if (listener != null) {
            alpha.setAnimationListener(listener);
        }
        view.startAnimation(alpha);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // super.onCreateOptionsMenu(menu);

        // Create menus (in order)
        // final MenuItem menuInfo = menu.add(0, INFORMATION, 0,
        // R.string.menu_information);
        final MenuItem menuNav = menu.add(0, NAVIGATION, 1, R.string.menu_navigation);
        // final MenuItem menuPreferences = menu.add(0, PREFERENCES, 2,
        // R.string.menu_preferences);

        // Establish icons for menus
        // menuInfo.setIcon(R.drawable.menu_information);
        menuNav.setIcon(R.drawable.menu_navigation);
        // menuPrs.setIcon(R.drawable.menu_preferences);

        return true;
    }


    @Override
    public void onDestroy() {
        mDamageReport.restoreOriginalHandler();
        mDamageReport=null;
        super.onDestroy();
        saveComicBookmark();

        if (mComicSource != null) {
            try {
                mComicSource.close();
            } catch (IOException e) {
                LOG.error("error closing mComicSource: %s", e.getLocalizedMessage());
            }
        }
    }

    void saveComicBookmark() {
        if (mBookmark != null) {
            mBookmark.setIndex(mViewPager.getCurrentItem());
            final String serializedBookmark = mBookmark.asBookmarkString();

            if (!previewMode && mMyComic != null) {
                mMyComic.setBookMark(serializedBookmark);
                final MyComicsModel mcm = new MyComicsModel(this);
                mcm.update(mMyComic);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case INFORMATION:
                final Intent i = new Intent(this, ComicInformationActivity.class);
                startActivityForResult(i, 0);
                break;
            case NAVIGATION:
                openNavigation();
                break;
            case PREFERENCES:
                openPreferences();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openNavigation() {
        final ComicNav nav = new ComicNav();

        nav.setMaxAndCurrent(mComicSource.getImageCount(), mViewPager.getCurrentItem());

        Dialog dlg = nav.createDialog(this);
        dlg.setOnDismissListener(new Dialog.OnDismissListener() {
            public void onDismiss(final DialogInterface dialog) {
                String message = nav.getMessage();
                if (message != null) {
                    Toast.makeText(ComicReaderPhotoViewActivity.this, message,
                            Toast.LENGTH_LONG).show();
                }
                if (!nav.isCanceled()) {
                    mViewPager.setCurrentItem(nav.getPanelPosition());
                }
            }
        });
        dlg.show();
    }

    private void openPreferences() {
        final Intent i = new Intent(this, ComicPreferencesActivity.class);
        startActivityForResult(i, 0);
    }


    public void showFailDialog() {
        if(!(ComicReaderPhotoViewActivity.this).isFinishing()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.icon)
                    .setTitle(R.string.comic_open_error_dialog_title)
                    .setMessage(R.string.comic_open_error_dialog_message)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }
                    ).create().show();
        }
    }


    @Override
    public void OnAsyncMyComicSourceOpenTaskFinished(boolean bSuccess) {
        if (bSuccess) {
            mLoadingOverlay.setVisibility(View.GONE);
            mComicSource.onOpenFinished(true);
            // We've received the mComic data, update the slider to match the number of pages
            setSliderMax();
        } else {
            if(!isFinishing() && !previewMode)
                 showFailDialog();
        }
    }

    @Override
    public void onComicSourceUpdated(IComicSourceAdapter adapter) {
        LOG.info("onComicSourceUpdated");
        mViewPager.setAdapter(new ComicPagerAdapter());
        // restore position from mBookmark
        if (mBookmark != null) {
            mViewPager.setCurrentItem(mBookmark.getIndex());
            updateSeekBar(mBookmark.getIndex());
        }
    }

    @Override
    public void onBitmapLoaded(int index, Bitmap bitmap) {

        LOG.info("onBitmapLoaded");
    }


    private class ComicPagerAdapter extends PagerAdapter implements PhotoViewAttacher.OnPhotoTapListener {
        final ImageLoader mImageLoader = ImageLoader.getInstance();
        final boolean mIsBundle;

        ComicPagerAdapter() {
            mIsBundle = mComicSource instanceof ComicBundleSourceAdapter;
        }

        @Override
        public int getCount() {
            return mComicSource.getImageCount();
        }
        @Override
        public int getItemPosition(Object object){
            return POSITION_NONE;
        }

        @Override
        public View instantiateItem(ViewGroup container, final int position) {

            final PhotoView photoView = new PhotoView(container.getContext());
            photoView.setOnPhotoTapListener(this);
            photoView.setTag(position);

            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            // If we're loading a comic page image from a ComicBundle then we do not have a simple
            // URI to load the image from, so we use loadComicPage here instead of Universal Image Loader.

//            if(isWaidForamt) {
//                photoView.refreshDrawableState();
//                applyNewFadeIn(photoView);
//            }
//            applyFadeOut(photoView, null);
            if (mIsBundle) {

                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Void... params) {
                        return mComicSource.loadComicPage(position);
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        if (bitmap != null) {
                            photoView.setImageBitmap(bitmap);
                            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                                if (isWaidForamt) {
                                    //do nothing
                                } else {
                                    photoView.setScale(2.5f, 0, 0, false);
                                }

                            }
                        }
                    }
                }.execute();
            } else {
                // https://github.com/nostra13/Android-Universal-Image-Loader/blob/master/sample/src/com/nostra13/example/universalimageloader/ImagePagerActivity.java
                mImageLoader.displayImage(mComicSource.getImageURI(position).toString(), photoView, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        mLoadingOverlay.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        String message = null;
                        switch (failReason.getType()) { //todo maybe show some user friendly message
                            case IO_ERROR:
                                message = "Input/Output error";
                                break;
                            case DECODING_ERROR:
                                message = "Image can't be decoded";
                                break;
                            case NETWORK_DENIED:
                                message = "Downloads are denied";
                                break;
                            case OUT_OF_MEMORY:
                                message = "Out Of Memory error";
                                break;
                            case UNKNOWN:
                                message = "Unknown error";
                                break;
                        }
                        Toast.makeText(ComicReaderPhotoViewActivity.this, message, Toast.LENGTH_SHORT).show();
                       ((PhotoView)view).setImageDrawable(getResources().getDrawable(R.drawable.no_preview_available));
                        mLoadingOverlay.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, final View view, Bitmap loadedImage) {
                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE  ) {
                            if(isWaidForamt){
                                //do nothing
                            }else {
                                ((PhotoView) view).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((PhotoView) view).setScale(2.5f, 0, 0, false);
                                    }
                                });
                            }
                        }
                        mLoadingOverlay.setVisibility(View.GONE);
                    }
                });
            }

            return photoView;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        // PhotoViewAttacher.OnPhotoTapListener
        @Override
        public void onPhotoTap(View view, float x, float y) {
            showNavigationLayer();
        }
    }



    private void showDeleteComicDialog(final MyComic comic) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(com.iversecomics.archie.android.R.string.delete_comic_title)
                .setIcon(android.R.drawable.stat_sys_warning)
                .setMessage(com.iversecomics.archie.android.R.string.delete_comic_message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //If the product is "Unlimited product", -  we need to update the server when we delete the product.
                        // because the offline que is limited to 6 only.
                        if (comic.getOnDemand() == true) {
                            removeOfflineProduct(comic.getComicId(), comic);
                            return;
                        }
                        // deletion of a normal product.
                        final MyComicsModel model = new MyComicsModel(mActivity);
                        model.delete(comic);
                        try {
                            Storage.assertAvailable();
                            final File comicAsset = new File(Storage.getExternalComicsStorageDir(), comic.getAssetFilename());
                            comicAsset.delete();
                            getFragmentManager().popBackStack();
                            finish();
                        } catch (Exception e) {
                            LOG.error("error deleting comic: " + e.getLocalizedMessage());
                        }
                    }
                })
                .setNegativeButton(com.iversecomics.archie.android.R.string.cancel, null);
        builder.create().show();

    }

    private void removeOfflineProduct(final String productId, final MyComic comic) {

        final ComicStore.ServerApi server = mIverseApp.getComicStore().getServerApi();
        final IverseApplication app = (IverseApplication)this.getApplication();
        final String deviceId = app.getOwnership().getUniqueId();

        new AsyncTask<String, Void, OnDemandResponse>() {
            @Override
            protected OnDemandResponse doInBackground(String... params) {

                JSONObject payload = new JSONObject();
                try {
                    payload.put("productId", productId);
                    payload.put("streamPages", false);
                    payload.put("deviceId", deviceId);
                    payload.put("installId", ComicPreferences.getInstance().getInstallId());
                    payload.put("removeFromQueue", true);
                } catch (JSONException e) {
                    LOG.error(e, "FetchOnDemandComicTask, error building payload json");
                }
                OnDemandResponse resp = null;
                try {
                    JSONObject respJson = server.submitOnDemandComicRequest(payload);
                    resp = new Gson().fromJson(respJson.toString(), OnDemandResponse.class);

                } catch (ComicStoreException e) {
                    LOG.error(e, "Unable to fetch on demand comic, productId:%s", productId);
                }
                return resp;
            }

            @Override
            protected void onPostExecute(OnDemandResponse resp) {
                if (resp != null) {
                    LOG.info("FetchOnDemandComicTask, response json:%s", resp.toString());
                    if (resp.isError()) {
                        // TODO: show error dialog.
                        String msg = resp.error != null ? resp.error.errorMessage : resp.status;
                        Toast.makeText(mActivity, getResources().getString(R.string.error) + msg, Toast.LENGTH_SHORT).show();
                    } else {
                        // Success!

                        final MyComicsModel model = new MyComicsModel(mActivity);
                        model.delete(comic);
                        try {
                            Storage.assertAvailable();
                            final File comicAsset = new File(Storage.getExternalComicsStorageDir(), comic.getAssetFilename());
                            comicAsset.delete();
                            // After deleting comic we are reducing the array count.
                            removeOnDemandOfflineQueueItem(productId);
                            finish();
                        } catch (Exception e) {
                            LOG.error("error deleting comic: " + e.getLocalizedMessage());

                        }
                    }
                } else {
                    // TODO: show error dialog.
                    Toast.makeText(mActivity, getResources().getString(R.string.error_in_processing_request), Toast.LENGTH_SHORT).show();
                }
            }

        }.execute();
    }

    /**
     * This method id used to delete the OnDemandOfflineQueue item from the parsed OnDemandOfflineQueue list. this item will be deleted locally.
     * @param productID - for the deleted OnDemandOfflineQueue item
     */
    private void removeOnDemandOfflineQueueItem(String productID){
        for(int i=0; i<ServerConfig.getDefault().getOnDemandOfflineQueue().size();i++){
            OnDemandOfflineQueue offlineQue = ServerConfig.getDefault().getOnDemandOfflineQueue().get(i);
            if(productID.equalsIgnoreCase(offlineQue.getComic_bundle_name())){
                LOG.debug("OnDemandOfflineQueue Comic Removed",offlineQue.getName());
                ServerConfig.getDefault().getOnDemandOfflineQueue().remove(i);
                break;
            }

        }
    }

    class OnDemandResponse implements Serializable {
        public MetaData metadata;
        public ArrayList<String> pages;
        /*
            example error response: {"error":{"errorCode":1005,"errorMessage":"Subscription expired or invalid"},"status":"error"}
        */
        public String status;
        public ErrorData error;

        public boolean isHorizontalOnly() {
            return metadata != null && metadata.horizontal_fades != 0;
        }

        public boolean isRemotePages() {
            return true;
        }

        public boolean isError() {
            return error != null || (status != null && status.equals("error"));
        }

        class MetaData {
            public String name;
            public String product_id;
            public String series_product_id;
            public String synopsis;
            public String product_type;
            public String description;
            public String publisher_id;
            public String publisher_name;
            public int manga_orientation;
            public int horizontal_fades;
        }

        class ErrorData {
            public int errorCode;
            public String errorMessage;
        }

    }

 public class ZoomOutTranformer extends ABaseTransformer {

        @Override
        protected void onTransform(View view, float position) {
            final float scale = 1f + Math.abs(position);
//            view.setScaleX(scale);
//            view.setScaleY(scale);
            view.setPivotX(view.getWidth() * 0.5f);
            view.setPivotY(view.getHeight() * 0.5f);
            view.setAlpha(position < -1f || position > 1f ? 0f : 1f - (scale - 1f));
            if(position == -1){
                view.setTranslationX(view.getWidth() * -1);
            }
        }

    }

    /**
     * This class is used to apply the Gradual fade to the WAID format books. Not for the other comics.
     */
    public class FixedSpeedScroller extends Scroller {
        private int mDuration = 2000;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, interpolator, flywheel);
        }


        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }

}
