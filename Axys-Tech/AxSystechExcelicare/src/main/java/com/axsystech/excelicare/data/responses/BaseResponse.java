package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Date: 14.04.14
 * Time: 8:40
 *
 * @author SomeswarReddy
 */
public class BaseResponse implements Serializable {

    private static final String SUCCESS = "success";

    @SerializedName("Status")
    private String mStatus;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("Message")
    private String mMessageCaps;

    @SerializedName("Reason")
    private String mReason;

    public String getMessageCaps() {
        return mMessageCaps;
    }

    public String getReason() {
        return mReason;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public boolean isNullStatus() {
        return mStatus == null;
    }

    public boolean isSuccess() {
        return isNullStatus() || SUCCESS.equalsIgnoreCase(mStatus.trim());//TODO: must be removed isNullStatus() after will be changed on server!!!
    }

    public String getMessage() {
        return mMessage == null ? "" : mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

}