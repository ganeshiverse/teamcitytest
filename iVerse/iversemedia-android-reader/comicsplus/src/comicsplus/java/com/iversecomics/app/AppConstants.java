package com.iversecomics.app;


import com.iversecomics.archie.android.BuildConfig;

/**
 * These are Archie specific constants that are used in {@link ComicApp}.
 */
public class AppConstants {

    public static final String DEV_SERVER_URL = "https://development.iverseapps.com/getConfig.php";
    public static final String PROD_SERVER_URL = "https://comicsplus.iverseapps.com/getConfig.php";
    public static final String SHARED_SECRET = "ldpokedihwefgy34gr74hfwidonhdbgoiwejfoiwehfuieg759hfiohfuidhgrytepihfourwghiuertoer394dhwfdhwiyt3eiw";


    public static final String SKU_PACKAGE_NAME = "com.iversecomics.comicsplus.android";

    public static final boolean DEBUG = BuildConfig.DEBUG;

    public static final String HOCKEY_APP_ID = "f7a21b032d58ff7473041a24ba68bc55";

    public static final boolean PUBLISHERS_INSTEAD_OF_TITLES_IN_SIDE_MENU = true;
    public static final boolean DISABLE_LOGGING = false;
    public static boolean SKU_SPECIFIC_TITLES = true;
    public static String ONDEMAND_BUTTON_TITLE = "Read Unlimited!";

    public static final String SOCIAL_NETWORKING_APP_NAME = "Comics Plus";

    public static final boolean USE_SUBSCRIPTIONS = true;
    public static final String DEFAULT_SUBSCRIPTION_SKUS = "com.iversecomics.archie.unlimited.subscription.monthly";

    // As per the Sofia request and bug No : https://redmine.iverseapps.com/issues/5251#change-20490, we are enabling the subscription service for Archie and disabling in Comic plus.
    // True -  Enabled , False - didabled
    public static boolean ENABLE_SUBSCRIPTIONS_SERVICE = false;

    // As per the Sofia request and bug No : https://redmine.iverseapps.com/issues/5308, we are disabling the TapJoy service for Archie and Comic plus.
    // True -  Enabled , False - disabled
    public static boolean ENABLE_TAPJOY = false;
    public static final boolean ENABLE_CRASH_REPORT = true;
}
