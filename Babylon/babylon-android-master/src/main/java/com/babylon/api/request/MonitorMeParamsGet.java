package com.babylon.api.request;

import android.text.TextUtils;
import com.babylon.api.request.base.AbsRequestGet;
import com.babylon.api.request.base.ResponseList;
import com.babylon.api.request.base.Urls;
import com.babylon.model.MonitorMeFeatures;
import com.babylon.utils.DateUtils;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class MonitorMeParamsGet extends AbsRequestGet<ResponseList<MonitorMeFeatures>> {

    private final static String DATE_FROM = "dateFrom";
    private final static String DATE_NOW = "dateNow";
    private final static String DATE_TO = "dateTo";
    private final static String FEATURE = "feature[]";

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.MONITOR_ME_PARAMS_GET);
        return TextUtils.concat(url, getUrlParams(params)).toString();
    }

    /**
     * @param dateFrom  date, timestamp mills
     * @param dateTo    date, timestamp mills
     * @param paramList list of ids of monitor me parameters
     */
    public static MonitorMeParamsGet newMonitorMeParamsGet(long dateFrom, long dateTo,
                                                           List<String> paramList) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

        params.add(new BasicNameValuePair(DATE_FROM, String.valueOf(dateFrom / android.text.format.DateUtils
                .SECOND_IN_MILLIS)));
        params.add(new BasicNameValuePair(DATE_TO, String.valueOf(dateTo / android.text.format.DateUtils
                .SECOND_IN_MILLIS)));

        if (paramList != null) {
            for (String paramId : paramList) {
                params.add(new BasicNameValuePair(FEATURE, paramId));
            }
        }

        long currentTimeInSeconds = DateUtils.getDateNowInSeconds();
        params.add(new BasicNameValuePair(DATE_NOW, String.valueOf(currentTimeInSeconds)));

        return (MonitorMeParamsGet) new MonitorMeParamsGet().withParams(params);
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }
}
