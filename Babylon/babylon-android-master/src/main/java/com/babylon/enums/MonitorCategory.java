package com.babylon.enums;

/**
 * Enum for all monitor me categories with id and options
 */
public enum MonitorCategory {
    FITNESS("Fitness", "1", false),
    SHAPE("Shape", "2", false),
    MIND("Mind", "3", false),
    HEART("Heart", "4"),
    KIDNEY("Kidney", "5"),
    LIVER("Liver", "6"),
    VITAMINS("Vitamins", "7"),
    BONE("Bone", "8"),
    SUGAR_AND_HORMONES("Sugar and Hormones", "9"),
    BLOOD("Blood", "10");

    private String name;
    private String id;
    private boolean isMedicalKitExist;

    private MonitorCategory(String name, String id) {
        this(name, id, true);
    }

    private MonitorCategory(String name, String id, boolean isMedicalKitExist) {
        this.name = name;
        this.id = id;
        this.isMedicalKitExist = isMedicalKitExist;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public boolean isMedicalKitExist() {
        return isMedicalKitExist;
    }

    public static String[] getAllNames() {
        MonitorCategory[] values = values();
        String[] names = new String[values.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = values[i].getName();
        }
        return names;
    }

    public static boolean isMedicalKitExist(String categoryId) {
        boolean isMedicalKitExist = false;
        for (MonitorCategory monitorCategory : values()) {
            if (monitorCategory.getId().equalsIgnoreCase(categoryId)) {
                isMedicalKitExist = monitorCategory.isMedicalKitExist();
                break;
            }
        }
        return isMedicalKitExist;
    }
}
