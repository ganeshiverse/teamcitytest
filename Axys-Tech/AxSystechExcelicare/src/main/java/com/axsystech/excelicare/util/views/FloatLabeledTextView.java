package com.axsystech.excelicare.util.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.animation.AnimatorProxy;

public class FloatLabeledTextView extends FrameLayout {

    private static final int DEFAULT_PADDING_LEFT = 2;

    private TextView mHintTextView;
    private TextView mContextTextView;

    private boolean isMandatoryField = false;
    private int mandatoryMarkDrawable;
    private String textViewHintText;
    private boolean isHintTextViewAdded = false;

    private Context mContext;

    public FloatLabeledTextView(Context context) {
        super(context);
        mContext = context;
    }

    public FloatLabeledTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setAttributes(attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public FloatLabeledTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        setAttributes(attrs);
    }

    private void setAttributes(AttributeSet attrs) {
        mHintTextView = new TextView(mContext);

        final TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.FloatLabeledTextView);

        final int padding = a.getDimensionPixelSize(R.styleable.FloatLabeledTextView_fltvPadding, 0);
        final int defaultPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_PADDING_LEFT, getResources().getDisplayMetrics());
        final int paddingLeft = a.getDimensionPixelSize(R.styleable.FloatLabeledTextView_fltvPaddingLeft, defaultPadding);
        final int paddingTop = a.getDimensionPixelSize(R.styleable.FloatLabeledTextView_fltvPaddingTop, 0);
        final int paddingRight = a.getDimensionPixelSize(R.styleable.FloatLabeledTextView_fltvPaddingRight, 0);
        final int paddingBottom = a.getDimensionPixelSize(R.styleable.FloatLabeledTextView_fltvPaddingBottom, 0);
        Drawable background = a.getDrawable(R.styleable.FloatLabeledTextView_fltvBackground);

        // get mandatory mark flag & drawable
        isMandatoryField = a.getBoolean(R.styleable.FloatLabeledTextView_fltvIsMandatory, false);
        mandatoryMarkDrawable = a.getResourceId(R.styleable.FloatLabeledTextView_fltvMandatoryMarkDrawable, R.drawable.ic_star_small);
        textViewHintText = a.getString(R.styleable.FloatLabeledTextView_fltvTextViewHintText);

        if (padding != 0) {
            mHintTextView.setPadding(padding, padding, padding, padding);
        } else {
            mHintTextView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }

        if (background != null) {
            setHintBackground(background);
        }

        if (isMandatoryField) {
            mHintTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, mandatoryMarkDrawable, 0);
        } else {
            mHintTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        mHintTextView.setTextAppearance(mContext, a.getResourceId(R.styleable.FloatLabeledTextView_fltvTextAppearance, android.R.style.TextAppearance_Small));

        //Start hidden
        mHintTextView.setVisibility(INVISIBLE);
        AnimatorProxy.wrap(mHintTextView).setAlpha(0);

        addView(mHintTextView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        a.recycle();
    }

    public void displayMandatoryMark(boolean isMandatory) {
        isMandatoryField = isMandatory;

        if(mHintTextView != null) {
            if (isMandatory) {
                mHintTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, mandatoryMarkDrawable, 0);
            } else {
                mHintTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
    }

    @SuppressLint("NewApi")
    private void setHintBackground(Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mHintTextView.setBackground(background);
        } else {
            mHintTextView.setBackgroundDrawable(background);
        }
    }

    @Override
    public final void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (isHintTextViewAdded && child instanceof TextView) {
            if (mContextTextView != null) {
                throw new IllegalArgumentException("Can only have one TextView subview");
            }

            final LayoutParams lp = new LayoutParams(params);
            lp.gravity = Gravity.BOTTOM;
            lp.topMargin = (int) (mHintTextView.getTextSize() + mHintTextView.getPaddingBottom() + mHintTextView.getPaddingTop());
            params = lp;

            setTextView((TextView) child);
        }

        super.addView(child, index, params);
        isHintTextViewAdded = true;
    }

    private void setTextView(TextView textView) {
        mContextTextView = textView;

//        mContextTextView.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setShowHint(true);
//            }
//        });

        setShowHint(true);

        mContextTextView.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean gotFocus) {
                onFocusChanged(gotFocus);
            }
        });

        mHintTextView.setText(textViewHintText);
        mHintTextView.setVisibility(VISIBLE);
        setShowHint(true);
    }

    private void onFocusChanged(boolean gotFocus) {
        if (gotFocus && mHintTextView.getVisibility() == VISIBLE) {
            ObjectAnimator.ofFloat(mHintTextView, "alpha", 0.50f, 1f).start();
        } else if (mHintTextView.getVisibility() == VISIBLE) {
            AnimatorProxy.wrap(mHintTextView).setAlpha(1f);  //Need this for compat reasons
            ObjectAnimator.ofFloat(mHintTextView, "alpha", 1f, 0.50f).start();
        }
    }

    private void setShowHint(final boolean show) {
        AnimatorSet animation = null;
        if (/*(mHintTextView.getVisibility() == VISIBLE) &&*/ !show) {
            animation = new AnimatorSet();
            ObjectAnimator move = ObjectAnimator.ofFloat(mHintTextView, "translationY", 0, mHintTextView.getHeight() / 8);
            ObjectAnimator fade = ObjectAnimator.ofFloat(mHintTextView, "alpha", 1, 0);
            animation.playTogether(move, fade);
        } else if (/*(mHintTextView.getVisibility() != VISIBLE) &&*/ show) {
            animation = new AnimatorSet();
            ObjectAnimator move = ObjectAnimator.ofFloat(mHintTextView, "translationY", mHintTextView.getHeight() / 8, 0);
            ObjectAnimator fade;
            if (mContextTextView.isFocused()) {
                fade = ObjectAnimator.ofFloat(mHintTextView, "alpha", 0, 1);
            } else {
                fade = ObjectAnimator.ofFloat(mHintTextView, "alpha", 0, 0.50f);
            }
            animation.playTogether(move, fade);
        }

        if (animation != null) {
            animation.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    mHintTextView.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mHintTextView.setVisibility(show ? VISIBLE : INVISIBLE);
                    AnimatorProxy.wrap(mHintTextView).setAlpha(show ? 1 : 0);
                }
            });
            animation.start();
        }
    }

    public TextView getTextView() {
        return mContextTextView;
    }

    public void setHint(String hint) {
        mHintTextView.setText(hint);
    }

    public CharSequence getHint() {
        return mHintTextView.getHint();
    }

}
