package com.babylon.utils;

/**
 * Saves applications constant
 */
public class Constants {

    private Constants() {
        //not called
    }

    /* Actions */

    public static class Sync_Keys {
        public static final String SYNC_MESSAGES = "SYNC_MESSAGES";
        public static final String SYNC_SINGLE_MESSAGE = "SYNC_SINGLE_MESSAGE";
        public static final String SYNC_MONITOR_ME_PARAMS = "SYNC_MONITOR_ME_PARAMS";
        public static final String SYNC_MONITOR_ME_REPORT = "SYNC_MONITOR_ME_REPORT";
        public static final String SYNC_MONITOR_ME_CATEGORIES = "SYNC_MONITOR_ME_CATEGORIES";
        public static final String SYNC_FOOD = "SYNC_FOOD";
        public static final String SYNC_PHYSICAL_ACTIVITY = "SYNC_PHYSICAL_ACTIVITY";
        public static final String SYNC_PATIENT = "SYNC_PATIENT";
        public static final String SYNC_STRESS_QUIZ = "SYNC_STRESS_QUIZ";
        public static final String SYNC_TRIAGE = "SYNC_TRIAGE";
        public static final String SYNC_REGION = "SYNC_REGION";
    }

    public static class Header_RequestKeys {
        public static final String HEADER_WSSE_KEY = "x-wsse";
        public static final String X_AUTH_TOKEN = "X-Access-Token";
        public static final String BASIC_AUTH_HEADER = "Authorization";
    }

    public static String TITLE_NAME = "TITLE_NAME";
    public static String IS_KIT_BUTTON_EXIST = "IS_KIT_BUTTON_EXIST";
    public static String DESCRIPTION = "DESCRIPTION";
    public static String DATE_IN_MILLIS = "EXTRA_DATE_IN_MILLIS";
    public static String CHART_PERIOD_ID = "CHART_PERIOD_ID";
    public static String CHART_FEATURE_ID = "CHART_FEATURE_ID";
    public static String SHOW_DATE_RADIO_GROUP = "SHOW_DATE_RADIO_GROUP";
    public static String DEFAULT_PERIOD_ID = "DEFAULT_PERIOD_ID";

    public static final String DATABASE_DATE_FORMAT = "yyyy-MM-dd";
    public static final String SHORT_DATE_PATTERN = "yyyy-MM-dd";
    public static final String FACEBOOK_DATE_PATTERN = "MM/dd/yyyy";

    public static final int QUESTION_LENGTH_MIN = 20;
    public static final int DATABASE_UNIDENTIFIED_TOKEN = 0;

    /* Sync keys*/
    public static final String PREF_SETUP_COMPLETE = "setup_complete";

    /* Tags */
    public static final String ASK_FRAGMENT_TAG = "ASK_FRAGMENT_TAG";
    public static final String MONITOR_ME_FRAGMENT_TAG = "MONITOR_ME_FRAGMENT_TAG";
    public static final String LOCATION_TRACKING_FRAGMENT_TAG = "LOCATION_TRACKING_FRAGMENT_TAG";
    public static final String ACTION_OPEN_LOCATION_FRAGMENT = "ACTION_OPEN_LOCATION_FRAGMENT";
    public static final String BILLINGS_FRAGMENT_TAG = "BILLINGS_FRAGMENT_TAG";
    public static final String SUBSCRIPTIONS_FRAGMENT_TAG = "SUBSCRIPTIONS_FRAGMENT_TAG";

    /* Extras */
    public static final String EXTRA_FRAGMENT_TYPE = "EXTRA_FRAGMENT_TYPE";
    public static final String EXTRA_FOOD = "EXTRA_FOOD";
    public static final String EXTRA_FOODS = "EXTRA_FOODS";
    public static final String EXTRA_BARCODE = "EXTRA_BARCODE";

    /* Actions */
    public static final String ACTION_FOOD_RECEIVE = "ACTION_FOOD_RECEIVE";

    /* Charts */
    public static final long DAY_TIME_IN_MS = 86400000;
    public static final long MONTH_TIME_IN_MS = 30 * DAY_TIME_IN_MS;
    public static final long SIX_MONTH_TIME_IN_MS = 6 * MONTH_TIME_IN_MS;
    public static final long YEAR_TIME_IN_MS = 12 * MONTH_TIME_IN_MS;

    public static final String CHART_DATE_FORMAT = "dd MMM";

    /* Activity recognition */
    public static final String KEY_CURRENT_ACTIVITY_TYPE = "KEY_CURRENT_ACTIVITY_TYPE";
    public static final String KEY_CATEGORY_LOCATION_SERVICES = "KEY_CATEGORY_LOCATION_SERVICES";
    public static final String KEY_ACTION_REFRESH_STATUS = "KEY_ACTION_REFRESH_STATUS";
    public static final String KEY_ACTIVITY_TYPE = "KEY_ACTIVITY_TYPE";

    public static final String CONNECTION_FAILURE_FILTER = "CONNECTION_FAILURE_FILTER";
    public static final String DETECTIONREQUESTER_CONNECTION_FAILURE = "DETECTIONREQUESTER_CONNECTION_FAILURE";
    public static final String DETECTIONREMOVER_CONNECTION_FAILURE = "DETECTIONREMOVER_CONNECTION_FAILURE";
    public static final String NEED_CLEAN_UP = "NEED_CLEAN_UP";

    public static final int DETECTION_INTERVAL_SECONDS = 20;
    public static final int MILLISECONDS_PER_SECOND = 1000;
    public static final int DETECTION_INTERVAL_MILLISECONDS = MILLISECONDS_PER_SECOND * DETECTION_INTERVAL_SECONDS;

    public static final String IS_SHOW_CONTACT_ALERT = "IS_SHOW_CONTACT_ALERT";

    /* Monitor me */
    public static final int MONITOR_ME_NORMAL_STATE = 1;
    public static final int MONITOR_ME_WARNING_STATE = 2;
    public static final int MONITOR_ME_BAD_STATE = 3;
    public static final int MONITOR_ME_UNDEFINIED_STATE = 4;
    public static final int MONITOR_ME_FULL_PERCENT = 100;

    public static final String EXTRA_MIN_DATE = "EXTRA_MIN_DATE";
    public static final String EXTRA_MAX_DATE = "EXTRA_MAX_DATE";

    public static final int REGION_MIN_AGE_DEFAULT = -1;

    /* Distance */
    public static final long KILOMETER_IN_METERS = 1000;
    /* Transaction result */
    public static String TRANSUCTION_RESULT = "TRANSACTION_RESULT";

    //public static String MIX_PANEL_TOKEN = "389badaa4d30598e33633d2299b3b307";
    public static String MIX_PANEL_TOKEN = "cb0633ff8b0ac980de39c64f4d630f24";
}
