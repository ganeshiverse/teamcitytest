package com.iversecomics.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.iversecomics.archie.android.R;

/**
 * The widget that provides a frame for a comic and enforces a height x width ratio.
 */
public class ComicSlot extends AspectConstrainedFrame {
    private ImageView mImageView;


    public ComicSlot(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.comic_frame);
        int padding = (int) Math.floor(context.getResources().getDimension(R.dimen.comic_frame_padding));
        setPadding(padding, padding, padding, padding);

        ImageView image = new ImageView(context);
        image.setId(R.id.thumb);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(context, attrs);
        layoutParams.height = LayoutParams.MATCH_PARENT;
        layoutParams.width = LayoutParams.MATCH_PARENT;
        image.setLayoutParams(layoutParams);
        addView(image);

        mImageView = image;
    }

    public ImageView getImageView() {
        return mImageView;
    }
}
