package com.iversecomics.ui.menu.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.comic.viewer.prefs.ComicPreferences;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.store.AppStoreAdapterLocator;
import com.iversecomics.store.ComicBuyer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class SettingsFragment extends Fragment {

    private ViewGroup mRootView;
    private SettingsAdapter mMenuAdapter;
    private DateFormat mDateFormat = SimpleDateFormat.getDateTimeInstance();
    private ComicBuyer mComicBuyer;
    private AbstractAppStoreAdapter appStoreAdapter;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMenuAdapter = new SettingsAdapter(getActivity());

        // This should have already been done, but just in case.
        ComicPreferences.initialize(getActivity());
        if (getActivity() instanceof AppStoreAdapterLocator) {
            appStoreAdapter = ((AppStoreAdapterLocator) getActivity()).getAppStoreAdapter();
        } else {
            throw new IllegalArgumentException("Invalid activity. Must implement AppStoreAdapterLocator.");
        }
        mComicBuyer = new ComicBuyer(getActivity(), appStoreAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        mMenuAdapter.setData(new MenuDataProvider().getAllData());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_settings, container, false);

        mRootView.findViewById(R.id.action_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        StickyListHeadersListView listView = (StickyListHeadersListView) mRootView.findViewById(android.R.id.list);
        listView.setAdapter(mMenuAdapter);

        //Remove line separator in list. I tried to do this in XML, but was getting errors setting dividerHeight to 0dp.
        listView.setDivider(null);
        listView.setDividerHeight(0);

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                handleItemClick(mMenuAdapter.getItem(position));
            }
        });

        return mRootView;
    }

    public void handleItemClick(SettingsMenuItem item) {

        int fragContainerId = ((ViewGroup) getView().getParent()).getId();

        if (item.hasSubMenu) {

            Fragment subMenuFrag = null;
            if (item.itemType == SettingsMenuItem.SettingsMenuItemType.Languages)
                subMenuFrag = new LanguagesFragment(); //subMenuFrag = SettingsFragment.newLanguagesInstance();
            else if (item.itemType == SettingsMenuItem.SettingsMenuItemType.ViewCredits)
                subMenuFrag = new OpenSourceCreditsFragment();
            else
                return; // could not determine which type of menu to show

            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
                    .addToBackStack(null)
                    .replace(fragContainerId, subMenuFrag)
                    .commit();
        } else if (item.itemType == SettingsMenuItem.SettingsMenuItemType.OnDemandSubscription) {
            if (ServerConfig.getDefault().isSubscribed())
                showSubscriptionStatusDialog();
            else
                showSubscribeActivity();
        } else if (item.itemType == SettingsMenuItem.SettingsMenuItemType.RestorePurchases) {
            showRestorePurchasesDialog();
        }
    }

    /**
     * This should only be called if subscription is active.
     */
    private void showSubscriptionStatusDialog() {

        // format subscription end date for display
        ServerConfig cfg = ServerConfig.getDefault();
        Date endDate = cfg.getSubscriptionEndDate();
        String endDateString = "Unknown";
        if (endDate != null)
            endDateString = mDateFormat.format(endDate);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.subscription) + cfg.getSubscriptionStatusString())
                .setMessage(getResources().getString(R.string.subscription_current) + endDateString + getResources().getString(R.string.subscription_cancel))
                .setIcon(R.drawable.icon_settings)
                .setCancelable(true)
                .setNeutralButton(getString(android.R.string.ok), null);

        builder.create().show();
    }

    private void showSubscribeActivity() {
        // TODO: As running out of time at this moment, we are just displaying alert dialog only for subscription. In future we need to show this in an activity as iOS.
        mComicBuyer.purchaseSubscription();
    }

    private void showRestorePurchasesDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.inapp_purchase))
                .setMessage(getResources().getString(R.string.restore_purchase))
                .setIcon(R.drawable.icon_settings)
                .setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.restore), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        if (getActivity() instanceof AppStoreAdapterLocator) {
                            ((AppStoreAdapterLocator) getActivity()).getAppStoreAdapter().restoreTransactions();
                        } else {
                            throw new RuntimeException("Activity does not implement AppStoreAdapterLocator.");
                        }
//todo                        BillingService billingService = new BillingService();
//                        billingService.setContext(getActivity());
//                        if (!billingService.checkBillingSupported()) {
//                            Toast.makeText(getActivity(), "Cannot connect to billing service.", Toast.LENGTH_LONG).show();
//                        }
//                        else
//                            billingService.restoreTransactions();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), null);

        builder.create().show();
    }

    /**
     * ***************************  Settings Menu Adapter  ************************************
     */

    class SettingsAdapter extends ArrayAdapter<SettingsMenuItem> implements StickyListHeadersAdapter {
        private List<SettingsMenuSection> mSections;
        /**
         * Array keeping the first index of the section. Indexes correspond to mSections.
         */
        private int[] mSectionHeads;

        private LayoutInflater mInflater;

        public SettingsAdapter(Context context) {
            super(context, 0);
            mInflater = LayoutInflater.from(context);
        }

        public void setData(List<SettingsMenuSection> data) {
            clear();
            if (data == null) {
                mSections = Collections.emptyList();
            } else {
                mSections = data;
            }
            int numSections = mSections.size();
            mSectionHeads = new int[numSections];

            int sectionHead = 0;

            for (int i = 0; i < numSections; ++i) {
                mSectionHeads[i] = sectionHead;

                SettingsMenuSection section = mSections.get(i);

                int sectionSize = section.size();
                List<SettingsMenuItem> SettingsMenuItems = section.menuItems;

                for (int j = 0; j < sectionSize; ++j)
                    add(SettingsMenuItems.get(j));

                sectionHead += sectionSize;
            }
            notifyDataSetChanged();
        }


        public View getView(int position, View view, ViewGroup parent) {

            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_setting, parent, false);
            }

            final SettingsMenuItem menuItem = getItem(position);

            TextView titleView = (TextView) view.findViewById(R.id.title);
            ToggleButton toggle = (ToggleButton) view.findViewById(R.id.toggle);

            titleView.setText(menuItem.title);

            // show chevron to indicate this item has a sub-menu
            if (menuItem.hasSubMenu) {
                Drawable chevron = getContext().getResources().getDrawable(R.drawable.cell_icon_chevron);
                titleView.setCompoundDrawablesWithIntrinsicBounds(null, null, chevron, null);
            } else {
                titleView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            }

            // show toggle button if this item toggles a boolean preference value.
            if (menuItem.preferenceKey != null) {
                toggle.setVisibility(View.VISIBLE);
                // remove previous listener so that it doesn't trigger when we set the new checked value
                toggle.setOnCheckedChangeListener(null);
                toggle.setChecked(ComicPreferences.getInstance().isSet(menuItem.preferenceKey, true));
                toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        ComicPreferences.getInstance().setBoolean(menuItem.preferenceKey, b);
                    }
                });
            }
            // show toggle button if this item toggles a LANGUAGE preference value.
            else if (menuItem.languagePreferenceKey != null) {
                toggle.setVisibility(View.VISIBLE);
                // remove previous listener so that it doesn't trigger when we set the new checked value
                toggle.setOnCheckedChangeListener(null);
                toggle.setChecked(ComicPreferences.getInstance().isSet(menuItem.languagePreferenceKey, false));
                toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        ComicPreferences.getInstance().setPreferredLanguage(menuItem.languagePreferenceKey, b);
                    }
                });
            } else {
                toggle.setVisibility(View.GONE);
            }

            // show secondary text if available (currently only applicable to Subscription status row
            TextView secondaryText = (TextView) view.findViewById(R.id.secondary);
            if (menuItem.secondary != null) {
                secondaryText.setText(menuItem.secondary);
                secondaryText.setVisibility(View.VISIBLE);
            } else {
                secondaryText.setVisibility(View.GONE);
            }

            return view;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            int sectionIndex = getSectionIndex(position);

            if (convertView == null)
                convertView = mInflater.inflate(R.layout.menu_item_header, parent, false);

            SettingsMenuSection section = mSections.get(sectionIndex);
            ((TextView) convertView).setText(section.title);

            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            return getSectionIndex(position);
        }

        private int getSectionIndex(int position) {
            int numSections = mSectionHeads.length;
            int sectionIndex = 0;

            for (; sectionIndex < numSections; ++sectionIndex) {
                //This should be the only check. We've reached the section head that is above the position.
                //It accounts for empty sections (e.g. mSectionHeads = [0, 2, 2, 8] and for
                //position 2, we want to return index 2, not 1).
                if (position < mSectionHeads[sectionIndex]) {
                    break;
                }
            }

            return sectionIndex - 1;
        }
    }


    /***************** Data Providers *********************/


    /**
     * Provides data for the list of items and section headers in the Settings Menu.
     */
    class MenuDataProvider {

        public final String versionString = ComicPreferences.getInstance().getVersionString();
        public final String[] SECTIONS = {/*getResources().getString(R.string.display_language),*/ getResources().getString(R.string.display_overlay), getResources().getString(R.string.display_ondemand), /*getResources().getString(R.string.update_restore),*/ versionString};
        public final String[] SECTIONS_SUBSCRIPTION = {/*getResources().getString(R.string.display_language),*/ getResources().getString(R.string.display_overlay), /*getResources().getString(R.string.update_restore),*/ versionString};
        public final boolean HAS_SUB_MENU = true;

        public List<SettingsMenuSection> getAllData() {
            List<SettingsMenuSection> res = new ArrayList<SettingsMenuSection>();
            if (AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                for (int i = 0; i < SECTIONS.length; i++)
                    res.add(getSection(i));
            } else {
                for (int i = 0; i < SECTIONS_SUBSCRIPTION.length; i++)
                    res.add(getSection(i));
            }

            return res;
        }

        public List<SettingsMenuItem> getFlattenedData() {
            List<SettingsMenuItem> res = new ArrayList<SettingsMenuItem>();

            for (int i = 0; i < SECTIONS.length; i++)
                res.addAll(getSection(i).second);

            return res;
        }

        public SettingsMenuSection getSection(int index) {
            ArrayList<SettingsMenuItem> resultList = new ArrayList<SettingsMenuItem>();
            if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE) {
                switch (index) {

                    /**
                     * As per Bug #5359
                     * Remove "languages" option from Menu > Settings
                     *Archie will not be localized, at least not for some time.
                     *Please remove "Display product langauges" and "languages" from Menu > Settings.*/
//                    case 0:
//                        // Display Product Languages
//                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.languages), SettingsMenuItem.SettingsMenuItemType.Languages, HAS_SUB_MENU));
//                        break;
                    // 10/31/2013, Notifications are not needed.
//                case 1:
//                    // Notify me for:
//                    resultList.add(new SettingsMenuItem("New Releases", SettingsMenuItemType.NotifyMeFor, ComicPreferences.ComicPreferenceKeys.PREF_NOTIFY_FOR_NEW_RELEASES));
//                    resultList.add(new SettingsMenuItem("My Series Updates", SettingsMenuItemType.NotifyMeFor, ComicPreferences.ComicPreferenceKeys.PREF_NOTIFY_FOR_SERIES_UPDATES));
//                    break;
                    case 0:
                        // Display overlays for:
                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.purchased_books), SettingsMenuItem.SettingsMenuItemType.DisplayOverlays, ComicPreferences.ComicPreferenceKeys.PREF_SHOW_OVERLAY_ON_PURCHASED_BOOKS));
                        break;
                    case 1:
                        // On Demand Subscription
                        String value = ServerConfig.getDefault().getSubscriptionStatusString();
                        SettingsMenuItem menuItem = new SettingsMenuItem(value, SettingsMenuItem.SettingsMenuItemType.OnDemandSubscription);
                        // show subscription start date if user is subscribed
                        if (ServerConfig.getDefault().isSubscribed()) {
                            Date startDate = ServerConfig.getDefault().getSubscriptionStartDate();
                            if (startDate != null)
                                //localize
                                menuItem.secondary = "Started: " + mDateFormat.format(startDate);
                        }
                        resultList.add(menuItem);
                        break;
//                    case 2:
//                        // Update and Restore
//                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.restore_inapp_purchases), SettingsMenuItem.SettingsMenuItemType.RestorePurchases));
//                        break;
                    case 2:
                        // Version
                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.view_credits), SettingsMenuItem.SettingsMenuItemType.ViewCredits, HAS_SUB_MENU));
                        break;
                    default:
                        throw new IllegalArgumentException("Segment must be >= 0 and < 5, but was " + index);
                }
            }else{
                switch (index) {
                    /**
                     * As per Bug #5359
                     * Remove "languages" option from Menu > Settings
                     *Archie will not be localized, at least not for some time.
                     *Please remove "Display product langauges" and "languages" from Menu > Settings.
                     */
//                    case 0:
//                        // Display Product Languages
//                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.languages), SettingsMenuItem.SettingsMenuItemType.Languages, HAS_SUB_MENU));
//                        break;
                    case 0:
                        // Display overlays for:
                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.purchased_books), SettingsMenuItem.SettingsMenuItemType.DisplayOverlays, ComicPreferences.ComicPreferenceKeys.PREF_SHOW_OVERLAY_ON_PURCHASED_BOOKS));
                        break;

//                    case 1:
//                        // Update and Restore
//                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.restore_inapp_purchases), SettingsMenuItem.SettingsMenuItemType.RestorePurchases));
//                        break;
                    case 1:
                        // Version
                        resultList.add(new SettingsMenuItem(getResources().getString(R.string.view_credits), SettingsMenuItem.SettingsMenuItemType.ViewCredits, HAS_SUB_MENU));
                        break;
                    default:
                        throw new IllegalArgumentException("Segment must be >= 0 and < 5, but was " + index);
                }
            }
            return new SettingsMenuSection(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE ? SECTIONS[index]:SECTIONS_SUBSCRIPTION[index] , resultList);
        }
    }

}
