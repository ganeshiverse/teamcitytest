package com.babylon.activity.payments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.*;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.fragment.BaseFragment;
import com.babylon.fragment.payments.*;
import com.babylon.model.Patient;
import com.babylon.model.payments.*;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.HandlerLoaderListener;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.PreferencesManager;
import com.babylon.utils.UIUtils;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class PromotionTransactionActivity extends PaymentTransactionActivity
        implements PromotionConfirmationFragment.OnPromotionConfirmationListener,
        PaymentConfirmationFragment.OnPaymentConfirmationListener,
        CreditCardPromotionConfirmationFragment.OnCreditCardPromotionConfirmationListener,
        CreditCardPromotionConfirmationFragment.DataProtocol, CreditCardPromotionConfirmationFragment.OnCardListRequestClickLister,
        PromotionConfirmationFragment.DataProtocol,
        PaymentConfirmationFragment.DataProtocol {


    public static final int PROMOTION_REQUEST_CODE = 4573;
    public static final String REDEMPTION = "REDEMPTION";
    private ProgressDialog mProgressDialog;
    private PaymentCard selectedPaymentCard;
    private HandlerLoaderListener handlerLoaderListener;

    private Redemption redemption;
    private Subscription subscription;

    public static void startPromotionTransactionActivity(Activity activity, Redemption redemption) {
        if (redemption != null) {
            Intent intent = new Intent(activity, PromotionTransactionActivity.class);
            intent.putExtra(Keys.KEY_REDEMPTION, redemption);
            activity.startActivityForResult(intent,
                    PromotionTransactionActivity.PROMOTION_REQUEST_CODE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handlerLoaderListener = new HandlerLoaderListener(this);
        initialiseData();
        if(redemption.getPromotion().getIsCreditCardRequired() == null || redemption.getPromotion().getIsCreditCardRequired().compareToIgnoreCase("false") == 0)
        {
            startFragment(new PromotionConfirmationFragment(), false, PromotionConfirmationFragment.TAG);
        }
        else
        {
            //Credit card required for promotion so launch appropriate fragment
            startFragment(new CreditCardPromotionConfirmationFragment(), false,
                    CreditCardPromotionConfirmationFragment.TAG);
        }
    }

    private void initialiseData() {
        redemption = (Redemption) getIntent().getSerializableExtra(Keys.KEY_REDEMPTION);
        if (redemption.getSubscriptions() != null && !redemption.getSubscriptions().isEmpty()) {
            subscription = redemption.getSubscriptions().get(0);
            if (subscription != null) {
                orderDetails = new LinkedList<>();
                Plan plan = subscription.getPlan();
                if (plan != null) {
                    final OrderDetail orderDetails = new OrderDetail(plan.getTitle(),
                            subscription.getPrice());
                    if( subscription.getPlan().isDefault() &&redemption.getAppointments()!=null && redemption
                            .getAppointments().size()>0 ){
                        orderDetails.setPrice(Double.valueOf(redemption.getAppointments().get(0).getPrice()));
                    }
                    this.orderDetails.add(orderDetails);
                }
            }
        }
    }

    @Override
    public void onCreatingPaymentCardClick(NewPaymentCard paymentCard) {
        setProgressBarVisible(true);

        new AddingCreditCardPost().withEntry(paymentCard).execute(PromotionTransactionActivity.this,
                onCreatingPaymentCardRequestListener);
    }
    @Override
    public void onPaymentCardSelect(PaymentCard paymentCard) {
        this.selectedPaymentCard = paymentCard;

        App.getInstance().getPatient().setLastUsedCreditCard(this.selectedPaymentCard);

        onBackPressed();

    }

    @Override
    public void onPaymentCardDelete(int id) {
        if (selectedPaymentCard.getId().intValue() == id) {
            selectedPaymentCard = new PaymentCardViewController().getNewCreditCard();
        }
    }

    @Override
    public PaymentCard getPaymentCard() {

        if (selectedPaymentCard != null)
            return selectedPaymentCard;

        PaymentCard card = App.getInstance().getPatient().getLastUsedCreditCard();

        if (card != null)
            selectedPaymentCard = card;
        else
            selectedPaymentCard = new PaymentCardViewController().getNewCreditCard();

        return selectedPaymentCard;
    }


    private final RequestListener<PaymentCard> onCreatingPaymentCardRequestListener = new
            RequestListenerErrorProcessed<PaymentCard>() {
                @Override
                public void onRequestComplete(Response<PaymentCard> response, String errorMessage) {
                    if (!PromotionTransactionActivity.this.isFinishing()) {
                        setProgressBarVisible(false);
                        if (response.isSuccess()) {
                            selectedPaymentCard = response.getData();

                            App.getInstance().getPatient().setLastUsedCreditCard(selectedPaymentCard);

                            //handlerLoaderListener.sendEmptyMessage(HandlerLoaderListener.POP_BACK_HANDLER_CODE);
                            handlerLoaderListener.sendEmptyMessage(HandlerLoaderListener.BACK_PRESS_HANDLER_CODE);

                        } else {
                            AlertDialogHelper.getInstance().showMessage(PromotionTransactionActivity.this, response.getUserResponseError());
                        }
                    }
                }
            };

   /* @Override
    public void onNewPaymentCardClick() {
         startFragment(new AddingPaymentCardFragment(), AddingPaymentCardFragment.TAG);
       *//* Fragment fragment = getSupportFragmentManager().findFragmentByTag(PaymentCardsListFragment.TAG);
        if (fragment != null) {
            ((BaseFragment) fragment).setProgressBarVisible(true);
        }

        new CreditCardsListGet().execute(this, cardsListRequestListener);*//*
    }
*/



    @Override
    public void onPayClick()
    {

        //Mix Panel event
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(PromotionTransactionActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.PAYMENT_REQUESTED);

        mProgressDialog = UIUtils.getLoadingDialog(PromotionTransactionActivity.this);
        mProgressDialog.show();
        Plan plan = redemption.getSubscriptions().get(0).getPlan();
        if(redemption.getSubscriptions().get(0).getState().compareToIgnoreCase("pending")==0 && redemption
                .getSubscriptions().get(0).getPrice()!=0)
        {

           /*PaymentAppointmentPost.getPaymentAppointmentPost(getPaymentCard().getId(),
                   redemption.getAppointments().get(0).getId(),
                   Integer.parseInt(patient.getId())).execute(this,
                    onAppointmentPayingReceiver); */

            int patientId =  redemption.getSubscriptions().get(0).getPatientId();
            new SubscribePatch().withEntry(new SubscribePatch.BodyParams(getPaymentCard().getId(), patientId)).execute(this, onPayRequestListener);

        }
        else if (plan != null && plan.getId() != null) {
            new PaymentSubscribtionPost().withEntry(new PaymentSubscribtionPost.PlanParams(plan.getId(), patient.getId()))
                    .execute(this, new RequestListenerErrorProcessed<Subscription>() {
                        @Override
                        public void onRequestComplete(Response<Subscription> response,
                                                      String errorMessage) {
                            if (!PromotionTransactionActivity.this.isFinishing()) {
                                if (!TextUtils.isEmpty(errorMessage)) {
                                    Toast.makeText(PromotionTransactionActivity.this,
                                            errorMessage, Toast.LENGTH_SHORT).show();
                                    if (subscription != null && subscription.getPrice() != null) {
                                        startPaymentTransaction();
                                    }
                                } else {
                                    subscription = response.getData();
                                    //if(patient.getPromotion()!=null)
                                    // startFragment(new AppointmentConfirmationFragment(), false,
                                    //    AppointmentConfirmationFragment.TAG);
                                    if (subscription != null && subscription.getPrice() != null) {
                                        startPaymentConfirmationScreen();
                                    }
                                }
                            }
                        }
                    });
        }
    }


    private final RequestListenerErrorProcessed<Subscription> onPayRequestListener = new
            RequestListenerErrorProcessed<Subscription>() {
                @Override
                public void onRequestComplete(Response<Subscription> response, String errorMessage) {

                    if (!PromotionTransactionActivity.this.isFinishing()) {
                        isPaymentInProgress = false;

                        setProgressBarVisible(false);

                       if(mProgressDialog.isShowing()) {
                           mProgressDialog.hide();
                       }

                        if (TextUtils.isEmpty(errorMessage)) {

                            if(redemption.getSubscriptions().get(0).getPlan().isDefault())
                            {
                                PaymentAppointmentPost.getPaymentAppointmentPost(getPaymentCard().getId(),
                                        redemption.getAppointments().get(0).getId(),
                                        Integer.parseInt(patient.getId())).execute(PromotionTransactionActivity.this,
                                        onAppointmentPayingReceiver);


                            }
                            else
                            {
                                if(redemption.getAppointments()!=null && redemption.getAppointments().size()>0 ) {

                                /*    new AlliantsAppointmentGet(String.valueOf(redemption.getAppointments().get(0)
                                            .getId())).execute
                                            (PromotionTransactionActivity.this, onAppointmentRequestListener);*/
                                    //*****        or   *************8/
                                    PaymentAppointmentPost.getPaymentAppointmentPost(getPaymentCard().getId(),
                                            redemption.getAppointments().get(0).getId(),
                                            Integer.parseInt(patient.getId())).execute(PromotionTransactionActivity.this,
                                            onAppointmentPayingReceiver);
                                }
                                else
                                {
                                    startFragment(new PaymentConfirmationFragment(), PaymentConfirmationFragment.TAG);
                                }
                            }

                            //  startFragment(new PaymentConfirmationFragment(), PaymentConfirmationFragment.TAG);

                        } else {
                            Toast.makeText(PromotionTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };


    private final RequestListenerErrorProcessed<Appointment> onAppointmentRequestListener = new
            RequestListenerErrorProcessed<Appointment>() {
                @Override
                public void onRequestComplete(Response<Appointment> response, String errorMessage) {
                    if (!PromotionTransactionActivity.this.isFinishing()) {
                        if (response.isSuccess()) {
                            final Appointment appointment = response.getData();
                            final OrderDetail orderDetail = new OrderDetail(getString(R.string
                                    .appointment_order_details),
                                    appointment.getPrice());
                            startFragment(new PaymentConfirmationFragment(), PaymentConfirmationFragment.TAG);
                            //  finish();
                        } else {
                            Toast.makeText(PromotionTransactionActivity.this, errorMessage,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

    private final RequestListenerErrorProcessed<Appointment> onAppointmentPayingReceiver = new
            RequestListenerErrorProcessed<Appointment>() {
                @Override
                public void onRequestComplete(Response<Appointment> response, String errorMessage) {
                    if (!PromotionTransactionActivity.this.isFinishing()) {
                        setProgressBarVisible(false);
                        if(mProgressDialog.isShowing()) {
                            mProgressDialog.hide();
                        }
                        setRightButtonEnabled(true, R.color.subscription_menu_font_disable);

                        if (TextUtils.isEmpty(errorMessage))
                        {
                            //Mix Panel event
                            JSONObject props = new JSONObject();
                            MixPanelHelper helper = new MixPanelHelper(PromotionTransactionActivity.this,props,null);
                            helper.sendEvent(MixPanelEventConstants.PAYMENT_SUCCESS);

                     /*  startFragment(new AppointmentConfirmationFragment(), false,
                                    AppointmentConfirmationFragment.TAG);*/
                            startFragment(new PaymentConfirmationFragment(), PaymentConfirmationFragment.TAG);
                        }
                        else
                        {
                            //Mix Panel event
                            JSONObject props = new JSONObject();
                            MixPanelHelper helper = new MixPanelHelper(PromotionTransactionActivity.this,props,null);
                            helper.sendEvent(MixPanelEventConstants.PAYMENT_FAIL);

                            Toast.makeText(PromotionTransactionActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            };

    private void startPaymentConfirmationScreen() {
        startFragment(new PaymentConfirmationFragment(), PaymentConfirmationFragment.TAG);
    }

    /** Data protocols */

    @Override
    public Redemption getRedemption() {
        return redemption;
    }

    @Override
    public Double getSubscriptionPrice() {
        //return subscription.getPrice();

        //Changes for proimo code fix

        if(redemption==null){
            return subscription.getPrice();
        }
        else{
            return  redemption.getSubscriptions().get(0).getPrice();
        }
    }

    @Override
    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    @Override
    public Patient getPatient() {
        return patient;
    }

    @Override
    public String getOrderDetailsSubTitle() {
        //subtitle is not needed
        return null;
    }

    @Override
    public String getOrderTotal() {
        //total is not needed
        return null;
    }

    @Override
    public boolean IsSpecialist() {
        return false;
    }
    /** Event listeners */

    @Override
    public void onPromotionDone()
    {
            if (redemption.getSubscriptions() != null && !redemption.getSubscriptions().isEmpty()) {
                Subscription subscription = redemption.getSubscriptions().get(0);
                if(PreferencesManager.getInstance().isFirstLaunchPromo()) {
                    PreferencesManager.getInstance().setIsFirstPromo(false);
                    onPaymentDone();
                }
                else
                {
                    if (subscription.isFree() ) {


                        onPaymentDone();
                        // }

                    } else {
                        startPaymentTransaction();
                    }
                }

            } else {
                onPaymentDone();
            }


        }


    @Override
    public void onBackPressed()
    {
        if(CreditCardPromotionConfirmationFragment.isOnTop)
        {
            return;
        }
        else
        {
            super.onBackPressed();
        }

    }

    @Override
    public void onPaymentDone() {
        Intent i = new Intent();
        i.putExtra(REDEMPTION, redemption);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

    @Override
    public void onAddFamilyToPlanClick() {
        // TODO
    }

    @Override
    public void onAddressClicked() {

    }

    @Override
    public void onCardListRequestClickLister() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(PaymentCardsListFragment.TAG);
        if (fragment != null) {
            ((BaseFragment) fragment).setProgressBarVisible(true);
        }

        new CreditCardsListGet().execute(this, cardsListRequestListener);
    }

    private final RequestListener<PaymentCard[]> cardsListRequestListener = new
            RequestListenerErrorProcessed<PaymentCard[]>() {
                @Override
                public void onRequestComplete(Response<PaymentCard[]> response, String errorMessage) {
                    if (!PromotionTransactionActivity.this.isFinishing()) {
                        if (!TextUtils.isEmpty(errorMessage)) {
                            Toast.makeText(PromotionTransactionActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        } else {
                            processPaymentCardsResponse(response);
                        }
                    }
                }

                private void processPaymentCardsResponse(Response<PaymentCard[]> response) {
                    if (response.getData().length >= 1)
                    {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag(PaymentCardsListFragment.TAG);
                        if (fragment == null) {
                            fragment = new PaymentCardsListFragment();
                        } else {
                            ((BaseFragment) fragment).setProgressBarVisible(false);
                        }

                        startFragment(fragment, PaymentCardsListFragment.TAG);
                    } else {
                        startFragment(new AddingPaymentCardFragment(), AddingPaymentCardFragment.TAG);
                    }
                }
            };
}
