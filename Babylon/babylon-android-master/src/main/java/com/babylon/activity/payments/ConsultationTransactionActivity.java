package com.babylon.activity.payments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.api.request.DoctorBiographyGet;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.PaymentAppointmentPost;
import com.babylon.api.request.payments.SubscribePatch;
import com.babylon.fragment.payments.AppointmentConfirmationFragment;
import com.babylon.fragment.payments.DoctorBiographyFragment;
import com.babylon.fragment.payments.OrderDetailsListFragment;
import com.babylon.model.Doctor;
import com.babylon.model.Patient;
import com.babylon.model.payments.Appointment;
import com.babylon.model.payments.OrderDetail;
import com.babylon.model.payments.Subscription;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.CalendarEventCreator;
import com.babylon.utils.DateUtils;
import com.babylon.utils.MixPanelEventConstants;
import com.babylon.utils.MixPanelHelper;
import com.babylon.utils.UIUtils;
import com.babylonpartners.babylon.HomePageActivity;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ConsultationTransactionActivity extends PaymentTransactionActivity implements
        AppointmentConfirmationFragment.OnAppointmentConfirmationListener,
        AppointmentConfirmationFragment.DataProtocol, DoctorBiographyFragment.DataProtocol,OrderDetailsListFragment.OrderAddressFragmentListener {

    private Appointment appointment;
    private Doctor doctor;
    private static String IS_SPECIALIST="Is_Specialist";
    private Patient patient_detail ;
    boolean mIsSpecialist=false;
    private ProgressDialog mProgressDialog;
    public static void startActivity(Activity activity, OrderDetail orderDetail, Appointment appointment, Patient
            patient,boolean IsSpecialist) {
        Intent intent = new Intent(activity, ConsultationTransactionActivity.class);
        intent.putExtra(Keys.KEY_ORDER_DETAILS, orderDetail);
        intent.putExtra(Keys.APPOINTMENT_KEY, appointment);
        intent.putExtra(PaymentTransactionActivity.PATIENT_EXTRA, patient);
        intent.putExtra(IS_SPECIALIST,IsSpecialist);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViewById(R.id.base_layout).setBackgroundResource(R.drawable.monitor_me_background);

        appointment = (Appointment) getIntent().getSerializableExtra(Keys.APPOINTMENT_KEY);
        mIsSpecialist=getIntent().getBooleanExtra(IS_SPECIALIST,false);
        final OrderDetail orderDetail = getIntent().getParcelableExtra(Keys.KEY_ORDER_DETAILS);

        orderDetails = new LinkedList<>();
       // getOrderDetails().add(orderDetail);

     if (orderDetail.isFree())
     {
            startFragment(new AppointmentConfirmationFragment(), false, AppointmentConfirmationFragment.TAG);
         getOrderDetails().add(orderDetail);
        } else {
           if(! patient.getPlan().isDefault()){
               if(appointment.getIsSpecialist()) {

                   getOrderDetails().add(orderDetail);
                   startFragment(new OrderDetailsListFragment(), false, OrderDetailsListFragment.TAG);
               }
               else {
                   orderDetail.setPrice(patient.getSubscription().getPrice());
                   orderDetail.setName("Subscription");
                   getOrderDetails().add(orderDetail);
                   startFragment(new OrderDetailsListFragment(), false, OrderDetailsListFragment.TAG);
               }
           }
         else{
               getOrderDetails().add(orderDetail);
               startFragment(new OrderDetailsListFragment(), false, OrderDetailsListFragment.TAG);
           }

      }
    }

    @Override
    public void onPayClick()
    {
        setProgressBarVisible(true);

        //Mix Panel event
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(ConsultationTransactionActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.PAYMENT_REQUESTED);

        // private ProgressDialog mProgressDialog;
        mProgressDialog = UIUtils.getLoadingDialog(this);
        mProgressDialog.show();
        if(patient.getPromotion()==null || (appointment!=null && appointment.getIsPromotion()==false)) {

            setRightButtonEnabled(false, R.color.subscription_menu_font_disable);
            PaymentAppointmentPost.getPaymentAppointmentPost(getPaymentCard().getId(), appointment.getId(), Integer.parseInt(patient.getId())).execute(this,
                    onAppointmentPayingReceiver);
        }
        else {

            int patientId = Integer.parseInt(patient.getId());
            new SubscribePatch().withEntry(new SubscribePatch.BodyParams(getPaymentCard().getId(), patientId))
                    .execute(this, onPayRequestListener);

        }
    }


    private final RequestListenerErrorProcessed<Subscription> onPayRequestListener = new
            RequestListenerErrorProcessed<Subscription>()
            {
                @Override
                public void onRequestComplete(Response<Subscription> response, String errorMessage) {

                    PaymentAppointmentPost.getPaymentAppointmentPost(getPaymentCard().getId(), appointment.getId(),
                            Integer.parseInt(patient.getId())).execute(ConsultationTransactionActivity.this,

                            onAppointmentPayingReceiver);

                }
            };

    @Override
    public Doctor getDoctor() {
        return doctor;
    }

    @Override
    public void onDoctorBiographyClick() {
        setProgressBarVisible(true);
        DoctorBiographyGet.getAlliantsDoctorBiography(appointment.getConsultantId()).execute
                (this, onDoctorRequestListener);
    }

    @Override
    public Appointment getAppointment() {
        return appointment;
    }

    @Override
    public void onAddCalendarEventClick() {
        final String startTime = appointment.getStartTime();
        final long millisecondsWhen = DateUtils.timeToLong(startTime, DateUtils.YT_DATE_FORMAT_ZZZ);
        final long timeSlotInMillis = TimeUnit.MINUTES.toMillis(appointment.getSlotSize());
        final String title = String.format(getString(R.string.appointment_calendar_title),
                appointment.getConsultName());

        new CalendarEventCreator(millisecondsWhen, millisecondsWhen + timeSlotInMillis, title).startIntent(this);
    }

    @Override
    public void onAddBabylonContactClick() {
        AlertDialogHelper.getInstance().showAddContactDialog(this, false);
    }

    @Override
    public void onAppointmentDone() {
        Intent i = new Intent(ConsultationTransactionActivity.this, HomePageActivity.class);
        i.putExtra(Keys.SHOULD_START_HOME_ACTIVITY, true);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                .FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }
    @Override
    public boolean IsSpecialist() {
        return mIsSpecialist;
    }




    @Override
    public String getOrderDetailsSubTitle() {
        //subtitle is not needed
        return null;
    }

    @Override
    public Patient getPatient() {
        return patient;
    }

    @Override
    public String getOrderTotal() {
        //total is not needed
        return null;
    }

    @Override
    public void onAddFamilyToPlanClick() {

    }





    private final RequestListenerErrorProcessed<Appointment> onAppointmentPayingReceiver = new
            RequestListenerErrorProcessed<Appointment>() {
                @Override
                public void onRequestComplete(Response<Appointment> response, String errorMessage)
                {
                    if (!ConsultationTransactionActivity.this.isFinishing())
                    {
                        setProgressBarVisible(false);
                        mProgressDialog.hide();
                        setRightButtonEnabled(true, R.color.subscription_menu_font_disable);

                        if (TextUtils.isEmpty(errorMessage))
                        {
                            //Mix Panel event
                            JSONObject props = new JSONObject();
                            MixPanelHelper helper = new MixPanelHelper(ConsultationTransactionActivity.this,props,null);
                            helper.sendEvent(MixPanelEventConstants.PAYMENT_SUCCESS);

                            startFragment(new AppointmentConfirmationFragment(), false,
                                    AppointmentConfirmationFragment.TAG);
                        }
                        else
                        {
                            //Mix Panel event
                            JSONObject props = new JSONObject();
                            MixPanelHelper helper = new MixPanelHelper(ConsultationTransactionActivity.this,props,null);
                            helper.sendEvent(MixPanelEventConstants.PAYMENT_FAIL);

                            Toast.makeText(ConsultationTransactionActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            };

    private final RequestListenerErrorProcessed<Doctor> onDoctorRequestListener = new
            RequestListenerErrorProcessed<Doctor>() {
                @Override
                public void onRequestComplete(Response<Doctor> response, String errorMessage) {
                    if (!ConsultationTransactionActivity.this.isFinishing()) {
                        setProgressBarVisible(false);

                        doctor = response.getData();

                        if (TextUtils.isEmpty(errorMessage) && doctor != null) {
                            startFragment(new DoctorBiographyFragment(), DoctorBiographyFragment.TAG);
                        } else {
                            Toast.makeText(ConsultationTransactionActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

    @Override
    public void onAddressClicked() {



    }


}
