package com.iversecomics.json;

/**
 * The JSONException is thrown by the JSON.org classes when things are amiss.
 * @author JSON.org
 * @version 2008-09-18
 */
public class JSONException extends Exception {
    private static final long serialVersionUID = 0;

    /**
     * Constructs a JSONException with an explanatory message.
     * @param message Detail about the reason for the exception.
     */
    public JSONException(String message) {
        super(message);
    }

    /**
     * Constructs a JSONException with an explanatory message.
     * @param message Detail about the reason for the exception.
     */
    public JSONException(String format, Object... args) {
        super(String.format(format, args));
    }

    public JSONException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public JSONException(Throwable throwable) {
        super(throwable);
    }
}
