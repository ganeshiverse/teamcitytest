package com.iversecomics.otto.event;

/**
 * Created by jeffaoliver on 12/27/13.
 */
public class ServerConfigErrorEvent {

    private String errorMessage;

    public ServerConfigErrorEvent(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
