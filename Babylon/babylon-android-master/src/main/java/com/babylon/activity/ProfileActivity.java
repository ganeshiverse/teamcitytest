package com.babylon.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.api.request.PatientGet;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.controllers.BirthdayViewController;
import com.babylon.controllers.OnDateSetListener;
import com.babylon.dialog.OkDialogFragment;
import com.babylon.enums.Gender;
import com.babylon.fragment.WeightParametersFragment;
import com.babylon.model.Patient;
import com.babylon.model.Region;
import com.babylon.utils.Constants;
import com.babylon.utils.PreferencesManager;
import com.babylon.validator.ValidationController;
import com.babylon.validator.ValueBetweenValidator;
import com.babylon.view.CustomEditText;
import com.babylon.view.GenderView;
import com.babylon.view.SmokerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class ProfileActivity extends BaseActivity {

    public enum ProfileField {
        HEIGHT,
        WEIGHT,
        GENDER,
        SMOKE,
        DATE_OF_BIRTH
    }

    private GenderView genderView;
    private SmokerView smokerView;

    private CustomEditText weightEditText;
    private CustomEditText heightEditText;

    private LinearLayout weightLyout;
    private LinearLayout heightLyout;
    private LinearLayout dateLyout;
    private LinearLayout genderLyout;


    private Date currentBirthDate;
    private BirthdayViewController birthdayViewController;



    private ValidationController validationController = new ValidationController();

    /**
     * @return an array of all fields which need to be set on current profile screen
     */
    public abstract ProfileField[] getFilledProfileFields();

    public abstract ProfileField[] getRequiredFilledProfileFields();

    public abstract void setTopBar();

    public abstract int getLayoutResID();

    public abstract int getBackgroundResId();

    /**
     * Determine actions after update user profile
     */
    public abstract void onProfileSent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResID());

        new PatientGet().execute(this, onPatientRequestListener);

        smokerView = (SmokerView) findViewById(R.id.view_smoker);
        genderView = (GenderView)findViewById(R.id.gender_view);

        weightLyout=(LinearLayout)findViewById(R.id.layout_weight);
        heightLyout=(LinearLayout)findViewById(R.id.layout_height);
        dateLyout=(LinearLayout)findViewById(R.id.dateLayout);
        genderLyout=(LinearLayout)findViewById(R.id.genderLayout);
        weightEditText = (CustomEditText) findViewById(R.id.edit_text_weight);
        heightEditText = (CustomEditText) findViewById(R.id.edit_text_height);
        genderView.setVisibility(View.GONE);
      //  smokerView.setVisibility(View.GONE);

        showUserData();

        if (isProfileFieldNeed(ProfileField.HEIGHT)) {
            initHeightEditText();
        }

        if (isProfileFieldNeed(ProfileField.WEIGHT)) {
            initWeightEditText();
        }
        showRequiredFieldsOnly();


        setTopBar();

        findViewById(R.id.layout_parent_scroll_view).setBackgroundResource(getBackgroundResId());
    }

    private void initWeightEditText() {
        weightEditText = (CustomEditText) findViewById(R.id.edit_text_weight);
        weightEditText.setVisibility(View.GONE);
        ValueBetweenValidator weightValueValidator = new ValueBetweenValidator(null,
                getString(R.string.validator_field_value_between, WeightParametersFragment.MIN_WEIGHT,
                        WeightParametersFragment.MAX_WEIGHT),
                WeightParametersFragment.MIN_WEIGHT, WeightParametersFragment.MAX_WEIGHT);

        validationController.addValidation(weightEditText, weightValueValidator);

        weightEditText.validationAfterTextChanged(validationController);
    }

    private void initHeightEditText() {
        heightEditText = (CustomEditText) findViewById(R.id.edit_text_height);
        heightEditText.setVisibility(View.GONE);
        ValueBetweenValidator heightValueValidator = new ValueBetweenValidator(null,
                getString(R.string.validator_field_value_between, WeightParametersFragment.MIN_HEIGHT,
                        WeightParametersFragment.MAX_HEIGHT),
                WeightParametersFragment.MIN_HEIGHT, WeightParametersFragment.MAX_HEIGHT);

        validationController.addValidation(heightEditText, heightValueValidator);

        heightEditText.validationAfterTextChanged(validationController);
    }

    private void setDateOfBirth(Date birthday, int regionId) {
        final TextView birthDateTextView = (TextView) findViewById(R.id.dateofBirthEdt);
        final EditText birthDateErrorEditText = (EditText) findViewById(R.id.edit_text_birthday_error);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BirthdayViewController.BIRTH_DATE_FORMAT,
                Locale.getDefault());
        birthDateTextView.setText(simpleDateFormat.format(birthday));
        currentBirthDate = birthday;

        birthDateErrorEditText.setInputType(InputType.TYPE_NULL);
        birthdayViewController = new BirthdayViewController(this, birthDateTextView,
                new OnDateSetListener() {
                    @Override
                    public void onDateSet(Date date) {
                        birthdayViewController.validateBirthday(date, birthDateErrorEditText);
                        currentBirthDate = date;

                    }
                }, currentBirthDate);
        setBirthdayRegionValidation(regionId, birthday, birthDateErrorEditText);
    }

    private void setBirthdayRegionValidation(int regionId, Date birthday,
                                             EditText birthDateErrorEditText) {
        Region[] regions = PreferencesManager.getInstance().getRegions();
        if (regions != null && regions.length != 0 && regionId != 0) {
            Region patientRegion = null;
            for (Region region : regions) {
                if (region.getId() != null && region.getId() == regionId) {
                    patientRegion = region;
                    break;
                }
            }
            if (patientRegion != null) {
                Integer minAge = patientRegion.getMinimumAge();
                if (minAge != null && minAge != Constants.REGION_MIN_AGE_DEFAULT) {
                    birthdayViewController.setMinAge(minAge);
                    birthdayViewController.validateBirthday(birthday, birthDateErrorEditText);
                }
            }
        }
    }

    private void showUserData() {
        Patient patient = App.getInstance().getPatient();
        final String smoker = patient.getSmokingStatus();
        final float weight = patient.getWeight();
        final float height = patient.getHeight();
        final String gender = patient.getGender();
        final Date birthday = patient.getBirthday();
        final int regionId = patient.getRegionId();

        if (isProfileFieldNeed(ProfileField.WEIGHT) && weight != 0 && weightEditText != null)
        {

            weightEditText.setText(String.valueOf(weight));
        }

        if (isProfileFieldNeed(ProfileField.HEIGHT) && height != 0 && heightEditText!= null)
        {
            heightEditText.setText(String.valueOf(height));
        }

        if(isProfileFieldNeed(ProfileField.GENDER) && !TextUtils.isEmpty(gender) && genderView != null)
        {
            genderView.setGender(Gender.get(gender));
        }

        if (isProfileFieldNeed(ProfileField.SMOKE) && smokerView!= null)
        {
            smokerView.setSmokerStatus(smoker);
        }

        if (isProfileFieldNeed(ProfileField.DATE_OF_BIRTH) && birthday != null) {
            setDateOfBirth(birthday, regionId);
        }
    }

    public void onSendProfileClick(View view) {
        if (isFieldsFilled()) {
            savePatient();
            setResult(Activity.RESULT_OK);
            onProfileSent();
        }
    }

    private boolean isFieldsFilled() {
        boolean isFieldsFilled = true;
       // for (ProfileField field : getFilledProfileFields()) {
        for (ProfileField field : getRequiredFilledProfileFields()) {
            switch (field) {
                case HEIGHT:
                    isFieldsFilled = validationController.performValidation();
                    break;
                case WEIGHT:
                    isFieldsFilled = validationController.performValidation();
                    break;
                case DATE_OF_BIRTH:
                    try {
                        isFieldsFilled = birthdayViewController.isValid(currentBirthDate);
                    }
                    catch (Exception ex)
                    {
                        Log.d("TAG",ex.toString());
                    }
                    break;
                case GENDER:
                    isFieldsFilled = !TextUtils.isEmpty(genderView.getSelectedGender());
                    if (!isFieldsFilled) {
                        OkDialogFragment.getInstance(R.string.gender_is_required).show(getSupportFragmentManager(),
                                OkDialogFragment.TAG);
                    }
                    break;
                default:
                    break;
            }
            if (!isFieldsFilled) {
                break;
            }
        }
        return isFieldsFilled;
    }

    private boolean showRequiredFieldsOnly() {
        boolean isFieldsFilled = true;
        for (ProfileField field : getRequiredFilledProfileFields()) {
            switch (field) {
                case HEIGHT:
                    heightEditText.setVisibility(View.VISIBLE);
                    heightLyout.setVisibility(View.VISIBLE);
                    break;
                case WEIGHT:
                    weightEditText.setVisibility(View.VISIBLE);
                    weightLyout.setVisibility(View.VISIBLE);
                    break;
                case DATE_OF_BIRTH:
                    if(currentBirthDate!=null) {
                        isFieldsFilled = birthdayViewController.isValid(currentBirthDate);
                        dateLyout.setVisibility(View.VISIBLE);
                    }
                    break;
                case GENDER:

                    genderLyout.setVisibility(View.VISIBLE);
                    genderView.setVisibility(View.VISIBLE);
                   /* isFieldsFilled = !TextUtils.isEmpty(genderView.getSelectedGender());
                    if (!isFieldsFilled) {
                        OkDialogFragment.getInstance(R.string.gender_is_required).show(getSupportFragmentManager(),
                                OkDialogFragment.TAG);
                    }*/
                    break;
                default:
                    break;
            }
            if (!isFieldsFilled) {
                break;
            }
        }
        return isFieldsFilled;
    }

    private void savePatient() {
        final Patient patient = App.getInstance().getPatient();

        for (ProfileField field : getRequiredFilledProfileFields()) {
            switch (field) {
                case HEIGHT:
                    heightEditText.setVisibility(View.VISIBLE);
                    final String height = heightEditText.getText();
                    patient.setHeight(Float.valueOf(height));
                    break;
                case WEIGHT:
                    weightEditText.setVisibility(View.VISIBLE);
                    final String weight = weightEditText.getText();
                    patient.setWeight(Float.valueOf(weight));
                    break;
                case DATE_OF_BIRTH:

                    patient.setBirthday(currentBirthDate);
                    break;
                case SMOKE:
                    patient.setSmokingStatus(smokerView.getNewServerSmokerStatus());
                    break;
                case GENDER:
                    patient.setGender(genderView.getSelectedGender());
                    break;
                default:
                    break;
            }
        }

        patient.setIsUpdated(true);
        App.getInstance().setPatient(patient);
    }

    private final RequestListener<Patient> onPatientRequestListener = new RequestListener<Patient>() {
        @Override
        public void onRequestComplete(Response<Patient> response) {
            if(response.isSuccess()) {
                Patient patient = response.getData();
                if (patient != null) {
                    App.getInstance().setPatient(patient);
                    showUserData();
                }
            }
        }
    };

    private boolean isProfileFieldNeed(ProfileField profileField) {
        boolean isNeed = false;
        for (ProfileField field : getFilledProfileFields()) {
            if (field.equals(profileField)) {
                isNeed = true;
                break;
            }
        }
        return isNeed;
    }
}
