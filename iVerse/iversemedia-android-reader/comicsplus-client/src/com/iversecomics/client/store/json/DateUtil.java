package com.iversecomics.client.store.json;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;
import com.iversecomics.util.text.StringUtils;

public final class DateUtil {
    private static final Logger  LOG = LoggerFactory.getLogger(DateUtil.class);
    public static final TimeZone UTC = TimeZone.getTimeZone("UTC");

    public static long getUTCNow() {
        return Calendar.getInstance(UTC).getTimeInMillis();
    }

    /**
     * Parse a date timestamp in the format that the server sends.
     * <p>
     * 
     * <pre>
     *   Server Timestamp: "Wed, 02 Mar 2011 05:11:13 GMT"
     *   Date/Time Format: "EE, dd MMM yyyy HH:mm:ss zz"
     * </pre>
     * 
     * @param dateStr
     *            the date timestamp.
     * @return the time in millis since epoch UTC for the provided timestamp, or 0 if unable to parse the timestamp.
     */
    public static long parseServerTimestamp(String dateStr) {
        if (TextUtils.isEmpty(dateStr)) {
            return 0;
        }

        // Example string to parse "Wed, 02 Mar 2011 05:11:13 GMT"
        SimpleDateFormat timestampFormat = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss zz");
        try {
            Date timestamp = timestampFormat.parse(dateStr);
            Calendar cal = Calendar.getInstance();
            cal.setTime(timestamp);
            cal.setTimeZone(UTC);
            return cal.getTimeInMillis();
        } catch (ParseException e) {
            LOG.warn(e, "Unable to parse timestamp: " + dateStr);
            return 0;
        }
    }
    
	public static synchronized Date parseDateString(final String dateStr)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss zz");
		
		try
		{
			return sdf.parse(dateStr);
		}
		catch (Exception e)
		{
			LOG.warn(e, "Unable to parse timestamp: " + dateStr);
		}
		
		return new Date();
	 }
}
