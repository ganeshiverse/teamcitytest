package com.babylon.api.request.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Response<T> implements Serializable {

    public static final int RESULT_ERROR = 400;
    public static final int RESULT_OK = 200;

    private Integer status = RESULT_ERROR;

    private List<Error> errors;
    private T data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<Error> getErrors() {
        return errors == null ? errors = new ArrayList<Error>() : errors;
    }

    public void addError(Error e) {
        getErrors().add(e);
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "Response{"
                + "status=" + status
                + ", errors='" + errors + '\''
                + ", data=" + data
                + '}';
    }

    public boolean isSuccess() {
        return status < 300;
    }

    public String getUserResponseError() {
        return errors != null && errors.size() > 0 ?
                errors.get(0).getUserMessage() : null;
    }

    public String getResponseError() {
        return errors != null && errors.size() > 0 ?
                errors.get(0).getMessage() : null;
    }
}
