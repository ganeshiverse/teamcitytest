package com.iversecomics.ui.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.iversecomics.app.AppConstants;
import com.iversecomics.app.ComicApp;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.store.ComicStore;
import com.iversecomics.client.store.OnDemandOfflineQueue;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.events.OnComicClickListener;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.client.store.tasks.VerifyPurchaseTask;
import com.iversecomics.client.util.AndroidInfo;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.logging.TimeLogger;
import com.iversecomics.otto.OttoBusProvider;
import com.iversecomics.otto.event.ServerConfigErrorEvent;
import com.iversecomics.store.AppStoreAdapter;
import com.iversecomics.store.AppStoreAdapterLocator;
import com.iversecomics.ui.ComicDetailsFragment;
import com.iversecomics.ui.ComicListFragment;
import com.iversecomics.ui.ComicListFragment.ComicListListener;
import com.iversecomics.ui.DownloadsFragment;
import com.iversecomics.ui.LoginActivity;
import com.iversecomics.ui.MyComicsFragment;
import com.iversecomics.ui.MyPurchasesFragment;
import com.iversecomics.ui.PublishersFragment;
import com.iversecomics.ui.SearchResultsFragment;
import com.iversecomics.ui.SignUpActivity;
import com.iversecomics.ui.featured.FeaturedComicsFragment;
import com.iversecomics.ui.main.event.ShowGroupEvent;
import com.iversecomics.ui.main.event.ShowPublisherEvent;
import com.iversecomics.ui.menu.MenuFragment;
import com.iversecomics.ui.menu.MenuFragment.MenuClickListener;
import com.iversecomics.ui.menu.settings.SettingsFragment;
import com.iversecomics.ui.widget.NavigationBar;
import com.iversecomics.util.Util;
import com.iversecomics.util.crashreporter.PostMortemReportExceptionHandler;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


public class MainActivity extends FragmentActivity implements MenuClickListener,
        NavigationBar.NavigationListener, ComicListListener, OnComicClickListener, AppStoreAdapterLocator, SlidingMenu.OnClosedListener{

    private SlidingMenu mSlidingMenu;
    private AbstractAppStoreAdapter appStoreAdapter = new AppStoreAdapter();
    private ProgressDialog progressDialog;
    protected PostMortemReportExceptionHandler mDamageReport = new PostMortemReportExceptionHandler(this);
    private ConnectivityReceiver receiver = null;

    private Bundle savedInstanceView=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(AppConstants.ENABLE_CRASH_REPORT) {
            mDamageReport.initialize();
        }
        receiver = new ConnectivityReceiver();
        TimeLogger.TimePoint startTime = TimeLogger.newTimePoint("create main activity");
        appStoreAdapter.onCreate(savedInstanceState, this);

        setContentView(R.layout.page_activity);
        initSlidingMenu();
        savedInstanceView = savedInstanceState;
        if (savedInstanceState == null) {
            //First launch, so go to featured page.
            if(ConnectionHelper.isConnectedToInternet(MainActivity.this)) {
                showTopFragment(new FeaturedComicsFragment(), false);
            }else{
                // if no internet connection available, we will navigate the user to "MyComicsFragment".
                //showTopFragment(new MyComicsFragment(), false);
            }
            }
        startTime.logEnd();
    }

    private void initSlidingMenu() {
        // initialize sliding menu
        mSlidingMenu = new SlidingMenu(this);
        mSlidingMenu.setMode(SlidingMenu.LEFT);
        mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        // menu.setShadowWidthRes(R.dimen.shadow_width);
        // menu.setShadowDrawable(R.drawable.shadow);
        // set the width of the overlapping top fragment when the menu is open
        mSlidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        mSlidingMenu.setFadeDegree(0.35f);
        mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        mSlidingMenu.setMenu(R.layout.menu_frame);
        mSlidingMenu.setOnClosedListener(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, new MenuFragment())
                .commit();
    }

    void dismissProgressDialog() {
        if (progressDialog != null && !isFinishing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(receiver ==null){
            receiver = new ConnectivityReceiver();
            registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }else{
            registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        OttoBusProvider.getInstance().register(this);
        appStoreAdapter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(receiver !=null) {
            this.unregisterReceiver(receiver);
            receiver = null;
        }
        OttoBusProvider.getInstance().unregister(this);
        appStoreAdapter.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        appStoreAdapter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        appStoreAdapter.onStop();
    }

    @Override
    protected void onDestroy() {
        mDamageReport.restoreOriginalHandler();
        mDamageReport=null;
        super.onDestroy();
        appStoreAdapter.onDestroy();
    }

    public void showTopFragment(Fragment newFrag) {
        showTopFragment(newFrag, true);
    }

    private void showTopFragment(Fragment newFrag, boolean fromRight) {

        if (mSlidingMenu.isMenuShowing())
            mSlidingMenu.showContent(true);

        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();

        if (fromRight)
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        else
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);

        ft.addToBackStack(null);
        ft.replace(R.id.pageContainer, newFrag);
        ft.commit();
    }

    private void showMenuFragment(Fragment newFrag) {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.addToBackStack(null);
        ft.add(R.id.menu_frame, newFrag);
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.menu_frame);
        if (!(fragment instanceof MenuFragment)) {
            super.onBackPressed();
            return;
        }
        if (mSlidingMenu.isMenuShowing()) {
            mSlidingMenu.showContent(true);
            return;
        }
        // we are closing the application onBackpressed when device is in Airplane mode.
        if((fragment instanceof MenuFragment) && !ConnectionHelper.isConnectedToInternet(MainActivity.this)){
            finish();
            return;
        }

        // pop fragments unless we're already at the last fragment (which should always be FeatureContentFragment)
        if (getSupportFragmentManager().getBackStackEntryCount() > 1)
            getSupportFragmentManager().popBackStack();
        else
            finish();
    }

    public SlidingMenu getSlidingMenu() {
        return mSlidingMenu;
    }

    @Override
    public void onPromotionClick(Promotion promo) {
        showTopFragment(ComicListFragment.newInstance(promo));
    }

    @Override
    public void onGroupClick(Group group) {
        ComicStore comicStore = IverseApplication.getApplication().getComicStore();

        // show sub-menu if applicable (this is for "Digests" titles)
        if (comicStore.getDatabaseApi().groupHasSubGroups(group.getGroupId()))
            showMenuFragment(MenuFragment.newInstanceForGroup(group.getGroupId(), group.getName()));
        else
            showTopFragment(ComicListFragment.newInstance(group));
    }

    @Override
    public void onPublisherClick(Publisher publisher) {
        ComicStore comicStore = IverseApplication.getApplication().getComicStore();

        // show sub-menu if applicable (this is for publisher groups)
        if (comicStore.getDatabaseApi().publisherHasGroups(publisher.getPublisherId()))
            showMenuFragment(MenuFragment.newInstanceForPublisherId(publisher.getPublisherId(), publisher.getName()));
        else
            showTopFragment(ComicListFragment.newInstanceForPublisher(publisher));
    }

    @Override
    public void onFeaturedClick() {
        showTopFragment(new FeaturedComicsFragment(), false);
    }

    @Override
    public void onDemandClick() {
        showTopFragment(FeaturedComicsFragment.newInstance(true));
    }

    @Override
    public void onNewReleasesClick() {
        showTopFragment(ComicListFragment.newInstanceNewReleases());
    }

    @Override
    public void onTopPaidClick() {
        showTopFragment(ComicListFragment.newInstanceTopPaid());
    }

    @Override
    public void onTopFreeClick() {
        showTopFragment(ComicListFragment.newInstanceTopFree());
    }

    @Override
    public void onSettingsClick() {
        showMenuFragment(new SettingsFragment());
    }

    @Override
    public void onStoreSearch(String searchTerm) {
//        SearchResultsFragment_Old frag = SearchResultsFragment_Old.newInstance(searchTerm);
        SearchResultsFragment frag = SearchResultsFragment.newInstance(searchTerm);
        frag.setOnComicClickListener(this);
        showTopFragment(frag);
    }

    @Override
    public void onLoginClick() {
        // Offline change
        if(ConnectionHelper.isConnectedToInternet(MainActivity.this)) {
            startActivity(new Intent(this, LoginActivity.class));
        }else {
            ConnectionHelper.showNoInternetMessage();
        }
    }

    @Override
    public void onSignUpClick() {
        // Offline change
        if(ConnectionHelper.isConnectedToInternet(MainActivity.this)) {
            startActivity(new Intent(this, SignUpActivity.class));
        }else {
            ConnectionHelper.showNoInternetMessage();
        }
    }

    @Override
    public void onSendFeedbackClick() {
        if(ConnectionHelper.isConnectedToInternet(MainActivity.this)) {
            Util.sendFeedBack(MainActivity.this);
        }else {
            ConnectionHelper.showNoInternetMessage();
        }
    }

    @Override
    public void onLogoutClick() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.log_off_title)
                .setPositiveButton(
                        R.string.log_off_button,
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ComicApp.getInstance().logout();
                            }
                        }
                )
                .setNegativeButton(R.string.cancel, null)
                .create()
                .show();
    }

    @Override
    public void onMyPurchasesClick() {
        showMenuFragment(new MyPurchasesFragment());
    }

    @Override
    public void onDownloadsClick() {
        showMenuFragment(new DownloadsFragment());
    }

    // NavigationListener
    @Override
    public void onMenuToggle() {
        if (mSlidingMenu != null)
            mSlidingMenu.toggle();
    }

    @Override
    public void onMyComicsClick() {
        showTopFragment(new MyComicsFragment());
    }

    @Override
    public void onAppIconCkick() {
        showTopFragment(new FeaturedComicsFragment(), false);
    }

    @Override
    public void onBackClick() {
        onBackPressed();
    }

    @Override
    public void onComicClicked(Comic comic) {
        ComicDetailsFragment comicDetailsFragment = ComicDetailsFragment.newInstanceForComicBundleName(comic.getComicBundleName());
        comicDetailsFragment.setAppStoreAdapter(appStoreAdapter);
        showTopFragment(comicDetailsFragment);
    }

    /**
     * ******** OnComicClickListener **************
     */
    @Override
    public void onComicClick(Fragment fragment, String comicBundleName) {
        ComicDetailsFragment detailsFrag = ComicDetailsFragment.newInstanceForComicBundleName(comicBundleName);
        detailsFrag.setAppStoreAdapter(appStoreAdapter);
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.addToBackStack(null);
        // not using showTopFragment here because we want to add the fragment, not replace the current top fragment.
        ft.add(R.id.pageContainer, detailsFrag);
        ft.commit();
    }

    @Override
    public AbstractAppStoreAdapter getAppStoreAdapter() {
        return appStoreAdapter;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (appStoreAdapter.canHandle(requestCode)) {
            appStoreAdapter.handleActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    /**
     * This method is used to get the "onDemandOfflineQueue" books download into device.
     **/
    private void reconcileOfflineQueue() {
        //Next, queue up and items that were in the server's queue that we don't have downloaded
        ArrayList<OnDemandOfflineQueue> onDemandOfflineQueue = ServerConfig.getDefault().getOnDemandOfflineQueue();
        if (onDemandOfflineQueue != null && onDemandOfflineQueue.size() > 0) {
            for (int i = 0; i < onDemandOfflineQueue.size(); i++) {
                final MyComicsModel model = new MyComicsModel(this);
                final MyComic myComic = model.getComicByBundleName(onDemandOfflineQueue.get(i).getComic_bundle_name());
                if(myComic == null){
                    // myComic == null - means the offline comic was not downloaded into device. so we can download the book into device
                    final ComicStore comicStore = ((IverseApplication) this.getApplication()).getComicStore();
                    final VerifyPurchaseTask task = new VerifyPurchaseTask(comicStore);
                    task.setComicBundleName(onDemandOfflineQueue.get(i).getComic_bundle_name());
                    task.setComicName(onDemandOfflineQueue.get(i).getName());
                    task.setDeviceId(AndroidInfo.getUniqueDeviceID(this));
                    task.setArtifactType(AndroidInfo.getArtifactForDevice(this));
                    task.setCoverImageFileName(onDemandOfflineQueue.get(i).getImage_filename());
                    task.setOnDemand(true);
                    ((IverseApplication) this.getApplication()).getTaskPool().submit(task);
                }

            }

        }
    }
    @Subscribe
    public void onShowGroup(ShowGroupEvent event) {
        ComicStore comicStore = IverseApplication.getApplication().getComicStore();
        Group group = event.group;
        // show sub-menu if applicable (this is for "Digests" titles)
        Fragment fragment = comicStore.getDatabaseApi().groupHasSubGroups(group.getGroupId())
                ? PublishersFragment.newInstanceForGroup(group)
                : ComicListFragment.newInstance(group);
        showTopFragment(fragment);
    }

    @Subscribe
    public void onShowPublisher(ShowPublisherEvent event) {
        ComicStore comicStore = IverseApplication.getApplication().getComicStore();
        Publisher publisher = event.publisher;
        // show sub-menu if applicable (this is for "Digests" titles)
        Fragment fragment = comicStore.getDatabaseApi().publisherHasGroups(publisher.getPublisherId())
                ? PublishersFragment.newInstanceForPublisher(publisher)
                : ComicListFragment.newInstanceForPublisher(publisher);
        showTopFragment(fragment);
    }

    @Subscribe
    public void serverConfigAvailable(ServerConfig config) {
        dismissProgressDialog();
        if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE && ConnectionHelper.isConnectedToInternet(MainActivity.this)) {
            reconcileOfflineQueue();
        }
    }

    @Subscribe
    public void serverConfigError(ServerConfigErrorEvent errorEvent) {
        dismissProgressDialog();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(com.iversecomics.archie.android.R.string.error_title)
                .setMessage(errorEvent.getErrorMessage())
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                })
                .create().show();
    }

    //called from sliding menu_square_01
    @Override
    public void onClosed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.menu_frame);
        if (!(fragment instanceof MenuFragment)) {
            onBackPressed();
        }
    }

    /**
     * Notifies on Network state changes.
     */
    private class ConnectivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (null != info) {
                String state = ConnectionHelper.getNetworkState(info.getState());
                String stateString = info.toString().replace(',', '\n');
                String infoString = String.format("Network Type: %s\nNetwork State: %s\n\n%s",info.getTypeName(), state, stateString);
                Log.d("Network Connection:", infoString);
            }
            //If the user not connected to the internet or device in Airplane mode we will navigate the user to "MyComicsFragment"
            if(!ConnectionHelper.getNetworkState(info.getState()).equalsIgnoreCase("Connected")){
                if(savedInstanceView == null) {
                    showTopFragment(new MyComicsFragment(), false);
                }
            }

        } // onReceive()
    } //ConnectivityReceiver()
}
