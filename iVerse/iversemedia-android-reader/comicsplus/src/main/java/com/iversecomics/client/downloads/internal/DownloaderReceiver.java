package com.iversecomics.client.downloads.internal;

import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.downloads.DownloaderConstants;
import com.iversecomics.client.downloads.internal.data.DatabasePruneExecutor;
import com.iversecomics.client.downloads.internal.data.DownloadAddExecutor;
import com.iversecomics.client.downloads.internal.data.DownloadData;
import com.iversecomics.client.downloads.internal.data.DownloadsTable;
import com.iversecomics.client.util.DebugUtil;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloaderReceiver extends BroadcastReceiver {
    private static final Logger LOG = LoggerFactory.getLogger(DownloaderReceiver.class);
    private ExecutorService addpool;
    private static boolean networkAvailable;

    static {
        networkAvailable = haveInternet(IverseApplication.getApplication());
    }


    public DownloaderReceiver() {
        this.addpool = Executors.newSingleThreadExecutor();
    }


    public static boolean isNetworkAvail() {
        return networkAvailable;
    }

    private static boolean haveInternet(Context ctx) {

        NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            // here is the roaming option you can change it if you want to
            // disable internet while roaming, just return false
            return false;
        }
        return true;
    }


    private void appendDatabasePruneExecutor(Context context) {
        Uri contentUri = DownloadsTable.CONTENT_URI;
        ContentProviderClient client = context.getContentResolver().acquireContentProviderClient(contentUri);
        addpool.execute(new DatabasePruneExecutor(contentUri, client));
    }

    private void appendDownloadAddExecutor(Context context, DownloadData download) {
        Uri contentUri = DownloadsTable.CONTENT_URI;
        ContentProviderClient client = context.getContentResolver().acquireContentProviderClient(contentUri);
        DownloadAddExecutor executor = new DownloadAddExecutor(contentUri, client, download);
        executor.addServiceIntent(context);
        addpool.execute(executor);
    }

    private void appendServiceStart(final Context context) {
        addpool.execute(new Runnable() {
            @Override
            public void run() {
                LOG.debug("Starting Service");
                Intent i = new Intent(context, DownloaderService.class);
                context.startService(i);
            }
        });
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        LOG.debug("onReceive() " + intent);

        if (intent.getPackage() != null && (!intent.getPackage().equals(context.getPackageName())))
            return;

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            context.startService(new Intent(context, DownloaderService.class));
            return;
        }

        if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            LOG.debug("Receiver onConnectivity");

            NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (info == null) {
                LOG.debug("No Network Info Available");
                return;
            }

            if (info.isConnected()) {
                LOG.debug("Broadcast: Network Up");
                networkAvailable = true;
                context.startService(new Intent(context, DownloaderService.class));
                return;
            } else {
                LOG.debug("Broadcast: Network Down");
                networkAvailable = false;
            }
            return;
        }

        if (DownloaderConstants.ACTION_DOWNLOAD_WAKEUP.equals(action)) {
            LOG.debug("Receiver wakeup/retry");
            appendDatabasePruneExecutor(context);
            appendServiceStart(context);
            return;
        }

        if (DownloaderConstants.ACTION_CLEANUP.equals(action)) {
            LOG.debug("Internal Cleanup");
            appendDatabasePruneExecutor(context);
            return;
        }

        if (DownloaderConstants.ACTION_DOWNLOAD_ADDED.equals(action)) {
            LOG.debug("Download Add: " + DebugUtil.toString(intent));
            DownloadData download = DownloadData.parseIntent(intent);
            appendDownloadAddExecutor(context, download);
            return;
        }

        LOG.debug("Got unknown Intent: " + intent);
    }
}
