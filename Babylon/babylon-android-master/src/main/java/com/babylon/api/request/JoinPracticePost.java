package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPost;
import com.babylon.api.request.base.AbsResponseProcessor;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Patient;
import com.babylon.model.PracticeId;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.Constants;

import org.apache.http.message.BasicHeader;

public class JoinPracticePost extends AbsRequestPost<PracticeId, Patient> {

    private String regionId;
    private String practiceId;

    @Override
    protected String getUrl() {
        return Urls.getRuby(String.format(Urls.JOIN_PRACTICE, regionId, practiceId));
    }

    @Override
    protected AbsResponseProcessor<Patient> getResponseProcessor() {
        return new RubyResponseProcessor<Patient>(getOutputType());
    }

    @Override
    protected BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    public static JoinPracticePost newAlliantsJoinPracticePost(String regionId, String practiceId) {
        JoinPracticePost practicePost = new JoinPracticePost();
        practicePost.regionId = regionId;
        practicePost.practiceId = practiceId;
        return practicePost;
    }

    public static JoinPracticePost newAlliantsJoinPracticePost(int regionId, int practiceId) {
        return newAlliantsJoinPracticePost(String.valueOf(regionId), String.valueOf(practiceId));
    }
}
