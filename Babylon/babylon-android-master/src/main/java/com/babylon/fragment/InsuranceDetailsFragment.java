package com.babylon.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.babylon.App;
import com.babylon.R;
import com.babylon.adapter.InsuranceArrayAdapter;
import com.babylon.api.request.PatientPatch;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Urls;
import com.babylon.model.Insurance;
import com.babylon.model.Patient;
import com.babylon.utils.ActionBarType;
import com.babylon.utils.AlertDialogHelper;
import com.babylon.utils.CommonHelper;
import com.babylon.view.TopBarInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpStatus;
import org.json.JSONArray;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class InsuranceDetailsFragment extends BaseFragment {

    public static final String TAG = InsuranceDetailsFragment.class.getSimpleName();
    private Spinner insurance_name_Spinner;
    private List<Insurance> insurerList;
    private EditText mEdtFname;
    private EditText mEdtLname;
    private EditText mEdtPostCode;
    private EditText birthDateTextView;
    private EditText mEdtInsuranceMembershipId;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private Button mBtnSave;
    private static int mInsuranceCompanyId;
    private Date birthday;
      Patient  patient;
    private InsuranceArrayAdapter adapter;
    ArrayList<Insurance> mInsuranceList;

    public static InsuranceDetailsFragment newInstance() {
        InsuranceDetailsFragment fragment = new InsuranceDetailsFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_insurance, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

       // setTitle(getString(R.string.insurance_fragment_title));
        getActivity().getActionBar().show();
        setupActionBar(ActionBarType.INSURANCE);
        myCalendar = Calendar.getInstance();
        insurance_name_Spinner=(Spinner)view.findViewById(R.id.insurer_name_spinner);
        mEdtFname=(EditText)view.findViewById(R.id.fnameEdt);
        mEdtLname=(EditText)view.findViewById(R.id.lnamedEdt);
        mEdtPostCode=(EditText)view.findViewById(R.id.postcodeEdt);
        mEdtInsuranceMembershipId=(EditText)view.findViewById(R.id.membershipNoEdt);

        birthDateTextView = (EditText)view.findViewById(R.id.dateofBirthEdt);
        birthDateTextView.setInputType(InputType.TYPE_NULL);
        date = new DatePickerDialog.OnDateSetListener()
        {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                birthday = myCalendar.getTime();

                //Check date for user to be over 16yrs of age
                if((new Date().getYear() - birthday.getYear()) < 16)
                {
                    AlertDialogHelper.getInstance().showMessage(getActivity(), R.string.age_not_valid);
                    return;
                }
                else {
                    updateLabel();
                }
            }

        };

        mBtnSave=(Button)view.findViewById(R.id.btnSaveInsurance);

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                try
                {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(birthDateTextView.getWindowToken(), 0);
                    saveInsuranecDetails();
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }
            }
        });




        birthDateTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(birthDateTextView.getWindowToken(), 0);

                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        patient = App.getInstance().getPatient();
        mEdtFname.setText(patient.getFirstName());
        mEdtLname.setText(patient.getLastName());
        mEdtPostCode.setText(patient.getAddressPostCode());
        mEdtInsuranceMembershipId.setText(patient.getInsuranceMembershipNumber());
        mInsuranceCompanyId=patient.getInsuranceCompanyId();
        patient.setInsuranceCompanyId(mInsuranceCompanyId);

        if(patient.getBirthday()!=null) {
            birthday = patient.getBirthday();
            updateLabel();
        }

        getInsuranceCompanies();


        insurance_name_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mInsuranceCompanyId=  mInsuranceList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // empty
            }
        });

    }

    //Fix for bug when keyboard not hidden after exiting fragment
    @Override
    public void onDestroy()
    {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(birthDateTextView.getWindowToken(), 0);
        super.onDestroy();
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        birthDateTextView.setTextColor(Color.WHITE);
        birthDateTextView.setText(sdf.format(birthday));
    }


    private void saveInsuranecDetails() throws ParseException {

        Patient patient = new Patient();

       patient.setId(App.getInstance().getPatient().getId());

        if(!TextUtils.isEmpty(mEdtFname.getText().toString())) {
            patient.setFirstName(mEdtFname.getText().toString());
        }

        if(!TextUtils.isEmpty(mEdtLname.getText().toString())) {
            patient.setLastName(mEdtLname.getText().toString());
        }

        if(!TextUtils.isEmpty(mEdtPostCode.getText().toString())) {
            patient.setAddressPostCode(mEdtPostCode.getText().toString());
        }

        if(!TextUtils.isEmpty(birthDateTextView.getText().toString())) {
            String dtStart = birthDateTextView.getText().toString();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date date = format.parse(dtStart);

            patient.setBirthday(date);
        }
        patient.setInsuranceCompanyId(mInsuranceCompanyId);
        if(!TextUtils.isEmpty(mEdtInsuranceMembershipId.getText())) {
            patient.setInsuranceMembershipNumber(mEdtInsuranceMembershipId.getText().toString());
        }
        new PatientPatch().withEntry(patient, new String[] {"first_name","last_name","date_of_birth",
                "insurance_company_id",
                "insurance_membership_number","address_post_code"}).execute(getActivity(),
                new RequestListener<Patient>() {
                    @Override
                    public void onRequestComplete(com.babylon.api.request.base.Response<Patient> response) {
                        // progressBar.setVisibility(View.INVISIBLE);

                        if (response.getStatus() == HttpStatus.SC_OK) {
                            //Success
                            App.getInstance().setPatient(response.getData());
                         Toast.makeText(getActivity().getApplicationContext(), getString(R.string
                                    .insurance_details_save_message), Toast.LENGTH_LONG).show();


                        } else if (response.getStatus() == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                            //Incorrect format
                            //  AlertDialogHelper.getInstance().showMessage(PhoneNumberDialogActivity.this, R.string
                            //   .phone_number_dialog_incorrect_number);
                        } else {
                            //Other error
                            // AlertDialogHelper.getInstance().showMessage(PhoneNumberDialogActivity.this, R.string
                            //   .phone_number_dialog_request_error);
                        }
                    }
                });

    }



    private void getInsuranceCompanies() {
        String mUri= CommonHelper.getUrl(Urls.GET_INSURERS);
        JsonArrayRequest mArrayReq = new JsonArrayRequest(mUri,	new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String jsonString = response.toString();
                Type collectionType = new TypeToken<List<Insurance>>() { }.getType();
                mInsuranceList=new ArrayList<Insurance>();
                mInsuranceList= gson.fromJson(jsonString, collectionType);
                Insurance insurance=new Insurance();
                insurance.setName("--");
                insurance.setId(0);
                mInsuranceList.add(insurance);
                ArrayList<Insurance> insureList=new ArrayList<Insurance>();
                insureList=mInsuranceList;
                adapter = new InsuranceArrayAdapter(getActivity().getApplicationContext(),mInsuranceList);
                insurance_name_Spinner.setAdapter(adapter);
                int index=0;
                for(Insurance insuranceitem : insureList){
                  if(mInsuranceCompanyId== insuranceitem.getId()){

                      break;
                  }
                    index++;
               }

                    insurance_name_Spinner.setSelection(index);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                return headers;
            }

        };

        if (App.getInstance().isConnection()){
            mArrayReq.setRetryPolicy(new DefaultRetryPolicy(18000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(mArrayReq);
        }


    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }


}






