package com.babylon.model.Kit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Babylon on 15/05/2015.
 */
public class AddressResponse {

    @SerializedName("id")
    @Expose
    int address_id;
    @SerializedName("Address")
    @Expose
    private Address address;



    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getAddress_id(){return  address_id;}

    public  void setAddress_id(int address_id){this.address_id=address_id;}

}