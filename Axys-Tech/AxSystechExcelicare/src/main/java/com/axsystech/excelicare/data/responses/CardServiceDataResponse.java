package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 14/07/15.
 */
public class CardServiceDataResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private String data;

    public String getData() {
        return data;
    }
}
