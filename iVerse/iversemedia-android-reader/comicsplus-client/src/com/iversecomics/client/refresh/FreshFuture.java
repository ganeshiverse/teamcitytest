package com.iversecomics.client.refresh;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

/**
 * A Future indicating that the content is already fresh.
 * <p>
 * Provided to eliminate NPE's during TaskPool usage.
 */
public class FreshFuture extends FutureTask<Void> {
    private static final Logger LOG = LoggerFactory.getLogger(FreshFuture.class);
    private String              id;

    public FreshFuture(final String id) {
        super(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                LOG.debug("Already Fresh: %s", id);
                return null;
            }
        });
        this.id = id;
    }

    public FreshFuture(Task task) {
        this(task.getClass().getSimpleName());
    }

    @Override
    public String toString() {
        return String.format("%s[%s]@%X", this.getClass().getSimpleName(), id, hashCode());
    }
}
