package com.axsystech.excelicare.loaders;

        import android.content.Context;

        import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
        import com.axsystech.excelicare.network.ServerMethods;
        import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by alanka on 10/20/2015.
 */
public class CircleListPaginationLoader extends DataAsyncTaskLibLoader<ListMyCirclesResponse> {

    private String token;
    private String pageSize;
    private int pageIndex;

    public CircleListPaginationLoader(Context context, int pageIndex, String pageSize, String token) {
        super(context);
        this.token = token;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    @Override
    protected ListMyCirclesResponse performLoad() throws Exception {
        return ServerMethods.getInstance().listMyCirclesPagination(pageIndex, pageSize, token);
    }
}