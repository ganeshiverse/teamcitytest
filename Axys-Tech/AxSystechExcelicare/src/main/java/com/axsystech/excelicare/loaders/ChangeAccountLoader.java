package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.ChangePasswordResponse;
import com.axsystech.excelicare.data.responses.LoginResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class ChangeAccountLoader extends DataAsyncTaskLibLoader<LoginResponse> {

    private final String token;
    private final String userId;

    public ChangeAccountLoader(final Context context, String token, String userId) {
        super(context);
        this.token = token;
        this.userId = userId;
    }

    @Override
    protected LoginResponse performLoad() throws Exception {
        return ServerMethods.getInstance().changeAccount(token, userId);
    }
}
