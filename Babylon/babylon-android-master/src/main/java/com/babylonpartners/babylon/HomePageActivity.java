
package com.babylonpartners.babylon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.*;
import com.android.volley.toolbox.*;
import com.appsflyer.AppsFlyerLib;
import com.babylon.App;
import com.babylon.Config;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.*;
import com.babylon.activity.settings.SettingsActivity;
import com.babylon.activity.shop.ShopCategoriesActivity;
import com.babylon.adapter.CarosselAdapter;
import com.babylon.analytics.AnalyticsWrapper;
import com.babylon.api.request.base.Urls;
import com.babylon.controllers.AskController;
import com.babylon.model.Notification;
import com.babylon.model.Patient;
import com.babylon.sync.SyncAdapter;
import com.babylon.sync.SyncUtils;
import com.babylon.utils.*;
import com.crashlytics.android.Crashlytics;
import com.google.gson.*;
import com.viewpagerindicator.CirclePageIndicator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class HomePageActivity extends BaseActivity implements PushNotificationHandlerUtil.IHomePageControl
{
    private ImageView mBtnAskAQuestion;
    private ImageView mBtnBookConsultation;
    private ImageView mBtnMonitorHealth;
    private ImageView mBtnClinicalRecords;
    private ImageView mBtnTestsAndKits;
    private ImageView mBtnSettings;
    private ImageView mBtnFaqs;
    private NetworkImageView mUserImg;
    private ImageLoader mImageLoader;
    private ViewPager mNotificationsView;
    private RelativeLayout mNotificationlayout;
    private boolean mWaitingForPlayServices = true;
    private Intent mPendingIntent;
    private Bundle mBundle;
    private String mUri;
    private RequestQueue mQueue;
    private JsonArrayRequest mJsonReq;
    private JsonObjectRequest mJsonObjReq;
    private CirclePageIndicator indicator;
    int mPosition=0;
    private ArrayList<Notification> mListOfNotifications = new ArrayList<>();
    private PushNotificationHandlerUtil mPushNotificationHandler;
    public static final int ASK_PHONE_NUMBER_REQUEST_CODE = 105;
    private static final int FAMILY_ACCOUNTS_REQUEST_CODE = 106;

    //Constants
    private int mRequestCode = -1;
    private static final int KIT_CATEGORIES_REQUEST_CODE = 107;
    public static final int CREATE_ACCOUNT_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        //Hide actionbar
        getActionBar().hide();


        //Count number of App Launches
        setEventAppLaunched();


        /*//Checking for first launch of the app
        if (PreferencesManager.getInstance().isFirstLaunch())
        {
            //Send Event to MixPanel
            try
            {
                JSONObject props = new JSONObject();
                props.put("Install Date",new Date().toString());
                MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props);
                helper.sendEvent(MixPanelEventConstants.APP_INSTALLED);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            PreferencesManager.getInstance().setIsFirstLogin(false);
            PreferencesManager.getInstance().setIsFirstLaunch(false);
            Intent intent = new Intent(this, WelcomeTourActivity.class);
            startActivity(intent);
            this.finish();
        }

        //Not first launch but the user is not logged in
        else if (!SyncUtils.isLoggedIn())
        {
            Intent intent=new Intent(this, RegistrationActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivityForResult(intent, CREATE_ACCOUNT_REQUEST_CODE);
            this.finish();
        }*/

        /*//Send Event to MixPanel
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props);
        helper.sendEvent(MixPanelEventConstants.APP_OPEN);*/

        //Parse push notifications
        mPushNotificationHandler = new PushNotificationHandlerUtil(this);
        mBundle = getIntent().getExtras();
        if (mBundle != null)
        {
            mPushNotificationHandler.processParsePushNotificationData(mBundle);
        }

        //User registered using facebook
        if (PreferencesManager.getInstance().shouldShowTermsPopup())
        {
            PreferencesManager.getInstance().setShouldShowTermsPopup(false);
            Intent termsPopupIntent = new Intent(this, TermsPopupActivity.class);
            startActivity(termsPopupIntent);
        }
        getViews();

        if (SyncUtils.isLoggedIn()) {
            //Get notifications from API --> needs to be called in a time limit of 10 secs
            getNotificationsFromAPI();
            Timer timer = new Timer();
            timer.schedule(new GetNotifications(HomePageActivity.this), 0, 10000);
        }
        if(mPosition!=0){
            mNotificationsView.setCurrentItem(mPosition);
           // mPosition
        }

        //Setup views


        //Sync Messages
        syncMessages();

        //Setup touch listeners
        setupViewControllers();

        //init crashanalytics
        Crashlytics.start(this);
    }

    @Override
    protected void onStop() {
        if(mQueue != null)
        {
            mQueue.cancelAll(this);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.APP_CLOSED);
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        if(mPushNotificationHandler == null)
        {
            mPushNotificationHandler = new PushNotificationHandlerUtil(HomePageActivity.this);
        }

        mBundle = intent.getExtras();
        if (mBundle != null)
        {
            mPushNotificationHandler.processParsePushNotificationData(mBundle);
        }
    }

    public void setEventAppLaunched()
    {
        int app_launch_count = PreferencesManager.getInstance().getAppLaunchCount();
        String date = PreferencesManager.getInstance().getLastAppLaunchDate();

        JSONObject props = new JSONObject();
        try
        {
            //props.put("Number of App Opens",app_launch_count);
            props.put("Last App Open",date);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Bundle fb_parameters = new Bundle();
        fb_parameters.putInt("Number of App Opens", app_launch_count);
        fb_parameters.putString("App Opened Date", date);

        MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props,fb_parameters);
        helper.sendEvent(MixPanelEventConstants.APP_LAUNCHED);
        helper.incrementProperty("Number of App Opens");

        PreferencesManager.getInstance().incrementAppLaunchCount();
        PreferencesManager.getInstance().setLastAppLaunchDate();
    }

    public void getNotificationsFromAPI()
    {
        mUri = Urls.getRuby(Urls.GET_NOTIFICATIONS);

        mQueue = App.getInstance().getRequestQueue();

        mJsonReq = new SecureJsonArrayRequest(mUri, HomePageActivity.this,new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response)
            {
                mListOfNotifications.clear();
                //Parse notification data and add to adapter
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                JsonParser parser = new JsonParser();

                JsonArray responseArray = parser.parse(response.toString()).getAsJsonArray();
                for (JsonElement obj : responseArray)
                {
                    Notification notificationFromAPI = gson.fromJson(obj,Notification.class);
                    mListOfNotifications.add(notificationFromAPI);
                }

                //If no notifications available for the user
                if(mListOfNotifications.size() == 0)
                {
                    mNotificationlayout.setVisibility(View.GONE);
                }
                else
                {
                    mNotificationlayout.setVisibility(View.VISIBLE);

                    mNotificationsView.setAdapter(new CarosselAdapter(HomePageActivity.this,mListOfNotifications));

                    mNotificationsView.setOnDragListener(new View.OnDragListener() {
                        @Override
                        public boolean onDrag(View v, DragEvent event) {
                            return false;
                        }
                    });
                    //mNotificationsView.setCurrentItem(0);

                    //Bind the title indicator to the adapter
                  //  CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id
                      //  .notification_page_indicator);
                    if(mPosition!=0){
                        mNotificationsView.setCurrentItem(mPosition);
                        // mPosition
                    }
                    indicator.setFillColor(R.color.purple_top_bar);
                    indicator.setStrokeColor(R.color.purple_top_bar);
                    //indicator.setBackgroundColor(getResources().getColor(R.color.purple_top_bar));
                    indicator.setViewPager(mNotificationsView);
                    indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                                mPosition=position;
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                }

            }
        },
        new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

            }
        });
        mJsonReq.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(mJsonReq);
    }

    public void getViews()
    {
        mBtnAskAQuestion = (ImageView) findViewById(R.id.activity_home_page_ask);
        mBtnBookConsultation = (ImageView) findViewById(R.id.activity_home_page_book_consultation);
        mBtnMonitorHealth = (ImageView) findViewById(R.id.activity_home_page_monitor);
        mBtnClinicalRecords = (ImageView) findViewById(R.id.activity_home_page_clinical_records);
        mBtnTestsAndKits = (ImageView) findViewById(R.id.activity_home_page_tests_and_kit);
        mBtnSettings = (ImageView)findViewById(R.id.activity_home_page_settings);
        mBtnFaqs = (ImageView) findViewById(R.id.activity_home_page_faq);
        mNotificationsView = (ViewPager) findViewById(R.id.home_page_notifications_view);
        mNotificationlayout = (RelativeLayout) findViewById(R.id.notification_layout);
        //mUserImg=(NetworkImageView)findViewById(R.id.activity_home_page_user_image);
          indicator = (CirclePageIndicator)findViewById(R.id.notification_page_indicator);

        updateNameDisplay();

}
    public void getImageLoader()
    {
        mImageLoader = new ImageLoader(App.getInstance().getRequestQueue(),new ImageLoader.ImageCache()
        {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10 * 1024 * 1024);
            public void putBitmap(String url, Bitmap bitmap)
            {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url)
            {
                return mCache.get(url);
            }
        });
    }



    private void updateNameDisplay() {
        TextDrawable faqdrawable = TextDrawable.builder()
                .beginConfig()
                .textColor(getResources().getColor(android.R.color.white))
                .useFont(Typeface.DEFAULT)
                .fontSize(40)
                .toUpperCase()
                .endConfig()
                .buildRound("FAQ", Color.parseColor("#73C3B6"));
        mBtnFaqs.setImageDrawable(faqdrawable);

    }


    private void syncMessages()
    {
        if (SyncUtils.isLoggedIn())
        {
            SyncAdapter.performSync(Constants.Sync_Keys.SYNC_MESSAGES, SyncAdapter.SYNC_PULL);
        }
    }

    public void setupViewControllers()
    {
        mBtnAskAQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askQuestion();
            }
        });

        mBtnBookConsultation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                //Send Event to MixPanel
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.APPOINTMENT_TRIED);


                Intent homePageIntent = new Intent(HomePageActivity.this, HomePageBackboneActivity.class);
                homePageIntent.putExtra("PAGE1", BackbonePageConstants.LOADING_SCREEN);
                homePageIntent.putExtra("PAGE2", BackbonePageConstants.BOOK_APPOINTMENT);
                startActivity(homePageIntent);
            }
        });

        mBtnMonitorHealth.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(HomePageActivity.this, MonitorMeActivity.class));

            }
        });

        mBtnClinicalRecords.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Send Event to MixPanel
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.RECORD_OPENED);


                Intent homePageIntent = new Intent(HomePageActivity.this,PasswordConfirmActivity.class);
                int requestCode = FAMILY_ACCOUNTS_REQUEST_CODE;
                startActivityForResult(homePageIntent, requestCode);

 
            }
        });

        mBtnTestsAndKits.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                testsAndKit();
            }
        });

        mBtnSettings.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Send Event to MixPanel
                JSONObject props = new JSONObject();
                MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props,null);
                helper.sendEvent(MixPanelEventConstants.SETTINGS_OPENED);

                startActivity(new Intent(HomePageActivity.this, SettingsActivity.class));
            }
        });

        mBtnFaqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomePageActivity.this, FaqActivity.class));
            }
        });

    }



    public void askQuestion()
    {

        Patient patient = App.getInstance().getPatient();
        if (patient == null) {
         //   noInternetAlert();
        } else {
            App.getInstance().setPatient(patient);
            if (patient.hasPhoneNumber()) {
                startAskControllerActivity(null);
            } else {
                startActivityForResult(new Intent(HomePageActivity.this,
                                PhoneNumberDialogActivity.class),
                        ASK_PHONE_NUMBER_REQUEST_CODE);
            }
        }

    }


    public void startAskControllerActivity(String questionId) {
        if (SyncUtils.isLoggedIn()) {
            AnalyticsWrapper.sendEvent(AnalyticsWrapper.Events.ASK_OPEN_EVENT);
            Bundle bundle = null;
            if (questionId != null) {
                bundle = new Bundle();
                bundle.putString(Keys.QUESTION_ID, questionId);
            }
            new AskController(this).start(bundle);
       }
        /*else {
            notLoggedInAction();
        }*/
    }




    public void testsAndKit()
    {
        //Send Event to MixPanel
        JSONObject props = new JSONObject();
        MixPanelHelper helper = new MixPanelHelper(HomePageActivity.this,props,null);
        helper.sendEvent(MixPanelEventConstants.KIT_BUTTON_TAPPED);

        //Start ShopCategoriesActivity
        Intent intent = new Intent(this,ShopCategoriesActivity.class);
        mRequestCode = KIT_CATEGORIES_REQUEST_CODE;
        startActivityForResult(intent, mRequestCode);
    }

    @Override
    protected void onResume() {
        super.onResume();
     //   AppEventsLogger.activateApp(this);
         getAvatarImage();
         updateNameDisplay();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {

            case CREATE_ACCOUNT_REQUEST_CODE: //Intentional fall-through

            case ASK_PHONE_NUMBER_REQUEST_CODE:
                if(resultCode == Activity.RESULT_OK) {
                    startAskControllerActivity(null);
                }
                break;



        }
        /*
         * Handle results returned to the FragmentActivity
         * by Google Play services
         */
       /* if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    *//*
                    Try the request again
                     *//*
                    break;
            }
        }*/
    }




    @Override
    public void webViewLoadUrl(HomePageBackboneActivity.HtmlPage url, String params)
    {

    }

    class GetNotifications extends TimerTask
    {
        Context mContext;

        public GetNotifications(Context context)
        {
            mContext = context;
        }
        public void run()
        {
            ((HomePageActivity)mContext).getNotificationsFromAPI();
        }
    }

    public void patchRequest(String groupname)
    {
        String mUri= CommonHelper.getUrl(Urls.CLEAR_NOTIFICATIONS);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, mUri, getJSONObject(groupname),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Adapter", "Error");

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }
    }

    public JSONObject getJSONObject(String groupName)
    {
        JSONObject passwordObj = new JSONObject();
        try
        {
            passwordObj.put("group",groupName);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return passwordObj;
    }

    public void getAvatarImage()
    {
        mUri = Urls.getRuby(String.format("/api/v1/accounts/%s/avatar_image",App.getInstance().getPatient().getId()));
        // mUri = "http://qa1.babylontesting.co.uk/api/v1/accounts/3646/avatar_image";

        ImageRequest jsonObjReq = new ImageRequest(mUri,new com.android.volley.Response.Listener<Bitmap>()
        {
            @Override
            public void onResponse(Bitmap response)
            {
                CircleTransform xform = new CircleTransform();
                Bitmap circleBitmap = xform.transform(response);
                mBtnSettings.setVisibility(View.VISIBLE);
                mBtnSettings.setImageBitmap(circleBitmap);
            }
        }, 200, 200,
                Bitmap.Config.RGB_565,new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Adapter", "Error");
                mBtnSettings.setVisibility(View.VISIBLE);
                String displayName = null;
                if (!TextUtils.isEmpty(App.getInstance().getPatient().getFirstName())) {
                    displayName = App.getInstance().getPatient().getFirstName();
                    displayName = Character.toString(displayName.charAt(0));
                    if (!TextUtils.isEmpty(App.getInstance().getPatient().getLastName())) {
                        String lastName = App.getInstance().getPatient().getLastName();
                        lastName = Character.toString(lastName.charAt(0));
                        displayName = displayName + (lastName);
                    }
                }


                if (!TextUtils.isEmpty(displayName)) {

                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                            .textColor(getResources().getColor(android.R.color.white))
                            .useFont(Typeface.DEFAULT)
                            .fontSize(45)
                            .toUpperCase()
                            .endConfig()
                            .buildRound(displayName, Color.parseColor("#73C3B6"));
                    mBtnSettings.setImageDrawable(drawable);
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CommonHelper.getAuthHeader().getName(), CommonHelper.getAuthHeader().getValue());
                headers.put("Content-Type", "image/*");

                return headers;
            }

        };
        if (App.getInstance().isConnection()){
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(CommonHelper.getRetryTime(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            App.getInstance().getRequestQueue().add(jsonObjReq);
        }
    }
}

