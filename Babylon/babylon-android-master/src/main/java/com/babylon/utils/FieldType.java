package com.babylon.utils;


import java.io.Serializable;
import java.util.Date;

public enum FieldType {
    INTEGER(Integer.class.getSimpleName()),
    STRING(String.class.getSimpleName()),
    BOOLEAN(Boolean.class.getSimpleName()),
    LONG(Long.class.getSimpleName()),
    BYTE(Byte.class.getSimpleName()),
    SHORT(Short.class.getSimpleName()),
    FLOAT(Float.class.getSimpleName()),
    DOUBLE(Double.class.getSimpleName()),
    BLOB(byte[].class.getSimpleName()),
    DATE(Date.class.getSimpleName()),
    SERILAIZABLE(Serializable.class.getSimpleName());

    private final String simpleName;

    private FieldType(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public static FieldType fromClassName(String className, Class<?> type) {
        FieldType fieldType = null;
        if (className != null) {
            for (FieldType field : FieldType.values()) {
                if (className.equals(field.getSimpleName())) {
                    fieldType = field;
                }
            }
        }
        if (fieldType == null && type != null) {
            fieldType = SERILAIZABLE;
        }
        return fieldType;
    }
}
