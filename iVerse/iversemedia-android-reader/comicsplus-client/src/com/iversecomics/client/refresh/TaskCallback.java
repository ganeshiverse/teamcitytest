package com.iversecomics.client.refresh;

/**
 * A callback that is invoked when a task has been completed.
 */
public interface TaskCallback {
    public void onTaskCompleted(Throwable error);
}
