package com.axsystech.excelicare.framework.ui.fragments.media.audio;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.axsystech.excelicare.R;

import java.io.InputStream;


/**
 * Created by kelindra on 2/5/2015.
 * custom component used to play .gif file animations
 */
public class GIFDecoderView extends ImageView {

    private String TAG = "GIFDecoderView";
    private boolean mIsPlayingGif = false;

    private GifDecoder mGifDecoder;

    private Bitmap mTmpBitmap;

    final Handler mHandler = new Handler();

    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            if (mTmpBitmap != null && !mTmpBitmap.isRecycled()) {
                GIFDecoderView.this.setImageBitmap(mTmpBitmap);
            }
        }
    };

    private int gifId;

    private double delay = 1.0;

    private int frameCount;

    private Context mContext;

    public GIFDecoderView(Context context) {
        super(context);
        mContext = context;
    }

    public GIFDecoderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setAttrs(attrs);
    }

    public GIFDecoderView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        setAttrs(attrs);
    }

    private void initializeView() {
        BitmapDrawable test = (BitmapDrawable) getResources().getDrawable(gifId);
        mTmpBitmap = test.getBitmap();
        mHandler.post(mUpdateResults);

        InputStream stream = getContext().getResources().openRawResource(gifId);
        mGifDecoder = new GifDecoder(mContext);
        mGifDecoder.read(stream);
        frameCount = mGifDecoder.getFrameCount();
    }

    /**
     * Gives the no.of frames existed in the .gif file
     *
     * @return the no.of frames
     */
    public int getFrameCount() {
        return frameCount;
    }

    /**
     * Plays the first half animation of the .gif file
     */
    public void playFirstHalf() {
        stopRendering();
        int startFrame = 0;
        int endFrame = (frameCount / 2) - 1;
        playGif(startFrame, endFrame);
    }

    /**
     * Plays the second half animation of the .gif file
     */
    public void playSecondHalf() {
        stopRendering();
        int startFrame = frameCount / 2;
        int endFrame = frameCount;
        playGif(startFrame, endFrame);
    }

    /**
     * Plays the full animation
     */
    public void playFull() {
        stopRendering();
        int startFrame = 0;
        int endFrame = frameCount;
        playGif(startFrame, endFrame);
    }

    /**
     * Plays the part of the .gif file
     *
     * @param startFrame frame number of the animation indicates where to start
     * @param endFrame   frame number of the animation indicates where to end
     */
    public synchronized void playGif(final int startFrame, final int endFrame) {
        mIsPlayingGif = true;

        new Thread(new Runnable() {
            public void run() {
                try {
                    for (int i = startFrame; (i < endFrame && mIsPlayingGif); i++) {
                        mTmpBitmap = mGifDecoder.getFrame(i);
                        int t = mGifDecoder.getDelay(i);
                        mHandler.post(mUpdateResults);
                        Thread.sleep((int) (t * delay));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (mIsPlayingGif) {
                    playGif(startFrame, endFrame);
                }
            }
        }).start();
    }


    /**
     * Stops the animation
     */
    public void stopRendering() {
        mIsPlayingGif = false;
    }

    /**
     * Sets the attributes to the custom component at run time
     *
     * @param attrs set of attributes
     */
    public void setAttrs(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.GIFDecoderView, 0, 0);
            String gifSource = a.getString(R.styleable.GIFDecoderView_src);

            if (gifSource == null) {
                a.recycle();
                return;
            }

            String sourceName = Uri.parse(gifSource).getLastPathSegment().replace(".gif", "");
            if (sourceName != null)
                setGIFResource(getResources().getIdentifier(sourceName, "drawable", getContext().getPackageName()));
            a.recycle();
        }
    }

    /**
     * Sets the .gif file source at run time
     *
     * @param resid file source id
     */
    public void setGIFResource(int resid) {
        this.gifId = resid;
        initializeView();
    }

    /**
     * Sets the delay time for the frames of the .gif file
     *
     * @param speedTimes multiples of the existing delay time either +ve or -ve
     */
    public void setDelay(double speedTimes) {
        delay = speedTimes;
    }

    /**
     * Gets the delay time for the frames of the .gif file
     *
     * @return multiples of the existing delay time
     */
    public double getDelay() {
        return delay;
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }
}