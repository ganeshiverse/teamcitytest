package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by someswarreddy on 30/06/15.
 */
public class SiteDetailsModel {

    @SerializedName("id")
    private String siteId;

    @SerializedName("name")
    private String siteName;

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public String toString() {
        return siteName;
    }
}
