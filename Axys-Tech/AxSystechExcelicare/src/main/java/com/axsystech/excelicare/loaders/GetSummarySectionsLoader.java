package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.SummarySectionsResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 07/10/15.
 */
public class GetSummarySectionsLoader extends DataAsyncTaskLibLoader<SummarySectionsResponse> {

    private String filterCriteria;
    private int max;
    private String panelId;
    private String patientId;
    private int start;
    private String token;
    private String isDictionaryJSONResponse;

    public GetSummarySectionsLoader(Context context, String filterCriteria, int max, String panelId,
                                    String patientId, int start, String token, String isDictionaryJSONResponse) {
        super(context);
        this.filterCriteria = filterCriteria;
        this.max = max;
        this.panelId = panelId;
        this.patientId = patientId;
        this.start = start;
        this.token = token;
        this.isDictionaryJSONResponse = isDictionaryJSONResponse;
    }

    @Override
    protected SummarySectionsResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getSummarySectionsList(filterCriteria, max, panelId,
                patientId, start, token, isDictionaryJSONResponse);
    }
}
