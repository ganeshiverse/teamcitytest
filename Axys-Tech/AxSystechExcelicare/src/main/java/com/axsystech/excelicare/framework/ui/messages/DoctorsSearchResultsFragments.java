package com.axsystech.excelicare.framework.ui.messages;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.CityOrLocationDetailsResponse;
import com.axsystech.excelicare.data.responses.ClinicianDetailsResponse;
import com.axsystech.excelicare.data.responses.LookUpDetailsResponse;
import com.axsystech.excelicare.data.responses.SearchDoctorsResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.fragments.appointments.ClinicianDetailsFragment;
import com.axsystech.excelicare.framework.ui.fragments.appointments.SaveFavoriteProviderTask;
import com.axsystech.excelicare.loaders.DoctorSearchResultsFragmentLoader;
import com.axsystech.excelicare.loaders.GetClinicianDetailsLoader;
import com.axsystech.excelicare.loaders.MsgInboxAndSentSearchLoader;
import com.axsystech.excelicare.loaders.SearchProvidersLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by someswarreddy on 24/08/15.
 */
public class DoctorsSearchResultsFragments extends BaseFragment implements View.OnClickListener, TextWatcher {

    private static final String SEARCH_DOCTORS_LOADER = "com.axsystech.excelicare.framework.ui.messages.DoctorsSearchResultsFragments.SEARCH_DOCTORS_LOADER ";

    private static final String CLINICIAN_DETAILS_VIEWPROFILE_LOADER = "com.axsystech.excelicare.framework.ui.messages.DoctorsSearchResultsFragments.CLINICIAN_DETAILS_LOADER ";
    //Sravan added book loader
    private static final String CLINICIAN_DETAILS_BOOK_LOADER = "com.axsystech.excelicare.framework.ui.messages.DoctorsSearchResultsFragments.CLINICIAN_DETAILS_BOOK_LOADER ";

    private String TAG = DoctorsSearchResultsFragments.class.getSimpleName();
    private MainContentActivity mActivity;
    private DoctorsSearchResultsFragments mFragment;
    private LayoutInflater mLayoutInflater;

    @InjectSavedState
    private User mActiveUser;

    @InjectSavedState
    private String mActionBarTitle;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;

    @InjectSavedState
    private String mProviderSource = AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS;

    @InjectSavedState
    private String mSearchLocationId = "0";

    @InjectSavedState
    private String mSearchSpecialityId = "0";

    @InjectView(R.id.listView)
    private ListView listView;

    //@InjectView(R.id.searchEditText)
    private EditText searchEditText;

    @InjectView(R.id.cancelButton)
    private Button cancelButton;

    boolean bookAppointments = true;

    private ArrayList<SearchDoctorsResponse.DoctorDetails> mDoctorDetailsArrayList;
    private DoctorsSearchResultsAdapter mDoctorsSearchResultsAdapter;

    private SearchDoctorsResponse.DoctorDetails selectedClinicianObject;

    @Override
    protected void initActionBar() {
        mActivity.getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(getActivity(), mActionBarTitle));
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message_inbox, container, false);
        searchEditText = (EditText) view.findViewById(R.id.searchEditText);
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        searchEditText.setTypeface(regular);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Book Appointments")) {
                    if (m.getValue() == 1) {
                        bookAppointments = true;
                    } else if (m.getValue() == 0) {
                        bookAppointments = false;
                    }
                }
            }
        }

        readIntentArgs();
        preloadData();
        addEventListeners();
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_ACTIONBAR_TITLE)) {
                mActionBarTitle = bundle.getString(AxSysConstants.EXTRA_ACTIONBAR_TITLE);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_DOCTORS_SEARCH_RESULTS_LIST)) {
                mDoctorDetailsArrayList = (ArrayList<SearchDoctorsResponse.DoctorDetails>) bundle.getSerializable(AxSysConstants.EXTRA_DOCTORS_SEARCH_RESULTS_LIST);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_PROVIDER_SOURCE)) {
                mProviderSource = bundle.getString(AxSysConstants.EXTRA_PROVIDER_SOURCE, AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SEARCH_LOCATION_ID)) {
                mSearchLocationId = bundle.getString(AxSysConstants.EXTRA_SEARCH_LOCATION_ID, "0");
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SEARCH_SPECIALITY_ID)) {
                mSearchSpecialityId = bundle.getString(AxSysConstants.EXTRA_SEARCH_SPECIALITY_ID, "0");
            }
        }
    }

    private void preloadData() {
        searchEditText.setHint(getString(R.string.search_by_name_speciality));

        mDoctorsSearchResultsAdapter = new DoctorsSearchResultsAdapter(mDoctorDetailsArrayList);
        listView.setAdapter(mDoctorsSearchResultsAdapter);
    }

    private void addEventListeners() {
        searchEditText.addTextChangedListener(this);
        cancelButton.setOnClickListener(this);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // get search results from server
                    searchProvider();
                }

                return false;
            }
        });
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_VIEWPROFILE_LOADER)) {
            showToast(R.string.error_clinician_details);
            //Sravan added else if id is book loader
        } else if (id == getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_BOOK_LOADER)) {
            showToast(R.string.error_clinician_details);
        } else if (id == getLoaderHelper().getLoaderId(SEARCH_DOCTORS_LOADER)) {
            showToast(R.string.no_search_results_found);
        }
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();

        if (id == getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_VIEWPROFILE_LOADER)) {
            showClinicianDetailsFragment(result, 0);
        } else if (id == getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_BOOK_LOADER)) {
            showClinicianDetailsFragment(result, 1);
        } else if (id == getLoaderHelper().getLoaderId(SEARCH_DOCTORS_LOADER)) {
            if (result != null && result instanceof SearchDoctorsResponse) {
                final SearchDoctorsResponse searchDoctorsResponse = (SearchDoctorsResponse) result;

                if (searchDoctorsResponse.getDoctorDetailsAL() != null && searchDoctorsResponse.getDoctorDetailsAL().size() > 0) {
                    if (mDoctorsSearchResultsAdapter == null) {
                        mDoctorsSearchResultsAdapter = new DoctorsSearchResultsAdapter(searchDoctorsResponse.getDoctorDetailsAL());
                        listView.setAdapter(mDoctorsSearchResultsAdapter);
                    } else {
                        mDoctorsSearchResultsAdapter.updateData(searchDoctorsResponse.getDoctorDetailsAL());
                    }
                } else {
                    if (!TextUtils.isEmpty(searchDoctorsResponse.getMessage())) {
                        showToast(searchDoctorsResponse.getMessage());
                    } else if (!TextUtils.isEmpty(searchDoctorsResponse.getMessageCaps())) {
                        showToast(searchDoctorsResponse.getMessageCaps());
                    }
                }
            }
        }
    }

    private void showClinicianDetailsFragment(Object result, final int index) {
        if (result != null && result instanceof ClinicianDetailsResponse) {
            final ClinicianDetailsResponse clinicianDetailsResponse = (ClinicianDetailsResponse) result;

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    if (clinicianDetailsResponse.getClinicianDetailsData() != null &&
                            clinicianDetailsResponse.getClinicianDetailsData() != null) {
                        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                        FragmentTransaction ft1 = fragmentManager.beginTransaction();
                        ClinicianDetailsFragment clinicianDetailsFragment = new ClinicianDetailsFragment();
                        Bundle bundle1 = new Bundle();
                        bundle1.putBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV, false);
                        if (selectedClinicianObject != null) {
                            bundle1.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, /*"Dr " +*/ selectedClinicianObject.getClinicianName());
                            bundle1.putSerializable(AxSysConstants.EXTRA_SELECTED_CLINICIAN_OBJECT, selectedClinicianObject);
                        }
                        bundle1.putSerializable(AxSysConstants.EXTRA_CLINICIAN_DETAILS, clinicianDetailsResponse.getClinicianDetailsData());
                        bundle1.putString(AxSysConstants.EXTRA_PROVIDER_SOURCE, mProviderSource);
                        //Sravan added extra bundle parameter.
                        bundle1.putInt("showTabAt", index);
                        clinicianDetailsFragment.setArguments(bundle1);
                        ft1.add(R.id.container_layout, clinicianDetailsFragment, ClinicianDetailsFragment.class.getSimpleName());
                        ft1.addToBackStack(ClinicianDetailsFragment.class.getSimpleName());
                        ft1.commit();
                    }
                }
            });
        }
    }

    private void searchProvider() {
        if (CommonUtils.hasInternet()) {
            String searchEdit = null;
            displayProgressLoader(false);
            try {
                searchEdit = URLEncoder.encode(searchEditText.getText().toString().trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String pageNumber = "1";
            String pageSize = "10";

            JSONObject searchCriteriaJson = new JSONObject();
            try {
                searchCriteriaJson.put("StrSearchText", searchEdit);

                searchCriteriaJson.put("LocationId", mSearchLocationId);

                searchCriteriaJson.put("SpecialityId", mSearchSpecialityId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (getView() != null && isAdded()) {
                DoctorSearchResultsFragmentLoader doctorSearchResultsFragmentLoader = null;
                try {
                    doctorSearchResultsFragmentLoader = new DoctorSearchResultsFragmentLoader(mActivity, mActiveUser.getToken(),
                            URLEncoder.encode(searchCriteriaJson.toString(), "UTF-8"), pageNumber, pageSize);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SEARCH_DOCTORS_LOADER), doctorSearchResultsFragmentLoader);
            }
        } else {
            showToast(R.string.error_no_network);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                searchEditText.setText("");
                // update to list adapter with original items
                if (mDoctorsSearchResultsAdapter != null) {
                    mDoctorsSearchResultsAdapter.updateData(mDoctorDetailsArrayList);
                } else {
                    mDoctorsSearchResultsAdapter = new DoctorsSearchResultsAdapter(mDoctorDetailsArrayList);
                    listView.setAdapter(mDoctorsSearchResultsAdapter);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (text != null && text.length() > 0) {
            cancelButton.setVisibility(View.VISIBLE);
        } else {
            cancelButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable text) {

    }

    private class DoctorsSearchResultsAdapter extends BaseAdapter {

        private ArrayList<SearchDoctorsResponse.DoctorDetails> doctorDetailsArrayList;

        public DoctorsSearchResultsAdapter(ArrayList<SearchDoctorsResponse.DoctorDetails> doctorDetailsArrayList) {
            this.doctorDetailsArrayList = doctorDetailsArrayList;
        }

        public void updateData(ArrayList<SearchDoctorsResponse.DoctorDetails> doctorDetailsArrayList) {
            this.doctorDetailsArrayList = doctorDetailsArrayList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (doctorDetailsArrayList != null && doctorDetailsArrayList.size() > 0) ? doctorDetailsArrayList.size() : 0;
        }

        @Override
        public SearchDoctorsResponse.DoctorDetails getItem(int position) {
            return doctorDetailsArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final SearchDoctorsResponse.DoctorDetails rowObject = getItem(position);

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.search_doctor_row, parent, false);
            }

            final ImageView doctorImageView = (ImageView) convertView.findViewById(R.id.doctorImageView);
            TextView doctorNameAndSpecialityTextView = (TextView) convertView.findViewById(R.id.doctorNameTextView);
            final CheckBox isFavoriteCheckBox = (CheckBox) convertView.findViewById(R.id.isFavoriteCheckBox);

            TextView experienceTextView = (TextView) convertView.findViewById(R.id.experienceTextView);
            TextView qualificationTextView = (TextView) convertView.findViewById(R.id.qualificationTextView);
            TextView feeTextView = (TextView) convertView.findViewById(R.id.feeTextView);
            TextView feetv = (TextView) convertView.findViewById(R.id.feetv);
            RelativeLayout viewProfileLayout = (RelativeLayout) convertView.findViewById(R.id.viewProfileLayout);
            TextView viewProfileTextView = (TextView) convertView.findViewById(R.id.viewProfileTextView);
            TextView specialityTextView = (TextView) convertView.findViewById(R.id.specialityTextView);

            RatingBar doctorRatingBar = (RatingBar) convertView.findViewById(R.id.doctorRatingBar);
            TextView likesTextView = (TextView) convertView.findViewById(R.id.likesTextView);
            ImageView phoneImageView = (ImageView) convertView.findViewById(R.id.phoneImageView);
            ImageView inpersonImageView = (ImageView) convertView.findViewById(R.id.inpersonImageView);
            ImageView videoImageView = (ImageView) convertView.findViewById(R.id.videoImageView);

            TextView clinicNameTextView = (TextView) convertView.findViewById(R.id.clinicNameTextView);
            LinearLayout bookButtonLayout = (LinearLayout) convertView.findViewById(R.id.bookButtonLayout);

            Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");

            doctorNameAndSpecialityTextView.setTypeface(bold);
            experienceTextView.setTypeface(bold);
            qualificationTextView.setTypeface(regular);
            feeTextView.setTypeface(bold);
            feetv.setTypeface(bold);
            viewProfileTextView.setTypeface(bold);
            specialityTextView.setTypeface(regular);
            likesTextView.setTypeface(bold);
            clinicNameTextView.setTypeface(bold);

            if (bookAppointments) {
                viewProfileTextView.setVisibility(View.VISIBLE);
                bookButtonLayout.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(rowObject.getPhoto())) {
                String imagePath = rowObject.getPhoto();
                if (!imagePath.startsWith("file://") && !imagePath.startsWith("http")) {
                    imagePath = "file://" + imagePath;
                }

                /*Picasso.with(mActivity)
                        .load(imagePath)
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon)
                        .into(doctorImageView);*/

                Picasso.with(getActivity()).load(imagePath).transform(new CircleTransform()).into(doctorImageView);

            }

            doctorNameAndSpecialityTextView.setText(rowObject.getClinicianName());
            specialityTextView.setText(rowObject.getSpeciality());
            if (!TextUtils.isEmpty(rowObject.getIsFavorite()) && rowObject.getIsFavorite().trim().equalsIgnoreCase("True")) {
                isFavoriteCheckBox.setChecked(true);
                isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
            } else {
                isFavoriteCheckBox.setChecked(false);
                isFavoriteCheckBox.setButtonDrawable(R.drawable.message_bluestar);
            }

            experienceTextView.setText("Exp: " + rowObject.getExperience());
            qualificationTextView.setText(rowObject.getQualification());
            feeTextView.setText(rowObject.getFee());

            if (!TextUtils.isEmpty(rowObject.getRating())) {
                float rating = Float.parseFloat(rowObject.getRating());
                doctorRatingBar.setRating(rating);
            } else {
                doctorRatingBar.setRating(0.0f);
            }
            likesTextView.setText(rowObject.getLikes());

            clinicNameTextView.setText(rowObject.getClinicName());

            isFavoriteCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(rowObject.getIsFavorite()) && rowObject.getIsFavorite().trim().equalsIgnoreCase("True")) {
                        isFavoriteCheckBox.setChecked(true);
                        isFavoriteCheckBox.setButtonDrawable(R.drawable.staractive_searchresults);
                    } else {
                        isFavoriteCheckBox.setChecked(false);
                        isFavoriteCheckBox.setButtonDrawable(R.drawable.message_bluestar);
                    }

                    SaveFavoriteProviderTask saveFavoriteProviderTask = new SaveFavoriteProviderTask(rowObject.getClinicianId(),
                            isFavoriteCheckBox.isChecked(), mActiveUser.getToken(), new SaveFavoriteProviderTask.SaveFavoriteProviderListener() {
                        @Override
                        public void onFavoriteProviderSaved() {
                            rowObject.setIsFavorite(isFavoriteCheckBox.isChecked() ? "False" : "True");
                        }

                        @Override
                        public void onFavoriteProviderFailed() {
                            showToast(R.string.error_save_favorite);
                        }
                    });

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        saveFavoriteProviderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        saveFavoriteProviderTask.execute();
                    }
                }
            });

            if (!TextUtils.isEmpty(mProviderSource)) {
                if (mProviderSource.equalsIgnoreCase(AxSysConstants.PROVIDER_SOURCE_APPOINTMENTS)) {
                    bookButtonLayout.setVisibility(View.VISIBLE);
                    viewProfileTextView.setVisibility(View.VISIBLE);
                    convertView.setEnabled(false);
                } else if (mProviderSource.equalsIgnoreCase(AxSysConstants.PROVIDER_SOURCE_COMPOSE)) {
                    bookButtonLayout.setVisibility(View.GONE);
                    viewProfileTextView.setVisibility(View.GONE);
                    convertView.setEnabled(true);
                }
            }

            bookButtonLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // get clinician Details
                    displayProgressLoader(false);
                    selectedClinicianObject = rowObject;
                    GetClinicianDetailsLoader getClinicianDetailsLoader = new GetClinicianDetailsLoader(mActivity, rowObject.getClinicianId(), mActiveUser.getToken());
                    if (mFragment.isAdded() && mFragment.getView() != null) {
                        //Sravan changed loader to CLINICIAN_DETAILS_BOOK_LOADER.
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_BOOK_LOADER), getClinicianDetailsLoader);
                    }
                }
            });


            viewProfileTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // get clinician Details
                    displayProgressLoader(false);
                    selectedClinicianObject = rowObject;
                    GetClinicianDetailsLoader getClinicianDetailsLoader = new GetClinicianDetailsLoader(mActivity, rowObject.getClinicianId(), mActiveUser.getToken());
                    if (mFragment.isAdded() && mFragment.getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_VIEWPROFILE_LOADER), getClinicianDetailsLoader);
                    }
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // get clinician Details
                    displayProgressLoader(false);
                    selectedClinicianObject = rowObject;
                    GetClinicianDetailsLoader getClinicianDetailsLoader = new GetClinicianDetailsLoader(mActivity, rowObject.getClinicianId(), mActiveUser.getToken());
                    if (mFragment.isAdded() && mFragment.getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CLINICIAN_DETAILS_VIEWPROFILE_LOADER), getClinicianDetailsLoader);
                    }
                }
            });

            return convertView;
        }
    }

}
