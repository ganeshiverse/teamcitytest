package com.babylon.fragment.payments;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babylon.App;
import com.babylon.R;
import com.babylon.activity.payments.OnAddFamilyToPlanListener;
import com.babylon.fragment.BaseFragment;
import com.babylon.view.TopBarInterface;

public class PaymentConfirmationFragment extends BaseFragment
        implements View.OnClickListener {

    public static final String TAG = PaymentConfirmationFragment.class.getSimpleName();

    public interface OnPaymentConfirmationListener extends OnAddFamilyToPlanListener {
        void onPaymentDone();
    }

    public interface DataProtocol {
        Double getSubscriptionPrice();
    }

    private OnPaymentConfirmationListener paymentConfirmationListener;
    private DataProtocol dataProtocol;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
      try {
            paymentConfirmationListener = (OnPaymentConfirmationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPaymentConfirmationListener");
        }
        try {
            dataProtocol = (DataProtocol) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DataProtocol");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        paymentConfirmationListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_subscription_confirmation, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setBackButtonVisible(false);

        TextView costTextView = (TextView) view.findViewById(R.id.subscription_cost_text_view);
        TextView familyPlanTextView = (TextView) view.findViewById(
                R.id.appointment_family_plan_text_view);

        Double subscriptionPrice = dataProtocol.getSubscriptionPrice();
        if (subscriptionPrice != null) {
            costTextView.setText(App.getInstance().getCurrentRegion().getFormattedPriceString(subscriptionPrice));
        }

        familyPlanTextView.setOnClickListener(this);

        setTitle(R.string.subscription_confirmation_title);
        setRightButton(this, R.string.done);
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    @Override
    public void onClick(View view) {
        if (paymentConfirmationListener != null) {
            switch (view.getId()) {
                case R.id.topbar_right_text_button:
                    paymentConfirmationListener.onPaymentDone();
                    break;
                case R.id.appointment_family_plan_text_view:
                    paymentConfirmationListener.onAddFamilyToPlanClick();
                    break;
                default:
                    break;
            }
        }
    }

}
