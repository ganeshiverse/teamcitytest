package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 29/09/15.
 */
public class UsersDetailsObject extends BaseResponse implements Serializable {

    @SerializedName("SiteID")
    private String siteID;

    @SerializedName("Sitename")
    private String sitename;

    @SerializedName("SitePhotoURL")
    private String sitePhotoURL;

    @SerializedName("ClientURL")
    private String clientURL;

    @SerializedName("SiteDescription")
    private String siteDescription;

    @SerializedName("SiteInfoLink")
    private String siteInfoLink;

    @SerializedName("EcUserID")
    private String ecUserID;

    @SerializedName("LoginName")
    private String loginName;

    @SerializedName("Forename")
    private String forename;

    @SerializedName("DOB")
    private String DOB;

    @SerializedName("Surname")
    private String surname;

    @SerializedName("Age")
    private String age;

    @SerializedName("Sex")
    private String gender;

    @SerializedName("SexValue")
    private String genderValue;

    @SerializedName("Photo")
    private String photo;

    @SerializedName("token")
    private String token;

    public String getSiteID() {
        return siteID;
    }

    public String getSitename() {
        return sitename;
    }

    public String getSitePhotoURL() {
        return sitePhotoURL;
    }

    public String getClientURL() {
        return clientURL;
    }

    public String getSiteDescription() {
        return siteDescription;
    }

    public String getSiteInfoLink() {
        return siteInfoLink;
    }

    public String getEcUserID() {
        return ecUserID;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getForename() {
        return forename;
    }

    public String getDOB() {
        return DOB;
    }

    public String getSurname() {
        return surname;
    }

    public String getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getGenderValue() {
        return genderValue;
    }

    public String getPhoto() {
        return photo;
    }

    public String getToken() {
        return token;
    }

    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public void setSitePhotoURL(String sitePhotoURL) {
        this.sitePhotoURL = sitePhotoURL;
    }

    public void setClientURL(String clientURL) {
        this.clientURL = clientURL;
    }

    public void setSiteDescription(String siteDescription) {
        this.siteDescription = siteDescription;
    }

    public void setSiteInfoLink(String siteInfoLink) {
        this.siteInfoLink = siteInfoLink;
    }

    public void setEcUserID(String ecUserID) {
        this.ecUserID = ecUserID;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setGenderValue(String genderValue) {
        this.genderValue = genderValue;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
