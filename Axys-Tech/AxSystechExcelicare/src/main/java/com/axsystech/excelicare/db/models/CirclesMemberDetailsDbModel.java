package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.axsystech.excelicare.util.CryptoEngine;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by vvydesai on 9/11/2015.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_CIRCLES_MEMBER_DETAILS)
public class CirclesMemberDetailsDbModel extends DBUtils implements Serializable {

    private static final long serialVersionUID = 2L;

    @DatabaseField(id = true, columnName = CIRCLE_MEMBER_DETAILS_USERID)
    private String detailsUserId;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ISACTIVE)
    private String detailsIsActive;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ISCIRCLEMANAGER)
    private int isCircleManager;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ADDRESS1)
    private String address1;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ADDRESS2)
    private String address2;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ADDRESS3)
    private String address3;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_EMAIL)
    private String email;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ENDREASONCOMMENT)
    private String endReasonComment;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ENDREASONLU)
    private String endReasonLu;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_FORENAME)
    private String foreName;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_HOMEPHONE)
    private String homePhone;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_LASTMODIFIEDDATE)
    private String lastModifiedDate;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_LASTMODIFIEDUSERID)
    private String lastModifieduserId;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_LMD)
    private int lmd;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_MOBILEPHONE)
    private String mobilePhone;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_PHONE)
    private boolean phone;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_PHOTOURL)
    private String photoUrl;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ROLESLU)
    private String roleSlu;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_ROLETYPESLU)
    private String roleTypeSlu;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_SITEID)
    private String siteId;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_SPECIALITY)
    private String speciality;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_STARTDATE)
    private String startDate;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_STOPDATE)
    private String stopDate;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_SURNAME)
    private String surName;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_TITLE)
    private String title;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_USERAUTHORITYTYPESLU)
    private String userAuthorityTypeSlu;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_USERCIRCLEID)
    private String userCircleId;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_USERCIRCLEMEMBERMAILID)
    private String userCircleMemberMailId;

    @DatabaseField(columnName = CIRCLE_MEMBER_DETAILS_USERCIRCLEMEMBERID)
    private String userCircleMemberId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDetailsUserId() throws Exception {
        return CryptoEngine.decrypt(detailsUserId);
    }

    public void setDetailsUserId(String detailsUserId) throws Exception {
        this.detailsUserId = CryptoEngine.encrypt(detailsUserId);
    }

    public String getDetailsIsActive() throws Exception {
        return CryptoEngine.decrypt(detailsIsActive);
    }

    public void setDetailsIsActive(String detailsIsActive) throws Exception {
        this.detailsIsActive = CryptoEngine.encrypt(detailsIsActive);
    }

    public int getIsCircleManager() {
        return isCircleManager;
    }

    public void setIsCircleManager(int isCircleManager) {
        this.isCircleManager = isCircleManager;
    }

    public String getAddress1() throws Exception {
        return CryptoEngine.decrypt(address1);
    }

    public void setAddress1(String address1) throws Exception {
        this.address1 = CryptoEngine.encrypt(address1);
    }

    public String getAddress2() throws Exception {
        return CryptoEngine.decrypt(address2);
    }

    public void setAddress2(String address2) throws Exception {
        this.address2 = CryptoEngine.encrypt(address2);
    }

    public String getAddress3() throws Exception {
        return CryptoEngine.decrypt(address3);
    }

    public void setAddress3(String address3) throws Exception {
        this.address3 = CryptoEngine.encrypt(address3);
    }

    public String getEmail() throws Exception {
        return CryptoEngine.decrypt(email);
    }

    public void setEmail(String email) throws Exception {
        this.email = CryptoEngine.encrypt(email);
    }

    public String getEndReasonComment() throws Exception {
        return CryptoEngine.decrypt(endReasonComment);
    }

    public void setEndReasonComment(String endReasonComment) throws Exception {
        this.endReasonComment = CryptoEngine.encrypt(endReasonComment);
    }

    public String getEndReasonLu() throws Exception {
        return CryptoEngine.decrypt(endReasonLu);
    }

    public void setEndReasonLu(String endReasonLu) throws Exception {
        this.endReasonLu = CryptoEngine.encrypt(endReasonLu);
    }

    public String getForeName() throws Exception {
        return CryptoEngine.decrypt(foreName);
    }

    public void setForeName(String foreName) throws Exception {
        this.foreName = CryptoEngine.encrypt(foreName);
    }

    public String getHomePhone() throws Exception {
        return CryptoEngine.decrypt(homePhone);
    }

    public void setHomePhone(String homePhone) throws Exception {
        this.homePhone = CryptoEngine.encrypt(homePhone);
    }

    public String getLastModifiedDate() throws Exception {
        return CryptoEngine.decrypt(lastModifiedDate);
    }

    public void setLastModifiedDate(String lastModifiedDate) throws Exception {
        this.lastModifiedDate = CryptoEngine.encrypt(lastModifiedDate);
    }

    public String getLastModifieduserId() throws Exception {
        return CryptoEngine.decrypt(lastModifieduserId);
    }

    public void setLastModifieduserId(String lastModifieduserId) throws Exception {
        this.lastModifieduserId = CryptoEngine.encrypt(lastModifieduserId);
    }

    public int getLmd() {
        return lmd;
    }

    public void setLmd(int lmd) {
        this.lmd = lmd;
    }

    public String getMobilePhone() throws Exception {
        return CryptoEngine.decrypt(mobilePhone);
    }

    public void setMobilePhone(String mobilePhone) throws Exception {
        this.mobilePhone = CryptoEngine.encrypt(mobilePhone);
    }

    public boolean isPhone() {
        return phone;
    }

    public void setPhone(boolean phone) {
        this.phone = phone;
    }

    public String getPhotoUrl() throws Exception {
        return CryptoEngine.decrypt(photoUrl);
    }

    public void setPhotoUrl(String photoUrl) throws Exception {
        this.photoUrl = CryptoEngine.encrypt(photoUrl);
    }

    public String getRoleSlu() throws Exception {
        return CryptoEngine.decrypt(roleSlu);
    }

    public void setRoleSlu(String roleSlu) throws Exception {
        this.roleSlu = CryptoEngine.encrypt(roleSlu);
    }

    public String getRoleTypeSlu() throws Exception {
        return CryptoEngine.decrypt(roleTypeSlu);
    }

    public void setRoleTypeSlu(String roleTypeSlu) throws Exception {
        this.roleTypeSlu = CryptoEngine.encrypt(roleTypeSlu);
    }

    public String getSiteId() throws Exception {
        return CryptoEngine.decrypt(siteId);
    }

    public void setSiteId(String siteId) throws Exception {
        this.siteId = CryptoEngine.encrypt(siteId);
    }

    public String getSpeciality() throws Exception {
        return CryptoEngine.decrypt(speciality);
    }

    public void setSpeciality(String speciality) throws Exception {
        this.speciality = CryptoEngine.encrypt(speciality);
    }

    public String getStartDate() throws Exception {
        return CryptoEngine.decrypt(startDate);
    }

    public void setStartDate(String startDate) throws Exception {
        this.startDate = CryptoEngine.encrypt(startDate);
    }

    public String getStopDate() throws Exception {
        return CryptoEngine.decrypt(stopDate);
    }

    public void setStopDate(String stopDate) throws Exception {
        this.stopDate = CryptoEngine.encrypt(stopDate);
    }

    public String getSurName() throws Exception {
        return CryptoEngine.decrypt(surName);
    }

    public void setSurName(String surName) throws Exception {
        this.surName = CryptoEngine.encrypt(surName);
    }

    public String getTitle() throws Exception {
        return CryptoEngine.decrypt(title);
    }

    public void setTitle(String title) throws Exception {
        this.title = CryptoEngine.encrypt(title);
    }

    public String getUserAuthorityTypeSlu() throws Exception {
        return CryptoEngine.decrypt(userAuthorityTypeSlu);
    }

    public void setUserAuthorityTypeSlu(String userAuthorityTypeSlu) throws Exception {
        this.userAuthorityTypeSlu = CryptoEngine.encrypt(userAuthorityTypeSlu);
    }

    public String getUserCircleId() throws Exception {
        return CryptoEngine.decrypt(userCircleId);
    }

    public void setUserCircleId(String userCircleId) throws Exception {
        this.userCircleId = CryptoEngine.encrypt(userCircleId);
    }

    public String getUserCircleMemberMailId() throws Exception {
        return CryptoEngine.decrypt(userCircleMemberMailId);
    }

    public void setUserCircleMemberMailId(String userCircleMemberMailId) throws Exception {
        this.userCircleMemberMailId = CryptoEngine.encrypt(userCircleMemberMailId);
    }

    public String getUserCircleMemberId() throws Exception {
        return CryptoEngine.decrypt(userCircleMemberId);
    }

    public void setUserCircleMemberId(String userCircleMemberId) throws Exception {
        this.userCircleMemberId = CryptoEngine.encrypt(userCircleMemberId);
    }
}
