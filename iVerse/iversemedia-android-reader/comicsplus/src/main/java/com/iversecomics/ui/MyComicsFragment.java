package com.iversecomics.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.iversecomics.app.AppConstants;
import com.iversecomics.archie.android.R;
import com.iversecomics.client.downloads.internal.DownloaderService;
import com.iversecomics.client.my.MyComicsModel;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.my.db.MyComicsTable;
import com.iversecomics.client.util.ConnectionHelper;
import com.iversecomics.ui.slidelist.SlideableComicListAdapter;
import com.iversecomics.ui.slidelist.SlideableListFragment;
import com.iversecomics.ui.widget.ComicReaderSeekBar;
import com.iversecomics.ui.widget.NavigationBar;
import com.iversecomics.util.ComicOpenHelper;
import com.iversecomics.util.ui.UiUtil;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.iversecomics.ui.slidelist.SlideableListConstants.MAX_NUM_ROWS;
import static com.iversecomics.ui.slidelist.SlideableListConstants.MIN_NUM_ROWS;
import static com.iversecomics.ui.slidelist.SlideableListConstants.NUM_PROGRESS_STEPS;

public class MyComicsFragment extends SlideableListFragment implements TwoWayAdapterView.OnItemClickListener {

    private ViewGroup rootView;
    private boolean flagListPurchased = true;
    private MyComicsModel mMyComicsModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_my_comics, null, false);
        NavigationBar navBar = (NavigationBar) rootView.findViewById(R.id.navbar);
        navBar.setNavigationListener((NavigationBar.NavigationListener) getActivity());
        navBar.setMenuButtonVisibility(View.VISIBLE);
        navBar.setMyComicsVisibility(View.INVISIBLE);

        //Util.exportDB();
//        navBar.setTitle(R.string.my_comics);

        mGrid = (TwoWayGridView) rootView.findViewById(com.iversecomics.archie.android.R.id.grid);
        // TODO
//        mGrid.setEmptyView(emptyView);
        mGrid.setOnItemClickListener(this);
        mGrid.setLongClickable(true);
        mGrid.setNumRows(mNumRows);

        initSeekBar((ComicReaderSeekBar) rootView.findViewById(R.id.seekbar));

        // When Internet Connection not available.
        if(!ConnectionHelper.isConnectedToInternet(getActivity())){
            navBar.setMenuButtonVisibility(View.INVISIBLE);
            navBar.setBackButtonVisibility(View.INVISIBLE);
            rootView.findViewById(R.id.icon).setClickable(false);
            // displays a offline info to user.
            if(savedInstanceState ==null)
                  showOfflineComicViewAlert();
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // text search
        final EditText txtSearch = (EditText) (rootView.findViewById(R.id.txtSearch));
        // cancel button
        View btnCancel = rootView.findViewById(R.id.btnCancel);
        UiUtil.applyButtonEffect(btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txtSearch.setText("");
                InputMethodManager imm = (InputMethodManager) (getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE));
                imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);
            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                // NA
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                // NA
            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editable.toString());
            }
        });

        // my purchased button
        final View btnPurchased = rootView.findViewById(R.id.btnShowPurchased);
        // my unlimited button
        final View btnUnlimited = rootView.findViewById(R.id.btnShowUnlimited);
        if(AppConstants.ENABLE_SUBSCRIPTIONS_SERVICE){
            btnUnlimited.setVisibility(View.VISIBLE);
            btnPurchased.setVisibility(View.VISIBLE);
        }else{
            btnUnlimited.setVisibility(View.INVISIBLE);
            btnPurchased.setVisibility(View.INVISIBLE);
        }
        final ColorFilter filter = new LightingColorFilter(0xA0A0A0A0, 0);
        btnPurchased.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                btnUnlimited.getBackground().setColorFilter(null);
                btnPurchased.getBackground().setColorFilter(filter);
                flagListPurchased = true;

                adapter.changeCursor(mMyComicsModel.getCursorMyComics(false));
            }
        });
        btnUnlimited.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                btnPurchased.getBackground().setColorFilter(null);
                btnUnlimited.getBackground().setColorFilter(filter);
                flagListPurchased = false;

                // TODO: joliver: 10/24/13 This hasn't been tested yet
                adapter.changeCursor(mMyComicsModel.getCursorMyComics(true));
            }
        });
        if (flagListPurchased) {
            btnPurchased.getBackground().setColorFilter(filter);
            btnUnlimited.getBackground().setColorFilter(null);
        } else {
            btnPurchased.getBackground().setColorFilter(null);
            btnUnlimited.getBackground().setColorFilter(filter);
        }


        //start download service in case there are some waiting downloads
        getActivity().startService(new Intent(getActivity(), DownloaderService.class));

        mMyComicsModel = new MyComicsModel(getActivity());
        Cursor cursor = mMyComicsModel.getCursorMyComics(false);
        adapter = new MyComicListAdapter(getActivity(), cursor);
        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                Cursor cursor = mMyComicsModel.getCursorMyComicsMatchingName(charSequence.toString());
                return cursor;
            }
        });
        mGrid.setAdapter(adapter);
    }

    // OnSeekBarChangeListener
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int numRows = (int) (((MAX_NUM_ROWS - MIN_NUM_ROWS) * (1 - (double) progress / NUM_PROGRESS_STEPS)) + MIN_NUM_ROWS);
        if (numRows != mNumRows) {
            mNumRows = numRows;
            mGrid.setNumRows(numRows);
            adapter.notifyDataSetInvalidated();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // NA
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    // OnItemClickListener
    @Override
    public void onItemClick(TwoWayAdapterView<?> adapterView, View view, int position, long id) {

        Cursor cursor = (Cursor) adapter.getItem(position);
        MyComic myComic = MyComicsTable.fromCursor(cursor);
        ComicOpenHelper.startComicReader(myComic, getActivity());
    }

    private class MyComicListAdapter extends SlideableComicListAdapter {

        public MyComicListAdapter(Context context, Cursor c) {
            super(context, c, R.layout.item_mycomic);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            MyComic comic = MyComicsTable.fromCursor(cursor);
            view.setTag(comic);
            comic.getComicId();
            //Uri coverUri = CoverSize.TABLET_LARGE.getServerUri(comic.getUriCover());
            Uri coverUri = comic.getUriCover();
            ImageView cover = (ImageView) view.findViewById(R.id.cover);
            cover.setImageDrawable(null);
            if (coverUri != null) {
                ImageLoader.getInstance().displayImage(coverUri.toString(), cover);
            }
        }
    }

    /**
     * Shows view offline comics information dialog.
     */
    private void showOfflineComicViewAlert(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getResources().getString(R.string.alert_title));
        dialog.setMessage(getResources().getString(R.string.infor_offine_view_saved_comics));
        dialog.setPositiveButton(getResources().getString(R.string.status_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
