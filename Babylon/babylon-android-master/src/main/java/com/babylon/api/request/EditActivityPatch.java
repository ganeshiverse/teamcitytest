package com.babylon.api.request;

import com.babylon.api.request.base.AbsRequestPatch;
import com.babylon.api.request.base.Urls;
import com.babylon.model.PhysicalActivity;
import com.babylon.utils.GsonWrapper;
import com.google.gson.Gson;

public class EditActivityPatch extends AbsRequestPatch<PhysicalActivity.PatchPhysicalActivity, PhysicalActivity.PatchPhysicalActivity> {
    private String activityId;

    @Override
    protected String getUrl() {
        String url = Urls.get(Urls.ACTIVITY_EDIT);
        return String.format(url, activityId);
    }

    public static EditActivityPatch getEditActivityPatch(PhysicalActivity.PatchPhysicalActivity activity, String id) {
        EditActivityPatch patch = new EditActivityPatch();

        patch.activityId = id;

        patch.withEntry(activity);

        return patch;
    }

    @Override
    protected Gson getGson() {
        return GsonWrapper.getTimestampGson();
    }
}
