package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.CityListResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by someswarreddy on 15/09/15.
 */
public class CityListLoader extends DataAsyncTaskLibLoader<CityListResponse> {

    private String dictionaryId;
    private String searchString;
    private String token;


    public CityListLoader(Context context, String dictionaryId, String searchString, String token) {
        super(context);
        this.dictionaryId = dictionaryId;
        this.searchString = searchString;
        this.token = token;
    }

    @Override
    protected CityListResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCityList(dictionaryId, searchString, token);
    }
}
