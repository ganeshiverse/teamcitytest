package com.axsystech.excelicare.db.models;

import com.axsystech.excelicare.db.DBUtils;
import com.axsystech.excelicare.framework.ui.fragments.media.FilterType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by someswarreddy on 12/07/15.
 */
@DatabaseTable(tableName = DBUtils.TABLE_NAME_MEDIA_FILES)
public class MediaItemDbModel extends DBUtils implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2L;

    @DatabaseField(id = true, columnName = MEDIA_ITEM_ID)
    private long mediaId;

    @DatabaseField(columnName = MEDIA_FOLDER_ID)
    private int folderId;

    @DatabaseField(columnName = MEDIA_SUB_FOLDER_ID)
    private int subFolderId;

    @DatabaseField(columnName = MEDIA_ITEM_IS_UPLOADED)
    private boolean isUploaded;

    @DatabaseField(columnName = MEDIA_ITEM_IS_EDITED)
    private boolean isEdited;

    @DatabaseField(columnName = MEDIA_ITEM_IS_DELETED)
    private boolean isDeleted;

    @DatabaseField(columnName = MEDIA_ITEM_IS_UPLOADING)
    private boolean isUploading;

    @DatabaseField(columnName = MEDIA_ITEM_NAME)
    private String mediaName;

    @DatabaseField(columnName = MEDIA_FILE_NAME)
    private String fileName;

    @DatabaseField(columnName = MEDIA_FILE_DURATION)
    private String fileDuration;

    private FilterType mediaType;

    @DatabaseField(columnName = MEDIA_ITEM_CREATED_DATE)
    private long createdDate; // will have DATE & TIME

    @DatabaseField(columnName = MEDIA_ITEM_LMD)
    private long lmd; // will have DATE & TIME

    @DatabaseField(columnName = MEDIA_ITEM_COMMENTS)
    private String comments;

    @DatabaseField(columnName = MEDIA_ITEM_ANNOTATION_MEDIA)
    private String annotationMedia;

    @DatabaseField(columnName = MEDIA_ITEM_ORIGINAL_MEDIA)
    private String originalMedia;

    @DatabaseField(columnName = MEDIA_ITEM_PREVIEW_MEDIA)
    private String previewMedia;

    @DatabaseField(columnName = SITE_ID)
    private String siteId;

    @DatabaseField(columnName = EC_USER_ID)
    private long ecUserID;

    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public int getSubFolderId() {
        return subFolderId;
    }

    public void setSubFolderId(int subFolderId) {
        this.subFolderId = subFolderId;
    }

    public long getMediaId() {
        return mediaId;
    }

    public void setMediaId(long mediaId) {
        this.mediaId = mediaId;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setIsEdited(boolean isEdited) {
        this.isEdited = isEdited;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public FilterType getMediaType() {
        return FilterType.getFilterTypeForFolderId(getFolderId());
    }

    public void setMediaType(FilterType mediaType) {
        this.mediaType = mediaType;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getLmd() {
        return lmd;
    }

    public void setLmd(long lmd) {
        this.lmd = lmd;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAnnotationMedia() {
        return annotationMedia;
    }

    public void setAnnotationMedia(String annotationMedia) {
        this.annotationMedia = annotationMedia;
    }

    public String getOriginalMedia() {
        return originalMedia;
    }

    public void setOriginalMedia(String originalMedia) {
        this.originalMedia = originalMedia;
    }

    public String getPreviewMedia() {
        return previewMedia;
    }

    public void setPreviewMedia(String previewMedia) {
        this.previewMedia = previewMedia;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public long getEcUserID() {
        return ecUserID;
    }

    public void setEcUserID(long ecUserID) {
        this.ecUserID = ecUserID;
    }

    public boolean isUploading() {
        return isUploading;
    }

    public void setIsUploading(boolean isUploading) {
        this.isUploading = isUploading;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDuration() {
        return fileDuration;
    }

    public void setFileDuration(String fileDuration) {
        this.fileDuration = fileDuration;
    }

    private boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

}
