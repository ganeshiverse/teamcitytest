package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SomeswarReddy on 7/29/2015.
 */
public class MessageDetailsResponse extends BaseResponse implements Serializable {

    @SerializedName("data")
    private DataObject dataObject;

    public DataObject getDataObject() {
        return dataObject;
    }

    public class DataObject implements Serializable {

        @SerializedName("Attachments")
        private ArrayList<AttachmentsObject> attachmentsArrayList;

        @SerializedName("ID")
        private String id;

        @SerializedName("Message")
        private String message;

        @SerializedName("MessageFormattedText")
        private String messageFormattedText;

        @SerializedName("MsgDate")
        private String msgDate;

        @SerializedName("MsgSource")
        private String msgSource;

        @SerializedName("MsgType")
        private String msgType;

        @SerializedName("Priority_Lu")
        private String priority_Lu;

        @SerializedName("Recipients")
        private ArrayList<RecipientsObject> recipentsArraylist;

        @SerializedName("RecordID")
        private String recordID;

        @SerializedName("SenderName")
        private String senderName;

        @SerializedName("SenderUserID")
        private String senderUserID;

        @SerializedName("SenderUsertype")
        private String senderUsertype;

        @SerializedName("Senderspeciality")
        private String senderspeciality;

        @SerializedName("Subject")
        private String subject;

        @SerializedName("Usr_Name")
        private String usr_Name;

        public ArrayList<AttachmentsObject> getAttachmentsArrayList() {
            return attachmentsArrayList;
        }

        public String getId() {
            return id;
        }

        public String getMessage() {
            return message;
        }

        public String getMessageFormattedText() {
            return messageFormattedText;
        }

        public String getMsgDate() {
            return msgDate;
        }

        public String getMsgSource() {
            return msgSource;
        }

        public String getMsgType() {
            return msgType;
        }

        public String getPriority_Lu() {
            return priority_Lu;
        }

        public ArrayList<RecipientsObject> getRecipentsArraylist() {
            return recipentsArraylist;
        }

        public String getRecordID() {
            return recordID;
        }

        public String getSenderName() {
            return senderName;
        }

        public String getSenderUserID() {
            return senderUserID;
        }

        public String getSenderUsertype() {
            return senderUsertype;
        }

        public String getSenderspeciality() {
            return senderspeciality;
        }

        public String getSubject() {
            return subject;
        }

        public String getUsr_Name() {
            return usr_Name;
        }
    }

    public class RecipientsObject implements Serializable {

        @SerializedName("RecipientName")
        private String recipientName;

        @SerializedName("RecipientSpeciality")
        private String recipientSpeciality;

        @SerializedName("RecipientType_LU")
        private String recipientTypeLU;

        @SerializedName("RecipientUserID")
        private String recipientUserID;

        @SerializedName("RecipientUsertype")
        private String recipientUsertype;

        public String getRecipientName() {
            return recipientName;
        }

        public String getRecipientSpeciality() {
            return recipientSpeciality;
        }

        public String getRecipientTypeLU() {
            return recipientTypeLU;
        }

        public String getRecipientUserID() {
            return recipientUserID;
        }

        public String getRecipientUsertype() {
            return recipientUsertype;
        }
    }

    public class AttachmentsObject implements Serializable {

        @SerializedName("ID")
        private String ID;

        @SerializedName("AttachmentName")
        private String attachmentName;

        @SerializedName("AttachmentSize")
        private String attachmentSize;

        @SerializedName("AttachmentType_LU")
        private String attachmentType_LU;

        public String getID() {
            return ID;
        }

        public String getAttachmentName() {
            return attachmentName;
        }

        public String getAttachmentSize() {
            return attachmentSize;
        }

        public String getAttachmentType_LU() {
            return attachmentType_LU;
        }
    }
}
