package com.babylon.view;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.babylon.Keys;
import com.babylon.R;
import com.babylon.activity.ImageActivity;
import com.babylon.activity.RecordAudioActivity;
import com.babylon.images.ImageLoader;
import com.babylon.model.FileEntry;
import com.babylon.model.Message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * TODO: document your custom view class.
 */
public class MessageView extends LinearLayout {

    public static final String ASK_DATE_PATTERN = "HH:mm, dd MMM yyyy";

    private Context context;

    public MessageView(Context context) {
        super(context);
        init(context);
    }

    public MessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MessageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_message, this);
        this.context = context;
    }

    /**
     *
     * @param audioByteArray this array could send from AskQuestionActivity, because while record
     * will be uploading to server, audio file path will be unavailable
     */
    public void setMessage(Message message, byte[] audioByteArray) {
        final String question = message.getText();
        showQuestion(question);

        final long date = message.getDate();
        setMessageDate(date);

        showFiles(message.getFiles(), audioByteArray);
    }

    private void showFiles(List<FileEntry> files, byte[] audioByteArray) {
        if (files != null) {
            for (FileEntry fileEntry : files) {
                addFile(fileEntry, audioByteArray);
            }
        }
    }

    private void showQuestion(String question) {
        final TextView questionTextView = (TextView) findViewById(R.id.text_view_message);

        if(TextUtils.isEmpty(question)) {
            final float density = getResources().getDisplayMetrics().density;
            final float textViewHeight = getResources().getDimension(R.dimen.ask_empty_message_view_height) * density;
            questionTextView.setHeight((int)textViewHeight);
        } else {
            questionTextView.setText(question);
        }

    }

    private void setMessageDate(long date) {
        final TextView dateTextView = (TextView) findViewById(R.id.text_view_date);

        SimpleDateFormat dateFormat = new SimpleDateFormat(ASK_DATE_PATTERN,
                Locale.getDefault());
        dateTextView.setText(dateFormat.format(new Date(date)));
    }

    private void addFile(FileEntry fileEntry, byte[] audioByteArray) {
        if(fileEntry.getMimetype()!=null) {
            if (fileEntry.isPhoto()) {
                setPhoto(fileEntry);
            } else if (fileEntry.isAudio()) {
                fileEntry.setData(audioByteArray);
                showAudioIcon(fileEntry);
            }
        }
    }

    private void setPhoto(final FileEntry fileEntry) {
        final ImageView imageView = (ImageView)findViewById(R.id.img_photo);
        imageView.setVisibility(View.VISIBLE);

        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar);

        ImageLoader.getInstance().displayImage(imageView, progressBar, ImageLoader.BIG_IMAGE, false, true, fileEntry);
        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String filePath = fileEntry.getFilePath();
                if (!TextUtils.isEmpty(filePath)) {
                    Intent intent = new Intent(context, ImageActivity.class);
                    intent.putExtra(Keys.KEY_PHOTO_PATH, filePath);
                    context.startActivity(intent);
                }
            }
        });
    }

    private void showAudioIcon(final FileEntry fileEntry) {
        final ImageView imageView = (ImageView)findViewById(R.id.img_audio);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String filePath = fileEntry.getFilePath();
                byte[] audioByteArray = fileEntry.getData();
                if (!TextUtils.isEmpty(filePath) || audioByteArray != null) {
                    Intent intent = new Intent(context, RecordAudioActivity.class);
                    if (audioByteArray != null) {
                        intent.putExtra(Keys.AUDIO_BYTE_ARRAY, audioByteArray);
                    } else {
                        intent.putExtra(Keys.KEY_AUDIO_PATH, filePath);
                    }
                    context.startActivity(intent);
                }
            }
        });
    }
}
