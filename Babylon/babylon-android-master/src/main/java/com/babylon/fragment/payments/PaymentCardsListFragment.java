package com.babylon.fragment.payments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.babylon.R;
import com.babylon.activity.payments.PaymentCardsListActivity;
import com.babylon.adapter.PaymentCardsCursorAdapter;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.RequestListenerErrorProcessed;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.CreditCardDelete;
import com.babylon.controllers.payments.PaymentCardViewController;
import com.babylon.database.schema.PaymentCardScheme;
import com.babylon.fragment.BaseFragment;
import com.babylon.model.payments.PaymentCard;
import com.babylon.view.PaymentCardView;
import com.babylon.view.TopBarInterface;

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the {@link com.babylon.fragment.payments
 * .CreditCardsListFragment.OnCardSelectedListener}
 * interface.
 */
public class PaymentCardsListFragment extends BaseFragment implements AbsListView.OnItemClickListener {

    public static final String TAG = PaymentCardsListFragment.class.getSimpleName();

    private OnPaymentCardInteractionListener onPaymentCardInteractionListener;
    /**
     * The fragment's ListView.
     */
    private ListView listView;
    public int deletingPaymentCardId;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private PaymentCardsCursorAdapter adapter;

    private boolean isListItemsEditable = false;
    private boolean isDeletingInProgress = false;
    public static final String PAYMENT_FRAGMENT_TAG = "PAYMENT_FRAGMENT_TAG";
    private boolean mHideArrow=false;

    public static PaymentCardsListFragment newInstance(String pActivityName) {
        PaymentCardsListFragment fragment = new PaymentCardsListFragment();
        Bundle args = new Bundle();
        args.putString(PAYMENT_FRAGMENT_TAG,pActivityName);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getString(R.string.select_payment_card_title));
        // Set the adapter
        listView = (ListView) view.findViewById(android.R.id.list);

        showFooter();
        Bundle arguments = getArguments();
        if (arguments != null) {
            String activityName = getArguments().getString(PAYMENT_FRAGMENT_TAG);
            if(activityName.compareToIgnoreCase(activityName)==0)
            {
                mHideArrow=true;
            }
        }

        adapter = new PaymentCardsCursorAdapter(getActivity(), onCreditCardDeleteListener,mHideArrow);
        listView.setAdapter(adapter);


        // Set OnItemClickListener so we can be notified on item clicks
        listView.setOnItemClickListener(this);

        if (isListItemsEditable) {
            setRightButton(onListEditingClickListener, R.string.payments_list_done);
        } else {
            setRightButton(onListEditingClickListener, R.string.payments_list_edit);
        }
    }

    private void setListItemsEnabled(Boolean editable)
    {
        isListItemsEditable = editable;
        adapter.notifyDataSetChanged();

        if (isListItemsEditable) {
            setRightButtonText(getString(R.string.payments_list_done));
        } else {
            setRightButtonText(getString(R.string.payments_list_edit));
        }
    }

    private void showFooter() {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.list_footer_payment_card_list, null);
        final PaymentCardView newPaymentCardListItem = (PaymentCardView)view.findViewById(R.id.payment_card_view);
        final PaymentCardViewController paymentCardController = new PaymentCardViewController();
        newPaymentCardListItem.showPaymentCard(paymentCardController, paymentCardController.getNewCreditCard(),onNewPaymentCardClickListener);
        listView.addFooterView(view, null, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(PaymentCardScheme.Query.CURSOR_LOAD_ALL_CREDIT_CARD,
                null, cardsListLoaderCallbacks);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onPaymentCardInteractionListener = (OnPaymentCardInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPaymentCardInteractionListener = null;
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.ORANGE_THEME;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != onPaymentCardInteractionListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            onPaymentCardInteractionListener.onPaymentCardSelect((PaymentCard) adapter.getItem(position));
        }
    }

    private final PaymentCardsCursorAdapter.OnCreditCardDeleteListener onCreditCardDeleteListener = new
            PaymentCardsCursorAdapter.OnCreditCardDeleteListener() {

                @Override
                public void onCreditCardDelete(int id) {
                    if (!isDeletingInProgress) {
                        setProgressBarVisible(true);

                        final CreditCardDelete deleteCreditCard = CreditCardDelete.getDeleteCreditCard(id);

                        deleteCreditCard.execute(getActivity(), deleteCardRequestListener);
                        deletingPaymentCardId = id;
                        isDeletingInProgress = true;
                    }
                }

                @Override
                public boolean isEditable() {
                    return isListItemsEditable;
                }
            };

    private final LoaderManager.LoaderCallbacks cardsListLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(),
                    PaymentCardScheme.CONTENT_URI, null,
                    null,
                    null,
                    PaymentCardScheme.SORT_PAYMENT_CARD_TYPE);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            adapter.swapCursor(data);

            if (data.moveToFirst()) {
                setRightButtonVisibility(View.VISIBLE);
            } else {
                setRightButtonVisibility(View.GONE);
            }

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    private final View.OnClickListener onListEditingClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        setListItemsEnabled(!isListItemsEditable);
        }
    };

    @Override
    protected void onTopBarButtonPressed() {
        if (isDeletingInProgress) {
            Toast.makeText(getActivity(), R.string.wait_while_card_is_deleting, Toast.LENGTH_SHORT).show();
        } else {
            super.onTopBarButtonPressed();
        }
    }

    private final RequestListener<Void> deleteCardRequestListener = new RequestListenerErrorProcessed<Void>() {
        @Override
        public synchronized void onRequestComplete(Response<Void> response, String errorMessage) {
            if (isVisible() && getActivity() != null) {
                setProgressBarVisible(false);
                isDeletingInProgress = false;

                onPaymentCardInteractionListener.onPaymentCardDelete(deletingPaymentCardId);

                if (!TextUtils.isEmpty(errorMessage)) {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            }

        }
    };

    private final PaymentCardView.OnPaymentCardClickListener onNewPaymentCardClickListener = new PaymentCardView
            .OnPaymentCardClickListener() {


        @Override
        public void onNewPaymentCardClick() {
            if (isListItemsEditable)
                setListItemsEnabled(false);

            onPaymentCardInteractionListener.onNewPaymentCardClick();
        }

        @Override
        public void onPaymentCardClick(PaymentCard paymentCard) {
            if (isListItemsEditable)
                setListItemsEnabled(false);

            onPaymentCardInteractionListener.onNewPaymentCardClick();
        }
    };

    public interface OnPaymentCardInteractionListener {
        public void onPaymentCardSelect(PaymentCard paymentCard);

        public void onPaymentCardDelete(int id);

        public void onNewPaymentCardClick();
    }
}
