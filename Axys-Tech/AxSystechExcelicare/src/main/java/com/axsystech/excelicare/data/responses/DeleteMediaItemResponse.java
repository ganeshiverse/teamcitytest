package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hkyerra on 7/9/2015.
 */
public class DeleteMediaItemResponse extends BaseResponse {
    @SerializedName("data")
    private String data;

    public String getData() {
        return data;
    }
}
