package com.iversecomics.client.store.db;

import android.test.suitebuilder.annotation.MediumTest;

import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.json.JSONArray;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;

public class ServerConfigTableTest extends AbstractComicStoreProviderTestCase {
    // Hardcoded JSON, as loading form assets isn't possible from ProviderTestCase
    private JSONObject createConfigJson() throws JSONException {
        JSONObject json = new JSONObject();

        String devHost = "https://development.iverseapps.com/";
        String devPV = "?p=613d32266c3d31&v=c90d1a861c4b0058fe492f17dcb4f609";
        String cdnHost = "http://iversemedia.cachefly.net/public/";

        JSONObject results = new JSONObject();
        results.put("blogFeed2Title", "");
        results.put("blogFeed2URL", "");
        results.put("blogFeedURL", "http://feeds.feedburner.com/Comicsplus");
        results.put("categoryBannerImageURL", cdnHost + "store/genre/banner_large/");
        results.put("categoryListURL", devHost + "categories.php" + devPV + "");
        results.put("featureImageURL", cdnHost + "store/cover/featured/");
        results.put("groupListURL", devHost + "groups.php" + devPV + "");
        results.put("groupsByGroupURL", devHost + "groups.php" + devPV + "&groupID=%@");
        results.put("groupsBySupplierURL", devHost + "groups.php" + devPV + "&supplierID=%@");
        results.put("iPadFeatureButtonImageURL", cdnHost + "store/cover/featured/ipadbutton.png");
        results.put("iPadMediumImageURL", cdnHost + "store/cover/padmedium/");
        results.put("largeFeatureImageURL", cdnHost + "store/cover/largefeatured/");
        results.put("largeImageURL", cdnHost + "store/cover/large/");
        results.put("mediumImageURL", cdnHost + "store/cover/medium/");
        results.put("previewImageURL", cdnHost + "store/preview/");
        results.put("previousPurchases", new JSONArray());
        results.put("productsByCategoryURL", devHost + "products.php" + devPV + "&action=category&categoryID=%@");
        results.put("productsByFeaturedCategoryURL", devHost + "products.php" + devPV
                + "&action=featured&categoryID=%@");
        results.put("productsByFeaturedURL", devHost + "products.php" + devPV + "&action=featured");
        results.put("productsByGroupURL", devHost + "products.php" + devPV + "&action=group&groupID=%@");
        results.put("productsBySearchURL", devHost + "products.php" + devPV + "&action=search&terms=%@");
        results.put("productsBySeriesURL", devHost + "products.php" + devPV + "&action=series&seriesID=%@");
        results.put("productsBySupplierURL", devHost + "products.php" + devPV + "&action=supplier&supplierID=%@");
        results.put("productsNewReleasesURL", devHost + "products.php" + devPV + "&action=new");
        results.put("productsTopFreeURL", devHost + "products.php" + devPV + "&action=free");
        results.put("productsTopPaidURL", devHost + "products.php" + devPV + "&action=paid");
        results.put("productsViewURL", devHost + "products.php" + devPV + "&action=product&productID=%@");
        results.put("publisherBannerImageURL", cdnHost + "store/publisher/banner/");
        results.put("publisherLargeBannerImageURL", cdnHost + "store/publisher/banner_large/");
        results.put("publisherLogoImageURL", cdnHost + "store/publisher/logo/");
        results.put("registerPushTokenURL", devHost + "registerPush.php" + devPV + "");
        results.put("serverAPIVersion", 2);
        results.put("serverStatusURL", cdnHost + "server.txt");
        results.put("smallImageURL", cdnHost + "store/cover/small/");
        results.put("submitRatingURL", devHost + "rateProduct.php" + devPV + "");
        results.put("supplierListURL", devHost + "suppliers.php" + devPV + "");
        results.put("supplierViewURL", devHost + "suppliers.php" + devPV + "&supplierID=%@");
        results.put("updatedLocale", false);
        results.put("verifyPurchaseURL", devHost + "download.php" + devPV + "");

        json.put("results", results);
        json.put("status", "ok");
        return json;
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        freshDB();
    }

    @MediumTest
    public void testLoadEmptyTable() {
        ServerConfig config = new ServerConfig();
        config.load(resolver);

        assertFalse("ServerConfig should not be configured (from an empty table)", config.isConfigured());
    }

    @MediumTest
    public void testSaveTable() throws Exception {
        ServerConfig config = new ServerConfig();
        config.setFromServerResponse(createConfigJson());

        // Save it.
        config.save(resolver);

        // Load it.
        ServerConfig config2 = new ServerConfig();
        config2.load(resolver);

        // Compare it.
        assertTrue("ServerConfig should be flagged as configured", config2.isConfigured());

    }
}
