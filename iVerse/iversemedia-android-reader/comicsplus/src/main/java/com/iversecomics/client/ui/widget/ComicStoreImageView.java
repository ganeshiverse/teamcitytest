package com.iversecomics.client.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.iversecomics.archie.android.R;
import com.iversecomics.client.bitmap.BitmapManager;
import com.iversecomics.client.bitmap.LateBitmapRef;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.net.URI;

public class ComicStoreImageView extends LateBitmapView {
    private static final Logger LOG = LoggerFactory.getLogger(ComicStoreImageView.class);
    public static int PADDING_DP = 19;//38;


    public ComicStoreImageView(Context context) {
        super(context);
        init();
    }

    public ComicStoreImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ComicStoreImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setImageResource(R.drawable.cover_placeholder);
    }

    public void setImageViaUri(Uri serverUri, BitmapManager bitmapManager) {
        LOG.debug("setImageViaUri(%s, bitmapManager)", serverUri.toString());

        String tag = serverUri.toString();
        setTag(tag);

        Bitmap bitmap = bitmapManager.getCacheDir().getBitmap(serverUri);
        if (bitmap == null) {
            this.setImageResource(R.drawable.cover_placeholder);
            LateBitmapRef ref = new LateBitmapRef(URI.create("cached:" + serverUri.toString()), this);
            ref.setTag(tag);
            bitmapManager.fetch(ref);
        } else {
            this.setLateBitmap(bitmap, tag);
        }
    }

    public static int dpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }
}
