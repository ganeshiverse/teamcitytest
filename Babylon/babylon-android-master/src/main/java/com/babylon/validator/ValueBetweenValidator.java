package com.babylon.validator;

public class ValueBetweenValidator extends BaseValidator {
    private float minimum;
    private float maximum;

    public ValueBetweenValidator(BaseValidator nextValidator, String errorMessage, float minimum, float maximum) {
        super(nextValidator, errorMessage);
        this.minimum = minimum;
        this.maximum = maximum;
    }

    @Override
    public String validate(String string) {
        String result = string;
        if(string.equals(".") || string.equals("-")) {
            result = "0";
        }

        return (Float.valueOf(result) < minimum || Float.valueOf(result) > maximum) ? errorMessage : null;
    }
}
