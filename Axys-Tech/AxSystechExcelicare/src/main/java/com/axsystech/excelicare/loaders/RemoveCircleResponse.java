package com.axsystech.excelicare.loaders;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by someswarreddy on 03/09/15.
 */
public class RemoveCircleResponse extends BaseResponse implements Serializable {

    public String getData() {
        return data;
    }

    @SerializedName("data")
    private String data;


}
