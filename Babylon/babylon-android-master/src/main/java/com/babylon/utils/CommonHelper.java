package com.babylon.utils;


import com.babylon.Config;
import com.babylon.model.Wsse;
import com.babylon.sync.SyncUtils;
import org.apache.http.message.BasicHeader;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CommonHelper
{
    private static String TAG  = CommonHelper.class.getSimpleName();
    private static  int RETRY=18000;
    public static boolean isEmailValid(CharSequence email)
    {

      return  ((email == null)) ? false :((android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())) ;


    }

    public static String getUrl(String type) {
        return Config.RUBY_SERVER_URL.concat(type);
    }


    public static BasicHeader getAuthHeader() {
        BasicHeader result = null;
        Wsse wsse = SyncUtils.getWsse();
        if (wsse != null) {
            result = new BasicHeader(Constants.Header_RequestKeys.BASIC_AUTH_HEADER, wsse.getToken());
        }
        return result;
    }

    public static int getRetryTime()
    {

        return RETRY;

    }

    public static float getAge(final Date birthdate) {
        return getAge(Calendar.getInstance().getTime(), birthdate);
    }


    public static float getAge(final Date current, final Date birthdate) {

        if (birthdate == null) {
            return 0;
        }
        if (current == null) {
            return getAge(birthdate);
        } else {
            final Calendar calend = new GregorianCalendar();
            calend.set(Calendar.HOUR_OF_DAY, 0);
            calend.set(Calendar.MINUTE, 0);
            calend.set(Calendar.SECOND, 0);
            calend.set(Calendar.MILLISECOND, 0);

            calend.setTimeInMillis(current.getTime() - birthdate.getTime());

            float result = 0;
            result = calend.get(Calendar.YEAR) - 1970;
            result += (float) calend.get(Calendar.MONTH) / (float) 12;
            return result;
        }

    }


}