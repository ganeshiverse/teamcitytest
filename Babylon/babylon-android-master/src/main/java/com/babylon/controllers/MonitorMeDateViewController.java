package com.babylon.controllers;

import android.content.Context;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

public class MonitorMeDateViewController extends DateViewController {

    public static final String DATE_FORMAT = "MMM dd yyyy";

    public MonitorMeDateViewController(Context context, TextView birthDateTextView, OnDateSetListener onDateSet, Date
            currentDate) {
        super(context, birthDateTextView, onDateSet, currentDate);
    }

    public MonitorMeDateViewController() {

    }

    @Override
    public String getFormatDate() {
        return DATE_FORMAT;
    }

    @Override
    protected long getDatePickerMaxDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTimeInMillis();
    }

    @Override
    protected long getDatePickerMinDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.YEAR, -1);
        return calendar.getTimeInMillis();
    }
}
