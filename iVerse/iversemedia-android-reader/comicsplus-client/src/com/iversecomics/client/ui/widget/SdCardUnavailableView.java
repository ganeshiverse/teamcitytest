package com.iversecomics.client.ui.widget;

import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.iversecomics.client.R;
import com.iversecomics.util.text.WordUtil;

public class SdCardUnavailableView extends View {
    private static final int   MAX_MESSAGE_LINES = 3;
    private static final float ANGLE             = 15f;
    private String             title;
    private Path               titlePath;
    private TextPaint          titlePaint;
    private String             message[]         = new String[MAX_MESSAGE_LINES];
    private Path               messagePath[];
    private TextPaint          messagePaint;
    private Bitmap             cardBitmap;
    private Matrix             cardOffset;
    private Path               warningTapePath;
    private Paint              warningTapePaint;
    private Paint              warningTapeShadowPaint;

    public SdCardUnavailableView(Context context) {
        super(context);
        init(context);
    }

    public SdCardUnavailableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SdCardUnavailableView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        canvas.save();

        Matrix m = canvas.getMatrix();
        m.preTranslate(20, 100);
        m.postRotate(ANGLE);

        canvas.drawBitmap(cardBitmap, cardOffset, null);

        canvas.drawTextOnPath(title, titlePath, 0.0F, 0.0F, titlePaint);

        // Break up message into 3 parts.
        for (int i = 0; i < MAX_MESSAGE_LINES; i++) {
            canvas.drawTextOnPath(message[i], messagePath[i], 0.0F, 0.0F, messagePaint);
        }

        canvas.save();
    }

    public String getTitle() {
        return title;
    }

    private void init(Context context) {
        titlePath = new Path();
        titlePath.moveTo(50, 73);
        titlePath.lineTo(304, 51);

        warningTapePath = new Path();
        warningTapePath.moveTo(0, 0);
        warningTapePath.lineTo(500, 0);
        warningTapePath.lineTo(500, 100);
        warningTapePath.lineTo(0, 100);
        warningTapePath.close();

        warningTapePaint = new Paint();
        warningTapePaint.setAntiAlias(true);
        warningTapePaint.setColor(Color.parseColor("#fdff75"));
        warningTapePaint.setStyle(Paint.Style.FILL);

        warningTapeShadowPaint = new Paint();
        warningTapeShadowPaint.setAntiAlias(true);
        warningTapeShadowPaint.setColor(Color.parseColor("#88000000"));
        warningTapeShadowPaint.setStyle(Paint.Style.FILL);

        titlePaint = new TextPaint();
        titlePaint.setAntiAlias(true);
        titlePaint.setStyle(Paint.Style.FILL);
        titlePaint.setColor(Color.BLACK);
        titlePaint.setTextSize(17.0F);
        titlePaint.setTypeface(Typeface.DEFAULT_BOLD);

        messagePaint = new TextPaint();
        messagePaint.setAntiAlias(true);
        messagePaint.setStyle(Paint.Style.FILL);
        messagePaint.setColor(Color.BLACK);
        messagePaint.setTextSize(12.0F);
        messagePaint.setTypeface(Typeface.DEFAULT_BOLD);

        messagePath = new Path[3];
        int offset;
        for (int i = 0; i < 3; i++) {
            message[i] = "";
            offset = (i * 12);
            messagePath[i] = new Path();
            messagePath[i].moveTo(55 + i, 95 + offset);
            messagePath[i].lineTo(304, 73 + offset);
        }

        cardOffset = new Matrix();
        cardOffset.setTranslate(5.0F, 70.0F);

        Resources resources = context.getResources();
        cardBitmap = BitmapFactory.decodeResource(resources, R.drawable.microsd_warn);
    }

    public void setMessage(String message) {
        List<String> lines = WordUtil.wrap(message, 40, true);
        if (lines.size() > MAX_MESSAGE_LINES) {
            throw new IllegalArgumentException("Message is too long: " + message);
        }

        int len = lines.size();

        for (int i = 0; i < len; i++) {
            this.message[i] = lines.get(i);
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
