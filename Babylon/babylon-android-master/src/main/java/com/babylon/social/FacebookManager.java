package com.babylon.social;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import com.babylon.utils.L;
import com.babylon.utils.UIUtils;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

import java.util.Arrays;
import java.util.List;

public class FacebookManager {
    private static String TAG = "FacebookManager";
    public static final List<String> PERMISSIONS = Arrays.asList("public_profile, email, user_friends, user_birthday");
    private static FacebookManager instance;
    private GraphUser currentUser;
    private List<GraphUser> friendList;
    private Activity activity;
    private OnFbLoginStatusListener loginListener;
    public static ProgressDialog mProgressDialog;

    public static final int FACEBOOK_REQUEST_CODE = 64206;

    public static FacebookManager getInstance() {
        if (instance == null) {
            instance = new FacebookManager();
        }
        return instance;
    }

    private FacebookManager() {
        //singleton
    }

    public void login(Activity activity, OnFbLoginStatusListener loginListener) {
        this.activity = activity;
        this.loginListener = loginListener;

        L.i(TAG, "login");
        Session session = Session.getActiveSession();
        if (hasAllPermission()) {
            if (session.isOpened()) {
                L.i(TAG, "Session is opened");
                callback.call(session, SessionState.OPENED, null);
            } else {
                L.i(TAG, "Session is not opened. Open for read");
                OpenRequest openRequest = new OpenRequest(activity).setPermissions(PERMISSIONS).setCallback(callback);
                if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                    session.openForRead(openRequest);
                } else {
                    Session.openActiveSession(activity, true, callback);
                }
            }
        } else {
            L.i(TAG, "Create new session. Open for read");
            if (session != null && session.isOpened()) {
                session.closeAndClearTokenInformation();
            }

            session = new Session(activity);
            Session.setActiveSession(session);

            OpenRequest openRequest = new OpenRequest(activity).setPermissions(PERMISSIONS).setCallback(callback);
            session.openForRead(openRequest);
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, Exception exception)
        {
            L.i(TAG, "Session. StatusCallback. State: " + state);
            if (state == SessionState.OPENED || state == SessionState.OPENED_TOKEN_UPDATED) {

                new Thread() {
                    public void run() {
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if (loginListener != null) {
                                        loginListener.onFbLoginSuccess(session.getAccessToken(),
                                                state.name());
                                    }

                                }
                            });
                        }
                    }
                }.start();

            } else if (state != SessionState.CLOSED && state != SessionState.CLOSED_LOGIN_FAILED
                    && state != SessionState.OPENING) {
                if (loginListener != null) {
                    loginListener.onFbLoginFail("Get access token fail");
                }

            }
        }
    };

    public Session.StatusCallback getStatusCallback() {
        return callback;
    }

    public void requestToGetCurrentUser(final OnGetCurrentUserListener listener) {
        Request request = Request.newMeRequest(Session.getActiveSession(), new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if (user != null) {
                    L.i(TAG, "requestToGetCurrentUser. UserId: " + user.getId());
                    setCurrentUser(user);
                    listener.onGetFbUserSucces(user);
                } else {
                    listener.onGetFbUserFail("Get current user fail");
                }
            }
        });
        request.executeAsync();
    }

    public void requestToGetFriendsList(final OnGetFbFriendsListListener listener) {
        Request request = Request.newMyFriendsRequest(Session.getActiveSession(), new Request.GraphUserListCallback() {

            @Override
            public void onCompleted(List<GraphUser> friendsList, Response response) {
                if (friendsList != null) {
                    L.i(TAG, "requestToGetFriendsList. friends.size: " + friendsList.size());
                    setFriendList(friendsList);
                    listener.onGetFbFriendsListSucces(friendsList);
                } else {
                    listener.onGetFbFriendsListFail("Get friends list fail");
                }
            }
        });
        request.executeAsync();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (activity != null) {
            mProgressDialog = UIUtils.getLoadingDialog(activity);
            mProgressDialog.show();
            Session.getActiveSession().onActivityResult(activity, requestCode, resultCode, data);
        }
    }

    public boolean isLogin() {
        return Session.getActiveSession() != null;
    }

    private boolean hasAllPermission() {
        boolean hasAllPermission = true;
        Session session = Session.getActiveSession();
        if (session != null) {
            for (String param : PERMISSIONS) {
                if (!session.getPermissions().contains(param)) {
                    hasAllPermission = false;
                }
            }
        } else {
            hasAllPermission = false;
        }

        return hasAllPermission;
    }

    public GraphUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(GraphUser currentUser) {
        this.currentUser = currentUser;
    }

    public List<GraphUser> getFriendList() {
        return friendList;
    }

    public void setFriendList(List<GraphUser> friendList) {
        this.friendList = friendList;
    }

    public String getVolueFromUrl(String url, String key) {
        String value = "";
        if (url != null) {
            String array[] = url.split("&");
            for (String parameter : array) {
                String v[] = parameter.split("=");

                if (v.length == 2 && v[0].equals(key)) {
                    value = v[1].trim();
                    break;
                }
            }
        }
        return value;
    }

    public static interface OnFbLoginStatusListener {
        public void onFbLoginSuccess(String facebookAccessToken, String action);

        public void onFbLoginFail(String result);
    }

    public static interface OnGetCurrentUserListener {
        public void onGetFbUserSucces(GraphUser user);

        public void onGetFbUserFail(String result);
    }

    public static interface OnGetFbFriendsListListener {
        public void onGetFbFriendsListSucces(List<GraphUser> friendsList);

        public void onGetFbFriendsListFail(String result);
    }
}