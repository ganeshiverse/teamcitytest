package com.axsystech.excelicare.app;

import com.axsystech.excelicare.data.responses.RegisterResponse;
import com.axsystech.excelicare.data.responses.SiteDetailsModel;
import com.axsystech.excelicare.db.models.MediaItemDbModel;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.axsystech.excelicare.db.models.User;

import java.util.ArrayList;

/**
 * Singleton class which holds the global data used by components in entire application.
 * Created by someswarreddy on 30/06/15.
 */
public class GlobalDataModel {

    private static GlobalDataModel mInstance = null;

    // Which will hold the system preference values retrieved from 'getRegistrationDetailsByUser' API
    private ArrayList<SystemPreferenceDBModel> mSystemPreferencesAL;

    // Which will hold the Site details along with client URLs retrieved from 'getRegistrationDetailsByUser' API
    private ArrayList<SiteDetailsModel> mSiteDetailsModelArrayList;

    // This will be updated once user is logged-in to application & also when user click on Logged-in user displayed in Left Menu
    private User mLoginUser;

    // This will be updated once user click on cared members in Left menu
    private User mActiveUser;

    private GlobalDataModel() {
        // Exists only to defeat instantiation
    }

    /**
     * Method to get the singleton instance of GlobalDataModel
     * @return
     */
    public static GlobalDataModel getInstance() {
        if (mInstance == null) {
            mInstance = new GlobalDataModel();
        }

        return mInstance;
    }

    public ArrayList<SystemPreferenceDBModel> getSystemPreferencesAL() {
        return mSystemPreferencesAL;
    }

    public void setSystemPreferencesAL(ArrayList<SystemPreferenceDBModel> mSystemPreferencesAL) {
        this.mSystemPreferencesAL = mSystemPreferencesAL;
    }

    public void setSiteList(ArrayList<SiteDetailsModel> siteDetailsModelArrayList) {
        this.mSiteDetailsModelArrayList = siteDetailsModelArrayList;
    }

    public ArrayList<SiteDetailsModel> getSiteList() {
        return this.mSiteDetailsModelArrayList;
    }

    public User getLoginUser() {
        return mLoginUser;
    }

    public void setLoginUser(User mLoginUser) {
        this.mLoginUser = mLoginUser;
    }

    public User getActiveUser() {
        return mActiveUser;
    }

    public void setActiveUser(User mActiveUser) {
        this.mActiveUser = mActiveUser;
    }

    /**
     * This variable will be used in Media module.
     * If this is 'true' - means user will be allowed to selected multiple media items for upload, delete, etc
     * If this is 'false' - user cannot select multiple media items.
     */
    private boolean isMultipleSelectionEnabled = false;

    public boolean isMultipleSelectionEnabled() {
        return isMultipleSelectionEnabled;
    }

    public void setMultipleSelectionEnabled(boolean isMultipleSelectionEnabled) {
        this.isMultipleSelectionEnabled = isMultipleSelectionEnabled;
    }

}
