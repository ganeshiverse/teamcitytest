package com.babylon.controllers;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import com.babylon.utils.DateUtils;

import java.util.Calendar;
import java.util.Date;

public abstract class DateViewController implements DatePickerDialog.OnDateSetListener, View.OnClickListener {
    private OnDateSetListener onDateSetListener;
    protected Date currentDate;
    protected TextView dateTextView;
    protected Context context;
    private boolean enabled = true;

    public DateViewController(Context context, TextView birthDateTextView, OnDateSetListener onDateSet,
                              Date currentDate) {
        this.dateTextView = birthDateTextView;
        this.onDateSetListener = onDateSet;
        this.currentDate = currentDate;
        this.context = context;

        birthDateTextView.setOnClickListener(this);
    }

    public DateViewController() {};

    public String getFormattedDay(Date date) {
        return DateUtils.getInstance().getDateFormat(getFormatDate())
                .format(date);
    }

    public String getFormattedDay(long dateInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        Date date = calendar.getTime();
        return getFormattedDay(date);
    }

    public abstract String getFormatDate();

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        Date date = calendar.getTime();

        dateTextView.setText(getFormattedDay(date));
        currentDate = date;

        if (onDateSetListener != null) {
            onDateSetListener.onDateSet(date);
        }
    }

    protected long getDatePickerMaxDate() {
        return 0;
    }

    protected long getDatePickerMinDate() {
        return 0;
    }

    /**
     *
     * @return pickers use constant pickers from @see DatePickerDialog
     * You can choose what you show in picker : day, month, year
     * If you are not using this param or send null, by default show all pickers
     */
    protected String[] getVisiblePickers() {
        return null;
    }

    @Override
    public void onClick(View view) {
        if (enabled) {
            com.babylon.view.DatePickerDialog datePickerDialog = new com.babylon.view.DatePickerDialog(context,
                    currentDate, this, getVisiblePickers());
            long maxDate = getDatePickerMaxDate();
            long minDate = getDatePickerMinDate();
            if (maxDate != 0) {
                datePickerDialog.setMaxDate(maxDate);
            }
            if (minDate != 0) {
                datePickerDialog.setMinDate(minDate - DateUtils.SECOND_IN_MILLS);
            }

            datePickerDialog.show();
        }
    }

    public void setCurrentDate(Date date) {
        this.currentDate = date;
    }

    public void setCurrentDateText(Date date) {
        if (date != null && dateTextView != null) {
            dateTextView.setText(getFormattedDay(date));
        }
    }

    public Date getCurrentDate() {
        return this.currentDate;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
