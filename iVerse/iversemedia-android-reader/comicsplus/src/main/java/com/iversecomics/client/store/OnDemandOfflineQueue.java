package com.iversecomics.client.store;

public class OnDemandOfflineQueue {
    private String product_id;
    private String SKU;
    private String image_filename;
    private int tier;
    private int download_count;
    private String purchase_date;
    private String amount_usd;
    private String name;
    private String comic_bundle_name;


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getImage_filename() {
        return image_filename;
    }

    public void setImage_filename(String image_filename) {
        this.image_filename = image_filename;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public int getDownload_count() {
        return download_count;
    }

    public void setDownload_count(int download_count) {
        this.download_count = download_count;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    public String getAmount_usd() {
        return amount_usd;
    }

    public void setAmount_usd(String amount_usd) {
        this.amount_usd = amount_usd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComic_bundle_name() {
        return comic_bundle_name;
    }

    public void setComic_bundle_name(String comic_bundle_name) {
        this.comic_bundle_name = comic_bundle_name;
    }





}