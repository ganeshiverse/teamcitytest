package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesListDbModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by vvydesai on 9/11/2015.
 */
public class GetCircleMemberListFromDbLoader extends DataAsyncTaskLibLoader<List<CircleMemberListDbModel>> {

    String mCircleId;

    public GetCircleMemberListFromDbLoader(final Context context, String mCircleId) {
        super(context, true);
        this.mCircleId = mCircleId;

        System.out.println("Get the count" + mCircleId);
    }

    @Override
    protected List<CircleMemberListDbModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<CircleMemberListDbModel, Long> circleMemberListDao = helper.getDao(CircleMemberListDbModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<CircleMemberListDbModel>>() {
                @Override
                public List<CircleMemberListDbModel> call() throws Exception {
                    return circleMemberListDao.queryBuilder().where().eq(CircleMemberListDbModel.CIRCLE_USER_MEMBER_CIRCLEID, mCircleId).query();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
