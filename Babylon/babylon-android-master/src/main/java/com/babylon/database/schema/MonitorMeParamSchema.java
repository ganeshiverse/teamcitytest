package com.babylon.database.schema;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;
import com.babylon.database.Table;
import com.babylon.enums.MonitorMeParam;
import com.babylon.sync.SyncUtils;

public interface MonitorMeParamSchema {

    Uri BASE_CONTENT_URI = Uri.parse("content://" + DatabaseHelper.CONTENT_AUTHORITY);
    Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("entity").appendPath(Table.MONITOR_ME_PARAM.toString()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        DATE("date", DBType.INT),
        FEATURE_ID("id", DBType.TEXT),
        NAME("name", DBType.TEXT),
        VALUE("value", DBType.TEXT),
        UNITS("units", DBType.TEXT),
        STATE("state", DBType.INT),
        TARGET_VALUE("normalValue", DBType.TEXT),
        DESCRIPTION("description", DBType.TEXT);

        private String columnName;
        private DBType type;

        private Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        @Override
        public String toString() {
            return columnName;
        }
    }

    public final static class Query extends BaseQuery {
        public static final String[] PROJECTION = new String[Columns.values().length];

        public static final int CURSOR_LOAD_MONTH_PARAMS = 3457;
        public static final int CURSOR_LOAD_SIX_MONTH_PARAMS = 3458;
        public static final int CURSOR_LOAD_YEAR_PARAMS = 3459;
        public static final int CURSOR_LOAD_DAY_PARAMS = 3460;
        public static final int CURSOR_LOAD_USER_PARAMS = 1000;
        public static final int CURSOR_LOAD_MONITOR_ME_REPORT = 2000;
        public static final int CURSOR_LOAD_SLEEP_PARAMS = 3470;
        public static final int CURSOR_LOAD_BMR_TODAY = 3468;
        public static final int CURSOR_LOAD_WEARABLE_CALORIES_TODAY = 3469;

        public static final String NOT_NULL_AND_PERIOD_SELECTION_STRING = " AND " +
                Columns.DATE.getName() + "> ? AND " +
                Columns.DATE.getName() + "<= ? AND " +
                Columns.VALUE.getName() + " IS NOT NULL AND "
                + Columns.SYNC_WITH_SERVER + " != "
                + String.valueOf(SyncUtils.SYNC_TYPE_DELETE);
        public static final String NOT_ZERO = " AND " + Columns.VALUE.getName() + " != 0";
        public static final String SORT_DATE_ASC = Columns.DATE.getName() + " ASC";
        public static final String SORT_ORDER_DESC_LIMIT_1 = Columns.DATE.getName() + " DESC LIMIT 1";
        public static final String NOT_THIS_DAY_SELECTION = Columns.DATE.getName() + "< ? OR "
                + Columns.DATE.getName() + ">= ?";

        public static final String DAY_SELECTION = Columns.DATE.getName() + ">= ? AND "
                + Columns.DATE.getName() + "< ? AND " + Columns.SYNC_WITH_SERVER + " != "
                + String.valueOf(SyncUtils.SYNC_TYPE_DELETE);
        public static final String PARAM_DAY_SELECTION =  DAY_SELECTION + " AND " + Columns.FEATURE_ID.getName() + " " +
                "= ? ";
        public static final String PERIOD_DELETE_SELECTION = Columns.DATE.getName() + ">= ? AND "
                + Columns.DATE.getName() + "< ? AND " + Columns.SYNC_WITH_SERVER + " != "
                + String.valueOf(SyncUtils.SYNC_TYPE_ADD);
        public static final String PULSE_NOT_NULL_AND_DATE_SELECTION =
                Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.RESTING_PULSE_ID.getId()
                        + NOT_ZERO
                        + NOT_NULL_AND_PERIOD_SELECTION_STRING;
        public static final String BMI_NOT_NULL_AND_DATE_SELECTION =
                Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.BMI_ID.getId()
                        + NOT_ZERO
                        + NOT_NULL_AND_PERIOD_SELECTION_STRING;
        public static final String WEIGHT_NOT_NULL_AND_DATE_SELECTION =
                Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.WEIGHT_ID.getId()
                        + NOT_ZERO
                        + NOT_NULL_AND_PERIOD_SELECTION_STRING;
        public static final String CALORIES_NOT_NULL_AND_DATE_SELECTION =
                "( " + Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.CALORIES_IN_ID.getId()
                        + " OR " + Columns.FEATURE_ID.getName() + " = "
                        + MonitorMeParam.CALORIES_BURNT_ID.getId()
                        + " OR " + Columns.FEATURE_ID.getName() + " = "
                        + MonitorMeParam.CALORIES_REMAINING_ID.getId() + " )"
                        + NOT_NULL_AND_PERIOD_SELECTION_STRING;
        public static final String CHOLESTEROL_NOT_NULL_AND_DATE_SELECTION =
                "( " + Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.CHOLESTEROL_ID.getId()
                        + " OR " + Columns.FEATURE_ID.getName() + " = "
                        + MonitorMeParam.CHOLESTEROL_HDL_ID.getId() + " )"
                        + NOT_NULL_AND_PERIOD_SELECTION_STRING;
        public static final String BLOOD_NOT_NULL_AND_DATE_SELECTION =
                "( " + Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.SYSTOLIC_BP_ID.getId()
                        + " OR " + Columns.FEATURE_ID.getName() + " = "
                        + MonitorMeParam.DIASTOLIC_BP_ID.getId() + " )"
                        + NOT_ZERO
                        + NOT_NULL_AND_PERIOD_SELECTION_STRING;
        public static final String SLEEP_FEATURE_DATE_SELECTION =
                Columns.FEATURE_ID.getName() + " = " + MonitorMeParam.SLEEP_ID.getId()
                        + " AND " + Columns.DATE.getName() + ">= ? AND " +
                        Columns.DATE.getName() + "< ?";

        public static final String BMR_TODAY_SELECTION =
                DAY_SELECTION + " AND " +  Columns.FEATURE_ID.getName() + "=" + MonitorMeParam.BMR.getId();

        public static final String OTHER_CALORIES_TODAY_SELECTION =
                DAY_SELECTION + " AND " +  Columns.FEATURE_ID.getName() + "=" + MonitorMeParam.WEARABLE_CALORIES.getId();

        static {
            for (int i = 0; i < PROJECTION.length; i++) {
                PROJECTION[i] = Columns.values()[i].getName();
            }
        }

        public static final int _ID = 0;
        public static final int SYNC_WITH_SERVER = 1;
        public static final int DATE = 2;
        public static final int FEATURE_ID = 3;
        public static final int NAME = 4;
        public static final int VALUE = 5;
        public static final int UNITS = 6;
        public static final int STATE = 7;
        public static final int TARGET_VALUE = 8;
        public static final int DESCRIPTION = 9;
        public static final int RANGES = 10;
    }
}
