package com.axsystech.excelicare.util;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.AxSysTechApplication;

import java.io.File;

/**
 * This is responsible for verifying and selecting the application storage directory where we can store
 * excelicare files and databases.
 *
 * @author SomeswarReddy
 */
public class MemoryUtil {
    private static final String MNT_MEDIA_NOOK_COLOR = "/mnt/media/";

    /**
     * This method is used for selecting and verifying the application storage directory
     *
     * @param context {@link Context} of the application
     * @return application storage directory to store content.zip files and databases
     */
    public static String getAppDirectoryPath(Context context) {

        String directoryPath = SharedPreferenceUtil.getStringFromSP(AxSysTechApplication.getAppContext(), AxSysConstants.APP_DIRECTORY_PATH);

        if (!TextUtils.isEmpty(directoryPath)) {
            return directoryPath;
        }

        File externalFilesDir = context.getExternalFilesDir(null); // non-removable sd card
        if (externalFilesDir != null) {
            if (isMemoryAvailable(externalFilesDir.toString())) {
                directoryPath = externalFilesDir + AxSysConstants.BASE_APP_DIRECTORY;
                SharedPreferenceUtil.saveStringInSP(AxSysTechApplication.getAppContext(), AxSysConstants.APP_DIRECTORY_PATH, directoryPath);

                return directoryPath;
            }
        }

        File ed = Environment.getDataDirectory(); // Application directory
        if (ed != null && isMemoryAvailable(ed)) {
            File mydir = context.getDir(AxSysTechApplication.getAppContext().getString(R.string.app_name), Context.MODE_PRIVATE); //Creating an internal dir;
            directoryPath = mydir.getAbsolutePath() + AxSysConstants.BASE_APP_DIRECTORY;
            SharedPreferenceUtil.saveStringInSP(AxSysTechApplication.getAppContext(), AxSysConstants.APP_DIRECTORY_PATH, directoryPath);

            return directoryPath;
        }

        File externalStorageDirectory = Environment.getExternalStorageDirectory(); // External removable sd card
        if (externalStorageDirectory != null) {
            if (isMemoryAvailable(externalStorageDirectory.toString())) {
                directoryPath = externalStorageDirectory + AxSysConstants.BASE_APP_DIRECTORY;
                SharedPreferenceUtil.saveStringInSP(AxSysTechApplication.getAppContext(), AxSysConstants.APP_DIRECTORY_PATH, directoryPath);

                return directoryPath;
            }
        }

        //for nook color
        if (isMemoryAvailable(MNT_MEDIA_NOOK_COLOR)) {
            directoryPath = MNT_MEDIA_NOOK_COLOR + AxSysConstants.BASE_APP_DIRECTORY;
            SharedPreferenceUtil.saveStringInSP(AxSysTechApplication.getAppContext(), AxSysConstants.APP_DIRECTORY_PATH, directoryPath);

            return directoryPath;
        }


        return null;
    }

    /**
     * This method is used for verifying the memory availability of the given storage directory path.
     *
     * @param dir path of the storage directory
     * @return true-if memory is available, false-otherwise
     */
    public static boolean isMemoryAvailable(String dir) {
        File dirFile = new File(dir + AxSysConstants.BASE_APP_DIRECTORY);
        dirFile.mkdir();

        return dirFile.canRead() && dirFile.canWrite() && isMemoryAvailable(new File(dir));
    }

    /**
     * This method is used for verifying the memory availability of the given storage {@link File}.
     *
     * @param ed selected {@link File} reference
     * @return true-if memory is available, false-otherwise
     */
    public static boolean isMemoryAvailable(File ed) {
        long SINGLE_MB = 1 * 1024 * 1024;
        boolean isAvailable = true;

        StatFs stat = new StatFs(ed.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        double availMem = (blockSize * availableBlocks) / SINGLE_MB;

        if (availMem < AxSysConstants.MANDATORY_SPACE_NEEDED) { //TODO figure out how much space is needed for this app
            isAvailable = false;
        }

        return isAvailable;
    }
}