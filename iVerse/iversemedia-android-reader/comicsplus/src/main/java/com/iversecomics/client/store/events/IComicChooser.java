package com.iversecomics.client.store.events;

public interface IComicChooser {
    OnComicClickListener getOnComicClickListener();

    void setOnComicClickListener(OnComicClickListener listener);
}
