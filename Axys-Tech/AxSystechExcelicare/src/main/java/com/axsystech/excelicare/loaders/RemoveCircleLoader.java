package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 8/27/2015.
 */
public class RemoveCircleLoader extends DataAsyncTaskLibLoader<RemoveCircleResponse> {

    private String token;
    private String userCircleMemberId;
    private String stopReasonSLU;
    private String stopReasonDescription;


    public RemoveCircleLoader(Context context, String token, String userCircleMemberId, String stopReasonDescription, String stopReasonSLU) {
        super(context);
        this.token = token;
        this.userCircleMemberId = userCircleMemberId;
        this.stopReasonSLU = stopReasonSLU;
        this.stopReasonDescription = stopReasonDescription;
    }

    @Override
    protected RemoveCircleResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getCircleRemove(token, userCircleMemberId, stopReasonDescription, stopReasonSLU);
    }
}

