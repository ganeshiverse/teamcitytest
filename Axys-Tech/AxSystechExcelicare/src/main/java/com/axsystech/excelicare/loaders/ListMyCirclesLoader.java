package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.ListMyCirclesResponse;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.List;

/**
 * Created by someswarreddy on 19/08/15.
 */
public class ListMyCirclesLoader extends DataAsyncTaskLibLoader<ListMyCirclesResponse> {

    private String token;
    private String includeHistory;
    private String criteria;
    private int pageIndex;
    private int pageSize;

    public ListMyCirclesLoader(Context context, String token, String includeHistory, String criteria, int pageIndex, int pageSize) {
        super(context);
        this.token = token;
        this.includeHistory = includeHistory;
        this.criteria = criteria;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    @Override
    protected ListMyCirclesResponse performLoad() throws Exception {
        return ServerMethods.getInstance().listMyCircles(token, includeHistory,criteria,String.valueOf(pageIndex),String.valueOf(pageSize));
    }
}
