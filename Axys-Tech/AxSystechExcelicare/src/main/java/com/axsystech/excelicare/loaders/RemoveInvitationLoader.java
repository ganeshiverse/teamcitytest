package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 8/28/2015.
 */
public class RemoveInvitationLoader extends DataAsyncTaskLibLoader<BaseResponse> {

    private String token;
    private String inviteSentId;


    public RemoveInvitationLoader(Context context, String token, String inviteSentId) {
        super(context);
        this.token = token;
        this.inviteSentId = inviteSentId;

    }

    @Override
    protected BaseResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getInviteRemove(token, inviteSentId);
    }
}


