package com.axsystech.excelicare.framework.ui.fragments.circles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.axsystech.excelicare.R;
import com.axsystech.excelicare.app.GlobalDataModel;
import com.axsystech.excelicare.data.responses.BaseResponse;
import com.axsystech.excelicare.data.responses.CircleMemberDetailDataResponse;
import com.axsystech.excelicare.db.models.CircleMemberListDbModel;
import com.axsystech.excelicare.db.models.CirclesMemberDetailsDbModel;
import com.axsystech.excelicare.db.models.User;
import com.axsystech.excelicare.framework.ui.activities.MainContentActivity;
import com.axsystech.excelicare.framework.ui.fragments.BaseFragment;
import com.axsystech.excelicare.framework.ui.messages.MessageComposeFragment;
import com.axsystech.excelicare.loaders.CircleChangeMemberRoleLoader;
import com.axsystech.excelicare.loaders.CircleManageMemberRoleLoader;
import com.axsystech.excelicare.loaders.CircleMemberDeleteLoader;
import com.axsystech.excelicare.loaders.CircleMemberDetailsDbLoader;
import com.axsystech.excelicare.loaders.CircleMembersDetailsLoader;
import com.axsystech.excelicare.loaders.GetCircleMemberDetailsFromDbLoader;
import com.axsystech.excelicare.loaders.GetCircleMemberListFromDbLoader;
import com.axsystech.excelicare.loaders.InvitationSentLoader;
import com.axsystech.excelicare.util.AxSysConstants;
import com.axsystech.excelicare.util.CommonUtils;
import com.axsystech.excelicare.util.Trace;
import com.axsystech.excelicare.util.views.CircleTransform;
import com.axsystech.excelicare.util.views.TypefaceSpan;
import com.mig35.injectorlib.utils.inject.InjectSavedState;
import com.mig35.injectorlib.utils.inject.InjectView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by vvydesai on 8/26/2015.
 */
public class CircleMembersDetailsFragments extends BaseFragment implements View.OnClickListener {

    private MainContentActivity mActivity;
    private String TAG = CircleMembersDetailsFragments.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private CircleMembersDetailsFragments mFragment;

    private ArrayList<CirclesMemberDetailsDbModel> dbCirclesMemberDetailsDbModels;

    private CircleMemberDetailDataResponse.DataObjectMembersList mDataObjectMembersList;
    private static final String MEMBER_DELETE = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments.MEMBER_DELETE";
    private static final String CIRCLE_MEMBER_CHANGE_ROLE = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments.CIRCLE_MEMBER_CHANGE_ROLE";
    private static final String CIRCLE_MEMBER_MANAGE_ROLE = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments.CIRCLE_MEMBER_MANAGE_ROLE";
    private static final String GET_CIRCLE_MEMBER_DETAILS_FROM_DB = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments.GET_CIRCLE_MEMBER_DETAILS_FROM_DB";
    private static final String CIRCLE_MEMBERS_DETAIL = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments.CIRCLE_MEMBERS_DETAIL";
    private static final String SAVE_MEMBER_DETAILS_DB_LOADER = "com.axsystech.excelicare.framework.ui.fragments.circles.CircleMembersDetailsFragments.SAVE_MEMBER_DETAILS_DB_LOADER";


    @InjectSavedState
    private User mActiveUser;

    @InjectView(R.id.MemberNameTextview)
    private TextView textViewMemberName;

    @InjectView(R.id.textViewSpecialist)
    private TextView mTextViewSpecialist;

    @InjectView(R.id.AddressTextView)
    private TextView textViewAddress;

    @InjectView(R.id.textViewAddressTitle)
    private TextView textViewAddressTitle;

    @InjectView(R.id.ContactTextView)
    private TextView textViewConatact;

    @InjectView(R.id.textViewContact)
    private TextView textViewContactTitle;

    @InjectView(R.id.textViewMobileTitle)
    private TextView textViewMobileTitle;

    @InjectView(R.id.textViewHomeTitle)
    private TextView textViewHomeTitle;

    @InjectView(R.id.textViewPhoneTitle)
    private TextView textViewPhoneTitle;

    @InjectView(R.id.MemberImageview)
    private ImageView mMemberImageview;

    @InjectView(R.id.MobileTextView)
    private TextView textViewMobile;

    @InjectView(R.id.HomeNumberTextView)
    private TextView textViewHome;

    @InjectView(R.id.textViewEmailTitle)
    private TextView textViewEmailTitle;

    @InjectView(R.id.buttonEditMember)
    private ImageButton mButtonEditMember;

    @InjectView(R.id.sendMessageTextView)
    private TextView textViewSendMessage;

    @InjectView(R.id.buttonSetRole)
    private ImageButton mButtonSetRole;

    @InjectView(R.id.buttonDeleteMember)
    private ImageButton mButtonDeleteMember;

    @InjectView(R.id.relativeLayoutFooter)
    private RelativeLayout relativeLayoutFooter;


    @InjectView(R.id.EmailTextView)
    private TextView textViewEmail;

    @InjectSavedState
    private boolean mShouldEnableLeftNav;
    boolean menu = true;

    String circleId;
    String memberId;
    String roleType;
    String memberName;
    String homePhone;
    String mobilePhone;
    String imagePath;
    String phone;
    String email;
    String address1;
    String address2;
    String address3;
    String name;
    String mailId;
    String userId;

    int i = 0;
    boolean sendMessage = true;

    @Override
    protected void initActionBar() {
        ((ActionBarActivity) mActivity).getSupportActionBar().setTitle(TypefaceSpan.setCustomActionBarTypeface(mActivity, name));

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV)) {
            mShouldEnableLeftNav = bundle.getBoolean(AxSysConstants.EXTRA_SHOULD_ENABLE_LEFT_NAV);
            if (mShouldEnableLeftNav) {
                mActivity.enableDraweToggle();
            } else {
                mActivity.disableDrawerToggle();
                mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
                mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
            }
        } else {
            mActivity.disableDrawerToggle();
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            mActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_back);
        }
        Trace.d(TAG, "mShouldEnableLeftNav::" + mShouldEnableLeftNav);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_circle_member_detail, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (MainContentActivity) getActivity();
        mFragment = this;
        mLayoutInflater = LayoutInflater.from(mActivity);

        initViews();
        readIntentArgs();
        getDetailsFromDb();
        addEventClickListener();
    }

    private void initViews() {
        if (MainContentActivity.stringIntegerHashMap != null && MainContentActivity.stringIntegerHashMap.size() > 0) {
            for (Map.Entry m : MainContentActivity.stringIntegerHashMap.entrySet()) {
                if (m.getKey().equals("Add Content")) {
                    if (m.getValue() == 1) {
                        menu = true;
                    } else if (m.getValue() == 0) {
                        menu = false;
                    }
                }
                if (m.getKey().equals("Message Providers")) {
                    if (m.getValue() == 1) {
                        sendMessage = true;
                    } else if (m.getValue() == 0) {
                        sendMessage = false;
                    }
                }
            }
        }

        String htmlString = "Send Message";
        textViewSendMessage.setText(Html.fromHtml(htmlString));
        if (menu) {
            relativeLayoutFooter.setVisibility(View.VISIBLE);
        } else {
            relativeLayoutFooter.setVisibility(View.GONE);
        }

    }

    private void getDetailsFromDb() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();
        if (CommonUtils.hasInternet()) {
            // Update selected circle name to global variable and will be used while navigating to circle members list fragment
            displayProgressLoader(false);

            CircleMembersDetailsLoader circleMembersDetailsLoader = new CircleMembersDetailsLoader(mActivity, mActiveUser.getToken(),
                    circleId, memberId);
            if (mFragment.isAdded() && mFragment.getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_MEMBERS_DETAIL), circleMembersDetailsLoader);
            }
        } else {
            final GetCircleMemberDetailsFromDbLoader getCircleMemberDetailsFromDbLoader = new GetCircleMemberDetailsFromDbLoader(mActivity);
            if (isAdded() && getView() != null) {
                getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CIRCLE_MEMBER_DETAILS_FROM_DB), getCircleMemberDetailsFromDbLoader);
            }
        }
    }

    private void readIntentArgs() {
        mActiveUser = GlobalDataModel.getInstance().getActiveUser();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_ID)) {
                memberId = bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_ID);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_CIRCLEID)) {
                circleId = bundle.getString(AxSysConstants.EXTRA_SELECTED_CIRCLEID);
            }

            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_NAME)) {
                name = bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_NAME);
            }
            if(bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_USER_ID)){
                userId = bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_USER_ID);
            }
            if (bundle.containsKey(AxSysConstants.EXTRA_SELECTED_MEMBER_MAIL)) {
                mailId = bundle.getString(AxSysConstants.EXTRA_SELECTED_MEMBER_MAIL);
            }
        }
    }

    private void addEventClickListener() {

        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_reg.ttf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bolditalic.ttf");
        Typeface regularItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/if_std_bold.ttf");

        textViewMemberName.setTypeface(bold);
        textViewSendMessage.setTypeface(bold);
        mTextViewSpecialist.setTypeface(regular);
        textViewContactTitle.setTypeface(bold);
        textViewPhoneTitle.setTypeface(bold);
        textViewConatact.setTypeface(bold);
        textViewMobileTitle.setTypeface(bold);
        textViewMobile.setTypeface(bold);
        textViewHomeTitle.setTypeface(bold);
        textViewHome.setTypeface(bold);
        textViewEmailTitle.setTypeface(bold);
        textViewEmail.setTypeface(bold);
        textViewAddressTitle.setTypeface(bold);
        textViewAddress.setTypeface(bold);

        mButtonDeleteMember.setOnClickListener(this);
        textViewSendMessage.setOnClickListener(this);
        mButtonSetRole.setOnClickListener(this);
        mButtonEditMember.setOnClickListener(this);
    }

    @Override
    public void onLoaderError(int id, Exception exception) {
        super.onLoaderError(id, exception);
        hideProgressLoader();
    }

    @Override
    public void onLoaderResult(int id, Object result) {
        super.onLoaderResult(id, result);
        hideProgressLoader();
        if (id == getLoaderHelper().getLoaderId(CIRCLE_MEMBERS_DETAIL)) {
            if (result != null && result instanceof CircleMemberDetailDataResponse) {
                final CircleMemberDetailDataResponse circleMemberDetailDataResponse = (CircleMemberDetailDataResponse) result;

                dbCirclesMemberDetailsDbModels = new ArrayList<CirclesMemberDetailsDbModel>();

                if (circleMemberDetailDataResponse.getDataObjectMembersList() != null &&
                        circleMemberDetailDataResponse.getDataObjectMembersList() != null) {

                    CirclesMemberDetailsDbModel circlesMemberDetailsDbModel = new CirclesMemberDetailsDbModel();
                    try {
                        circlesMemberDetailsDbModel.setDetailsUserId(circleMemberDetailDataResponse.getDataObjectMembersList().getUserID());
                        circlesMemberDetailsDbModel.setDetailsIsActive(circleMemberDetailDataResponse.getDataObjectMembersList().getIsActive());
                        circlesMemberDetailsDbModel.setAddress1(circleMemberDetailDataResponse.getDataObjectMembersList().getAddress1());
                        circlesMemberDetailsDbModel.setAddress2(circleMemberDetailDataResponse.getDataObjectMembersList().getAddress2());
                        circlesMemberDetailsDbModel.setAddress3(circleMemberDetailDataResponse.getDataObjectMembersList().getAddress3());
                        circlesMemberDetailsDbModel.setRoleTypeSlu(circleMemberDetailDataResponse.getDataObjectMembersList().getRoleType_SLU());
                        circlesMemberDetailsDbModel.setEndReasonComment(circleMemberDetailDataResponse.getDataObjectMembersList().getEndReasonComment());
                        circlesMemberDetailsDbModel.setForeName(circleMemberDetailDataResponse.getDataObjectMembersList().getForeName());
                        circlesMemberDetailsDbModel.setEmail(circleMemberDetailDataResponse.getDataObjectMembersList().getEmail());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dbCirclesMemberDetailsDbModels.add(circlesMemberDetailsDbModel);
                }
                final CircleMemberDetailsDbLoader circleMemberDetailsDbLoader = new CircleMemberDetailsDbLoader(mActivity, dbCirclesMemberDetailsDbModels);
                if (isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(SAVE_MEMBER_DETAILS_DB_LOADER), circleMemberDetailsDbLoader);
                } else {
                    showToast(R.string.no_circle_member_details_found);
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(SAVE_MEMBER_DETAILS_DB_LOADER)) {
            if (result != null && result instanceof ArrayList) {
                dbCirclesMemberDetailsDbModels = (ArrayList<CirclesMemberDetailsDbModel>) result;

                final GetCircleMemberDetailsFromDbLoader getCircleMemberDetailsFromDbLoader = new GetCircleMemberDetailsFromDbLoader(mActivity);
                if (isAdded() && getView() != null) {
                    getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(GET_CIRCLE_MEMBER_DETAILS_FROM_DB), getCircleMemberDetailsFromDbLoader);
                }

            }
        } else if (id == getLoaderHelper().getLoaderId(MEMBER_DELETE)) {
            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponseRemove = (BaseResponse) result;

                if (baseResponseRemove.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showToast(baseResponseRemove.getMessage());
                            mActivity.onBackPressed();
                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CIRCLE_MEMBER_CHANGE_ROLE)) {
            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponseRemove = (BaseResponse) result;

                if (baseResponseRemove.isSuccess()) {

                    if (CommonUtils.hasInternet()) {
                        // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                        displayProgressLoader(false);

                        CircleMembersDetailsLoader circleMembersDetailsLoader = new CircleMembersDetailsLoader(mActivity, mActiveUser.getToken(),
                                circleId, memberId);
                        if (mFragment.isAdded() && mFragment.getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_MEMBERS_DETAIL), circleMembersDetailsLoader);
                        }
                    }
                   /* new Handler().post(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });*/
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(CIRCLE_MEMBER_MANAGE_ROLE)) {
            if (result != null && result instanceof BaseResponse) {
                final BaseResponse baseResponseRemove = (BaseResponse) result;

                if (baseResponseRemove.isSuccess()) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            }
        } else if (id == getLoaderHelper().getLoaderId(GET_CIRCLE_MEMBER_DETAILS_FROM_DB)) {
            if (result != null && result instanceof ArrayList) {
                dbCirclesMemberDetailsDbModels = (ArrayList<CirclesMemberDetailsDbModel>) result;

                System.out.println("Member Details" + dbCirclesMemberDetailsDbModels.toString());
                if (dbCirclesMemberDetailsDbModels != null && dbCirclesMemberDetailsDbModels.size() > 0) {

                    for (int i = 0; i < dbCirclesMemberDetailsDbModels.size(); i++) {
                        try {
                            roleType = dbCirclesMemberDetailsDbModels.get(i).getRoleTypeSlu();
                            memberName = dbCirclesMemberDetailsDbModels.get(i).getForeName();
                            email = dbCirclesMemberDetailsDbModels.get(i).getEmail();
                            address1 = dbCirclesMemberDetailsDbModels.get(i).getAddress1();
                            address2 = dbCirclesMemberDetailsDbModels.get(i).getAddress2();
                            address3 = dbCirclesMemberDetailsDbModels.get(i).getAddress3();
                            homePhone = dbCirclesMemberDetailsDbModels.get(i).getHomePhone();
                            mobilePhone = dbCirclesMemberDetailsDbModels.get(i).getMobilePhone();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                updateUi();
            }
        }
    }

    public void updateUi() {
        textViewMemberName.setText(name);
        mTextViewSpecialist.setText(roleType);
        textViewEmail.setText(email);
        textViewAddress.setText(address1);
        textViewConatact.setText(homePhone);
        Picasso.with(getActivity()).load(R.drawable.user_icon).transform(new CircleTransform()).into(mMemberImageview);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonDeleteMember:

                if (CommonUtils.hasInternet()) {
                    // Update selected circle name to global variable and will be used while navigating to circle members list fragment
                    displayProgressLoader(false);
                    CircleMemberDeleteLoader circleMemberDeleteLoader = new CircleMemberDeleteLoader(mActivity, mActiveUser.getToken(),
                            memberId, "", "");
                    if (mFragment.isAdded() && mFragment.getView() != null) {
                        getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(MEMBER_DELETE), circleMemberDeleteLoader);
                    }
                } else {
                    showToast(R.string.error_no_network);
                }

                break;

            case R.id.buttonSetRole:
                if(CommonUtils.hasInternet()){
                    displayOptionsDialog();
                }
                else {
                    showToast(R.string.error_no_network);
                }

                break;

            case R.id.sendMessageTextView:
                if (CommonUtils.hasInternet()) {
                    final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                    final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                    MessageComposeFragment messageComposeFragment = new MessageComposeFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AxSysConstants.EXTRA_ACTIONBAR_TITLE, "Compose Message");
                    bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_MAILID, mailId);
                    bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_ID, userId);
                    messageComposeFragment.setArguments(bundle);
                    ft.replace(R.id.container_layout, messageComposeFragment, CircleMembersListFragment.class.getSimpleName());
                    ft.addToBackStack(CircleMembersListFragment.class.getSimpleName());
                    ft.commit();
                } else {
                    showToast(R.string.error_network_required);
                }

                break;

            case R.id.buttonEditMember:

                if(CommonUtils.hasInternet()){
                    final FragmentManager activityFragmentManager = mActivity.getSupportFragmentManager();
                    final FragmentTransaction ft = activityFragmentManager.beginTransaction();

                    CircleUserPermissionsFragment circleUserPermissionsFragment = new CircleUserPermissionsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AxSysConstants.EXTRA_SELECTED_MEMBER_ID, memberId);
                    circleUserPermissionsFragment.setArguments(bundle);
                    ft.replace(R.id.container_layout, circleUserPermissionsFragment, CircleMembersListFragment.class.getSimpleName());
                    ft.addToBackStack(CircleMembersListFragment.class.getSimpleName());
                    ft.commit();
                }

                else {
                    showToast(R.string.error_network_required);
                }


                break;
        }
    }

    //
    private void displayOptionsDialog() {
        final CharSequence[] items = {
                getString(R.string.dialog_text_set_role),
                getString(R.string.dialog_text_manager_status)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Set Role");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(getString(R.string.dialog_text_set_role))) {
                    displayRolesDialog();
                } else if (items[item].equals(getString(R.string.dialog_text_manager_status))) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        CircleManageMemberRoleLoader circleChangeMemberRoleLoader = null;
                        circleChangeMemberRoleLoader = new CircleManageMemberRoleLoader(mActivity, mActiveUser.getToken(), circleId, memberId,
                                "1");
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_MEMBER_CHANGE_ROLE), circleChangeMemberRoleLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    // Select Roles Dialog
    private void displayRolesDialog() {

        final CharSequence[] items = {
                getString(R.string.dialog_text_memner),
                getString(R.string.dialog_text_proxy)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Set Role");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.dialog_text_memner))) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        CircleChangeMemberRoleLoader circleChangeMemberRoleLoader = null;
                        circleChangeMemberRoleLoader = new CircleChangeMemberRoleLoader(mActivity, AxSysConstants.LOOKUP_CLINICAL_CARE_MANAGER, mActiveUser.getToken(), circleId, memberId,
                                "1");
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_MEMBER_CHANGE_ROLE), circleChangeMemberRoleLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                } else if (items[item].equals(getString(R.string.dialog_text_proxy))) {
                    if (CommonUtils.hasInternet()) {
                        displayProgressLoader(false);
                        CircleChangeMemberRoleLoader circleChangeMemberRoleLoader = null;
                        circleChangeMemberRoleLoader = new CircleChangeMemberRoleLoader(mActivity, AxSysConstants.LOOKUP_PROXY, mActiveUser.getToken(), circleId, memberId,
                                "1");
                        if (isAdded() && getView() != null) {
                            getLoaderHelper().initAsyncLoader(getLoaderHelper().getLoaderId(CIRCLE_MEMBER_CHANGE_ROLE), circleChangeMemberRoleLoader);
                        }
                    } else {
                        showToast(R.string.error_no_network);
                    }
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
