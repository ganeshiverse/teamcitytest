package com.babylon.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;

import com.babylon.R;
import com.babylon.utils.L;

import java.io.IOException;

public class RingVideoCallService extends Service {

    private static final String TAG = RingVideoCallService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        play();
        return super.onStartCommand(intent, flags, startId);
    }

    private void play() {
        try {
            MediaPlayer mp = new MediaPlayer();
            mp.setDataSource(getApplicationContext(), Uri.parse("android.resource://" + "com.babylon" + "/" + R.raw.ring_starting_video_call));
            mp.setAudioStreamType(AudioManager.STREAM_RING);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.stop();
                    mp.release();
                    stopSelf();
                }
            });
            mp.prepare();

            mp.start();
        } catch (IOException e) {
            L.e(TAG, e.getMessage(), e);
        }
    }

}
