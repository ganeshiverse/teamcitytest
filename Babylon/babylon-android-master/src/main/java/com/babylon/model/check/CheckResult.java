package com.babylon.model.check;

import com.babylon.model.ServerModel;

import java.util.ArrayList;

public class CheckResult extends ServerModel {

    private int bodyPartId;
    private int outcome;
    private ArrayList<SymptomAnswer> answersList;

    public CheckResult() {
    }

    public CheckResult(int bodyPartId, int outcome, ArrayList<SymptomAnswer> answersList) {
        this.bodyPartId = bodyPartId;
        this.outcome = outcome;
        this.answersList = answersList;
    }

    public int getBodyPartId() {
        return bodyPartId;
    }

    public int getOutcome() {
        return outcome;
    }

    public ArrayList<SymptomAnswer> getAnswersList() {
        return answersList;
    }

}
