package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class GCMResponse implements Serializable {

    private static final String SUCCESS = "success";

    @SerializedName("Status")
    public String mStatus;



    @SerializedName("message")
    public String message;

}