package com.iversecomics.client.my;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.iversecomics.client.Storage;
import com.iversecomics.client.my.db.MyComic;
import com.iversecomics.client.my.db.MyComicsTable;
import com.iversecomics.client.store.ServerConfig;
import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.util.DBUtil;

import java.io.File;

public class MyComicsModel {

    public enum PossessionState {
        PURCHASED,    //comic purchased but bundle not available on SD
        AVAILABLE,  //purchased and available
        UNOWNED        //not yet purchased
    }

    private ContentResolver contentResolver;

    public MyComicsModel(Context context) {
        this.contentResolver = context.getContentResolver();
    }

    public Cursor getCursorMyComicByAssetFilename(String assetFilename) {
        String sortOrder = MyComicsTable.DEFAULT_SORT_ORDER;
        String where = MyComicsTable.ASSET_FILENAME + " = ?";
        String whereArgs[] = {assetFilename};
        return contentResolver.query(MyComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
    }

    public Cursor getCursorMyComics() {
        String sortOrder = MyComicsTable.DEFAULT_SORT_ORDER;
        String where = null;
        String whereArgs[] = null;
        return contentResolver.query(MyComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
    }

    // TODO: joliver: 10/24/13 The ON_DEMAND column is not being populated yet.
    // I think we need to capture the value from ComicsTable.SUBSCRIPTION when a comic
    // is inserted to this table.
    public Cursor getCursorMyComics(boolean onDemand) {
        String sortOrder = MyComicsTable.DEFAULT_SORT_ORDER;
        String where = MyComicsTable.ON_DEMAND + "=?";
        String onDemandBoolNum = onDemand ? "1" : "0";
        String whereArgs[] = new String[]{onDemandBoolNum};
        return contentResolver.query(MyComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
    }

    public Cursor getCursorMyComicsMatchingName(String name) {
        String sortOrder = MyComicsTable.DEFAULT_SORT_ORDER;
        String where = MyComicsTable.NAME + " LIKE ?";
        String whereArgs[] = new String[]{"%" + name.toUpperCase() + "%"};
        return contentResolver.query(MyComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
    }

    public Uri insert(MyComic comic) {
        ContentValues values = MyComicsTable.asValues(comic);
        Uri uri = contentResolver.insert(MyComicsTable.CONTENT_URI, values);
        return uri;
    }

    public void update(final MyComic comic) {
        ContentValues values = MyComicsTable.asValues(comic);
        String where = MyComicsTable.COMICID + " = ?";
        String whereArgs[] = {comic.getComicId()};
        int count = contentResolver.update(MyComicsTable.CONTENT_URI, values, where, whereArgs);
    }

    public int updateByAssetName(String assetFilename, MyComic comic) {
        ContentValues values = MyComicsTable.asValues(comic);

        String where = MyComicsTable.ASSET_FILENAME + " = ?";
        String whereArgs[] = {assetFilename};
        int count = contentResolver.update(MyComicsTable.CONTENT_URI, values, where, whereArgs);
        return count;
    }

    public void delete(final MyComic comic) {
        //ContentValues values = MyComicsTable.asValues(comic);
        String where = MyComicsTable.COMICID + " = ?";
        String whereArgs[] = {comic.getComicId()};

        int count = contentResolver.delete(MyComicsTable.CONTENT_URI, where, whereArgs);
    }

    public MyComic getComicByBundleName(final String comicBundleName) {
        Cursor c = null;
        try {
            final String sortOrder = MyComicsTable.NAME + " ASC";
            final String where = MyComicsTable.TABLE + "." + MyComicsTable.COMICID + " = ?";
            final String whereArgs[] = new String[]{
                    comicBundleName
            };
            c = contentResolver.query(MyComicsTable.CONTENT_URI, null, where, whereArgs, sortOrder);
            if (c.moveToFirst() && !c.isAfterLast()) {
                return MyComicsTable.fromCursor(c);
            }
            return null;
        } finally {
            DBUtil.close(c);
        }
    }

    public PossessionState isOwned(final Comic comic) {
        final String comicBundleName = comic.getComicBundleName();
        final MyComic c = getComicByBundleName(comicBundleName);

        if (c != null) {
            try {
                Storage.assertAvailable();
            } catch (Exception e) {
                return PossessionState.UNOWNED;
            }

            //comic has an entry in the db is there a cb on the sd?
            final File file = new File(Storage.getExternalComicsStorageDir(), c.getAssetFilename());

            if (file.exists()) {
                //if the bundle is available the comic is fully in tact
                return PossessionState.AVAILABLE;
            }

            //comic's been purchased - bundle needs to be downloaded
            return PossessionState.PURCHASED;
        } else if (ServerConfig.getDefault().checkForPreviousPurchase(comicBundleName)) {
            //server says that this comic is available to download
            return PossessionState.PURCHASED;
        }

        //comic has never been purchased
        return PossessionState.UNOWNED;
    }
}
