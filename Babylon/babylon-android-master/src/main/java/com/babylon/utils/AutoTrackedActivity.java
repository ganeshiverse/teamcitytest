package com.babylon.utils;

import android.content.ContentResolver;
import android.text.TextUtils;
import com.babylon.App;
import com.babylon.Keys;
import com.babylon.R;
import com.babylon.database.DatabaseOperationUtils;
import com.babylon.database.schema.PhysicalActivitySchema;
import com.babylon.enums.ActivityType;
import com.babylon.model.PhysicalActivity;
import com.babylon.sync.SyncUtils;

public class AutoTrackedActivity {

    public static PhysicalActivity readFromCache() {
        PhysicalActivity walkingActivity = (PhysicalActivity) PreferencesManager.getInstance().getObject(Keys
                .WALKING_ACTIVITY_KEY, PhysicalActivity.class);

        if (TextUtils.isEmpty(walkingActivity.getSource())) {
            walkingActivity.setTitle(ActivityType.WALKING.getTitle());
            walkingActivity.setType(ActivityType.WALKING.getValue());
            walkingActivity.setSource(PhysicalActivity.Source.AUTO);
            walkingActivity.setIntensity(App.getInstance().getString(R.string.add_activity_medium));

        }

        return walkingActivity;
    }

    public static void save(ContentResolver contentResolver) {
        PhysicalActivity walkingActivity = readFromCache();
        saveToDatabase(contentResolver, walkingActivity);

        PreferencesManager.getInstance().setBoolean(Constants.NEED_CLEAN_UP, true);
        PreferencesManager.getInstance().setObject(Keys.WALKING_ACTIVITY_KEY, new PhysicalActivity());
    }

    private static void saveToDatabase(ContentResolver contentResolver, PhysicalActivity walkingActivity) {
        if (walkingActivity != null && walkingActivity.getSteps() != null
                && walkingActivity.getSteps() != 0) {
            int steps = walkingActivity.getSteps();

            if (steps != 0) {
                walkingActivity.setCalories(walkingActivity.getCalories());

                processNewActivity(contentResolver, walkingActivity);
            }
        }
    }

    private static void processNewActivity(ContentResolver contentResolver, PhysicalActivity walkingActivity) {
        PhysicalActivity prevActivity = DatabaseOperationUtils.getInstance().getPhysicalActivityFromDay
                (walkingActivity.getStartDate(), walkingActivity.getSource(), contentResolver);

        if (prevActivity != null) {
            mergeActivities(walkingActivity, prevActivity);
            DatabaseOperationUtils.getInstance().updatePhysicalActivity(walkingActivity, contentResolver);
        } else {
            contentResolver.insert(PhysicalActivitySchema.CONTENT_URI,
                    walkingActivity.toContentValues());
        }
    }

    private static void mergeActivities(PhysicalActivity walkingActivity, PhysicalActivity prevActivity) {
        if (prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADDED) {
            walkingActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_PATCH);
        } else if (prevActivity.getSyncWithServer() == SyncUtils.SYNC_TYPE_ADD) {
            walkingActivity.setSyncWithServer(SyncUtils.SYNC_TYPE_ADD);
        }

        walkingActivity.setCalories(walkingActivity.getCalories() + prevActivity.getCalories());
        walkingActivity.setSteps(walkingActivity.getSteps() + prevActivity.getSteps());
        walkingActivity.setId(prevActivity.getId());
    }
}
