package com.babylon.controllers.payments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.babylon.App;
import com.babylon.api.request.base.RequestListener;
import com.babylon.api.request.base.Response;
import com.babylon.api.request.payments.RedemptionPost;
import com.babylon.model.Patient;
import com.babylon.model.payments.Redemption;

public class PromoCodeController {

    public interface PromoCodeValidationListener {
        void onValidPromoCode(@Nullable Redemption redemption);
        void onInvalidPromoCode(@Nullable String error);
    }

    public void sendPromoCode(@NonNull FragmentActivity activity, @NonNull String promoCode, int patientId,
                              @NonNull final PromoCodeValidationListener promoCodeValidationListener) {

        Patient signedInPatient = App.getInstance().getPatient();
        if (patientId < 1) {
            patientId = Integer.parseInt(signedInPatient.getId());
            if (signedInPatient.getSwitchedToFamilyAccountId() > 0) {
                patientId = signedInPatient.getSwitchedToFamilyAccountId();
            }
        }

        new RedemptionPost().withEntry(promoCode, patientId).execute(activity,
                new RequestListener<Redemption>() {
                    @Override
                    public void onRequestComplete(Response<Redemption> response) {
                        if (response.isSuccess()) {
                            Redemption redemption = response.getData();
                            promoCodeValidationListener.onValidPromoCode(redemption);
                        } else {
                            promoCodeValidationListener.onInvalidPromoCode(
                                    response.getUserResponseError());
                        }
                    }
                });
    }
}
