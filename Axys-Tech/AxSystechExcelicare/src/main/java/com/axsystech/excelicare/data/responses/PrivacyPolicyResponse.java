package com.axsystech.excelicare.data.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by someswar on 7/1/2015.
 */
public class PrivacyPolicyResponse extends BaseResponse {

    @SerializedName("data")
    private PrivacyPolicyHtmlData privacyPolicyHtmlData;

    public PrivacyPolicyHtmlData getPrivacyPolicyHtmlData() {
        return privacyPolicyHtmlData;
    }

    public void setPrivacyPolicyHtmlData(PrivacyPolicyHtmlData privacyPolicyHtmlData) {
        this.privacyPolicyHtmlData = privacyPolicyHtmlData;
    }

    public class PrivacyPolicyHtmlData {
        @SerializedName("PrivacyHTML")
        private String privacyHTML;

        public String getPrivacyHTML() {
            return privacyHTML;
        }

        public void setPrivacyHTML(String privacyHTML) {
            this.privacyHTML = privacyHTML;
        }
    }
}
