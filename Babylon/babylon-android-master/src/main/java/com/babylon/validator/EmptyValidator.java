package com.babylon.validator;

import android.text.TextUtils;

public class EmptyValidator extends BaseValidator {

    public EmptyValidator(BaseValidator nextValidator, String errorMessage) {
        super(nextValidator, errorMessage);
    }

    @Override
    public String validate(String string) {
        return TextUtils.isEmpty(string) ? errorMessage : null;
    }
}
