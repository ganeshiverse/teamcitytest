package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.db.DatabaseHelper;
import com.axsystech.excelicare.db.models.SystemPreferenceDBModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.sqlcipher.android.apptools.OpenHelperManager;
import com.j256.ormlite.sqlcipher.android.apptools.OrmLiteSqliteOpenHelper;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 07.05.14
 * Time: 13:04
 *
 * @author SomeswarReddy
 */
public class GetSystemPreferencesDBLoader extends DataAsyncTaskLibLoader<List<SystemPreferenceDBModel>> {

    public GetSystemPreferencesDBLoader(final Context context) {
        super(context, true);
    }

    @Override
    protected List<SystemPreferenceDBModel> performLoad() throws Exception {
        try {
            final OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);

            final Dao<SystemPreferenceDBModel, Long> systemPrefDao = helper.getDao(SystemPreferenceDBModel.class);

            return TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<List<SystemPreferenceDBModel>>() {
                @Override
                public List<SystemPreferenceDBModel> call() throws Exception {
                    return systemPrefDao.queryForAll();
                }
            });
        } finally {
            OpenHelperManager.releaseHelper();
        }
    }
}
