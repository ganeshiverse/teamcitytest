package com.iversecomics.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.iversecomics.client.iab.AbstractAppStoreAdapter;
import com.iversecomics.store.AppStoreAdapter;
import com.iversecomics.store.AppStoreAdapterLocator;
import com.iversecomics.ui.featured.SubscriptionFragment;

public class SubscritpionActivity extends FragmentActivity {
    private static final int CONTENT_VIEW_ID = 10101010;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frame = new FrameLayout(this);
        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        if (savedInstanceState == null) {
            Fragment subscriptionFragment = new SubscriptionFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(CONTENT_VIEW_ID, subscriptionFragment).commit();
        }

    }

}
