package com.babylon;

import android.os.Environment;

public class BaseConfig {

    public static final boolean LOG_ENABLED = BuildConfig.DEBUG;
    /**
     * Allow test time performance of long server request.
     * OFF this flag before make an release  type build
     */
    public static boolean PERFORMANCE_TEST_ENABLED = false;
    public static final String CACHE_DIR = Environment.getExternalStoragePublicDirectory(
            "/Android/data/" + BuildConfig.APPLICATION_ID + "/cache/").getPath();
}
