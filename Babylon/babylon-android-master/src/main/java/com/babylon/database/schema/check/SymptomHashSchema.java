package com.babylon.database.schema.check;

import android.net.Uri;

import com.babylon.database.DBType;
import com.babylon.database.DatabaseHelper;

import static com.babylon.database.check.CheckTables.SYMPTOM_HASHES;

public interface SymptomHashSchema {
    String CUSTOM_SQL = ", UNIQUE (" + Columns.HASH.getName() + ") ON CONFLICT REPLACE";
    Uri CONTENT_URI = DatabaseHelper.BASE_CONTENT_URI.buildUpon().appendPath("check_entity").appendPath(SYMPTOM_HASHES.getName()).build();

    public enum Columns {
        _ID("_id", DBType.PRIMARY),
        ID("id", DBType.TEXT),
        SYNC_WITH_SERVER("syncWithServer", DBType.INT),
        HASH("hash", DBType.TEXT),
        BODY_PART_ID("bodyPartId", DBType.TEXT),
        OUTCOME_ID("outcomeId", DBType.TEXT);


        private String columnName;
        private DBType type;

        Columns(String columnName, DBType type) {
            this.columnName = columnName;
            this.type = type;
        }

        public String getName() {
            return columnName;
        }

        public DBType getType() {
            return type;
        }

        public String toString() {
            return this.getName();
        }
    }

    public interface Query {

        String[] PROJECTION = {
                SYMPTOM_HASHES + "." + Columns._ID.getName(),
                SYMPTOM_HASHES.getName() + "." + Columns.ID,
                SYMPTOM_HASHES + "." + Columns.SYNC_WITH_SERVER,
                SYMPTOM_HASHES + "." + Columns.HASH,
                SYMPTOM_HASHES + "." + Columns.BODY_PART_ID,
                SYMPTOM_HASHES + "." + Columns.OUTCOME_ID
        };

        int _ID = 0;
        int ID = 1;
        int SYNC_WITH_SERVER = 2;
        int HASH = 3;
        int BODY_PART_ID = 4;
        int OUTCOME_ID = 5;

    }
}
