package com.iversecomics.client.store;

import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.iversecomics.client.IverseApplication;
import com.iversecomics.client.refresh.Freshness;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.util.Dim;
import com.iversecomics.client.util.Time;
import com.iversecomics.json.JSONException;
import com.iversecomics.json.JSONObject;
import com.iversecomics.logging.Logger;
import com.iversecomics.logging.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ServerConfig {

    private static final Logger LOG = LoggerFactory.getLogger(ServerConfig.class);
    private static final String FRESHNESS_KEY = "server-config";
    private static final String PREF_CONFIG_JSON = "ServerConfig.json";
    private static ServerConfig DEFAULT = new ServerConfig();
    /**
     * Internal fields
     */
    private URI _uriMain;
    ;
    private boolean _configured;
    /**
     * Server response fields
     * These field are populated by setFromServerResponse,
     * the names must match the key values in the response JSON.
     */
    private String androidMarketPublicKey;
    private String blogFeed2Title;
    private URI blogFeed2Uri;
    private URI blogFeedUri;
    private Uri categoryBannerImageUri;
    private URI categoryListUri;
    private Uri featureImageUri;
    private URI groupListUri;
    private String groupsByGroupUri;
    private String groupsBySupplierUri;
    private Uri iPadFeatureButtonImageUri;
    private Uri iPadMediumImageUri;
    private Uri largeFeatureImageUri;
    private Uri largeImageUri;
    private Uri mediumImageUri;
    private ArrayList<PreviousPurchase> previousPurchases;
    private Uri previewImageUri;
    private String productsByCategoryUri;
    private String productsByFeaturedCategoryUri;
    private URI productsByFeaturedUri;
    private String productsByGroupUri;
    private String productsBySearchUri;
    private String productsBySeriesUri;
    private String productsBySupplierUri;
    private URI productsNewReleasesUri;
    private URI productsTopFreeUri;
    private URI productsTopPaidUri;
    private String productsViewUri;
    private Uri publisherBannerImageUri;
    private Uri publisherLargeBannerImageUri;
    private Uri publisherLogoImageUri;
    private URI registerPushTokenUri;
    private int serverAPIVersion;
    private URI serverStatusUri;
    private Uri smallImageUri;
    private URI submitRatingUri;
    private URI supplierListUri;
    private String supplierViewUri;
    private boolean updatedLocale;
    private URI verifyPurchaseUri;
    private String applicationParams;
    private URI newsPageUri;
    private String productsByPromotionUri;
    private URI promotionListUri;
    private URI privacyPolicyUri;
    private String webProductUri;
    private URI badCodeRedirectUri;
    private String userAccountStatus;
    private URI productReleaseDatesUri;
    private String productsByReleaseDateUri;
    private URI userAccountsUri;
    private Uri iPadLargeImageUri;
    private String diamondCSLSByLocation;
    private String diamondCSLSByZip;
    private int userPoints;
    private String loginMovieUri;
    private URI featuredSlotsUri;
    private Uri onDemandUri;
    private List<String> availableLanguages;
    private Subscription subscription;
    private Uri featuredPageListUri;
    private Uri productsLatestForDateUri;
    private URI purchaseSubscriptionUri;
    private Uri productsByLanguageUri;
    // joliver: 12/31/2013 not sure what differentiates marvelSingleSignOnLoginUri and marvelSingleSignOnUri
    private Uri marvelSingleSignOnLoginUri;
    private Uri marvelSingleSignOnUri;
    private String userAccountName;

    private List<String> subscriptionProducts;

    public ServerConfig() {
        //set the default cp config file for default configuration
        _uriMain = URI.create("https://development.iverseapps.com/getConfig.php");
        _configured = false;
    }

    public static final ServerConfig getDefault() {
        if (!DEFAULT.isConfigured()) {
            DEFAULT.restoreFromSharedPrefs();
        }
        return DEFAULT;
    }

    private static final SharedPreferences getSharedPreferences() {
        return IverseApplication.getApplication().getSharedPreferences("ServerConfig", 0);
    }

    public static final void setServerUrl(final String url) {
        DEFAULT.setUriMain(url);
    }

    public final void restoreFromSharedPrefs() {
        SharedPreferences prefs = getSharedPreferences();
        String json = prefs.getString(PREF_CONFIG_JSON, null);
        if (json != null) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                setFromServerResponse(jsonObject);
            } catch (JSONException e) {
                Log.e("ServerConfig", "Unable to initialize ServerConfig from shared preferences.", e);
            }
        }
    }

    public URI getproductsByReleaseDateUri(String releaseDate) {
        return toBoundUri(productsByReleaseDateUri, releaseDate);
    }

    public void clear() {
        _configured = false;
        serverAPIVersion = 0;
        androidMarketPublicKey = null;
        blogFeed2Title = null;
        blogFeed2Uri = null;
        blogFeedUri = null;
        categoryBannerImageUri = null;
        categoryListUri = null;
        featureImageUri = null;
        groupListUri = null;
        groupsByGroupUri = null;
        groupsBySupplierUri = null;
        iPadFeatureButtonImageUri = null;
        iPadMediumImageUri = null;
        largeFeatureImageUri = null;
        largeImageUri = null;
        mediumImageUri = null;
        previousPurchases = null;
        previewImageUri = null;
        productsByCategoryUri = null;
        productsByFeaturedCategoryUri = null;
        productsByFeaturedUri = null;
        productsByGroupUri = null;
        productsBySearchUri = null;
        productsBySeriesUri = null;
        productsBySupplierUri = null;
        productsNewReleasesUri = null;
        productsTopFreeUri = null;
        productsTopPaidUri = null;
        productsViewUri = null;
        publisherBannerImageUri = null;
        publisherLargeBannerImageUri = null;
        publisherLogoImageUri = null;
        registerPushTokenUri = null;
        serverStatusUri = null;
        smallImageUri = null;
        submitRatingUri = null;
        supplierListUri = null;
        supplierViewUri = null;
        updatedLocale = false;
        applicationParams = null;
        newsPageUri = null;
        productsByPromotionUri = null;
        promotionListUri = null;
        privacyPolicyUri = null;
        productsByReleaseDateUri = null;
        badCodeRedirectUri = null;
        userAccountStatus = null;
        productReleaseDatesUri = null;
        userAccountsUri = null;
        iPadLargeImageUri = null;
        diamondCSLSByLocation = null;
        diamondCSLSByZip = null;

        featuredSlotsUri = null;
        onDemandUri = null;
        loginMovieUri = null;
        availableLanguages = null;
        registerPushTokenUri = null;

        subscription = null;
    }

    public boolean checkForPreviousPurchase(final String bundleName) {
        if (previousPurchases == null)
            return false;

        for (int i = 0; i < previousPurchases.size(); i++) {
            if (previousPurchases.get(i).getComic_bundle_name().equals(bundleName))
                return true;
        }

        return false;
    }

    public ArrayList<PreviousPurchase> getPreviousPurchases() {
        return previousPurchases;
    }

    public URI getPrivacyPolicyUri() {
        return privacyPolicyUri;
    }

    public String getApplicationParams() {
        return applicationParams;
    }

    public URI getNewsPageUri() {
        return newsPageUri;
    }

    public URI getProductsByPromotionUri(String promotionId) {
        return toBoundUri(productsByPromotionUri, promotionId);
    }

    public URI getPromotionListUri() {
        return promotionListUri;
    }

    public String getProductsByReleaseDateUri() {
        return productsByReleaseDateUri;
    }

    public URI getBadCodeRedirectUri() {
        return badCodeRedirectUri;
    }

    public String getUserAccountStatus() {
        return userAccountStatus;
    }

    public URI getProductReleaseDatesUri() {
        return productReleaseDatesUri;
    }

    public URI getUserAccountsUri() {
        return userAccountsUri;
    }

    public String getAndroidMarketPublicKey() {
        return androidMarketPublicKey;
    }

    public String getBlogFeed2Title() {
        return blogFeed2Title;
    }

    public URI getBlogFeed2Uri() {
        return blogFeed2Uri;
    }

    public URI getBlogFeedUri() {
        return blogFeedUri;
    }

    public Uri getCategoryBannerImageUri(String filename) {
        return Uri.withAppendedPath(categoryBannerImageUri, filename + ".png");
    }

    /**
     * Get the list of Categories (Genres).
     *
     * @return the Server URI for the JSON/REST call.
     */
    public URI getCategoryListUri() {
        return categoryListUri;
    }

    /**
     * Get the Featured Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#FEATURED
     */
    public Uri getFeatureImageUri(String filename) {
        return Uri.withAppendedPath(featureImageUri, filename + ".png");
    }

    public URI getGroupListUri() {
        return groupListUri;
    }

    public URI getGroupsByGroupUri(String groupId) {
        return toBoundUri(groupsByGroupUri, groupId);
    }

    public URI getGroupsBySupplierUri(String supplierId) {
        return toBoundUri(groupsBySupplierUri, supplierId);
    }

    public Uri getiPadFeatureButtonImageUri(String filename) {
        return Uri.withAppendedPath(iPadFeatureButtonImageUri, filename + ".png");
    }

    /**
     * Get the Medium Sized (for Tablet) Cover Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#COVER_TABLET_MEDIUM
     */
    public Uri getiPadMediumImageUri(String filename) {
        return Uri.withAppendedPath(iPadMediumImageUri, filename + ".png");
    }

    /**
     * Get the large Sized (for Tablet) Cover Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#COVER_TABLET_LARGE
     */
    public Uri getiPadLargeImageUri(String filename) {
        return Uri.withAppendedPath(iPadLargeImageUri, filename + ".png");
    }

    /**
     * Get the Large Sized Featured Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#FEATURED_LARGE
     */
    public Uri getLargeFeatureImageUri(String filename) {
        return Uri.withAppendedPath(largeFeatureImageUri, filename + ".png");
    }

    /**
     * Get the Large Sized Featured Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#COVER_LARGE
     */
    public Uri getLargeImageUri(String filename) {
        return Uri.withAppendedPath(largeImageUri, filename + ".png");
    }

    /**
     * Get the Medium Sized Featured Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#COVER_MEDIUM
     */
    public Uri getMediumImageUri(String filename) {
        return Uri.withAppendedPath(mediumImageUri, filename + ".png");
    }

    public Uri getPreviewImageUri(String filename) {
        return Uri.withAppendedPath(previewImageUri, filename + ".png");
    }

    /**
     * Get Products (Comics) that belong to a specific Category (Genre).
     *
     * @param categoryId the category id {@link Genre#getGenreId()}
     * @return the Server URI for the JSON/REST call.
     */
    public URI getProductsByCategoryUri(String categoryId, int numberToSkip, int limit) {
        URI myUri = toBoundUri(productsByCategoryUri, categoryId);

        URI finalUri = URI.create(myUri.toString() + "&skip=" + numberToSkip + "&limit=" + limit);
        return finalUri;
    }

    public URI getProductsByFeaturedCategoryUri(String categoryId) {
        return toBoundUri(productsByFeaturedCategoryUri, categoryId);
    }

    public URI getProductsByFeaturedUri() {
        return productsByFeaturedUri;
    }

    public URI getProductsByGroupUri(String groupId) {
        return toBoundUri(productsByGroupUri, groupId);
    }

    public URI getProductsBySearchUri(String terms) {
        return toBoundUri(productsBySearchUri, terms);
    }

    public URI getProductsBySeriesUri(String seriesId) {
        return toBoundUri(productsBySeriesUri, seriesId);
    }

    public URI getProductsBySupplierUri(String supplierId) {
        return toBoundUri(productsBySupplierUri, supplierId);
    }

    public URI getProductsNewReleasesUri() {
        return productsNewReleasesUri;
    }

    public URI getProductsTopFreeUri() {
        return productsTopFreeUri;
    }

    public URI getProductsTopPaidUri() {
        return productsTopPaidUri;
    }

    public URI getProductsByReleaseDateUri(String date) {
        return toBoundUri(productsByReleaseDateUri, date);
    }

    public URI getProductsViewUri(String comicBundleName) {
        return toBoundUri(productsViewUri, comicBundleName);
    }

    public URI getComicShopByZIP(String zip) {
        return toBoundUri(diamondCSLSByZip, zip);
    }

    public URI getComicShopByLocation(double longitude, double latitude) {
        return URI.create(String.format(diamondCSLSByLocation, latitude, longitude));
    }

    /**
     * Get the Banner Image for the Supplier (Publisher)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#PUBLISHER_BANNER
     */
    public Uri getPublisherBannerImageUri(String filename) {
        return Uri.withAppendedPath(publisherBannerImageUri, filename + ".png");
    }

    /**
     * Get the Large Format Banner Image for the Supplier (Publisher)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#PUBLISHER_BANNER_LARGE
     */
    public Uri getPublisherLargeBannerImageUri(String filename) {
        return Uri.withAppendedPath(publisherLargeBannerImageUri, filename + ".png");
    }

    /**
     * Get the Icon Image for the Supplier (Publisher)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#PUBLISHER_ICON
     */
    public Uri getPublisherLogoImageUri(String filename) {
        return Uri.withAppendedPath(publisherLogoImageUri, filename + ".png");
    }

    public URI getRegisterPushTokenUri() {
        return registerPushTokenUri;
    }

    public int getServerApiVersion() {
        return serverAPIVersion;
    }

    public URI getServerStatusUri() {
        return serverStatusUri;
    }

    /**
     * Get the Small Sized Cover Image for the Product (Comic)
     *
     * @param filename the filename to fetch (Note: do not include ".png");
     * @return the CDN URI for the image/png
     * @see Dim#COVER_SMALL
     */
    public Uri getSmallImageUri(String filename) {
        return Uri.withAppendedPath(smallImageUri, filename + ".png");
    }

    public URI getSubmitRatingUri() {
        return submitRatingUri;
    }

    public URI getSupplierListUri() {
        return supplierListUri;
    }

    public URI getSupplierViewUri(String supplierId) {
        return toBoundUri(supplierViewUri, supplierId);
    }

    public URI getUriMain() {
        return _uriMain;
    }

    public void setUriMain(final String uriMain) {
        _uriMain = URI.create(uriMain);
    }

    public URI getVerifyPurchaseUri() {
        return verifyPurchaseUri;
    }

    public boolean isConfigured() {
        return _configured;
    }

    public URI getFeaturedSlotsUri() {
        return featuredSlotsUri;
    }

    public boolean isFresh(Freshness freshness) {
        return freshness.isFresh(FRESHNESS_KEY);
    }

    public boolean isUpdatedLocale() {
        return updatedLocale;
    }

    public void setFromServerResponse(JSONObject json) throws JSONException {
        String status = json.getString("status");
        // TODO: joliver: this is bad, it's not a JSONException, it's an error from the server
        // that needs to be communicated to the user.
        if (!"ok".equals(status)) {
            throw new JSONException("Status from server is not ok [" + status + "]");
        }

        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();

        JSONObject results = json.getJSONObject("results");
        Iterator<String> iterkeys = results.keys();
        while (iterkeys.hasNext()) {
            String key = iterkeys.next();

            // handle list of previous purchases
            if ("previousPurchases".equals(key)) {
                Type listType = new TypeToken<ArrayList<PreviousPurchase>>() {}.getType();
                String value = results.getString(key);
                previousPurchases = gson.fromJson(value, listType);
            }
            // handle list of available languages
            else if ("availableLanguages".equals(key)) {
                String value = results.getString(key);
                Type listType = new TypeToken<ArrayList<String>>() {}.getType();
                availableLanguages = gson.fromJson(value, listType);
            }
            // handle list of subscription products
            else if ("appleSubscriptionSKUs".equals(key)) {
                String value = results.getString(key);
                Type listType = new TypeToken<ArrayList<String>>() {}.getType();
                subscriptionProducts = gson.fromJson(value, listType);
            }
            else if ("subscription".equals(key)) {
                parseSubscribtionData(results.getJSONObject(key));
            } else {
                // simple string values
                String value = results.getString(key);
                setProperty(key.replaceAll("URL$", "Uri"), value);
            }
        }

        //Save json into shared prefs.
        getSharedPreferences().edit().putString(PREF_CONFIG_JSON, json.toString()).commit();

        _configured = true;
    }

    private void parseSubscribtionData(JSONObject subscription) {

        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        this.subscription = gson.fromJson(subscription.toString(), Subscription.class);

        /* TODO: joliver: Re-check subscription status after it expires
        NSTimeInterval timeToExpiration = [self.subscriptionEndDate timeIntervalSinceNow];
        if (timeToExpiration > 0) {
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fetchConfigForced:) object:nil];
            [self performSelector:@selector(fetchConfigForced:) withObject:@(YES) afterDelay:timeToExpiration];
        }
        */
    }

    private void setProperty(String key, String value) {
        try {
            Field fld = this.getClass().getDeclaredField(key);
            if (fld != null) {
                Class<?> t = fld.getType();
                if (boolean.class.isAssignableFrom(t)) {
                    fld.setBoolean(this, Boolean.parseBoolean(value));
                } else if (int.class.isAssignableFrom(t)) {
                    fld.setInt(this, (int) Double.parseDouble(value));
                } else if (URI.class.isAssignableFrom(t)) {
                    URI uri = URI.create(value);
                    fld.set(this, uri);
                } else if (Uri.class.isAssignableFrom(t)) {
                    Uri uri = Uri.parse(value);
                    fld.set(this, uri);
                } else if (String.class.isAssignableFrom(t)) {
                    fld.set(this, value);
                } else {
                    LOG.warn("%s is of unknown type? <%s>", fld.getName(), t);
                    fld.set(this, value);
                }
            }
        } catch (Throwable t) {
            LOG.warn(t, "Unable to set field [" + key + "]");
        }
    }

    private URI toBoundUri(String taggedUrl, String value) {
        return URI.create(taggedUrl.replace("%@", Uri.encode(value)));
    }

    public void updateFreshness(Freshness freshness) {
        freshness.setExpiresAfter(FRESHNESS_KEY, Time.HOUR * 1);
    }

    public void setProductsByPromotionUri(String productsByPromotionUri) {
        this.productsByPromotionUri = productsByPromotionUri;
    }

    public int getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(int userPoints) {
        this.userPoints = userPoints;
    }

    public URI getWebProductUri(String productID) {
        return toBoundUri(webProductUri, productID);
    }

    public void setWebProductUri(String webProductUri) {
        this.webProductUri = webProductUri;
    }

    public String getLoginMovieUri() {
        return loginMovieUri;
    }

    public void setLoginMovieUri(String loginMovieUri) {
        this.loginMovieUri = loginMovieUri;
    }

    public URI getOnDemandUri() {
        return URI.create(onDemandUri.toString());
    }

    public List<String> getAvailableLanguages() {
        return this.availableLanguages;
    }

    public List<String> getSubscriptionProducts() {
        return this.subscriptionProducts;
    }

    public URI getPurchaseSubscriptionUri() {
        return purchaseSubscriptionUri;
    }

    public boolean isSubscribed() {
        return (getSubscriptionStatus() == SubscriptionStatus.SubscriptionStatusCurrent);
    }

    public SubscriptionStatus getSubscriptionStatus() {
        SubscriptionStatus status = SubscriptionStatus.SubscriptionStatusUnubscribed;
        try {
            status = SubscriptionStatus.values()[this.subscription.status];
        } catch (IndexOutOfBoundsException e) {
            Log.e(ServerConfig.class.getSimpleName(), "subscriptionStatus is an unexpected value: " + this.subscription.status, e);
        }
        return status;
    }

    public String getSubscriptionStatusString() {
        String value = "Unknown";
        switch (getSubscriptionStatus()) {
            case SubscriptionStatusCurrent:
                value = "Active";
                break;
            case SubscriptionStatusExpired:
                value = "Expired";
                break;
            case SubscriptionStatusUnubscribed:
                value = "Not Subscribed";
                break;
        }
        return value;
    }

    public Date getSubscriptionStartDate() {
        if (subscription != null)
            return subscription.subscriptionStartDate;
        else
            return null;
    }

    public Date getSubscriptionEndDate() {
        if (subscription != null)
            return subscription.subscriptionEndDate;
        else
            return null;
    }

    public enum SubscriptionStatus {
        SubscriptionStatusUnubscribed,
        SubscriptionStatusCurrent,
        SubscriptionStatusExpired
    }

    /**
     * Holds data under "subscription" field from ServerConfig.json
     */
    class Subscription {
        // These serializedName values are redundant since the variable name matches the json key;
        // they're included for clarity and to prevent breakage when refactoring.
        @SerializedName("status")
        int status;
        @SerializedName("latestIssueProductID")
        String latestIssueProductID;
        @SerializedName("latestIssueReleaseDate")
        Date latestIssueReleaseDate;
        @SerializedName("subscriptionStartDate")
        Date subscriptionStartDate;
        @SerializedName("subscriptionEndDate")
        Date subscriptionEndDate;
    }

}
