package com.axsystech.excelicare.loaders;

import android.content.Context;

import com.axsystech.excelicare.data.responses.GetRoleDataResponse;
import com.axsystech.excelicare.data.responses.MessagesInboxAndSentResponse;
import com.axsystech.excelicare.network.ServerMethods;
import com.mig35.loaderlib.loaders.DataAsyncTaskLibLoader;

/**
 * Created by vvydesai on 9/1/2015.
 */
public class GetRoleLoader extends DataAsyncTaskLibLoader<GetRoleDataResponse> {

    private String token;
    private String panelId;
    private String patientId;
    private String filterCriteria;
    private int start;
    private int max;
    private int IsDictionaryJSONResponse;

    public GetRoleLoader(Context context, String token) {
        super(context);
        this.token = token;
    }

    @Override
    protected GetRoleDataResponse performLoad() throws Exception {
        return ServerMethods.getInstance().getRole(token);
    }
}