package com.iversecomics.client.store.json;

import com.iversecomics.client.store.model.Comic;
import com.iversecomics.client.store.model.FeaturedSlot;
import com.iversecomics.client.store.model.Genre;
import com.iversecomics.client.store.model.Group;
import com.iversecomics.client.store.model.Promotion;
import com.iversecomics.client.store.model.Publisher;
import com.iversecomics.json.JSONArray;
import com.iversecomics.json.JSONObject;

import java.util.ArrayList;

public class ResponseParser {
    private boolean getOptBoolean(JSONObject json, String id, boolean defValue) {
        String val = json.optString(id);
        if (val == null) {
            return defValue;
        }
        if (val.length() < 1) {
            return false;
        }
        char ch = Character.toLowerCase(val.charAt(0));
        return ((ch == '1') || (ch == 't') || (ch == 'y'));
    }

    public void parseComics(JSONObject json, IComicStoreParsingEvent callback) {
        callback.onParseStart();

        JSONArray products = json.optJSONArray("results");
        if (products == null) {
            callback.onParseEnd();
            return;
        }

        int count = products.length();
        for (int i = 0; i < count; i++) {
            JSONObject prod = products.optJSONObject(i);
            if (prod == null) {
                continue; // skip, no object?
            }
            Comic comic = new Comic();
            comic.setComicId(prod.optString("id"));
            comic.setName(prod.optString("name"));

            comic.setAmountUSD(prod.optString("amount_usd"));
            comic.setAvgRating(prod.optDouble("average_rating"));
            comic.setComicBundleName(prod.optString("comic_bundle_name"));
            comic.setDescription(prod.optString("description"));
            comic.setImageFileName(prod.optString("image_filename"));
            comic.setLegal(prod.optString("legal"));
            comic.setNumber(prod.optInt("number"));
            comic.setPricingTier(prod.optInt("price_tier"));
            comic.setProductType(prod.optString("product_type"));
            comic.setRawName(prod.optString("raw_name"));
            comic.setSeriesBundleName(prod.optString("series_bundle_name"));
            comic.setSku(prod.optString("sku"));
            comic.setPublisherId(prod.optString("supplier_id"));
            comic.setPublisherLegacyId(prod.optString("supplier_legacy_id"));
            comic.setPublisherName(prod.optString("supplier_name"));
            comic.setSubscription(prod.optInt("subscription") == 1);
            comic.setTapjoy(prod.optInt("tapjoy") == 1);

            JSONArray assets = prod.optJSONArray("assets");
            if (assets != null) {
                int asslen = assets.length();
                String types[] = new String[asslen];
                JSONObject typeObj;
                for (int j = 0; j < asslen; j++) {
                    typeObj = assets.optJSONObject(j);
                    if (typeObj == null) {
                        types[j] = "";
                    } else {
                        types[j] = typeObj.optString("type");
                    }
                }
                comic.setAssetTypes(types);
            }

            if (comic != null) {
                callback.onParsedComic(comic);
            }
        }

        callback.onParseEnd();
    }

    public void parseGenres(JSONObject json, IComicStoreParsingEvent callback) {
        callback.onParseStart();
        JSONArray categories = json.optJSONArray("results");
        if (categories == null) {
            callback.onParseEnd();
            return;
        }

        int count = categories.length();
        for (int i = 0; i < count; i++) {
            JSONObject cat = categories.optJSONObject(i);
            if (cat == null) {
                continue; // skip, no object?
            }
            Genre genre = new Genre();
            genre.setGenreId(cat.optString("id"));
            genre.setName(cat.optString("name"));
            genre.setBannerFilename(cat.optString("banner_filename"));
            genre.setLegacyKey(cat.optString("legacy_key"));

            if (genre != null) {
                callback.onParsedGenre(genre);
            }
        }
        callback.onParseEnd();
    }


    public void parseGroups(JSONArray groups, IComicStoreParsingEvent callback) {
        parseGroups(groups, callback, true);
    }

    public void parseGroups(JSONArray groups, IComicStoreParsingEvent callback, boolean notifyObservers) {
        if (notifyObservers) {
            callback.onParseStart();
        }
        int len = groups.length();
        for (int i = 0; i < len; i++) {
            JSONObject grp = groups.optJSONObject(i);

            Group group = new Group();

            group.setGroupId(grp.optString("id"));
            group.setFeatured(getOptBoolean(grp, "featured", false));
            group.setImageUrl(grp.optString("image_url"));
            group.setBannerImageFileName(grp.optString("banner_image_file_name"));
            group.setLogoImageFileName(grp.optString("logo_image_file_name"));
            group.setParentId(grp.optString("parent_id"));
            group.setPublisherId(grp.optString("supplier_id"));
            group.setName(grp.optString("name"));

            callback.onParsedGroup(group);

            JSONArray nested = grp.optJSONArray("groups");
            if (nested != null) {
                parseGroups(nested, callback, notifyObservers);
            }
        }
        if (notifyObservers) {
            callback.onParseEnd();
        }
    }


    public void parsePublishers(JSONObject json, IComicStoreParsingEvent callback) {
        callback.onParseStart();
        JSONArray suppliers = json.optJSONArray("results");
        if (suppliers == null) {
            callback.onParseEnd();
            return;
        }

        Publisher publisher;

        int count = suppliers.length();
        for (int i = 0; i < count; i++) {
            JSONObject supl = suppliers.optJSONObject(i);
            if (supl == null) {
                continue; // skip, no object?
            }
            publisher = new Publisher();
            publisher.setPublisherId(supl.optString("id"));
            publisher.setName(supl.optString("name"));
            publisher.setImageUrl(supl.optString("image_url"));
            publisher.setBannerImageFileName(supl.optString("banner_image_file_name"));
            publisher.setLogoImageFileName(supl.optString("logo_image_file_name"));
            publisher.setFeatured(getOptBoolean(supl, "featured", false));

            callback.onParsedPublisher(publisher);

            JSONArray groups = supl.optJSONArray("groups");
            if (groups != null) {
                parseGroups(groups, callback, false);
            }
        }
        callback.onParseEnd();
    }

    public void parseFeaturedSlot(JSONObject json, IComicStoreParsingEvent callback) {
        callback.onParseStart();
        JSONArray slots = json.optJSONArray("results");
        if (slots == null) {
            callback.onParseEnd();
            return;
        }

        FeaturedSlot fslot;

        int count = slots.length();
        for (int i = 0; i < count; i++) {
            JSONObject slot = slots.optJSONObject(i);
            if (slot == null) {
                continue; // skip, no object?
            }

            fslot = new FeaturedSlot();
            fslot.setSlotID(slot.optInt("id"));
            fslot.setSlot(slot.optInt("slot_number"));
            fslot.setSlotType(slot.optInt("slot_type"));
            fslot.setTitle(slot.optString("name"));
            fslot.setSubscription(slot.optInt("subscription") == 0 ? false : true);
            fslot.setImageURL(slot.optString("image_url"));
            fslot.setTargetURL(slot.optString("target_url"));
            fslot.setLanguageCode(slot.optString("language_code"));
            fslot.setProductID(slot.optInt("product_id"));
            fslot.setPromotionID(slot.optInt("promotion_id"));
            fslot.setSupplierID(slot.optInt("supplier_id"));
            fslot.setGroupID(slot.optInt("group_id"));

            callback.onParsedFeaturedSlot(fslot);
        }
        callback.onParseEnd();
    }

    public ArrayList<Promotion> parsePromotion(JSONObject json, IComicStoreParsingEvent callback) {
        if (callback != null) callback.onParseStart();
        JSONArray promotions = json.optJSONArray("results");
        if (promotions == null) {
            if (callback != null) callback.onParseEnd();
            return null;
        }

        ArrayList<Promotion> res_list = new ArrayList<Promotion>();

        Promotion promotion;

        int count = promotions.length();
        for (int i = 0; i < count; i++) {
            JSONObject promoJSONObj = promotions.optJSONObject(i);
            if (promoJSONObj == null) {
                continue; // skip, no object?
            }

            promotion = new Promotion();
            promotion.setActive(promoJSONObj.optInt("active") == 1 ? true : false);
            promotion.setPromotionID(promoJSONObj.optInt("id"));
            promotion.setName(promoJSONObj.optString("name"));
            promotion.setImageURLString(promoJSONObj.optString("content_url"));
            promotion.setImageURLPhoneString(promoJSONObj.optString("content_url_phone"));
            promotion.setTargetURLString(promoJSONObj.optString("target_url"));

            if (callback != null) callback.onParsedPromotion(promotion);

            res_list.add(promotion);
        }
        if (callback != null) callback.onParseEnd();
        return res_list;
    }

}
