package com.babylon.fragment.check;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.babylon.R;
import com.babylon.adapter.check.ChildSymptomAdapter;
import com.babylon.database.schema.check.SymptomSchema;
import com.babylon.enums.CheckGender;
import com.babylon.model.check.export.Symptom;

import java.util.ArrayList;
import java.util.List;

import static com.babylon.database.schema.check.SymptomSchema.SymptomQuery;

public class ChildSymptomDialogFragment extends DialogFragment implements DialogInterface.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final String KEY_PARENT_ID = "KEY_PARENT_ID";
    private static final String KEY_PARENT_NAME = "KEY_PARENT_NAME";
    private static final String KEY_GENDER = "KEY_GENDER";

    private static final int CHILD_SYMPTOM_LOADER = 1012;

    private int parentId;
    private String parentName;

    private List<Symptom> symptoms = new ArrayList<Symptom>();
    private ChildSymptomAdapter childQuestionsAdapter;
    private CheckGender gender;


    public static ChildSymptomDialogFragment newInstance(CheckGender gender, String parentName, int parentId) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_GENDER, gender);
        bundle.putInt(KEY_PARENT_ID, parentId);
        bundle.putString(KEY_PARENT_NAME, parentName);
        ChildSymptomDialogFragment fragment = new ChildSymptomDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null) {
            parentId = getArguments().getInt(KEY_PARENT_ID);
            parentName = getArguments().getString(KEY_PARENT_NAME);
            gender = (CheckGender) getArguments().getSerializable(KEY_GENDER);
        }
        return new AlertDialog.Builder(getActivity())
                .setTitle(parentName)
                .setAdapter(childQuestionsAdapter, this).create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(CHILD_SYMPTOM_LOADER, null, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        childQuestionsAdapter = new ChildSymptomAdapter(getActivity(), symptoms, R.layout.list_item_child_symptom);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Symptom symptom = symptoms.get(which);
        QuestionsFragment questionsFragment = (QuestionsFragment) getTargetFragment();
        questionsFragment.onChildSymptomSelected(parentId, symptom);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Loader callbacks
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] selectionArgs = new String[]{
                String.valueOf(gender.checkValue),
                String.valueOf(parentId)
        };
        return new CursorLoader(getActivity(),
                SymptomSchema.CONTENT_URI_WITH_CHILDREN_COUNT,
                SymptomQuery.PROJECTION,
                SymptomQuery.SELECTION,
                selectionArgs,
                SymptomQuery.SORT_ASC);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (cursor == null) {
            return;
        }
        while (cursor.moveToNext()) {
            Symptom symptom = new Symptom();
            symptom.createFromCursor(cursor);
            symptoms.add(symptom);
        }
        childQuestionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
}
