//
//  CKBundlePageInfo.h
//  Comics
//
//  Created by Derek Stutsman on 7/8/14.
//  Copyright (c) 2014 iVerse Media. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CKBundlePageType)
{
    CKBundlePageTypeLocalImage,
    CKBundlePageTypeRemoteImage,
    CKBundlePageTypeURL,
    CKBundlePageTypeVideo
};

typedef NS_ENUM(NSInteger, CKBundlePageOrientation)
{
    CKBundlePageOrientationPortrait,
    CKBundlePageOrientationLandscape
};

@interface CKBundlePageInfo : NSObject

@property (assign, nonatomic) CKBundlePageType pageType;
@property (assign, nonatomic) CKBundlePageOrientation orientation;
@property (assign, nonatomic) NSRange binaryRange;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *filePath;

- (instancetype)initWithLocalImageFile:(NSString*)filePath range:(NSRange)range orientation:(CKBundlePageOrientation)orientation;
- (instancetype)initWithRemoteImageURL:(NSURL*)url orientation:(CKBundlePageOrientation)orientation;
- (instancetype)initWithURL:(NSURL*)url;
- (instancetype)initWithVideoURL:(NSURL*)url;

@end
