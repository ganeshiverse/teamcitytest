package com.babylon.adapter.shop;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.babylon.App;
import com.babylon.R;
import com.babylon.model.Kit.OrderProducts;
import com.babylon.model.Kit.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HealthKitCheckListAdapter extends ArrayAdapter<Product> {

    public interface OnPropertyItemClickListener {
        View.OnClickListener getOnPropertyClickListener();
        View.OnClickListener getOnMoreDetailClickListener();

    }

    private static final String TAG = HealthKitCheckListAdapter.class.getSimpleName();
    private  List<OrderProducts> mProductList=new ArrayList<OrderProducts>();
    private static final String NOT_ORDERED_TAG="not_ordered";
    private static final String ORDERED_TAG="ordered";
    private static final String ORDER_TAG="Order";
    private OnPropertyItemClickListener onPropertyItemClickListener;
    public HealthKitCheckListAdapter(Context context, List<Product> categories,
                                     OnPropertyItemClickListener onPropertyItemClickListener,
                                     List<OrderProducts> productList) {
        super(context, 0, categories);
        this.onPropertyItemClickListener = onPropertyItemClickListener;
        this.mProductList=productList;
    }

    private class ViewHolder
    {
        ImageView avatarView;
        TextView catNameView;
        TextView descriptionView;
        Button orderBtn;
        TextView moreView;
        TextView priceView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = View.inflate(getContext(), R.layout.list_item_product, null);
            viewHolder.avatarView = (ImageView) convertView.findViewById(R.id.product_image_view);
            viewHolder.catNameView = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.descriptionView = (TextView) convertView.findViewById(R.id.tvDescription);
            viewHolder.orderBtn=(Button)convertView.findViewById(R.id.imgBtnOrder);
            viewHolder.moreView=(TextView)convertView.findViewById(R.id.tvMore);
            viewHolder.priceView=(TextView)convertView.findViewById(R.id.tvPrice);
            viewHolder.orderBtn.setTag(NOT_ORDERED_TAG);
            if (onPropertyItemClickListener != null) {
                viewHolder.orderBtn.setOnClickListener(onPropertyItemClickListener
                        .getOnPropertyClickListener());
                viewHolder.moreView.setOnClickListener(onPropertyItemClickListener
                        .getOnMoreDetailClickListener());

            }


            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Product product = getItem(position);
        if (product.getImageUrl() != null) {
            Picasso.with(getContext()).load(product.getImageUrl()).into(viewHolder.avatarView);
        }
        OrderProducts prod=new OrderProducts(product.getId());

        if(mProductList.contains(prod)){
           viewHolder.orderBtn.setTag(product.getId()+"-"+ORDERED_TAG);
           viewHolder.orderBtn.setBackgroundResource(R.drawable.btn_product_order);
            viewHolder.orderBtn.setText("");
       }
       else {
           viewHolder.orderBtn.setTag(product.getId() + "-" + NOT_ORDERED_TAG);
           viewHolder.orderBtn.setBackgroundResource(R.drawable.circle);
           viewHolder.orderBtn.setText(ORDER_TAG);
       }
        viewHolder.catNameView.setText(product.getName());
        viewHolder.priceView.setText(App.getInstance().getCurrentRegion().getFormattedPriceString
                (Double.valueOf(product.getPrice())));
        viewHolder.descriptionView.setText(product.getShortDescription());
        if(!TextUtils.isEmpty(product.getLongDescription())) {
            viewHolder.moreView.setTag(product.getName()+"::"+product.getLongDescription());
            viewHolder.moreView.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.moreView.setVisibility(View.GONE);
        }

        return convertView;
    }

   /* public void setOnPropertyItemClickListener(OnPropertyItemClickListener onPropertyItemClickListener) {
        this.onPropertyItemClickListener = onPropertyItemClickListener;
    }*/

}
