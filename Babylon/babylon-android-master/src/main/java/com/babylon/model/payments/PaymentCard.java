package com.babylon.model.payments;

import android.support.annotation.DrawableRes;
import android.text.TextUtils;

import com.babylon.model.BaseModel;
import com.google.gson.annotations.SerializedName;

public class PaymentCard extends BaseModel {
    @SkipFieldInContentValues
    private static final long serialVersionUID = 1820L;

    private Integer id;
    @SerializedName("card_type")
    private String cardType;
    @SerializedName("masked_number")
    private String maskedNumber;
    @SerializedName("expires_on")
    private String expiresOn;
    @SkipFieldInContentValues
    private @DrawableRes int imageResId;

    public Integer getId() {
        return getInt(id);
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getMaskedNumber() {
        return maskedNumber;
    }

    public String getDisplayName() {
        return cardType + " " + maskedNumber.substring(maskedNumber.length() - 8);
    }

    public void setMaskedNumber(String maskedNumber) {
        this.maskedNumber = maskedNumber;
    }

    public String getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(String expiresOn) {
        this.expiresOn = expiresOn;
    }

    public @DrawableRes int getImageResId() {
        return imageResId;
    }

    public void setImageResId(@DrawableRes int imageResId) {
        this.imageResId = imageResId;
    }

    public boolean isCreated() {
        return !TextUtils.isEmpty(maskedNumber);
    }
}
