package com.iversecomics.ui.featured;

import com.iversecomics.otto.event.FeaturedComicsOnDemand;
import com.iversecomics.otto.event.FeaturedSlotsOnDemand;
import com.squareup.otto.Subscribe;

/**
 */
public class OnDemandFeaturedContentAdapter extends AbstractFeaturedContentAdapter {

    @Subscribe
    public void updateFeaturedComicsOnDemand(FeaturedComicsOnDemand fc) {
        setFeaturedComics(fc.getComics());
    }

    @Subscribe
    public void updateFeaturedSlotsOnDemand(FeaturedSlotsOnDemand fc) {
        setSlots(fc.getSlots());
    }

}
