package com.babylon.activity.check;

import com.babylon.R;
import com.babylon.activity.ProfileActivity;
import com.babylon.sync.SyncAdapter;
import com.babylon.utils.Constants;
import com.babylon.view.TopBarInterface;

public class CheckProfileActivity extends ProfileActivity {

    @Override
    public ProfileField[] getFilledProfileFields() {
        return new ProfileField[]{
                ProfileField.GENDER,
                ProfileField.HEIGHT,
                ProfileField.WEIGHT
        };
    }
    @Override
    public ProfileField[] getRequiredFilledProfileFields() {
        return new ProfileField[] {
                ProfileField.HEIGHT,
                ProfileField.WEIGHT,
                ProfileField.GENDER,
                ProfileField.DATE_OF_BIRTH};
    }

    @Override
    public void setTopBar() {
        initializeTopBar();
        setTitle(R.string.check_me_screen_title);
    }

    @Override
    public int getLayoutResID() {
        return R.layout.activity_check_profile;
    }

    @Override
    public int getColorTheme() {
        return TopBarInterface.BLUE_THEME;
    }

    @Override
    public int getBackgroundResId() {
        return R.drawable.monitor_me_background;
    }

    @Override
    public void onProfileSent() {
        SyncAdapter.performSync(Constants.Sync_Keys.SYNC_PATIENT,
                SyncAdapter.SYNC_PUSH);
        finish();
    }
}
